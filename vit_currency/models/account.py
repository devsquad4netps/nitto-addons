from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _

class account_abstract_payment(models.AbstractModel):
    _inherit = "account.abstract.payment"


    @api.onchange('journal_id')
    def journal_id_change(self):
        #return {'domain': {'journal_id': [('type', 'in', ('bank', 'cash')),('currency_id','=',self.currency_id.id)]}}
        return {'domain': {'journal_id': [('type', 'in', ('bank', 'cash'))]}}


    @api.onchange('currency_id')
    def currency_id_change(self):
        self.journal_id = False
        
    # journal_id = fields.Many2one('account.journal', string='Payment Journal', required=True, domain=[('type', 'in', ('bank', 'cash')),('currency_id', '=', currency_id)])

account_abstract_payment()