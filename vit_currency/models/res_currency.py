from odoo import api, fields, models, tools, _


class Currency(models.Model):
    _inherit = "res.currency"

    rate = fields.Float(compute='_compute_current_rate', string='Current Rate.', digits=(12, 14),
                        help='The rate of the currency to the currency of rate 1.')

Currency()


class CurrencyRate(models.Model):
    _inherit = "res.currency.rate"

    rate = fields.Float(digits=(12, 14), default=1.0, help='The rate of the currency to the currency of rate 1')
    rate_calc = fields.Float("Rate Origin",digits=(12,5))

    @api.multi
    @api.onchange('rate_calc')
    def _onchange_rate_calc(self):
        for rec in self:
            try :
                rec.rate = 1/rec.rate_calc
            except ZeroDivisionError :
                rec.rate = 0.0

CurrencyRate()