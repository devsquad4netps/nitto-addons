{
    "name"          : "Manage Decimal Currency",
    "version"       : "0.3",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Accounting",
    "license"       : "LGPL-3",
    "contributors"  : """
    """,
    "summary"       : "Ubah decimal currency rate jadi 14 digit",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
    ],
    "data"          : [
        'views/res_curency.xml',
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}