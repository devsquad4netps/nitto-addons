from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning



class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"


    def get_picking_aju_header_out(self, aju_id, values):
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id or picking.picking_type_id.id == picking.subcontract_id.picking_type_id.picking_type_interco_id.id) and picking.state == 'done' and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            jml_type_kemasan = 0
            packs = []
            for kemasan in picking_id.kemasan_ids:
                if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                    packs.append(kemasan.package_type_id.id)
                    jml_type_kemasan+=1
            if jml_type_kemasan == 0 :
                jml_type_kemasan = 1
            amount_untaxed = 0.00
            for move in picking_id.move_ids_without_package:
                product_exist = picking_id.subcontract_id.product_ids.filtered(lambda prod:prod.product_id.id == move.product_id.id)
                if product_exist:
                    amount_untaxed += (product_exist[0].cost_per_qty*move.quantity_done)
            values.append({
                'NOMOR AJU': aju_id.name or '',
                'KPPBC': self.bc_id.kppbc or '',
                'PERUSAHAAN': self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                'PEMASOK': '',
                'STATUS': '00', #self.bc_id.status or '',
                'KODE DOKUMEN PABEAN': self.bc_id.kode_dokumen_pabean or '',
                'NPPJK': '',
                'ALAMAT PEMASOK': '',
                'ALAMAT PEMILIK': '',
                'ALAMAT PENERIMA BARANG': picking_id.partner_id.street or '',
                'ALAMAT PENGIRIM': '',
                'ALAMAT PENGUSAHA': picking_id.company_id.street or '',
                'ALAMAT PPJK': '',
                'API PEMILIK': '',
                'API PENERIMA': '',
                'API PENGUSAHA': '',
                'ASAL DATA': self.bc_id.asal_data or '',
                'ASURANSI': '0.00',
                'BIAYA TAMBAHAN': '0.00',
                'BRUTO': sum(move.brutto for move in picking_id.move_ids_without_package) or '',
                'CIF': '0.00',
                'CIF RUPIAH': '0.00',
                'DISKON': '0.00',
                'FLAG PEMILIK': '',
                'URL DOKUMEN PABEAN': '',
                'FOB': '0.00',
                'FREIGHT': '0.00',
                'HARGA BARANG LDP': '0.00',
                'HARGA INVOICE': '0.00',
                'HARGA PENYERAHAN': amount_untaxed,
                'HARGA TOTAL': '0.00',
                'ID MODUL': self.bc_id.id_modul or '',
                'ID PEMASOK': '',
                'ID PEMILIK': '',
                'ID PENERIMA BARANG': picking_id.partner_id.vat or '',
                'ID PENGIRIM':'',
                'ID PENGUSAHA': self.company_id.vat or '',
                'ID PPJK': '',
                'JABATAN TTD': self.bc_id.jabatan_ttd or '',
                # 'JUMLAH BARANG': sum(move.quantity_done for move in kanban_id.move_ids_without_package) or '',
                'JUMLAH BARANG': len(picking_id.move_ids_without_package.mapped('product_id')),
                #'JUMLAH KEMASAN': sum(kemasan.qty for kemasan in picking_id.kemasan_ids) or '0.00',
                'JUMLAH KEMASAN': jml_type_kemasan,
                'JUMLAH KEMASAN': '1',
                'JUMLAH KONTAINER': len(picking_id.kontainer_ids),
                'KESESUAIAN DOKUMEN': '',
                'KETERANGAN': '',
                'KODE ASAL BARANG': '',
                'KODE ASURANSI': '',
                'KODE BENDERA': '',
                'KODE CARA ANGKUT': '',
                'KODE CARA BAYAR': '',
                'KODE DAERAH ASAL': '',
                'KODE FASILITAS': '',
                'KODE FTZ': '',
                'KODE HARGA': '',
                'KODE ID PEMASOK': '',
                'KODE ID PEMILIK': '',
                'KODE ID PENERIMA BARANG': self.bc_id.kode_id_penerima or '',
                'KODE ID PENGIRIM': '',
                'KODE ID PENGUSAHA': self.bc_id.kode_id_pengusaha or '',
                'KODE ID PPJK': '',
                'KODE JENIS API': '',
                'KODE JENIS API PEMILIK': '',
                'KODE JENIS API PENERIMA': '',
                'KODE JENIS API PENGUSAHA': '',
                'KODE JENIS BARANG': '',
                'KODE JENIS BC25': '',
                'KODE JENIS NILAI': '',
                'KODE JENIS PEMASUKAN01': '',
                'KODE JENIS PEMASUKAN 02': '',
                'KODE JENIS TPB': self.bc_id.kode_jenis_tpb or '',
                'KODE KANTOR BONGKAR': '',
                'KODE KANTOR TUJUAN': '',
                'KODE LOKASI BAYAR': '',
                '': '',
                'KODE NEGARA PEMASOK': '',
                'KODE NEGARA PENGIRIM': '',
                'KODE NEGARA PEMILIK': '',
                'KODE NEGARA TUJUAN': '',
                'KODE PEL BONGKAR': '',
                'KODE PEL MUAT': '',
                'KODE PEL TRANSIT': '',
                'KODE PEMBAYAR': '',
                'KODE STATUS PENGUSAHA': '',
                'STATUS PERBAIKAN': '',
                'KODE TPS': '',
                'KODE TUJUAN PEMASUKAN': '',
                'KODE TUJUAN PENGIRIMAN': self.bc_id.kode_tujuan_pengiriman or '',
                'KODE TUJUAN TPB': '',
                'KODE TUTUP PU': '',
                'KODE VALUTA': picking_id.subcontract_id.partner_id.property_purchase_currency_id.name or '',#valuta or '',
                'KOTA TTD': self.bc_id.kota_ttd or '',
                'NAMA PEMILIK': '',
                'NAMA PENERIMA BARANG': picking_id.partner_id.name or '',
                'NAMA PENGANGKUT': picking_id.nama_pengangkut or '',
                'NAMA PENGIRIM': '',
                'NAMA PPJK': '',
                'NAMA TTD': self.bc_id.nama_ttd or '',
                'NDPBM': '0.0000',
                'NETTO': sum(move.netto for move in picking_id.move_ids_without_package) or '',
                'NILAI INCOTERM': '0.0000',
                'NIPER PENERIMA': '',
                'NOMOR API': '',
                'NOMOR BC11': '',
                'NOMOR BILLING': '',
                'NOMOR DAFTAR': '',
                'NOMOR IJIN BPK PEMASOK': '',
                'NOMOR IJIN BPK PENGUSAHA': '',
                'NOMOR IJIN TPB': self.bc_id.nomor_ijin_tpb or '',
                'NOMOR IJIN TPB PENERIMA': '',
                'NOMOR VOYV FLIGHT': '',
                'NPWP BILLING': '',
                'POS BC11': '',
                'SERI': self.bc_id.seri or '',
                'SUBPOS BC11': '',
                'SUB SUBPOS BC11': '',
                'TANGGAL BC11': '',
                'TANGGAL BERANGKAT': '',
                'TANGGAL BILLING': '',
                'TANGGAL DAFTAR': '',
                'TANGGAL IJIN BPK PEMASOK': '',
                'TANGGAL IJIN BPK PENGUSAHA': '',
                'TANGGAL IJIN TPB': '',
                'TANGGAL NPPPJK': '',
                'TANGGAL TIBA': '',
                'TANGGAL TTD': self.reformat_date(picking_id.tgl_ttd.strftime('%Y-%m-%d')) if picking_id.tgl_ttd else '',
                'TANGGAL JATUH TEMPO': '',
                'TOTAL BAYAR': '0.00',
                'TOTAL BEBAS': '0.00',
                'TOTAL DILUNASI': '0.00',
                'TOTAL JAMIN': '0.00',
                'TOTAL SUDAH DILUNASI': '0.00',
                'TOTAL TANGGUH': '0.00',
                'TOTAL TANGGUNG': '0.00',
                'TOTAL TIDAK DIPUNGUT': '0.00',
                'URL DOKUMEN PABEAN': '',
                'VERSI MODUL': self.bc_id.versi_modul or '',
                'VOLUME': sum(move.quantity_done * move.product_id.volume for move in picking_id.move_ids_without_package) or '',
                'WAKTU BONGKAR': '',
                'WAKTU STUFFING': '',
                'NOMOR POLISI': picking_id.no_polisi or '',
                'CALL SIGN': '',
                'JUMLAH TANDA PENGAMAN': '0',
                'KODE JENIS TANDA PENGAMAN': '',
                'KODE KANTOR MUAT': '',
                'KODE PEL TUJUAN': '',
                '': '',
                'TANGGAL STUFFING': '',
                'TANGGAL MUAT': '',
                'KODE GUDANG ASAL': '',
                'KODE GUDANG TUJUAN': '',
            })
            break
        return values

    def get_picking_aju_barang_out(self, aju_id, values):
        seri_barang = 1
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id or picking.picking_type_id.id == picking.subcontract_id.picking_type_id.picking_type_interco_id.id) and picking.state == 'done' and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            seri_barang = 1
            for move in picking_id.move_ids_without_package :
                move.seri_barang = seri_barang
                seri_barang += 1
                kemasan_ids = picking_id.kemasan_ids.filtered(lambda kemasan: kemasan.product_id.id == move.product_id.id)
                cost_per_qty = 0
                product_exist = picking_id.subcontract_id.product_ids.filtered(lambda prod:prod.product_id.id == move.product_id.id)
                if product_exist:
                    cost_per_qty = product_exist[0].cost_per_qty
                jumlah_bahan_baku = 0
                if move.production_subcon_id:
                    jumlah_bahan_baku += len(move.production_subcon_id.move_raw_ids)
                kode_kemasan = ''
                if picking_id.kemasan_ids:
                    kode_kemasan = picking_id.kemasan_ids[0].package_type_id.name if picking_id.kemasan_ids[0].package_type_id else ''
                values.append({
                    'NOMOR AJU': aju_id.name.strip() or '',
                    'SERI BARANG': move.seri_barang.strip() or '1',
                    'ASURANSI': '0.00',
                    'CIF': '0.00',
                    'CIF RUPIAH': '0.00',
                    'DISKON': '0.00',
                    'FLAG KENDARAAN': '',
                    'FOB': '0.00',
                    'FREIGHT': '0.00',
                    'BARANG BARANG LDP': '0.00',
                    'HARGA INVOICE': '0.00',
                    'HARGA PENYERAHAN': cost_per_qty * move.product_uom_qty or '0.00',
                    'HARGA SATUAN': '0.00',
                    'JENIS KENDARAAN': '',
                    'JUMLAH BAHAN BAKU': jumlah_bahan_baku,
                    'JUMLAH KEMASAN': sum(kemasan.qty for kemasan in picking_id.kemasan_ids) or '0.00',
                    'JUMLAH SATUAN': move.quantity_done or '',
                    'KAPASITAS SILINDER': '0.00',
                    'KATEGORI BARANG': '',
                    'KODE ASAL BARANG': '',
                    'KODE BARANG': move.product_id.default_code.strip() or '',
                    'KODE FASILITAS': '',
                    'KODE GUNA': '',
                    'KODE JENIS NILAI': '',
                    'KODE KEMASAN': kode_kemasan,
                    'KODE LEBIH DARI 4 TAHUN': '',
                    'KODE NEGARA ASAL': '',
                    'KODE SATUAN': move.product_uom.name.strip() or '',
                    'KODE SKEMA TARIF': '',
                    'KODE STATUS': self.bc_id.kode_status or '',
                    'KONDISI BARANG': '',
                    'MERK': 'NITTO'+picking_id.name,
                    # 'NETTO': sum(line.netto for line in picking_id.move_ids_without_package) or '0.00',
                    'NETTO': move.netto or '0.00', # netto per barang
                    'NILAI INCOTERM': '0.0000',
                    'NILAI PABEAN': '0.00',
                    'NOMOR MESIN': '',
                    'POS TARIF': move.product_id.group_id.kode_tarif or '',
                    'SERI POS TARIF': '0',
                    'SPESIFIKASI LAIN': move.product_id.group_id.notes.strip() if move.product_id.group_id.notes else move.product_id.group_id.name.strip() or '',
                    'TAHUN PEMBUATAN': '',
                    'TIPE': move.product_id.type_id.name or '',
                    'UKURAN': '',
                    'URAIAN': move.product_id.name or '',
                    'VOLUME': move.quantity_done * move.product_id.volume or '0.000',
                    'SERI IJIN': '0',
                    'ID EKSPORTIR': '',
                    'NAMA EKSPORTIR': '',
                    'ALAMAT EKSPORTIR': '',
                    'KODE PERHITUNGAN': '',
                })
        return values

    def get_picking_aju_bahan_baku_out(self, aju_id, values):
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id or picking.picking_type_id.id == picking.subcontract_id.picking_type_id.picking_type_interco_id.id) and picking.state == 'done' and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            seri_barang = 1
            for move in picking_id.move_ids_without_package.filtered(lambda pr:pr.production_subcon_id) :
                move.seri_barang = seri_barang
                seri_barang += 1
                ratio = sum(move.move_line_ids.mapped('qty_done'))/move.production_subcon_id.product_qty
                seri_bahan_baku = 1
                for raw_line in move.production_subcon_id.move_raw_ids :
                    for move_line in raw_line.move_line_ids :
                        incoming_move_line_id = self.env['stock.move.line'].search([
                            ('product_id','=',move_line.product_id.id),
                            ('lot_id','=',move_line.lot_id.id),
                            ('move_id.production_id','=',False),
                            ('state','=','done'),
                            ('move_id.location_id.usage','=','supplier'),
                        ], limit=1)
                        if not incoming_move_line_id :
                            continue

                        # cari PO bahan baku
                        purchase_move_line_id = self.env['stock.move.line'].search([
                            ('product_id','=',move_line.product_id.id),
                            ('lot_id','=',move_line.lot_id.id),
                            ('move_id.purchase_line_id','!=',False),
                            ('state','=','done'),
                            ('move_id.location_id.usage','=','supplier'),
                        ], limit=1, order='date desc')
                        jml_bb = 0
                        if purchase_move_line_id:
                            rasio_bb = move_line.move_id.product_uom_qty/incoming_move_line_id.qty_done
                            jml_bb = purchase_move_line_id.move_id.product_uom_qty * rasio_bb 
                        kode_jenis_dok_asal = ''
                        kode_asal_bahan_baku = ''
                        nomor_aju_id = False
                        doc_transaction = False
                        if incoming_move_line_id.picking_id.nomor_aju_id:
                            nomor_aju_id = incoming_move_line_id.picking_id.nomor_aju_id
                            doc_transaction = incoming_move_line_id.picking_id
                            kode_jenis_dok_asal = nomor_aju_id.name[4:6]
                            if kode_jenis_dok_asal in ('23','27'):
                                kode_asal_bahan_baku = '0'
                            elif kode_jenis_dok_asal == '40':
                                kode_asal_bahan_baku = '1'
                        elif incoming_move_line_id.picking_id.kanban_id and incoming_move_line_id.picking_id.kanban_id.nomor_aju_id:
                            nomor_aju_id = incoming_move_line_id.picking_id.kanban_id.nomor_aju_id
                            doc_transaction = incoming_move_line_id.picking_id.kanban_id
                            kode_jenis_dok_asal = nomor_aju_id.name[4:6]
                            if kode_jenis_dok_asal in ('23','27'):
                                kode_asal_bahan_baku = '0'
                            elif kode_jenis_dok_asal == '40':
                                kode_asal_bahan_baku = '1'
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI BARANG': move.seri_barang or '',
                            #'SERI BAHAN BAKU': incoming_move_line_id.move_id.purchase_line_id.seri or '',
                            'SERI BAHAN BAKU':seri_bahan_baku,
                            'CIF': '0.00',
                            'CIF RUPIAH': '0.00',
                            #'HARGA PENYERAHAN': incoming_move_line_id.move_id.purchase_line_id.price_unit * incoming_move_line_id.move_id.product_uom_qty or '0.00',
                            'HARGA PENYERAHAN': incoming_move_line_id.move_id.purchase_line_id.price_unit,# * jml_bb,
                            'HARGA PEROLEHAN': '0.00',
                            'JENIS SATUAN': incoming_move_line_id.move_id.product_uom.name or '',
                            # 'JUMLAH SATUAN': incoming_move_line_id.move_id.quantity_done or '',
                            'JUMLAH SATUAN': jml_bb or '0.00',
                            'KODE ASAL BAHAN BAKU': kode_asal_bahan_baku,
                            'KODE BARANG': incoming_move_line_id.move_id.product_id.default_code or '',
                            'KODE FASILITAS': '',
                            'KODE JENIS DOK ASAL': kode_jenis_dok_asal,
                            'KODE KANTOR': self.bc_id.kode_kantor_pabean_asal or '',
                            'KODE SKEMA TARIF': '',
                            'KODE STATUS': self.bc_id.kode_status or '',
                            'MERK': '',
                            'NDPBM': '0.0000',
                            'NETTO': '0.0000',
                            'NOMOR AJU DOKUMEN ASAL': nomor_aju_id.name if nomor_aju_id else '',
                            'NOMOR DAFTAR DOKUMEN ASAL': doc_transaction.no_pabean if doc_transaction else '',
                            'POS TARIF': '',
                            'SERI BARANG DOKUMEN ASAL': incoming_move_line_id.picking_id.seri_dokumen or '1',
                            'SPESIFIKASI LAIN': incoming_move_line_id.move_id.product_id.group_id.notes.strip() if incoming_move_line_id.move_id.product_id.group_id.notes else incoming_move_line_id.move_id.product_id.group_id.name.strip() or '',
                            'TANGGAL DAFTAR DOKUMEN ASAL': doc_transaction.tgl_pabean.strftime("%Y-%m-%d") if doc_transaction and doc_transaction.tgl_pabean else '',
                            'TIPE': incoming_move_line_id.move_id.product_id.type_id.name or '',
                            'UKURAN': '',
                            'URAIAN': incoming_move_line_id.move_id.product_id.name or '',
                            'SERI IJIN': '',
                        })
                        seri_bahan_baku +=1
        return values

    def get_picking_aju_dokumen_out(self, aju_id, values):
        seri_dokumen = 1
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id or picking.picking_type_id.id == picking.subcontract_id.picking_type_id.picking_type_interco_id.id) and picking.state == 'done' and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            for dok in picking_id.dokumen_ids :
                values.append({
                            'NOMOR AJU': aju_id.name.strip() or '',
                            'SERI DOKUMEN': seri_dokumen,
                            'FLAG URL DOKUMEN': '',
                            'KODE JENIS DOKUMEN': dok.dokumen_master_id.code.strip() if dok.dokumen_master_id else '',
                            # 'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                            'NOMOR DOKUMEN': dok.nomor.strip() if dok.nomor else '',
                            'TANGGAL DOKUMEN': self.reformat_date(dok.tanggal.strftime('%Y-%m-%d')) if dok.tanggal else '',
                            'TIPE DOKUMEN': self.bc_id.tipe_dokumen or '',
                            'URL DOKUMEN': '',
                            })
                seri_dokumen += 1
        return values

    def get_picking_aju_kemasan_out(self, aju_id, values):
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id or picking.picking_type_id.id == picking.subcontract_id.picking_type_id.picking_type_interco_id.id) and picking.state == 'done' and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            for kemasan_id in picking_id.kemasan_ids.filtered(lambda x:x.qty > 0.0) :
                values.append({
                    'NOMOR AJU': aju_id.name.strip() or '',
                    'SERI KEMASAN': '',
                    'JUMLAH KEMASAN': kemasan_id.qty,
                    'KESESUAIAN DOKUMEN': '',
                    'KETERANGAN': '',
                    'KODE JENIS KEMASAN': kemasan_id.package_type_id.code.strip() if kemasan_id.package_type_id.code else '',
                    # 'MEREK KEMASAN': kemasan_id.picking_id.no_sj_pengirim.strip() if kemasan_id.picking_id.no_sj_pengirim else '',
                    'MEREK KEMASAN':'',
                    'NIP GATE IN': '',
                    'NIP GATE OUT': '',
                    'NOMOR POLISI': '',
                    'NOMOR SEGEL': '',
                    'WAKTU GATE IN': '',
                    'WAKTU GATE OUT': '',
                })
        return values


VitExportImportBcWizard()