from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning

class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"

    @api.multi
    def get_datas(self):
        res = super(VitExportImportBcWizard, self).get_datas()
        if self.bc_id.name == 'BC 4.1' :
            invoice = self.env['account.invoice']
            no_aju_ids = self.env['vit.nomor.aju']

            # search SO retur
            pick_retur = self.env['stock.picking'].sudo().search([('group_id','=',False),
                                                                    ('location_id.usage','=','internal'),
                                                                    ('location_dest_id.usage','in',('customer','supplier')),
                                                                    ('state','=','done'),
                                                                    ('partner_id.transaction_type','=','Local'),
                                                                    ('company_id','=',self.company_id.id),
                                                                    ('tgl_dokumen','=',self.date)])
            for pr in pick_retur :
                no_aju_ids |= pr.nomor_aju_id

            # Subcont Interco
            pickings = self.env['stock.picking'].search([('company_id','=',self.company_id.id),
                                                        ('state','=','done'),
                                                        ('tgl_dokumen','=',self.date),
                                                        ('subcontract_id','!=',False)])
            if pickings :
                for picking_id in pickings.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id or picking.picking_type_id.id == picking.subcontract_id.picking_type_id.picking_type_interco_id.id) and picking.subcontract_id.is_interco):
                    picking_id.write({'has_export':True})
                    no_aju_ids |= picking_id.nomor_aju_id

            sale_ids = self.env['sale.order'].search([
                ('company_id','=',self.company_id.id),
                ('state','in',['sale','done']),
                ('sale_type','=','Local'),
            ])
            for sale_id in sale_ids :
                # for picking_id in sale_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                for picking_id in sale_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer' and picking.tgl_dokumen == self.date):
                    # picking_id.write({'has_export':True})
                    if picking_id.bc_type :
                        if picking_id.bc_type == 'BC 4.1':
                            no_aju_ids |= picking_id.nomor_aju_id
                    else :
                        purchase_id = self.env['purchase.order']
                        if not purchase_id :
                            for move in picking_id.move_ids_without_package :
                                if not move.sale_line_id :
                                    continue
                                for line in move.move_line_ids :
                                    finished_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',line.product_id.id),
                                        ('lot_id','=',line.lot_id.id),
                                        ('move_id.production_id','!=',False),
                                        ('state','=','done'),
                                    ], limit=1)
                                    if not finished_move_line_id :
                                        continue
                                    for raw_line in finished_move_line_id.move_id.production_id.move_raw_ids :
                                        for move_line in raw_line.active_move_line_ids :
                                            incoming_move_line_id = self.env['stock.move.line'].search([
                                                ('product_id','=',move_line.product_id.id),
                                                ('lot_id','=',move_line.lot_id.id),
                                                ('move_id.production_id','=',False),
                                                ('state','=','done'),
                                                ('move_id.location_id.usage','=','supplier'),
                                            ], limit=1)
                                            if not incoming_move_line_id :
                                                continue
                                            else :
                                                purchase_id = incoming_move_line_id.move_id.purchase_line_id.order_id
                                                break
                                        if purchase_id :
                                            break
                                    if purchase_id :
                                        break
                                if purchase_id :
                                    break
                        if purchase_id and purchase_id.purchase_type == 'Local' :
                            no_aju_ids |= picking_id.nomor_aju_id

            no_aju_ids = no_aju_ids.filtered(lambda aju: not aju.has_export)
            headers = [
                'NOMOR AJU',
                'KPPBC',
                'PERUSAHAAN',
                'PEMASOK',
                'STATUS',
                'KODE DOKUMEN PABEAN',
                'NPPJK',
                'ALAMAT PEMASOK',
                'ALAMAT PEMILIK',
                'ALAMAT PENERIMA BARANG',
                'ALAMAT PENGIRIM',
                'ALAMAT PENGUSAHA',
                'ALAMAT PPJK',
                'API PEMILIK',
                'API PENERIMA',
                'API PENGUSAHA',
                'ASAL DATA',
                'ASURANSI',
                'BIAYA TAMBAHAN',
                'BRUTO',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG PEMILIK',
                'URL DOKUMEN PABEAN',
                'FOB',
                'FREIGHT',
                'HARGA BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA TOTAL',
                'ID MODUL',
                'ID PEMASOK',
                'ID PEMILIK',
                'ID PENERIMA BARANG',
                'ID PENGIRIM',
                'ID PENGUSAHA',
                'ID PPJK',
                'JABATAN TTD',
                'JUMLAH BARANG',
                'JUMLAH KEMASAN',
                'JUMLAH KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE ASAL BARANG',
                'KODE ASURANSI',
                'KODE BENDERA',
                'KODE CARA ANGKUT',
                'KODE CARA BAYAR',
                'KODE DAERAH ASAL',
                'KODE FASILITAS',
                'KODE FTZ',
                'KODE HARGA',
                'KODE ID PEMASOK',
                'KODE ID PEMILIK',
                'KODE ID PENERIMA BARANG',
                'KODE ID PENGIRIM',
                'KODE ID PENGUSAHA',
                'KODE ID PPJK',
                'KODE JENIS API',
                'KODE JENIS API PEMILIK',
                'KODE JENIS API PENERIMA',
                'KODE JENIS API PENGUSAHA',
                'KODE JENIS BARANG',
                'KODE JENIS BC25',
                'KODE JENIS NILAI',
                'KODE JENIS PEMASUKAN01',
                'KODE JENIS PEMASUKAN 02',
                'KODE JENIS TPB',
                'KODE KANTOR BONGKAR',
                'KODE KANTOR TUJUAN',
                'KODE LOKASI BAYAR',
                '',
                'KODE NEGARA PEMASOK',
                'KODE NEGARA PENGIRIM',
                'KODE NEGARA PEMILIK',
                'KODE NEGARA TUJUAN',
                'KODE PEL BONGKAR',
                'KODE PEL MUAT',
                'KODE PEL TRANSIT',
                'KODE PEMBAYAR',
                'KODE STATUS PENGUSAHA',
                'STATUS PERBAIKAN',
                'KODE TPS',
                'KODE TUJUAN PEMASUKAN',
                'KODE TUJUAN PENGIRIMAN',
                'KODE TUJUAN TPB',
                'KODE TUTUP PU',
                'KODE VALUTA',
                'KOTA TTD',
                'NAMA PEMILIK',
                'NAMA PENERIMA BARANG',
                'NAMA PENGANGKUT',
                'NAMA PENGIRIM',
                'NAMA PPJK',
                'NAMA TTD',
                'NDPBM',
                'NETTO',
                'NILAI INCOTERM',
                'NIPER PENERIMA',
                'NOMOR API',
                'NOMOR BC11',
                'NOMOR BILLING',
                'NOMOR DAFTAR',
                'NOMOR IJIN BPK PEMASOK',
                'NOMOR IJIN BPK PENGUSAHA',
                'NOMOR IJIN TPB',
                'NOMOR IJIN TPB PENERIMA',
                'NOMOR VOYV FLIGHT',
                'NPWP BILLING',
                'POS BC11',
                'SERI',
                'SUBPOS BC11',
                'SUB SUBPOS BC11',
                'TANGGAL BC11',
                'TANGGAL BERANGKAT',
                'TANGGAL BILLING',
                'TANGGAL DAFTAR',
                'TANGGAL IJIN BPK PEMASOK',
                'TANGGAL IJIN BPK PENGUSAHA',
                'TANGGAL IJIN TPB',
                'TANGGAL NPPPJK',
                'TANGGAL TIBA',
                'TANGGAL TTD',
                'TANGGAL JATUH TEMPO',
                'TOTAL BAYAR',
                'TOTAL BEBAS',
                'TOTAL DILUNASI',
                'TOTAL JAMIN',
                'TOTAL SUDAH DILUNASI',
                'TOTAL TANGGUH',
                'TOTAL TANGGUNG',
                'TOTAL TIDAK DIPUNGUT',
                'URL DOKUMEN PABEAN',
                'VERSI MODUL',
                'VOLUME',
                'WAKTU BONGKAR',
                'WAKTU STUFFING',
                'NOMOR POLISI',
                'CALL SIGN',
                'JUMLAH TANDA PENGAMAN',
                'KODE JENIS TANDA PENGAMAN',
                'KODE KANTOR MUAT',
                'KODE PEL TUJUAN',
                '',
                'TANGGAL STUFFING',
                'TANGGAL MUAT',
                'KODE GUDANG ASAL',
                'KODE GUDANG TUJUAN'
            ]

            values = []
            for aju_id in no_aju_ids :
                # search kanban:
                kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                for kanban_id in kanban_ids :
                #for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage in  ('customer','supplier')):
                    move_line_ids = kanban_id.detail_ids.mapped('move_line_ids')
                    sale_line_ids = kanban_id.detail_ids.mapped('sale_line_id')
                    valuta = kanban_id.invoice_id.currency_id.name
                    jml_type_kemasan = 0
                    packs = []
                    for kemasan in kanban_id.kemasan_ids:
                        if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                            packs.append(kemasan.package_type_id.id)
                            jml_type_kemasan+=1
                    if jml_type_kemasan == 0 :
                        jml_type_kemasan = 1
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'KPPBC': self.bc_id.kppbc or '',
                        'PERUSAHAAN': self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                        'PEMASOK': '',
                        'STATUS': '00', #self.bc_id.status or '',
                        'KODE DOKUMEN PABEAN': self.bc_id.kode_dokumen_pabean or '',
                        'NPPJK': '',
                        'ALAMAT PEMASOK': '',
                        'ALAMAT PEMILIK': '',
                        'ALAMAT PENERIMA BARANG': kanban_id.partner_id.street or '',
                        'ALAMAT PENGIRIM': '',
                        'ALAMAT PENGUSAHA': kanban_id.company_id.street or '',
                        'ALAMAT PPJK': '',
                        'API PEMILIK': '',
                        'API PENERIMA': '',
                        'API PENGUSAHA': '',
                        'ASAL DATA': self.bc_id.asal_data or '',
                        'ASURANSI': '0.00',
                        'BIAYA TAMBAHAN': '0.00',
                        'BRUTO': sum(move.brutto for move in kanban_id.detail_ids) or '',
                        'CIF': '0.00',
                        'CIF RUPIAH': '0.00',
                        'DISKON': '0.00',
                        'FLAG PEMILIK': '',
                        'URL DOKUMEN PABEAN': '',
                        'FOB': '0.00',
                        'FREIGHT': '0.00',
                        'HARGA BARANG LDP': '0.00',
                        'HARGA INVOICE': '0.00',
                        'HARGA PENYERAHAN': sum(move.sale_line_id.price_unit * move.product_uom_qty for move in kanban_id.detail_ids) or '0.00',
                        'HARGA TOTAL': '0.00',
                        'ID MODUL': self.bc_id.id_modul or '',
                        'ID PEMASOK': '',
                        'ID PEMILIK': '',
                        'ID PENERIMA BARANG': kanban_id.partner_id.vat or '',
                        'ID PENGIRIM':'',
                        'ID PENGUSAHA': self.company_id.vat or '',
                        'ID PPJK': '',
                        'JABATAN TTD': self.bc_id.jabatan_ttd or '',
                        # 'JUMLAH BARANG': sum(move.quantity_done for move in kanban_id.move_ids_without_package) or '',
                        'JUMLAH BARANG': len(kanban_id.detail_ids.mapped('product_id')),
                        #'JUMLAH KEMASAN': sum(kemasan.qty for kemasan in picking_id.kemasan_ids) or '0.00',
                        'JUMLAH KEMASAN': jml_type_kemasan,
                        'JUMLAH KEMASAN': '1',
                        'JUMLAH KONTAINER': len(kanban_id.kontainer_ids),
                        'KESESUAIAN DOKUMEN': '',
                        'KETERANGAN': '',
                        'KODE ASAL BARANG': '',
                        'KODE ASURANSI': '',
                        'KODE BENDERA': '',
                        'KODE CARA ANGKUT': '',
                        'KODE CARA BAYAR': '',
                        'KODE DAERAH ASAL': '',
                        'KODE FASILITAS': '',
                        'KODE FTZ': '',
                        'KODE HARGA': '',
                        'KODE ID PEMASOK': '',
                        'KODE ID PEMILIK': '',
                        'KODE ID PENERIMA BARANG': self.bc_id.kode_id_penerima or '',
                        'KODE ID PENGIRIM': '',
                        'KODE ID PENGUSAHA': self.bc_id.kode_id_pengusaha or '',
                        'KODE ID PPJK': '',
                        'KODE JENIS API': '',
                        'KODE JENIS API PEMILIK': '',
                        'KODE JENIS API PENERIMA': '',
                        'KODE JENIS API PENGUSAHA': '',
                        'KODE JENIS BARANG': '',
                        'KODE JENIS BC25': '',
                        'KODE JENIS NILAI': '',
                        'KODE JENIS PEMASUKAN01': '',
                        'KODE JENIS PEMASUKAN 02': '',
                        'KODE JENIS TPB': self.bc_id.kode_jenis_tpb or '',
                        'KODE KANTOR BONGKAR': '',
                        'KODE KANTOR TUJUAN': '',
                        'KODE LOKASI BAYAR': '',
                        '': '',
                        'KODE NEGARA PEMASOK': '',
                        'KODE NEGARA PENGIRIM': '',
                        'KODE NEGARA PEMILIK': '',
                        'KODE NEGARA TUJUAN': '',
                        'KODE PEL BONGKAR': '',
                        'KODE PEL MUAT': '',
                        'KODE PEL TRANSIT': '',
                        'KODE PEMBAYAR': '',
                        'KODE STATUS PENGUSAHA': '',
                        'STATUS PERBAIKAN': '',
                        'KODE TPS': '',
                        'KODE TUJUAN PEMASUKAN': '',
                        'KODE TUJUAN PENGIRIMAN': self.bc_id.kode_tujuan_pengiriman or '',
                        'KODE TUJUAN TPB': '',
                        'KODE TUTUP PU': '',
                        'KODE VALUTA': valuta or '',
                        'KOTA TTD': self.bc_id.kota_ttd or '',
                        'NAMA PEMILIK': '',
                        'NAMA PENERIMA BARANG': kanban_id.partner_id.name or '',
                        'NAMA PENGANGKUT': kanban_id.nama_pengangkut or '',
                        'NAMA PENGIRIM': '',
                        'NAMA PPJK': '',
                        'NAMA TTD': self.bc_id.nama_ttd or '',
                        'NDPBM': '0.0000',
                        'NETTO': sum(move.netto for move in kanban_id.detail_ids) or '',
                        'NILAI INCOTERM': '0.0000',
                        'NIPER PENERIMA': '',
                        'NOMOR API': '',
                        'NOMOR BC11': '',
                        'NOMOR BILLING': '',
                        'NOMOR DAFTAR': '',
                        'NOMOR IJIN BPK PEMASOK': '',
                        'NOMOR IJIN BPK PENGUSAHA': '',
                        'NOMOR IJIN TPB': self.bc_id.nomor_ijin_tpb or '',
                        'NOMOR IJIN TPB PENERIMA': '',
                        'NOMOR VOYV FLIGHT': '',
                        'NPWP BILLING': '',
                        'POS BC11': '',
                        'SERI': self.bc_id.seri or '',
                        'SUBPOS BC11': '',
                        'SUB SUBPOS BC11': '',
                        'TANGGAL BC11': '',
                        'TANGGAL BERANGKAT': '',
                        'TANGGAL BILLING': '',
                        'TANGGAL DAFTAR': '',
                        'TANGGAL IJIN BPK PEMASOK': '',
                        'TANGGAL IJIN BPK PENGUSAHA': '',
                        'TANGGAL IJIN TPB': '',
                        'TANGGAL NPPPJK': '',
                        'TANGGAL TIBA': '',
                        'TANGGAL TTD': self.reformat_date(kanban_id.tgl_ttd.strftime('%Y-%m-%d')) if kanban_id.tgl_ttd else '',
                        'TANGGAL JATUH TEMPO': '',
                        'TOTAL BAYAR': '0.00',
                        'TOTAL BEBAS': '0.00',
                        'TOTAL DILUNASI': '0.00',
                        'TOTAL JAMIN': '0.00',
                        'TOTAL SUDAH DILUNASI': '0.00',
                        'TOTAL TANGGUH': '0.00',
                        'TOTAL TANGGUNG': '0.00',
                        'TOTAL TIDAK DIPUNGUT': '0.00',
                        'URL DOKUMEN PABEAN': '',
                        'VERSI MODUL': self.bc_id.versi_modul or '',
                        'VOLUME': sum(move.quantity_done * move.product_id.volume for move in kanban_id.detail_ids) or '',
                        'WAKTU BONGKAR': '',
                        'WAKTU STUFFING': '',
                        'NOMOR POLISI': kanban_id.no_polisi or '',
                        'CALL SIGN': '',
                        'JUMLAH TANDA PENGAMAN': '0',
                        'KODE JENIS TANDA PENGAMAN': '',
                        'KODE KANTOR MUAT': '',
                        'KODE PEL TUJUAN': '',
                        '': '',
                        'TANGGAL STUFFING': '',
                        'TANGGAL MUAT': '',
                        'KODE GUDANG ASAL': '',
                        'KODE GUDANG TUJUAN': '',
                    })
                
                #interco
                if not kanban_ids :
                    values = self.get_picking_aju_header_out(aju_id, values)
            res.append(['Header'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'CIF',
                'CIF RUPIAH',
                'HARGA PENYERAHAN',
                'HARGA PEROLEHAN',
                'JENIS SATUAN',
                'JUMLAH SATUAN',
                'KODE ASAL BAHAN BAKU',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE JENIS DOK ASAL',
                'KODE KANTOR',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'MERK',
                'NDPBM',
                'NETTO',
                'NOMOR AJU DOKUMEN ASAL',
                'NOMOR DAFTAR DOKUMEN ASAL',
                'POS TARIF',
                'SERI BARANG DOKUMEN ASAL',
                'SPESIFIKASI LAIN',
                'TANGGAL DAFTAR DOKUMEN ASAL',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'SERI IJIN',
            ]

            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage in ('customer','supplier')):
                    seri_barang = 1
                    for move in picking_id.move_ids_without_package :
                        if not move.sale_line_id :
                            continue
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        for line in move.move_line_ids.mapped('lot_id') :
                            finished_move_line_id = self.env['stock.move.line'].search([
                                ('product_id','=',line.product_id.id),
                                ('lot_id','=',line.id),
                                ('move_id.production_id','!=',False),
                                ('state','=','done'),
                            ], limit=1)
                            if not finished_move_line_id :
                                continue
                            # ratio = line.qty_done/finished_move_line_id.production_id.product_qty
                            # for raw_line in finished_move_line_id.production_id.move_raw_ids :
                            # naik dulu ke move_id baru ke production_id
                            
                            ratio = sum(move.move_line_ids.mapped('qty_done'))/finished_move_line_id.move_id.production_id.product_qty
                            seri_bahan_baku = 1
                            raw_ids = finished_move_line_id.move_id.production_id.move_raw_ids
                            if finished_move_line_id.move_id.production_id.reprocess_mo_id:
                                raw_ids = finished_move_line_id.move_id.production_id.reprocess_mo_id.move_raw_ids
                            for raw_line in raw_ids :
                                for move_line in raw_line.active_move_line_ids :
                                    incoming_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',move_line.product_id.id),
                                        ('lot_id','=',move_line.lot_id.id),
                                        ('move_id.production_id','=',False),
                                        ('state','=','done'),
                                        ('move_id.location_id.usage','=','supplier'),
                                    ], limit=1)
                                    if not incoming_move_line_id :
                                        continue

                                    # cari move Sales
                                    # sale_move_line_id = self.env['stock.move.line'].search([
                                    #     ('product_id','=',move_line.product_id.id),
                                    #     ('lot_id','=',move_line.lot_id.id),
                                    #     ('move_id.sale_line_id','!=',False),
                                    #     ('state','=','done'),
                                    #     ('move_id.location_dest_id.usage','=','customer'),
                                    # ], limit=1, order='date desc')

                                    # cari PO bahan baku
                                    purchase_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',move_line.product_id.id),
                                        ('lot_id','=',move_line.lot_id.id),
                                        ('move_id.purchase_line_id','!=',False),
                                        ('state','=','done'),
                                        ('move_id.location_id.usage','=','supplier'),
                                    ], limit=1, order='date desc')
                                    jml_bb = 0
                                    if purchase_move_line_id:
                                        rasio_bb = move_line.move_id.product_uom_qty/finished_move_line_id.qty_done
                                        jml_bb = purchase_move_line_id.move_id.product_uom_qty * rasio_bb 
                                    kode_jenis_dok_asal = ''
                                    kode_asal_bahan_baku = ''
                                    if incoming_move_line_id.picking_id.nomor_aju_id.name:
                                        kode_jenis_dok_asal = incoming_move_line_id.picking_id.nomor_aju_id.name[4:6]
                                        if kode_jenis_dok_asal in ('23','27'):
                                            kode_asal_bahan_baku = '0'
                                        elif kode_jenis_dok_asal == '40':
                                            kode_asal_bahan_baku = '1'
                                    values.append({
                                        'NOMOR AJU': aju_id.name or '',
                                        'SERI BARANG': move.seri_barang or '',
                                        #'SERI BAHAN BAKU': incoming_move_line_id.move_id.purchase_line_id.seri or '',
                                        'SERI BAHAN BAKU':seri_bahan_baku,
                                        'CIF': '0.00',
                                        'CIF RUPIAH': '0.00',
                                        #'HARGA PENYERAHAN': incoming_move_line_id.move_id.purchase_line_id.price_unit * incoming_move_line_id.move_id.product_uom_qty or '0.00',
                                        'HARGA PENYERAHAN': incoming_move_line_id.move_id.purchase_line_id.price_unit,# * jml_bb,
                                        'HARGA PEROLEHAN': '',
                                        'JENIS SATUAN': incoming_move_line_id.move_id.product_uom.name or '',
                                        # 'JUMLAH SATUAN': incoming_move_line_id.move_id.quantity_done or '',
                                        'JUMLAH SATUAN': jml_bb or '0.00',
                                        'KODE ASAL BAHAN BAKU': kode_asal_bahan_baku,
                                        'KODE BARANG': incoming_move_line_id.move_id.product_id.default_code or '',
                                        'KODE FASILITAS': '',
                                        'KODE JENIS DOK ASAL': kode_jenis_dok_asal,
                                        'KODE KANTOR': self.bc_id.kode_kantor_pabean_asal or '',
                                        'KODE SKEMA TARIF': '',
                                        'KODE STATUS': self.bc_id.kode_status or '',
                                        'MERK': '',
                                        'NDPBM': '0.0000',
                                        'NETTO': '0.0000',
                                        'NOMOR AJU DOKUMEN ASAL': incoming_move_line_id.picking_id.nomor_aju_id.name or '',
                                        'NOMOR DAFTAR DOKUMEN ASAL': incoming_move_line_id.picking_id.no_pabean or '',
                                        'POS TARIF': '',
                                        'SERI BARANG DOKUMEN ASAL': incoming_move_line_id.picking_id.seri_dokumen or '',
                                        'SPESIFIKASI LAIN': incoming_move_line_id.move_id.product_id.group_id.notes.strip() if incoming_move_line_id.move_id.product_id.group_id.notes else incoming_move_line_id.move_id.product_id.group_id.name.strip() or '',
                                        'TANGGAL DAFTAR DOKUMEN ASAL': incoming_move_line_id.picking_id.tgl_pabean.strftime("%Y-%m-%d") if incoming_move_line_id.picking_id.tgl_pabean else '',
                                        'TIPE': incoming_move_line_id.move_id.product_id.type_id.name or '',
                                        'UKURAN': '',
                                        'URAIAN': incoming_move_line_id.move_id.product_id.name or '',
                                        'SERI IJIN': '',
                                    })
                                    seri_bahan_baku +=1
            
                #interco
                values = self.get_picking_aju_bahan_baku_out(aju_id, values)
            res.append(['BahanBaku'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE ASAL BAHAN BAKU',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'KODE SATUAN',
                'KODE TARIF',
                'NILAI BAYAR',
                'NILAI FASILITAS',
                'NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            res.append(['BahanBakuTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'SERI DOKUMEN',
                'KODE ASAL BAHAN BAKU'
            ]
            values = []
            res.append(['BahanBakuDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'ASURANSI',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG KENDARAAN',
                'FOB',
                'FREIGHT',
                'BARANG BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA SATUAN',
                'JENIS KENDARAAN',
                'JUMLAH BAHAN BAKU',
                'JUMLAH KEMASAN',
                'JUMLAH SATUAN',
                'KAPASITAS SILINDER',
                'KATEGORI BARANG',
                'KODE ASAL BARANG',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE GUNA',
                'KODE JENIS NILAI',
                'KODE KEMASAN',
                'KODE LEBIH DARI 4 TAHUN',
                'KODE NEGARA ASAL',
                'KODE SATUAN',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'KONDISI BARANG',
                'MERK',
                'NETTO',
                'NILAI INCOTERM',
                'NILAI PABEAN',
                'NOMOR MESIN',
                'POS TARIF',
                'SERI POS TARIF',
                'SPESIFIKASI LAIN',
                'TAHUN PEMBUATAN',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'VOLUME',
                'SERI IJIN',
                'ID EKSPORTIR',
                'NAMA EKSPORTIR',
                'ALAMAT EKSPORTIR',
                'KODE PERHITUNGAN'
            ]
            values = []
            for aju_id in no_aju_ids :
                kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                for kanban_id in kanban_ids :  
                    seri_barang = 1
                    for move in kanban_id.detail_ids.sorted('kanban_sequence') :
                        if not move.sale_line_id :
                            continue
                        jumlah_bahan_baku = 0

                        for line in move.move_line_ids :
                            finished_move_line_id = self.env['stock.move.line'].search([
                                ('product_id','=',line.product_id.id),
                                ('lot_id','=',line.lot_id.id),
                                ('move_id.production_id','!=',False),
                                ('state','=','done'),
                            ], limit=1)
                            if not finished_move_line_id :
                                continue
                            jumlah_bahan_baku += len(finished_move_line_id.move_id.production_id.move_raw_ids)

                        move.seri_barang = seri_barang
                        seri_barang += 1
                        kode_kemasan = ''
                        if kanban_id.kemasan_ids:
                            kode_kemasan = kanban_id.kemasan_ids[0].package_type_id.name if kanban_id.kemasan_ids[0].package_type_id else ''
                        kemasan_ids = kanban_id.kemasan_ids.filtered(lambda kemasan: kemasan.product_id.id == move.product_id.id)
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI BARANG': move.seri_barang or '',
                            'ASURANSI': '',
                            'CIF': '',
                            'CIF RUPIAH': '',
                            'DISKON': '',
                            'FLAG KENDARAAN': '',
                            'FOB': '',
                            'FREIGHT': '',
                            'BARANG BARANG LDP': '',
                            'HARGA INVOICE': '',
                            'HARGA PENYERAHAN': move.sale_line_id.price_unit * move.product_uom_qty or '0.00',
                            'HARGA SATUAN': '',
                            'JENIS KENDARAAN': '',
                            'JUMLAH BAHAN BAKU': jumlah_bahan_baku,
                            'JUMLAH KEMASAN': '0.00',#sum(kemasan.qty for kemasan in kemasan_ids) or '0.00',
                            'JUMLAH SATUAN': move.quantity_done or '',
                            'KAPASITAS SILINDER': '',
                            'KATEGORI BARANG': '',
                            'KODE ASAL BARANG': '',
                            'KODE BARANG': move.product_id.default_code or '',
                            'KODE FASILITAS': '',
                            'KODE GUNA': '',
                            'KODE JENIS NILAI': '',
                            'KODE KEMASAN': kode_kemasan or '', #todo
                            'KODE LEBIH DARI 4 TAHUN': '',
                            'KODE NEGARA ASAL': '',
                            'KODE SATUAN': move.product_uom.name or '',
                            'KODE SKEMA TARIF': '',
                            'KODE STATUS': self.bc_id.kode_status or '',
                            'KONDISI BARANG': '',
                            'MERK': 'NITTO'+kanban_id.name,#move.product_id.default_code or '',
                            'NETTO': sum(line.netto for line in kanban_id.detail_ids) or '',
                            'NILAI INCOTERM': '0.0000',
                            'NILAI PABEAN': '0.00',
                            'NOMOR MESIN': '',
                            'POS TARIF': move.product_id.group_id.kode_tarif or '',
                            'SERI POS TARIF': '0',
                            'SPESIFIKASI LAIN': move.product_id.group_id.notes.strip() if move.product_id.group_id.notes else move.product_id.group_id.name.strip() or '',
                            'TAHUN PEMBUATAN': '',
                            'TIPE': move.product_id.type_id.name or '',
                            'UKURAN': '',
                            'URAIAN': move.product_id.name or '',
                            'VOLUME': move.quantity_done * move.product_id.volume or '',
                            'SERI IJIN': '',
                            'ID EKSPORTIR': '',
                            'NAMA EKSPORTIR': '',
                            'ALAMAT EKSPORTIR': '',
                            'KODE PERHITUNGAN': '',
                        })

                 #interco
                if not kanban_ids :
                    values = self.get_picking_aju_barang_out(aju_id, values)
            res.append(['Barang'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'TARIF KODE SATUAN',
                'TARIF KODE TARIF',
                'TARIF NILAI BAYAR',
                'TARIF NILAI FASILITAS',
                'TARIF NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            res.append(['BarangTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI DOKUMEN'
            ]
            values = []
            res.append(['BarangDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI DOKUMEN',
                'FLAG URL DOKUMEN',
                'KODE JENIS DOKUMEN',
                'NOMOR DOKUMEN',
                'TANGGAL DOKUMEN',
                'TIPE DOKUMEN',
                'URL DOKUMEN'
            ]
            values = []
            for aju_id in no_aju_ids :
                move_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage in ('customer','supplier')).mapped('move_ids_without_package')
                sale_line_ids = move_ids.mapped('sale_line_id')
                valuta = ''
                if not sale_line_ids :
                    sale_line_ids = self.env['sale.order.line']
                    if picking_id.kanban_id :
                        invoice_ids = invoice.sudo().search([('origin','=',picking_id.kanban_id.name)])
                        if invoice_ids :
                            valuta = invoice_ids[0].currency_id.name
                    else :
                        invoice_ids = invoice
                    #continue
                if sale_line_ids :
                    sale_ids = sale_line_ids.mapped('order_id')
                    if sale_ids:
                        valuta = sale_ids[0].currency_id.name
                    invoice_ids = sale_ids.mapped('invoice_ids')
                seri_dokumen = 1
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage in ('customer','supplier')):
                    if self.bc_id.kode_jenis_dokumen_sj == '217' :
                        continue
                    picking_id.seri_dokumen = seri_dokumen
                    seri_dokumen += 1
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'SERI DOKUMEN': picking_id.seri_dokumen or '',
                        'FLAG URL DOKUMEN': '',
                        'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_sj or '',
                        'NOMOR DOKUMEN': picking_id.name or '',
                        'TANGGAL DOKUMEN': self.reformat_date(picking_id.tgl_dokumen.strftime('%Y-%m-%d')) if picking_id.tgl_dokumen else '',
                        'TIPE DOKUMEN': self.bc_id.tipe_dokumen2 or '',
                        'URL DOKUMEN': '',
                    })
                for invoice_id in invoice_ids :
                    if self.bc_id.kode_jenis_dokumen_inv == '217' :
                        continue
                    invoice_id.seri_dokumen = seri_dokumen
                    seri_dokumen += 1
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'SERI DOKUMEN': invoice_id.seri_dokumen or '',
                        'FLAG URL DOKUMEN': '',
                        'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_inv or '',
                        'NOMOR DOKUMEN': invoice_id.number or '',
                        'TANGGAL DOKUMEN': self.reformat_date(invoice_id.date_invoice.strftime('%Y-%m-%d')) if invoice_id.date_invoice else '',
                        'TIPE DOKUMEN': self.bc_id.tipe_dokumen2 or '',
                        'URL DOKUMEN': '',
                    })
                # tambah packing list
                move_line_ids = move_ids.mapped('move_line_ids')
                for kemasan in aju_id.picking_ids.mapped('kemasan_ids') :
                    if self.bc_id.kode_jenis_dokumen_package == '217' :
                        continue
                    kemasan.seri = seri_dokumen
                    seri_dokumen += 1
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'SERI DOKUMEN': kemasan.seri,
                        'FLAG URL DOKUMEN': '',
                        'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_package or '',
                        'NOMOR DOKUMEN': kemasan.package_type_id.name or '',
                        'TANGGAL DOKUMEN': self.reformat_date(kemasan.create_date.strftime('%Y-%m-%d')) if kemasan.create_date else '',
                        'TIPE DOKUMEN': self.bc_id.tipe_dokumen2 or '',
                        'URL DOKUMEN': '',
                    })
                incoming_picking_ids = self.env['stock.picking']
                for line in move_line_ids :
                    finished_move_line_id = self.env['stock.move.line'].search([
                        ('product_id','=',line.product_id.id),
                        ('lot_id','=',line.lot_id.id),
                        ('move_id.production_id','!=',False),
                        ('state','=','done'),
                    ], limit=1)
                    if not finished_move_line_id :
                        continue
                    for raw_line in finished_move_line_id.move_id.production_id.move_raw_ids :
                        for move_line in raw_line.active_move_line_ids :
                            incoming_move_line_id = self.env['stock.move.line'].search([
                                ('product_id','=',move_line.product_id.id),
                                ('lot_id','=',move_line.lot_id.id),
                                ('move_id.production_id','=',False),
                                ('state','=','done'),
                                ('move_id.location_id.usage','=','supplier'),
                            ], limit=1)
                            if not incoming_move_line_id :
                                continue
                            incoming_picking_ids |= incoming_move_line_id.picking_id
                for incoming_picking_id in incoming_picking_ids :
                    if self.bc_id.kode_jenis_dokumen_bahan_baku == '217' :
                        continue
                    incoming_picking_id.seri_dokumen = seri_dokumen
                    seri_dokumen += 1
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'SERI DOKUMEN': incoming_picking_id.seri_dokumen or '',
                        'FLAG URL DOKUMEN': '',
                        'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_bahan_baku or '',
                        'NOMOR DOKUMEN': incoming_picking_id.kanban_id.name or '',
                        'TANGGAL DOKUMEN': self.reformat_date(incoming_picking_id.tgl_dokumen.strftime('%Y-%m-%d')) if incoming_picking_id.tgl_dokumen else '',
                        'TIPE DOKUMEN': self.bc_id.tipe_dokumen2 or '',
                        'URL DOKUMEN': '',
                    })

                #interco
                values = self.get_picking_aju_dokumen_out(aju_id, values)

            res.append(['Dokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KEMASAN',
                'JUMLAH KEMASAN',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE JENIS KEMASAN',
                'MEREK KEMASAN',
                'NIP GATE IN',
                'NIP GATE OUT',
                'NOMOR POLISI',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT',
            ]
            values = []
            for aju_id in no_aju_ids :
                kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                for kanban_id in kanban_ids :  
                #kemasan_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage in ('customer','supplier')).mapped('kemasan_ids')
                    for kemasan_id in kanban_id.kemasan_ids.filtered(lambda kmsn:kmsn.package_type_id and kmsn.qty > 0.0) :
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI KEMASAN': '',
                            'JUMLAH KEMASAN': kemasan_id.qty or '0.00',
                            'KESESUAIAN DOKUMEN': '',
                            'KETERANGAN': '',
                            'KODE JENIS KEMASAN': kemasan_id.package_type_id.code.strip() if kemasan_id.package_type_id.code else kemasan_id.package_type_id.name,
                            'MEREK KEMASAN': 'NITTO',#kemasan_id.picking_id.no_sj_pengirim.strip() if kemasan_id.picking_id.no_sj_pengirim else '',
                            'NIP GATE IN': '',
                            'NIP GATE OUT': '',
                            'NOMOR POLISI': '',
                            'NOMOR SEGEL': '',
                            'WAKTU GATE IN': '',
                            'WAKTU GATE OUT': '',
                        })
                if not kanban_ids:
                    #interco
                    values = self.get_picking_aju_kemasan_out(aju_id, values)
                    
            res.append(['Kemasan'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE STUFFING',
                'KODE TIPE KONTAINER',
                'KODE UKURAN KONTAINER',
                'FLAG GATE IN',
                'FLAG GATE OUT',
                'NOMOR POLISI',
                'NOMOR KONTAINER',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT'
            ]
            values = []
            res.append(['Kontainer'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON',
                'TANGGAL RESPON',
                'WAKTU RESPON',
                'BYTE STRAM PDF'
            ]
            values = []
            res.append(['Respon'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON'
            ]
            values = []
            res.append(['Status'] + [headers, values])

            no_aju_ids.write({'has_export':True})
            no_aju_ids.mapped('picking_ids').write({'has_export':True})
            
        return res
