#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _
import xlrd

import base64
from odoo.exceptions import Warning
import datetime

import logging
_logger = logging.getLogger(__name__)

class import_mo(models.Model):

    _name = "vit.import_mo"
    name = fields.Char( required=True, string="Name",  help="")
    import_mo_file = fields.Binary( string="Import MO file",  help="")
    import_mo_file_filename = fields.Char( string="Import MO file filename",  help="")
    import_wo_file = fields.Binary( string="Import WO file",  help="")
    import_wo_file_filename = fields.Char( string="Import WO file filename",  help="")

    @api.model_cr
    def init(self):
        _logger.info("creating functions...")
        self.env.cr.execute("""
        DROP FUNCTION IF EXISTS vit_create_mo(TEXT, INT);
        CREATE OR REPLACE FUNCTION vit_create_mo(data TEXT, v_user_id INT)
        RETURNS VOID AS $BODY$
        DECLARE
            records TEXT[];
            rec TEXT;
            mo_rec TEXT[];
            v_mo_exist TEXT;
            v_external_id TEXT;
            v_name TEXT;
            v_company TEXT;
            v_date_planned DATE;
            v_product TEXT;
            v_product_uom TEXT;
            v_availability TEXT;
            v_product_qty NUMERIC;
            v_routing TEXT;
            v_origin TEXT;
            v_state TEXT;
            v_customer TEXT;
            v_code_product TEXT;
            v_customer_rec RECORD;
            v_code_product_rec RECORD;
            v_bom TEXT;
            v_picking_type TEXT;
            v_location_src TEXT;
            v_location_dest TEXT;   
            v_proc_group_id INT;         
            v_product_rec RECORD;
            v_mo_id INTEGER;
            v_move_line_id INTEGER;
            v_move_id INTEGER;
            v_loc_rec RECORD;
            v_loc_dest_rec RECORD;
            v_loc_production_rec RECORD;
            v_company_rec RECORD;
            v_uom_rec RECORD;
            v_line_rec RECORD;
            v_raw_stock_move_id INT;
            v_raw_move_line_id INT;
            v_picking_id INT;
            v_lot_id INT;
            v_picking_type_rec RECORD;
            v_sequence_rec RECORD;
            v_rp_name TEXT;
            v_production_amount NUMERIC;
            v_no_poly_box TEXT;
            
        BEGIN
            SELECT string_to_array(data, '|') INTO records;
            FOREACH rec IN ARRAY records LOOP
                SELECT string_to_array(rec, '~~') INTO mo_rec;
                v_external_id   = mo_rec[1];
                v_name          = mo_rec[2];
                v_company       = mo_rec[3];
                v_date_planned  = mo_rec[4];
                v_availability  = mo_rec[5];
                v_product       = mo_rec[6];
                v_product_uom   = mo_rec[7];
                v_product_qty   = mo_rec[8];
                v_routing       = mo_rec[9];
                v_origin        = mo_rec[10];
                v_state         = mo_rec[11];
                v_bom           = mo_rec[12];
                v_location_src  = mo_rec[13];
                v_location_dest = mo_rec[14];             
                v_customer      = mo_rec[15];            
                v_code_product  = mo_rec[16];  
                v_no_poly_box   = mo_rec[17];         

                SELECT id from mrp_production where name = v_name INTO v_mo_exist;
    
                IF v_mo_exist IS NULL THEN
                
                    -- find product Finish Goods
                    select pp.id, pt.name, pt.uom_id from product_product pp join product_template pt on pt.id=pp.product_tmpl_id 
                    where pt.name = v_product into v_product_rec;
                    
                    -- find locations
                    select id from stock_location where complete_name = v_location_src INTO v_loc_rec;
                    select id from stock_location where complete_name = v_location_dest INTO v_loc_dest_rec;        
                    select id from stock_location where complete_name = 'Virtual Locations/Production' INTO v_loc_production_rec;        
                    
                    -- find company
                    select id from res_company where name = v_company INTO v_company_rec;           
                                        
                    -- find UOM
                    select id from uom_uom where name = v_product_uom into v_uom_rec;   
                    
                    -- find customer 
                    select id from res_partner where name = v_customer or ref = v_customer into v_customer_rec;
                    
                    -- find customer_code
                    select id from product_customer_code where product_code = v_code_product into v_code_product_rec;
                            
                    
                    -- insert the proc grup
                    insert into procurement_group (name, move_type, create_uid, create_date, write_uid, write_date)
                            values (v_name, 'direct', v_user_id, NOW(), v_user_id, NOW()) 
                            returning id INTO v_proc_group_id;
    
                    -- insert stock.production.lot
                    INSERT INTO stock_production_lot(
                        name,
                        product_id
                    )
                    VALUES (
                        v_name,
                        v_product_rec.id
                    ) RETURNING id INTO v_lot_id;
                    
                    
                    
                    -- find production amount
                    SELECT sum(v_product_qty/bom.product_qty * line.product_qty * prop.value_float)
                        from mrp_bom_line line
                        left join mrp_bom bom on line.bom_id = bom.id 
                        left join product_template temp on bom.product_tmpl_id = temp.id     
                        left join product_product pp on pp.product_tmpl_id = temp.id
                        left join product_product pp_raw on line.product_id = pp_raw.id                        
                        left join ir_property prop on prop.res_id = 'product.product,' || pp_raw.id
                        where temp.name = v_product and prop.company_id = v_company_rec.id
                        INTO v_production_amount;
                        
                    RAISE NOTICE 'v_production_amount=  %', v_production_amount;
                                     
                    -- insert mrp_production
                    INSERT INTO mrp_production (
                        name,
                        company_id,
                        date_planned_start,
                        date_start,
                        product_id,
                        product_uom_id,
                        product_qty,
                        routing_id,
                        origin,
                        state,
                        bom_id,
                        picking_type_id,
                        location_src_id,
                        location_dest_id,
                        availability,
                        create_date,
                        write_date,
                        create_uid,
                        write_uid,
                        user_id,
                        procurement_group_id,
                        customer_id,
                        code_cust_id,
                        no_poly_box
                    ) VALUES (
                        v_name,
                        v_company_rec.id,
                        v_date_planned,
                        v_date_planned,
                        v_product_rec.id,
                        v_uom_rec.id,
                        v_product_qty,
                        (select id from mrp_routing where name = v_routing),
                        v_origin,
                        v_state,
                        (select bom.id from mrp_bom bom left join product_template tmpl on bom.product_tmpl_id = tmpl.id where tmpl.name = v_product),
                        (select pt.id from stock_picking_type pt 
                            join stock_warehouse wh on pt.warehouse_id=wh.id
                            join res_company com on wh.company_id = com.id and com.name = v_company
                            and pt.code = 'mrp_operation' limit 1
                        ),
                        v_loc_rec.id,
                        v_loc_dest_rec.id,
                        'assigned',
                        now(),
                        now(),
                        v_user_id,
                        v_user_id,
                        v_user_id,
                        v_proc_group_id,
                        v_customer_rec.id,
                        v_code_product_rec.id,
                        v_no_poly_box           
                    ) RETURNING id INTO v_mo_id;
                    
                    -- get RP stock_picking_type
                    SELECT pt.id, pt.sequence_id, 'ir_sequence_'||LPAD(pt.sequence_id::TEXT,3,'0') as seq_id from stock_picking_type pt
                        LEFT JOIN stock_warehouse wh ON pt.warehouse_id = wh.id 
                        WHERE pt.name = 'Receipt from Production' 
                        AND wh.company_id = v_company_rec.id INTO v_picking_type_rec;
                    
                    -- get RP picking sequence
                    SELECT prefix FROM ir_sequence where id = v_picking_type_rec.sequence_id INTO v_sequence_rec;
                    SELECT v_sequence_rec.prefix || LPAD(nextval(v_picking_type_rec.seq_id)::TEXT,5,'0') INTO v_rp_name;
                    
                    -- insert Picking RP
                    INSERT INTO stock_picking (
                        name,
                        origin,
                        move_type,
                        state,
                        group_id,
                        scheduled_date,
                        date,
                        location_id,
                        location_dest_id,
                        picking_type_id,
                        company_id
                    )
                    VALUES(
                        v_rp_name,
                        v_name,
                        'direct',
                        'draft',
                        v_proc_group_id,
                        now(),
                        now(),
                        v_loc_production_rec.id,
                        v_loc_dest_rec.id,
                        v_picking_type_rec.id,
                        v_company_rec.id                        
                    ) RETURNING id INTO v_picking_id;
                    
                    -- insert stock move FG
                    INSERT INTO stock_move (
                        picking_id,
                        production_id,
                        name,
                        date,
                        company_id,
                        product_id,
                        product_qty,
                        product_uom_qty,
                        product_uom,
                        location_id,
                        location_dest_id,
                        state,
                        origin,
                        group_id,
                        reference,
                        date_expected,
                        procure_method,
                        create_uid,
                        write_uid,
                        create_date,
                        write_date
                    ) VALUES (
                        v_picking_id,
                        v_mo_id,
                        v_name,
                        v_date_planned,
                        v_company_rec.id,
                        v_product_rec.id,
                        v_product_qty,
                        v_product_qty,
                        v_uom_rec.id,
                        v_loc_production_rec.id,
                        v_loc_dest_rec.id,
                        'draft',
                        v_name,
                        v_proc_group_id,
                        v_name,
                        v_date_planned,
                        'make_to_stock',
                        v_user_id,                     
                        v_user_id,
                        NOW(),
                        NOW() 
                    ) RETURNING id INTO v_move_id;
                    
                    -- insert stock move lines FG
                    
                    -- INSERT INTO stock_move_line (
                    --     move_id,
                    --     date,
                    --     product_id,
                    --     qty_done,
                    --     product_uom_qty,
                    --     product_uom_id,
                    --     reference,
                    --     production_id,
                    --     location_id,
                    --     location_dest_id,
                    --     state
                    -- ) VALUES (
                    --     v_move_id,
                    --     NOW(),
                    --     v_product_rec.id,
                    --     0,
                    --     v_product_qty,
                    --     v_product_rec.uom_id,
                    --     v_name,
                    --     v_mo_id,
                    --     v_loc_rec.id,
                    --     v_loc_dest_rec.id,
                    --     'draft'                  
                    -- ) RETURNING id INTO v_move_line_id;
                    
                    
                    -- find BOM lines  and insert raw material from BOM
                    
                    FOR v_line_rec IN SELECT 
                        line.product_id, (v_product_qty/bom.product_qty * line.product_qty) as product_qty,
                        line.product_uom_id, prop.value_float
                        from mrp_bom_line line
                        left join mrp_bom bom on line.bom_id = bom.id 
                        left join product_template temp on bom.product_tmpl_id = temp.id     
                        left join product_product pp on pp.product_tmpl_id = temp.id      
                    	left join product_product pp_raw on line.product_id = pp_raw.id
                        left join ir_property prop on prop.res_id = 'product.product,' || pp_raw.id
                        where temp.name = v_product and prop.company_id = v_company_rec.id
                    LOOP
                        INSERT INTO stock_move (
                            raw_material_production_id,
                            name,
                            date,
                            company_id,
                            product_id,
                            product_qty,
                            product_uom_qty,
                            product_uom,
                            location_id,
                            location_dest_id,
                            state,
                            origin,
                            group_id,
                            reference,
                            date_expected,
                            procure_method,
                            create_uid,
                            write_uid,
                            create_date,
                            write_date,
                            price_unit
                        ) VALUES (
                            v_mo_id,
                            v_name,
                            v_date_planned,
                            v_company_rec.id,
                            v_line_rec.product_id,
                            v_line_rec.product_qty,
                            v_line_rec.product_qty,
                            v_line_rec.product_uom_id,
                            v_loc_rec.id,
                            v_loc_production_rec.id,
                            'done',
                            v_name,
                            v_proc_group_id,
                            v_name,
                            v_date_planned,
                            'make_to_stock',
                            v_user_id,                     
                            v_user_id,
                            NOW(),
                            NOW(),
                            v_line_rec.value_float 
                        ) RETURNING id INTO v_raw_stock_move_id;
                        RAISE NOTICE 'create raw_stock_move % %', v_raw_stock_move_id, v_line_rec.value_float;
                        
                        -- insert stock move lines raw material
                        
                        INSERT INTO stock_move_line (
                            move_id,
                            date,
                            product_id,
                            qty_done,
                            product_qty,
                            product_uom_qty,
                            product_uom_id,
                            reference,
                            production_id,
                            location_id,
                            location_dest_id,
                            state,
                            done_wo
                        ) VALUES (
                            v_raw_stock_move_id,
                            NOW(),
                            v_line_rec.product_id,
                            v_line_rec.product_qty,
                            0.0,
                            0.0,
                            v_line_rec.product_uom_id,
                            v_name,
                            v_mo_id,
                            v_loc_rec.id,
                            v_loc_production_rec.id,
                            'done',
                            't'         
                        ) RETURNING id INTO v_raw_move_line_id;
                        
                        RAISE NOTICE 'create v_raw_move_line_id %', v_raw_move_line_id;                        
                    END LOOP;
                    
                    -- RAISE NOTICE 'create stock_move %', v_move_id;

                END IF;
                RAISE NOTICE 'create MO %', v_name;
            END LOOP;
        END;
    
        $BODY$
        LANGUAGE plpgsql;
       
       
        DROP FUNCTION IF EXISTS vit_create_wo(TEXT, INT);
        CREATE OR REPLACE FUNCTION vit_create_wo(data TEXT, v_user_id INT)
        RETURNS VOID AS $BODY$
        DECLARE
            records TEXT[];
            rec TEXT;
            wo_rec TEXT[];
            v_wo_exist      TEXT;
            v_external_id   TEXT;
            v_mo            TEXT;	
            v_name          TEXT;
            v_workcenter    TEXT;	
            v_state         TEXT;	
            v_current_qty   NUMERIC;	
            v_produced_qty  NUMERIC;
            v_qty_produced_real NUMERIC;
            v_mo_rec    RECORD;
            v_wo_rec RECORD;
            v_last_wo_id    INTEGER;
            v_next_id INTEGER;

        BEGIN
            SELECT string_to_array(data, '|') INTO records;
            FOREACH rec IN ARRAY records LOOP
                SELECT string_to_array(rec, '~~') INTO wo_rec;
                
                v_external_id       = wo_rec[1]; 
                v_mo                = wo_rec[2]; 
                v_name              = wo_rec[3];
                v_workcenter        = wo_rec[4];
                v_state             = wo_rec[5];
                v_current_qty       = wo_rec[6];
                v_produced_qty      = wo_rec[7];
                v_qty_produced_real = wo_rec[7];
                             
                --SELECT id from mrp_workorder where name = v_name INTO v_wo_exist;
                v_wo_exist = NULL;
    
                IF v_wo_exist IS NULL THEN
                    -- find MO
                    SELECT * from mrp_production where name = v_mo INTO v_mo_rec;
                    RAISE NOTICE 'selected MO %', v_mo_rec;
                    
                    -- insert mrp_workorder
                    INSERT INTO mrp_workorder (
                            name,
                            workcenter_id,
                            production_id,
                            product_id,
                            production_availability, 
                            qty_produced, 
                            qty_producing, 
                            qty_produced_real,
                            state, 
                            date_planned_start, 
                            date_start, 
                            --duration_expected, 
                            --duration, 
                            --duration_unit, 
                            --duration_percent, 
                            operation_id, 
                            --final_lot_id, 
                            --next_work_order_id, 
                            production_date, 
                            --capacity, 
                            create_uid, 
                            create_date, 
                            write_uid, 
                            write_date
                        ) VALUES (
                            v_name,
                            (select id from mrp_workcenter where name = v_workcenter limit 1),
                            v_mo_rec.id,
                            v_mo_rec.product_id,
                            v_mo_rec.availability, 
                            v_produced_qty, 
                            v_current_qty, 
                            v_qty_produced_real,
                            v_state, 
                            v_mo_rec.date_planned_start, 
                            v_mo_rec.date_start, 
                            --duration_expected, 
                            --duration, 
                            --duration_unit, 
                            (select id from mrp_routing_workcenter where name = v_name limit 1), 
                            --final_lot_id, 
                            --next_work_order_id, 
                            v_mo_rec.date_start, 
                            --capacity, 
                            v_user_id, 
                            NOW(), 
                            v_user_id, 
                            NOW()
                    ) RETURNING id INTO v_last_wo_id;
                END IF;
                RAISE NOTICE 'create WO %', v_name;

                -- update next_work_order_id
                FOR v_wo_rec IN SELECT * from mrp_workorder WHERE production_id=v_mo_rec.id ORDER BY id LOOP
                    SELECT id from mrp_workorder where production_id=v_mo_rec.id and id > v_wo_rec.id ORDER BY id INTO v_next_id; 
                    UPDATE mrp_workorder SET next_work_order_id = v_next_id WHERE id = v_wo_rec.id;
                END LOOP;
                
            END LOOP;
            
            

            
        END;
    
        $BODY$
        LANGUAGE plpgsql;       
        """)

    @api.multi
    def action_process(self):
        self.process_file('vit_create_mo', self.import_mo_file)
        self.process_file('vit_create_wo', self.import_wo_file)



    def process_file(self, fname, fieldname):
        try:
            wb = xlrd.open_workbook(file_contents=base64.decodebytes(fieldname))
            for s in wb.sheets():
                final_data = []
                for row in range(1, s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = s.cell(row, col).value
                        if s.cell(row, col).ctype == xlrd.XL_CELL_DATE:
                            value = datetime.datetime(*xlrd.xldate_as_tuple(value, wb.datemode))
                        col_value.append(str(value).strip())

                    external_id = col_value[0]
                    if external_id != "":
                        mo_data = "~~".join(col_value)
                        final_data.append(mo_data)

                final_data = "|".join(final_data)
                sql = "select "+fname+"(%s, %s)"
                _logger.info(final_data)
                self.env.cr.execute(sql, (final_data, self.env.user.id))

        except Exception as e:
            raise Warning(e)
