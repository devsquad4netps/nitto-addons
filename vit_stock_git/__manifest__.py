# -*- coding: utf-8 -*-
{
    'name': "Stock Goods in Transit",

    'summary': """
        Advanced route untuk import 
        """,

    'description': """
        Vendor -> Transit -> Warehouse.
        
        If destination location usage is "git":
        Jurnal terbentuk waktu Vendor -> Transit:
        
        Db Goods in Transit   (dest)
        Cr      Stock Interim (source)
        
        If source location usage is "git"
        Jurnal Transit -> Warehouse:
        
        Db Stock                  (dest)
        Cr      Goods in transit  (source)
        
        Setting: 
        create advanced warehouse push rule "Import"
        Route Vendor -> Harbour/Stock (type goods in transit) 
    """,

    'author': "Akhmad D. Sembiring [vitraining.com]",
    'website': "http://vitraining.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Inventory',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock_account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/stock_location.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}