# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp

class MrpWorkorderRepair(models.TransientModel):
    _name = 'mrp.workorder.repair'
    _description = 'Split Workorder'

    @api.model
    def _get_default_workorder_id(self):
        return self._context.get('active_id')

    workorder_id = fields.Many2one('mrp.workorder', 'Work Order', required=False, default=_get_default_workorder_id)
    workorder_date = fields.Date('Production Date')
    shift           = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift", copy=False)
    iduser          = fields.Char(string='Operator')
    barcodewo       = fields.Char(string='ID Mesin')
    machine_id      = fields.Many2one(comodel_name="mrp.machine",string="Machine")
    user_id         = fields.Many2one(comodel_name="res.users", required=False, string="Operator")
    retur           = fields.Char('Retur')
    warna           = fields.Selection([('Putih','Putih'),('Kuning','Kuning'),('Hijau','Hijau'),('Pink','Pink')], string='Warna')
    quantity        = fields.Float('Quantity', default=0.0, digits=dp.get_precision('Product Unit(s)'))

    @api.multi
    def confirm_repair(self):
        cr = self.env.cr
        if self.workorder_date:
            cr.execute("UPDATE mrp_workorder SET workorder_date=%s WHERE id=%s",(str(self.workorder_date),self.workorder_id.id))
        if self.shift:
            cr.execute("UPDATE mrp_workorder SET shift=%s WHERE id=%s",(str(self.shift),self.workorder_id.id))
        if self.user_id:
            cr.execute("UPDATE mrp_workorder SET user_id=%s, iduser=%s WHERE id=%s",(self.user_id.id,self.user_id.login,self.workorder_id.id))
        if self.machine_id:
            cr.execute("UPDATE mrp_workorder SET machine_id=%s, barcodewo=%s WHERE id=%s",(self.machine_id.id,self.machine_id.code,self.workorder_id.id))
        if self.retur:
            cr.execute("UPDATE mrp_workorder SET retur=%s WHERE id=%s",(self.retur,self.workorder_id.id))
        if self.warna:
            cr.execute("UPDATE mrp_workorder SET warna=%s WHERE id=%s",(self.warna,self.workorder_id.id))
        if self.quantity > 0.0 :
            cr.execute("UPDATE mrp_workorder SET qty_produced=%s, qty_produced_real=%s WHERE id=%s",(self.quantity,self.quantity,self.workorder_id.id))
        self.workorder_id.message_post(body=_('Workorder repaired...'))
        #return {'type': 'ir.actions.client', 'tag': 'reload'}

MrpWorkorderRepair()


class MrpWorkorderWire(models.TransientModel):
    _name = 'mrp.workorder.wire'
    _description = 'Scan Wire Workorder'

    @api.model
    def _get_default_workorder_id(self):
        return self._context.get('active_id')

    workorder_id = fields.Many2one('mrp.workorder', 'Work Order', required=True, default=_get_default_workorder_id)
    message           = fields.Char('Message')

MrpWorkorderWire()