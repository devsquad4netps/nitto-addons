# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class MrpWorkorderSplit(models.TransientModel):
    _name = 'mrp.workorder.split.wizard'
    _description = 'Split Wortkorder'

    @api.model
    def _get_default_workorder_id(self):
        return self._context.get('active_id')

    split_qty = fields.Float('Quantity to Split')
    workorder_id = fields.Many2one('mrp.workorder', 'Work Order', required=True, default=_get_default_workorder_id)

    @api.multi
    def confirm_split(self):
        self.ensure_one()
        if self.split_qty <= 0.0 :
            raise UserError(_("Quantity split tidak boleh nol !")) 
        if self.split_qty >= self.workorder_id.qty_producing :
            raise UserError(_("Quantity split tidak boleh sama dengan atau lebih besar dari %s !") % (self.workorder_id.qty_producing))
        return self.workorder_id.button_split_workorder(self.split_qty)

MrpWorkorderSplit()