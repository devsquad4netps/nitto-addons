# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _


class res_config_settings(models.TransientModel):
    _inherit = 'res.config.settings'

    sh_workorder_scan = fields.Selection([
        ('iduser','Operator'),
        ('barcodewo','ID Mesin'),
        ('both','Both'),
        ],related='company_id.sh_workorder_scan', string='Workorder Scan Options', translate=True,readonly = False)

res_config_settings()