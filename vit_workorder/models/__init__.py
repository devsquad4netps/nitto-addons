# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import mrp_workorder
from . import res_users
from . import res_company
from . import res_config_scanner
from . import stock
from . import mrp_bom