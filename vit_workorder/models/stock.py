# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import exceptions
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning, ValidationError
from odoo.addons import decimal_precision as dp


class StockPicking(models.Model):
    _inherit = 'stock.picking'


    lot_temp_id = fields.Many2one('stock.production.lot.temp','Lot Temp', copy=False)
    scan_date = fields.Date('Scan Date', copy=False)

    @api.multi
    def get_lot_raw_material(self):
        for pick in self:
            workorder = self.env['mrp.workorder'].sudo().search([('production_id.procurement_group_id','=',pick.group_id.id),
                                                                ('lot_temp_line_ids','!=',False)],limit=1)
            if workorder :
                lot = self.env['stock.production.lot']
                lot_temp = self.env['stock.production.lot.temp']
                lot_id2 = False
                scan = []
                for list_name in workorder.lot_temp_line_ids.mapped('name'):
                    scan_name = list_name.split("#")
                    if len(scan_name) == 6 :
                        sname = '#'.join(scan_name[:5])
                    else :
                        sname = list_name
                    scan.append(sname)
                for mv in pick.move_ids_without_package :
                    #t_qty = sum(workorder.lot_temp_line_ids.mapped('qty'))      
                    for line in workorder.lot_temp_line_ids.filtered(lambda x:x.qty > 0.0 and x.product_lot_id.id == mv.product_id.id) :
                        lot_name = line.name.split("#")
                        t_qty = line.qty
                        if len(lot_name) == 6 :
                            name = '#'.join(lot_name[:5])
                        else :
                            name = list_name
                        t_lot_temp = lot_temp.sudo().search([('name','like',name),('workorder_id','=',workorder.id)])
                        if t_lot_temp:
                            t_qty = sum(t_lot_temp.mapped('qty'))
                            
                        lot_id = lot.sudo().search([('name','=',name),('product_id','=',line.product_lot_id.id)],limit=1)
                        if not lot_id and line.product_lot_id.id == mv.product_id.id:
                            lot_id = lot.create({'name' : name,'product_id':mv.product_id.id})

                        if mv.move_line_ids and lot_id:
                            for mvl in mv.move_line_ids :
                                if not mvl.lot_id or mvl.lot_id.name not in scan:
                                    mvl.write({'lot_id' : lot_id.id,'qty_done' : t_qty,'workorder_id':workorder.id,})
                                    break
                                elif mvl.lot_id and mvl.lot_id.name == name and mvl.qty_done == 0.0 :
                                    mvl.write({'qty_done' :t_qty})
                                    break
                                elif not mv.move_line_ids.filtered(lambda x:x.lot_id.name == name):
                                    self.env['stock.move.line'].create({'move_id': mv.id,
                                                                        'product_id': mv.product_id.id,
                                                                        'lot_id':lot_id.id,
                                                                        'qty_done':t_qty,#line.qty,
                                                                        'picking_id':mv.picking_id.id,
                                                                        'product_uom_id':mv.product_uom.id,
                                                                        'location_id':mv.location_id.id,
                                                                        'workorder_id':workorder.id,
                                                                        'done_wo':True,
                                                                        'location_dest_id':mv.location_dest_id.id})
                                else :
                                    continue
                        if not mv.move_line_ids.filtered(lambda x:x.lot_id.name == name) and lot_id:
                            self.env['stock.move.line'].create({'move_id': mv.id,
                                            'product_id': mv.product_id.id,
                                            'lot_id':lot_id.id,
                                            'qty_done':t_qty,#line.qty,
                                            'picking_id':mv.picking_id.id,
                                            'product_uom_id':mv.product_uom.id,
                                            'location_id':mv.location_id.id,
                                            'workorder_id':workorder.id,
                                            'done_wo':True,
                                            'location_dest_id':mv.location_dest_id.id})
                    if mv.quantity_done == 0.0 :
                        mv.quantity_done = sum(mv.move_line_ids.mapped('qty_done'))

                product_ids = pick.move_ids_without_package.mapped('product_id')
                for temp in workorder.lot_temp_line_ids.filtered(lambda x:x.qty > 0.0 and x.product_lot_id.id not in product_ids.ids) :
                    lot_name = temp.name.split("#")
                    t_qty = temp.qty
                    lot_id2 = False
                    if len(lot_name) == 6 :
                        name = '#'.join(lot_name[:5])
                    else :
                        name = list_name
                    if temp.product_lot_id:
                        lot_id2 = lot.sudo().search([('name','=',name),('product_id','=',temp.product_lot_id.id)],limit=1)
                        if not lot_id2:
                            lot_id2 = lot.create({'name' : name,'product_id':temp.product_lot_id.id})
                    else:
                        if workorder.bom_wire :
                            lot_id2 = lot.sudo().search([('name','=',name),('product_id','=',workorder.bom_wire.id)],limit=1)
                            if not lot_id2:
                                lot_id2 = lot.create({'name' : name,'product_id':workorder.bom_wire.id})
                    if lot_id2 :
                        move_line_vals = {'product_id': lot_id2.product_id.id,
                                                'lot_id':lot_id2.id,
                                                'qty_done':t_qty,
                                                'picking_id':pick.id,
                                                'product_uom_id':lot_id2.product_id.uom_id.id,
                                                'location_id':pick.location_id.id,
                                                'workorder_id':workorder.id,
                                                'done_wo':True,
                                                'location_dest_id':pick.location_dest_id.id}
                        self.env['stock.move'].create({'picking_id': pick.id,
                                                'product_id': lot_id2.product_id.id,
                                                'name': lot_id2.product_id.name,
                                                'product_uom_qty' : t_qty,
                                                'product_uom': lot_id2.product_id.uom_id.id,
                                                'location_id': pick.location_id.id,
                                                'location_dest_id': pick.location_dest_id.id,
                                                'raw_material_production_id': workorder.production_id.id,
                                                'group_id' : pick.group_id.id,
                                                'move_line_ids' : [(0, 0, move_line_vals)]
                                            })

                if pick.state == 'draft' :
                    # hapus qty done yg 0
                    pick.move_ids_without_package.filtered(lambda qty:qty.quantity_done <= 0.0)._action_cancel()
                    pick.move_ids_without_package.filtered(lambda qty:qty.quantity_done <= 0.0).unlink()
                    pick.action_confirm()
                #delete yg tidak ada lotnya
                for mve in pick.move_ids_without_package :
                    for mvlines in mve.move_line_ids :
                        # if not mvlines.lot_id and mvlines.product_uom_qty == 0.0 and mvlines.qty_done == 0.0 :
                        if mvlines.qty_done == 0.0 :
                            mvlines.unlink()
                if pick.state == 'waiting' :
                    pick.action_assign()
        return True

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        for pick in self:
            if pick.location_id.usage == 'production' and pick.group_id:
                mo = self.env['mrp.production'].sudo().search([('procurement_group_id','=',pick.group_id.id)])
                if mo :
                    mo.button_mark_done()
                # if mo.workorder_ids:
                #     wo_pending = mo.workorder_ids.filtered(lambda wo:wo.state not in ('cancel','done'))
                #     if not wo_pending:
                #         picking_pending = mo.picking_ids.filtered(lambda picking:picking.state not in ('draft','cancel','done'))
                #         if not picking_pending:
                #             mo.button_mark_done()
        return res

StockPicking()


class StockMoveLive(models.Model):
    _inherit = 'stock.move.line'

    workorder_id = fields.Many2one('mrp.workorder','Workorder')
    no_urut = fields.Integer('No Urut', copy=False)

    @api.constrains('lot_id', 'product_id')
    def _check_lot_product(self):
        for line in self:
            if line.lot_id and line.product_id != line.lot_id.product_id:
                desc = line.picking_id.name
                if not desc :
                    desc = line.move_id.picking_id.name
                if not desc :
                    desc = line.move_id.origin
                if not desc :
                    desc = line.move_id.name
                if not desc :
                    desc = 'id move : '+str(line.move_id.id)
                raise ValidationError(_('Lot %s tidak bisa di assign ke %s, \n karena sudah dipakai di product %s (transaksi nomor : %s)' % (line.lot_id.name, line.product_id.display_name, line.lot_id.product_id.display_name, desc)))

StockMoveLive()


class StockMove(models.Model):
    _inherit = 'stock.move'

    barcode_wire = fields.Char('Barcode Wire',copy=False)
    warning = fields.Char('Warning',copy=False)

    @api.onchange('barcode_wire')
    def onchange_barcode_wire(self):
        if self.barcode_wire:
            no_urut = 0
            wire = self.barcode_wire.split("#")
            if len(wire) == 6 :
                barcode_wire = '#'.join(wire[:5])
                no_urut = int(wire[-1:][0])
            else :
                barcode_wire=self.barcode_wire
            lot = self.env['stock.production.lot'].sudo().search([('name','=',barcode_wire)],limit=1)
            if not lot :
                self.warning = 'Lot '+ str(barcode_wire)+' tidak ditemukan di sistem (master lot).'
                self.barcode_wire = False
            else :
                if lot.product_id.id != self.product_id.id :
                    self.warning = 'Lot ('+ str(barcode_wire)+') '+lot.product_id.name+' tidak sesuai dengan product ini '+self.product_id.name
                    self.barcode_wire = False
                else:
                    if self.move_line_ids.filtered(lambda i:i.no_urut == no_urut and i.lot_id.id == lot.id):
                        self.barcode_wire = False
                    else:
                        lot_wire = self.env['stock.production.lot.temp'].sudo().search([('name','=',self.barcode_wire)],limit=1)
                        vals = {'move_id': self.id,
                                'product_id': self.product_id.id,
                                'lot_id':lot.id if lot else False,
                                # 'qty_done':lot_wire.qty if lot_wire else 0.0,
                                'qty_done': 0.0,
                                'picking_id':self.picking_id.id,
                                'product_uom_id':self.product_uom.id,
                                'location_id':self.location_id.id,
                                'no_urut': no_urut,
                                'location_dest_id':self.location_dest_id.id}                 
                        new_line = self.move_line_ids.new(vals)
                        self.move_line_ids |= new_line
            self.barcode_wire = False

    @api.multi
    def _action_assign(self):
        #import pdb;pdb.set_trace()
        if self :
            lot = False
            move =  self.filtered(lambda mv:mv.location_dest_id.usage == 'production' and mv.raw_material_production_id)
            if move :
                lot = []
                for mvl in move.move_line_ids.filtered(lambda mvl:mvl.lot_id) :
                    lot.append(mvl.lot_id.name)
        res = super(StockMove, self)._action_assign()
        if self and lot:
            move = self.filtered(lambda mv:mv.location_dest_id.usage == 'production' and mv.raw_material_production_id)
            if move :
               for mvl in move.move_line_ids.filtered(lambda mvl:mvl.lot_id.name not in lot) : 
                    mvl.unlink()
        return res

StockMove()


class StockLotTemporary(models.Model):
    _name = 'stock.production.lot.temp'


    @api.model
    def create(self, vals):
        if 'qty' not in vals:
            raise Warning('Quantity wire %s tidak boleh bernilai nol' % (vals['name']))
        elif vals['qty'] == 0.0:
            raise Warning('Quantity wire %s tidak boleh bernilai nol.' % (vals['name']))
        elif 'product_lot_id' not in vals:
            raise Warning('Raw Material %s tidak ada di master data.' % (vals['name']))
        elif vals['product_lot_id'] == False :
            raise Warning('Raw Material %s tidak ada di master data' % (vals['name']))
        res = super(StockLotTemporary, self).create(vals)
        if res.workorder_id:
            if res.workorder_id.production_id.move_raw_ids :
                picking_id = res.workorder_id.production_id.move_raw_ids[0].picking_id
                if picking_id :
                    if not picking_id.lot_temp_id:
                        picking_id.write({'lot_temp_id': res.id})
                    if not picking_id.scan_date:
                        picking_id.write({'scan_date': res.create_date})
        return res


    name = fields.Char('Name',size=60, required=True,)
    workorder_id = fields.Many2one('mrp.workorder','Workorder')
    product_id = fields.Many2one('product.product',related='workorder_id.product_id',store=True)
    product_lot_id = fields.Many2one('product.product','Raw Material')
    qty = fields.Float('Quantity')
    note = fields.Char('Note')
    wire_tidak_sesuai = fields.Boolean('Wire Tidak Sesuai',default=True, copy=False)
    alternative_product_ids = fields.Many2many('product.product',string='Alternative Component', copy=False)

    # _sql_constraints = [
    #     ('default_name_uniq', 'unique (name,product_id)', 'The name and product of the temporary lot must be unique !'),
    # ]

StockLotTemporary()