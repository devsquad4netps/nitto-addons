# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp


class ResUsers(models.Model):
    _inherit = 'res.users'

    workcenter_ids = fields.Many2many('mrp.workcenter',string='Allowed Workcenter')

ResUsers()