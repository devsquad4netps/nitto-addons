# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = 'res.company'
    
    sh_workorder_scan = fields.Selection([
        ('iduser','Operator'),
        ('barcodewo','ID Mesin'),
        ('both','Both'),
        ],default = 'both' ,string='Workorder Scan Options', translate=True)

ResCompany()