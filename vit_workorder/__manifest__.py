# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Production Workorder Split and Search Old WO and Add manual WO',
    'website': 'http://www.vitraining.com',
    'author': 'vITraining',
    'version': '5.3',

    'support': 'widianajuniar@gmail.com',
    'license': 'AGPL-3',
    'images': [],
    'category': 'Manufacturing',
    'sequence': 14,
    'summary': 'Split of Workorder',

    'depends': ['base','mrp','vit_mrp_cost','sh_barcode_scanner','vit_nitto_mrp','vit_nitto_partner'],

    'description': """
    * Fitur untuk pecah/spit Workorder dalam satu nomor Manufacturing Order dan add manual WO
    * bisa get lot wire di picking sesuai dengan yang di scan di workorder
    """,
    'data': [
        'data/data.xml',
        'security/group.xml',
        'security/ir.model.access.csv',
        'wizard/mrp_workorder_split_views.xml',
        'wizard/mrp_workorder_addnew_views.xml',
        'wizard/workorder_repair.xml',
        'views/mrp_workorder_views.xml',
        'views/mrp_production_views.xml',
        'views/mrp_bom_views.xml',
        'views/res_users.xml',
        'views/stock_views.xml',
    ],
    'test': [],
    'application': True,
}
