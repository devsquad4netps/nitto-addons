from odoo import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    alias_name = fields.Char(string='Alias',related='partner_id.alias_name')

SaleOrder()