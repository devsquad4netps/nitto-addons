from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    type_partner_id = fields.Many2one('res.partner.type','Partner Type', related="partner_id.type_partner_id", store=True)
    group_partner_id = fields.Many2one('res.partner.group','Partner Group', related="partner_id.group_partner_id", store=True)

AccountInvoice()