from odoo import api, exceptions, fields, models, _
from odoo.addons import decimal_precision as dp

class ResPartner(models.Model):
    _inherit = 'res.partner'


    @api.multi
    @api.depends('ref', 'name','alias_name')
    def name_get(self):
        #result = super(ResPartner, self).name_get()
        result = []
        for res in self:
            name = res.name
            if res.ref :
                name = '['+res.ref+'] '+name
            if res.alias_name :
                name = name + ' -- ' + res.alias_name
            result.append((res.id, name))
        return result


    @api.onchange('alias_name')
    def onchange_barcode(self):
        if self.alias_name:
            kode = self.ref
            if kode :
                ref = '['+self.ref+'] '
            else:
                ref = ''

            self.display_name = ref+self.name+' (' + self.alias_name+ ')'


    type_partner_id = fields.Many2one('res.partner.type','Type', track_visibility='on_change')
    group_partner_id = fields.Many2one('res.partner.group','Group', track_visibility='on_change')
    alias_name = fields.Char('Alias',size=250, track_visibility='on_change', help="Untuk keperluan Print out Surat Jalan")
    bc_name = fields.Char('BC Name',size=50, track_visibility='on_change' ,help="Untuk keperluan Report BC")



ResPartner()