from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class ResPartnerGroup(models.Model):
    _name = 'res.partner.group'

    name = fields.Char('Name', required=True)

ResPartnerGroup()