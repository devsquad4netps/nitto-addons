from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class ResPartnerType(models.Model):
    _name = 'res.partner.type'

    name = fields.Char('Name', required=True)
    notes = fields.Char('Notes', required=False, help="Untuk keperluan di kartu produksi")

ResPartnerType()