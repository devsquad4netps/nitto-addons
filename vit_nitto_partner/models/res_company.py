from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class ResCompany(models.Model):
    _inherit = 'res.company'

    second_name = fields.Char('Second Name')
    bc_name = fields.Char('BC Name', size=50)

    @api.multi
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.second_name :
                name = res.second_name
            result.append((res.id, name))
        return result

ResCompany()