{
	"name": "Nitto Partner",
	"version": "1.4", 
	"depends": [
		"base",
		"account",
		"sale",
		"sale_management",
	],
	"author": "http://www.vitraining.com", 
	"category": "Tool", 
	'website': 'http://www.vitraining.com',
	"description": """\


""",
	"data": [
		"views/res_partner_view.xml",
		"views/res_company_view.xml",
		"views/res_partner_master_view.xml",
		"views/sale_order_view.xml",
		"security/ir.model.access.csv"
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}