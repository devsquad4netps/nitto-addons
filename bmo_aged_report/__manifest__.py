# -*- coding: utf-8 -*-
{
    'name': "Aged Report by Currency",

    'summary': """
        to customized Aged Report by Currency
    """,

    'description': """
        to customized Aged Report by Currency
    """,

    'author': "Dani R.",
    'website': "https://www.bemosoft.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Report',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account_reports'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/src_template_views.xml',
        # 'views/views.xml',
        # 'views/templates.xml',
    ],
    'auto_install': True,
    'installable': True,
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}