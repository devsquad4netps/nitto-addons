
from odoo import models, api, fields, _
from odoo.tools.misc import formatLang
from odoo.tools.misc import format_date

class report_account_aged_partner(models.AbstractModel):
    _inherit = "account.aged.partner"
    
    # currency
    filter_currency = True
    # -------

    # force override
    @api.model
    def _get_lines(self, options, line_id=None):
        sign = -1.0 if self.env.context.get('aged_balance') else 1.0
        lines = []
        account_types = [self.env.context.get('account_type')]
        # specify currency value
        domain = [('code', '=', 'HB')]
        aged_curr_id = self.env['account.aged.currency'].search(domain)
        if 'selected_currency_id' in options:
            if options['selected_currency_id']:
                aged_curr_id = self.env['account.aged.currency'].browse(options['selected_currency_id'])
        convert_dict = {'HB': True, 'OC': False,}
        convert_currency = False
        if aged_curr_id:
            convert_currency = convert_dict[aged_curr_id.code]
        # ------------------------------------------------
        results, total, amls = self.env['report.account.report_agedpartnerbalance'].with_context(\
            include_nullified_amount=True)._get_partner_move_lines(account_types, self._context['date_to'], 'posted', 30)
        for values in results:
            partner_id = self.env['res.partner'].browse(values['partner_id'])
            curr_dict = {
                'HB': self.env.user.company_id.currency_id,
                'OC': partner_id.property_purchase_currency_id,
            }
            currency_id = curr_dict['HB']
            if aged_curr_id:
                currency_id = curr_dict[aged_curr_id.code]
            aml = False
            for line in amls[values['partner_id']]:
                aml = line['line']
            values['direction'] = aml.company_id.currency_id._convert(values['direction'], currency_id, aml.company_id, aml.date)
            values['4'] = aml.company_id.currency_id._convert(values['4'], currency_id, aml.company_id, aml.date)
            values['3'] = aml.company_id.currency_id._convert(values['3'], currency_id, aml.company_id, aml.date)
            values['2'] = aml.company_id.currency_id._convert(values['2'], currency_id, aml.company_id, aml.date)
            values['1'] = aml.company_id.currency_id._convert(values['1'], currency_id, aml.company_id, aml.date)
            values['0'] = aml.company_id.currency_id._convert(values['0'], currency_id, aml.company_id, aml.date)
            values['total'] = aml.company_id.currency_id._convert(values['total'], currency_id, aml.company_id, aml.date)
            if line_id and 'partner_%s' % (values['partner_id'],) != line_id:
                continue
            vals_name = values['name']
            columns = [{'name': ''}] * 3 + [{'name': self.format_value(sign * v, currency_id)} for v in [values['direction'], \
                values['4'], values['3'], values['2'], values['1'], values['0'], values['total']]]
            if 'print_mode' in self.env.context and 'commit_assetsbundle' not in self.env.context:
                if self.env.context['print_mode']:
                    columns = [{'name': ''}] * 3 + [{'name': currency_id.name}] + [{'name': self.format_value(sign * v, currency_id)} \
                        for v in [values['direction'], values['4'], values['3'], values['2'], values['1'], values['0'], values['total']]]
            if 'print_mode' in self.env.context and 'commit_assetsbundle' in self.env.context:
                if self.env.context['commit_assetsbundle']:
                    columns = [{'name': ''}] * 3 + [{'name': formatLang(self.env, sign * v)} for v in [values['direction'], values['4'], \
                    values['3'], values['2'], values['1'], values['0'], values['total']]]
                    vals_name = values['name'] + '(%s)' % currency_id.name

            vals = {
                'id': 'partner_%s' % (values['partner_id'],),
                'name': vals_name,
                'level': 2,
                'columns': columns,
                'unfoldable': True,
                'unfolded': 'partner_%s' % (values['partner_id'],) in options.get('unfolded_lines'),
            }
            lines.append(vals)
            if 'partner_%s' % (values['partner_id'],) in options.get('unfolded_lines'):
                for line in amls[values['partner_id']]:
                    aml = line['line']
                    caret_type = 'account.move'
                    if aml.invoice_id:
                        caret_type = 'account.invoice.in' if aml.invoice_id.type in ('in_refund', 'in_invoice') else 'account.invoice.out'
                    elif aml.payment_id:
                        caret_type = 'account.payment'
                    line['amount'] = aml.company_id.currency_id._convert(line['amount'], currency_id, aml.company_id, aml.date)
                    columns = [{'name': v} for v in [aml.journal_id.code, aml.account_id.code, self._format_aml_name(aml)]] + \
                            [{'name': v} for v in [line['period'] == 6-i and self.format_value(sign * line['amount'], currency_id) \
                            or '' for i in range(7)]]
                    if 'print_mode' in self.env.context and 'commit_assetsbundle' not in self.env.context:
                        if self.env.context['print_mode']:
                            columns = [{'name': v} for v in [aml.journal_id.code, aml.account_id.code, self._format_aml_name(aml)]] \
                                + [{'name': currency_id.name}] + [{'name': v} for v in [line['period'] == 6-i and \
                                self.format_value(sign * line['amount'], currency_id) or '' for i in range(7)]]
                    vals = {
                        'id': aml.id,
                        'name': format_date(self.env, aml.date_maturity or aml.date),
                        'class': 'date',
                        'caret_options': caret_type,
                        'level': 4,
                        'parent_id': 'partner_%s' % (values['partner_id'],),
                        'columns': columns,
                        'action_context': aml.get_action_context(),
                    }
                    lines.append(vals)
        if total and not line_id:
            total[6] = aml.company_id.currency_id._convert(total[6], currency_id, aml.company_id, aml.date)
            total[4] = aml.company_id.currency_id._convert(total[4], currency_id, aml.company_id, aml.date)
            total[3] = aml.company_id.currency_id._convert(total[3], currency_id, aml.company_id, aml.date)
            total[2] = aml.company_id.currency_id._convert(total[2], currency_id, aml.company_id, aml.date)
            total[1] = aml.company_id.currency_id._convert(total[1], currency_id, aml.company_id, aml.date)
            total[0] = aml.company_id.currency_id._convert(total[0], currency_id, aml.company_id, aml.date)
            total[5] = aml.company_id.currency_id._convert(total[5], currency_id, aml.company_id, aml.date)
            columns = [{'name': ''}] * 3 + [{'name': self.format_value(sign * v, currency_id)} 
                    for v in [total[6], total[4], total[3], total[2], total[1], total[0], total[5]]]
            if 'print_mode' in self.env.context and 'commit_assetsbundle' not in self.env.context:
                if self.env.context['print_mode']:
                    columns = [{'name': ''}] * 4 + [{'name': self.format_value(sign * v, currency_id)} 
                            for v in [total[6], total[4], total[3], total[2], total[1], total[0], total[5]]]
            total_line = {
                'id': 0,
                'name': _('Total'),
                'class': 'total',
                'level': 2,
                'columns': columns,
            }
            if 'print_mode' in self.env.context:
                if self.env.context['print_mode'] and convert_currency:
                    lines.append(total_line)
            else:
                lines.append(total_line)
        return lines

