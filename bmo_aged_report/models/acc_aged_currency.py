from odoo import api, fields, models

class AgedCurrency(models.Model):
    _name = 'account.aged.currency'
    _description = 'Currency Selection for Aged Report'

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='Code', required=True)
