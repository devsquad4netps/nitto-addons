from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportBalanceSubconWizard(models.TransientModel):
    _name = "vit.report.balance_subcon.wizard"
    _description = "Report Balance Subcon"

    @api.onchange('partner_ids','group_report')
    def onchange_group_report(self):
        domain = []
        contract = self.env['mrp.subcontract']
        contract_exist = contract.sudo().search([('company_id','in',self.company_ids.ids),('state','=','confirm')])
        if contract_exist :
            domain = {'domain': {'partner_ids': [('id','in',contract_exist.mapped('partner_id').ids)]}}
        return domain

    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes")
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    partner_ids = fields.Many2many("res.partner",string='Vendor')
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    group_report = fields.Selection([('CF','CF'),                                   
                                    ('Heading','Heading'),
                                    ('Machine','Machining'),
                                    ('Rolling','Rolling'),
                                    ('Trimming','Trimming'),
                                    ('Sloting','Sloting'),
                                    ('Cutting','Cutting'),
                                    ('Washing','Washing'),
                                    ('Furnished','Furnace'),
                                    ('Plating','Plating'),
                                    ('Final Quality','Final Quality'),
                                    ('Three Bond','Three Bond'),
                                    ('Packing','Packing')],"Group")

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'left','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000'})

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        wbf['header_month_sum'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number_val'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})

        wbf['content_number_sum_val'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['grey']})

        
        return wbf, workbook

    @api.multi
    def action_print_report(self):
        self.ensure_one()
        for report in self :
            i_type = dict(self._fields['group_report'].selection).get(report.group_report)
            report_name = 'Laporan Balance %s Subcont Period %s s/d %s'%(report.group_report,str(report.date_start),str(report.date_end))
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
            company = company[:-3]
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")
            return report.action_print_report_balance_subcont(i_type, report_name, company, companys, self.partner_ids)

    def action_print_report_balance_subcont(self, i_type, report_name, company, companys, partners):
        for i in self :
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name+' ('+i_type+')')
            worksheet.set_column('A1:A1', 8)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 35)
            worksheet.set_column('D1:D1', 25)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 25)
            worksheet.set_column('K1:K1', 25)
            worksheet.set_column('LL:L1', 5)
            
            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:J2', report_name, wbf['company'])
            worksheet.merge_range('H3:J3', 'Printed on : '+ str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            row = 4
            move = self.env['stock.move']
            grand_t_netto = 0.0
            for partner in partners :
                # OUT SUBCON
                sql = "select sm.id from stock_move sm " \
                  "left join stock_picking sp on sp.id = sm.picking_id " \
                  "where sp.state = 'done' and sm.production_subcon_id is not null " \
                  "and (sp.is_incoming_subcon is null or sp.is_incoming_subcon = false) " \
                  "and sp.company_id in %s and sp.partner_id = %s and (sp.date_done between '%s' and '%s') " \
                  "order by sp.date_done asc , sp.partner_id" % (companys, partner.id, str(self.date_start), str(self.date_end))
            
                cr.execute(sql)
                datas = cr.fetchall()
                if not datas :
                    continue
                worksheet.write('A%s'%(row), partner.ref or '', wbf['header_table'])
                worksheet.merge_range('B%s:C%s'%(row,row), partner.name, wbf['header_table'])
                row+=1
                worksheet.write('A%s'%(row), 'No', wbf['header_month'])
                worksheet.write('B%s'%(row), 'Item Code', wbf['header_month'])
                worksheet.write('C%s'%(row), 'Item Name', wbf['header_month'])
                worksheet.write('D%s'%(row), 'Manufacturing Order', wbf['header_month'])
                worksheet.write('E%s'%(row), 'No Poly Box', wbf['header_month'])
                worksheet.write('F%s'%(row), 'Netto', wbf['header_month'])
                worksheet.write('G%s'%(row), 'Balance', wbf['header_month'])
                worksheet.write('H%s'%(row), 'Delivery Date', wbf['header_month'])
                worksheet.write('I%s'%(row), 'Incoming Date', wbf['header_month'])
                worksheet.write('J%s'%(row), 'Vendor Alias', wbf['header_month'])
                worksheet.write('K%s'%(row), 'Contract Number', wbf['header_month'])
                worksheet.write('L%s'%(row), 'Days', wbf['header_month'])
                row+=1
                no = 1
                t_netto = 0.0
                t_balance = 0.0
                
                for dt in datas :
                    data = move.browse(dt)
                    days = ''
                    # IN SUBCON
                    sql2 = "select sm.id from stock_move sm " \
                      "left join stock_picking sp on sp.id = sm.picking_id " \
                      "where sp.state = 'done' and sm.production_subcon_id = %s " \
                      "and sp.is_incoming_subcon = true and sp.company_id in %s " \
                      "and sp.partner_id = %s and (sp.date_done between '%s' and '%s') " \
                      "order by sp.date_done asc , sp.partner_id limit 1" % (data.production_subcon_id.id, companys, partner.id, str(self.date_start), str(self.date_end))
                
                    cr.execute(sql2)
                    dat = cr.fetchall()
                    incoming_date = ''
                    if dat :
                        # Incoming Subcon
                        incoming_date = str(move.browse(dat[0][0]).picking_id.date_done)[:10]
                        dt_out = datetime.strptime(str(data.picking_id.date_done)[:10], "%Y-%m-%d")
                        dt_in = datetime.strptime(incoming_date, "%Y-%m-%d")
                        day = dt_in - dt_out
                        days = day.days
                    else :
                        # Outgoing Subcon
                        dt_out = datetime.strptime(str(data.picking_id.date_done)[:10], "%Y-%m-%d")
                        dt_in = datetime.strptime(str(i.date_end), "%Y-%m-%d")
                        day = dt_in - dt_out
                        days = day.days

                    if data.production_subcon_id.workorder_ids.filtered(lambda wo:wo.workcenter_id.group_report == i.group_report and wo.is_subcontracting):
                        worksheet.write('A%s'%(row), no, wbf['title_doc'])
                        worksheet.write('B%s'%(row), data.product_id.default_code, wbf['title_doc'])
                        worksheet.write('C%s'%(row), data.product_id.name, wbf['title_doc'])
                        worksheet.write('D%s'%(row), data.production_subcon_id.name, wbf['title_doc'])
                        worksheet.write('E%s'%(row), data.production_subcon_id.no_poly_box, wbf['title_doc'])
                        worksheet.write('F%s'%(row), data.netto_wire, wbf['content_number_val'])
                        worksheet.write('G%s'%(row), data.product_uom_qty if not incoming_date else '0', wbf['content_number_val'])
                        worksheet.write('H%s'%(row), str(data.picking_id.date_done)[:10] if data.picking_id.date_done else '', wbf['title_doc'])
                        worksheet.write('I%s'%(row), incoming_date, wbf['title_doc'])
                        worksheet.write('J%s'%(row), data.picking_id.partner_id.alias_name_kartu_produksi if data.picking_id and data.picking_id.partner_id and data.picking_id.partner_id.alias_name_kartu_produksi else '', wbf['content_number_val'])
                        worksheet.write('K%s'%(row), data.picking_id.subcontract_id.name, wbf['title_doc'])
                        worksheet.write('L%s'%(row), days, wbf['content_number_val'])

                        t_netto += data.netto_wire
                        if not incoming_date :
                            t_balance += data.product_uom_qty

                        no += 1
                        row += 1
                # sum total perpartner
                worksheet.write('F%s'%(row), t_netto, wbf['content_number_sum_val'])
                worksheet.write('G%s'%(row), t_balance, wbf['content_number_sum_val'])

                #sum grand total
                grand_t_netto += t_netto
                row += 1
            if i.notes :
                worksheet.merge_range('A%s:J%s'%(row+2,row+2), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

ReportBalanceSubconWizard()