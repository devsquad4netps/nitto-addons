from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import UserError, Warning
import logging
_logger = logging.getLogger(__name__)


class ReportBalanceType(models.Model):
    _name = "vit.report_balance_type"
    _description = "Report Balance Type"

    name = fields.Char("Name",required=True)  
    notes = fields.Char("Notes") 

    def _get_action(self, action_xmlid):
        # TDE TODO check to have one view + custo in methods
        action = self.env.ref(action_xmlid).read()[0]
        if self:
            action['name'] = self.name
        return action

    def get_report_type(self):
        return self._get_action('vit_report_balance.vit_report_balance_action')

ReportBalanceType()  


class report_balance(models.Model):
    _name = "vit.report_balance"
    _description = "Report Balance"

    @api.onchange('report_type')
    def report_type_change(self):
        self.name = self.report_type.name                                    

    @api.onchange('date_start','date_end')
    def date_change(self):
        if self.date_start > self.date_end :
            raise Warning("Start date tidak boleh lebih besar dari end date")

    @api.model
    def _default_warehouse_id(self):
        company = self.env.user.company_id.id
        warehouse_ids = self.env['stock.warehouse'].search([('company_id', '=', company)], limit=1)
        return warehouse_ids

    name = fields.Char( required=True, string="Name",  help="")
    name_report = fields.Char(related='report_type.name',string='Report Type Name',store=True)
    report_type = fields.Many2one('vit.report_balance_type','Report Type')
    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',default=_default_warehouse_id)
    company_id = fields.Many2one(comodel_name="res.company",  string="Company" ,required=True,default=lambda self: self.env.user.company_id)
    report_so_ids = fields.One2many(
        string='report',
        comodel_name='vit.report_balance_so',
        inverse_name='report_id')
    report_wip_ids = fields.One2many(
        string='report',
        comodel_name='vit.report_balance_wip',
        inverse_name='report_id')
    date_start = fields.Date( string="Date start", required=True, default='2020-01-01') #default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    data = fields.Binary('File')


    sql_wo = """select 
            mwo.product_id as product_id
            from
                mrp_workorder mwo
                left join product_product pp on pp.id = mwo.product_id 
                left join mrp_production mpo on mwo.production_id = mpo.id
                left join product_template pt on pp.product_tmpl_id = pt.id 
                left join product_category pc on pt.categ_id = pc.id 
                where mwo.state = 'done' and mwo.is_wip = true
                and pc.name ilike '%%Finish Good%%' 
                and mpo.company_id=%s 
                and mwo.date_finished between %s and %s
                group by mwo.product_id
            """ 
    sql_heading = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Heading'
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_rolling = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Rolling' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_furnace = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Furnished' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_plating = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and wo.is_subcontracting=false and mw.group_report = 'Plating' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_subcon_plating = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and wo.is_subcontracting=true and mw.group_report = 'Plating' 
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_fq = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Final Quality' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_machine = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end  
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Machine' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_tb = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Three Bond' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_washing = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Washing' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_packing = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Packing' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and mp.state not in ('cancel','done')
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """
    sql_so_bln_lalu = """select sum(product_uom_qty - qty_delivered)
                        from sale_order_line sol 
                        left join sale_order so on sol.order_id = so.id
                        where so.state in ('sale','done')
                        and sol.delivery_date < %s
                        and sol.product_id = %s
                        and so.company_id = %s
                        """
    sql_so_bln_ini = """select sum(product_uom_qty - qty_delivered)
                        from sale_order_line sol 
                        left join sale_order so on sol.order_id = so.id
                        where so.state in ('sale','done')
                        and sol.delivery_date between %s and %s
                        and sol.product_id = %s
                        and so.company_id = %s
                    """
    sql_subcon_in = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and wo.is_subcontracting=true 
                 and wo.product_id=%s and mp.company_id=%s and wo.date_finished between %s and %s 
                """

    sql_subcon_out = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'progress' and wo.is_wip = true and wo.is_subcontracting=true 
                 and wo.product_id=%s and mp.company_id=%s and wo.date_start between %s and %s 
                """


    # //////////////////////////////////////////////////////// GENERATE MASTER //////////////////////////////////////////////////////////////////
    @api.multi
    def generate_master(self):
        # import pdb;pdb.set_trace()
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        date1 = datetime.strptime(str(self.date_start)+" 00:00:00",DATETIME_FORMAT)
        date2 = datetime.strptime(str(self.date_end)+" 23:59:59",DATETIME_FORMAT)
        date_start = date1 + timedelta(hours=-7)
        date_end = date2 + timedelta(hours=-7)
        date_start_month = fields.Date.to_string(self.date_end.replace(day=1))
        if self.name_report == 'Report Balance SO' :
            return self.button_turbo_report(date_start,date_start_month,date_end,'so')
        else:
            return self.button_turbo_report(date_start,date_start_month,date_end,'wip')       

    @api.multi
    def generate_balance_wip(self,date_start,date_end):
        src_location_id = self.warehouse_id.lot_stock_id.id
        cr = self.env.cr
        sql = "delete from vit_report_balance_wip where report_id=%s"
        cr.execute(sql, (self.id,) )
        cr.execute(self.sql_wo, (self.company_id.id,date_start, date_end ))
        result = cr.dictfetchall()
        #import pdb;pdb.set_trace()
        jml=0
        line = self.env['vit.report_balance_wip']
        for res in result:
            product = self.env['product.product'].browse(res['product_id'])
            product_location = product.with_context(location=src_location_id)
            qty_available = product_location.qty_available
            onhand = product.uom_id._compute_quantity(qty_available, product.uom_id)    
            #import pdb;pdb.set_trace()
            # heading
            cr.execute(self.sql_heading, (res['product_id'],self.company_id.id, date_start, date_end ))
            heading = cr.dictfetchone()
            heading = heading['sum'] if heading['sum'] else 0
            # rolling
            cr.execute(self.sql_rolling, (res['product_id'],self.company_id.id, date_start, date_end ))
            rolling = cr.dictfetchone()
            rolling = rolling['sum'] if rolling['sum'] else 0
            # furnace
            cr.execute(self.sql_furnace, (res['product_id'],self.company_id.id, date_start, date_end ))
            furnace = cr.dictfetchone()
            furnace = furnace['sum'] if furnace['sum'] else 0
            # plating
            cr.execute(self.sql_plating, (res['product_id'],self.company_id.id, date_start, date_end ))
            plating = cr.dictfetchone()
            plating = plating['sum'] if plating['sum'] else 0
            # Subcon plating
            cr.execute(self.sql_subcon_plating, (res['product_id'],self.company_id.id, date_start, date_end ))
            subcon_plating = cr.dictfetchone()
            subcon_plating = subcon_plating['sum'] if subcon_plating['sum'] else 0
            # fq
            cr.execute(self.sql_fq, (res['product_id'],self.company_id.id, date_start, date_end ))
            fq = cr.dictfetchone()
            fq = fq['sum'] if fq['sum'] else 0
            # machine
            cr.execute(self.sql_machine, (res['product_id'],self.company_id.id, date_start, date_end ))
            machine = cr.dictfetchone()
            machine = machine['sum'] if machine['sum'] else 0
            # tb
            cr.execute(self.sql_tb, (res['product_id'],self.company_id.id, date_start, date_end ))
            tb = cr.dictfetchone()
            tb = tb['sum'] if tb['sum'] else 0
            # washing
            cr.execute(self.sql_washing, (res['product_id'],self.company_id.id, date_start, date_end ))
            washing = cr.dictfetchone()
            washing = washing['sum'] if washing['sum'] else 0
            # packing
            cr.execute(self.sql_packing, (res['product_id'],self.company_id.id, date_start, date_end ))
            packing = cr.dictfetchone()
            packing = packing['sum'] if packing['sum'] else 0
            # subcon_in
            cr.execute(self.sql_subcon_in, (res['product_id'],self.company_id.id, date_start, date_end ))
            subcon_in = cr.dictfetchone()
            # subcon_out
            cr.execute(self.sql_subcon_out, (res['product_id'],self.company_id.id, date_start, date_end ))
            subcon_out = cr.dictfetchone()
            balance = heading+rolling+furnace+plating+subcon_plating+fq+machine+tb+washing+packing
            sql = """
                    INSERT INTO vit_report_balance_wip 
                    (report_id,
                    product_id,
                    onhand,
                    heading,
                    rolling,
                    furnace,
                    plating,
                    subcon_plating,
                    subcon_in,
                    subcon_out,
                    fq,
                    machine,
                    tb,
                    washing,
                    packing,
                    total_wip)
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                    --RETURNING id 
                """%(self.id,res['product_id'],onhand,heading,rolling,furnace,plating,subcon_plating,subcon_in['sum'] if subcon_in['sum'] else 0,subcon_out['sum'] if subcon_out['sum'] else 0,fq,machine,tb,washing,packing,balance)
            self.env.cr.execute(sql)
            jml+=1
            _logger.info("%s sql balance WIP lines sukses dibuat"%str(jml))

    @api.model_cr
    def init(self):
        _logger.info("creating function create report balance...")
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_create_report_balance(balance_id integer,comp_id integer, uid integer, start_date date, date_start_month date,end_date date, report_type varchar)
RETURNS void AS
$BODY$
DECLARE
  product_ids record;
  product record;
  production record;
  balance_line record;
  workorder record;
  heading numeric := 0;
  rolling numeric := 0;
  furnace numeric := 0;
  plating numeric := 0;
  subcon_plating numeric := 0;
  fq numeric := 0;
  machine numeric := 0;
  tb numeric := 0;
  washing numeric := 0;
  packing numeric := 0;
  cutting numeric := 0;
  onhand numeric := 0;
  quant numeric := 0;
  so_bln_ini numeric := 0;
  so_bln_lalu numeric := 0;
  count integer := 0;

BEGIN

for workorder in 
    select 
    foo.product,
    sum(foo.heading) as heading, 
    sum(foo.rolling) as rolling,
    sum(foo.furnace) as furnace,
    sum(foo.plating) as plating,
    sum(foo.subcon_plating) as subcon_plating,
    sum(foo.tb) as tb,
    sum(foo.machine) as machine,
    sum(foo.fq) as fq,
    sum(foo.packing) as packing,
    sum(foo.washing) as washing,
    sum(foo.cutting) as cutting,
    sum(foo.qty) as qty
    from
    (select 
    case when mw.group_report = 'Heading' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as heading,
    case when mw.group_report = 'Rolling' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as rolling,
    case when mw.group_report = 'Furnished' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as furnace,
    case when mw.group_report = 'Plating' and wo.is_subcontracting  = false then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as plating,
    case when mw.group_report = 'Plating' and wo.is_subcontracting  = true then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as subcon_plating,
    case when mw.group_report = 'Three Bond' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as tb,
    case when mw.group_report = 'Machine' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as machine,
    case when mw.group_report = 'Final Quality' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as fq,
    case when mw.group_report = 'Packing' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as packing,
    case when mw.group_report = 'Washing' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as washing,
    case when mw.group_report = 'Cutting' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as cutting,
    wo.product_id as product, (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) as qty, mw.group_report as group_report, wo.is_subcontracting as subcontract
    from mrp_workorder wo
    left join mrp_workcenter mw on wo.workcenter_id = mw.id
    left join mrp_production mp on wo.production_id = mp.id
    where wo.is_wip_report = true and (
        (mw.group_report is not null
            and (wo.is_subcontracting = false or wo.is_subcontracting is null)) 
        or 
        (mw.group_report is not null
            and (wo.is_subcontracting = true))
    )
    and mp.company_id=comp_id
    and mp.state not in ('cancel','done')
    and wo.state = 'done' 
    -- and mp.product_id = 74294
    and wo.product_id in (select 
                            mwo.product_id as product_id
                            from mrp_workorder mwo
                            left join product_product pp on pp.id = mwo.product_id 
                            left join mrp_production mpo on mwo.production_id = mpo.id
                            where mpo.company_id=comp_id
                            and (mwo.qty_produced_real > 0.0 or mwo.qty_produced > 0.0)
                            and (mwo.state = 'done' and mwo.is_wip_report = true and mwo.date_finished between start_date and end_date)
                            or (mwo.state = 'progress' and mwo.is_wip_report = true and mwo.date_start between start_date and end_date)
                            group by mwo.product_id)
    group by wo.product_id,mw.group_report, wo.is_subcontracting) as foo 
    group by foo.product
    loop
        select case when sum(sq.quantity) is not null then sum(sq.quantity) else 0 end from stock_quant sq 
            left join stock_location sl on sl.id = sq.location_id 
            where sq.product_id = workorder.product and sl.usage = 'internal' into quant;
        select case when sum(product_uom_qty - qty_delivered) is not null then sum(product_uom_qty - qty_delivered) else 0 end
            from sale_order_line sol 
            left join sale_order so on sol.order_id = so.id
            where so.state in ('sale','done')
            and sol.delivery_date between date_start_month and end_date
            and sol.product_id = workorder.product
            and so.company_id = comp_id into so_bln_ini;
        select case when sum(product_uom_qty - qty_delivered) is not null then sum(product_uom_qty - qty_delivered) else 0 end
            from sale_order_line sol 
            left join sale_order so on sol.order_id = so.id
            where so.state in ('sale','done')
            and sol.delivery_date < date_start_month
            and sol.product_id = workorder.product
            and so.company_id = comp_id into so_bln_lalu;

        -- INSERT OR CREATE REPORT BALANCE
        IF workorder.heading > 0 or workorder.rolling > 0 or workorder.furnace > 0 or workorder.plating > 0 or workorder.subcon_plating > 0 or workorder.tb > 0 or workorder.machine > 0 or workorder.fq > 0 or workorder.packing > 0 or workorder.washing > 0 or workorder.cutting > 0 THEN
            IF report_type = 'so' THEN
                INSERT INTO vit_report_balance_so("create_uid", "create_date", "write_uid", "write_date", "report_id", "product_id", "onhand","total_so_bulan_ini","total_so_bln_lalu", "heading","rolling","furnace","plating","subcon_plating","tb","machine","fq", "packing", "washing", "cutting", "wip_on_hand", "balance_so")
                    VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), balance_id, workorder.product, quant,so_bln_ini,so_bln_lalu, workorder.heading, workorder.rolling, workorder.furnace, workorder.plating, workorder.subcon_plating, workorder.tb, workorder.machine, workorder.fq, workorder.packing, workorder.washing, workorder.cutting,
                            workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting,
                            quant+workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting-(case when so_bln_lalu >= 0 then so_bln_lalu else so_bln_lalu*-1 end) -(case when so_bln_ini >= 0 then so_bln_ini else so_bln_ini*-1 end)
                            );
                ELSE
                INSERT INTO vit_report_balance_wip("create_uid", "create_date", "write_uid", "write_date", "report_id", "product_id", "onhand", "heading","rolling","furnace","plating","subcon_plating","tb","machine","fq", "packing", "washing", "cutting", "total_wip")
                    VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), balance_id, workorder.product, quant, workorder.heading, workorder.rolling, workorder.furnace, workorder.plating, workorder.subcon_plating, workorder.tb, workorder.machine, workorder.fq, workorder.packing, workorder.washing, workorder.cutting,
                            workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting
                            );
            END IF;
        END IF;
        -- END CREATE
        END LOOP;

END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
        """)

        # company NAI TGR kode mesin < 800
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_create_report_balance_tgr800(balance_id integer,comp_id integer, uid integer, start_date date, date_start_month date,end_date date, report_type varchar)
RETURNS void AS
$BODY$
DECLARE
  product_ids record;
  product record;
  production record;
  balance_line record;
  workorder record;
  heading numeric := 0;
  rolling numeric := 0;
  furnace numeric := 0;
  plating numeric := 0;
  subcon_plating numeric := 0;
  fq numeric := 0;
  machine numeric := 0;
  tb numeric := 0;
  washing numeric := 0;
  packing numeric := 0;
  cutting numeric := 0;
  onhand numeric := 0;
  quant numeric := 0;
  so_bln_ini numeric := 0;
  so_bln_lalu numeric := 0;
  count integer := 0;

BEGIN

for workorder in 
    select 
    foo.product,
    sum(foo.heading) as heading, 
    sum(foo.rolling) as rolling,
    sum(foo.furnace) as furnace,
    sum(foo.plating) as plating,
    sum(foo.subcon_plating) as subcon_plating,
    sum(foo.tb) as tb,
    sum(foo.machine) as machine,
    sum(foo.fq) as fq,
    sum(foo.packing) as packing,
    sum(foo.washing) as washing,
    sum(foo.cutting) as cutting,
    sum(foo.qty) as qty
    from
    (select 
    case when mw.group_report = 'Heading' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as heading,
    case when mw.group_report = 'Rolling' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as rolling,
    case when mw.group_report = 'Furnished' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as furnace,
    case when mw.group_report = 'Plating' and wo.is_subcontracting  = false then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as plating,
    case when mw.group_report = 'Plating' and wo.is_subcontracting  = true then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as subcon_plating,
    case when mw.group_report = 'Three Bond' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as tb,
    case when mw.group_report = 'Machine' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as machine,
    case when mw.group_report = 'Final Quality' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as fq,
    case when mw.group_report = 'Packing' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as packing,
    case when mw.group_report = 'Washing' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as washing,
    case when mw.group_report = 'Cutting' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as cutting,
    wo.product_id as product, (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) as qty, mw.group_report as group_report, wo.is_subcontracting as subcontract
    from mrp_workorder wo
    left join mrp_workcenter mw on wo.workcenter_id = mw.id
    left join mrp_production mp on wo.production_id = mp.id
    left join mrp_machine mm on wo.machine_id = mm.id
    where wo.is_wip_report = true and (
        (mw.group_report is not null
            and (wo.is_subcontracting = false or wo.is_subcontracting is null)) 
        or 
        (mw.group_report is not null
            and (wo.is_subcontracting = true and wo.state in ('ready','pending')))
    )
    and cast(mm.code as int) < 800
    and mp.state not in ('cancel','done')
    -- and wo.state = 'done' 

    -- and mp.product_id = 74294
    and wo.product_id in (select 
                            mwo.product_id as product_id
                            from mrp_workorder mwo
                            left join product_product pp on pp.id = mwo.product_id 
                            left join mrp_production mpo on mwo.production_id = mpo.id
                            left join mrp_machine mrm on mwo.machine_id = mrm.id
                            where cast(mrm.code as int) < 800
                            and (mwo.qty_produced_real > 0.0 or mwo.qty_produced > 0.0)
                            and (mwo.state = 'done' and mwo.is_wip_report = true and mwo.date_finished between start_date and end_date)
                            or (mwo.state = 'progress' and mwo.is_wip_report = true and mwo.date_start between start_date and end_date)
                            group by mwo.product_id)
    group by wo.product_id,mw.group_report, wo.is_subcontracting) as foo 
    group by foo.product
    loop
        select case when sum(sq.quantity) is not null then sum(sq.quantity) else 0 end from stock_quant sq 
            left join stock_location sl on sl.id = sq.location_id 
            where sq.product_id = workorder.product and sl.usage = 'internal' into quant;
        select case when sum(product_uom_qty - qty_delivered) is not null then sum(product_uom_qty - qty_delivered) else 0 end
            from sale_order_line sol 
            left join sale_order so on sol.order_id = so.id
            where so.state in ('sale','done')
            and sol.delivery_date between date_start_month and end_date
            and sol.product_id = workorder.product
            and so.company_id = comp_id into so_bln_ini;
        select case when sum(product_uom_qty - qty_delivered) is not null then sum(product_uom_qty - qty_delivered) else 0 end
            from sale_order_line sol 
            left join sale_order so on sol.order_id = so.id
            where so.state in ('sale','done')
            and sol.delivery_date < date_start_month
            and sol.product_id = workorder.product
            and so.company_id = comp_id into so_bln_lalu;

        -- INSERT OR CREATE REPORT BALANCE
        IF workorder.heading > 0 or workorder.rolling > 0 or workorder.furnace > 0 or workorder.plating > 0 or workorder.subcon_plating > 0 or workorder.tb > 0 or workorder.machine > 0 or workorder.fq > 0 or workorder.packing > 0 or workorder.washing > 0 or workorder.cutting > 0 THEN
            IF report_type = 'so' THEN
                INSERT INTO vit_report_balance_so("create_uid", "create_date", "write_uid", "write_date", "report_id", "product_id", "onhand","total_so_bulan_ini","total_so_bln_lalu", "heading","rolling","furnace","plating","subcon_plating","tb","machine","fq", "packing", "washing", "cutting", "wip_on_hand", "balance_so")
                    VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), balance_id, workorder.product, quant,so_bln_ini,so_bln_lalu, workorder.heading, workorder.rolling, workorder.furnace, workorder.plating, workorder.subcon_plating, workorder.tb, workorder.machine, workorder.fq, workorder.packing, workorder.washing, workorder.cutting,
                            workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting,
                            quant+workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting-(case when so_bln_lalu >= 0 then so_bln_lalu else so_bln_lalu*-1 end) -(case when so_bln_ini >= 0 then so_bln_ini else so_bln_ini*-1 end)
                            );
                ELSE
                INSERT INTO vit_report_balance_wip("create_uid", "create_date", "write_uid", "write_date", "report_id", "product_id", "onhand", "heading","rolling","furnace","plating","subcon_plating","tb","machine","fq", "packing", "washing", "cutting", "total_wip")
                    VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), balance_id, workorder.product, quant, workorder.heading, workorder.rolling, workorder.furnace, workorder.plating, workorder.subcon_plating, workorder.tb, workorder.machine, workorder.fq, workorder.packing, workorder.washing, workorder.cutting,
                            workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting
                            );
            END IF;
        END IF;
        -- END CREATE
        END LOOP;

END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
        """)


        # company NAI BKS kode mesin >= 800
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_create_report_balance_bks800(balance_id integer,comp_id integer, uid integer, start_date date, date_start_month date,end_date date, report_type varchar)
RETURNS void AS
$BODY$
DECLARE
  product_ids record;
  product record;
  production record;
  balance_line record;
  workorder record;
  heading numeric := 0;
  rolling numeric := 0;
  furnace numeric := 0;
  plating numeric := 0;
  subcon_plating numeric := 0;
  fq numeric := 0;
  machine numeric := 0;
  tb numeric := 0;
  washing numeric := 0;
  packing numeric := 0;
  cutting numeric := 0;
  onhand numeric := 0;
  quant numeric := 0;
  so_bln_ini numeric := 0;
  so_bln_lalu numeric := 0;
  count integer := 0;

BEGIN

for workorder in 
    select 
    foo.product,
    sum(foo.heading) as heading, 
    sum(foo.rolling) as rolling,
    sum(foo.furnace) as furnace,
    sum(foo.plating) as plating,
    sum(foo.subcon_plating) as subcon_plating,
    sum(foo.tb) as tb,
    sum(foo.machine) as machine,
    sum(foo.fq) as fq,
    sum(foo.packing) as packing,
    sum(foo.washing) as washing,
    sum(foo.cutting) as cutting,
    sum(foo.qty) as qty
    from
    (select 
    case when mw.group_report = 'Heading' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as heading,
    case when mw.group_report = 'Rolling' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as rolling,
    case when mw.group_report = 'Furnished' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as furnace,
    case when mw.group_report = 'Plating' and wo.is_subcontracting  = false then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as plating,
    case when mw.group_report = 'Plating' and wo.is_subcontracting  = true then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as subcon_plating,
    case when mw.group_report = 'Three Bond' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as tb,
    case when mw.group_report = 'Machine' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as machine,
    case when mw.group_report = 'Final Quality' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as fq,
    case when mw.group_report = 'Packing' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as packing,
    case when mw.group_report = 'Washing' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as washing,
    case when mw.group_report = 'Cutting' then (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) else 0 end as cutting,
    wo.product_id as product, (case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end) as qty, mw.group_report as group_report, wo.is_subcontracting as subcontract
    from mrp_workorder wo
    left join mrp_workcenter mw on wo.workcenter_id = mw.id
    left join mrp_production mp on wo.production_id = mp.id
    left join mrp_machine mm on wo.machine_id = mm.id
    where wo.is_wip_report = true and (
        (mw.group_report is not null
            and (wo.is_subcontracting = false or wo.is_subcontracting is null)) 
        or 
        (mw.group_report is not null
            and (wo.is_subcontracting = true and wo.state in ('ready','pending')))
    )
    and cast(mm.code as int) >= 800
    and mp.state not in ('cancel','done')
    -- and wo.state = 'done' 
    -- and mp.product_id = 74294
    and wo.product_id in (select 
                            mwo.product_id as product_id
                            from mrp_workorder mwo
                            left join product_product pp on pp.id = mwo.product_id 
                            left join mrp_production mpo on mwo.production_id = mpo.id
                            left join mrp_machine mrm on mwo.machine_id = mrm.id
                            where cast(mrm.code as int) >= 800
                            and (mwo.qty_produced_real > 0.0 or mwo.qty_produced > 0.0)
                            and (mwo.state = 'done' and mwo.is_wip_report = true and mwo.date_finished between start_date and end_date)
                            or (mwo.state = 'progress' and mwo.is_wip_report = true and mwo.date_start between start_date and end_date)
                            group by mwo.product_id)
    group by wo.product_id,mw.group_report, wo.is_subcontracting) as foo 
    group by foo.product
    loop
        select case when sum(sq.quantity) is not null then sum(sq.quantity) else 0 end from stock_quant sq 
            left join stock_location sl on sl.id = sq.location_id 
            where sq.product_id = workorder.product and sl.usage = 'internal' into quant;
        select case when sum(product_uom_qty - qty_delivered) is not null then sum(product_uom_qty - qty_delivered) else 0 end
            from sale_order_line sol 
            left join sale_order so on sol.order_id = so.id
            where so.state in ('sale','done')
            and sol.delivery_date between date_start_month and end_date
            and sol.product_id = workorder.product
            and so.company_id = comp_id into so_bln_ini;
        select case when sum(product_uom_qty - qty_delivered) is not null then sum(product_uom_qty - qty_delivered) else 0 end
            from sale_order_line sol 
            left join sale_order so on sol.order_id = so.id
            where so.state in ('sale','done')
            and sol.delivery_date < date_start_month
            and sol.product_id = workorder.product
            and so.company_id = comp_id into so_bln_lalu;

        -- INSERT OR CREATE REPORT BALANCE
        IF workorder.heading > 0 or workorder.rolling > 0 or workorder.furnace > 0 or workorder.plating > 0 or workorder.subcon_plating > 0 or workorder.tb > 0 or workorder.machine > 0 or workorder.fq > 0 or workorder.packing > 0 or workorder.washing > 0 or workorder.cutting > 0 THEN
            IF report_type = 'so' THEN
                INSERT INTO vit_report_balance_so("create_uid", "create_date", "write_uid", "write_date", "report_id", "product_id", "onhand","total_so_bulan_ini","total_so_bln_lalu", "heading","rolling","furnace","plating","subcon_plating","tb","machine","fq", "packing", "washing", "cutting", "wip_on_hand", "balance_so")
                    VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), balance_id, workorder.product, quant,so_bln_ini,so_bln_lalu, workorder.heading, workorder.rolling, workorder.furnace, workorder.plating, workorder.subcon_plating, workorder.tb, workorder.machine, workorder.fq, workorder.packing, workorder.washing, workorder.cutting,
                            workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting,
                            quant+workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting-(case when so_bln_lalu >= 0 then so_bln_lalu else so_bln_lalu*-1 end) -(case when so_bln_ini >= 0 then so_bln_ini else so_bln_ini*-1 end)
                            );
                ELSE
                INSERT INTO vit_report_balance_wip("create_uid", "create_date", "write_uid", "write_date", "report_id", "product_id", "onhand", "heading","rolling","furnace","plating","subcon_plating","tb","machine","fq", "packing", "washing", "cutting", "total_wip")
                    VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), balance_id, workorder.product, quant, workorder.heading, workorder.rolling, workorder.furnace, workorder.plating, workorder.subcon_plating, workorder.tb, workorder.machine, workorder.fq, workorder.packing, workorder.washing, workorder.cutting,
                            workorder.heading+workorder.rolling+workorder.furnace+workorder.plating+workorder.subcon_plating+workorder.tb+workorder.machine+workorder.fq+workorder.packing+workorder.washing+workorder.cutting
                            );
            END IF;
        END IF;
        -- END CREATE
        END LOOP;

END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
        """)


    @api.multi
    def button_turbo_report(self,date_start,date_start_month,date_end,report_type):
        if not self.company_id.second_name :
            raise UserError(_('Second Company name %s belum diisi (NAI TGR atau NAI BKS)!')%(self.company_id.name))
        if report_type == 'so':
            sql = "delete from vit_report_balance_so where report_id=%s"
        else :
            sql = "delete from vit_report_balance_wip where report_id=%s"
        self.env.cr.execute(sql, (self.id,))
        self.env.cr.execute("update mrp_workorder set is_subcontracting = false where is_subcontracting is null")
        if self.company_id.second_name == 'NAI TGR':
            self.env.cr.execute("SELECT vit_create_report_balance_tgr800(%d,%d,%d,'%s','%s','%s','%s')" % (self.id, self.company_id.id , self.env.uid, date_start, date_start_month ,date_end, report_type ))
        elif self.company_id.second_name == 'NAI BKS':
            self.env.cr.execute("SELECT vit_create_report_balance_bks800(%d,%d,%d,'%s','%s','%s','%s')" % (self.id, self.company_id.id , self.env.uid, date_start, date_start_month ,date_end, report_type ))

    # ///////////////// export excel/////////////////
    @api.multi
    def export_excel(self):
        if self.name_report == 'Report Balance SO' :
            return self.export_excel_so()
        else:
            return self.export_excel_wip()
        
    def cell_format(self, workbook):
        cell_format = {}
        cell_format['title'] = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 20,
            'font_name': 'Arial',
        })
        cell_format['header'] = workbook.add_format({
            'bold': True,
            'align': 'center',
            'border': True,
            'font_name': 'Arial',
        })
        cell_format['content'] = workbook.add_format({
            'font_size': 11,
            'border': True,
            'font_name': 'Arial',
        })
        cell_format['content_float'] = workbook.add_format({
            'font_size': 11,
            'border': True,
            'num_format': '#,##0.00',
            'font_name': 'Arial',
        })
        cell_format['total'] = workbook.add_format({
            'bold': True,
            'num_format': '#,##0.00',
            'border': True,
            'font_name': 'Arial',
        })
        return cell_format, workbook
    
    @api.multi
    def export_excel_so(self):
        headers = [
            "No",
            "Product",
            "Code",
            "Total SO Bln. Lalu",
            "Total SO Bln. Ini",
            "Heading",
            "Rolling",
            "Washing",
            "Cutting",
            "Furnace",
            # "Subcon Out",
            # "Subcon In",
            "Plating",
            "Subcount Plating",
            "Subcount TB",
            "Subcount Machining",
            "FQ",
            "Packing",
            "WIP",
            "FG",
            "Balance SO"
        ]

        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        cell_format, workbook = self.cell_format(workbook)

        if not self.report_so_ids :
            raise Warning("Data tidak ditemukan. Mohon Generate Report terlebih dahulu")

        worksheet = workbook.add_worksheet()
        worksheet.set_column('A:ZZ', 30)
        column_length = len(headers)

        ########## parameters
        worksheet.write(0, 4, "REPORT BALANCE SO", cell_format['title'])
        worksheet.write(1, 0, "Tanggal", cell_format['content'])
        worksheet.write(1, 1, self.date_start.strftime("%d-%b-%Y") + ' sampai ' + self.date_end.strftime("%d-%b-%Y"), cell_format['content'])
        worksheet.write(2, 0, "Company", cell_format['content'])
        worksheet.write(2, 1, self.company_id.name , cell_format['content'])

        ########### header
        column = 0
        row = 4
        for col in headers:
            worksheet.write(row, column, col, cell_format['header'])
            column += 1

        ########### contents
        row = 5
        final_data=[]
        no=1
        for data in self.report_so_ids :
            final_data.append([
                no,
                data.product_id.name,
                data.product_code,
                data.total_so_bln_lalu,
                data.total_so_bulan_ini,
                #data.product_id.qty_available,
                data.heading,
                data.rolling,
                data.washing,
                data.cutting,
                data.furnace,
                # data.subcon_out,
                # data.subcon_in,
                data.plating,
                data.subcon_plating,
                data.tb,
                data.machine,
                data.fq,
                data.packing,
                data.wip_on_hand,
                data.onhand,
                data.balance_so,
            ])
            no += 1

        for data in final_data:
            column = 0
            for col in data:
                worksheet.write(row, column, col, cell_format['content'] if column<2 else  cell_format['content_float'])
                column += 1
            row += 1

        workbook.close()
        result = base64.encodestring(fp.getvalue())
        filename = self.name_report + '-' + self.company_id.name + '%2Exlsx'
        self.write({'data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    @api.multi
    def export_excel_wip(self):

        headers = [
            "No",
            "Product",
            "Code",
            #"On Hand",
            "Heading",
            "Rolling",
            "Washing",
            "Cutting",
            "Furnace",
            "Plating",
            "Subcount Plating",
            "Subcount TB",
            "Subcount Machining",
            # "Subcon Out",
            # "Subcon In",
            "FQ",
            "Packing",
            "Total WIP",
        ]

        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        cell_format, workbook = self.cell_format(workbook)

        if not self.report_wip_ids :
            raise Warning("Data tidak ditemukan. Mohon Generate Report terlebih dahulu")

        worksheet = workbook.add_worksheet()
        worksheet.set_column('A:ZZ', 30)
        column_length = len(headers)

        ########## parameters
        worksheet.write(0, 4, "REPORT BALANCE WIP", cell_format['title'])
        worksheet.write(1, 0, "Tanggal", cell_format['content'])
        worksheet.write(1, 1, self.date_start.strftime("%d-%b-%Y") + ' sampai ' + self.date_end.strftime("%d-%b-%Y"), cell_format['content'])
        worksheet.write(2, 0, "Company", cell_format['content'])
        worksheet.write(2, 1, self.company_id.name , cell_format['content'])

        ########### header
        column = 0
        row = 4
        for col in headers:
            worksheet.write(row, column, col, cell_format['header'])
            column += 1

        ########### contents
        row = 5
        final_data=[]
        no=1
        for data in self.report_wip_ids :
            final_data.append([
                no,
                data.product_id.name,
                data.product_code,
                #data.product_id.qty_available,
                data.heading,
                data.rolling,
                data.washing,
                data.cutting,
                data.furnace,
                data.plating,
                data.subcon_plating,
                # data.subcon_out,
                # data.subcon_in,
                data.tb,
                data.machine,
                data.fq,
                data.packing,
                # data.total_wip,
                data.wip_on_hand,
            ])
            no += 1

        for data in final_data:
            column = 0
            for col in data:
                worksheet.write(row, column, col, cell_format['content'] if column<2 else  cell_format['content_float'])
                column += 1
            row += 1

        workbook.close()
        result = base64.encodestring(fp.getvalue())
        filename = self.name_report + '-' + self.company_id.name + '%2Exlsx'
        self.write({'data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }


