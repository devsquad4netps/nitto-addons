#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class report_balance_so(models.Model):
    _name = "vit.report_balance_so"
    _description = "Report Balance lines SO"
    

    product_code = fields.Char( string="Product code",related='product_id.default_code')
    total_so_bln_lalu = fields.Float( string="Total SO bln lalu",digits=(16, 0))
    total_so_bulan_ini = fields.Float( string="Total SO bulan ini",digits=(16, 0))
    heading = fields.Float( string="Heading",digits=(16, 0))
    rolling = fields.Float( string="Rolling",digits=(16, 0))
    furnace = fields.Float( string="Furnace",digits=(16, 0))
    plating = fields.Float( string="Plating",digits=(16, 0))
    subcon_plating = fields.Float( string="Subcount Plating",digits=(16, 0))
    fq = fields.Float( string="FQ",digits=(16, 0))
    subcon_out = fields.Float( string="Subcon Out",digits=(16, 0))
    subcon_in = fields.Float( string="Subcon In",digits=(16, 0))
    machine = fields.Float( string="Machine",digits=(16, 0))
    cutting = fields.Float( string="Cutting",digits=(16, 0))
    tb = fields.Float( string="Three Bond",digits=(16, 0))
    washing = fields.Float( string="Washing",digits=(16, 0))
    packing = fields.Float( string="Packing",digits=(16, 0))
    wip_on_hand = fields.Float( string="WIP",digits=(16, 0))
    balance_so = fields.Float( string="Balance",digits=(16, 0))
    onhand = fields.Float( string="On hand",digits=(16, 0))

    report_id = fields.Many2one(comodel_name="vit.report_balance",  string="Report",  help="")
    product_id = fields.Many2one(comodel_name="product.product",  string="Product Name",  help="")
    
    
class report_balance_wip(models.Model):
    _name = "vit.report_balance_wip"
    _description = "Report Balance lines WIP"
    
    product_code = fields.Char( string="Product code", related='product_id.default_code')
    onhand = fields.Float( string="On hand",digits=(16, 0))
    heading = fields.Float( string="Heading",digits=(16, 0))
    rolling = fields.Float( string="Rolling",digits=(16, 0))
    furnace = fields.Float( string="Furnace",digits=(16, 0))
    plating = fields.Float( string="Plating",digits=(16, 0))
    subcon_plating = fields.Float( string="Subcount Plating",digits=(16, 0))
    fq = fields.Float( string="FQ",digits=(16, 0))
    subcon_out = fields.Float( string="Subcon Out",digits=(16, 0))
    subcon_in = fields.Float( string="Subcon In",digits=(16, 0))
    machine = fields.Float( string="Machine",digits=(16, 0))
    cutting = fields.Float( string="Cutting",digits=(16, 0))
    tb = fields.Float( string="Three Bond",digits=(16, 0))
    washing = fields.Float( string="Washing",digits=(16, 0))
    packing = fields.Float( string="Packing",digits=(16, 0))
    total_wip = fields.Float( string="Total WIP",digits=(16, 0))

    wip_on_hand = fields.Float( string="WIP",digits=(16, 0))    
    report_id = fields.Many2one(comodel_name="vit.report_balance",  string="Report",)
    product_id = fields.Many2one(comodel_name="product.product",  string="Product Name",)

    