from odoo import api, fields, models

class MRPWorkcenter(models.Model):
      _inherit = "mrp.workcenter"

      #group_report = fields.Selection([('CF','CF'),('Furnished','Furnished'),('Final Quality','Final Quality'),('Heading','Heading'),('Machine','Machine'),('Packing','Packing'),('Plating','Plating'),('Rolling','Rolling'),('Three Bond','Three Bond'),('Washing','Washing')],"Group")
      group_report = fields.Selection([('CF','CF'),                                   
                                    ('Heading','Heading'),
                                    ('Machine','Machining'),
                                    ('Rolling','Rolling'),
                                    ('Trimming','Trimming'),
                                    ('Sloting','Sloting'),
                                    ('Cutting','Cutting'),
                                    ('Washing','Washing'),
                                    ('Furnished','Furnace'),
                                    ('Plating','Plating'),
                                    ('Final Quality','Final Quality'),
                                    ('Three Bond','Three Bond'),
                                    ('Packing','Packing')],"Group")
      is_wip_report = fields.Boolean('Is WiP Report', copy=False, default=False)

MRPWorkcenter()