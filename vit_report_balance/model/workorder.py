from odoo import api, fields, models

class MRPWorkorder(models.Model):
    _inherit = "mrp.workorder"

    group_report = fields.Selection(related="workcenter_id.group_report",string="Group",store=True)
    is_wip_report = fields.Boolean('Is WiP Report', copy=False, default=False)

    @api.multi
    def button_subcontractor(self):
        res = super(MRPWorkorder, self).button_subcontractor()
        for x in self:
            if not x.is_wip_report :
                cr = self.env.cr
                try:
                    cr.execute("UPDATE mrp_workorder SET is_wip_report=false WHERE production_id",(x.production_id.id))
                    cr.execute("UPDATE mrp_workorder SET is_wip_report=true WHERE id=%s",(x.id))
                except:
                    pass
        return res

MRPWorkorder()