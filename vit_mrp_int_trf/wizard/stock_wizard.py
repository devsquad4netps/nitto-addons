# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class MrpInternalTransfer(models.TransientModel):
    _name = 'mrp.internal.transfer'
    _description = 'Internal Transfer MRP'

    @api.model
    def _get_default_picking_id(self):
        return self._context.get('active_id')

    @api.onchange('picking_ids')
    def _onchange_picking_ids(self):
        domain = {}
        if self.picking_id:
            domain = {'domain': {'picking_ids': [('location_dest_id', '=', self.picking_id.location_id.id),
                                                    ('state','=','done'),('internal_transfer_mrp','=',False)]}}
            return domain

    picking_ids = fields.Many2many('stock.picking', string='Document Number',required=True )
    picking_id = fields.Many2one('stock.picking', string='Picking', default=_get_default_picking_id)

    @api.multi
    def insert_lines(self):
        self.ensure_one()
        #import pdb;pdb.set_trace()
        data = []
        for pick in self.picking_ids :
            if not pick.scheduled_date :
                dates = str(date.today())
            else :
                dates = pick.scheduled_date
            for mv in pick.move_ids_without_package:
                data.append((0,0, {'product_id':mv.product_id.id,
                                    'name': pick.name,
                                    'product_uom':mv.product_uom.id,
                                    'product_uom_qty':mv.product_uom_qty,
                                    'netto':mv.netto,
                                    'brutto':mv.brutto,
                                    'state':'draft',
                                    'picking_type_id':pick.picking_type_id.id,
                                    'location_id':pick.location_id.id,
                                    'location_dest_id':pick.location_dest_id.id,
                                    'company_id': pick.company_id.id,
                                    'date_expected':dates,
                                    'picking_internal_id':pick.id,
                                    'move_internal_id':mv.id
                                    })) 
        self.picking_id.move_ids_without_package = data

MrpInternalTransfer()