# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Internal Transfer from Production',
    'website': 'http://www.vitraining.com',
    'author': 'vITraining',
    'version': '12.0.1.0.0',
    'support': 'widianajuniar@gmail.com',
    'license': 'AGPL-3',
    'images': [],
    'category': 'Stock',
    'sequence': 14,
    'summary': 'Create internal transfer from incoming production',
    'depends': ['base','mrp','stock','vit_mrp_cost'],
    'description': " Fitur untuk internal transfer banyak dokumen RP(Receipt from Productin)",
    'data': [
        'wizard/stock_wizard.xml',
        'views/stock.xml',
    ],
    'test': [],
    'application': True,
}
