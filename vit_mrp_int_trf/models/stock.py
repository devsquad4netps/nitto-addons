# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning
from odoo.addons import decimal_precision as dp


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    def action_internal_trf_wizard(self):
        action = self.env.ref('vit_mrp_int_trf.action_internal_transfer_mrp').read()[0]
        return action

    is_int_trf = fields.Boolean(related='picking_type_id.is_int_trf', string='Internal Transfer MRP', copy=False, store=True)
    move_internal_id = fields.Many2one('stock.move','Move Internal')
    internal_transfer_mrp = fields.Char(related='move_internal_id.picking_id.name',string='Internal Transfer MRP', store=True)
    
    @api.multi
    def action_assign(self):
        res = super(StockPicking, self).action_assign()
        if self.is_int_trf :
            # looping lagi untuk dapat lot, qty done, bruto, netto dan result package
            for int_trf in self.move_ids_without_package :
                if int_trf.move_internal_id :
                    seq_int = 0
                    for x in int_trf.move_line_ids:
                        seq_int += 1
                        seq_kbn = 0
                        for y in int_trf.move_internal_id.move_line_ids:
                            seq_kbn += 1
                            if y.lot_id and seq_int == seq_kbn:
                                x.lot_id = y.lot_id.id
                                x.qty_done = y.qty_done
                                x.netto = y.netto
                                x.bruto = y.bruto
                                x.result_package_id = y.result_package_id.id
                                break
        return res

StockPicking()


class StockMove(models.Model):
    _inherit = 'stock.move'
    
    picking_internal_id = fields.Many2one('stock.picking','Int Trf Origin')
    move_internal_id = fields.Many2one('stock.move','Move Int Trf Origin')

    @api.model
    def create(self, vals):
        move = super(StockMove, self).create(vals)
        if move.picking_internal_id :
            move.picking_internal_id.move_internal_id = move.id
        return move

StockMove()


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'
    
    is_int_trf = fields.Boolean('Internal Transfer MRP', copy=False)

StockPickingType()