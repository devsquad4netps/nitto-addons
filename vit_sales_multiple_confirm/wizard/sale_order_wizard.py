from odoo import models, fields, api, tools, _

import logging

_logger = logging.getLogger(__name__)


class SaleOrderConfirmationWizard(models.TransientModel):
    _name = "sale.order.confirmation.wizard"

    def confirm_orders(self):
        order_ids = self.env.context.get('active_ids', [])
        orders = self.env['sale.order'].browse(order_ids)
        for order in orders:
            if order.state == 'draft' or order.state == 'sent':
                self.confirm(order)

    def confirm(self, order):
        return order.action_confirm()
