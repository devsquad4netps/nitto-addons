{
    'name': "Sales Order Multiple Confirm",
    'version': '0.1',
    'author': '',
    'category': 'Sales',
    'depends': ['sale_management'],
    'data': [
        "views/sale_views.xml",
        "wizard/sale_order_wizard.xml",
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}
