{
    "name"          : "Split Picking DO",
    "version"       : "1.0",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "Account",
    "summary"       : "Split picking DO sesuai jumlah lines di SO",
    "description"   : """
        
    """,
    "depends"       : [
        "sale_stock"
    ],
    "data"          : [

    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}