from odoo import fields, models, api
from odoo.exceptions import Warning
import time
from datetime import datetime


class VitDefault(models.Model):
    _name = "vit.default"
    _description = "Set Default Value"
    _rec_name = "model_id"

    model_id = fields.Many2one('ir.model', string='Object', required=True)
    model = fields.Char(string='Model', related='model_id.model')
    company_id = fields.Many2one('res.company', string='Company')
    receivable_id = fields.Many2one('account.account', string='Account Receivable', domain=[('internal_type','=','receivable')])
    payable_id = fields.Many2one('account.account', string='Account Payable', domain=[('internal_type','=','payable')])
    taxes_id = fields.Many2many('account.tax', 'default_taxes_rel', 'default_id', 'tax_id', string='Customer Taxes',
        domain=[('type_tax_use', '=', 'sale')])
    supplier_taxes_id = fields.Many2many('account.tax', 'default_supplier_taxes_rel', 'default_id', 'tax_id', string='Vendor Taxes',
        domain=[('type_tax_use', '=', 'purchase')])

    @api.onchange('company_id')
    def filter_company(self):
        domain = {}
        if self.company_id :
            receivable_ids = self.env['account.account'].search([
                ('company_id','=',self.company_id.id),
                ('internal_type','=','receivable'),
            ])
            domain['receivable_id'] = [('id','in',receivable_ids.ids)]
            payable_ids = self.env['account.account'].search([
                ('company_id','=',self.company_id.id),
                ('internal_type','=','payable'),
            ])
            domain['payable_id'] = [('id','in',payable_ids.ids)]
            taxes_id = self.env['account.tax'].search([
                ('company_id','=',self.company_id.id),
                ('type_tax_use', '=', 'sale'),
            ])
            domain['taxes_id'] = [('id','in',taxes_id.ids)]
            supplier_taxes_id = self.env['account.tax'].search([
                ('company_id','=',self.company_id.id),
                ('type_tax_use', '=', 'purchase'),
            ])
            domain['supplier_taxes_id'] = [('id','in',supplier_taxes_id.ids)]
        else :
            receivable_ids = self.env['account.account'].search([
                ('internal_type','=','receivable')
            ])
            domain['receivable_id'] = [('id','in',receivable_ids.ids)]
            payable_ids = self.env['account.account'].search([
                ('internal_type','=','payable'),
            ])
            domain['payable_id'] = [('id','in',payable_ids.ids)]
            taxes_id = self.env['account.tax'].search([
                ('type_tax_use', '=', 'sale'),
            ])
            domain['taxes_id'] = [('id','in',taxes_id.ids)]
            supplier_taxes_id = self.env['account.tax'].search([
                ('type_tax_use', '=', 'purchase'),
            ])
            domain['supplier_taxes_id'] = [('id','in',supplier_taxes_id.ids)]
        return {'domain':domain}

VitDefault()


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def default_get(self, fields):
        res = super(ResPartner, self).default_get(fields)
        obj_default = self.env['vit.default']
        if not res.get('property_account_receivable_id', False):
            default_id = obj_default.search([
                ('company_id','=',self.env.user.company_id.id),
                ('model_id.model','=',self._name),
            ], limit=1)
            if not default_id :
                default_id = obj_default.search([
                    ('company_id','=',False),
                    ('model_id.model','=',self._name),
                ], limit=1)
            if default_id :
                res['property_account_receivable_id'] = default_id.receivable_id.id
        if not res.get('property_account_payable_id', False):
            default_id = obj_default.search([
                ('company_id','=',self.env.user.company_id.id),
                ('model_id.model','=',self._name),
            ], limit=1)
            if not default_id :
                default_id = obj_default.search([
                    ('company_id','=',False),
                    ('model_id.model','=',self._name),
                ], limit=1)
            if default_id :
                res['property_account_payable_id'] = default_id.payable_id.id
        return res

ResPartner()


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def default_get(self, fields):
        res = super(ProductProduct, self).default_get(fields)
        obj_default = self.env['vit.default']
        if not res.get('taxes_id', False) or res.get('taxes_id', False) == [(6, 0, [])]:
            default_id = obj_default.search([
                ('company_id','=',self.env.user.company_id.id),
                ('model_id.model','=',self._name),
            ], limit=1)
            if not default_id :
                default_id = obj_default.search([
                    ('company_id','=',False),
                    ('model_id.model','=',self._name),
                ], limit=1)
            if default_id and default_id.taxes_id :
                res['taxes_id'] = [(6, 0, default_id.taxes_id.ids)]
        if not res.get('supplier_taxes_id', False) or res.get('supplier_taxes_id', False) == [(6, 0, [])]:
            default_id = obj_default.search([
                ('company_id','=',self.env.user.company_id.id),
                ('model_id.model','=',self._name),
            ], limit=1)
            if not default_id :
                default_id = obj_default.search([
                    ('company_id','=',False),
                    ('model_id.model','=',self._name),
                ], limit=1)
            if default_id and default_id.supplier_taxes_id :
                res['supplier_taxes_id'] = [(6, 0, default_id.supplier_taxes_id.ids)]
        return res

ProductProduct()