{
    "name"          : "Default Value",
    "version"       : "1.0",
    "author"        : "vITraining - [Miftahussalam]",
    "website"       : "https://vitraining.com/",
    "category"      : "General",
    "summary"       : "Set Default Value",
    "description"   : """
        
    """,
    "depends"       : [
        "product","stock",
    ],
    "data"          : [
        "views/vit_default_view.xml",
        "security/ir.model.access.csv"
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}