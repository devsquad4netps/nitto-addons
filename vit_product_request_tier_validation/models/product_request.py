# Copyright 2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models


class ProductRequest(models.Model):
    _name = "vit.product.request"
    _inherit = ['vit.product.request', 'tier.validation']
    _state_from = ['draft']
    _state_to = ['open']
