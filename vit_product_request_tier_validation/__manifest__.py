# Copyright 2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Product Request Tier Validation",
    "summary": "Extends the functionality of Product Request to "
               "support a tier validation process.",
    "version": "12.0.1.0.0",
    "category": "Purchases",
    "website": "https://vitraining.com",
    "author": "vITraining",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "vit_product_request",
        "base_tier_validation",
    ],
    "data": [
        "views/product_request_view.xml",
    ],
}
