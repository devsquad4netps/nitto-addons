# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    subcon_process_id = fields.Many2one('vit.subcon.process','Subcon Process')

StockPicking()