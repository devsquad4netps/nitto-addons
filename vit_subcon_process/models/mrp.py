# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class MRPWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    production_report = fields.Boolean('Production Report', default=False)
    subcon_process = fields.Selection([('CF','CF'),                                   
                                    ('Heading','Heading'),
                                    ('Machine','Machining'),
                                    ('Rolling','Rolling'),
                                    ('Trimming','Trimming'),
                                    ('Sloting','Sloting'),
                                    ('Cutting','Cutting'),
                                    ('Washing','Washing'),
                                    ('Furnished','Furnace'),
                                    ('Plating','Plating'),
                                    ('Final Quality','Final Quality'),
                                    ('Three Bond','Three Bond'),
                                    ('Packing','Packing')],"Group Subcon Process")

MRPWorkcenter()


class MRPWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    subcon_process_id = fields.Many2one('vit.subcon.process','Subcon Process',copy=False)

MRPWorkorder()