from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby



class VitSubconProcess(models.Model):
    _name           = "vit.subcon.process"
    _inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
    _description    = 'Subcon Process External'
    _order          = 'name desc'

    # @api.onchange('partner_subcon_id')
    # def onchange_partner_subcon_id(self):
    #     if self :
    #         if self.partner_subcon_id:
    #             subcon = self.env['stock.move'].sudo().search([('picking_id.partner_id','=',self.partner_subcon_id.id),
    #                                                             ('picking_id.is_incoming_subcon','=',False),
    #                                                             ('picking_id.is_subcontracting','=',True),
    #                                                             ('production_subcon_id','!=',False),
    #                                                             ('state','not in',('done','cancel'))
    #                                                             ])
    #             data = []
    #             mo = []
    #             for move in subcon :
    #                 if move.production_subcon_id.id not in mo:
    #                     wos = move.production_subcon_id.workorder_ids.filtered(lambda w:w.is_wip == True)
    #                     data.append((0,0, {'production_id':move.production_subcon_id.id,
    #                                         'workorder_id':wos[0].id,
    #                                         'product_id':move.production_subcon_id.product_id.id,
    #                                         'workcenter_id':wos[0].workcenter_id.id,
    #                                         'quantity':move.production_subcon_id.product_qty,
    #                                         'note':wos[0].workcenter_id.name+' '+ wos[0].state}))
    #                     mo.append(move.production_subcon_id.id)
    #             self.subcon_process_ids = data

    @api.onchange('picking_id')
    def onchange_picking_id(self):
        if self :
            if self.picking_id:
                data = []
                for move in self.picking_id.move_ids_without_package :
                    wos = move.production_subcon_id.workorder_ids.filtered(lambda w:w.is_wip == True)
                    if wos :
                        data.append((0,0, {'production_id':move.production_subcon_id.id,
                                            'workorder_id':wos[0].id,
                                            'product_id':move.production_subcon_id.product_id.id,
                                            'workcenter_id':wos[0].workcenter_id.id,
                                            'quantity':move.production_subcon_id.product_qty,
                                            'note':wos[0].workcenter_id.name+' : '+ wos[0].state}))
                self.subcon_process_ids = data

    @api.onchange('group_report')
    def onchange_group_report(self):
        if self and self.group_report:
            self.subcon_process_ids = False
            data = []
            for move in self.picking_id.move_ids_without_package :
                # wos = move.production_subcon_id.workorder_ids.filtered(lambda w:w.workcenter_id.subcon_process == self.group_report)
                # for wo in wos :
                #     state = dict(wo._fields['state'].selection).get(wo.state)
                #     data.append((0,0, {'workorder_id':wo.id,
                #                         'workcenter_id':wo.workcenter_id.id,
                #                          }))
                data.append((0,0,{'production_id':move.production_subcon_id.id,
                                            'product_id':move.production_subcon_id.product_id.id,
                                            'quantity':move.production_subcon_id.product_qty}))

            self.subcon_process_ids = data


    name = fields.Char('Number',required=True, default='/')
    date = fields.Date('Date', required=True, track_visibility='onchange',default=fields.Date.context_today,readonly=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)    
    partner_subcon_id = fields.Many2one('res.partner','Partner',readonly=True,states={'draft': [('readonly', False)]})
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm'),('done','Done')],default='draft', track_visibility='onchange', string="State")
    notes = fields.Text('Notes', track_visibility='onchange')
    picking_id = fields.Many2one('stock.picking','Subcon In',readonly=True,states={'draft': [('readonly', False)]})
    subcon_process_ids = fields.One2many('vit.subcon.process.line', 'subcon_process_id', 'Details',readonly=True,states={'draft': [('readonly', False)]})
    group_report = fields.Selection([('CF','CF'),                                   
                                    ('Heading','Heading'),
                                    ('Machine','Machining'),
                                    ('Rolling','Rolling'),
                                    ('Trimming','Trimming'),
                                    ('Sloting','Sloting'),
                                    ('Cutting','Cutting'),
                                    ('Washing','Washing'),
                                    ('Furnished','Furnace'),
                                    ('Plating','Plating'),
                                    ('Final Quality','Final Quality'),
                                    ('Three Bond','Three Bond'),
                                    ('Packing','Packing')],"Group WO",readonly=True,states={'draft': [('readonly', False)]})

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            number = self.env['ir.sequence'].next_by_code('vit.subcon.process')
            if number :
                vals['name'] = number
        elif 'name' not in vals :
            number = self.env['ir.sequence'].next_by_code('vit.subcon.process')
            if number :
                vals['name'] = number
            else :
                vals['name'] = '#'
        return super(VitSubconProcess, self).create(vals)


    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(VitSubconProcess, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'
        self.picking_id.subcon_process_id = False

    @api.multi
    def action_confirm(self):
        if self.picking_id.subcon_process_id and self.picking_id.subcon_process_id.id != self.id :
            raise UserError(_('Dokumen %s sudah di assign ke subcon proses %s ')%(self.picking_id.name,self.picking_id.subcon_process_id.name))
        self.picking_id.subcon_process_id = self.id
        self.state = 'confirm'

    @api.multi
    def action_done(self):
        if self.picking_id.subcon_process_id and self.picking_id.subcon_process_id.id != self.id :
            raise UserError(_('Dokumen %s sudah di assign ke subcon proses %s !')%(self.picking_id.name,self.picking_id.subcon_process_id.name))
        self.picking_id.subcon_process_id = self.id
        self.state = 'done'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        self.picking_id.subcon_process_id = False


    @api.multi
    def action_execute_lines(self):
        to_execute = self.subcon_process_ids.filtered(lambda exe:not exe.executed)
        if not to_execute :
            raise UserError(_('Data sudah di eksekusi semua !'))
        for line in to_execute :
            line.button_execute()
        if self.state != 'done' :
            self.action_done()

VitSubconProcess()


class VitSubconProcessLine(models.Model):
    _name           = "vit.subcon.process.line"
    _description    = 'Subcon Process Line'

    @api.multi
    def button_turbo_finish_subcon(self,workorder):
        self.ensure_one()
        #previous_wo = self.env['mrp.workorder'].sudo().search([('next_work_order_id','=',workorder.id)],limit=1)
        # if previous_wo and previous_wo.state not in ('done','cancel'):
        #     if not self.env.user.has_group('vit_workorder.group_force_finish_previous_wo') :
        #         routings = workorder.production_id.workorder_ids.sorted('id')
        #         rout = ''
        #         disini = ''
        #         no = 1
        #         for r in routings:
        #             state = dict(wo._fields['state'].selection).get(r.state)
        #             if r.id == previous_wo.id :
        #                 disini = ' <----'
        #             rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
        #             disini = ''
        #             no += 1
        #         raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
            #raise UserError(_("Work Order sebelumnya belum selesai (%s)")%(previous_wo.name))
        self.env.cr.execute("SELECT vit_done_wo(%s,%s,%s)" % (workorder.id, workorder.production_id.id, self.env.uid) )

        # update done qty barang jadi di moveline
        if not workorder.next_work_order_id :
            picking_id = self.env['stock.picking'].sudo().search([('group_id','=',workorder.production_id.procurement_group_id.id),
                                                            ('location_id.usage','=','production')],limit=1)
            if picking_id :
                picking_id.action_confirm()
                picking_id.action_assign()

        # update done qty barang jadi di moveline
        if not workorder.next_work_order_id:
            move = self.env['stock.move'].sudo().search([('group_id','=',workorder.production_id.procurement_group_id.id),
                                                            ('location_id.usage','=','production'),
                                                            ('production_id','=',workorder.production_id.id)],limit=1)
            if move :
                if move.state != 'assigned' :
                    move.picking_id.action_assign()
                for line in move.move_line_ids :
                    line.qty_done = self.quantity
                    line.lot_id = workorder.final_lot_id.id
                    if not line.picking_id :
                        line.picking_id = move.picking_id.id
        return True


    @api.multi
    def button_execute(self):
        for exe in self:
            workorder = exe.production_id.workorder_ids.filtered(lambda x:x.workcenter_id.subcon_process == exe.subcon_process_id.group_report)
            if workorder :
                all_done = True
                for wo in workorder.sorted('id'):
                    if wo.state in ('pending','ready'):
                        wo.button_start()
                        exe.button_turbo_finish_subcon(wo)
                    elif wo.state == 'progress':
                        exe.button_turbo_finish_subcon(wo)
                    try:
                        cr.execute("UPDATE mrp_workorder SET subcon_process_id=%s WHERE id=%s",(exe.subcon_process_id.id,wo.id))
                    except:
                        pass
                    self.env.cr.commit()
                    if wo.state != 'done':
                        all_done = False
                if all_done :
                    exe.write({'note2' : exe.note2 +'\n'+'.'})
                #exe.get_wo_info()
                pendingan = exe.subcon_process_id.subcon_process_ids.filtered(lambda sub:not sub.executed)
                if not pendingan:
                    exe.subcon_process_id.action_done()

    @api.multi
    def button_start(self):
        self.workorder_id.button_start()

    @api.multi
    def record_production(self):
        self.workorder_id.record_production()

    @api.multi
    def button_turbo_finish(self):
        self.workorder_id.button_turbo_finish()
        if not self.subcon_process_id.subcon_process_ids.filtered(lambda sub:sub.workorder_id.state != 'done'):
            self.subcon_process_id.action_done()


    subcon_process_id = fields.Many2one('vit.subcon.process','Subcon Process',ondelete='cascade')
    production_id = fields.Many2one('mrp.production','Production')
    product_id = fields.Many2one('product.product', 'Product')
    quantity = fields.Float('Quantity')
    partner_id = fields.Many2one('res.partner', 'Customer', related='subcon_process_id.partner_subcon_id', store=True)
    note = fields.Text('Workorders',compute='get_wo_by_group_report')
    note2 = fields.Text('Status',compute='get_wo_by_group_report')
    executed = fields.Boolean('Executed',compute='get_wo_info',store=True)
    state = fields.Selection(string='Process Status', related='subcon_process_id.state',store=True)
    workorder_ids = fields.One2many('mrp.workorder','Workorder', related='production_id.workorder_ids')

    @api.depends('production_id','production_id.workorder_ids.state','subcon_process_id')
    def get_wo_by_group_report(self):
        for i in self:
            description = ''
            description2 = ''
            workorder = i.production_id.workorder_ids.filtered(lambda x:x.workcenter_id.subcon_process == i.subcon_process_id.group_report)
            if workorder :
                for wo in workorder.sorted('id'):
                    state = dict(wo._fields['state'].selection).get(wo.state)
                    description += wo.name+'\n'
                    description2 += state+'\n'
            i.note = description
            i.note2 = description2

    @api.depends('production_id','production_id.workorder_ids.state','state','note2')
    def get_wo_info(self):
        for i in self:
            executed = True
            workorder = i.production_id.workorder_ids.filtered(lambda x:x.workcenter_id.subcon_process == i.subcon_process_id.group_report)
            if workorder :
                for wo in workorder:
                    if wo.state != 'done' :
                        executed = False
                        break
            i.executed = executed


VitSubconProcessLine()