{
    "name"          : "External Subcon Process",
    "version"       : "0.7",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "MRP",
    "summary"       : "Eksekusi banyak WO subcon dalam satu waktu bedasarkan Subcon In",
    "description"   : """
        
    """,
    "depends"       : [
        "mrp","vit_subcontractor","vit_turbo_mrp","vit_report_balance"
    ],
    "data"          : [
        "security/group.xml",
        "security/ir.model.access.csv",
        "data/data.xml",
        "views/subcon_process_view.xml",
        "views/mrp_view.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}