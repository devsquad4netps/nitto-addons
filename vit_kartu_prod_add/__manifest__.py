# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2017  Odoo SA  (http://www.vitraining.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Report Manufacture",
    "version": "4.8",
    "category": "Extra Tools",
    "sequence": 90,
    "author":  "vITraining",
    "website": "www.vitraining.com",
    "license": "AGPL-3",
    "summary": "",
    "description": """
    * Report Kartu Produksi

    """,
    "depends": [
        "base",
        "stock",
        "mrp",
        "vit_mrp_reprocess"
        # "vit_do_add",
        # "sale_management",
        # "product_customer_code",
    ],
    "data": [
        "views/mo.xml",
        "views/product.xml",
        "views/partner.xml",
        "views/color.xml",
        "views/template_print_kartu_prod.xml",
        "views/template_print_kartu_prod_1.xml",
        "views/template_print_kartu_prod_2.xml",
        "views/template_print_kartu_prod_3.xml",
        "security/ir.model.access.csv"
    ],

    "demo": [
    ],

    "test": [
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}