from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
import pdb

class production(models.Model):
    _inherit = 'mrp.production'

    @api.model
    def create(self, vals):
        if 'code_cust_id' not in vals :
            if 'customer_id' in vals :
                product_cust_obj= self.env['product.customer.code']
                cust_code = product_cust_obj.sudo().search([('company_id','=',vals['company_id']),
                                                        ('partner_id','=',vals['customer_id']),
                                                        ('product_id','=',vals['product_id'])],limit=1)
                if cust_code :
                    vals['code_cust_id'] = cust_code.id
        return super(production, self).create(vals)
        

    customer_id = fields.Many2one('res.partner', 'Customer', help='Customer MO ini')
    code_cust_id = fields.Many2one('product.customer.code', 'Code Product', help='kode product versi customer')
    warna           = fields.Char("Warna",related="product_id.warna", store=True)
    no_poly_box = fields.Char(string="No Poly Box",size=3, required=True)
    jml_box = fields.Float(string="Jumlah Box", compute='_compute_jml_box',store=True)

    @api.depends('state','no_poly_box','product_id.kg_pal','workorder_ids','workorder_ids.state')
    def _compute_jml_box(self):
        for mo in self:
            if mo.no_poly_box :
            	box = mo.no_poly_box[2:]
            	if box :
            		try :
            			mo.jml_box = float(box)
            		except:
            			mo.jml_box = 0.0


    @api.onchange('customer_id')
    def onchange_template_id(self):
        prod_code_ids = self.env['product.customer.code'].sudo().search([('product_id', '=', self.product_id.id),('partner_id', '=', self.customer_id.id)], limit=1).id
        if prod_code_ids:
            self.code_cust_id = prod_code_ids