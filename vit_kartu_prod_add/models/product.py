from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
import pdb

class ProductColor(models.Model):
    _name = 'product.color'

    @api.multi
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.code :
                name = res.name+' ['+res.code+']'
            result.append((res.id, name))
        return result

    name         = fields.Char("Name", required=True)
    code         = fields.Char("Code",)
    note         = fields.Char("Notes",)

    _sql_constraints = [
        ('default_code_uniq', 'unique (code)', 'The code of color must be unique !'),
    ]

    @api.onchange('name')
    def onchange_warna(self):
        if self.name :
            self.name = self.name.upper()


ProductColor()

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    plt             = fields.Char("PLT", track_visibility='on_change')
    lt              = fields.Float("LT", track_visibility='on_change')
    no_poly         = fields.Char("No. Poly Box", track_visibility='on_change')
    kg_pal          = fields.Integer("Berat/KG (PAL)", track_visibility='on_change')
    heading         = fields.Integer("Heading (LOT)", track_visibility='on_change')
    NoSpec          = fields.Char("NoSpec", track_visibility='on_change',default="-")
    Diameter        = fields.Char("Diameter", track_visibility='on_change')
    Ket_Furnice     = fields.Char("Keterangan Furnace ", track_visibility='on_change')
    warna           = fields.Char("Warna (string)", track_visibility='on_change')
    warna_id        = fields.Many2one("product.color","Warna", track_visibility='on_change')
    No_Registrasi   = fields.Char("No Registrasi ", track_visibility='on_change')
    Std_PolyBox     = fields.Float("Standart Poly Box", track_visibility='on_change')
    rev_dwg         = fields.Char("Rev. Dwg", track_visibility='on_change')
    rev_tools       = fields.Char("Rev. Tools", track_visibility='on_change')
    quality       = fields.Char("Quality", track_visibility='on_change')
    item_name       = fields.Char("Item Name", track_visibility='on_change')

ProductTemplate()


class ProductProduct(models.Model):
    _inherit = 'product.product'

    # onchange hrs di panggil di product.product, karena di product.template ga jalan
    @api.onchange('warna_id')
    def onchange_warna_id(self):
        if self.warna_id:
            if self.warna_id.code :
                self.warna = self.warna_id.name + ' ['+self.warna_id.code+']'
            else :
                self.warna = self.warna_id.name

#     plt             = fields.Char("PLT", track_visibility='on_change')
#     lt              = fields.Char("LT", track_visibility='on_change')
#     no_poly         = fields.Char("No. Poly Box", track_visibility='on_change')
#     kg_pal          = fields.Integer("Berat/KG (PAL)", track_visibility='on_change')
#     heading         = fields.Integer("Heading (LOT)", track_visibility='on_change')
#     NoSpec          = fields.Char("NoSpec", track_visibility='on_change')
#     Diameter        = fields.Float("Diameter", track_visibility='on_change')
#     Ket_Furnice     = fields.Char("Keterangan Furnice ", track_visibility='on_change')
#     warna           = fields.Char("Warna", track_visibility='on_change')
#     No_Registrasi   = fields.Char("No Registrasi ", track_visibility='on_change')
#     Std_PolyBox     = fields.Char("Standart Poly Box", track_visibility='on_change')

#     @api.onchange('warna')
#     def onchange_warna(self):
#         if self.warna :
#             self.warna = self.warna.upper()


ProductProduct()