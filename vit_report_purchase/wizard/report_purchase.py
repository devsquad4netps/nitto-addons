from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportPurchaseWizard(models.TransientModel):
    _name = "vit.report.purchase.wizard"
    _description = "Report Purchase"

    @api.onchange('type')
    def onchange_type(self):
        if self.type:
            # if self.type == 'balance_po' :
            #     self.date_start = '2019-01-01'
            # else :
            self.date_start = fields.Date.to_string(date.today().replace(day=1))
            self.based_on = 'schedule_date'

    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes")
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    partner_ids = fields.Many2many("res.partner",string='Vendor', domain=[('supplier','=',True)])
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    based_on = fields.Selection([('request_date','Request Date'),('schedule_date','Schedule/Delivery Date'),('all','All')],string='Based on', default='schedule_date')
    type = fields.Selection([('all_po','All PO'),('balance_po','Balance PO')], string='Type', required=True, default=False)
    product_ids = fields.Many2many('product.product',string='Product', domain=[('purchase_ok','=',True)])
    purchase_ids = fields.Many2many('purchase.order',string='Purchase Order', domain=[('state','not in',['draft','cancel'])])


    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'left','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000'})
        #wbf['header_table'].set_border()

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        wbf['header_month_sum'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})

        wbf['content_number_sum'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['grey']})
        
        return wbf, workbook

    @api.multi
    def action_print_report(self):
        self.ensure_one()
        for report in self :
            i_type = dict(self._fields['based_on'].selection).get(report.based_on)
            report_name = 'Laporan Balance PO %s - %s)'%(report.date_start, report.date_end)
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
            company = company[:-3]
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")
            if self.partner_ids :
                # partners = str(tuple(report.partner_ids.ids)).replace(",)",")")
                partners = self.partner_ids
            else :
                partners = self.env['res.partner'].search([('supplier','=',True)])
            if self.product_ids :
                products = " and pol.product_id in %s " % (str(tuple(report.product_ids.ids)).replace(",)",")"))
            else :
                products = ''
            if self.purchase_ids :
                purchases = " and pol.order_id in %s " % (str(tuple(report.purchase_ids.ids)).replace(",)",")"))
            else :
                purchases = ''
            if report.type == 'balance_po' :
                return report.action_print_report_balance_po(i_type, report_name, company, companys, partners, products, purchases)
            else :
                return report.action_print_report_all_po(i_type, report_name, company, companys, partners, products, purchases)

    def action_print_report_all_po(self, i_type, report_name, company, companys, partners, products, purchases):
        for i in self :
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name+' ('+i_type+')')
            worksheet.set_column('A1:A1', 4)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 15)
            worksheet.set_column('D1:D1', 15)
            worksheet.set_column('E1:E1', 35)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:J2', 'Laporan PO Supplier '+str(i.date_start)+ ' to ' +str(i.date_end), wbf['company'])
            worksheet.merge_range('J3:L3', 'Printed on : '+ str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            row = 4

            grand_t_qty = 0.0
            grand_t_received = 0.0
            grand_t_subtotal = 0.0
            order_line = self.env['purchase.order.line']
            for partner in partners :
                if i.based_on == 'schedule_date':
                    clause_date = " and (pol.date_planned between '%s' and '%s') " % (str(i.date_start), str(i.date_end))
                    clause_order = " pol.date_planned asc,"
                elif i.based_on == 'request_date':
                    clause_date = " and (po.date_order between '%s' and '%s') " % (str(i.date_start), str(i.date_end))
                    clause_order = " po.date_order asc,"
                else :
                    clause_date = " and ((pol.date_planned between '%s' and '%s') or (po.date_order between '%s' and '%s')) " % (str(i.date_start), str(i.date_end), str(i.date_start), str(i.date_end))
                    clause_order = " po.date_order asc, pol.date_planned asc,"
                sql = "select pol.id from purchase_order_line pol " \
                  "left join purchase_order po on po.id = pol.order_id " \
                  "left join product_product pp on pol.product_id = pp.id " \
                  "where po.state != 'cancel' and po.partner_id = %s " \
                  "and po.company_id in %s and pol.cancel_date is null %s %s %s" \
                  "order by %s po.partner_id, pp.default_code asc" % (partner.id, companys,products, purchases, clause_date, clause_order)
            
                cr.execute(sql)
                datas = cr.fetchall()
                if not datas :
                    continue
                worksheet.merge_range('A%s:C%s'%(row,row), partner.name, wbf['header_table'])
                row+=1
                worksheet.write('A%s'%(row), 'No', wbf['header_month'])
                worksheet.write('B%s'%(row), 'Created Date', wbf['header_month'])
                worksheet.write('C%s'%(row), 'PO Number', wbf['header_month'])
                worksheet.write('D%s'%(row), 'Item Code', wbf['header_month'])
                worksheet.write('E%s'%(row), 'Item Name', wbf['header_month'])
                worksheet.write('F%s'%(row), 'Order Qty', wbf['header_month'])
                worksheet.write('G%s'%(row), 'Unit Price', wbf['header_month'])
                worksheet.write('H%s'%(row), 'Amount', wbf['header_month'])
                worksheet.write('I%s'%(row), 'Line Status', wbf['header_month'])
                worksheet.write('J%s'%(row), 'Currency', wbf['header_month'])
                worksheet.write('K%s'%(row), 'Delivery Date', wbf['header_month'])
                worksheet.write('L%s'%(row), 'Delivered Qty', wbf['header_month'])

                row+=1
                no = 0
                t_qty = 0.0
                t_received = 0.0
                t_subtotal = 0.0
                po_numbers = []
                for dt in datas :
                    data = order_line.browse(dt)
                    line_status = 'Open Order'
                    if data.move_ids.filtered(lambda mv:mv.state == 'done'):
                        line_status = 'Invoiced'
                    if data.order_id.name in po_numbers:
                        po_number = ''
                        number = ''
                    else :
                        po_numbers.append(data.order_id.name)
                        no += 1
                        number = no
                        po_number = data.order_id.name

                    worksheet.write('A%s'%(row), number, wbf['title_doc'])
                    worksheet.write('B%s'%(row), str(data.create_date)[:10], wbf['title_doc'])
                    worksheet.write('C%s'%(row), po_number, wbf['title_doc'])
                    worksheet.write('D%s'%(row), data.product_id.default_code, wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('F%s'%(row), data.product_qty, wbf['content_number'])
                    worksheet.write('G%s'%(row), data.price_unit, wbf['content_number'])
                    worksheet.write('H%s'%(row), data.price_subtotal, wbf['content_number'])
                    worksheet.write('I%s'%(row), line_status, wbf['title_doc'])
                    worksheet.write('J%s'%(row), data.order_id.currency_id.name, wbf['content_number'])
                    worksheet.write('K%s'%(row), str(data.date_planned)[:10] if data.date_planned else '', wbf['title_doc'])
                    worksheet.write('L%s'%(row), data.qty_received, wbf['content_number'])
                            
                    t_qty += data.product_qty
                    t_received += data.qty_received
                    t_subtotal += data.price_subtotal
                    row += 1
                    
                    grand_t_qty += t_qty
                    grand_t_received += t_received
                    grand_t_subtotal += t_subtotal
                # sum total perpartner
                worksheet.write('F%s'%(row), t_qty, wbf['content_number_sum'])
                worksheet.write('H%s'%(row), t_subtotal, wbf['content_number_sum'])
                worksheet.write('L%s'%(row), t_received, wbf['content_number_sum'])
                row += 1

            worksheet.merge_range('A%s:E%s'%(row+2,row+2), 'Grand Total', wbf['header_table'])
            worksheet.write('F%s'%(row+2), grand_t_qty, wbf['content_number_sum'])
            worksheet.write('H%s'%(row+2), grand_t_subtotal, wbf['content_number_sum'])
            worksheet.write('L%s'%(row+2), grand_t_received, wbf['content_number_sum'])

            if i.notes :
                worksheet.merge_range('A%s:L%s'%(row+4,row+4), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }                

    def action_print_report_balance_po(self, i_type, report_name, company, companys, partners, products, purchases):
        for i in self :
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name+' ('+i_type+')')
            worksheet.set_column('A1:A1', 4)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 20)
            worksheet.set_column('D1:D1', 20)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 35)
            worksheet.set_column('G1:G1', 20)
            worksheet.set_column('H1:H1', 20)
            worksheet.set_column('I1:I1', 20)
            worksheet.set_column('J1:J1', 20)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 20)
            worksheet.set_column('N1:N1', 20)
            worksheet.set_column('O1:O1', 20)

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:J2', 'Laporan Balance PO '+str(i.date_start)+ ' to ' +str(i.date_end), wbf['company'])
            worksheet.merge_range('J3:N3', 'Printed on : '+ str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            row = 4

            grand_t_qty = 0.0
            grand_t_received = 0.0
            grand_t_outstanding = 0.0
            order_line = self.env['purchase.order.line']
            for partner in partners :
                if i.based_on == 'schedule_date':
                    clause_date = " and (pol.date_planned between '%s' and '%s') " % (str(i.date_start), str(i.date_end))
                    clause_order = " pol.date_planned asc,"
                elif i.based_on == 'request_date':
                    clause_date = " and (po.date_order between '%s' and '%s') " % (str(i.date_start), str(i.date_end))
                    clause_order = " po.date_order asc,"
                else :
                    clause_date = " and ((pol.date_planned between '%s' and '%s') or (po.date_order between '%s' and '%s')) " % (str(i.date_start), str(i.date_end), str(i.date_start), str(i.date_end))
                    clause_order = " po.date_order asc, pol.date_planned asc,"
                sql = "select pol.id from purchase_order_line pol " \
                  "left join purchase_order po on po.id = pol.order_id " \
                  "left join product_product pp on pol.product_id = pp.id " \
                  "where po.state != 'cancel' and po.partner_id = %s and pol.cancel_date is null " \
                  " %s %s " \
                  "and po.company_id in %s %s" \
                  "order by %s po.partner_id, pp.default_code asc" % (partner.id, products, purchases ,companys,clause_date, clause_order)
            
                cr.execute(sql)
                datas = cr.fetchall()
                if not datas :
                    continue
                worksheet.merge_range('A%s:C%s'%(row,row), partner.name, wbf['header_table'])
                row+=1
                worksheet.write('A%s'%(row), 'No', wbf['header_month'])
                worksheet.write('B%s'%(row), 'Vendor Code', wbf['header_month'])
                worksheet.write('C%s'%(row), 'Vendor Name', wbf['header_month'])
                worksheet.write('D%s'%(row), 'PO Number', wbf['header_month'])
                worksheet.write('E%s'%(row), 'Item Code', wbf['header_month'])
                worksheet.write('F%s'%(row), 'Item Name', wbf['header_month'])
                worksheet.write('G%s'%(row), 'Request/Order Date', wbf['header_month'])
                worksheet.write('H%s'%(row), 'Schedule/Delivery Date', wbf['header_month'])
                worksheet.write('I%s'%(row), 'Order Qty', wbf['header_month'])
                worksheet.write('J%s'%(row), 'Total Received Qty', wbf['header_month'])
                worksheet.write('K%s'%(row), 'Total Outstanding Qty', wbf['header_month'])
                worksheet.write('L%s'%(row), 'Received Date', wbf['header_month'])
                worksheet.write('M%s'%(row), 'Received Qty', wbf['header_month'])
                worksheet.write('N%s'%(row), 'Packing Slip', wbf['header_month'])
                worksheet.write('O%s'%(row), 'Invoice Number', wbf['header_month'])

                row+=1
                no = 0
                t_qty = 0.0
                t_received = 0.0
                t_outstanding = 0.0
                po_numbers = []
                vendor_codes = []
                for dt in datas :
                    data = order_line.browse(dt)
                    if data.order_id.name in po_numbers:
                        po_number = ''
                        number = ''
                    else :
                        po_numbers.append(data.order_id.name)
                        no += 1
                        number = no
                        po_number = data.order_id.name
                    if data.order_id.partner_id.ref :
                        if data.order_id.partner_id.ref in vendor_codes:
                            partner_name = ''
                            partner_code = ''
                        else :
                            vendor_codes.append(data.order_id.partner_id.ref)
                            partner_code = data.order_id.partner_id.ref
                            partner_name = data.order_id.partner_id.name

                    qty_outstanding = data.product_qty - data.qty_received

                    worksheet.write('A%s'%(row), number, wbf['title_doc'])
                    worksheet.write('B%s'%(row), partner_code or '', wbf['title_doc'])
                    worksheet.write('C%s'%(row), partner_name, wbf['title_doc'])
                    worksheet.write('D%s'%(row), po_number, wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.product_id.default_code, wbf['title_doc'])
                    worksheet.write('F%s'%(row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('G%s'%(row), str(data.order_id.date_order)[:10] if data.order_id.date_order else '', wbf['title_doc'])
                    worksheet.write('H%s'%(row), str(data.date_planned)[:10] if data.date_planned else '', wbf['title_doc'])
                    worksheet.write('I%s'%(row), data.product_qty, wbf['content_number'])
                    worksheet.write('J%s'%(row), data.qty_received, wbf['content_number'])
                    worksheet.write('K%s'%(row), qty_outstanding, wbf['content_number'])
                    moves = data.move_ids.filtered(lambda x:x.state == 'done')
                    if not moves:
                        worksheet.write('L%s'%(row), '', wbf['title_doc'])
                        worksheet.write('M%s'%(row), '', wbf['title_doc'])
                        worksheet.write('N%s'%(row), '', wbf['title_doc'])
                        worksheet.write('O%s'%(row), '', wbf['title_doc'])
                        row += 1
                    else :
                        for move in moves.sorted('date') :
                            worksheet.write('L%s'%(row),str(move.kanban_id.date) if move.kanban_id else str(move.date)[:10], wbf['title_doc'])
                            worksheet.write('M%s'%(row), move.product_uom_qty, wbf['content_number'])
                            worksheet.write('N%s'%(row), move.kanban_id.name, wbf['title_doc'])
                            worksheet.write('O%s'%(row), move.kanban_id.no_sj_pengirim , wbf['title_doc'])
                            row += 1
                    t_qty += data.product_qty
                    t_received += data.qty_received
                    t_outstanding += qty_outstanding
                    
                    grand_t_qty += t_qty
                    grand_t_received += t_received
                    grand_t_outstanding += t_outstanding
                # sum total perpartner
                worksheet.write('I%s'%(row), t_qty, wbf['content_number_sum'])
                worksheet.write('J%s'%(row), t_received, wbf['content_number_sum'])
                worksheet.write('K%s'%(row), t_outstanding, wbf['content_number_sum'])
                row += 1

            worksheet.merge_range('A%s:H%s'%(row+2,row+2), 'Grand Total', wbf['header_table'])
            worksheet.write('I%s'%(row+2), grand_t_qty, wbf['content_number_sum'])
            worksheet.write('J%s'%(row+2), grand_t_received, wbf['content_number_sum'])
            worksheet.write('K%s'%(row+2), grand_t_outstanding, wbf['content_number_sum'])

            if i.notes :
                worksheet.merge_range('A%s:K%s'%(row+4,row+4), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }


ReportPurchaseWizard()