#-*- coding: utf-8 -*-

{
	"name": "Report Purchase",
	"version": "0.7", 
	"depends": [
		'base',
		'account',
		'purchase',
		'vit_kanban',
		'vit_po_add'
	],
	'author': 'widianajuniar@gmail.com',
	'website': 'http://www.vitraining.com',
	"summary": "",
	"description": """

""",
	"data": [
		"wizard/report_purchase.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}