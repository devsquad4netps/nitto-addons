from odoo import fields, api, models
from odoo.exceptions import Warning, UserError, ValidationError
from odoo.addons import decimal_precision as dp
from datetime import datetime
from dateutil.relativedelta import relativedelta
import base64
from xlrd import open_workbook



class StockMove(models.Model):
    _inherit = "stock.move"

    file_data = fields.Binary(string='Import File')
    filename = fields.Char(string='File Name')

    @api.multi
    def import_data(self):
        for move in self :
            wb = open_workbook(file_contents=base64.decodestring(move.file_data))
            values = []
            for s in wb.sheets():
                for row in range(s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = (s.cell(row, col).value)
                        col_value.append(value)
                    values.append(col_value)
            products = {}
            header_found = False
            move_line = self.env['stock.move.line']
            for value in values :
                if value[0] and value[0] not in ('COIL NUMBER','HEAT NUMBER','QUANTITY'):
                    if not move_line.sudo().search([('lot_name1','=',value[0]),('lot_name2','=',value[1])]) :
                        line = move_line.create({'move_id': move.id,
                                'product_id': move.product_id.id,
                                #'lot_id': lot.id,
                                'lot_name1': value[0],
                                'lot_name2': value[1],
                                'location_id': move.location_id.id,
                                'location_dest_id': move.location_dest_id.id,
                                'product_uom_id': move.product_uom.id,
                                'product_uom_qty': value[2],
                                'qty_done': value[2] })
                        line.onchange_lot_name()


StockMove()