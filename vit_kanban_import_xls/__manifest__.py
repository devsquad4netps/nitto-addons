{
    "name"          : "Import Data Penerimaan Barang",
    "version"       : "0.1",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Inventory",
    "license"       : "LGPL-3",
    "contributors"  : """
    """,
    "summary"       : "import data stock move line di penerimaan barang",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "stock_account",
        "stock",
        "sale",
        "sale_stock",
        "purchase",
        "purchase_stock",
        "vit_nitto_product",

    ],
    "data"          : [
        "views/stock_view.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}