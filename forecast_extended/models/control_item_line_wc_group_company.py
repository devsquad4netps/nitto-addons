from odoo import api, fields, models, _


class ControlItemLineWcGroupCompany(models.Model):
    _name = "control.item.line.wc.group.company"
    _description = "Control Item Line WC Group Company"
    _order = "name"

    name = fields.Integer('No')
    line_wc_id = fields.Many2one(
        comodel_name='control.item.line.wc.group',
        string='Item Line WC',
        required=False,
        ondelete='cascade')
    company_id = fields.Many2one(
        comodel_name='res.company',
        string='Plant',
        required=False)
    qty = fields.Float(
        string='Qty',
        required=False)
