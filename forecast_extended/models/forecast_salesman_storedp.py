from odoo import models, fields, api, _
from io import BytesIO
from pytz import timezone
from datetime import datetime
from xlrd import open_workbook
from odoo.exceptions import Warning
import xlsxwriter
import base64
import pytz
import logging
_logger = logging.getLogger(__name__)

class ForecastForecastSalesman(models.Model):
    _inherit = 'forecast.forecast_salesman'

    file_import = fields.Binary(string="File Import")
    filename_import = fields.Char(
        string='Filename Import',
        required=False)

    @api.model_cr
    def init(self):
        _logger.info("creating function create report forecast salesman...")
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_create_forecast_salesman(forecast_id integer , salesmanid integer, comp_id integer, uid integer, year integer, month1 integer, month2 integer, month3 integer, month4 integer, month5 integer, month6 integer, end_date date)
            RETURNS void AS
            $BODY$
            DECLARE
              product_ids record;
              product record;
              periode record;
              sol record;
              next_deivery record;
              last_delivery record;
              forecast_detail_id integer;
              wip record;
              del_min1 numeric := 0;
              del_min2 numeric := 0;
              del_min3 numeric := 0;
              so_min1 numeric := 0;
              so_min2 numeric := 0;
              so_min3 numeric := 0;
              del_plus0 numeric := 0;
              del_plus1 numeric := 0;
              del_plus2 numeric := 0;
              count integer := 0;
              count_wip integer := 0;
              loss_sale integer := 0;
              quant record;

            BEGIN

            FOR product IN 
                    SELECT 
                        pcc.product_id as product_id, 
                        pcc.partner_id as partner_id, 
                        pcc.product_code as product_code, 
                        rp.alias_name_kartu_produksi as partner_name

                    FROM 
                        product_customer_code pcc
                    LEFT JOIN 
                        res_partner rp on rp.id = pcc.partner_id
                    LEFT JOIN 
                        product_product pp on pp.id = pcc.product_id
                    WHERE 
                        rp.user_id = salesmanid
                        -- AND pcc.product_id=73140
                    ORDER BY pcc.product_id,pcc.partner_id
                LOOP
                    -- LAST DELIVERY
                    SELECT sum(del.sub_del_min1) AS del_min1,sum(del.sub_del_min2) AS del_min2,sum(del.sub_del_min3) AS del_min3 FROM 
                    (SELECT 
                        CASE WHEN extract(month from sm.date) = month1 
                        THEN sum(sm.product_uom_qty) ELSE 0 END AS sub_del_min1,
                        CASE WHEN extract(month from sm.date) = month2
                        THEN sum(sm.product_uom_qty) ELSE 0 END AS sub_del_min2,
                        CASE WHEN extract(month from sm.date) = month3 
                        THEN sum(sm.product_uom_qty) ELSE 0 END AS sub_del_min3
                        FROM 
                            stock_move sm
                        LEFT JOIN
                            stock_picking sp on sp.id = sm.picking_id
                        LEFT JOIN
                            stock_location sl on sl.id = sm.location_id
                        LEFT JOIN
                            stock_location dl on dl.id = sm.location_dest_id
                        LEFT JOIN
                            res_partner rp on rp.id = sp.partner_id
                        WHERE
                            sm.product_id = product.product_id
                            and rp.user_id = salesmanid
                            and sl.usage = 'internal'
                            and dl.usage = 'customer'
                            and sm.state = 'done'
                            and sm.company_id = comp_id
                            and sp.partner_id = product.partner_id
                            and 
                            -- Jika bulan 3 ke bawah tambah filter tahun lalu
                            (CASE WHEN month4 = 3 THEN extract(year from sm.date) in (year,year-1)
                            ELSE extract(year from sm.date) = year END)
                        GROUP BY 
                            extract(month from sm.date) ) AS del into last_delivery;
                    -- SALE ORDER
                    SELECT sum(foo.sub_so_min1) AS so_min1,sum(foo.sub_so_min2) AS so_min2,sum(foo.sub_so_min3) AS so_min3,sum(foo.sub_so_min4) AS so_min4 FROM 
                    (SELECT
                        CASE WHEN extract(month from so_l.delivery_date) = month1 
                        THEN sum(so_l.product_uom_qty) ELSE 0 END AS sub_so_min1,
                        CASE WHEN extract(month from so_l.delivery_date) = month2
                        THEN sum(so_l.product_uom_qty) ELSE 0 END AS sub_so_min2,
                        CASE WHEN extract(month from so_l.delivery_date) = month3 
                        THEN sum(so_l.product_uom_qty) ELSE 0 END AS sub_so_min3,
                        CASE WHEN extract(month from so_l.delivery_date) = month4 
                        THEN sum(so_l.product_uom_qty) ELSE 0 END AS sub_so_min4
                        
                        FROM 
                            sale_order_line so_l
                        LEFT JOIN
                            sale_order so ON so.id=so_l.order_id
                        WHERE
                            product_id = product.product_id
                            and so_l.salesman_id = salesmanid
                            and so_l.state in ('sale','done')
                            and so_l.order_partner_id = product.partner_id
                            and so.company_id = comp_id
                            and so.partner_id = product.partner_id
                            and 
                            (CASE WHEN so_l.delivery_date IS NOT null 
                                    THEN extract(month from so_l.delivery_date) 
                                WHEN so_l.delivery_date IS null AND so.commitment_date IS NOT null
                                    THEN extract(month from so.commitment_date) 
                                WHEN so_l.delivery_date IS null AND so.commitment_date IS null AND so.effective_date IS NOT null
                                    THEN extract(month from so.effective_date)  
                                ELSE
                                   extract(month from so.date_order)     
                            END ) in (month1,month2,month3,month4)
                            and 
                            -- Jika bulan 3 ke bawah tambah filter tahun lalu
                            (CASE WHEN month4 = 3 THEN extract(year from so_l.delivery_date) in (year,year-1)
                            ELSE extract(year from so_l.delivery_date) = year END)
                            
                        GROUP BY 
                            extract(month from so_l.delivery_date) ) AS foo into sol;
                    -- NEXT DELIVERY
                    SELECT sum(nextdel.sub_ndel_min1) AS del_plus0,sum(nextdel.sub_ndel_min2) AS del_plus1,sum(nextdel.sub_ndel_min3) AS del_plus2 FROM 
                    (SELECT 
                        CASE WHEN extract(month from sp.scheduled_date) = month4 
                        THEN sum(sm.product_uom_qty) ELSE 0 END AS sub_ndel_min1,
                        CASE WHEN extract(month from sp.scheduled_date) = month5
                        THEN sum(sm.product_uom_qty) ELSE 0 END AS sub_ndel_min2,
                        CASE WHEN extract(month from sp.scheduled_date) = month6
                        THEN sum(sm.product_uom_qty) ELSE 0 END AS sub_ndel_min3
                        FROM 
                            stock_move sm
                        LEFT JOIN
                            stock_picking sp on sp.id = sm.picking_id
                        LEFT JOIN
                            stock_location sl on sl.id = sm.location_id
                        LEFT JOIN
                            stock_location dl on dl.id = sm.location_dest_id
                        LEFT JOIN
                            res_partner rp on rp.id = sp.partner_id
                        WHERE
                            sm.product_id = product.product_id
                            and rp.user_id = salesmanid
                            and sl.usage = 'internal'
                            and dl.usage = 'customer'
                            and sm.state not in ('done','cancel')
                            and sm.company_id = comp_id
                            and rp.id = product.partner_id
                            and 
                            -- Jika bulan 11 ke atas tambah filter tahun depan
                            (CASE WHEN month4 = 11 THEN extract(year from sp.scheduled_date) in (year,year+1)
                            ELSE extract(year from sp.scheduled_date) = year END)
                        GROUP BY 
                            extract(month from sp.scheduled_date) ) AS nextdel into next_deivery;

                    -- ONHAND LOCAL (01) AND IMPORT (02)
                    SELECT CASE WHEN pp.default_code like '01%' THEN sum(sq.quantity) ELSE 0 END as quant_local,
                        CASE WHEN pp.default_code like '02%' THEN sum(sq.quantity) ELSE 0 END as quant_import
                    FROM stock_quant sq 
                        LEFT JOIN stock_location sl ON sl.id = sq.location_id 
                        LEFT JOIN product_product pp ON pp.id = sq.product_id
                        WHERE sq.product_id = product.product_id AND sl.usage = 'internal'
                        GROUP BY pp.default_code into quant;

                    -- SUM WIP
                    SELECT CASE WHEN sum(mw.qty_produced_real) > 0
                            THEN sum(mw.qty_produced_real) ELSE sum(mw.qty_produced-mw.qty_remaining) END as qty_wip 
                        FROM mrp_workorder mw
                        LEFT JOIN mrp_production mp ON mp.id=mw.production_id
                        WHERE 
                            mw.product_id = product.product_id
                            AND mp.state not in ('done','cancel')
                            AND ( 
                                    (mw.state = 'progress' and is_wip = True) OR (mw.state = 'done' and is_wip = True) 
                                        ) into wip ;

                    -- LOSS SALES
                    SELECT sum(soline.product_uom_qty)-sum(soline.qty_delivered)
                        FROM sale_order_line soline
                        LEFT JOIN sale_order so on soline.order_id=so.id
                            WHERE 
                            soline.product_id = product.product_id
                            and soline.salesman_id = salesmanid
                            and soline.state in ('sale','done')
                            and soline.order_partner_id = product.partner_id
                            and so.company_id = comp_id
                            and soline.state in ('sale','done') 
                            and so.partner_id = product.partner_id
                            and soline.delivery_date <= end_date 
                            into loss_sale;
                    
                    -- INSERT OR CREATE REPORT SALESMAN
                    count = count+1;
                    INSERT INTO forecast_forecast_salesman_detail("create_uid", "create_date", "write_uid", "write_date",
                                "forecast_salesman_id",
                                "product_id",
                                "name",
                                "part_customer",
                                "customer_id",
                                "delivery_min3",
                                "delivery_min2",
                                "delivery_min1",
                                "so_min3",
                                "so_min2",
                                "so_min1",
                                "so_min0",
                                "delivery_plus2",
                                "delivery_plus1",
                                "delivery_plus0",
                                "loss_sales",
                                "total_finish_goods",
                                "fg_local",
                                "fg_import",
                                "wip",
                                "customer_name"
                                 )
                        VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'),
                                forecast_id,
                                product.product_id,
                                count,
                                product.product_code,
                                product.partner_id,
                                last_delivery.del_min3,
                                last_delivery.del_min2,
                                last_delivery.del_min1,
                                sol.so_min3,
                                sol.so_min2,
                                sol.so_min1,
                                sol.so_min4,
                                next_deivery.del_plus2,
                                next_deivery.del_plus1,
                                next_deivery.del_plus0,
                                -- (sol.so_min1+sol.so_min2+sol.so_min3)-(last_delivery.del_min1+last_delivery.del_min2+last_delivery.del_min3),
                                loss_sale,
                                quant.quant_local+quant.quant_import,
                                quant.quant_local,
                                quant.quant_import,
                                wip.qty_wip,
                                product.partner_name
                                );
                    -- END CREATE
                    -- WIP
                    -- FOR wip IN SELECT mw.workcenter_id as workcenter_id, 
                    --     CASE WHEN sum(mw.qty_produced_real) > 0
                    --         THEN sum(mw.qty_produced_real) ELSE sum(mw.qty_produced-mw.qty_remaining) END as qty 
                    --     FROM mrp_workorder mw
                    --     LEFT JOIN mrp_production mp ON mp.id=mw.production_id
                    --     WHERE 
                    --         mw.product_id = product.product_id
                    --         AND mp.state not in ('done','cancel')
                    --         AND (mw.state = 'progress' and is_wip = True)
                    --         OR (mw.state = 'done' and is_wip = True)
                    --     GROUP BY 
                    --         mw.workcenter_id
                    --     loop
                    --         count_wip = count_wip+1;
                    --         INSERT INTO forecast_wip_quantity("forecast_salesman_detail_id","name","quantity","routing_id")
                    --             VALUES
                    --         (forecast_detail_id, count_wip, wip.qty, wip.workcenter_id);    
                    -- end loop;
                END LOOP;
            END;

            $BODY$
              LANGUAGE plpgsql VOLATILE
              COST 100;
                    """)

        _logger.info("creating function validate forecast salesman...")
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_validate_forecast_salesman(forecast_id integer)
            RETURNS void AS
            $BODY$
            DECLARE
              product record;
              wip record;
              count_wip integer := 0;

            BEGIN
                FOR product IN SELECT id,product_id FROM forecast_forecast_salesman_detail WHERE forecast_salesman_id = forecast_id AND (estimation_plus1 != 0.0 OR estimation_plus2 != 0.0 OR estimation_plus3 != 0.0)
                    LOOP
                    -- WIP
                    count_wip := 0;
                    FOR wip IN SELECT mw.workcenter_id as workcenter_id, 
                        CASE WHEN sum(mw.qty_produced_real) > 0
                            THEN sum(mw.qty_produced_real) ELSE sum(mw.qty_produced-mw.qty_remaining) END as qty 
                        FROM mrp_workorder mw
                        LEFT JOIN mrp_production mp ON mp.id=mw.production_id
                        WHERE 
                            mw.product_id = product.product_id
                            AND mp.state not in ('done','cancel')
                            AND (
                                (mw.state = 'progress' and is_wip = True)
                                OR (mw.state = 'done' and is_wip = True)
                                )
                        GROUP BY 
                            mw.workcenter_id
                        LOOP
                            count_wip = count_wip+1;
                            INSERT INTO forecast_wip_quantity("forecast_salesman_detail_id","name","quantity","routing_id")
                                VALUES
                            (product.id, count_wip, wip.qty, wip.workcenter_id);    
                    END LOOP;
                END LOOP;
            END;

            $BODY$
              LANGUAGE plpgsql VOLATILE
              COST 100;
                    """)

    @api.multi
    def action_validate(self):
        self.ensure_one()
        datas_exist = self.search([('periode_id','=',self.periode_id.id),
                                    ('salesman_id','=',self.salesman_id.id),
                                    ('company_id','=',self.company_id.id),
                                    ('state','=','validate')])
        if datas_exist :
            raise Warning("Duplicate data found (%s) has been validated !" % datas_exist[0].name)
        line_ids = str(tuple(self.line_ids.ids)).replace(',)', ')')
        self.env.cr.execute("""
            DELETE FROM 
                forecast_wip_quantity 
            WHERE 
                forecast_salesman_detail_id in %s
        """%( line_ids) )
        self.env.cr.execute("SELECT vit_validate_forecast_salesman(%d)" % self.id)
        return super(ForecastForecastSalesman, self).action_validate()

    @api.multi
    def new_fill_product(self):
        self.ensure_one()
        end_date = self.periode_id.get_start_end_date(diff=0)[1][:10]
        # hapus existing detail
        self.env.cr.execute("""
            DELETE FROM 
                forecast_forecast_salesman_detail 
            WHERE 
                forecast_salesman_id = %s
        """%(self.id))
        #import pdb;pdb.set_trace()
        bulan = int(self.periode_id.bulan)
        tahun = int(self.periode_id.tahun)
        bulan_min1 = bulan-1
        if bulan_min1 <= 0 :
            bulan_min1 = 12
        bulan_min2 = bulan-2
        if bulan_min2 == 0 :
            bulan_min2 = 12
        elif bulan_min2 == -1 :
            bulan_min2 = 11
        bulan_min3 = bulan-3
        if bulan_min3 == 0 :
            bulan_min3 = 12
        elif bulan_min3 == -1 :
            bulan_min3 = 11
        elif bulan_min3 == -2 :
            bulan_min3 = 10
        bulan_plus1 = bulan+1
        if bulan_plus1 == 13 :
            bulan_plus1 = 1
        bulan_plus2 = bulan+12
        if bulan_plus2 == 13:
            bulan_plus2 = 1
        elif bulan_plus2 == 14:
            bulan_plus2 = 2
        self.env.cr.execute("SELECT vit_create_forecast_salesman(%d, %d,%d, %d, %d, %d, %d, %d, %d, %d, %d, '%s')" % 
            (self.id, self.salesman_id.id, self.company_id.id, self.env.uid, tahun,  bulan_min1, bulan_min2, bulan_min3, bulan, bulan_plus1, bulan_plus2, end_date) )
        self.write({
            'export_data': False,
            'name_ex': False,
        })
        return {
                'type': 'ir.actions.client',
                'tag': 'reload',
            }

    def action_import(self):
        self.ensure_one()
        if not self.export_data :
            return
        wb = open_workbook(file_contents=base64.decodestring(self.file_import))
        values = []
        for s in wb.sheets():
            for row in range(s.nrows):
                col_value = []
                for col in range(s.ncols):
                    value = (s.cell(row, col).value)
                    col_value.append(value)
                values.append(col_value)
        row = 0
        for data in values:
            row += 1
            if row in (1,2) or data[0] == 'Total' or not data[34] :
                continue
            line_id = self.env['forecast.forecast_salesman_detail'].browse(int(data[34]))
            line_id.write({
                'estimation_plus1': data[16] or 0,
                'estimation_plus2': data[17] or 0,
                'estimation_plus3': data[18] or 0,
            })
        self.file_import = False

    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone(self.env.user.tz or 'Asia/Jakarta'))

    def new_export_excel(self):
        self.ensure_one()
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)

        date_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        report_name = 'Forecast'
        filename = '%s %s.xlsx' % (report_name, date_string)

        worksheet = workbook.add_worksheet(report_name)

        # set column with
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 40)
        worksheet.set_column('C:C', 40)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 20)
        worksheet.set_column('H:H', 20)
        worksheet.set_column('I:I', 20)
        worksheet.set_column('J:J', 20)
        worksheet.set_column('K:K', 20)
        worksheet.set_column('L:L', 20)
        worksheet.set_column('M:M', 20)
        worksheet.set_column('N:N', 20)
        worksheet.set_column('O:O', 20)
        worksheet.set_column('P:P', 20)
        worksheet.set_column('Q:Q', 20)
        worksheet.set_column('R:R', 20)
        worksheet.set_column('S:S', 20)
        worksheet.set_column('T:T', 20)
        worksheet.set_column('U:U', 20)
        worksheet.set_column('V:V', 20)
        worksheet.set_column('W:W', 20)
        worksheet.set_column('X:X', 20)
        worksheet.set_column('Y:Y', 20)
        worksheet.set_column('Z:Z', 20)
        worksheet.set_column('AA:AA', 20)
        worksheet.set_column('AB:AB', 20)
        worksheet.set_column('AC:AC', 20)
        worksheet.set_column('AD:AD', 20)
        worksheet.set_column('AE:AE', 20)
        worksheet.set_column('AF:AF', 20)
        worksheet.set_column('AG:AG', 20)
        worksheet.set_column('AH:AH', 20)
        worksheet.set_column('AI:AI', 20)

        worksheet.merge_range('A2:A3', 'No', wbf['header_orange'])
        worksheet.merge_range('B2:B3', 'Type', wbf['header_orange'])
        worksheet.merge_range('C2:C3', 'Area Produksi', wbf['header_orange'])
        worksheet.merge_range('D2:D3', 'LT', wbf['header_orange'])
        worksheet.merge_range('E2:E3', 'STD PL', wbf['header_orange'])
        worksheet.merge_range('F2:I2', 'H', wbf['header_orange'])
        worksheet.write('F3', 'SO'+self.periode_id.get_month_name(-3), wbf['header_orange'])
        worksheet.write('G3', 'SO'+self.periode_id.get_month_name(-2), wbf['header_orange'])
        worksheet.write('H3', 'SO'+self.periode_id.get_month_name(-1), wbf['header_orange'])
        worksheet.write('I3', 'XSO', wbf['header_orange'])
        worksheet.merge_range('J2:M2', 'SALES', wbf['header_orange'])
        worksheet.write('J3', 'DO'+self.periode_id.get_month_name(-3), wbf['header_orange'])
        worksheet.write('K3', 'DO'+self.periode_id.get_month_name(-2), wbf['header_orange'])
        worksheet.write('L3', 'DO'+self.periode_id.get_month_name(-1), wbf['header_orange'])
        worksheet.write('M3', 'XSALES', wbf['header_orange'])
        worksheet.write('N2', 'A', wbf['header_orange'])
        worksheet.write('O2', 'B', wbf['header_orange'])
        worksheet.write('P2', 'C', wbf['header_orange'])
        worksheet.write('N3', 'LOSS SL', wbf['header_orange'])
        worksheet.write('O3', 'BL SO '+self.periode_id.get_month_name(), wbf['header_orange'])
        worksheet.write('P3', 'A + B', wbf['header_orange'])
        worksheet.write('Q2', 'D', wbf['header_orange'])
        worksheet.write('R2', 'E', wbf['header_orange'])
        worksheet.write('S2', 'F', wbf['header_orange'])
        worksheet.write('T2', 'A+D+E+F=G', wbf['header_orange'])
        worksheet.write('Q3', 'REQ '+self.periode_id.get_month_name(1), wbf['header_orange'])
        worksheet.write('R3', 'REQ '+self.periode_id.get_month_name(2), wbf['header_orange'])
        worksheet.write('S3', 'REQ '+self.periode_id.get_month_name(3), wbf['header_orange'])
        worksheet.write('T3', 'TOTAL REQ', wbf['header_orange'])
        worksheet.write('U2', 'I', wbf['header_orange'])
        worksheet.write('U3', 'WIP', wbf['header_orange'])
        worksheet.merge_range('V2:W2', 'J', wbf['header_orange'])
        worksheet.write('V3', 'FGLOCAL', wbf['header_orange'])
        worksheet.write('W3', 'FGIMP', wbf['header_orange'])
        worksheet.write('X2', 'H+I+J=K', wbf['header_orange'])
        worksheet.write('Y2', 'K-(A+D)=L', wbf['header_orange'])
        worksheet.write('Z2', 'L-E=M', wbf['header_orange'])
        worksheet.write('AA2', 'M-F=N', wbf['header_orange'])
        worksheet.write('X3', 'TOTAL', wbf['header_orange'])
        worksheet.write('Y3', 'A+D PROD', wbf['header_orange'])
        worksheet.write('Z3', 'E PROD', wbf['header_orange'])
        worksheet.write('AA3', 'F PROD', wbf['header_orange'])
        worksheet.merge_range('AB2:AC2', 'O', wbf['header_orange'])
        worksheet.merge_range('AD2:AE2', 'P', wbf['header_orange'])
        worksheet.merge_range('AF2:AG2', 'Q', wbf['header_orange'])
        worksheet.write('AB3', '', wbf['header_orange'])
        worksheet.write('AC3', '', wbf['header_orange'])
        worksheet.write('AD3', '', wbf['header_orange'])
        worksheet.write('AE3', '', wbf['header_orange'])
        worksheet.write('AF3', '', wbf['header_orange'])
        worksheet.write('AG3', '', wbf['header_orange'])
        worksheet.merge_range('AH2:AH3', 'Customer', wbf['header_orange'])
        worksheet.merge_range('AI2:AI3', 'Database ID', wbf['header_orange'])

        row = first_row = 4
        no = 1
        for line in self.line_ids :
            worksheet.write('A%s' % row, no, wbf['content_number'])
            worksheet.write('B%s' % row, line.product_id.name, wbf['content'])
            worksheet.write('C%s' % row, line.forecast_salesman_id.company_id.name, wbf['content'])
            worksheet.write('D%s' % row, line.product_id.lt or '', wbf['content_float'])
            worksheet.write('E%s' % row, line.product_id.Std_PolyBox or '', wbf['content_float'])

            worksheet.write('F%s' % row, line.so_min3 or '', wbf['content_float'])
            worksheet.write('G%s' % row, line.so_min2 or '', wbf['content_float'])
            worksheet.write('H%s' % row, line.so_min1 or '', wbf['content_float'])
            worksheet.write('I%s' % row, (line.so_min3+line.so_min2+line.so_min1)/3 if line.so_min3 or line.so_min2 or line.so_min1 else '', wbf['content_float'])
            
            worksheet.write('J%s' % row, line.delivery_min3 or '', wbf['content_float'])
            worksheet.write('K%s' % row, line.delivery_min2 or '', wbf['content_float'])
            worksheet.write('L%s' % row, line.delivery_min1 or '', wbf['content_float'])
            worksheet.write('M%s' % row, (line.delivery_min3+line.delivery_min2+line.delivery_min1)/3 if line.delivery_min3 or line.delivery_min2 or line.delivery_min1 else '', wbf['content_float'])

            worksheet.write('N%s' % row, line.loss_sales or '', wbf['content_float'])
            worksheet.write('O%s' % row, line.so_min0 or '', wbf['content_float'])
            worksheet.write('P%s' % row, (line.loss_sales + line.so_min0) or '', wbf['content_float'])
            worksheet.write('Q%s' % row, line.estimation_plus1 or '', wbf['content_float'])
            worksheet.write('R%s' % row, line.estimation_plus2 or '', wbf['content_float'])
            worksheet.write('S%s' % row, line.estimation_plus3 or '', wbf['content_float'])
            worksheet.write('T%s' % row, line.estimation_plus1+line.estimation_plus2+line.estimation_plus3 or '', wbf['content_float'])
            worksheet.write('U%s' % row, line.wip or '', wbf['content_float'])
            worksheet.write('V%s' % row, line.fg_local or '', wbf['content_float'])
            worksheet.write('W%s' % row, line.fg_import or '', wbf['content_float'])
            worksheet.write_formula('X%s' % row, '{=I%s+U%s+V%s+W%s}' % (row, row, row, row), wbf['content_float'])
            worksheet.write_formula('Y%s' % row, '{=K%s-(A%s+D%s)}' % (row, row, row), wbf['content_float'])
            worksheet.write_formula('Z%s' % row, '{=Y%s-R%s}' % (row, row), wbf['content_float'])
            worksheet.write_formula('AA%s' % row, '{=Z%s-S%s}' % (row, row), wbf['content_float'])
            worksheet.write_formula('AB%s' % row, '{=IF(SUM(Y%s)<0,ABS(ROUNDUP(ABS(SUM(Y%s))/E%s,0)*E%s)," ")}' % (row, row, row, row), wbf['content_float'])
            worksheet.write_formula('AC%s' % row, '{=IF(SUM(AB%s)>0,AB%s-(ABS(Y%s)/E%s)*E%s," ")}' % (row, row, row, row, row), wbf['content_float'])
            worksheet.write_formula('AD%s' % row, '{=IF(AND(Z%s<0,AT%s>0),(ROUNDUP(ABS(AT%s)/E%s,0)*E%s)," ")}' % (row, row, row, row, row), wbf['content_float'])
            worksheet.write_formula('AE%s' % row, '{=IF(SUM(AD%s)>0,AD%s-AT%s," ")}' % (row, row, row), wbf['content_float'])
            worksheet.write_formula('AF%s' % row, '{=IF(AND(AA%s<0,SUM(AU%s)>0),ROUNDUP((ABS(AA%s)-SUM(AE%s))/E%s,0)*E%s," ")}' % (row, row, row, row, row, row), wbf['content_float'])
            worksheet.write('AG%s' % row, '' or '', wbf['content_float'])
            worksheet.write('AH%s' % row, line.customer_name, wbf['content'])
            worksheet.write('AI%s' % row, line.id, wbf['content_number'])
            row += 1
            no += 1

        worksheet.merge_range('A%s:B%s' % (row, row), 'Total', wbf['total_orange'])
        worksheet.write('C%s' % row, '', wbf['total_orange'])
        worksheet.write('D%s' % row, '', wbf['total_orange'])
        worksheet.write('E%s' % row, '', wbf['total_orange'])
        worksheet.write_formula('F%s' % row, '{=subtotal(9,F%s:F%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('G%s' % row, '{=subtotal(9,G%s:G%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('H%s' % row, '{=subtotal(9,H%s:H%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('I%s' % row, '{=subtotal(9,I%s:I%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('J%s' % row, '{=subtotal(9,J%s:J%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('K%s' % row, '{=subtotal(9,K%s:K%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('L%s' % row, '{=subtotal(9,L%s:L%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('M%s' % row, '{=subtotal(9,M%s:M%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('N%s' % row, '{=subtotal(9,N%s:N%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('O%s' % row, '{=subtotal(9,O%s:O%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('P%s' % row, '{=subtotal(9,P%s:P%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('Q%s' % row, '{=subtotal(9,Q%s:Q%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('R%s' % row, '{=subtotal(9,R%s:R%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('S%s' % row, '{=subtotal(9,S%s:S%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('T%s' % row, '{=subtotal(9,T%s:T%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('U%s' % row, '{=subtotal(9,U%s:U%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('V%s' % row, '{=subtotal(9,V%s:V%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('W%s' % row, '{=subtotal(9,X%s:X%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('X%s' % row, '{=subtotal(9,X%s:X%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('Y%s' % row, '{=subtotal(9,Y%s:Y%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('Z%s' % row, '{=subtotal(9,Z%s:Z%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AA%s' % row, '{=subtotal(9,AA%s:AA%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AB%s' % row, '{=subtotal(9,AB%s:AB%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AC%s' % row, '{=subtotal(9,AC%s:AC%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AD%s' % row, '{=subtotal(9,AD%s:AD%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AE%s' % row, '{=subtotal(9,AE%s:AE%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AF%s' % row, '{=subtotal(9,AF%s:AF%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AG%s' % row, '{=subtotal(9,AG%s:AG%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write('AH%s' % row, '', wbf['total_float_orange'])
        worksheet.write('AI%s' % row, '', wbf['total_float_orange'])

        workbook.close()
        out = base64.encodestring(fp.getvalue())
        self.write({
            'export_data': out,
            'name_ex': filename,
        })
        fp.close()
        url = 'web/content/?model=%s&id=%s&field=export_data&download=true&filename=%s' % (self._name, self.id, filename)
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': url,
        }

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': '#FFFFDB', 'font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': colors['orange'], 'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': colors['yellow'], 'font_color': '#000000'})
        wbf['header_yellow'].set_border()

        wbf['header_no'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': '#FFFFDB', 'font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')

        wbf['footer'] = workbook.add_format({'align': 'left'})

        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()

        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right()

        wbf['title_doc'] = workbook.add_format({'bold': 1, 'align': 'left'})
        wbf['title_doc'].set_font_size(12)

        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)

        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right()

        wbf['content_float'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.00'})
        wbf['content_float'].set_right()
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right()
        wbf['content_number'].set_left()

        wbf['content_percent'] = workbook.add_format({'align': 'right', 'num_format': '0.00%'})
        wbf['content_percent'].set_right()
        wbf['content_percent'].set_left()

        wbf['total_float'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['white_orange'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()

        wbf['total_number'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['white_orange'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()

        wbf['total_orange'] = workbook.add_format({'bold': 1, 'bg_color': colors['white_orange'], 'align': 'center', 'valign': 'vcenter'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['yellow'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()

        wbf['total_number_yellow'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['yellow'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()

        wbf['total_yellow'] = workbook.add_format({'bold': 1, 'bg_color': colors['yellow'], 'align': 'center', 'valign': 'vcenter'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['orange'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()

        wbf['total_number_orange'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['orange'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()

        wbf['total_orange'] = workbook.add_format({'bold': 1, 'bg_color': colors['orange'], 'align': 'center', 'valign': 'vcenter'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()

        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()

        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()

        return wbf, workbook