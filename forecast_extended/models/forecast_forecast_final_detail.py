from odoo import api, fields, models

class ForecastForecastFinalDetail(models.Model):
    _inherit = 'forecast.forecast_final_detail'
    _order = 'name'

    salesman_id = fields.Many2one(
        comodel_name='res.users',
        string='Salesman',
        required=False)
    name = fields.Integer(required=True, string="No", help="")
    customer_id = fields.Many2one(
        comodel_name='res.partner',
        string='Customer',
        required=False)
    customer_name = fields.Text(
        string='Customers',
        required=False)


ForecastForecastFinalDetail()