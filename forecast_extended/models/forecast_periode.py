from odoo import api, fields, models
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

MONTHS = {
    -2: 'OKT',
    -1: 'NOV',
    0: 'DES',
    1: 'JAN',
    2: 'FEB',
    3: 'MAR',
    4: 'APR',
    5: 'MEI',
    6: 'JUN',
    7: 'JUL',
    8: 'AGT',
    9: 'SEP',
    10: 'OKT',
    11: 'NOV',
    12: 'DES',
    13: 'JAN',
    14: 'FEB',
    15: 'MAR',
}

class ForecastPeriode(models.Model):
    _inherit = 'forecast.periode'

    def get_start_end_date(self, diff=0, format='datetime'):
        self.ensure_one()
        month = self.bulan
        if len(month) == 1 :
            month = '0%s'%(month)
        month = month.replace(' ','')
        year = self.tahun.replace(' ','')
        start_date = '%s-%s-01'%(year, month)
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        if diff :
            start_date = start_date + relativedelta(months=diff)
        next_month_start_date = start_date + relativedelta(months=1)
        end_date = next_month_start_date - timedelta(days=1)

        # tambahkan time
        if format == 'datetime' :
            start_date = (start_date - timedelta(days=-1)).strftime('%Y-%m-%d 17:00:00')
            end_date = end_date.strftime('%Y-%m-%d 16:59:59')
        else :
            start_date = start_date.strftime('%Y-%m-%d')
            end_date = end_date.strftime('%Y-%m-%d')
        return start_date, end_date

    def get_month_name(self, diff=0):
        self.ensure_one()
        month_name = ''
        month = self.bulan
        if month :
            month_range = int(month) + diff
            month_name = MONTHS[month_range]
        return month_name

    def get_working_days(self, months=[]):
        working_days = 30
        # format months: [2020-01]
        self.ensure_one()
        # if not months :
        #     months = ['%s-%s'%(self.tahun, self.bulan)]
        # # TODO: logic untuk menghitung hari kerja
        # if months :
        #     working_days *= len(months)
        if self.hari_kerja:
            working_days = self.hari_kerja
        return working_days

    def get_pref_period(self, diff=1):
        self.ensure_one()
        date_str = '%s-%s-01'%(self.tahun, self.bulan)
        current_month = datetime.strptime(date_str, '%Y-%m-%d')
        pref_month = current_month - relativedelta(months=diff)
        pref_period = self.search([
            ('bulan','=','%02d'%pref_month.month),
            ('tahun','=',str(pref_month.year)),
        ], limit=1)
        return pref_period