from odoo import api, fields, models, _

class ControlItemLineWcGroup(models.Model):
    _name = "control.item.line.wc.group"
    _description = "Control Item Line WC Group"
    _order = "name"

    name = fields.Integer('No')
    line_id = fields.Many2one(
        comodel_name='control.item.line',
        string='Item Line',
        required=False,
        ondelete='cascade')
    group_report = fields.Selection([
        ('CF', 'CF'),
        ('Heading', 'Heading'),
        ('Machine', 'Machining'),
        ('Rolling', 'Rolling'),
        ('Trimming', 'Trimming'),
        ('Sloting', 'Sloting'),
        ('Cutting', 'Cutting'),
        ('Washing', 'Washing'),
        ('Furnished', 'Furnace'),
        ('Plating', 'Plating'),
        ('Final Quality', 'Final Quality'),
        ('Three Bond', 'Three Bond'),
        ('Packing', 'Packing')
    ], 'Workcenter Group')
    item_line_wc_group = fields.One2many(
        comodel_name='control.item.line.wc.group.company',
        inverse_name='line_wc_id',
        string='Item Line WC Group',
        required=False)
