import xlsxwriter
import base64
from odoo import fields, models, api
from io import BytesIO
from datetime import datetime, timedelta, date
from pytz import timezone
import pytz
import calendar

class ControlItemLine(models.Model):
    _name = "control.item.line"
    _description = "Control Item Line"
    _order = "name"
    
    @api.multi
    def _get_judg(self):
        for rec in self :
            if rec.day < 10 :
                judg = 'X'
            else :
                judg = 'O'
            rec.judg = judg

    name = fields.Integer('No', required=True)
    control_id = fields.Many2one(
        comodel_name='control.item',
        string='Control Item',
        ondelete='cascade')
    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Item Name',
        required=True)
    default_code = fields.Char(
        string='Item ID')
    qty_month = fields.Float(
        string='Qty/Month',
        required=False)
    qty_day = fields.Float(
        string='Qty/Day',
        required=False)
    qty_finish_good = fields.Float(
        string='Qty (Finish Good)',
        required=False)
    day = fields.Integer(
        string='Day',
        required=False)
    judg = fields.Char(
        string='Judg',
        compute='_get_judg')
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Customer',
        required=False)
    lt = fields.Float(
        string='LT',
        required=False)
    area_hd_id = fields.Many2one(
        comodel_name='res.company',
        string='Area HD',
        required=False)
    category = fields.Char(
        string='Category')
    sisa_kartu = fields.Float(
        string='Sisa Kartu',
        required=False)
    kurang_kartu = fields.Float(
        string='Kurang Kartu',
        required=False)
    lebih_kartu = fields.Float(
        string='Lebih Kartu',
        required=False)
    plt = fields.Float(
        string='STD PLT')
    lt2 = fields.Float(
        string='LT 2',
        required=False)
    qty = fields.Float(
        string='Qty',
        required=False)
    new_card = fields.Float(
        string='New Card',
        required=False)
    lt3 = fields.Float(
        string='LT 3',
        required=False)
    outstanding_sale = fields.Float(
        string='BL SO',
        required=False)
    sale_prev3 = fields.Float(
        string='Sale Prev 3',
        required=False)
    sale_prev2 = fields.Float(
        string='Sale Prev 2',
        required=False)
    sale_prev1 = fields.Float(
        string='Sale Prev 1',
        required=False)
    sale_current = fields.Float(
        string='Sale Current',
        required=False)
    item_line_wc = fields.One2many(
        comodel_name='control.item.line.wc.group',
        inverse_name='line_id',
        string='Item Line WC Group',
        required=False)
    total_wip = fields.Float(
        string='Total Wip',
        required=False)
