from odoo import api, fields, models

class ForecastForecastSalesmanDetail(models.Model):
    _inherit = 'forecast.forecast_salesman_detail'
    _order = 'name'

    name = fields.Integer(required=True, string="No", help="")
