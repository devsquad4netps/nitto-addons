# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from io import BytesIO
from pytz import timezone
from datetime import datetime
from xlrd import open_workbook

import xlsxwriter
import base64
import pytz

class ForecastForecastSalesman(models.Model):
    _inherit = 'forecast.forecast_salesman'

    file_import = fields.Binary(string="File Import")
    filename_import = fields.Char(
        string='Filename Import',
        required=False)

    @api.multi
    def new_fill_product(self):
        for rec in self :
            # hapus existing detail
            self._cr.execute("""
                DELETE FROM 
                    forecast_forecast_salesman_detail 
                WHERE 
                    forecast_salesman_id = %s
            """%(rec.id))

            # get product dari table customer code
            query = """
                SELECT 
                    pcc.product_id, 
                    pcc.partner_id, 
                    pcc.product_code, 
                    rp.name as partner_name,
                    pp.plant_id
                FROM 
                    product_customer_code pcc
                LEFT JOIN 
                    res_partner rp on rp.id = pcc.partner_id
                LEFT JOIN 
                    product_product pp on pp.id = pcc.product_id
                WHERE 
                    rp.user_id = %s
            """%(rec.salesman_id.id)
            # query += " limit 20 " # for testing
            self._cr.execute(query)
            result = self._cr.dictfetchall()

            # looping sebanyak product
            no = 0
            line_vals = []
            for res in result :
                product_id = self.env['product.product'].browse(res['product_id'])
                no += 1
                so_min1 = rec.get_sale_qty(product_id=product_id.id, diff=-1)
                so_min2 = rec.get_sale_qty(product_id=product_id.id, diff=-2)
                so_min3 = rec.get_sale_qty(product_id=product_id.id, diff=-3)
                do_min1 = rec.get_delivery_qty(product_id=product_id.id, diff=-1)
                do_min2 = rec.get_delivery_qty(product_id=product_id.id, diff=-2)
                do_min3 = rec.get_delivery_qty(product_id=product_id.id, diff=-3)
                do_plus0 = rec.get_delivery_qty(product_id=product_id.id, diff=0)
                do_plus1 = rec.get_delivery_qty(product_id=product_id.id, diff=1)
                do_plus2 = rec.get_delivery_qty(product_id=product_id.id, diff=2)

                wip_vals = []
                wip_data = rec.get_wip_detail(product_id.id)
                wip_no = 0
                for wip in wip_data :
                    wip_no += 1
                    wip_vals.append((0,0,{
                        'name': wip_no,
                        'routing_id': wip['workcenter_id'],
                        'quantity': wip['qty'],
                    }))
                # create line
                if not wip_vals :
                    line_vals.append({
                        'forecast_salesman_id': rec.id,
                        'product_id': product_id.id,
                        'name': no,
                        'part_customer': res['product_code'] or '',
                        'delivery_min3': do_min3,
                        'delivery_min2': do_min2,
                        'delivery_min1': do_min1,
                        'delivery_plus0': do_plus0,
                        'delivery_plus1': do_plus1,
                        'delivery_plus2': do_plus2,
                        'so_min3': so_min3,
                        'so_min2': so_min2,
                        'so_min1': so_min1,
                        'loss_sales': (so_min3+so_min2+so_min1) - (do_min3+do_min2+do_min1),
                        'total_finish_goods': product_id.qty_available,
                        'plant_id': res['plant_id'],
                        'customer_id': res['partner_id'],
                    })
                else :
                    rec.line_ids.create({
                        'forecast_salesman_id': rec.id,
                        'product_id': product_id.id,
                        'name': no,
                        'part_customer': res['product_code'] or '',
                        'delivery_min3': do_min3,
                        'delivery_min2': do_min2,
                        'delivery_min1': do_min1,
                        'delivery_plus0': do_plus0,
                        'delivery_plus1': do_plus1,
                        'delivery_plus2': do_plus2,
                        'so_min3': so_min3,
                        'so_min2': so_min2,
                        'so_min1': so_min1,
                        'loss_sales': (so_min3 + so_min2 + so_min1) - (do_min3 + do_min2 + do_min1),
                        'total_finish_goods': product_id.qty_available,
                        'plant_id': res['plant_id'],
                        'wip_qty_ids': wip_vals,
                        'customer_id': res['partner_id'],
                    })
            rec.insert_line(line_vals=line_vals)

    def insert_line(self, line_vals):
        insert_query = ''
        for line_val in line_vals :
            column = list(line_val.keys())
            values = ''
            for val in line_val.values():
                if isinstance(val, bool):
                    val = 'null'
                elif isinstance(val, str):
                    val = "'%s'"%(val)
                else :
                    val = str(val)
                if values :
                    values += ',%s'%val
                else :
                    values += '%s' % val
            if column and values:
                column = ','.join(column)
                values = values.replace('False','null').replace('None','null')
                insert_query += """
                    INSERT INTO
                        forecast_forecast_salesman_detail (%s)
                    VALUES (%s);
                """%(column,values)
        if insert_query :
            self._cr.execute(insert_query)

    def get_sale_qty(self, product_id, diff=0):
        self.ensure_one()
        start_date, end_date = self.periode_id.get_start_end_date(diff=diff, format='date')
        qty = 0
        query = """
            SELECT 
                sum(sol.product_uom_qty) as qty
            FROM 
                sale_order_line sol
            LEFT JOIN
                sale_order so on so.id = sol.order_id
            LEFT JOIN
                res_partner rp on rp.id = so.partner_id
            WHERE
                so.state in ('sale','done')
                and sol.product_id = %s
                and rp.user_id = %s
                and sol.delivery_date >= '%s'
                and sol.delivery_date <= '%s'
        """%(product_id, self.salesman_id.id, start_date, end_date)
        self._cr.execute(query)
        result = self._cr.dictfetchall()
        if result :
            qty = result[0]['qty']
        return qty or 0

    def get_delivery_qty(self, product_id, diff=0):
        self.ensure_one()
        start_date, end_date = self.periode_id.get_start_end_date(diff=diff)
        qty = 0
        query = """
            SELECT 
                sum(sm.product_uom_qty) as qty
            FROM 
                stock_move sm
            LEFT JOIN
                stock_picking sp on sp.id = sm.picking_id
            LEFT JOIN
                stock_location sl on sl.id = sm.location_id
            LEFT JOIN
                stock_location dl on dl.id = sm.location_dest_id
            LEFT JOIN
                res_partner rp on rp.id = sp.partner_id
            WHERE
                sm.product_id = %s
                and rp.user_id = %s
                and sl.usage = 'internal'
                and dl.usage = 'customer'
                and sm.state = 'done'
        """%(product_id, self.salesman_id.id)
        if diff > 0 :
            query += """
                and sp.scheduled_date >= '%s'
                and sp.scheduled_date <= '%s'
            """%(start_date, end_date)
        else :
            query += """
                and sp.date_done >= '%s'
                and sp.date_done <= '%s'
            """ % (start_date, end_date)
        self._cr.execute(query)
        result = self._cr.dictfetchall()
        if result :
            qty = result[0]['qty']
        return qty or 0

    def get_wip_detail(self, product_id):
        self.ensure_one()
        query = """
            SELECT
                workcenter_id, 
                sum(qty_producing) as qty 
            FROM 
                mrp_workorder
            WHERE 
                product_id = %s 
                and state = 'progress'
                and is_wip = True
            GROUP BY 
                workcenter_id
        """%(product_id)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    def get_wip(self, product_id):
        self.ensure_one()
        query = """
            SELECT
                sum(qty_producing) as qty 
            FROM 
                mrp_workorder
            WHERE 
                product_id = %s 
                and state = 'progress'
                and is_wip = True
        """ % (product_id)
        self._cr.execute(query)
        return self._cr.dictfetchall()[0]['qty'] or 0

    def action_import(self):
        self.ensure_one()
        if not self.export_data :
            return
        wb = open_workbook(file_contents=base64.decodestring(self.file_import))
        values = []
        for s in wb.sheets():
            for row in range(s.nrows):
                col_value = []
                for col in range(s.ncols):
                    value = (s.cell(row, col).value)
                    col_value.append(value)
                values.append(col_value)
        row = 0
        for data in values:
            row += 1
            if row in (1,2) or data[0] == 'Total' or not data[33] :
                continue
            line_id = self.env['forecast.forecast_salesman_detail'].browse(int(data[33]))
            line_id.write({
                'estimation_plus1': data[16] or 0,
                'estimation_plus2': data[17] or 0,
                'estimation_plus3': data[18] or 0,
            })
        self.file_import = False

    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone(self.env.user.tz or 'Asia/Jakarta'))

    def new_export_excel(self):
        self.ensure_one()
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)

        date_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        report_name = 'Forecast'
        filename = '%s %s.xlsx' % (report_name, date_string)

        worksheet = workbook.add_worksheet(report_name)

        # set column with
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 40)
        worksheet.set_column('C:C', 40)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 20)
        worksheet.set_column('H:H', 20)
        worksheet.set_column('I:I', 20)
        worksheet.set_column('J:J', 20)
        worksheet.set_column('K:K', 20)
        worksheet.set_column('L:L', 20)
        worksheet.set_column('M:M', 20)
        worksheet.set_column('N:N', 20)
        worksheet.set_column('O:O', 20)
        worksheet.set_column('P:P', 20)
        worksheet.set_column('Q:Q', 20)
        worksheet.set_column('R:R', 20)
        worksheet.set_column('S:S', 20)
        worksheet.set_column('T:T', 20)
        worksheet.set_column('U:U', 20)
        worksheet.set_column('V:V', 20)
        worksheet.set_column('W:W', 20)
        worksheet.set_column('X:X', 20)
        worksheet.set_column('Y:Y', 20)
        worksheet.set_column('Z:Z', 20)
        worksheet.set_column('AA:AA', 20)
        worksheet.set_column('AB:AB', 20)
        worksheet.set_column('AC:AC', 20)
        worksheet.set_column('AD:AD', 20)
        worksheet.set_column('AE:AE', 20)
        worksheet.set_column('AF:AF', 20)
        worksheet.set_column('AG:AG', 20)
        worksheet.set_column('AH:AH', 20)

        worksheet.merge_range('A2:A3', 'No', wbf['header_orange'])
        worksheet.merge_range('B2:B3', 'Type', wbf['header_orange'])
        worksheet.merge_range('C2:C3', 'Area Produksi', wbf['header_orange'])
        worksheet.merge_range('D2:D3', 'LT', wbf['header_orange'])
        worksheet.merge_range('E2:E3', 'STD PL', wbf['header_orange'])
        worksheet.merge_range('F2:I2', 'H', wbf['header_orange'])
        worksheet.write('F3', 'SO'+self.periode_id.get_month_name(-3), wbf['header_orange'])
        worksheet.write('G3', 'SO'+self.periode_id.get_month_name(-2), wbf['header_orange'])
        worksheet.write('H3', 'SO'+self.periode_id.get_month_name(-1), wbf['header_orange'])
        worksheet.write('I3', 'XSO', wbf['header_orange'])
        worksheet.merge_range('J2:M2', 'SALES', wbf['header_orange'])
        worksheet.write('J3', 'DO'+self.periode_id.get_month_name(-3), wbf['header_orange'])
        worksheet.write('K3', 'DO'+self.periode_id.get_month_name(-2), wbf['header_orange'])
        worksheet.write('L3', 'DO'+self.periode_id.get_month_name(-1), wbf['header_orange'])
        worksheet.write('M3', 'XSALES', wbf['header_orange'])
        worksheet.write('N2', 'A', wbf['header_orange'])
        worksheet.write('O2', 'B', wbf['header_orange'])
        worksheet.write('P2', 'C', wbf['header_orange'])
        worksheet.write('N3', 'LOSS SL', wbf['header_orange'])
        worksheet.write('O3', 'BL SO '+self.periode_id.get_month_name(), wbf['header_orange'])
        worksheet.write('P3', 'A + B', wbf['header_orange'])
        worksheet.write('Q2', 'D', wbf['header_orange'])
        worksheet.write('R2', 'E', wbf['header_orange'])
        worksheet.write('S2', 'F', wbf['header_orange'])
        worksheet.write('T2', 'A+D+E+F=G', wbf['header_orange'])
        worksheet.write('Q3', 'REQ '+self.periode_id.get_month_name(1), wbf['header_orange'])
        worksheet.write('R3', 'REQ '+self.periode_id.get_month_name(2), wbf['header_orange'])
        worksheet.write('S3', 'REQ '+self.periode_id.get_month_name(3), wbf['header_orange'])
        worksheet.write('T3', 'TOTAL REQ', wbf['header_orange'])
        worksheet.write('U2', 'I', wbf['header_orange'])
        worksheet.write('U3', 'WIP', wbf['header_orange'])
        worksheet.merge_range('V2:W2', 'J', wbf['header_orange'])
        worksheet.write('V3', 'FGLOCAL', wbf['header_orange'])
        worksheet.write('W3', 'FGIMP', wbf['header_orange'])
        worksheet.write('X2', 'H+I+J=K', wbf['header_orange'])
        worksheet.write('Y2', 'K-(A+D)=L', wbf['header_orange'])
        worksheet.write('Z2', 'L-E=M', wbf['header_orange'])
        worksheet.write('AA2', 'M-F=N', wbf['header_orange'])
        worksheet.write('X3', 'TOTAL', wbf['header_orange'])
        worksheet.write('Y3', 'A+D PROD', wbf['header_orange'])
        worksheet.write('Z3', 'E PROD', wbf['header_orange'])
        worksheet.write('AA3', 'F PROD', wbf['header_orange'])
        worksheet.merge_range('AB2:AC2', 'O', wbf['header_orange'])
        worksheet.merge_range('AD2:AE2', 'P', wbf['header_orange'])
        worksheet.merge_range('AF2:AG2', 'Q', wbf['header_orange'])
        worksheet.write('AB3', '', wbf['header_orange'])
        worksheet.write('AC3', '', wbf['header_orange'])
        worksheet.write('AD3', '', wbf['header_orange'])
        worksheet.write('AE3', '', wbf['header_orange'])
        worksheet.write('AF3', '', wbf['header_orange'])
        worksheet.write('AG3', '', wbf['header_orange'])
        worksheet.merge_range('AH2:AH3', 'Database ID', wbf['header_orange'])

        row = first_row = 4
        no = 1
        for line in self.line_ids :
            current_sales = self.get_sale_qty(line.product_id.id)
            fg_local = 0
            fg_imp = 0
            if line.product_id.default_code :
                if line.product_id.default_code[:2] == '01' :
                    fg_local = line.product_id.qty_available
                elif line.product_id.default_code[:2] == '02' :
                    fg_imp = line.product_id.qty_available
            worksheet.write('A%s' % row, no, wbf['content_number'])
            worksheet.write('B%s' % row, line.product_id.name, wbf['content'])
            worksheet.write('C%s' % row, line.forecast_salesman_id.company_id.name, wbf['content'])
            worksheet.write('D%s' % row, line.product_id.lt or '', wbf['content_float'])
            worksheet.write('E%s' % row, line.product_id.Std_PolyBox or '', wbf['content_float'])
            worksheet.write('F%s' % row, line.delivery_min3 or '', wbf['content_float'])
            worksheet.write('G%s' % row, line.delivery_min2 or '', wbf['content_float'])
            worksheet.write('H%s' % row, line.delivery_min1 or '', wbf['content_float'])
            worksheet.write('I%s' % row, (line.delivery_min3+line.delivery_min2+line.delivery_min1)/3 if line.delivery_min3 or line.delivery_min2 or line.delivery_min1 else '', wbf['content_float'])
            worksheet.write('J%s' % row, line.so_min3 or '', wbf['content_float'])
            worksheet.write('K%s' % row, line.so_min2 or '', wbf['content_float'])
            worksheet.write('L%s' % row, line.so_min1 or '', wbf['content_float'])
            worksheet.write('M%s' % row, (line.so_min3+line.so_min2+line.so_min1)/3 if line.so_min3 or line.so_min2 or line.so_min1 else '', wbf['content_float'])
            worksheet.write('N%s' % row, line.loss_sales or '', wbf['content_float'])
            worksheet.write('O%s' % row, current_sales or '', wbf['content_float'])
            worksheet.write('P%s' % row, line.loss_sales + current_sales or '', wbf['content_float'])
            worksheet.write('Q%s' % row, line.estimation_plus1 or '', wbf['content_float'])
            worksheet.write('R%s' % row, line.estimation_plus2 or '', wbf['content_float'])
            worksheet.write('S%s' % row, line.estimation_plus3 or '', wbf['content_float'])
            worksheet.write('T%s' % row, line.estimation_plus1+line.estimation_plus2+line.estimation_plus3 or '', wbf['content_float'])
            worksheet.write('U%s' % row, self.get_wip(line.product_id.id) or '', wbf['content_float'])
            worksheet.write('V%s' % row, fg_local or '', wbf['content_float'])
            worksheet.write('W%s' % row, fg_imp or '', wbf['content_float'])
            worksheet.write_formula('X%s' % row, '{=I%s+U%s+V%s+W%s}' % (row, row, row, row), wbf['content_float'])
            worksheet.write_formula('Y%s' % row, '{=K%s-(A%s+D%s)}' % (row, row, row), wbf['content_float'])
            worksheet.write_formula('Z%s' % row, '{=Y%s-R%s}' % (row, row), wbf['content_float'])
            worksheet.write_formula('AA%s' % row, '{=Z%s-S%s}' % (row, row), wbf['content_float'])
            worksheet.write_formula('AB%s' % row, '{=IF(SUM(Y%s)<0,ABS(ROUNDUP(ABS(SUM(Y%s))/E%s,0)*E%s)," ")}' % (row, row, row, row), wbf['content_float'])
            worksheet.write_formula('AC%s' % row, '{=IF(SUM(AB%s)>0,AB%s-(ABS(Y%s)/E%s)*E%s," ")}' % (row, row, row, row, row), wbf['content_float'])
            worksheet.write_formula('AD%s' % row, '{=IF(AND(Z%s<0,AT%s>0),(ROUNDUP(ABS(AT%s)/E%s,0)*E%s)," ")}' % (row, row, row, row, row), wbf['content_float'])
            worksheet.write_formula('AE%s' % row, '{=IF(SUM(AD%s)>0,AD%s-AT%s," ")}' % (row, row, row), wbf['content_float'])
            worksheet.write_formula('AF%s' % row, '{=IF(AND(AA%s<0,SUM(AU%s)>0),ROUNDUP((ABS(AA%s)-SUM(AE%s))/E%s,0)*E%s," ")}' % (row, row, row, row, row, row), wbf['content_float'])
            worksheet.write('AG%s' % row, '' or '', wbf['content_float'])
            worksheet.write('AH%s' % row, line.id, wbf['content_number'])
            row += 1
            no += 1

        worksheet.merge_range('A%s:B%s' % (row, row), 'Total', wbf['total_orange'])
        worksheet.write('C%s' % row, '', wbf['total_orange'])
        worksheet.write('D%s' % row, '', wbf['total_orange'])
        worksheet.write('E%s' % row, '', wbf['total_orange'])
        worksheet.write_formula('F%s' % row, '{=subtotal(9,F%s:F%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('G%s' % row, '{=subtotal(9,G%s:G%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('H%s' % row, '{=subtotal(9,H%s:H%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('I%s' % row, '{=subtotal(9,I%s:I%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('J%s' % row, '{=subtotal(9,J%s:J%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('K%s' % row, '{=subtotal(9,K%s:K%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('L%s' % row, '{=subtotal(9,L%s:L%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('M%s' % row, '{=subtotal(9,M%s:M%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('N%s' % row, '{=subtotal(9,N%s:N%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('O%s' % row, '{=subtotal(9,O%s:O%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('P%s' % row, '{=subtotal(9,P%s:P%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('Q%s' % row, '{=subtotal(9,Q%s:Q%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('R%s' % row, '{=subtotal(9,R%s:R%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('S%s' % row, '{=subtotal(9,S%s:S%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('T%s' % row, '{=subtotal(9,T%s:T%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('U%s' % row, '{=subtotal(9,U%s:U%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('V%s' % row, '{=subtotal(9,V%s:V%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('W%s' % row, '{=subtotal(9,X%s:X%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('X%s' % row, '{=subtotal(9,X%s:X%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('Y%s' % row, '{=subtotal(9,Y%s:Y%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('Z%s' % row, '{=subtotal(9,Z%s:Z%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AA%s' % row, '{=subtotal(9,AA%s:AA%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AB%s' % row, '{=subtotal(9,AB%s:AB%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AC%s' % row, '{=subtotal(9,AC%s:AC%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AD%s' % row, '{=subtotal(9,AD%s:AD%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AE%s' % row, '{=subtotal(9,AE%s:AE%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AF%s' % row, '{=subtotal(9,AF%s:AF%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write_formula('AG%s' % row, '{=subtotal(9,AG%s:AG%s)}' % (first_row, row), wbf['total_float_orange'])
        worksheet.write('AH%s' % row, '', wbf['total_float_orange'])

        workbook.close()
        out = base64.encodestring(fp.getvalue())
        self.write({
            'export_data': out,
            'name_ex': filename,
        })
        fp.close()
        url = 'web/content/?model=%s&id=%s&field=export_data&download=true&filename=%s' % (self._name, self.id, filename)
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': url,
        }

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': '#FFFFDB', 'font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': colors['orange'], 'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': colors['yellow'], 'font_color': '#000000'})
        wbf['header_yellow'].set_border()

        wbf['header_no'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': '#FFFFDB', 'font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')

        wbf['footer'] = workbook.add_format({'align': 'left'})

        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()

        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right()

        wbf['title_doc'] = workbook.add_format({'bold': 1, 'align': 'left'})
        wbf['title_doc'].set_font_size(12)

        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)

        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right()

        wbf['content_float'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.00'})
        wbf['content_float'].set_right()
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right()
        wbf['content_number'].set_left()

        wbf['content_percent'] = workbook.add_format({'align': 'right', 'num_format': '0.00%'})
        wbf['content_percent'].set_right()
        wbf['content_percent'].set_left()

        wbf['total_float'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['white_orange'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()

        wbf['total_number'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['white_orange'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()

        wbf['total_orange'] = workbook.add_format({'bold': 1, 'bg_color': colors['white_orange'], 'align': 'center', 'valign': 'vcenter'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['yellow'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()

        wbf['total_number_yellow'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['yellow'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()

        wbf['total_yellow'] = workbook.add_format({'bold': 1, 'bg_color': colors['yellow'], 'align': 'center', 'valign': 'vcenter'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['orange'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()

        wbf['total_number_orange'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['orange'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()

        wbf['total_orange'] = workbook.add_format({'bold': 1, 'bg_color': colors['orange'], 'align': 'center', 'valign': 'vcenter'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()

        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()

        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()

        return wbf, workbook
    