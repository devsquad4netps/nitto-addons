import xlsxwriter
import base64

from mako.runtime import Context
from odoo import fields, models, api, _
from io import BytesIO
from datetime import datetime, timedelta, date
from pytz import timezone
from odoo.exceptions import ValidationError
import pytz
import calendar

ALPHABETS = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ']

class ControlItem(models.Model):
    _name = "control.item"
    _description = "Control Item"
    _order = "id desc"

    def _default_period(self):
        period_id = self.env['forecast.periode'].search([
            ('bulan','=','%02d'%(fields.Datetime.now().month)),
            ('tahun','=',fields.Datetime.now().strftime('%Y')),
        ], limit=1)
        return period_id.id

    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone(self.env.user.tz or 'UTC'))

    datas = fields.Binary('File', readonly=True)
    datas_fname = fields.Char('Filename', readonly=True)
    name = fields.Char(required=True)
    period_id = fields.Many2one(
        comodel_name='forecast.periode',
        string='Period',
        default=_default_period,
        required=True)
    company_ids = fields.Many2many(
        comodel_name='res.company',
        required=True,
        domain=[('parent_id','!=',False)],
        string='Company')
    item_line = fields.One2many(
        comodel_name='control.item.line',
        inverse_name='control_id',
        string='Control Item Line',
        required=False)

    @api.multi
    def action_calculate(self):
        for rec in self :
            pref_period_id = self.period_id.get_pref_period(diff=3)
            forecast_ids = self.env['forecast.forecast_final'].search([
                ('period_id','=',pref_period_id.id)
            ])
            months = []
            for x in [-1,-2]:
                next_period_id = self.period_id.get_pref_period(diff=x)
                # months.append(f'{next_period_id.tahun}-{next_period_id.bulan}')
                months.append('%s-%s'%(next_period_id.tahun, next_period_id.bulan))
            working_days = self.period_id.get_working_days(months=months)
            start_date, end_date = self.period_id.get_start_end_date()
            pref1_start_date, pref1_end_date = self.period_id.get_start_end_date(diff=-1)
            pref2_start_date, pref2_end_date = self.period_id.get_start_end_date(diff=-2)
            pref3_start_date, pref3_end_date = self.period_id.get_start_end_date(diff=-3)
            self._cr.execute("""
                DELETE FROM 
                    control_item_line 
                WHERE 
                    control_id = %s
            """ % (rec.id))
            #import pdb;pdb.set_trace()
            query = """
                SELECT
                    wo.product_id,
                    0 as qty_month,
                    (SUM(COALESCE(ffdt.estimation_plus1,0))+SUM(COALESCE(ffdt.estimation_plus2,0)))/%s as qty_day,
                    --SUM(COALESCE(sq.quantity, 0)) as qty_finish_good,
                    (SELECT SUM(COALESCE(sq.quantity,0)) as quantity FROM stock_quant sq LEFT JOIN stock_location l ON l.id = sq.location_id WHERE l.usage = 'internal' AND l.active is true AND sq.product_id=wo.product_id) as qty_finish_good,
                    0 as day,
                    min(mp.customer_id) as partner_id,
                    0 as lt,
                    0 as sisa_kartu,
                    0 as kurang_kartu,
                    0 as lebih_kartu,
                    0 as lt2,
                    0 as qty,
                    0 as new_card,
                    0 as lt3,
                    SUM(COALESCE(current_sale.product_uom_qty,0)) - SUM(COALESCE(current_sale.qty_delivered,0)) as outstanding_sale,
                    SUM(COALESCE(pref3_sale.qty_delivered,0)) as sale_prev3,
                    SUM(COALESCE(pref2_sale.qty_delivered,0)) as sale_prev2,
                    SUM(COALESCE(pref1_sale.qty_delivered,0)) as sale_prev1,
                    SUM(COALESCE(current_sale.qty_delivered,0)) as sale_current,
                    SUM(COALESCE(wo.qty_produced_real,0)) as qty_produced_real
                FROM 
                    forecast_forecast_final fdt
                LEFT JOIN
                    forecast_forecast_final_detail ffdt ON ffdt.forecast_id = fdt.id
                LEFT JOIN
                    mrp_workorder wo ON wo.product_id = ffdt.product_id
                LEFT JOIN
                    mrp_production mp ON mp.id = wo.production_id
                LEFT JOIN
                    res_partner rp ON rp.id = mp.customer_id
                LEFT JOIN
                    res_company rc ON rc.id = mp.company_id
                -- LEFT JOIN
                --     (SELECT sq.product_id, SUM(COALESCE(sq.quantity,0)) as quantity FROM stock_quant sq LEFT JOIN stock_location l ON l.id = sq.location_id WHERE l.usage = 'internal' AND l.active is true GROUP BY sq.product_id) sq ON sq.product_id = wo.product_id
                LEFT JOIN
                    (SELECT fl.product_id, SUM(COALESCE(fl.estimation_plus1,0)) as estimation_plus1, SUM(COALESCE(fl.estimation_plus2,0)) as estimation_plus2 FROM forecast_forecast_final_detail fl LEFT JOIN forecast_forecast_final f ON f.id = fl.forecast_id WHERE f.id in %s GROUP BY fl.product_id) f ON f.product_id = wo.product_id
                LEFT JOIN
                    (SELECT sol.product_id, SUM(COALESCE(sol.product_uom_qty,0)) as product_uom_qty, SUM(COALESCE(sol.qty_delivered,0)) as qty_delivered FROM sale_order_line sol LEFT JOIN sale_order so ON so.id = sol.order_id WHERE so.state in ('sale','done') and so.confirmation_date >= '%s' and so.confirmation_date <= '%s' GROUP BY sol.product_id) current_sale ON current_sale.product_id = wo.product_id
                LEFT JOIN
                    (SELECT sol.product_id, SUM(COALESCE(sol.product_uom_qty,0)) as product_uom_qty, SUM(COALESCE(sol.qty_delivered,0)) as qty_delivered FROM sale_order_line sol LEFT JOIN sale_order so ON so.id = sol.order_id WHERE so.state in ('sale','done') and so.confirmation_date >= '%s' and so.confirmation_date <= '%s' GROUP BY sol.product_id) pref1_sale ON pref1_sale.product_id = wo.product_id
                LEFT JOIN
                    (SELECT sol.product_id, SUM(COALESCE(sol.product_uom_qty,0)) as product_uom_qty, SUM(COALESCE(sol.qty_delivered,0)) as qty_delivered FROM sale_order_line sol LEFT JOIN sale_order so ON so.id = sol.order_id WHERE so.state in ('sale','done') and so.confirmation_date >= '%s' and so.confirmation_date <= '%s' GROUP BY sol.product_id) pref2_sale ON pref2_sale.product_id = wo.product_id
                LEFT JOIN
                    (SELECT sol.product_id, SUM(COALESCE(sol.product_uom_qty,0)) as product_uom_qty, SUM(COALESCE(sol.qty_delivered,0)) as qty_delivered FROM sale_order_line sol LEFT JOIN sale_order so ON so.id = sol.order_id WHERE so.state in ('sale','done') and so.confirmation_date >= '%s' and so.confirmation_date <= '%s' GROUP BY sol.product_id) pref3_sale ON pref3_sale.product_id = wo.product_id
                WHERE 
                    mp.company_id in %s
                    AND 
                    fdt.period_id = %s
                    AND 
                    -- wo.state = 'progress' and wo.is_wip is true
                    (wo.state in ('progress','done') and wo.is_wip is true)
                GROUP BY
                    wo.product_id
            """%(
                working_days,
                str(tuple(forecast_ids.ids)).replace(',)',')') if forecast_ids else '(null)',
                start_date, end_date,
                pref1_start_date, pref1_end_date,
                pref2_start_date, pref2_end_date,
                pref3_start_date, pref3_end_date,
                str(tuple(self.company_ids.ids)).replace(',)',')') ,
                self.period_id.id,
            )
            # query += ' LIMIT 100 '
            self._cr.execute(query)
            result = self._cr.dictfetchall()
            no = 1

            for res in result :
                query = """
                    INSERT INTO
                        control_item_line(
                            name,
                            control_id,
                            product_id,
                            qty_month,
                            qty_day,
                            qty_finish_good,
                            day,
                            partner_id,
                            lt,
                            sisa_kartu,
                            kurang_kartu,
                            lebih_kartu,
                            lt2,
                            qty,
                            new_card,
                            lt3,
                            outstanding_sale,
                            sale_prev3,
                            sale_prev2,
                            sale_prev1,
                            sale_current,
                            total_wip
                        )
                    VALUES(
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s
                    )
                """%(
                    no,
                    rec.id,
                    res['product_id'],
                    res['qty_month'],
                    res['qty_day'],
                    res['qty_finish_good'],
                    res['day'],
                    res['partner_id'],
                    res['lt'],
                    res['sisa_kartu'],
                    res['kurang_kartu'],
                    res['lebih_kartu'],
                    res['lt2'],
                    res['qty'],
                    res['new_card'],
                    res['lt3'],
                    res['outstanding_sale'],
                    res['sale_prev3'],
                    res['sale_prev2'],
                    res['sale_prev1'],
                    res['sale_current'],
                    res['qty_produced_real'],
                )
                self._cr.execute(query)
                no += 1
            rec.finishing_calculate()

    @api.multi
    def finishing_calculate(self):
        self.ensure_one()
        current_working_days = self.period_id.get_working_days()
        query_update = ''
        for line in self.item_line :
            query = """
                SELECT
                    wo.product_id,
                    wc.group_report,
                    mp.company_id,
                    SUM(COALESCE(wo.qty_produced_real,0)) as qty_produced_real
                FROM 
                    mrp_workorder wo
                LEFT JOIN
                    mrp_production mp ON mp.id = wo.production_id
                LEFT JOIN
                    mrp_workcenter wc ON wc.id = wo.workcenter_id
                LEFT JOIN
                    res_company rc ON rc.id = mp.company_id
                WHERE 
                    (wo.state in ('progress','done') and wo.is_wip is true)
                    and mp.company_id in %s
                    and wo.product_id = %s
                GROUP BY
                    wo.product_id,
                    wc.group_report,
                    mp.company_id
            """ % (
                str(tuple(self.company_ids.ids)).replace(',)',')') ,
                line.product_id.id
            )
            self._cr.execute(query)
            result = self._cr.dictfetchall()
            group_report_companies = {}
            for res in result :
                group_report_companies.setdefault(res['group_report'], {})
                group_report_companies[res['group_report']][res['company_id']] = res['qty_produced_real']
            for group_report, company_qty in group_report_companies.items():
                self._cr.execute("""
                    INSERT INTO
                        control_item_line_wc_group(
                            line_id,
                            group_report
                        )
                    VALUES(
                        %s,
                        '%s'
                    )
                    RETURNING id
                """%(
                    line.id,
                    group_report or ''
                ))
                line_wc_id = self._cr.dictfetchall()[0]['id']
                for company_id, qty in company_qty.items():
                    self._cr.execute("""
                        INSERT INTO
                            control_item_line_wc_group_company(
                                line_wc_id,
                                company_id,
                                qty
                            )
                        VALUES(
                            %s,
                            %s,
                            %s
                        )
                        RETURNING id
                    """%(
                        line_wc_id,
                        company_id,
                        qty
                    ))
            lt = 0
            area_hd_id = False
            sisa_krt = 0
            kurang_krt = 0
            lebih_krt = 0
            lt2 = 0
            qty = 0
            new_card = 0
            lt3 = 0
            if line.qty_month :
                lt = (line.qty_finish_good + line.total_wip) / line.qty_month
            self._cr.execute("""
                SELECT
                    lgc.company_id
                FROM
                    control_item_line_wc_group_company lgc
                LEFT JOIN
                    control_item_line_wc_group lg ON lg.id = lgc.line_wc_id
                WHERE
                    lg.line_id = %s
                    and lg.group_report = 'Heading'
                ORDER BY
                    lgc.qty desc
                LIMIT 1
            """%(
                line.id
            ))
            result = self._cr.dictfetchall()
            if result :
                area_hd_id = result[0]['company_id']
            self._cr.execute("""
                SELECT
                    SUM(COALESCE(lgc.qty, 0)) as qty
                FROM
                    control_item_line_wc_group_company lgc
                LEFT JOIN
                    control_item_line_wc_group lg ON lg.id = lgc.line_wc_id
                WHERE
                    lg.line_id = %s
                    and lg.group_report = 'Heading'
            """ % (
                line.id
            ))
            result = self._cr.dictfetchall()
            if result :
                sisa_krt = result[0]['qty'] or 0
            category = line.product_id.Ket_Furnice or ''
            category = category.lower()
            if area_hd_id :
                company_name = self.env['res.company'].browse(area_hd_id).name.lower()
            else :
                company_name = ''
            if ('tangerang' in company_name and 'double header' in category) or 'bekasi' in company_name :
                kurang_krt = (line.total_wip + line.qty_finish_good) - (line.qty_month * 1.2)
                lebih_krt = (line.total_wip + line.qty_finish_good) - (line.qty_month * 1.2)
            elif ('bekasi' in company_name and '2d3b' in category) :
                kurang_krt = (line.total_wip + line.qty_finish_good) - (line.qty_month * 1.5)
                lebih_krt = (line.total_wip + line.qty_finish_good) - (line.qty_month * 1.5)
            if kurang_krt > 0 :
                kurang_krt = 0
            elif kurang_krt < 0 :
                kurang_krt = kurang_krt * -1
            if lebih_krt < 0 :
                lebih_krt = 0
            qty_month = line.qty_day * current_working_days
            if qty_month :
                lt2 = (line.qty_finish_good + line.total_wip) / qty_month
            if line.plt :
                qty = round(kurang_krt/line.plt, 0)
            if qty > 0 :
                new_card = qty * line.plt
            if qty_month :
                lt3 = (line.qty_finish_good + line.total_wip + sisa_krt + new_card) / qty_month

            query_update += """
                UPDATE
                    control_item_line l
                SET
                    qty_month = %s,
                    day = CASE WHEN l.qty_day > 0 THEN l.qty_finish_good / l.qty_day ELSE 0 END,
                    default_code = p.default_code,
                    plt = %s,
                    lt = %s,
                    area_hd_id = %s,
                    category = '%s',
                    sisa_kartu = '%s',
                    kurang_kartu = '%s',
                    lebih_kartu = '%s',
                    lt2 = '%s',
                    qty = '%s',
                    new_card = '%s',
                    lt3 = '%s'
                FROM
                    product_product p
                WHERE
                    p.id = l.product_id
                    and l.id = %d;
            """ % (
                qty_month,
                line.product_id.Std_PolyBox or 0,
                lt,
                area_hd_id or 'null',
                line.product_id.Ket_Furnice or '',
                sisa_krt,
                kurang_krt,
                lebih_krt,
                lt2,
                qty,
                new_card,
                lt3,
                line.id
            )
        if query_update :
            self._cr.execute(query_update)

    @api.multi
    def action_export_excel(self):
        self.ensure_one()
        if not self.item_line :
            raise ValidationError(_('Item line not available, please calculate first.'))
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Item Control'
        filename = '%s %s %s' % (report_name, self.name, date_string)

        datetime_format = '%Y-%m-%d %H:%M:%S'
        utc = datetime.now().strftime(datetime_format)
        utc = datetime.strptime(utc, datetime_format)
        tz = self.get_default_date_model().strftime(datetime_format)
        tz = datetime.strptime(tz, datetime_format)
        duration = tz - utc
        hours = duration.seconds / 60 / 60
        if hours > 1 or hours < 1:
            hours = str(hours) + ' hours'
        else:
            hours = str(hours) + ' hour'

        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)

        worksheet = workbook.add_worksheet(report_name)
        worksheet.write('A5', 'Period %s' % (self.period_id.display_name), wbf['content_datetime'])

        row = 7

        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 40)
        worksheet.set_column('C1:C1', 20)
        worksheet.set_column('D1:D1', 20)
        worksheet.set_column('E1:E1', 20)
        worksheet.set_column('F1:F1', 20)
        worksheet.set_column('G1:G1', 20)
        worksheet.set_column('H1:H1', 5)

        worksheet.merge_range('A%s:A%s'%(row, row+1), 'No', wbf['header_no'])
        worksheet.merge_range('B%s:B%s'%(row, row+1), 'Item Name', wbf['header_orange'])
        worksheet.merge_range('C%s:C%s'%(row, row+1), 'Item ID', wbf['header_orange'])
        worksheet.merge_range('D%s:D%s'%(row, row+1), 'Qty/Month', wbf['header_orange'])
        worksheet.merge_range('E%s:E%s'%(row, row+1), 'Qty/Day', wbf['header_orange'])
        worksheet.merge_range('F%s:F%s'%(row, row+1), 'Qty (Finish Good)', wbf['header_orange'])
        worksheet.merge_range('G%s:G%s'%(row, row+1), 'Day', wbf['header_orange'])
        worksheet.merge_range('H%s:H%s'%(row, row+1), 'Judg', wbf['header_orange'])

        # get plant group by work center group
        query = """
            SELECT DISTINCT
                c.second_name as company,
                lg.group_report
            FROM
                control_item_line_wc_group_company lgc
            LEFT JOIN
                control_item_line_wc_group lg ON lg.id = lgc.line_wc_id
            LEFT JOIN
                res_company c ON C.id = lgc.company_id
            WHERE
                lg.line_id in %s
            ORDER BY
                lg.group_report, c.second_name
        """%(str(tuple(self.item_line.ids)).replace(',)',')'))
        self._cr.execute(query)
        result = self._cr.dictfetchall()
        wc_group = {}
        for res in result :
            wc_group.setdefault(res['group_report'], [])
            wc_group[res['group_report']].append(res['company'])

        col = 8
        for gr, p_list in wc_group.items():
            plant_count = len(p_list)
            if plant_count > 1 :
                worksheet.merge_range(6, col, 6, col + plant_count - 1, gr, wbf['header_orange'])
            else :
                worksheet.write(6, col, gr, wbf['header_orange'])
            for p in p_list :
                worksheet.set_column(col, col, 20)
                worksheet.write(7, col, p, wbf['header_orange'])
                col += 1

        columns = [
            ('Customer', 40, 'char', 'char'),
            ('LT', 20, 'float', 'float'),
            ('Area HD', 20, 'char', 'char'),
            ('Category', 20, 'char', 'char'),
            ('Sisa Kartu', 20, 'float', 'float'),
            ('Kurang Kartu', 20, 'float', 'float'),
            ('Lebih Kartu', 20, 'float', 'float'),
            ('STD PLT', 20, 'float', 'float'),
            ('LT 2', 20, 'float', 'float'),
            ('Qty', 20, 'float', 'float'),
            ('New Card', 20, 'float', 'float'),
            ('LT 3', 20, 'float', 'float'),
            ('BL SO', 20, 'float', 'float'),
            ('SAL_%s'%(self.period_id.get_month_name(diff=-3)), 20, 'float', 'float'),
            ('SAL_%s'%(self.period_id.get_month_name(diff=-2)), 20, 'float', 'float'),
            ('SAL_%s'%(self.period_id.get_month_name(diff=-1)), 20, 'float', 'float'),
            ('SAL%s_CURR'%(self.period_id.get_month_name(diff=0)), 20, 'float', 'float'),
        ]

        for column in columns :
            worksheet.set_column(col, col, column[1])
            worksheet.merge_range(6, col, 7, col, column[0], wbf['header_orange'])
            col += 1

        row += 2
        row_start = row
        no = 1

        for line in self.item_line:
            worksheet.write('A%s' % row, line.name, wbf['content_no'])
            worksheet.write('B%s' % row, line.product_id.display_name or '', wbf['content'])
            worksheet.write('C%s' % row, line.default_code or '', wbf['content'])
            worksheet.write('D%s' % row, line.qty_month, wbf['content_float'])
            worksheet.write('E%s' % row, line.qty_day, wbf['content_float'])
            worksheet.write('F%s' % row, line.qty_finish_good, wbf['content_float'])
            worksheet.write('G%s' % row, line.day, wbf['content_float'])
            worksheet.write('H%s' % row, line.judg, wbf['content_no'])
            col = 8
            for gr, p_list in wc_group.items():
                for p in p_list :
                    line_group_company_id = self.env['control.item.line.wc.group.company'].search([
                        ('line_wc_id.line_id','=',line.id),
                        ('company_id.second_name','=',p),
                        ('line_wc_id.group_report','=',gr),
                    ], limit=1)
                    worksheet.write(row-1, col, line_group_company_id.qty, wbf['content_float'])
                    col += 1
            worksheet.write(row-1, col, line.partner_id.name or '', wbf['content'])
            col += 1
            worksheet.write(row-1, col, line.lt, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.area_hd_id.second_name or '', wbf['content'])
            col += 1
            worksheet.write(row-1, col, line.category or '', wbf['content'])
            col += 1
            worksheet.write(row-1, col, line.sisa_kartu, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.kurang_kartu, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.lebih_kartu, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.plt, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.lt2, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.qty, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.new_card, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.lt3, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.outstanding_sale, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.sale_prev3, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.sale_prev2, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.sale_prev1, wbf['content_float'])
            col += 1
            worksheet.write(row-1, col, line.sale_current, wbf['content_float'])
            col += 1
            row += 1
            no += 1

        row_end = row - 1
        worksheet.merge_range('A%s:B%s' % (row, row), 'Grand Total', wbf['total_orange'])
        worksheet.write('C%s' % row, '', wbf['total_orange'])
        worksheet.write_formula('D%s' % row, '{=subtotal(9,D%s:C%s)}' % (row_start, row_end), wbf['total_float_orange'])
        worksheet.write_formula('E%s' % row, '{=subtotal(9,E%s:E%s)}' % (row_start, row_end), wbf['total_float_orange'])
        worksheet.write_formula('F%s' % row, '{=subtotal(9,F%s:F%s)}' % (row_start, row_end), wbf['total_float_orange'])
        worksheet.write_formula('G%s' % row, '{=subtotal(9,G%s:G%s)}' % (row_start, row_end), wbf['total_float_orange'])
        worksheet.write('H%s' % row, '', wbf['total_orange'])
        col = 8
        for gr, p_list in wc_group.items():
            for p in p_list:
                worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
                col += 1
        worksheet.write(row-1, col, '', wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write(row-1, col, '', wbf['total_float_orange'])
        col += 1
        worksheet.write(row-1, col, '', wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])
        col += 1
        worksheet.write_formula(row-1, col, '{=subtotal(9,%s%s:%s%s)}' % (ALPHABETS[col], row_start, ALPHABETS[col], row_end), wbf['total_float_orange'])

        worksheet.merge_range('A2:%s3'%(ALPHABETS[col]), report_name, wbf['title_doc'])
        worksheet.write('A%s' % (row + 2), 'Date %s (%s)' % (datetime_string, self.env.user.tz or 'UTC'), wbf['content_datetime'])
        workbook.close()
        out = base64.encodestring(fp.getvalue())
        self.write({'datas': out, 'datas_fname': filename})
        fp.close()
        filename += '%2Exlsx'

        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'web/content/?model=' + self._name + '&id=' + str(self.id) + '&field=datas&download=true&filename=' + filename,
        }

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': '#FFFFDB', 'font_color': '#000000', 'font_name': 'Georgia'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'valign': 'vcenter', 'bg_color': colors['orange'], 'font_color': '#000000',
             'font_name': 'Georgia'})
        wbf['header_orange'].set_border()
        wbf['header_orange'].set_text_wrap()

        wbf['header_yellow'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': colors['yellow'], 'font_color': '#000000',
             'font_name': 'Georgia'})
        wbf['header_yellow'].set_border()

        wbf['header_no'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': colors['orange'], 'font_color': '#000000', 'font_name': 'Georgia'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')

        wbf['footer'] = workbook.add_format({'align': 'left', 'font_name': 'Georgia'})

        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss', 'font_name': 'Georgia'})
        wbf['content_datetime'].set_left()
        # wbf['content_datetime'].set_right()

        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd', 'font_name': 'Georgia'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right()

        wbf['title_doc'] = workbook.add_format({
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 20,
            'font_name': 'Georgia',
        })

        wbf['company'] = workbook.add_format({'align': 'left', 'font_name': 'Georgia'})
        wbf['company'].set_font_size(11)

        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right()

        wbf['content_no'] = workbook.add_format({'align': 'center', 'num_format': '#,##0', 'font_name': 'Georgia'})
        wbf['content_no'].set_right()
        wbf['content_no'].set_left()

        wbf['content_float'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.00', 'font_name': 'Georgia'})
        wbf['content_float'].set_right()
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0', 'font_name': 'Georgia'})
        wbf['content_number'].set_right()
        wbf['content_number'].set_left()

        wbf['content_percent'] = workbook.add_format({'align': 'right', 'num_format': '0.00%', 'font_name': 'Georgia'})
        wbf['content_percent'].set_right()
        wbf['content_percent'].set_left()

        wbf['total_float'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['white_orange'], 'align': 'right', 'num_format': '#,##0.00',
             'font_name': 'Georgia'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()

        wbf['total_number'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['white_orange'], 'bold': 1, 'num_format': '#,##0',
             'font_name': 'Georgia'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()

        wbf['total'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['white_orange'], 'align': 'center', 'font_name': 'Georgia'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['yellow'], 'align': 'right', 'num_format': '#,##0.00',
             'font_name': 'Georgia'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()

        wbf['total_number_yellow'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['yellow'], 'bold': 1, 'num_format': '#,##0', 'font_name': 'Georgia'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()

        wbf['total_yellow'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['yellow'], 'align': 'center', 'font_name': 'Georgia'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['orange'], 'align': 'right', 'num_format': '#,##0.00',
             'font_name': 'Georgia'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()

        wbf['total_number_orange'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['orange'], 'bold': 1, 'num_format': '#,##0', 'font_name': 'Georgia'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()

        wbf['total_orange'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['orange'], 'align': 'center', 'font_name': 'Georgia'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()

        wbf['header_detail_space'] = workbook.add_format({'font_name': 'Georgia'})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()

        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2', 'font_name': 'Georgia'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()

        return wbf, workbook

    @api.model
    def fields_view_get(self, view_id=None, view_type=None, context=None, toolbar=False, submenu=False):
        result = super(ControlItem, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar,
            submenu=submenu)
        if result['type'] == 'form' and result['model'] == 'control.item' and self._context.get('params',{}).get('id', False):
            control_id = self.search([('id','=',self._context['params']['id'])])
            sale_prev3 = 'SAL_%s' % (control_id.period_id.get_month_name(diff=-3))
            sale_prev2 = 'SAL_%s' % (control_id.period_id.get_month_name(diff=-2))
            sale_prev1 = 'SAL_%s' % (control_id.period_id.get_month_name(diff=-1))
            sale_current = 'SAL%s_CURR' % (control_id.period_id.get_month_name(diff=0))
            result['fields']['item_line']['views']['tree']['fields']['sale_prev3']['string'] = sale_prev3
            result['fields']['item_line']['views']['tree']['fields']['sale_prev2']['string'] = sale_prev2
            result['fields']['item_line']['views']['tree']['fields']['sale_prev1']['string'] = sale_prev1
            result['fields']['item_line']['views']['tree']['fields']['sale_current']['string'] = sale_current
        return result
