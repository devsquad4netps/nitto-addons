# -*- coding: utf-8 -*-
{
    'name': "Forecast Extended",

    'summary': """
        Extend module for forecast
    """,

    'description': """

    """,

    'author': "Vitraining",
    'website': "http://www.vitraining.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Reporting',
    'version': '1.2',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'forecast'
    ],

    # always loaded
    'data': [
        'views/forecast_forecast_salesman_views.xml',
        'views/forecast_forecast_final_views.xml',
        'views/forecast_forecast_salesman_detail_views.xml',
        'views/forecast_forecast_final_detail_views.xml',
        'views/control_item_views.xml',
        'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}