from odoo import api, fields, models, _
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)

class ReportMRPWizard(models.TransientModel):
    _name = "vit.report.mrp.wizard"
    _description = "Report MRP"

    @api.onchange('group_report')
    def onchange_group_report(self):
        if self.group_report:
            self.workcenter_ids = False

    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes") 
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    group_report = fields.Selection([('Cutting','Cutting'),('CF','CF'),('Furnished','FURNISHED'),('Final Quality','FINAL QUALITY'),('Heading','HEADING'),('Machine','MACHINE'),('Packing','PACKING'),('Plating','PLATING'),('Rolling','ROLLING'),('Three Bond','THREE BOND'),('Washing','WASHING')],"Group Workcenter", required=True)
    state = fields.Selection([('progress','In Progress'),('done','Finished')], string='Status', default='done', required=True,
        help="* In Progress: Data yang diambil Work Order yang berstatus In Progress dan Ready"
             "\n* Finished: Data yang diambil Work Order yang berstatus Finished dan field Production Report di workcenternya dicentang")
    shift1 = fields.Boolean('Shift 1', default=True)
    shift2 = fields.Boolean('Shift 2', default=True)
    shift3 = fields.Boolean('Shift 3', default=True)
    workcenter_ids = fields.Many2many('mrp.workcenter',string='Workcenter')

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'center','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        #wbf['header_table'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_footer'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['dark_grey']})

        wbf['content_string'] = workbook.add_format({'align': 'left'})
        
        return wbf, workbook

    @api.multi
    def action_print_report_mrp(self):
        self.ensure_one()
        for report in self :
            lot_temp = self.env['stock.production.lot.temp']
            group_report = dict(self._fields['group_report'].selection).get(report.group_report)
            report_name = 'LAPORAN PRODUKSI %s'%(group_report)
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + "+"
            company = company[:-1]
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")

            row = 1
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = report.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name)
            worksheet.set_column('A1:A1', 5)
            worksheet.set_column('B1:B1', 10)
            worksheet.set_column('C1:C1', 35)
            worksheet.set_column('D1:D1', 25)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 35)
            worksheet.set_column('N1:N1', 15)
            worksheet.set_column('O1:O1', 15)
            worksheet.set_column('P1:P1', 15)
            worksheet.set_column('Q1:Q1', 15)
            worksheet.set_column('R1:R1', 15)
            worksheet.set_column('S1:S1', 15)
            worksheet.set_column('T1:T1', 15)

            # local_tz = pytz.timezone('Asia/Jakarta')
            # date_print = datetime.today().replace(tzinfo=local_tz)
            # date_print = date_print.astimezone(timezone('Asia/Jakarta'))
            date_print = datetime.today() + timedelta(hours=+7)
            worksheet.write('A%s'%(row), str(date_print)[:19], wbf['title_doc'])
            worksheet.merge_range('A%s:R%s'%(row+1,row+1), 'LAPORAN PRODUKSI %s PT. NITTO ALAM INDONESIA (%s)'%(group_report,company), wbf['company'])
            worksheet.merge_range('A%s:R%s'%(row+2,row+2), report.date_start.strftime("%d %b %Y")+' - '+report.date_end.strftime("%d %b %Y"), wbf['company'])
            
            # header tabel
            row+=4
            worksheet.write('A%s'%(row), 'NO', wbf['header_table'])
            worksheet.write('B%s'%(row), 'ITEM ID', wbf['header_table'])
            worksheet.write('C%s'%(row), 'ITEM NAME', wbf['header_table'])
            worksheet.write('D%s'%(row), 'NO MO', wbf['header_table'])
            worksheet.write('E%s'%(row), 'NO KARTU', wbf['header_table'])
            worksheet.write('F%s'%(row), 'NO PALET', wbf['header_table'])
            worksheet.write('G%s'%(row), 'QTY PCS', wbf['header_table'])
            worksheet.write('H%s'%(row), 'QTY KG', wbf['header_table'])
            worksheet.write('I%s'%(row), 'MESIN', wbf['header_table'])
            worksheet.write('J%s'%(row), 'LINE', wbf['header_table'])
            worksheet.write('K%s'%(row), 'OPERATOR', wbf['header_table'])
            worksheet.write('L%s'%(row), 'GRUP', wbf['header_table'])
            worksheet.write('M%s'%(row), 'CUSTOMER', wbf['header_table'])
            worksheet.write('N%s'%(row), 'TANGGAL', wbf['header_table'])
            worksheet.write('O%s'%(row), 'EFECTIVE DATE', wbf['header_table'])
            worksheet.write('P%s'%(row), 'SHIFT', wbf['header_table'])
            worksheet.write('Q%s'%(row), 'WORKCENTER', wbf['header_table'])
            worksheet.write('R%s'%(row), 'STATUS', wbf['header_table'])
            worksheet.write('S%s'%(row), 'COMPANY', wbf['header_table'])
            worksheet.write('T%s'%(row), 'NOTES', wbf['header_table'])

            # jam 6 sore hari sebelumnya
            date_start = str(report.date_start + timedelta(hours=-7))+ ' 17:00:00'
            # jam 6 sore sesuai hr yg dipilih
            date_end = str(report.date_end)+' 16:59:59'

            #date_end = report.date_end + timedelta(days=1)# tambah sehari karena shift 3 masuk ke hari berikutnya
            domain = [('workcenter_id.group_report','=',report.group_report),('production_id.reprocess','!=',True),('workorder_date','>=',str(report.date_start)),('workorder_date','<=',str(report.date_end))]
            
            # if report.state == 'done':
            #     domain.append(('state','=',report.state))
            #     domain.append(('workcenter_id.production_report','=',True))
            #     domain.append(('date_start','>=',date_start))
            #     domain.append(('date_finished','<=',date_end))
            # else:
            #     domain.append(('state','in',('ready','progress')))
            #     domain.append(('date_planned_start','>=',date_start))
            #     domain.append(('date_planned_finished','<=',date_end))
            if report.workcenter_ids:
                domain.append(('workcenter_id','in',report.workcenter_ids.ids))

            if report.state == 'done':
                domain.append(('state','=',report.state))
                if not report.workcenter_ids:
                    domain.append(('workcenter_id.production_report','=',True))
            else:
                domain.append(('state','in',('ready','progress')))

            wo_exist = self.env['mrp.workorder'].sudo().search(domain, order="product_id asc, workorder_date asc, shift asc")
            if not wo_exist :
                raise Warning('Report Production not found..')

            no = 1
            row += 1
            for wo in wo_exist :
                kode_mesin = wo.barcodewo
                if len(report.company_ids.ids) == 1 and (wo.barcodewo or wo.machine_id):
                    # kode >= 800 BKS
                    # kode < 800  TGR
                    if not kode_mesin:
                        kode_mesin = wo.machine_id.code
                    if report.company_ids[0].second_name == 'NAI TGR' :
                        if kode_mesin and int(kode_mesin) >= 800:
                            continue
                    elif report.company_ids[0].second_name == 'NAI BKS' :
                        if kode_mesin and int(kode_mesin) < 800:
                            continue
                # shift
                # date_start = wo.date_start.astimezone(timezone('Asia/Jakarta'))
                # date_start = wo.date_start + timedelta(hours=+7)
                # jam_start = date_start.strftime('%H:%M:%S')
                # jam = jam_start[:2]
                # menit = jam_start[3:5]
                # jamenit = float(jam+'.'+menit)
                # if jamenit >= 7.30 and jamenit <= 16.29 :
                #     shift = 'I'
                # elif jamenit >= 16.30 and jamenit <= 23.59 :
                #     shift = 'II'
                # else :
                #     shift = 'III'

                shift = wo.shift
                if not report.shift1:
                    if wo.shift == '1':
                        continue
                if not report.shift2:
                    if wo.shift == '2':
                        continue
                if not report.shift3:
                    if wo.shift == '3':
                        continue


                try :
                    qty_scan_wo = int(wo.production_id.no_poly_box.strip()[-1:])*wo.production_id.product_id.kg_pal
                except :
                    qty_scan_wo = 0
                # lot_temp_exist = lot_temp.search([('workorder_id','in',wo.production_id.workorder_ids.ids)])
                # if lot_temp_exist:
                #     qty_scan_wo = sum(lot_temp_exist.mapped('qty'))
                # in_raw_qty = 0.0
                # out_raw_qty = 0.0
                # in_raw = wo.production_id.move_raw_ids.filtered(lambda move:move.state != 'cancel' and move.location_dest_id.usage == 'production')
                # if in_raw:
                #     in_raw_qty = sum(in_raw.mapped('quantity_done'))
                # out_raw = wo.production_id.move_raw_ids.filtered(lambda move:move.state != 'cancel' and move.location_id.usage == 'production')
                # if out_raw:
                #     out_raw_qty = sum(out_raw.mapped('quantity_done'))
                company_name = 'NAI TGR'
                if int(wo.machine_id.code) >= 800 :
                    company_name = 'NAI BKS'
                if not kode_mesin:
                    company_name = ''
                polybox = wo.production_id.no_poly_box.strip()
                worksheet.write('A%s'%(row), no, wbf['content_number'])
                worksheet.write('B%s'%(row), wo.product_id.default_code or '', wbf['content_string'])
                worksheet.write('C%s'%(row), wo.product_id.name, wbf['content_string'])
                worksheet.write('D%s'%(row), wo.production_id.name, wbf['content_string'])
                worksheet.write('E%s'%(row), wo.production_id.no_poly_box or '', wbf['content_string'])
                worksheet.write('F%s'%(row), int(polybox[-1:]) or '', wbf['content_number'])
                worksheet.write('G%s'%(row), wo.qty_produced_real, wbf['content_number'])
                # worksheet.write('H%s'%(row), in_raw_qty-out_raw_qty, wbf['content_number'])
                worksheet.write('H%s'%(row), qty_scan_wo, wbf['content_number'])
                worksheet.write('I%s'%(row), wo.machine_id.name or '', wbf['content_string'])
                worksheet.write('J%s'%(row), wo.machine_id.line_id.name or '', wbf['content_string'])
                worksheet.write('K%s'%(row), wo.user_id.name or '', wbf['content_string'])
                worksheet.write('L%s'%(row), wo.product_id.group_id.name or '', wbf['content_string'])
                worksheet.write('M%s'%(row), wo.production_id.customer_id.name or '', wbf['content_string'])
                # worksheet.write('N%s'%(row), str(wo.date_start + timedelta(hours=+7))[:10] or '', wbf['content_string'])
                worksheet.write('N%s'%(row), str(wo.workorder_date) or '', wbf['content_string'])
                worksheet.write('O%s'%(row), str(wo.date_finished) or '', wbf['content_string'])
                worksheet.write('P%s'%(row), shift or '', wbf['content_string'])
                worksheet.write('Q%s'%(row), wo.workcenter_id.name, wbf['content_string'])
                worksheet.write('R%s'%(row), dict(wo._fields['state'].selection).get(wo.state), wbf['content_string'])
                worksheet.write('S%s'%(row), company_name or '', wbf['content_string'])
                worksheet.write('T%s'%(row), 'Subcon' if wo.is_subcontracting else ''+ ' NoLot: '+wo.no_lot if wo.no_lot else '', wbf['content_string'])

                row+=1
                no+=1

            worksheet.merge_range('A%s:D%s'%(row,row),'' , wbf['header_table'])
            worksheet.write('E%s'%(row), 'TOTAL', wbf['header_table'])
            worksheet.write_formula('F%s'%(row), '=sum(F%s:F%s)' % (6, row-1), wbf['content_footer'])
            worksheet.write_formula('G%s'%(row), '=sum(G%s:G%s)' % (6, row-1), wbf['content_footer'])
            worksheet.write_formula('H%s'%(row), '=sum(H%s:H%s)' % (6, row-1), wbf['content_footer'])
            worksheet.merge_range('I%s:T%s'%(row,row),'' , wbf['header_table'])

            if report.notes :
                worksheet.merge_range('A%s:S%s'%(row+2,row+2), report.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            report.write({'file_data':result})
            filename = report_name+' ('+company+')'
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(report.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

ReportMRPWizard()