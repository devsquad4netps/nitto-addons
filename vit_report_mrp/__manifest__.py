#-*- coding: utf-8 -*-

{
	"name": "Report MRP",
	"version": "2.0", 
	"depends": [
		'base','account','sale',"vit_report_balance","vit_nitto_mrp","vit_subcontractor","vit_mrp_reprocess","vit_workorder"
	],
	'author': 'widianajuniar@gmail.com',
	'website': 'http://www.vitraining.com',
	"summary": "",
	"description": """

""",
	"data": [
		"wizard/report_mrp.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}