from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning

class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"

    @api.multi
    def get_datas(self):
        res = super(VitExportImportBcWizard, self).get_datas()
        if self.bc_id.name == 'BC 2.3' :
            kurs_pajak = self.env['vit.kurs.pajak']
            no_aju_ids = self.env['vit.nomor.aju']
            kanban = self.env['vit.kanban']
            purchase_ids = self.env['purchase.order'].search([
                ('company_id','=',self.company_id.id),
                ('state','in',['purchase','done']),
                ('purchase_type','=','Import'),
            ])
            for purchase_id in purchase_ids :
                # for picking_id in purchase_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier'):
                # for picking_id in purchase_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier' and picking.tgl_dokumen == self.date):
                for picking_id in purchase_id.picking_ids.filtered(lambda picking:picking.location_id.usage == 'supplier' and picking.tgl_dokumen == self.date and picking.nomor_aju_id):
                    picking_id.write({'has_export':True})
                    no_aju_ids |= picking_id.nomor_aju_id

            no_aju_ids = no_aju_ids.filtered(lambda aju: not aju.has_export)

            headers = [
                'NOMOR AJU',
                'KPPBC',
                'PERUSAHAAN',
                'PEMASOK',
                'STATUS',
                'KODE DOKUMEN PABEAN',
                'NPPJK',
                'ALAMAT PEMASOK',
                'ALAMAT PEMILIK',
                'ALAMAT PENERIMA BARANG',
                'ALAMAT PENGIRIM',
                'ALAMAT PENGUSAHA',
                'ALAMAT PPJK',
                'API PEMILIK',
                'API PENERIMA',
                'API PENGUSAHA',
                'ASAL DATA',
                'ASURANSI',
                'BIAYA TAMBAHAN',
                'BRUTO',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG PEMILIK',
                'URL DOKUMEN PABEAN',
                'FOB',
                'FREIGHT',
                'HARGA BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA TOTAL',
                'ID MODUL',
                'ID PEMASOK',
                'ID PEMILIK',
                'ID PENERIMA BARANG',
                'ID PENGIRIM',
                'ID PENGUSAHA',
                'ID PPJK',
                'JABATAN TTD',
                'JUMLAH BARANG',
                'JUMLAH KEMASAN',
                'JUMLAH KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE ASAL BARANG',
                'KODE ASURANSI',
                'KODE BENDERA',
                'KODE CARA ANGKUT',
                'KODE CARA BAYAR',
                'KODE DAERAH ASAL',
                'KODE FASILITAS',
                'KODE FTZ',
                'KODE HARGA',
                'KODE ID PEMASOK',
                'KODE ID PEMILIK',
                'KODE ID PENERIMA BARANG',
                'KODE ID PENGIRIM',
                'KODE ID PENGUSAHA',
                'KODE ID PPJK',
                'KODE JENIS API',
                'KODE JENIS API PEMILIK',
                'KODE JENIS API PENERIMA',
                'KODE JENIS API PENGUSAHA',
                'KODE JENIS BARANG',
                'KODE JENIS BC25',
                'KODE JENIS NILAI',
                'KODE JENIS PEMASUKAN01',
                'KODE JENIS PEMASUKAN 02',
                'KODE JENIS TPB',
                'KODE KANTOR BONGKAR',
                'KODE KANTOR TUJUAN',
                'KODE LOKASI BAYAR',
                '',
                'KODE NEGARA PEMASOK',
                'KODE NEGARA PENGIRIM',
                'KODE NEGARA PEMILIK',
                'KODE NEGARA TUJUAN',
                'KODE PEL BONGKAR',
                'KODE PEL MUAT',
                'KODE PEL TRANSIT',
                'KODE PEMBAYAR',
                'KODE STATUS PENGUSAHA',
                'STATUS PERBAIKAN',
                'KODE TPS',
                'KODE TUJUAN PEMASUKAN',
                'KODE TUJUAN PENGIRIMAN',
                'KODE TUJUAN TPB',
                'KODE TUTUP PU',
                'KODE VALUTA',
                'KOTA TTD',
                'NAMA PEMILIK',
                'NAMA PENERIMA BARANG',
                'NAMA PENGANGKUT',
                'NAMA PENGIRIM',
                'NAMA PPJK',
                'NAMA TTD',
                'NDPBM',
                'NETTO',
                'NILAI INCOTERM',
                'NIPER PENERIMA',
                'NOMOR API',
                'NOMOR BC11',
                'NOMOR BILLING',
                'NOMOR DAFTAR',
                'NOMOR IJIN BPK PEMASOK',
                'NOMOR IJIN BPK PENGUSAHA',
                'NOMOR IJIN TPB',
                'NOMOR IJIN TPB PENERIMA',
                'NOMOR VOYV FLIGHT',
                'NPWP BILLING',
                'POS BC11',
                'SERI',
                'SUBPOS BC11',
                'SUB SUBPOS BC11',
                'TANGGAL BC11',
                'TANGGAL BERANGKAT',
                'TANGGAL BILLING',
                'TANGGAL DAFTAR',
                'TANGGAL IJIN BPK PEMASOK',
                'TANGGAL IJIN BPK PENGUSAHA',
                'TANGGAL IJIN TPB',
                'TANGGAL NPPPJK',
                'TANGGAL TIBA',
                'TANGGAL TTD',
                'TANGGAL JATUH TEMPO',
                'TOTAL BAYAR',
                'TOTAL BEBAS',
                'TOTAL DILUNASI',
                'TOTAL JAMIN',
                'TOTAL SUDAH DILUNASI',
                'TOTAL TANGGUH',
                'TOTAL TANGGUNG',
                'TOTAL TIDAK DIPUNGUT',
                'URL DOKUMEN PABEAN',
                'VERSI MODUL',
                'VOLUME',
                'WAKTU BONGKAR',
                'WAKTU STUFFING',
                'NOMOR POLISI',
                'CALL SIGN',
                'JUMLAH TANDA PENGAMAN',
                'KODE JENIS TANDA PENGAMAN',
                'KODE KANTOR MUAT',
                'KODE PEL TUJUAN',
                '',
                'TANGGAL STUFFING',
                'TANGGAL MUAT',
                'KODE GUDANG ASAL',
                'KODE GUDANG TUJUAN'
            ]
            values = []
            header_exist = []
            for aju_id in no_aju_ids :
                # for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier'):
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.nomor_aju_id and picking.location_id.usage == 'supplier'):
                    move_line_ids = picking_id.move_ids_without_package.mapped('move_line_ids')
                    purchase_line_ids = picking_id.move_ids_without_package.mapped('purchase_line_id')
                    if not purchase_line_ids :
                        continue
                    purchase_ids = purchase_line_ids.mapped('order_id')
                    invoice_ids = purchase_ids.mapped('invoice_ids')
                    kanban_id = kanban.search([('nomor_aju_id','=',aju_id.id)],limit=1)
                    if kanban_id :
                        for inv in invoice_ids :
                            if inv.origin:
                                lbm_inv = inv.origin.split(" : ")
                                if lbm_inv[0] == kanban_id.name:
                                    invoice_ids = inv
                                    break
                    if not invoice_ids :
                        continue
                        #raise Warning('Invoice belum dibuat (nomor kanban : %s) ' % kanban_id.name)
                    jml_type_kemasan = 0
                    packs = []
                    for kemasan in picking_id.kemasan_ids:
                        if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                            packs.append(kemasan.package_type_id.id)
                            jml_type_kemasan+=1
                    if not picking_id.pelabuhan_bongkar_id :
                        raise Warning('Kode pelabuhan bongkar %s belum di set' % picking_id.name)
                    kurs_pajak_exist = kurs_pajak.sudo().search([('currency_id','=',invoice_ids[0].currency_id.id)],limit=1)
                    kurs_rate = 1
                    if kurs_pajak_exist :
                        kurs = kurs_pajak_exist.rate_ids.sorted('date')[0]
                        kurs_rate = kurs.rate
                    biaya_tambahan = 0
                    tambahan_inv = invoice_ids[0].invoice_line_ids.filtered(lambda v:v.product_id.type == 'service')
                    if tambahan_inv :
                        biaya_tambahan = sum(tambahan_inv.mapped('price_subtotal'))
                    diskon = sum(invoice_ids[0].invoice_line_ids.mapped('discount_amount'))
                    if aju_id.name in header_exist :
                        continue
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'KPPBC': self.bc_id.kppbc or '',
                        'PERUSAHAAN': self.company_id.bc_name if self.company_id.bc_name else self.company_id.name,
                        'PEMASOK': kanban_id.partner_shipment_id.name if kanban_id.partner_shipment_id else kanban_id.partner_id.name or '',
                        'STATUS': '00',
                        'KODE DOKUMEN PABEAN': self.bc_id.kode_dokumen_pabean or '',
                        'NPPJK': '',
                        'ALAMAT PEMASOK': kanban_id.partner_shipment_id.street if kanban_id.partner_shipment_id.street else kanban_id.partner_id.street or '',
                        'ALAMAT PEMILIK': picking_id.company_id.street or '',
                        'ALAMAT PENERIMA BARANG': '',
                        'ALAMAT PENGIRIM': '',
                        'ALAMAT PENGUSAHA': picking_id.company_id.street or '',
                        'ALAMAT PPJK': '',
                        'API PEMILIK': self.bc_id.api_pemilik or '',
                        'API PENERIMA': '',
                        'API PENGUSAHA': self.bc_id.api_pengusaha or '',
                        'ASAL DATA': self.bc_id.asal_data or '',
                        'ASURANSI': '', #todo
                        'BIAYA TAMBAHAN': biaya_tambahan,
                        'BRUTO': sum(line.brutto for line in picking_id.move_ids_without_package) or '',
                        'CIF': sum((line.amount_untaxed) for line in invoice_ids) or '',
                        #'CIF RUPIAH': sum(line.currency_id.compute(line.price_subtotal, line.company_id.currency_id) for line in invoice_ids) or '',
                        'CIF RUPIAH': sum((line.amount_untaxed) for line in invoice_ids) * kurs_rate,
                        'DISKON': diskon,
                        'FLAG PEMILIK': '',
                        'URL DOKUMEN PABEAN': '',
                        'FOB': sum((line.amount_untaxed) for line in invoice_ids) or '',
                        'FREIGHT': picking_id.freight,
                        'HARGA BARANG LDP': '',
                        #'HARGA INVOICE': sum(inv.amount_total for inv in invoice_ids) or '',
                        'HARGA INVOICE': sum((line.amount_untaxed) for line in invoice_ids) or '',
                        'HARGA PENYERAHAN': '',
                        'HARGA TOTAL': '',
                        'ID MODUL': self.bc_id.id_modul or '',
                        'ID PEMASOK': '',
                        'ID PEMILIK': self.company_id.vat  or '',
                        'ID PENERIMA BARANG': '',
                        'ID PENGIRIM': '',
                        'ID PENGUSAHA': self.company_id.vat  or '',
                        'ID PPJK': '',
                        'JABATAN TTD': self.bc_id.jabatan_ttd  or '',
                        # 'JUMLAH BARANG': sum(move.quantity_done for move in picking_id.move_ids_without_package) or '',
                        'JUMLAH BARANG': len(kanban_id.detail_ids) or '', # jumlah line
                        'JUMLAH KEMASAN': jml_type_kemasan,
                        'JUMLAH KONTAINER': len(picking_id.kontainer_ids),
                        'KESESUAIAN DOKUMEN': '',
                        'KETERANGAN': '',
                        'KODE ASAL BARANG': '',
                        'KODE ASURANSI': '1' if picking_id.kode_harga_id.code and picking_id.kode_harga_id.code == 'FOB' else '',
                        'KODE BENDERA': picking_id.partner_id.country_id.code  or '',
                        'KODE CARA ANGKUT': picking_id.kode_cara_angkut if picking_id.kode_cara_angkut else '',
                        'KODE CARA BAYAR': '',
                        'KODE DAERAH ASAL': '',
                        'KODE FASILITAS': '',
                        'KODE FTZ': '',
                        'KODE HARGA': picking_id.kode_harga_id.code if picking_id.kode_harga_id.code else '',
                        'KODE ID PEMASOK': '',
                        'KODE ID PEMILIK': self.bc_id.kode_id_pengusaha  or '',
                        'KODE ID PENERIMA BARANG': '',
                        'KODE ID PENGIRIM': '',
                        'KODE ID PENGUSAHA': self.bc_id.kode_id_pengusaha  or '',
                        'KODE ID PPJK': '',
                        'KODE JENIS API': '',
                        'KODE JENIS API PEMILIK': self.bc_id.kode_jenis_api_pemilik or '',
                        'KODE JENIS API PENERIMA': '',
                        'KODE JENIS API PENGUSAHA': self.bc_id.kode_jenis_api_pemilik or '',
                        'KODE JENIS BARANG': '',
                        'KODE JENIS BC25': '',
                        'KODE JENIS NILAI': '',
                        'KODE JENIS PEMASUKAN01': '',
                        'KODE JENIS PEMASUKAN 02': '',
                        'KODE JENIS TPB': '',
                        'KODE KANTOR BONGKAR': picking_id.pelabuhan_bongkar_id.kantor_bongkar if picking_id.pelabuhan_bongkar_id.kantor_bongkar else '',
                        'KODE KANTOR TUJUAN': '',
                        'KODE LOKASI BAYAR': '',
                        '': '',
                        'KODE NEGARA PEMASOK': kanban_id.partner_shipment_id.country_id.code if kanban_id.partner_shipment_id.country_id else kanban_id.partner_id.country_id.code or '',
                        'KODE NEGARA PENGIRIM': '',
                        'KODE NEGARA PEMILIK': '',
                        'KODE NEGARA TUJUAN': '',
                        'KODE PEL BONGKAR': picking_id.pelabuhan_bongkar_id.code if picking_id.pelabuhan_bongkar_id.code else '',
                        'KODE PEL MUAT': picking_id.pelabuhan_muat_id.code if picking_id.pelabuhan_muat_id.code else '',
                        'KODE PEL TRANSIT': '',
                        'KODE PEMBAYAR': '',
                        'KODE STATUS PENGUSAHA': '',
                        'STATUS PERBAIKAN': '',
                        'KODE TPS': picking_id.tempat_penimbun_id.code if picking_id.tempat_penimbun_id.code else '',
                        'KODE TUJUAN PEMASUKAN': '',
                        'KODE TUJUAN PENGIRIMAN': '',
                        'KODE TUJUAN TPB': self.bc_id.kode_tujuan_tpb or '',
                        'KODE TUTUP PU': '',
                        'KODE VALUTA': purchase_ids[:1].currency_id.name or '',
                        'KOTA TTD': self.bc_id.kota_ttd or '',
                        'NAMA PEMILIK': self.company_id.bc_name if self.company_id.bc_name else self.company_id.name,
                        'NAMA PENERIMA BARANG': '',
                        'NAMA PENGANGKUT': picking_id.nama_pengangkut if picking_id.nama_pengangkut else '',
                        'NAMA PENGIRIM': '',
                        'NAMA PPJK': '',
                        'NAMA TTD': self.bc_id.nama_ttd or '',
                        'NDPBM': picking_id.ndpbm_id.get_rate(str(picking_id.scheduled_date)) or '',
                        'NETTO': sum(line.netto for line in picking_id.move_ids_without_package) or '',
                        'NILAI INCOTERM': '',
                        'NIPER PENERIMA': '',
                        'NOMOR API': '',
                        'NOMOR BC11': '',
                        'NOMOR BILLING': '',
                        'NOMOR DAFTAR': '',
                        'NOMOR IJIN BPK PEMASOK': '',
                        'NOMOR IJIN BPK PENGUSAHA': '',
                        'NOMOR IJIN TPB': self.bc_id.nomor_ijin_tpb or '',
                        'NOMOR IJIN TPB PENERIMA': kanban_id.partner_shipment_id.no_ijin_tpb if kanban_id.partner_shipment_id.no_ijin_tpb else kanban_id.partner_id.no_ijin_tpb or '',
                        'NOMOR VOYV FLIGHT': '',
                        'NPWP BILLING': '',
                        'POS BC11': '',
                        'SERI': self.bc_id.seri or '0',
                        'SUBPOS BC11': '',
                        'SUB SUBPOS BC11': '',
                        'TANGGAL BC11': '',
                        'TANGGAL BERANGKAT': '',
                        'TANGGAL BILLING': '',
                        'TANGGAL DAFTAR': '',
                        'TANGGAL IJIN BPK PEMASOK': '',
                        'TANGGAL IJIN BPK PENGUSAHA': '',
                        'TANGGAL IJIN TPB': '',
                        'TANGGAL NPPPJK': '',
                        'TANGGAL TIBA': '',
                        'TANGGAL TTD': self.reformat_date(picking_id.tgl_ttd.strftime('%Y-%m-%d')) if picking_id.tgl_ttd else '',
                        'TANGGAL JATUH TEMPO': '',
                        'TOTAL BAYAR': '',
                        'TOTAL BEBAS': '',
                        'TOTAL DILUNASI': '',
                        'TOTAL JAMIN': '',
                        'TOTAL SUDAH DILUNASI': '',
                        'TOTAL TANGGUH': '',
                        'TOTAL TANGGUNG': '', #todo
                        'TOTAL TIDAK DIPUNGUT': '', #todo
                        'URL DOKUMEN PABEAN2': '',
                        'VERSI MODUL': self.bc_id.versi_modul or '',
                        'VOLUME': '',
                        'WAKTU BONGKAR': '',
                        'WAKTU STUFFING': '',
                        'NOMOR POLISI': '',
                        'CALL SIGN': '',
                        'JUMLAH TANDA PENGAMAN': '',
                        'KODE JENIS TANDA PENGAMAN': '',
                        'KODE KANTOR MUAT': '',
                        'KODE PEL TUJUAN': '',
                        '': '',
                        'TANGGAL STUFFING': '',
                        'TANGGAL MUAT': '',
                        'KODE GUDANG ASAL': '',
                        'KODE GUDANG TUJUAN': '',
                    })
                    header_exist.append(aju_id.name)
            res.append(['Header'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'ASURANSI',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG KENDARAAN',
                'FOB',
                'FREIGHT',
                'BARANG BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA SATUAN',
                'JENIS KENDARAAN',
                'JUMLAH BAHAN BAKU',
                'JUMLAH KEMASAN',
                'JUMLAH SATUAN',
                'KAPASITAS SILINDER',
                'KATEGORI BARANG',
                'KODE ASAL BARANG',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE GUNA',
                'KODE JENIS NILAI',
                'KODE KEMASAN',
                'KODE LEBIH DARI 4 TAHUN',
                'KODE NEGARA ASAL',
                'KODE SATUAN',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'KONDISI BARANG',
                'MERK',
                'NETTO',
                'NILAI INCOTERM',
                'NILAI PABEAN',
                'NOMOR MESIN',
                'POS TARIF',
                'SERI POS TARIF',
                'SPESIFIKASI LAIN',
                'TAHUN PEMBUATAN',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'VOLUME',
                'SERI IJIN',
                'ID EKSPORTIR',
                'NAMA EKSPORTIR',
                'ALAMAT EKSPORTIR',
                'KODE PERHITUNGAN'
            ]
            values = []
            seri_barang = 1
            for aju_id in no_aju_ids :
                # search kanban:
                kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                for kanban_id in kanban_ids :
                #for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier'):
                #for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.nomor_aju_id and picking.location_id.usage == 'supplier'):    
                    for move in kanban_id.detail_ids.sorted('kanban_sequence') :    
                        #seri_barang = 1
                        #for move in picking_id.move_ids_without_package.sorted('sequence') :
                        if not move.purchase_line_id :
                            continue
                        exist_kanban = kanban_id.detail_ids.mapped('id')
                        if exist_kanban and move.id not in exist_kanban :
                            continue
                        #invoice_ids = move.purchase_line_id.order_id.invoice_ids
                        invoice_ids = kanban_id.invoice_id
                        if not invoice_ids :
                            continue
                            #raise Warning('Invoice belum dibuat (nomor kanban : %s) ' % kanban_id.name)
                        invoice_line_ids = invoice_ids.mapped('invoice_line_ids').filtered(lambda inv_line: inv_line.product_id.id == move.product_id.id and move.purchase_line_id.price_unit == inv_line.price_unit and move.quantity_done == inv_line.quantity and inv_line.purchase_line_id.order_id.id == move.purchase_line_id.order_id.id)
                        kurs_pajak_exist = kurs_pajak.sudo().search([('currency_id','=',invoice_ids[0].currency_id.id)],limit=1)
                        kurs_rate = 0.0
                        if kurs_pajak_exist :
                            kurs = kurs_pajak_exist.rate_ids.sorted('date')[0]
                            kurs_rate = kurs.rate
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        kategori_barang = ''
                        if move.product_id.categ_id.pos_tarif :
                            kategori_barang = move.product_id.categ_id.pos_tarif
                        kode_kemasan = ''
                        if kanban_id.kemasan_ids:
                            kode_kemasan = kanban_id.kemasan_ids[0].package_type_id.code if kanban_id.kemasan_ids[0].package_type_id else ''
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI BARANG': move.seri_barang or '1',
                            'ASURANSI': '0.00', #todo
                            'CIF': sum(line.price_subtotal for line in invoice_line_ids) or 0,
                            #'CIF RUPIAH': sum(line.currency_id.compute(line.price_subtotal, line.company_id.currency_id) for line in invoice_line_ids) or '0.00',
                            'CIF RUPIAH': sum(line.price_subtotal for line in invoice_line_ids)*kurs_rate or 0,
                            'DISKON': '0.00',
                            'FLAG KENDARAAN': '',
                            # 'FOB': sum(line.price_subtotal for line in invoice_line_ids) or '0.00',
                            'FOB': sum(line.price_subtotal for line in invoice_line_ids) or 0,
                            'FREIGHT': '0.00', #todo
                            'BARANG BARANG LDP': '',
                            #'HARGA INVOICE': sum(inv_line.price_subtotal for inv_line in invoice_line_ids) or '',
                            'HARGA INVOICE' :  sum(line.price_subtotal for line in invoice_line_ids) or 0,
                            'HARGA PENYERAHAN': '',
                            'HARGA SATUAN': move.purchase_line_id.price_unit or 0,
                            #'HARGA SATUAN': sum(line.price_unit for line in invoice_line_ids) or '0.00',
                            'JENIS KENDARAAN': '',
                            'JUMLAH BAHAN BAKU': '',
                            'JUMLAH KEMASAN': len(move.move_line_ids.mapped('result_package_id')) or 0, #todo
                            'JUMLAH SATUAN': move.quantity_done or '',
                            'KAPASITAS SILINDER': '',
                            'KATEGORI BARANG': kategori_barang,
                            'KODE ASAL BARANG': '',
                            'KODE BARANG': move.product_id.default_code or '',
                            'KODE FASILITAS': '',
                            'KODE GUNA': '',
                            'KODE JENIS NILAI': '',
                            'KODE KEMASAN': kode_kemasan,
                            'KODE LEBIH DARI 4 TAHUN': '',
                            'KODE NEGARA ASAL': kanban_id.partner_shipment_id.country_id.code if kanban_id.partner_shipment_id.country_id else kanban_id.partner_id.country_id.code or '',
                            'KODE SATUAN': move.product_uom.name or '',
                            'KODE SKEMA TARIF': '',
                            'KODE STATUS': self.bc_id.kode_status or '',
                            'KONDISI BARANG': '',
                            'MERK': kanban_id.invoice_id.reference if kanban_id.invoice_id.reference else '',
                            # 'MERK': kanban_id.no_sj_pengirim if kanban_id.no_sj_pengirim else '',
                            # 'NETTO': sum(line.netto for line in picking_id.move_ids_without_package) or '0.00',
                            'NETTO': move.netto or '0.00', # netto per barang
                            'NILAI INCOTERM': '',
                            'NILAI PABEAN': '',
                            'NOMOR MESIN': '',
                            'POS TARIF': move.product_id.group_id.kode_tarif if move.product_id.group_id else '',
                            'SERI POS TARIF': '',
                            'SPESIFIKASI LAIN': move.product_id.group_id.notes if move.product_id.group_id.notes else move.product_id.group_id.name or '',
                            'TAHUN PEMBUATAN': '',
                            'TIPE': move.product_id.type_id.name if move.product_id.type_id else '',
                            'UKURAN': '',
                            # 'URAIAN': move.product_id.name or '',
                            'URAIAN': move.product_vendor if move.product_vendor else move.product_id.name or '',
                            'VOLUME': '',
                            'SERI IJIN': '',
                            'ID EKSPORTIR': '',
                            'NAMA EKSPORTIR': '',
                            'ALAMAT EKSPORTIR': '',
                            'KODE PERHITUNGAN': '',
                        })
            res.append(['Barang'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'TARIF KODE SATUAN',
                'TARIF KODE TARIF',
                'TARIF NILAI BAYAR',
                'TARIF NILAI FASILITAS',
                'TARIF NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            seri_barang = 1
            for aju_id in no_aju_ids :
                # search kanban:
                kanban_id = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                #for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier'):
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.nomor_aju_id and picking.location_id.usage == 'supplier'):    
                    # seri_barang = 1
                    for move in picking_id.move_ids_without_package :
                        if not move.purchase_line_id :
                            continue
                        exist_kanban = kanban_id.detail_ids.mapped('id')
                        if exist_kanban and move.id not in exist_kanban :
                            continue
                        invoice_ids = move.purchase_line_id.order_id.invoice_ids
                        if not invoice_ids :
                            raise Warning('Invoice belum dibuat (%s) ' % move.purchase_line_id.order_id.name)
                        invoice_line_ids = invoice_ids.mapped('invoice_line_ids').filtered(lambda inv_line: inv_line.product_id.id == move.product_id.id)
                        # for tax_id in move.product_id.supplier_taxes_id :
                        #     values.append({
                        #         'NOMOR AJU': aju_id.name or '',
                        #         'SERI BARANG': seri_barang,
                        #         'JENIS TARIF': tax_id.name or '',
                        #         'JUMLAH SATUAN': '',
                        #         'KODE FASILITAS': '',
                        #         'KODE KOMODITI CUKAI': '',
                        #         'TARIF KODE SATUAN': '',
                        #         'TARIF KODE TARIF': tax_id.kode_tarif or '',
                        #         'TARIF NILAI BAYAR': '0.00',
                        #         'TARIF NILAI FASILITAS': tax_id._compute_amount(move.quantity_done*move.purchase_line_id.price_unit,move.purchase_line_id.price_unit,move.quantity_done) or '',
                        #         'TARIF NILAI SUDAH DILUNASI': '',
                        #         'TARIF': tax_id.amount or '',
                        #         'TARIF FASILITAS': '100.00',
                        #     })

                        if not move.product_id.group_id:
                            raise Warning('Product group belum di set (%s) ' % move.product_id.name)
                        for tarif in ['BM','PPN','PPH']:
                            if tarif == 'BM' :
                                nilai_tarif = move.product_id.group_id.tarif_bm
                                kode_fasilitas = 2
                            elif tarif == 'PPN':
                                nilai_tarif = move.product_id.group_id.ppn
                                kode_fasilitas = 5
                            elif tarif == 'PPH' :
                                nilai_tarif = move.product_id.group_id.pph
                                kode_fasilitas = 5
                            values.append({
                                    'NOMOR AJU': aju_id.name,
                                    'SERI BARANG': seri_barang,
                                    'JENIS TARIF': tarif,
                                    'JUMLAH SATUAN': '',
                                    'KODE FASILITAS': kode_fasilitas,
                                    'KODE KOMODITI CUKAI': '',
                                    'TARIF KODE SATUAN': '',
                                    #'TARIF KODE TARIF': move.product_id.group_id.kode_bm if move.product_id.group_id.kode_bm else '',
                                    'TARIF KODE TARIF': move.product_id.group_id.kode_bm if move.product_id.group_id and tarif == 'BM' else '',
                                    'TARIF NILAI BAYAR': '0.00',
                                    'TARIF NILAI FASILITAS': '0.00',#float(tarif)*sum(line.price_subtotal for line in invoice_line_ids),
                                    'TARIF NILAI SUDAH DILUNASI': '',
                                    'TARIF': nilai_tarif,#,tarif,
                                    'TARIF FASILITAS': '100.00',
                                })
                        seri_barang += 1
            res.append(['BarangTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI DOKUMEN'
            ]
            values = []
            res.append(['BarangDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI DOKUMEN',
                'FLAG URL DOKUMEN',
                'KODE JENIS DOKUMEN',
                'NOMOR DOKUMEN',
                'TANGGAL DOKUMEN',
                'TIPE DOKUMEN',
                'URL DOKUMEN'
            ]
            values = []
            seri_dokumen = 1
            kode_dok = []
            for aju_id in no_aju_ids :
                move_ids = aju_id.picking_ids.filtered(lambda picking: picking.nomor_aju_id and picking.location_id.usage == 'supplier').mapped('move_ids_without_package')
                # move_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier').mapped('move_ids_without_package')
                purchase_line_ids = move_ids.mapped('purchase_line_id')
                if not purchase_line_ids :
                    continue
                # purchase_ids = purchase_line_ids.mapped('order_id')
                # invoice_ids = purchase_ids.mapped('invoice_ids')
                # if not invoice_ids :
                #     raise Warning('Invoice belum dibuat (nomor aju : %s) ' % aju_id.name)
                # # seri_dokumen = 1
                # no_inv = False
                # tgl_inv = False
                # for invoice_id in invoice_ids :
                #     invoice_id.seri_dokumen = seri_dokumen
                #     if invoice_id.origin :
                #         lbm = kanban.search([('name','=',invoice_id.origin)],limit=1)
                #         if lbm :
                #             tgl = lbm.dokumen_ids.filtered(lambda x:x.dokumen_master_id.code == '380' and x.tanggal)
                #             if tgl :
                #                 tgl_inv = tgl[0].tanggal
                #             if lbm.nomor_aju_id.id != aju_id.id:
                #                 continue
                #     if self.bc_id.kode_jenis_dokumen_inv and aju_id.name+self.bc_id.kode_jenis_dokumen_inv not in kode_dok :

                #         if not tgl_inv :
                #             tgl_inv = self.reformat_date(invoice_id.date_invoice.strftime('%Y-%m-%d')) if invoice_id.date_invoice else ''
                #         values.append({
                #             'NOMOR AJU': aju_id.name or '',
                #             # 'SERI DOKUMEN': invoice_id.seri_dokumen or '',
                #             'SERI DOKUMEN': seri_dokumen,
                #             'FLAG URL DOKUMEN': '',
                #             'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_inv or '',
                #             # 'NOMOR DOKUMEN': invoice_id.reference or '',
                #             'NOMOR DOKUMEN': invoice_id.reference if invoice_id.reference else '',
                #             'TANGGAL DOKUMEN': tgl_inv,
                #             'TIPE DOKUMEN': self.bc_id.tipe_dokumen or '',
                #             'URL DOKUMEN': '',
                #         })
                #         seri_dokumen += 1
                #         no_inv = True
                #         kode_dok.append(aju_id.name+self.bc_id.kode_jenis_dokumen_inv)
                
                # for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state in ('done','assigned') and picking.location_id.usage == 'supplier'):
                #     picking_id.seri_dokumen = seri_dokumen
                #     if self.bc_id.kode_jenis_dokumen_sj and aju_id.name+self.bc_id.kode_jenis_dokumen_sj not in kode_dok :
                #         values.append({
                #             'NOMOR AJU': aju_id.name or '',
                #             # 'SERI DOKUMEN': picking_id.seri_dokumen or '',
                #             'SERI DOKUMEN': seri_dokumen,
                #             'FLAG URL DOKUMEN': '',
                #             'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_sj or '',
                #             # 'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                #             'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                #             'TANGGAL DOKUMEN': self.reformat_date(picking_id.tgl_dokumen.strftime('%Y-%m-%d')) if picking_id.tgl_dokumen else '',
                #             'TIPE DOKUMEN': self.bc_id.tipe_dokumen or '',
                #             'URL DOKUMEN': '',
                #         })
                #         kode_dok.append(aju_id.name+self.bc_id.kode_jenis_dokumen_sj)
                #         seri_dokumen += 1

                    # for dok in picking_id.dokumen_ids :
                    #     if dok.dokumen_master_id.code != '640':
                    #         if dok.dokumen_master_id.code == '380':
                    #             if not no_inv :
                    #                 if aju_id.name+dok.dokumen_master_id.code not in kode_dok :
                    #                     values.append({
                    #                         'NOMOR AJU': aju_id.name or '',
                    #                         'SERI DOKUMEN': seri_dokumen,
                    #                         'FLAG URL DOKUMEN': '',
                    #                         'KODE JENIS DOKUMEN': dok.dokumen_master_id.code or '',
                    #                         # 'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                    #                         'NOMOR DOKUMEN': dok.nomor or '',
                    #                         'TANGGAL DOKUMEN': self.reformat_date(dok.tanggal.strftime('%Y-%m-%d')) if dok.tanggal else '',
                    #                         'TIPE DOKUMEN': self.bc_id.tipe_dokumen or '',
                    #                         'URL DOKUMEN': '',
                    #                     })
                    #                     seri_dokumen += 1
                    #                     kode_dok.append(aju_id.name+dok.dokumen_master_id.code)
                    #         else :
                    #             if aju_id.name+dok.dokumen_master_id.code in kode_dok :
                    #                 continue
                    #             values.append({
                    #                     'NOMOR AJU': aju_id.name or '',
                    #                     'SERI DOKUMEN': seri_dokumen,
                    #                     'FLAG URL DOKUMEN': '',
                    #                     'KODE JENIS DOKUMEN': dok.dokumen_master_id.code or '',
                    #                     # 'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                    #                     'NOMOR DOKUMEN': dok.nomor or '',
                    #                     'TANGGAL DOKUMEN': self.reformat_date(dok.tanggal.strftime('%Y-%m-%d')) if dok.tanggal else '',
                    #                     'TIPE DOKUMEN': self.bc_id.tipe_dokumen or '',
                    #                     'URL DOKUMEN': '',
                    #                 })
                    #             seri_dokumen += 1
                    #             kode_dok.append(aju_id.name+dok.dokumen_master_id.code)
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state in ('done','assigned') and picking.location_id.usage == 'supplier'):
                    for dok in picking_id.dokumen_ids :
                        if aju_id.name+dok.dokumen_master_id.code in kode_dok :
                            continue
                        values.append({
                                'NOMOR AJU': aju_id.name or '',
                                'SERI DOKUMEN': seri_dokumen,
                                'FLAG URL DOKUMEN': '',
                                'KODE JENIS DOKUMEN': dok.dokumen_master_id.code if dok.dokumen_master_id.code else '',
                                # 'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                                'NOMOR DOKUMEN': dok.nomor if dok.nomor else '',
                                'TANGGAL DOKUMEN': self.reformat_date(dok.tanggal.strftime('%Y-%m-%d')) if dok.tanggal else '',
                                'TIPE DOKUMEN': self.bc_id.tipe_dokumen if self.bc_id.tipe_dokumen else '',
                                'URL DOKUMEN': '',
                            })
                        seri_dokumen += 1
                        kode_dok.append(aju_id.name+dok.dokumen_master_id.code)
            res.append(['Dokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KEMASAN',
                'JUMLAH KEMASAN',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE JENIS KEMASAN',
                'MEREK KEMASAN',
                'NIP GATE IN',
                'NIP GATE OUT',
                'NOMOR POLISI',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT',
            ]
            values = []
            seri_kemasan = 1
            aju_kemasan = []
            for aju_id in no_aju_ids :
                # kemasan_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier').mapped('kemasan_ids')
                kemasan_ids = aju_id.picking_ids.filtered(lambda picking: picking.nomor_aju_id and picking.location_id.usage == 'supplier').mapped('kemasan_ids')
                for kemasan_id in kemasan_ids.filtered(lambda kmsn:kmsn.qty > 0) :
                    if aju_id.name in aju_kemasan :
                        continue
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'SERI KEMASAN': seri_kemasan,
                        'JUMLAH KEMASAN': kemasan_id.qty or '0.00',
                        'KESESUAIAN DOKUMEN': '',
                        'KETERANGAN': '',
                        'KODE JENIS KEMASAN': kemasan_id.package_type_id.code if kemasan_id.package_type_id else '',
                        'MEREK KEMASAN': kemasan_id.picking_id.no_sj_pengirim or '',
                        'NIP GATE IN': '',
                        'NIP GATE OUT': '',
                        'NOMOR POLISI': '',
                        'NOMOR SEGEL': '',
                        'WAKTU GATE IN': '',
                        'WAKTU GATE OUT': '',
                    })
                    seri_kemasan += 1
                    aju_kemasan.append(aju_id.name)
            res.append(['Kemasan'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE STUFFING',
                'KODE TIPE KONTAINER',
                'KODE UKURAN KONTAINER',
                'FLAG GATE IN',
                'FLAG GATE OUT',
                'NOMOR POLISI',
                'NOMOR KONTAINER',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT',
            ]
            values = []
            for aju_id in no_aju_ids :
                # for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_id.usage == 'supplier'):
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.nomor_aju_id and picking.location_id.usage == 'supplier'):    
                    for kontainer in picking_id.kontainer_ids :
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI KONTAINER': kontainer.seri_kontainer if kontainer.seri_kontainer else '',
                            'KESESUAIAN DOKUMEN': kontainer.kesesuaian_dokumen if kontainer.kesesuaian_dokumen else '',
                            'KETERANGAN': kontainer.keterangan if kontainer.keterangan else '',
                            'KODE STUFFING': kontainer.kode_stuffing if kontainer.kode_stuffing else '',
                            'KODE TIPE KONTAINER': kontainer.kode_tipe_kontainer if kontainer.kode_tipe_kontainer else '',
                            'KODE UKURAN KONTAINER': kontainer.kode_ukuran_kontainer if kontainer.kode_ukuran_kontainer else '',
                            'FLAG GATE IN': kontainer.flag_gate_in if kontainer.flag_gate_in else '',
                            'FLAG GATE OUT': kontainer.flag_gate_out if kontainer.flag_gate_out else '',
                            'NOMOR POLISI': kontainer.nomor_polisi if kontainer.nomor_polisi else '',
                            'NOMOR KONTAINER': kontainer.name if kontainer.name else '',
                            'NOMOR SEGEL': kontainer.nomor_segel if kontainer.nomor_segel else '',
                            'WAKTU GATE IN': self.reformat_date(kontainer.waktu_gate_in.strftime('%Y-%m-%d')) if kontainer.waktu_gate_in else '',
                            'WAKTU GATE OUT': self.reformat_date(kontainer.waktu_gate_out.strftime('%Y-%m-%d'))  if kontainer.waktu_gate_out else '',
                        })
            res.append(['Kontainer'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON',
                'TANGGAL RESPON',
                'WAKTU RESPON',
                'BYTE STRAM PDF'
            ]
            values = []
            res.append(['Respon'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON'
            ]
            values = []
            res.append(['Status'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'JENIS TARIF',
                'KODE FASILITAS',
                'NILAI PUNGUTAN'
            ]
            values = []
            res.append(['Pungutan'] + [headers, values])

            no_aju_ids.write({'has_export':True})
            no_aju_ids.mapped('picking_ids').write({'has_export':True})

        return res
