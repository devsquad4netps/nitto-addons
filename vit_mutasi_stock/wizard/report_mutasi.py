from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportMutasiStockWizard(models.TransientModel):
    _name = "vit.report.mutasi_stock.wizard"
    _description = "Report Mutasi Stock"

    @api.onchange('type')
    def onchange_type(self):
        domain = []
        if self.type:
            location = self.env['stock.location']
            partner = self.env['res.partner']
            if self.type == 'Masuk':
                loc_exist = location.sudo().search([('name','ilike','vendor')],order='id asc')
                if loc_exist :
                    self.location_id = loc_exist[0].id
                    domain = {'domain': {'location_id': [('id','in',loc_exist.ids)],'partner_ids': [('id','in',partner.search([('supplier','=',True)]).ids)]}}
            else:
                loc_exist = location.sudo().search([('name','ilike','customer')],order='id asc')
                if loc_exist :
                    self.location_id = loc_exist[0].id
                    domain = {'domain': {'location_id': [('id','in',loc_exist.ids)],'partner_ids': [('id','in',partner.search([('customer','=',True)]).ids)]}}

        return domain

    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes")
    file_data = fields.Binary('File', readonly=True)
    location_id = fields.Many2one("stock.location","Location", required=True)
    name = fields.Char('Filename')
    partner_ids = fields.Many2many("res.partner",string='Partner')
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    type = fields.Selection([('Masuk','Source Location'),('Keluar','Destination Location')],string='Type', required=True, default=False)

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'left','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000'})
        #wbf['header_table'].set_border()

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        wbf['header_month_sum'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_float'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.0000'})

        wbf['content_number_sum'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['grey']})
        
        return wbf, workbook

    @api.multi
    def action_print_report(self):
        self.ensure_one()
        for report in self :
            i_type = dict(self._fields['type'].selection).get(report.type)

            report_name = 'Laporan Mutasi Barang'
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
            company = company[:-3]
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")
            if self.partner_ids :
                # partners = str(tuple(report.partner_ids.ids)).replace(",)",")")
                partners = self.partner_ids
            else :
                partner_obj = self.env['res.partner']
                if report.location_id.name.lower() in ('vendor','vendors') :
                    partners = partner_obj.search([('supplier','=',True)])
                elif report.location_id.name.lower() in ('customer','customers') :
                    partners = partner_obj.search([('customer','=',True)])
                else :
                    raise Warning('Lokasi (%s) belum bisa export data...' % (report.location_id.complete_name))

            return report.action_print_report_mutasi_stock(i_type, report_name, company, companys, partners)


    def action_print_report_mutasi_stock(self, i_type, report_name, company, companys, partners):
        for i in self :
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name)
            worksheet.set_column('A1:A1', 4)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 20)
            worksheet.set_column('D1:D1', 35)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 35)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 8)
            worksheet.set_column('K1:K1', 12)#Rate
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 15)
            worksheet.set_column('N1:N1', 35)
            if not self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                worksheet.set_column('I1:I1', 35)
            source_location = 'Source Location'
            kata_sambung = 'dari'
            if i.type == 'Keluar':
                kata_sambung = 'ke'
                source_location = 'Destination Location'

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:J2', 'Laporan Mutasi Barang %s %s %s'%(i.type, kata_sambung, i.location_id.name), wbf['company'])
            worksheet.merge_range('A3:J3', 'Periode %s - %s'%(i.date_start, i.date_end), wbf['company'])
            if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                worksheet.merge_range('K4:M4', 'Printed on : '+ str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            else :
                worksheet.merge_range('G4:I4', 'Printed on : '+ str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            row = 5

            grand_t_qty = 0.0
            grand_t_valas = 0.0
            grand_t_subtotal = 0.0
            stock_move = self.env['stock.move']
            for partner in partners :
                sql = "select sm.id from stock_move sm " \
                  "left join stock_picking sp on sp.id = sm.picking_id " \
                  "where sm.state = 'done' and sp.partner_id = %s and (sm.date between '%s' and '%s') and sm.company_id in (%s)" \
                  "order by sm.date asc, sp.partner_id" % (partner.id, str(self.date_start), str(self.date_end),companys)
            
                cr.execute(sql)
                datas = cr.fetchall()
                if not datas :
                    continue
                worksheet.merge_range('A%s:C%s'%(row,row), partner.name, wbf['header_table'])
                row+=1
                worksheet.write('A%s'%(row), 'No', wbf['header_month'])
                worksheet.write('B%s'%(row), 'Date', wbf['header_month'])
                worksheet.write('C%s'%(row), 'Invoice', wbf['header_month'])
                worksheet.write('D%s'%(row), 'Partner name', wbf['header_month'])
                worksheet.write('E%s'%(row), 'Item Code', wbf['header_month'])
                worksheet.write('F%s'%(row), 'Item Name', wbf['header_month'])
                worksheet.write('G%s'%(row), 'Quantity', wbf['header_month'])
                if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                    worksheet.write('H%s'%(row), 'Unit Price', wbf['header_month'])
                    worksheet.write('I%s'%(row), 'Invoice Amount', wbf['header_month'])
                    worksheet.write('J%s'%(row), 'Currency', wbf['header_month'])
                    worksheet.write('K%s'%(row), 'Rate', wbf['header_month'])#rate
                    worksheet.write('L%s'%(row), 'Amount IDR', wbf['header_month'])
                    worksheet.write('M%s'%(row), 'Source Document', wbf['header_month'])
                    worksheet.write('N%s'%(row), source_location, wbf['header_month'])
                else :
                    worksheet.write('O%s'%(row), 'Source Document', wbf['header_month'])
                    worksheet.write('P%s'%(row), source_location, wbf['header_month'])

                row+=1
                no = 1
                t_qty = 0.0
                t_subtotal = 0.0
                t_valas = 0.0
                for dt in datas :
                    data = stock_move.browse(dt)
                    picking_id = data.picking_id
                    date = data.date
                    company_currency = data.company_id.currency_id
                    invoice = ''
                    currency = ''
                    price_unit = ''
                    subtotal = ''
                    valas_price = ''
                    currency_rate = 1.0
                    location = data.location_id.complete_name
                    if i_type == 'Source Location':
                        location = data.location_dest_id.complete_name
                    if data.kanban_id:
                        date = data.kanban_id.date
                        invoice = data.kanban_id.invoice_id.number or ''
                    if not invoice:
                        if data.picking_id.invoice_id:
                            invoice = data.picking_id.invoice_id.number if data.picking_id.invoice_id.number else data.picking_id.invoice_id.name+' (Ref)'
                        else:
                            continue
                    if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                        if data.purchase_line_id :
                            price_unit = data.purchase_line_id.price_unit
                            subtotal = price_unit*data.product_uom_qty
                            currency = data.purchase_line_id.order_id.currency_id
                            valas_price = subtotal
                            if currency.name != 'IDR':
                                valas_price = currency._convert(subtotal, company_currency, data.purchase_line_id.order_id.company_id, data.date)
                                currency_date = self.env['res.currency.rate'].search([('currency_id','=',currency.id),('name','<=',str(date))], order='name desc', limit=1)
                                if currency_date:
                                    currency_rate = currency_date[0].rate_calc
                        elif data.sale_line_id :
                            price_unit = data.sale_line_id.price_unit
                            subtotal = price_unit*data.product_uom_qty
                            currency = data.sale_line_id.order_id.currency_id
                            valas_price = subtotal
                            if currency.name != 'IDR':
                                valas_price = currency._convert(subtotal, company_currency, data.sale_line_id.order_id.company_id, data.date)
                                currency_date = self.env['res.currency.rate'].search([('currency_id','=',currency.id),('name','<=',str(date))], order='name desc', limit=1)
                                if currency_date:
                                    currency_rate = currency_date[0].rate_calc
                    worksheet.write('A%s'%(row), no, wbf['title_doc'])
                    worksheet.write('B%s'%(row), str(date)[:10], wbf['title_doc'])
                    worksheet.write('C%s'%(row), invoice, wbf['title_doc'])
                    worksheet.write('D%s'%(row), picking_id.partner_id.name or '', wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.product_id.default_code, wbf['title_doc'])
                    worksheet.write('F%s'%(row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('G%s'%(row), data.product_uom_qty, wbf['content_number'])
                    if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                        worksheet.write('H%s'%(row), price_unit, wbf['content_number'])
                        worksheet.write('I%s'%(row), subtotal, wbf['content_number'])
                        worksheet.write('J%s'%(row), currency.name if currency else '', wbf['title_doc'])
                        if currency_rate != 1.0 :
                            worksheet.write('K%s'%(row), currency_rate, wbf['content_float'])#rate
                        worksheet.write('L%s'%(row), valas_price, wbf['content_number'])
                        worksheet.write('M%s'%(row), picking_id.origin or '', wbf['title_doc'])
                        worksheet.write('N%s'%(row), location, wbf['title_doc'])
                    else :
                        worksheet.write('O%s'%(row), picking_id.origin or '', wbf['title_doc'])
                        worksheet.write('P%s'%(row), location, wbf['title_doc'])
                            
                    t_qty += data.product_qty
                    if valas_price :
                        t_valas += valas_price
                    if subtotal :
                        t_subtotal += subtotal
                    no += 1
                    row += 1
                    
                    grand_t_qty += t_qty
                    grand_t_subtotal += t_subtotal
                    grand_t_valas += t_valas
                # sum total perpartner
                worksheet.write('G%s'%(row), t_qty, wbf['content_number_sum'])
                if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                    worksheet.write('I%s'%(row), t_subtotal, wbf['content_number_sum'])
                    worksheet.write('L%s'%(row), t_valas, wbf['content_number_sum'])
                row += 1

            worksheet.merge_range('A%s:F%s'%(row+2,row+2), 'Grand Total', wbf['header_table'])
            worksheet.write('G%s'%(row+2), grand_t_qty, wbf['content_number_sum'])
            if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') :
                worksheet.write('I%s'%(row+2), grand_t_subtotal, wbf['content_number_sum'])
                worksheet.write('L%s'%(row+2), grand_t_valas, wbf['content_number_sum'])

            if i.notes :
                worksheet.merge_range('A%s:M%s'%(row+4,row+4), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }                


ReportMutasiStockWizard()