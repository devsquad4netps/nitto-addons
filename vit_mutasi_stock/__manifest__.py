#-*- coding: utf-8 -*-

{
	"name": "Report Mutasi Stock",
	"version": "0.2", 
	"depends": [
		'base',
		'stock',
		'sale',
		'purchase',
		'vit_stock_card_pro'
	],
	'author': 'widianajuniar@gmail.com',
	'website': 'http://www.vitraining.com',
	"summary": "",
	"description": """
	* Laporan Mutasi Barang
""",
	"data": [
		"security/group.xml",
		"wizard/report_mutasi.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}