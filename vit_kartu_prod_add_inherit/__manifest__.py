# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2022  Odoo SA  (http://www.vitraining.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Report Manufacture",
    "version": "1.4",
    "category": "Extra Tools",
    "sequence": 90,
    "author":  "vITraining",
    "website": "www.vitraining.com",
    "license": "AGPL-3",
    "summary": "",
    "description": """
    * Report Kartu Produksi 1/1 sampai 1/9
    * Print dibuat dalam 1 report menu

    """,
    "depends": [
        "vit_kartu_prod_add",
    ],
    "data": [
        "security/group.xml",
        "views/mrp_views.xml",
        "views/template_print_kartu_prod_4.xml",
        "views/template_print_kartu_prod_5.xml",
        "views/template_print_kartu_prod_6.xml",
        "views/template_print_kartu_prod_7.xml",
        "views/template_print_kartu_prod_8.xml",
        "views/template_print_kartu_prod_9.xml",
    ],

    "demo": [
    ],

    "test": [
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}