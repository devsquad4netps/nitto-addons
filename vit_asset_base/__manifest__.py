{
    "name"          : "Penambahan field Asset",
    "version"       : "0.1",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Inventory",
    "license"       : "LGPL-3",
    "contributors"  : """
    """,
    "summary"       : "Penambahan field",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "om_account_asset",
    ],
    "data"          : [
        "views/account_asset_views.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}