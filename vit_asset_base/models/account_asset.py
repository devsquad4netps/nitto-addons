from odoo import models, fields, api, _

class AccountAssetAsset(models.Model):
    _inherit = "account.asset.asset"

    code_1 = fields.Char(string='Code 1',size=32, readonly=True, states={'draft': [('readonly', False)]})
    location = fields.Char(string='Location',size=200, readonly=True, states={'draft': [('readonly', False)]})


AccountAssetAsset()