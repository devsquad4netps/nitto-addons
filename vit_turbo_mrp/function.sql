CREATE OR REPLACE FUNCTION public.fn_get_shift_date(pcompany_id integer, pworkcenter_id integer, pdatescan timestamp without time zone)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
DECLARE
  returnLookup  varchar(100);
  returnDefault  varchar(100);	
	returnFinal  varchar(100);	
BEGIN

 


Select 

   cast(Production_date as varchar) ||'#'||cast(shift_no as varchar)	 		into returnLookup 
	  
from
(
Select 
      (cast(pdatescan as timestamp)+ '07:00:00'::interval) as Tanggal_Scan ,
 

      (CASE
            WHEN (((cast(pdatescan as timestamp) + '31:00:00'::interval) >= (((cast(pdatescan as timestamp) + '07:00:00'::interval))::date + (((nss.time_from)::text || ':00'::text))::interval)) AND 
						      ((cast(pdatescan as timestamp) + '31:00:00'::interval) < (((cast(pdatescan as timestamp) + '07:00:00'::interval))::date + (((nss.time_to)::text || ':00'::text))::interval))) THEN
									(((cast(pdatescan as timestamp) + '07:00:00'::interval) - '24:00:00'::interval))::date
            ELSE  ((cast(pdatescan as timestamp) + '07:00:00'::interval))::date
        END) as Production_date
				,nss.shift_no
		--		,(cast(cast(pdatescan as timestamp) + '07:00:00'::interval as date))  + (nss.time_from || ':00')::interval  s_from
		--		,(cast(cast(pdatescan as timestamp) + '07:00:00'::interval as date)) +   (nss.time_to || ':00')::interval  s_to
		--		, nss.shift_id
		--		, nss.shift_name
		--		, nss.valid_from
		--		, nss.valid_to
		--		, nss.time_from
		--		, nss.time_to

	--	into dtreturn 
		
from 
     vw_nitto_shift_schedule nss 

where 
	   nss.company_id = pcompany_id  AND 
	   nss.workcenter_id = pworkcenter_id  AND 
	   
	   cast(pdatescan as timestamp)+ '07:00:00'::interval >= nss.valid_from AND 	  
	   cast(pdatescan as timestamp) + '07:00:00'::interval < nss.valid_to AND 
	   (
			 (
					(cast(pdatescan as timestamp) + '07:00:00'::interval >= (cast(cast(pdatescan as timestamp) + '07:00:00'::interval as date)) + (nss.time_from || ':00')::interval) AND 
					(cast(pdatescan as timestamp) + '07:00:00'::interval < (cast(cast(pdatescan as timestamp) + '07:00:00'::interval as date)) +   (nss.time_to || ':00')::interval)
				) 
			
			OR 		
			(
			 (cast(pdatescan as timestamp) + '31:00:00'::interval >= (cast(cast(pdatescan as timestamp) + '07:00:00'::interval as date)) + (nss.time_from || ':00')::interval) AND 
			 (cast(pdatescan as timestamp) + '31:00:00'::interval < (cast(cast(pdatescan as timestamp)+ '07:00:00'::interval as date)) + (nss.time_to || ':00')::interval)
			)
		)
		
)		a
  limit 1;
		
		
    if to_char(cast(pdatescan as timestamp)+ '07:00:00'::interval ,'HH24:MI')<'07:00' then 
		    returnDefault=cast(cast(   (cast(pdatescan as timestamp)+ '07:00:00'::interval) - '24:00:00'::interval    as date) as varchar)||'#0';		
		else
		    returnDefault=cast(cast((cast(pdatescan as timestamp)+ '07:00:00'::interval) as date) as varchar)||'#0';		
		end if;
	
  
		
		
    returnFinal=COALESCE(returnLookup,returnDefault);


RETURN returnFinal;
END; $function$
