{
    "name"          : "Turbo Function MRP",
    "version"       : "2.4",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "MRP",
    "summary"       : "Replace ORM to store procedure MRP",
    "description"   : """
        
    """,
    "depends"       : [
        "mrp","vit_nitto_mrp","vit_workorder"
    ],
    "data"          : [
        "wizard/production_date_confirmation.xml",
        "views/mrp_production_views.xml",
        "views/mrp_workorder_views.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}