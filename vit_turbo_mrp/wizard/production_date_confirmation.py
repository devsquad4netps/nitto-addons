# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare


class MRPWorkorderConfirmation(models.TransientModel):
    _name = 'mrp.workorder.confirmation'
    _description = 'Production Date Confirmation'

    workorder_id = fields.Many2one('mrp.workorder', 'Workorder')
    confirm_production_message = fields.Text('Confirm Production Message', related='workorder_id.confirm_production_message')

    @api.one
    def process_turbo_done(self):
        self.workorder_id.confirm_production_date = True
        self.workorder_id.button_turbo_finish()