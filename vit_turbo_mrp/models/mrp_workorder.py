from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import UserError, AccessError, ValidationError, Warning


class MRPWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    @api.model_cr
    def init(self):
        _logger.info("creating function done WO...")
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_done_wo(wo_id integer, mo_id integer, uid integer)
RETURNS void AS
$BODY$
DECLARE
  production record;
  workorder record;
  next_work_order record;
  history_wo_ids record;
  wo_state varchar;
  outsanding_sales_sql integer := 0;
  customer_sql varchar;
  prev_wo_id record;
  wip boolean;
  outstanding_qty integer := 0;
  prev_qty_produced integer :=0 ;

BEGIN
	-- INITIALIZE
	SELECT * FROM mrp_production WHERE id=mo_id into production;
	SELECT * FROM mrp_workorder WHERE id=wo_id into workorder;
	SELECT id,qty_producing,qty_produced FROM mrp_workorder WHERE next_work_order_id=wo_id into prev_wo_id;
	-- END of INITIALIZE

	-- UPDATE HISTORY WO
	INSERT INTO mrp_workorder_history ("create_uid", "create_date", "write_uid", "write_date", "name", "workorder_id") 
	VALUES (uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), workorder.qty_producing, workorder.id);

	-- UPDATE WO
    IF prev_wo_id is not null THEN
        outstanding_qty := prev_wo_id.qty_producing; 
        prev_qty_produced := prev_wo_id.qty_produced;
    ELSE
        outstanding_qty := production.product_qty;
        prev_qty_produced := production.product_qty;
    END IF;

	IF (workorder.qty_produced+workorder.qty_producing) >= prev_qty_produced OR workorder.qty_producing <= 0 THEN
		wo_state := 'done';
        wip := true;
        -- UPDATE NEXT WO
        IF workorder.next_work_order_id is not null and wo_state = 'done' THEN
            SELECT * FROM mrp_workorder WHERE id=workorder.next_work_order_id into next_work_order;
            IF next_work_order.state <> 'done' THEN
                UPDATE mrp_workorder SET 
                    state = 'ready',
                    date_planned_start = (now() at time zone 'UTC'),
                    qty_producing = workorder.qty_produced+workorder.qty_producing,
                    retur = workorder.retur,
                    warna = workorder.warna
                WHERE 
                    id = next_work_order.id;     
            END IF;
        END IF;

        -- UPDATE PREVIOUS WO
        IF prev_wo_id is not null and wo_state = 'done' THEN
            UPDATE mrp_workorder SET 
                is_wip = false
            WHERE 
                id = prev_wo_id.id;  
        END IF;
	ELSE
		wo_state := 'progress';
        wip := false;
	END IF;

	SELECT (sum(product_uom_qty)-sum(qty_delivered))
                        FROM sale_order_line 
                        WHERE product_id = workorder.product_id AND state IN ('sale','done') into outsanding_sales_sql;
    SELECT rp.name as customer_name from sale_order_line sol 
                            LEFT JOIN sale_order so ON so.id=sol.order_id
                            LEFT JOIN res_partner rp ON rp.id=so.partner_id
                            WHERE sol.state IN ('sale','done') AND sol.product_id = workorder.product_id 
                            LIMIT 1 into customer_sql;

	UPDATE mrp_workorder SET 
		state = wo_state, 
        date_start = (now() at time zone 'UTC'),
		date_finished = (now() at time zone 'UTC'),
		qty_produced_real = workorder.qty_produced+workorder.qty_producing,
		qty_produced = workorder.qty_produced+workorder.qty_producing,
        qty_producing = 0.0,
		-- qty_producing = prev_qty_produced-workorder.qty_produced-workorder.qty_producing,
		is_wip = wip,
		outsanding_sales = outsanding_sales_sql,
        customer = customer_sql
	WHERE
		id = workorder.id;

    IF (SELECT is_wip_report FROM mrp_workcenter WHERE id=workorder.workcenter_id ) is true THEN
        -- tambah kondisi jika jumping proses ga di update is_wip_report nya
        IF (SELECT is_wip_report FROM mrp_workorder WHERE production_id=workorder.production_id AND id > workorder.id AND is_wip_report is true) is not true THEN
            UPDATE mrp_workorder SET 
                is_wip_report = false
            WHERE
                id <> workorder.id
                AND production_id = production.id;

            UPDATE mrp_workorder SET 
                is_wip_report = true
            WHERE
                id = workorder.id;
        END IF;
    END IF;    

    -- UPDATE WORKCENTER PRODUCTIVITIES
    UPDATE mrp_workcenter_productivity SET date_end = (now() at time zone 'UTC') WHERE workorder_id = workorder.id AND date_end is null ;

END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
        """)


    confirm_production_date = fields.Boolean('Confirm Production Date', copy=False, default=False)
    confirm_production_message = fields.Text('Confirm Production Message', copy=False)


    @api.multi
    def button_turbo_finish(self):
        self.ensure_one()
        # if not self.user_has_groups('vit_nitto_mrp.group_bypass_scan_wo'):
        if not self.user_id or not self.machine_id :
            raise ValidationError("Operator dan id mesin harus diisi !")
        if not self.workorder_date or not self.shift :
            raise ValidationError("Production date dan shift harus diisi !")
        # protek jumping proses
        if self.workcenter_id.no_jumping_process :
            outstanding_wo = self.search([('production_id','=',self.production_id.id),('id','<',self.id),('state','not in',['done','cancel'])],limit=1)
            if outstanding_wo :
                raise Warning('Tidak bisa jumping process atas workorder ini !')
        if self.workcenter_id.routing_validation > 0 :
            polybox = self.production_id.no_poly_box.strip()
            jml_polybox = int(polybox[-1:])
            # if len(self.polybox_ids) != jml_polybox :
            #     raise Warning('Jumlah polybox yang di scan (%s) tidak sama dengan total polybox (%s)' % (len(self.polybox_ids), jml_polybox))
        if self.workorder_date and not self.confirm_production_date:
            import datetime
            now = datetime.datetime.now()
            self.env.cr.execute("SELECT fn_get_shift_date (%s,%s, cast('%s' as timestamp))" % (self.production_id.company_id.id, self.workcenter_id.id, str(now)[:19]) )
            cek_shift = self.env.cr.fetchone()
            if cek_shift :
                cek_shift = cek_shift[0]
                cek_shift_date = cek_shift.split('#')[0]
                if cek_shift_date != str(self.workorder_date) :
                    from datetime import datetime
                    cek_shift_date = datetime.strptime(cek_shift_date, '%Y-%m-%d')
                    self.confirm_production_message = 'Your production date input is '+str(self.workorder_date.strftime("%d-%B-%Y"))+'\nProduction date formula is '+str(cek_shift_date.strftime("%d-%B-%Y"))
                    return self.action_confirm_production_date()

        previous_wo = self.search([('next_work_order_id','=',self.id)],limit=1)
        if previous_wo and previous_wo.state not in ('done','cancel'):
            if not self.env.user.has_group('vit_workorder.group_force_finish_previous_wo') :
                routings = self.production_id.workorder_ids.sorted('id')
                rout = ''
                disini = ''
                no = 1
                for r in routings:
                    state = dict(self._fields['state'].selection).get(r.state)
                    if r.id == previous_wo.id :
                        disini = ' <----'
                    rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
                    disini = ''
                    no += 1
                raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
            #raise UserError(_("Work Order sebelumnya belum selesai (%s)")%(previous_wo.name))
        self.env.cr.execute("SELECT vit_done_wo(%s,%s,%s)" % (self.id, self.production_id.id, self.env.uid) )
        # cek jika ada is_wip yg true di depan

        next_wip = self.env['mrp.workorder'].sudo().search([('production_id','=',self.production_id.id),
                                                                ('id','>',self.id),
                                                                ('is_wip','=',True)],limit=1)
        if not next_wip :
            # update is_wip sebelumnya
            self.env.cr.execute("UPDATE mrp_workorder set is_wip=false where id != %s and production_id = %s and is_wip = true " % (self.id, self.production_id.id,) )
            # update is_wip=true di wo existing ini
            self.env.cr.execute("UPDATE mrp_workorder set is_wip=true where id = %s " % (self.id,) )
        else :
             self.env.cr.execute("UPDATE mrp_workorder set is_wip=false where id = %s " % (self.id,) )
        if self.workcenter_id.code == 'H':
            picking_cons = self.env['stock.picking'].sudo().search([('group_id','=',self.production_id.procurement_group_id.id),
                                                            ('location_dest_id.usage','=','production'),
                                                            ('state','not in',['cancel','assigned','done'])],limit=1)
            if picking_cons:
                picking_cons.action_assign()
                picking_cons.get_lot_raw_material()
                if picking_cons.state == 'assigned':
                    picking_cons.action_done()
        # update done qty barang jadi di moveline
        if not self.next_work_order_id:
            move = self.env['stock.move'].sudo().search([('group_id','=',self.production_id.procurement_group_id.id),
                                                            ('location_id.usage','=','production'),
                                                            ('production_id','=',self.production_id.id)],limit=1)
            if move :
                if move.state != 'assigned' :
                    move.picking_id.action_assign()
                for line in move.move_line_ids :
                    line.qty_done = self.qty_produced_real
                    line.lot_id = self.final_lot_id.id
                    if not line.picking_id :
                        line.picking_id = move.picking_id.id

        return True


    def action_confirm_production_date(self):
        view = self.env.ref('vit_turbo_mrp.view_production_date_confirmation')
        wiz = self.env['mrp.workorder.confirmation'].create({'workorder_id': self.id})
        return {
            'name': _('Production date input warning'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mrp.workorder.confirmation',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz.id,
            'context': self.env.context,
        }

MRPWorkorder()


class MRPWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    no_jumping_process = fields.Boolean('No Jumping Process', default=False, help="Tidak bisa turbo done WO jika proses sebelum nya belum selesai")

MRPWorkcenter()
