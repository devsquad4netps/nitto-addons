from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import UserError, AccessError, ValidationError


class MRProduction(models.Model):
    _inherit = 'mrp.production'

    @api.model_cr
    def init(self):
        _logger.info("creating function create WO...")
        self.env.cr.execute("""
          CREATE OR REPLACE FUNCTION vit_create_wo(mo_id integer, uid integer)
RETURNS void AS
$BODY$
DECLARE
  production record;
  routing_line record;
  workorder_line record;
  picking_consume record;
  wo_id integer :=0;
  state varchar;
  count integer := 0;

BEGIN
	-- INITIALIZE
	SELECT * FROM mrp_production WHERE id=mo_id into production;
	SELECT picking_id FROM stock_move WHERE raw_material_production_id=production.id into picking_consume;
	-- END of INITIALIZE

	-- LOOP ROUTING LINES
	FOR routing_line IN SELECT id,name,workcenter_id FROM mrp_routing_workcenter WHERE routing_id = production.routing_id ORDER BY sequence,id ASC
	  	LOOP
	  		IF count > 0 THEN
	  			state := 'pending';
	  		ELSE
	  			state := 'ready';
	  		END IF;
		    -- INSERT OR CREATE MRP_WORKORDER
		    INSERT INTO mrp_workorder("create_uid", "create_date", "write_uid", "write_date", "capacity", "duration_expected", "name", "operation_id", "production_id", "qty_produced", "qty_producing", "state", "workcenter_id","product_id","picking_id")
		      	VALUES (uid, (now() at time zone 'UTC'),uid, (now() at time zone 'UTC'), 1.0, '90.00', routing_line.name, routing_line.id, production.id, '0.000', production.product_qty,state, routing_line.workcenter_id, production.product_id, picking_consume.picking_id);
		    count = count+1;
		    -- END CREATE
	    END LOOP;
	-- LOOP WO, UPDATE next_work_order_id
	FOR workorder_line IN SELECT id FROM mrp_workorder WHERE production_id = production.id ORDER BY id DESC
		LOOP
			IF wo_id != 0 THEN
				UPDATE mrp_workorder set next_work_order_id = wo_id where id = workorder_line.id;
			END IF;
			wo_id := workorder_line.id;

		END LOOP;
	-- UPDATE STATE MO
	UPDATE mrp_production set state = 'planned' where id = production.id;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
        """)

    @api.multi
    def button_plan_turbo(self):
        """ Create work orders. And probably do stuff, like things. """
        orders_to_plan = self.filtered(lambda order: order.routing_id and order.state == 'confirmed' and not order.workorder_ids)
        for order in orders_to_plan:
            self.env.cr.execute("SELECT vit_create_wo(%s,%s)" % (order.id,self.env.uid) )
            self.env.cr.commit()
        if not orders_to_plan :
            raise ValidationError(_("Tidak ada data yang bisa di eksekusi"))

MRProduction()