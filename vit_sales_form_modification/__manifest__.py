{
    'name': "Sales Order Modification",
    'version': '0.1',
    'author': '',
    'category': 'Sales',
    'depends': ['sale_management'],
    'data': [
        "views/sale_views.xml",
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}
