# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Production Subcontractor SPK Report',
    'website': 'http://www.vitraining.com',
    'author': 'vITraining',
    'version': '4.0',
    'support': 'widianajuniar@gmail.com',
    'license': 'AGPL-3',
    'images': [],
    'category': 'Manufacturing',
    'sequence': 75,
    'summary': 'Subcontractor SPK Report',
    'depends': ['vit_subcontractor', 'report_qweb_element_page_visibility'],
    'description': "SPK Report subcon",
    'data': [
        'report/spk_report.xml'
    ],
    'test': [],
    'application': True,
}