from odoo import models, fields, api


class SPKReport(models.AbstractModel):
    _name = 'report.vit_subcontractor_spk.spk_report'
    _description = 'SPK Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        current_id = docids[0]
        self._cr.execute("select * from vw_mrp_spk_report vw where vw.spk_id = %s" % current_id)
        spk = self._cr.dictfetchall()
        header = spk[0]
        total_kg = sum(i['total_kg'] for i in spk)
        total_box = sum(i['total_box'] for i in spk)
        company = self.env['res.company'].browse([1])

        self._cr.execute("select * from vw_mrp_spk_report_detail vw where vw.sp_id = %s" % current_id)
        spk_detail = self._cr.dictfetchall()

        item_ids = set(d['item_id'] for d in spk_detail)
        subtotals = []
        for item_id in item_ids:
            sum_kg = sum(o['kg'] for o in spk_detail if o['item_id'] == item_id)
            sum_palet = sum(o['palet'] for o in spk_detail if o['item_id'] == item_id)
            subtotals.append({'item_id': item_id, 'sum_kg': sum_kg, 'sum_palet': sum_palet})

        grandtotal_kg = sum(i['sum_kg'] for i in subtotals)
        grandtotal_palet = sum(i['sum_palet'] for i in subtotals)

        return {
            'doc': data,
            'header': header,
            'company': company,
            'spk': spk,
            'total_kg': total_kg,
            'total_box': total_box,
            'spk_detail': spk_detail,
            'subtotals': subtotals,
            'grand_total_kg': grandtotal_kg,
            'grand_total_palet': grandtotal_palet,
        }
