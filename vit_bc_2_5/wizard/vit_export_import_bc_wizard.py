from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import UserError, ValidationError


class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"

    @api.multi
    def get_datas(self):
        res = super(VitExportImportBcWizard, self).get_datas()
        if self.bc_id.name == 'BC 2.5' :
            no_aju_ids = self.env['vit.nomor.aju']
            kurs_pajak = self.env['vit.kurs.pajak']
            sale_ids = self.env['sale.order'].search([
                ('company_id','=',self.company_id.id),
                ('state','in',['sale','done']),
                ('sale_type','=','Local'),
            ])
            for sale_id in sale_ids :
                # for picking_id in sale_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                for picking_id in sale_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer' and picking.tgl_dokumen == self.date):
                    picking_id.write({'has_export':True})
                    purchase_id = self.env['purchase.order']
                    if not purchase_id :
                        for move in picking_id.move_ids_without_package :
                            if not move.sale_line_id :
                                continue
                            for line in move.move_line_ids :
                                finished_move_line_id = self.env['stock.move.line'].search([
                                    ('product_id','=',line.product_id.id),
                                    ('lot_id','=',line.lot_id.id),
                                    ('move_id.production_id','!=',False),
                                    ('state','=','done'),
                                ], limit=1)
                                if not finished_move_line_id :
                                    continue
                                for raw_line in finished_move_line_id.move_id.production_id.move_raw_ids :
                                    for move_line in raw_line.active_move_line_ids :
                                        incoming_move_line_id = self.env['stock.move.line'].search([
                                            ('product_id','=',move_line.product_id.id),
                                            ('lot_id','=',move_line.lot_id.id),
                                            ('move_id.production_id','=',False),
                                            ('state','=','done'),
                                            ('move_id.location_id.usage','=','supplier'),
                                        ], limit=1)
                                        if not incoming_move_line_id :
                                            continue
                                        else :
                                            purchase_id = incoming_move_line_id.move_id.purchase_line_id.order_id
                                            break
                                    if purchase_id :
                                        break
                                if purchase_id :
                                    break
                            if purchase_id :
                                break
                    if purchase_id and purchase_id.purchase_type == 'Import' :
                        no_aju_ids |= picking_id.nomor_aju_id

            no_aju_ids = no_aju_ids.filtered(lambda aju: not aju.has_export)

            headers = [
                'NOMOR AJU',
                'KPPBC',
                'PERUSAHAAN',
                'PEMASOK',
                'STATUS',
                'KODE DOKUMEN PABEAN',
                'NPPJK',
                'ALAMAT PEMASOK',
                'ALAMAT PEMILIK',
                'ALAMAT PENERIMA BARANG',
                'ALAMAT PENGIRIM',
                'ALAMAT PENGUSAHA',
                'ALAMAT PPJK',
                'API PEMILIK',
                'API PENERIMA',
                'API PENGUSAHA',
                'ASAL DATA',
                'ASURANSI',
                'BIAYA TAMBAHAN',
                'BRUTO',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG PEMILIK',
                'URL DOKUMEN PABEAN',
                'FOB',
                'FREIGHT',
                'HARGA BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA TOTAL',
                'ID MODUL',
                'ID PEMASOK',
                'ID PEMILIK',
                'ID PENERIMA BARANG',
                'ID PENGIRIM',
                'ID PENGUSAHA',
                'ID PPJK',
                'JABATAN TTD',
                'JUMLAH BARANG',
                'JUMLAH KEMASAN',
                'JUMLAH KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE ASAL BARANG',
                'KODE ASURANSI',
                'KODE BENDERA',
                'KODE CARA ANGKUT',
                'KODE CARA BAYAR',
                'KODE DAERAH ASAL',
                'KODE FASILITAS',
                'KODE FTZ',
                'KODE HARGA',
                'KODE ID PEMASOK',
                'KODE ID PEMILIK',
                'KODE ID PENERIMA BARANG',
                'KODE ID PENGIRIM',
                'KODE ID PENGUSAHA',
                'KODE ID PPJK',
                'KODE JENIS API',
                'KODE JENIS API PEMILIK',
                'KODE JENIS API PENERIMA',
                'KODE JENIS API PENGUSAHA',
                'KODE JENIS BARANG',
                'KODE JENIS BC25',
                'KODE JENIS NILAI',
                'KODE JENIS PEMASUKAN01',
                'KODE JENIS PEMASUKAN 02',
                'KODE JENIS TPB',
                'KODE KANTOR BONGKAR',
                'KODE KANTOR TUJUAN',
                'KODE LOKASI BAYAR',
                '',
                'KODE NEGARA PEMASOK',
                'KODE NEGARA PENGIRIM',
                'KODE NEGARA PEMILIK',
                'KODE NEGARA TUJUAN',
                'KODE PEL BONGKAR',
                'KODE PEL MUAT',
                'KODE PEL TRANSIT',
                'KODE PEMBAYAR',
                'KODE STATUS PENGUSAHA',
                'STATUS PERBAIKAN',
                'KODE TPS',
                'KODE TUJUAN PEMASUKAN',
                'KODE TUJUAN PENGIRIMAN',
                'KODE TUJUAN TPB',
                'KODE TUTUP PU',
                'KODE VALUTA',
                'KOTA TTD',
                'NAMA PEMILIK',
                'NAMA PENERIMA BARANG',
                'NAMA PENGANGKUT',
                'NAMA PENGIRIM',
                'NAMA PPJK',
                'NAMA TTD',
                'NDPBM',
                'NETTO',
                'NILAI INCOTERM',
                'NIPER PENERIMA',
                'NOMOR API',
                'NOMOR BC11',
                'NOMOR BILLING',
                'NOMOR DAFTAR',
                'NOMOR IJIN BPK PEMASOK',
                'NOMOR IJIN BPK PENGUSAHA',
                'NOMOR IJIN TPB',
                'NOMOR IJIN TPB PENERIMA',
                'NOMOR VOYV FLIGHT',
                'NPWP BILLING',
                'POS BC11',
                'SERI',
                'SUBPOS BC11',
                'SUB SUBPOS BC11',
                'TANGGAL BC11',
                'TANGGAL BERANGKAT',
                'TANGGAL BILLING',
                'TANGGAL DAFTAR',
                'TANGGAL IJIN BPK PEMASOK',
                'TANGGAL IJIN BPK PENGUSAHA',
                'TANGGAL IJIN TPB',
                'TANGGAL NPPPJK',
                'TANGGAL TIBA',
                'TANGGAL TTD',
                'TANGGAL JATUH TEMPO',
                'TOTAL BAYAR',
                'TOTAL BEBAS',
                'TOTAL DILUNASI',
                'TOTAL JAMIN',
                'TOTAL SUDAH DILUNASI',
                'TOTAL TANGGUH',
                'TOTAL TANGGUNG',
                'TOTAL TIDAK DIPUNGUT',
                'URL DOKUMEN PABEAN',
                'VERSI MODUL',
                'VOLUME',
                'WAKTU BONGKAR',
                'WAKTU STUFFING',
                'NOMOR POLISI',
                'CALL SIGN',
                'JUMLAH TANDA PENGAMAN',
                'KODE JENIS TANDA PENGAMAN',
                'KODE KANTOR MUAT',
                'KODE PEL TUJUAN',
                '',
                'TANGGAL STUFFING',
                'TANGGAL MUAT',
                'KODE GUDANG ASAL',
                'KODE GUDANG TUJUAN'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                    move_line_ids = picking_id.move_ids_without_package.mapped('move_line_ids')
                    sale_line_ids = picking_id.move_ids_without_package.mapped('sale_line_id')
                    if not sale_line_ids :
                        continue
                    sale_ids = sale_line_ids.mapped('order_id')
                    valuta = ''
                    if sale_ids:
                        valuta = sale_ids[0].currency_id.name
                    invoice_ids = sale_ids.mapped('invoice_ids')
                    jml_type_kemasan = 0
                    packs = []
                    for kemasan in picking_id.kemasan_ids:
                        if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                            packs.append(kemasan.package_type_id.id)
                            jml_type_kemasan+=1
                    if not picking_id.no_polisi :
                        picking_name = picking_id.name
                        kanban = picking_id.kanban_id
                        if kanban :
                            picking_name = picking_name +' - '+kanban.name
                        raise ValidationError(_('Nomor polisi %s belum di set !') % picking_name)
                    values.append({
                        'NOMOR AJU': aju_id.name.strip() or '',
                        'KPPBC': self.bc_id.kppbc.strip() or '',
                        'PERUSAHAAN': self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                        'PEMASOK': '',
                        'STATUS': '85',
                        'KODE DOKUMEN PABEAN': self.bc_id.kode_dokumen_pabean.strip() or '',
                        'NPPJK': '',
                        'ALAMAT PEMASOK': '',
                        'ALAMAT PEMILIK': picking_id.company_id.street.strip() or '',
                        'ALAMAT PENERIMA BARANG': picking_id.partner_id.street.strip() or '',
                        'ALAMAT PENGIRIM': '',
                        'ALAMAT PENGUSAHA': picking_id.company_id.street.strip() or '',
                        'ALAMAT PPJK': '',
                        'API PEMILIK': self.bc_id.api_pemilik.strip() or '',
                        'API PENERIMA': '',
                        'API PENGUSAHA': self.bc_id.api_pengusaha.strip() or '',
                        'ASAL DATA': self.bc_id.asal_data.strip() or '',
                        'ASURANSI': '0.00',
                        'BIAYA TAMBAHAN': '0.00',
                        'BRUTO': sum(line.brutto for line in picking_id.move_ids_without_package) or '',
                        'CIF': sum(line.price_subtotal for line in sale_line_ids) or '0.00',
                        'CIF RUPIAH': sum(line.currency_id.compute(line.price_subtotal, line.company_id.currency_id) for line in sale_line_ids) or '0.00',
                        'DISKON': '0.00',
                        'FLAG PEMILIK': '',
                        'URL DOKUMEN PABEAN': '',
                        'FOB': '0.00',
                        'FREIGHT': '0.00',
                        'HARGA BARANG LDP': '0.00',
                        'HARGA INVOICE': '0.00',
                        'HARGA PENYERAHAN': sum(inv.amount_total for inv in invoice_ids) or '',
                        'HARGA TOTAL': '0.00',
                        'ID MODUL': self.bc_id.id_modul.strip() or '',
                        'ID PEMASOK': '',
                        'ID PEMILIK': self.company_id.vat.strip() or '',
                        'ID PENERIMA BARANG': picking_id.partner_id.vat.strip() or '',
                        'ID PENGIRIM':'',
                        'ID PENGUSAHA': self.company_id.vat.strip() or '',
                        'ID PPJK': '',
                        'JABATAN TTD': self.bc_id.jabatan_ttd.strip() or '',
                        # 'JUMLAH BARANG': sum(move.quantity_done for move in picking_id.move_ids_without_package) or '',
                        'JUMLAH BARANG': len(picking_id.move_ids_without_package) or '', # jumlah line
                        'JUMLAH KEMASAN': jml_type_kemasan,
                        'JUMLAH KONTAINER': '0',
                        'KESESUAIAN DOKUMEN': '',
                        'KETERANGAN': '',
                        'KODE ASAL BARANG': '',
                        'KODE ASURANSI': '',
                        'KODE BENDERA': '',
                        'KODE CARA ANGKUT': picking_id.kode_cara_angkut.strip() or '',
                        'KODE CARA BAYAR': '',
                        'KODE DAERAH ASAL': '',
                        'KODE FASILITAS': '',
                        'KODE FTZ': '',
                        'KODE HARGA': '',
                        'KODE ID PEMASOK': '',
                        'KODE ID PEMILIK': self.bc_id.kode_id_pemilik.strip() or '',
                        'KODE ID PENERIMA BARANG': self.bc_id.kode_id_penerima.strip() or '',
                        'KODE ID PENGIRIM': '',
                        'KODE ID PENGUSAHA': self.bc_id.kode_id_pengusaha.strip() or '',
                        'KODE ID PPJK': '',
                        'KODE JENIS API': '',
                        'KODE JENIS API PEMILIK': '2',
                        'KODE JENIS API PENERIMA': '',
                        'KODE JENIS API PENGUSAHA': '2',
                        'KODE JENIS BARANG': '',
                        'KODE JENIS BC25': '',
                        'KODE JENIS NILAI': '',
                        'KODE JENIS PEMASUKAN01': '',
                        'KODE JENIS PEMASUKAN 02': '',
                        'KODE JENIS TPB': self.bc_id.kode_jenis_tpb.strip() or '',
                        'KODE KANTOR BONGKAR': '',
                        'KODE KANTOR TUJUAN': '',
                        'KODE LOKASI BAYAR': '1',
                        '': '',
                        'KODE NEGARA PEMASOK': '',
                        'KODE NEGARA PENGIRIM': '',
                        'KODE NEGARA PEMILIK': '',
                        'KODE NEGARA TUJUAN': '',
                        'KODE PEL BONGKAR': '',
                        'KODE PEL MUAT': '',
                        'KODE PEL TRANSIT': '',
                        'KODE PEMBAYAR': '1',
                        'KODE STATUS PENGUSAHA': '',
                        'STATUS PERBAIKAN': '',
                        'KODE TPS': '',
                        'KODE TUJUAN PEMASUKAN': '',
                        'KODE TUJUAN PENGIRIMAN': '',
                        'KODE TUJUAN TPB': '',
                        'KODE TUTUP PU': '',
                        'KODE VALUTA': valuta or '',
                        'KOTA TTD': self.bc_id.kota_ttd.strip() or '',
                        'NAMA PEMILIK': self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                        'NAMA PENERIMA BARANG': picking_id.partner_id.name.strip() or '',
                        'NAMA PENGANGKUT': '',
                        'NAMA PENGIRIM': '',
                        'NAMA PPJK': '',
                        'NAMA TTD': self.bc_id.nama_ttd.strip() or '',
                        'NDPBM': picking_id.ndpbm_id.get_rate(str(picking_id.scheduled_date)),
                        'NETTO': sum(move.product_id.weight * move.product_qty for move in picking_id.move_ids_without_package) or '',
                        'NILAI INCOTERM': '',
                        'NIPER PENERIMA': '',
                        'NOMOR API': '',
                        'NOMOR BC11': '',
                        'NOMOR BILLING': '',
                        'NOMOR DAFTAR': '',
                        'NOMOR IJIN BPK PEMASOK': '',
                        'NOMOR IJIN BPK PENGUSAHA': '',
                        'NOMOR IJIN TPB': self.bc_id.nomor_ijin_tpb.strip() or '',
                        'NOMOR IJIN TPB PENERIMA': '',
                        'NOMOR VOYV FLIGHT': '',
                        'NPWP BILLING': '010615326055000',
                        'POS BC11': '',
                        'SERI': self.bc_id.seri or '',
                        'SUBPOS BC11': '',
                        'SUB SUBPOS BC11': '',
                        'TANGGAL BC11': '',
                        'TANGGAL BERANGKAT': '',
                        'TANGGAL BILLING': '',
                        'TANGGAL DAFTAR': '',
                        'TANGGAL IJIN BPK PEMASOK': '',
                        'TANGGAL IJIN BPK PENGUSAHA': '',
                        'TANGGAL IJIN TPB': '',
                        'TANGGAL NPPPJK': '',
                        'TANGGAL TIBA': '',
                        'TANGGAL TTD': self.reformat_date(picking_id.tgl_ttd.strftime('%Y-%m-%d')) if picking_id.tgl_ttd else '',
                        'TANGGAL JATUH TEMPO': '',
                        'TOTAL BAYAR': '0.00', #todo
                        'TOTAL BEBAS': '0.00',
                        'TOTAL DILUNASI': '0.00',
                        'TOTAL JAMIN': '',
                        'TOTAL SUDAH DILUNASI': '',
                        'TOTAL TANGGUH': '',
                        'TOTAL TANGGUNG': '0.00',
                        'TOTAL TIDAK DIPUNGUT': '0.00',
                        'URL DOKUMEN PABEAN': '',
                        'VERSI MODUL': self.bc_id.versi_modul.strip() or '',
                        'VOLUME': '',
                        'WAKTU BONGKAR': '',
                        'WAKTU STUFFING': '',
                        'NOMOR POLISI': '',#picking_id.no_polisi.strip() or '',
                        'CALL SIGN': '',
                        'JUMLAH TANDA PENGAMAN': '0',
                        'KODE JENIS TANDA PENGAMAN': '',
                        'KODE KANTOR MUAT': '',
                        'KODE PEL TUJUAN': '',
                        '': '',
                        'TANGGAL STUFFING': '',
                        'TANGGAL MUAT': '',
                        'KODE GUDANG ASAL': '',
                        'KODE GUDANG TUJUAN': '',
                    })
            res.append(['Header'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'CIF',
                'CIF RUPIAH',
                'HARGA PENYERAHAN',
                'HARGA PEROLEHAN',
                'JENIS SATUAN',
                'JUMLAH SATUAN',
                'KODE ASAL BAHAN BAKU',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE JENIS DOK ASAL',
                'KODE KANTOR',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'MERK',
                'NDPBM',
                'NETTO',
                'NOMOR AJU DOKUMEN ASAL',
                'NOMOR DAFTAR DOKUMEN ASAL',
                'POS TARIF',
                'SERI BARANG DOKUMEN ASAL',
                'SPESIFIKASI LAIN',
                'TANGGAL DAFTAR DOKUMEN ASAL',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'SERI IJIN'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                    seri_barang = 1
                    identik = []
                    for move in picking_id.move_ids_without_package :
                        if not move.sale_line_id :
                            continue
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        for line in move.move_line_ids :
                            finished_move_line_id = self.env['stock.move.line'].search([
                                ('product_id','=',line.product_id.id),
                                ('lot_id','=',line.lot_id.id),
                                ('move_id.production_id','!=',False),
                                ('state','=','done'),
                            ], limit=1)
                            if not finished_move_line_id :
                                continue
                            # ratio = line.qty_done/finished_move_line_id.production_id.product_qty
                            # for raw_line in finished_move_line_id.production_id.move_raw_ids :
                            # naik dulu ke move_id baru ke production_id
                            ratio = line.qty_done/finished_move_line_id.move_id.production_id.product_qty
                            raw_ids = finished_move_line_id.move_id.production_id.move_raw_ids
                            if finished_move_line_id.move_id.production_id.reprocess_mo_id:
                                raw_ids = finished_move_line_id.move_id.production_id.reprocess_mo_id.move_raw_ids
                            for raw_line in raw_ids :
                                for move_line in raw_line.active_move_line_ids :
                                    incoming_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',move_line.product_id.id),
                                        ('lot_id','=',move_line.lot_id.id),
                                        ('move_id.production_id','=',False),
                                        ('state','=','done'),
                                        ('move_id.location_id.usage','=','supplier'),
                                    ], limit=1)
                                    if not incoming_move_line_id :
                                        continue
                                    if incoming_move_line_id.picking_id.no_pabean:
                                        dok_asal = incoming_move_line_id.picking_id.no_pabean.strip()
                                    else :
                                        dok_asal = ''
                                    kode_jenis_dok_asal = ''
                                    kode_asal_bahan_baku = '0'
                                    if incoming_move_line_id.picking_id.nomor_aju_id.name:
                                        kode_jenis_dok_asal = incoming_move_line_id.picking_id.nomor_aju_id.name[4:6]
                                        if kode_jenis_dok_asal in ('25','27'):
                                            kode_asal_bahan_baku = '0'
                                        elif kode_jenis_dok_asal == '40':
                                            kode_asal_bahan_baku = '1'
                                    rasio_bb = line.qty_done/raw_line.raw_material_production_id.bom_id.product_qty
                                    jml_bb =  raw_line.raw_material_production_id.bom_id.bom_line_ids.filtered(lambda p:p.product_id.categ_id.name == 'Wire').product_qty* rasio_bb 
                                    # rate pajak
                                    kurs_rate = 1
                                    if incoming_move_line_id.move_id.purchase_line_id :
                                        kurs_pajak_exist = kurs_pajak.sudo().search([('currency_id','=',incoming_move_line_id.move_id.purchase_line_id.currency_id.id)],limit=1)
                                        if kurs_pajak_exist :
                                            kurs = kurs_pajak_exist.rate_ids.sorted('date')[0]
                                            kurs_rate = kurs.rate
                                    #jml_bb = incoming_move_line_id.move_id.product_uom_qty * ratio
                                    identik_loop = aju_id.name+str(move.seri_barang)+str(incoming_move_line_id.move_id.product_id.name)+str(incoming_move_line_id.picking_id.nomor_aju_id.name)
                                    if identik_loop not in identik :
                                        values.append({
                                            'NOMOR AJU': aju_id.name.strip() or '',
                                            'SERI BARANG': move.seri_barang.strip() if move.seri_barang else '',
                                            'SERI BAHAN BAKU': incoming_move_line_id.move_id.seri_barang.strip() if incoming_move_line_id.move_id.seri_barang else '',
                                            'CIF': incoming_move_line_id.qty_done * incoming_move_line_id.move_id.purchase_line_id.price_unit or '0.00',
                                            'CIF RUPIAH': incoming_move_line_id.move_id.purchase_line_id.currency_id.compute(incoming_move_line_id.qty_done * incoming_move_line_id.move_id.purchase_line_id.price_unit, incoming_move_line_id.move_id.purchase_line_id.company_id.currency_id) or '0.00',
                                            'HARGA PENYERAHAN': '',#incoming_move_line_id.move_id.purchase_line_id.price_unit or '',
                                            'HARGA PEROLEHAN': '',
                                            'JENIS SATUAN': incoming_move_line_id.move_id.product_uom.name.strip() or '',
                                            'JUMLAH SATUAN': round(jml_bb,2) or '',
                                            'KODE ASAL BAHAN BAKU': kode_asal_bahan_baku,
                                            'KODE BARANG': incoming_move_line_id.move_id.product_id.default_code.strip() or '',
                                            'KODE FASILITAS': '',
                                            'KODE JENIS DOK ASAL': kode_jenis_dok_asal,
                                            'KODE KANTOR': self.bc_id.kode_kantor_pabean_asal.strip() or '',
                                            'KODE SKEMA TARIF': '',
                                            'KODE STATUS': self.bc_id.kode_status.strip() or '',
                                            'MERK': '',
                                            'NDPBM': kurs_rate,#picking_id.ndpbm_id.get_rate(str(picking_id.scheduled_date)),
                                            'NETTO': incoming_move_line_id.netto or '0.0000',
                                            'NOMOR AJU DOKUMEN ASAL': incoming_move_line_id.picking_id.nomor_aju_id.name.strip() or '',
                                            'NOMOR DAFTAR DOKUMEN ASAL': dok_asal,
                                            'POS TARIF': incoming_move_line_id.move_id.product_id.group_id.kode_tarif.strip() or '',
                                            'SERI BARANG DOKUMEN ASAL': incoming_move_line_id.picking_id.seri_dokumen if incoming_move_line_id.picking_id.seri_dokumen else '',
                                            'SPESIFIKASI LAIN': incoming_move_line_id.move_id.product_id.group_id.notes.strip() if incoming_move_line_id.move_id.product_id.group_id.notes else incoming_move_line_id.move_id.product_id.group_id.name.strip() or '',
                                            'TANGGAL DAFTAR DOKUMEN ASAL': incoming_move_line_id.picking_id.tgl_pabean.strftime("%Y-%m-%d").strip() if incoming_move_line_id.picking_id.tgl_pabean else '',
                                            'TIPE': incoming_move_line_id.move_id.product_id.type_id.name.strip() or '',
                                            'UKURAN': '',
                                            'URAIAN': incoming_move_line_id.move_id.product_id.name.strip() or '',
                                            'SERI IJIN': '',
                                        })
                                        identik.append(identik_loop)
            res.append(['BahanBaku'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE ASAL BAHAN BAKU',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'KODE SATUAN',
                'KODE TARIF',
                'NILAI BAYAR',
                'NILAI FASILITAS',
                'NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                    seri_barang = 1
                    identik = []
                    for move in picking_id.move_ids_without_package :
                        if not move.sale_line_id :
                            continue
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        for line in move.move_line_ids :
                            finished_move_line_id = self.env['stock.move.line'].search([
                                ('product_id','=',line.product_id.id),
                                ('lot_id','=',line.lot_id.id),
                                ('move_id.production_id','!=',False),
                                ('state','=','done'),
                            ], limit=1)
                            if not finished_move_line_id :
                                continue
                            # ratio = line.qty_done/finished_move_line_id.production_id.product_qty
                            # for raw_line in finished_move_line_id.production_id.move_raw_ids :
                            # naik dulu ke move_id baru ke production_id
                            ratio = line.qty_done/finished_move_line_id.move_id.production_id.product_qty
                            raw_ids = finished_move_line_id.move_id.production_id.move_raw_ids
                            if finished_move_line_id.move_id.production_id.reprocess_mo_id:
                                raw_ids = finished_move_line_id.move_id.production_id.reprocess_mo_id.move_raw_ids
                            for raw_line in raw_ids :
                                for move_line in raw_line.active_move_line_ids :
                                    incoming_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',move_line.product_id.id),
                                        ('lot_id','=',move_line.lot_id.id),
                                        ('move_id.production_id','=',False),
                                        ('state','=','done'),
                                        ('move_id.location_id.usage','=','supplier'),
                                    ], limit=1)
                                    if not incoming_move_line_id :
                                        continue
                                    jenis_tarif = ''
                                    tarif = ''
                                    # if incoming_move_line_id.product_id.group_id.tarif_bm :
                                    #     jenis_tarif = 'BM'
                                    #     kode_fasilitas = 2
                                    # elif incoming_move_line_id.product_id.group_id.ppn :
                                    #     jenis_tarif = 'PPN'
                                    #     kode_fasilitas = 5
                                    # elif incoming_move_line_id.product_id.group_id.pph :
                                    #     jenis_tarif = 'PPH'
                                    #     kode_fasilitas = 5
                                    # if not jenis_tarif:
                                    #     raise Warning('Product group belum di set (%s) ' % incoming_move_line_id.product_id.name)
                                    for trf in ['BM','PPN','PPH']:
                                        if trf == 'BM' :
                                            kode_fasilitas = 2
                                            tarif = incoming_move_line_id.product_id.group_id.tarif_bm
                                        elif trf == 'PPN':
                                            kode_fasilitas = 5
                                            tarif = incoming_move_line_id.product_id.group_id.ppn
                                        elif trf == 'PPH' :
                                            kode_fasilitas = 5
                                            tarif = incoming_move_line_id.product_id.group_id.pph
                                        tax_id = incoming_move_line_id.move_id.product_id.supplier_taxes_id
                                        identik_loop = aju_id.name+str(move.seri_barang )+str(trf)+str(incoming_move_line_id.move_id.picking_id.name)
                                        if identik_loop not in identik :
                                            values.append({
                                                'NOMOR AJU': aju_id.name.strip() or '',
                                                'SERI BARANG': move.seri_barang if move.seri_barang else '',
                                                'SERI BAHAN BAKU': incoming_move_line_id.move_id.seri_barang if incoming_move_line_id.move_id.seri_barang else '',
                                                'JENIS TARIF': trf,
                                                'JUMLAH SATUAN': '',
                                                'KODE ASAL BAHAN BAKU': '1' if incoming_move_line_id.move_id.purchase_line_id.order_id.purchase_type == 'Local' else '0',
                                                'KODE FASILITAS': '4' if incoming_move_line_id.move_id.purchase_line_id.order_id.purchase_type == 'Local' else '0',
                                                'KODE KOMODITI CUKAI': '',
                                                'KODE SATUAN': '',
                                                'KODE TARIF': move_line.product_id.group_id.kode_bm.strip() or '',
                                                'NILAI BAYAR': tax_id._compute_amount(incoming_move_line_id.qty_done*incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.qty_done) or '0.00',
                                                'NILAI FASILITAS': tax_id._compute_amount(incoming_move_line_id.qty_done*incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.qty_done) or '0.00',
                                                'NILAI SUDAH DILUNASI': '',
                                                'TARIF': tarif,
                                                'TARIF FASILITAS': '100.00',
                                            })
                                        identik.append(identik_loop)
                                        seri_barang += 1
            res.append(['BahanBakuTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'SERI DOKUMEN',
                'KODE ASAL BAHAN BAKU'
            ]
            values = []
            res.append(['BahanBakuDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'ASURANSI',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG KENDARAAN',
                'FOB',
                'FREIGHT',
                'BARANG BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA SATUAN',
                'JENIS KENDARAAN',
                'JUMLAH BAHAN BAKU',
                'JUMLAH KEMASAN',
                'JUMLAH SATUAN',
                'KAPASITAS SILINDER',
                'KATEGORI BARANG',
                'KODE ASAL BARANG',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE GUNA',
                'KODE JENIS NILAI',
                'KODE KEMASAN',
                'KODE LEBIH DARI 4 TAHUN',
                'KODE NEGARA ASAL',
                'KODE SATUAN',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'KONDISI BARANG',
                'MERK',
                'NETTO',
                'NILAI INCOTERM',
                'NILAI PABEAN',
                'NOMOR MESIN',
                'POS TARIF',
                'SERI POS TARIF',
                'SPESIFIKASI LAIN',
                'TAHUN PEMBUATAN',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'VOLUME',
                'SERI IJIN',
                'ID EKSPORTIR',
                'NAMA EKSPORTIR',
                'ALAMAT EKSPORTIR',
                'KODE PERHITUNGAN'
            ]
            values = []
            for aju_id in no_aju_ids :
                # search kanban:
                kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                for kanban_id in kanban_ids :  
                    for move in kanban_id.detail_ids.sorted('kanban_sequence') : 
                    #for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                        jml_type_kemasan = 0
                        packs = []
                        # for kemasan in picking_id.kemasan_ids:
                        for kemasan in kanban_id.kemasan_ids:
                            if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                                packs.append(kemasan.package_type_id.id)
                                jml_type_kemasan+=1

                        seri_barang = 1
                        # for move in picking_id.move_ids_without_package :
                        if not move.sale_line_id :
                            continue
                                
                        jumlah_item_bahan_baku = self.env['stock.move']

                        for line in move.move_line_ids :
                            finished_move_line_id = self.env['stock.move.line'].search([
                                ('product_id','=',line.product_id.id),
                                ('lot_id','=',line.lot_id.id),
                                ('move_id.production_id','!=',False),
                                ('state','=','done'),
                            ], limit=1)
                            if not finished_move_line_id :
                                continue
                            if finished_move_line_id.move_id.production_id.move_raw_ids :
                                jumlah_item_bahan_baku |= finished_move_line_id.move_id.production_id.move_raw_ids
                        jumlah_bahan_baku = len(jumlah_item_bahan_baku.mapped('product_id'))
                         # rate pajak
                        kurs_rate = 1
                        if move.sale_line_id.currency_id :
                            kurs_pajak_exist = kurs_pajak.sudo().search([('currency_id','=',move.sale_line_id.currency_id.id)],limit=1)
                            if kurs_pajak_exist :
                                kurs = kurs_pajak_exist.rate_ids.sorted('date')[0]
                                kurs_rate = kurs.rate
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        kode_kemasan = ''
                        if kanban_id.kemasan_ids:
                            kode_kemasan = kanban_id.kemasan_ids[0].package_type_id.code if kanban_id.kemasan_ids[0].package_type_id else ''
                        values.append({
                            'NOMOR AJU': aju_id.name.strip() or '',
                            'SERI BARANG': move.seri_barang or '',
                            'ASURANSI': '',
                            'CIF': move.quantity_done * move.sale_line_id.price_unit or '0.00',
                            'CIF RUPIAH': move.quantity_done * (move.sale_line_id.price_unit * kurs_rate) or '0.00',
                            #'CIF RUPIAH': move.sale_line_id.currency_id.compute(move.quantity_done * move.sale_line_id.price_unit, move.company_id.currency_id) or '0.00',
                            'DISKON': '',
                            'FLAG KENDARAAN': '',
                            'FOB': '',
                            'FREIGHT': kanban_id.freight or '0.00',
                            'BARANG BARANG LDP': '',
                            'HARGA INVOICE': '',
                            'HARGA PENYERAHAN': move.quantity_done * (move.sale_line_id.price_unit * kurs_rate) or '0.00',
                            'HARGA SATUAN': '',
                            'JENIS KENDARAAN': '',
                            'JUMLAH BAHAN BAKU': jumlah_bahan_baku,
                            'JUMLAH KEMASAN': jml_type_kemasan,
                            'JUMLAH SATUAN': jumlah_bahan_baku,#move.quantity_done or '',
                            'KAPASITAS SILINDER': '',
                            'KATEGORI BARANG': '1',#'11' if move.product_id.categ_id.name == 'Wire' else '14',
                            'KODE ASAL BARANG': '', #to be confirm
                            'KODE BARANG': move.product_id.default_code.strip() or '',
                            'KODE FASILITAS': '',
                            'KODE GUNA': '4',
                            'KODE JENIS NILAI': '',
                            'KODE KEMASAN': kode_kemasan or '', #todo
                            'KODE LEBIH DARI 4 TAHUN': '0',
                            'KODE NEGARA ASAL': 'ID',
                            'KODE SATUAN': move.product_uom.name.strip() or '',
                            'KODE SKEMA TARIF': '',
                            'KODE STATUS': self.bc_id.kode_status.strip() or '',
                            'KONDISI BARANG': '', #todo
                            'MERK': move.product_id.default_code.strip() or '',
                            'NETTO': move.netto or '',
                            'NILAI INCOTERM': '',
                            'NILAI PABEAN': '',
                            'NOMOR MESIN': '',
                            'POS TARIF': move.product_id.group_id.kode_tarif.strip() or '',
                            'SERI POS TARIF': '',
                            'SPESIFIKASI LAIN': move.product_id.group_id.notes.strip() if move.product_id.group_id.notes else move.product_id.group_id.name.strip() or '',
                            'TAHUN PEMBUATAN': '',
                            'TIPE': move.product_id.type_id.name.strip() or '',
                            'UKURAN': '',
                            'URAIAN': move.product_id.name.strip() or '',
                            'VOLUME': move.quantity_done * move.product_id.volume or '',
                            'SERI IJIN': '',
                            'ID EKSPORTIR': '',
                            'NAMA EKSPORTIR': '',
                            'ALAMAT EKSPORTIR': '',
                            'KODE PERHITUNGAN': '0',
                        })
            res.append(['Barang'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'TARIF KODE SATUAN',
                'TARIF KODE TARIF',
                'TARIF NILAI BAYAR',
                'TARIF NILAI FASILITAS',
                'TARIF NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                    seri_barang = 1
                    for move in picking_id.move_ids_without_package :
                        if not move.sale_line_id :
                            continue
                        # for tax_id in move.product_id.taxes_id :
                        #     values.append({
                        #         'NOMOR AJU': aju_id.name.strip() or '',
                        #         'SERI BARANG': seri_barang,
                        #         'JENIS TARIF': tax_id.name.strip() or '',
                        #         'JUMLAH SATUAN': '',
                        #         'KODE FASILITAS': '',
                        #         'KODE KOMODITI CUKAI': '',
                        #         'TARIF KODE SATUAN': '',
                        #         'TARIF KODE TARIF': tax_id.kode_tarif.strip() or '',
                        #         'TARIF NILAI BAYAR': '0.00',
                        #         'TARIF NILAI FASILITAS': tax_id._compute_amount(move.quantity_done*move.sale_line_id.price_unit,move.sale_line_id.price_unit,move.quantity_done) or '',
                        #         'TARIF NILAI SUDAH DILUNASI': '',
                        #         'TARIF': tax_id.amount or '',
                        #         'TARIF FASILITAS': '100.00',
                        #     })
                        #jenis_tarif = False
                        tarif= ''
                        kode_trf = ''
                        # if move.product_id.group_id.tarif_bm :
                        #     jenis_tarif = 'BM'
                        #     kode_fasilitas = 2
                        # elif move.product_id.group_id.ppn :
                        #     jenis_tarif = 'PPN'
                        #     kode_fasilitas = 5
                        # elif move.product_id.group_id.pph :
                        #     jenis_tarif = 'PPH'
                        #     kode_fasilitas = 5
                        # if not jenis_tarif:
                        #     raise Warning('Product group belum di set (%s) ' % move.product_id.name)
                        for trf in ['BM','PPN','PPH']:
                            if trf == 'BM' :
                                tarif = move.product_id.group_id.tarif_bm
                                kode_trf = '1'
                                kode_fasilitas = 2
                            elif trf == 'PPN':
                                tarif = move.product_id.group_id.ppn
                                kode_trf = ''
                                kode_fasilitas = 5
                            elif trf == 'PPH' :
                                tarif = move.product_id.group_id.pph
                                kode_trf = ''
                                kode_fasilitas = 5
                            if not tarif:
                                raise Warning('Jenis tarif product group belum di set (%s) ' % move.product_id.name)
                            values.append({
                                    'NOMOR AJU': aju_id.name.strip(),
                                    'SERI BARANG': seri_barang,
                                    'JENIS TARIF': trf,
                                    'JUMLAH SATUAN': '',
                                    'KODE FASILITAS': '',#kode_fasilitas,
                                    'KODE KOMODITI CUKAI': '',
                                    'TARIF KODE SATUAN': '',
                                    'TARIF KODE TARIF': kode_trf,#tarif.strip() or '',
                                    'TARIF NILAI BAYAR': float(tarif)*(move.quantity_done*move.sale_line_id.price_unit),
                                    'TARIF NILAI FASILITAS': '',
                                    'TARIF NILAI SUDAH DILUNASI': '',
                                    'TARIF': tarif,
                                    'TARIF FASILITAS': '100.00',
                                })
                        seri_barang += 1
            res.append(['BarangTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI DOKUMEN'
            ]
            values = []
            res.append(['BarangDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI DOKUMEN',
                'FLAG URL DOKUMEN',
                'KODE JENIS DOKUMEN',
                'NOMOR DOKUMEN',
                'TANGGAL DOKUMEN',
                'TIPE DOKUMEN',
                'URL DOKUMEN'
            ]
            values = []
            for aju_id in no_aju_ids :
                move_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking_id.location_dest_id.usage == 'customer').mapped('move_ids_without_package')
                sale_line_ids = move_ids.mapped('sale_line_id')
                if not sale_line_ids :
                    continue
                sale_ids = sale_line_ids.mapped('order_id')
                invoice_ids = sale_ids.mapped('invoice_ids')
                seri_dokumen = 1
                seri_tab_dokumen = 1
                # search kanban:
                kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id','=',aju_id.id)])
                for kanban_id in kanban_ids :
                    for move in kanban_id.detail_ids.filtered(lambda x:x.picking_id).mapped('picking_id'):
                        move.seri_dokumen = seri_dokumen
                        seri_dokumen += 1
                    for dok in kanban_id.dokumen_ids :
                        values.append({
                            'NOMOR AJU': aju_id.name.strip() or '',
                            'SERI DOKUMEN': seri_tab_dokumen,
                            'FLAG URL DOKUMEN': '',
                            'KODE JENIS DOKUMEN': dok.dokumen_master_id.code.strip() or '',
                            'NOMOR DOKUMEN': dok.nomor,
                            'TANGGAL DOKUMEN': self.reformat_date(dok.tanggal.strftime('%Y-%m-%d')) if dok.tanggal else '',
                            'TIPE DOKUMEN': '02',
                            'URL DOKUMEN': '',
                        })
                        seri_tab_dokumen += 1
                # for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
                #     if not self.bc_id.kode_jenis_dokumen_sj :
                #         continue
                #     picking_id.seri_dokumen = seri_dokumen
                #     seri_dokumen += 1
                #     values.append({
                #         'NOMOR AJU': aju_id.name.strip() or '',
                #         'SERI DOKUMEN': picking_id.seri_dokumen.strip() or '',
                #         'FLAG URL DOKUMEN': '',
                #         'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_sj.strip() or '',
                #         'NOMOR DOKUMEN': picking_id.name.strip() or '',
                #         'TANGGAL DOKUMEN': self.reformat_date(picking_id.tgl_dokumen.strftime('%Y-%m-%d')) if picking_id.tgl_dokumen else '',
                #         'TIPE DOKUMEN': self.bc_id.tipe_dokumen2.strip() or '',
                #         'URL DOKUMEN': '',
                #     })
                # for invoice_id in invoice_ids :
                #     if not self.bc_id.kode_jenis_dokumen_inv :
                #         continue
                #     invoice_id.seri_dokumen = seri_dokumen
                #     seri_dokumen += 1
                #     values.append({
                #         'NOMOR AJU': aju_id.name.strip() or '',
                #         'SERI DOKUMEN': invoice_id.seri_dokumen or '',
                #         'FLAG URL DOKUMEN': '',
                #         'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_inv.strip() or '',
                #         'NOMOR DOKUMEN': invoice_id.number.strip() or '',
                #         'TANGGAL DOKUMEN': self.reformat_date(invoice_id.date_invoice.strftime('%Y-%m-%d')) if invoice_id.date_invoice else '',
                #         'TIPE DOKUMEN': self.bc_id.tipe_dokumen2.strip() or '',
                #         'URL DOKUMEN': '',
                #     })
                # # tambah packing list
                # move_line_ids = move_ids.mapped('move_line_ids')
                # # for kemasan in aju_id.picking_ids.mapped('kemasan_ids') :
                # #     kemasan.seri = seri_dokumen
                # #     seri_dokumen += 1
                # #     values.append({
                # #         'NOMOR AJU': aju_id.name.strip() or '',
                # #         'SERI DOKUMEN': kemasan.seri,
                # #         'FLAG URL DOKUMEN': '',
                # #         'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_package.strip() or '',
                # #         'NOMOR DOKUMEN': kemasan.package_type_id.name.strip() or '',
                # #         'TANGGAL DOKUMEN': self.reformat_date(kemasan.create_date.strftime('%Y-%m-%d')) if kemasan.create_date else '',
                # #         'TIPE DOKUMEN': self.bc_id.tipe_dokumen2.strip() or '',
                # #         'URL DOKUMEN': '',
                # #     })
                # incoming_picking_ids = self.env['stock.picking']
                # for line in move_line_ids :
                #     finished_move_line_id = self.env['stock.move.line'].search([
                #         ('product_id','=',line.product_id.id),
                #         ('lot_id','=',line.lot_id.id),
                #         ('move_id.production_id','!=',False),
                #         ('state','=','done'),
                #     ], limit=1)
                #     if not finished_move_line_id :
                #         continue
                #     for raw_line in finished_move_line_id.move_id.production_id.move_raw_ids :
                #         for move_line in raw_line.active_move_line_ids :
                #             incoming_move_line_id = self.env['stock.move.line'].search([
                #                 ('product_id','=',move_line.product_id.id),
                #                 ('lot_id','=',move_line.lot_id.id),
                #                 ('move_id.production_id','=',False),
                #                 ('state','=','done'),
                #                 ('move_id.location_id.usage','=','supplier'),
                #             ], limit=1)
                #             if not incoming_move_line_id :
                #                 continue
                #             incoming_picking_ids |= incoming_move_line_id.picking_id
                # for incoming_picking_id in incoming_picking_ids :
                #     if not self.bc_id.kode_jenis_dokumen_bahan_baku :
                #         continue
                #     incoming_picking_id.seri_dokumen = seri_dokumen
                #     seri_dokumen += 1
                #     values.append({
                #         'NOMOR AJU': aju_id.name.strip() or '',
                #         'SERI DOKUMEN': incoming_picking_id.seri_dokumen or '',
                #         'FLAG URL DOKUMEN': '',
                #         'KODE JENIS DOKUMEN': self.bc_id.kode_jenis_dokumen_bahan_baku.strip() or '',
                #         'NOMOR DOKUMEN': incoming_picking_id.name.strip() or '',
                #         'TANGGAL DOKUMEN': self.reformat_date(incoming_picking_id.tgl_dokumen.strftime('%Y-%m-%d')) if incoming_picking_id.tgl_dokumen else '',
                #         'TIPE DOKUMEN': self.bc_id.tipe_dokumen.strip() or '',
                #         'URL DOKUMEN': '',
                #     })
            res.append(['Dokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KEMASAN',
                'JUMLAH KEMASAN',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE JENIS KEMASAN',
                'MEREK KEMASAN',
                'NIP GATE IN',
                'NIP GATE OUT',
                'NOMOR POLISI',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT',
            ]
            values = []
            for aju_id in no_aju_ids :
                kemasan_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer').mapped('kemasan_ids')
                for kemasan_id in kemasan_ids.filtered(lambda x:x.qty > 0.0) :
                    if kemasan_id.package_type_id :
                        values.append({
                            'NOMOR AJU': aju_id.name.strip() or '',
                            'SERI KEMASAN': '',
                            'JUMLAH KEMASAN': kemasan_id.qty or '0.00',
                            'KESESUAIAN DOKUMEN': '',
                            'KETERANGAN': '',
                            'KODE JENIS KEMASAN': kemasan_id.package_type_id.code.strip() if kemasan_id.package_type_id.code else  '',
                            'MEREK KEMASAN': 'NITTO',#kemasan_id.picking_id.no_sj_pengirim.strip() if kemasan_id.picking_id.no_sj_pengirim else '',
                            'NIP GATE IN': '',
                            'NIP GATE OUT': '',
                            'NOMOR POLISI': '',
                            'NOMOR SEGEL': '',
                            'WAKTU GATE IN': '',
                            'WAKTU GATE OUT': '',
                        })
            res.append(['Kemasan'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE STUFFING',
                'KODE TIPE KONTAINER',
                'KODE UKURAN KONTAINER',
                'FLAG GATE IN',
                'FLAG GATE OUT',
                'NOMOR POLISI',
                'NOMOR KONTAINER',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT'
            ]
            values = []
            res.append(['Kontainer'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON',
                'TANGGAL RESPON',
                'WAKTU RESPON',
                'BYTE STRAM PDF'
            ]
            values = []
            res.append(['Respon'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON'
            ]
            values = []
            res.append(['Status'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'JENIS TARIF',
                'NPWP BILLING'
            ]
            values = []
            for aju_id in no_aju_ids :
                move_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer').mapped('move_ids_without_package')
                sale_line_ids = move_ids.mapped('sale_line_id')
                if not sale_line_ids :
                    continue
                # sale_ids = sale_line_ids.mapped('order_id')
                # invoice_ids = sale_ids.mapped('invoice_ids')
                # for invoice_id in invoice_ids :
                #     for inv_line in invoice_id.invoice_line_ids :
                #         for tax_id in inv_line.product_id.taxes_id :
                #             values.append({
                #                 'NOMOR AJU': aju_id.name.strip() or '',
                #                 'JENIS TARIF': tax_id.name.strip() or '',
                #                 'NPWP BILLING': self.company_id.vat.strip() or '',
                #             })
                for tarif in ['BM','PPN','PPH']:
                    values.append({'NOMOR AJU': aju_id.name.strip() or '',
                                'JENIS TARIF': tarif,
                                'NPWP BILLING': self.company_id.vat.strip() or '',
                            })
            res.append(['Billing'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'JENIS TARIF',
                'KODE FASILITAS',
                'NILAI PUNGUTAN'
            ]
            values = []
            res.append(['Pungutan'] + [headers, values])

            no_aju_ids.write({'has_export':True})
            no_aju_ids.mapped('picking_ids').write({'has_export':True})

        return res
