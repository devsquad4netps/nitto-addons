from odoo import fields, models, api, _
from odoo.exceptions import Warning

class VitExportImportBc(models.Model):
    _inherit = 'vit.export.import.bc'

    name = fields.Selection(selection_add=[('BC 2.5','BC 2.5')])
