# -*- coding: utf-8 -*-
{
    'name': "Automatic Lot  Number create on MO",

    'summary': """
        Menambahkan Lot pas create Manufacuture Order.
        Auto assign Lot waktu finish 

        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gmail.com",
    'website': "http://vitraining.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.3',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','mrp','sh_barcode_scanner'],

    # always loaded
    'data': [
        'views/lot.xml',
        'views/stock.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}