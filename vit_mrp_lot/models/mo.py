# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class MrpProduction(models.Model):
	_name ='mrp.production'
	_inherit = 'mrp.production'


	@api.model
	def create(self, values):
		res = super(MrpProduction, self).create(values)
		lot = self.env['stock.production.lot']
		lot_exist = lot.sudo().search([('name','=',res.name),('product_id','=',res.product_id.id)])
		if not lot_exist:
			lot_data = {
				'name': res.name,
				'product_id': res.product_id.id
			}
			lot.create(lot_data)
		return res

MrpProduction()

class MrpWorkorder(models.Model):
	_name ='mrp.workorder'
	_inherit = 'mrp.workorder'

	@api.model
	def create(self, values):
		res = super(MrpWorkorder, self).create(values)
		
		lot = self.env['stock.production.lot'].sudo().search([('name','=',res.production_id.name),
														('product_id','=',res.product_id.id)], limit=1)
		if not lot :
			lot_data = {'name': res.production_id.name,'product_id': res.product_id.id}
			lot = self.env['stock.production.lot'].create(lot_data)
		res['final_lot_id'] = lot.id
		return res

	@api.multi
	def button_start(self):
		self.ensure_one()
		if not self.final_lot_id :
			object_lot = self.env['stock.production.lot']
			lot_exist = object_lot.search([('name','=',self.production_id.name)],limit=1)
			if lot_exist :
				lot = lot_exist
			else :
				lot_data = {'name': self.production_id.name,'product_id': self.product_id.id}
				lot = object_lot.create(lot_data)
			self.final_lot_id = lot.id
		return super(MrpWorkorder, self).button_start()
		

	@api.multi
	def button_finish(self):
		self.ensure_one()
		if not self.final_lot_id :
			object_lot = self.env['stock.production.lot']
			lot_exist = object_lot.search([('name','=',self.production_id.name)],limit=1)
			if lot_exist :
				lot = lot_exist
			else :
				lot_data = {'name': self.production_id.name,'product_id': self.product_id.id}
				lot = object_lot.create(lot_data)
			self.final_lot_id = lot.id
		return super(MrpWorkorder, self).button_finish()

MrpWorkorder()