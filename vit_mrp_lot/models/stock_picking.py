from odoo import api, fields, models, _
import time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

# class StockPicking(models.Model):
#     _name = 'stock.picking'
#     _inherit = 'stock.picking'


#     def action_assign(self):
#         import pdb;pdb.set_trace()
#         res = super(picking, self).action_assign()
#         for pick in self :
#             move_ids = pick.move_ids_without_package
#             for move in move_ids.filtered(lambda mv:mv.location_id.usage == 'production') :
#                 lot = self.env['stock.production.lot'].search([
#                     ('product_id', '=', move.product_id.id),
#                     ('name', '=', pick.origin)
#                 ])
#                 for line in move.move_line_ids:
#                     line.lot_id = lot.id

#         return res

# StockPicking()


class StockMoves(models.Model):
    _inherit = 'stock.move'

    def _action_assign(self):
        res = super(StockMoves, self)._action_assign()
        for move in self.filtered(lambda mv:mv.production_id) :
            lot = self.env['stock.production.lot'].search([
                ('product_id', '=', move.product_id.id),
                ('name', '=', move.production_id.name)
            ])
            if lot :
                for line in move.move_line_ids:
                    line.lot_id = lot.id
                    wo_id = move.production_id.workorder_ids.filtered(lambda wo:not wo.next_work_order_id and wo.state=='done')
                    if wo_id :
                        line.qty_done = wo_id.qty_produced
            #move._quantity_done_compute()

        return res

StockMoves()        