from datetime import datetime
from dateutil import relativedelta
from itertools import groupby
from operator import itemgetter

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare, float_round, float_is_zero
from odoo.exceptions import UserError, Warning, ValidationError

class StockMove(models.Model):
	_name ='stock.move'
	_inherit = ["barcodes.barcode_events_mixin", "stock.move"] 

	lot = fields.Many2one('stock.production.lot','Lot/Serial number', compute='create_data')

	def create_data(self):
		for ress in self:
			#import pdb;pdb.set_trace()
			if ress.location_id.usage == 'production' :
				data = self.env['stock.production.lot'].search([
					('product_id','=', ress.product_id.id),
					('name', '=', ress.picking_id.origin)
					])
				if data : 
					ress.lot = data.id

	def _action_assign(self):
		res = super(StockMove, self)._action_assign()
		for mv in self:
			if mv.location_dest_id.usage == 'production':
				# hapus jika data ada yg double :
				for line in mv.move_line_ids :
					if line.product_qty == 0.0 and line.qty_done == 0.0 and not line.lot_id:
						line.unlink()		
		return res
	
	def _add_lot(self, barcode):
		#step 1 make sure order in proper state.
		if self and self.state in ["cancel","done"]:
			selections = self.fields_get()["state"]["selection"]
			value = next((v[1] for v in selections if v[0] == self.state), self.state)
			raise UserError(_("You can not scan item in %s state.") %(value))
				
		#step 2 increaset lot product not in stock move line than create new line.
		elif self :
			search_lines = False
			domain = []         
			barcode_scan = barcode.split('#')
			if len(barcode_scan) > 2 :
				barcode = barcode_scan[0]+'#'+barcode_scan[1]+'#'+barcode_scan[3]
			search_lines = self.move_line_ids.filtered(lambda mvl: mvl.lot_id.name == barcode)   
			domain = [("name","=",barcode)]                                            
			if search_lines:
				raise UserError(_("lot %s sudah di scan.") %(barcode))
			else:
				search_lot = self.env["stock.production.lot"].sudo().search(domain, limit = 1)
				if not search_lot :
					raise UserError(_("lot %s tidak ditemukan di master lot.") %(barcode))
				search_lines_null = self.move_line_ids.filtered(lambda mvl: not mvl.lot_id)
				qty_done = search_lot.product_qty
				sisa = self.reserved_availability-self.quantity_done
				if sisa > 0.0 and sisa <= search_lot.product_qty :
					qty_done = sisa
				if search_lines_null :
					search_lines_null[0].lot_id = search_lot.id
					search_lines_null[0].qty_done = qty_done
				else :
					vals = {'move_id': self.id,
							'product_id': self.product_id.id,
							'lot_id':search_lot.id,
							#'package_id':,
							'qty_done':qty_done,
							'product_uom_id':self.product_uom.id,
							'location_id':self.location_id.id,
							'location_dest_id':self.location_dest_id.id
					}                     
					new_move_line = self.move_line_ids.new(vals)
					self.move_line_ids |= new_move_line
					new_move_line._compute_bruto_netto()
			   
			
	def on_barcode_scanned(self, barcode):
		self._add_lot(barcode)
		# if self.raw_material_production_id :
		# 	self._add_lot(barcode)
		# else :
		# 	super(StockMove, self).on_barcode_scanned(barcode)

class StockPicking(models.Model):
	_name = "stock.picking"
	_inherit = ['barcodes.barcode_events_mixin', 'stock.picking']   
	
	def _add_lot(self, barcode):
		#step 1 make sure order in proper state.
		if self and self.state in ["cancel","done"]:
			selections = self.fields_get()["state"]["selection"]
			value = next((v[1] for v in selections if v[0] == self.state), self.state)
			raise UserError(_("You can not scan item in %s state.") %(value))
				
		#step 2 increaset lot product not in stock move line than create new line.
		elif self :
			for move in self.move_ids_without_package:
				if move.show_details_visible:
					raise UserError(_("You can not scan product item for lot/serial directly here, Please click detail button (at end each line) and than rescan your product item."))
				search_lines = False
				domain = []          
				search_lines = move.move_line_ids.filtered(lambda mvl: mvl.lot_id.name == barcode)   
				domain = [("name","=",barcode)]                                            
				if search_lines:
					raise UserError(_("lot %s sudah di scan.") %(barcode))
				else:
					search_lot = self.env["stock.production.lot"].sudo().search(domain, limit = 1)
					if not search_lot :
						raise UserError(_("lot %s tidak ditemukan di master lot.") %(barcode))
					search_lines_null = move.move_line_ids.filtered(lambda mvl: not mvl.lot_id)
					if search_lines_null :
						search_lines_null[0].lot_id = search_lot.id
					else :
						sisa = move.reserved_avaibility-move.quantity_done
						if sisa > 0.0 and sisa <= search_lot.product_qty :
							qty_done = sisa
						else:
							qty_done = search_lot.product_qty 
						vals = {'move_id': move.id,
								'product_id': move.product_id.id,
								'lot_id':search_lot.id,
								#'package_id':,
								'qty_done':qty_done,
								'product_uom_id':move.product_uom.id,
								'location_id':move.location_id.id,
								'location_dest_id':move.location_dest_id.id
						}                     
						new_move_line = move.move_line_ids.new(vals)
						move.move_line_ids |= new_move_line
						new_move_line._compute_bruto_netto()

	def on_barcode_scanned(self, barcode):
		if self.picking_type_id.name == 'Consume' :
			self._add_lot(barcode)
		else :
			super(StockPicking, self).on_barcode_scanned(barcode)