from odoo import api, fields, models, _
from datetime import datetime, date,  timedelta
from odoo.exceptions import UserError, ValidationError


class CreateMrpWizard(models.TransientModel):

    _name = 'create.mrp.wizard'

    def _get_active_applicant(self):
        context = self.env.context
        if context.get('active_model') == 'forecast.forecast_mrp_new':
           return context.get('active_ids', False)
        return False

    forecast_mrp_ids = fields.Many2many(comodel_name="forecast.forecast_mrp_new", required=False, string='Forecast Mrp', default=_get_active_applicant)

    #edit indra 03/19 
    def action_mrp_create(self):
        Std_PolyBox = 0
        qty_Box = 0 
        res_Box = 0
        qty = 0 
        sisa = 0
        ranges = 0
        qty_prod = 0
        for x in self.forecast_mrp_ids:
            # pdb.set_trace();
            Std_PolyBox = x.Std_PolyBox
            qty_Box = x.qty_Box
            qty_prod = x.qty
            qty = qty_Box * Std_PolyBox
            sisa = qty_prod % Std_PolyBox
            ranges = qty_prod // Std_PolyBox
            result = []
            for i in range(int(ranges)):
                picking_type = self.env['stock.picking.type'].sudo().search([('warehouse_id.company_id','=',x.company_id.id),('code','=','mrp_operation')], limit=1)
                data = {
                          'product_id'        : x.product_id.id,
                          'product_qty'       : qty,
                          'product_uom_id'    : x.product_id.uom_id.id,
                          'bom_id'            : x.bom_id.id,
                          'date_planned_start': datetime.now(),
                          'user_id'           : self.env.uid,
                          'customer_id'       : x.partner_id.id,
                          'company_id'        : x.company_id.id,
                          'picking_type_id'   : picking_type.id,
                          'location_src_id'   : picking_type.default_location_src_id.id,
                          'location_dest_id'  : picking_type.default_location_dest_id.id,
                        }

                inv = self.env['mrp.production'].sudo().create(data)
                

            if sisa != 0:
                #create mo
                picking_type = self.env['stock.picking.type'].sudo().search([('warehouse_id.company_id','=',x.company_id.id),('code','=','mrp_operation')], limit=1)
                data = {
                            'product_id'        : x.product_id.id,
                            'product_qty'       : sisa,
                            'product_uom_id'    : x.product_id.uom_id.id,
                            'bom_id'            : x.bom_id.id,
                            'date_planned_start': datetime.now(),
                            'user_id'           : self.env.uid,
                            'customer_id'       : x.partner_id.id,
                            'company_id'        : x.company_id.id,
                            'picking_type_id'   : picking_type.id,
                            'location_src_id'   : picking_type.default_location_src_id.id,
                            'location_dest_id'  : picking_type.default_location_dest_id.id,
                        }
                inv = self.env['mrp.production'].sudo().create(data)