#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)


class production(models.Model):
    _name = "mrp.production"
    _inherit = "mrp.production"


    @api.model_cr
    def init(self):
        _logger.info("creating create MO from forecast function...")
        self.env.cr.execute("""
            CREATE OR REPLACE FUNCTION create_mo_from_forecast(forecast_id integer, mo_name varchar, v_company_id integer, product_id integer, uom_id integer,
            qty numeric, v_bom_id integer, routing_id integer, warehouse_id integer,location_src_id integer, location_dest_id integer,
            code_cust_id integer, no_poly_box varchar, origin varchar, uid integer, customer_id integer, picking_type_id integer , jml_mo integer, date_planned_start timestamp, date_planned_finished timestamp
             )

            RETURNS void AS
            $BODY$
            DECLARE
                v_bom_line record;
                v_bom record;
                v_product record;
                v_product_tmpl record;
                v_group_id integer;
                v_mo_id integer;
                v_stock_move_id integer;
                v_component_qty numeric;
                v_warna text;
                
            BEGIN
                -- cari product_product
                SELECT * from product_product where id = product_id into v_product;

                -- cari product_template
                SELECT * from product_template where id = v_product.product_tmpl_id into v_product_tmpl;

                IF v_product_tmpl.warna is not null
                THEN
                    v_warna = v_product_tmpl.warna;
                ELSE
                    v_warna = '';
                END IF;

                -- create procurement grup
                INSERT INTO "procurement_group" ("id", "create_uid", "create_date", "write_uid", "write_date", "move_type", "name") 
                VALUES (nextval('procurement_group_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), 'direct', mo_name) RETURNING id INTO v_group_id;
                
                -- create MO
                INSERT INTO "mrp_production" ("id", "create_uid", "create_date", "write_uid", "write_date",
                    "no_poly_box","customer_id","code_cust_id","routing_id",
                    "bom_id", "company_id", "is_locked", 
                    "location_dest_id", "location_src_id", 
                    "name", "origin", "picking_type_id", "priority", "procurement_group_id", "product_id", "product_qty", 
                    "product_uom_id", "propagate", "state", "user_id","jml_lot", "warna","date_planned_finished", "date_planned_start")
                VALUES (nextval('mrp_production_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), 
                    no_poly_box, customer_id, code_cust_id, routing_id,v_bom_id, v_company_id,  true, 
                    location_dest_id, location_src_id, 
                    mo_name, origin, picking_type_id, '1', v_group_id, product_id, qty, 
                    uom_id, false, 'confirmed', uid, jml_mo, v_warna, date_planned_start,date_planned_finished)
                RETURNING id INTO v_mo_id;   

                -- CREATE MRP LIST
                INSERT INTO "mrp_list" ("id", "create_uid", "create_date", "write_uid", "write_date", "forecast_id", "mrp_id")
                VALUES (nextval('mrp_list_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), forecast_id, v_mo_id);
                
                -- create stock_move finished
                INSERT INTO "stock_move" ("id", "create_uid", "create_date", "write_uid", "write_date", "additional", 
                    "company_id", "date", "date_expected", 
                    "group_id", "is_done", "location_dest_id", "location_id", 
                    "name", "origin", "picking_type_id", "priority", "procure_method", 
                    "product_id", "product_uom", "product_uom_qty", "product_qty","value",
                    "production_id", "propagate", "scrapped", "sequence", "state", "to_refund", "warehouse_id") 
                VALUES (nextval('stock_move_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), false, 
                    v_company_id, (now() at time zone 'UTC'), (now() at time zone 'UTC'), 
                    v_group_id, false, location_dest_id, location_src_id, 
                    mo_name, mo_name, picking_type_id, '1', 'make_to_stock', 
                    product_id, uom_id, qty, qty, 0.0,
                    v_mo_id, false, false, 10, 'draft', false, warehouse_id) RETURNING id INTO v_stock_move_id;

                -- cari qty BoM
                SELECT product_qty from mrp_bom where id = v_bom_id into v_bom;
                -- create stock_move raw material
                FOR v_bom_line IN SELECT * FROM mrp_bom_line WHERE bom_id=v_bom_id
                LOOP
                    v_component_qty := (v_bom_line.product_qty * qty)/ v_bom.product_qty;
                    INSERT INTO "stock_move" ("id", "create_uid", "create_date", "write_uid", "write_date", "additional", 
                        "bom_line_id", "company_id", "date", "date_expected", "group_id", "is_done", 
                        "location_dest_id", "location_id", "name", "operation_id", "origin", "picking_type_id", "price_unit", 
                        "priority", "procure_method", "product_id", "product_uom", "product_uom_qty", "product_qty","value","propagate", 
                        "raw_material_production_id", "scrapped", "sequence", "state", "to_refund", "unit_factor", "warehouse_id") 
                    VALUES (nextval('stock_move_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), false, 
                        v_bom_line.id, v_company_id,(now() at time zone 'UTC'), (now() at time zone 'UTC'), v_group_id, false, 
                        location_dest_id, location_src_id, mo_name, NULL, mo_name, picking_type_id, 0.0, 
                        '1', 'make_to_stock', v_bom_line.product_id, v_bom_line.product_uom_id, v_component_qty,v_component_qty, 0.0, false, 
                        v_mo_id, false, 1, 'confirmed', false, 1.0, warehouse_id) RETURNING id INTO v_stock_move_id;
                    
                END LOOP;

            END;
            $BODY$
            LANGUAGE plpgsql VOLATILE
            COST 100;
        """)

    no_poly_box = fields.Char(string="No Poly Box",size=3, required=True)
    jml_lot = fields.Integer('Total Lot', default=1, copy=False)

production()


class MrpBom(models.Model):
    _inherit = 'mrp.bom'
    _rec_name = 'display_name'

    display_name = fields.Char(compute='_compute_display_name', store=True, index=True)

    @api.multi
    @api.depends('product_tmpl_id.name', 'product_tmpl_id.default_code','code')
    def name_get(self):
        result = []
        for res in self:
            name = res.product_tmpl_id.name
            if res.product_tmpl_id.default_code :
                name = '['+res.product_tmpl_id.default_code+'] '+name
            if res.code :
                name = res.code+': '+name
            result.append((res.id, name))
        return result

    @api.depends('product_tmpl_id.name', 'product_tmpl_id.default_code', 'code')
    def _compute_display_name(self):
        diff = dict(show_address=None, show_address_only=None, show_email=None, html_format=None, show_vat=False)
        names = dict(self.with_context(**diff).name_get())
        for bom in self:
            bom.display_name = names.get(bom.id)

MrpBom()