#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class product(models.Model):
    _name = "product.product"
    _inherit = "product.product"

    plant_id = fields.Many2one(comodel_name="forecast.plant",  string="Plant",  help="")

product()


class producttemplate(models.Model):
    _inherit = 'product.template'

    qty_Box         = fields.Float("Qty Box/Lot")
    
producttemplate()
