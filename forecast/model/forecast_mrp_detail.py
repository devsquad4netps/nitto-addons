#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class forecast_mrp_detail(models.Model):

    _name = "forecast.forecast_mrp_detail"
    name = fields.Float(string="Name", digits=(6,0), help="")
    product_id = fields.Many2one(comodel_name="product.product",  string="Product",  help="")
    product_tmpl_id = fields.Many2one('product.template', 'Product Template', related='product_id.product_tmpl_id', readonly=True)
    qty = fields.Float( string="Quantity", digits=(6,0),)
    bom_id = fields.Many2one('mrp.bom', 'Bill of Material')
    forecast_mrp_id = fields.Many2one(comodel_name="forecast.forecast_mrp",  string="Forecast salesman",  help="")
    mrp_ref_id = fields.One2many('forecast.forecast_mrp_detail_ref',  inverse_name="mrp_detail", string='Manufacture')

class forecast_mrp_detail_ref(models.Model) :

    _name = "forecast.forecast_mrp_detail_ref"

    mrp_detail    = fields.Many2one("forecast.forecast_mrp_detail",)
    mrp_id        = fields.Many2one("mrp.production",)
