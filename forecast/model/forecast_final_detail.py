#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class forecast_final_detail(models.Model):
    _name = "forecast.forecast_final_detail"
    _description = "Forecast Final Detail"


    name = fields.Float( required=True, string="Name", digits=(6,0), help="")
    lt = fields.Float( string="LT", digits=(6,0), help="Lead time")
    std_pl = fields.Float( string="Std Polybox", digits=(6,0), help="Standard polybox / palet")
    delivery_min3 = fields.Float( string="Delivery Min3", digits=(6,0), help="SO rencana delivery 2 bulan sebelum")
    delivery_min2 = fields.Float( string="Delivery Min2", digits=(6,0), help="SO rencana delivery 1  bulan sebelum")
    delivery_min1 = fields.Float( string="Delivery Min1", digits=(6,0), help="SO rencana delivery bulan ini")
    delivery_avg = fields.Float( string="Delivery AVG", digits=(6,0), help="Rata rata rencana So untuk delivery 3 buan terakhir")
    so_min3 = fields.Float( string="SO Min3", digits=(6,0), help="Penjualan 2 bulan sebelum")
    so_min2 = fields.Float( string="SO Min2", digits=(6,0), help="Penjualan 1 bulan sebelum")
    so_min1 = fields.Float( string="SO Min1", digits=(6,0), help="Penjualan bulan ini")
    so_avg = fields.Float( string="SO AVG", digits=(6,0), help="Rata rata penjualan 3 bulan terakhir")
    loss_sales = fields.Float( string="Loss Sales", digits=(6,0), help="A. Outstanding SO yang belum terkirim sampai akhir bulan Juni")
    so_plus0 = fields.Float( string="SO Plus0", digits=(6,0), help="B. SO untuk delivery bulan ini")
    total_a_b = fields.Float( string="Loss Sales + SO Plus0", digits=(6,0), help="A + B")
    estimation_plus1 = fields.Float( string="Estimation Plus0", digits=(6,0),  help="Estimasi 1 bulan kedepan, total dari seluruh salesman")
    estimation_plus2 = fields.Float( string="Estimation Plus1", digits=(6,0), help="Estimasi 2 bulan kedepan, total dari seluruh salesman")
    estimation_plus3 = fields.Float( string="Estimation Plus2", digits=(6,0), help="Estimasi 3 bulan kedepan, total dari seluruh salesman")
    estimation_total = fields.Float( string="Estimation Total", digits=(6,0), help="Estimasi total")
    wip = fields.Float( string="WIP", digits=(6,0), help="Stock WIP akhir bulan ini")
    fg_local = fields.Float( string="FG Local", digits=(6,0), help="Kode barang awal 01")
    fg_import = fields.Float( string="FG Import", digits=(6,0), help="Kode barang awal 02")
    fg_total = fields.Float( string="FG Total", digits=(6,0), help="Total WIP dan FG")
    sisa_plus0 = fields.Float( string="Sisa Plus0", digits=(6,0), help="")
    sisa_plus1 = fields.Float( string="Sisa Plus1", digits=(6,0), help="")
    sisa_plus2 = fields.Float( string="Sisa Plus2", digits=(6,0), help="")


    product_id = fields.Many2one(comodel_name="product.product",  string="Product",  help="Item name")
    forecast_id = fields.Many2one(comodel_name="forecast.forecast_final",  string="Forecast",  ondelete="CASCADE")
    plant_id = fields.Many2one(comodel_name="forecast.plant",  string="Plant",  help="Lokasi produksi Heading")


forecast_final_detail()