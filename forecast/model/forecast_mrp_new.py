#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import xlwt
from io import BytesIO
from xlrd import open_workbook
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
import pdb
import math
import logging
_logger = logging.getLogger(__name__)


SESSION_STATES =[('draft','Draft'),('cancel','Cancel'),('validate','Validated')]

class forecast_mrp_new(models.Model):
    _name = "forecast.forecast_mrp_new"
    _inherit = ["mail.thread"]

    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(forecast_mrp_new, self).unlink()

    product_id = fields.Many2one(comodel_name="product.product",  string="Product",  help="" ,track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    product_tmpl_id = fields.Many2one(comodel_name="product.template",  string="Product Tmpl", related="product_id.product_tmpl_id" ,store=True)# tambah utk domain bom biar ga lemot
    name = fields.Char( required=True, string="Source Reference",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    state = fields.Char( string="State",  help="",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    periode_id = fields.Many2one(comodel_name="forecast.periode",  string="Periode",   domain=[('is_active','=',True)],track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    qty = fields.Float( string="Quantity Produce",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    bom_id = fields.Many2one('mrp.bom', 'Bill of Material',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    routing_id = fields.Many2one('mrp.routing', 'Routing',related='bom_id.routing_id',readonly=True,states={'draft': [('readonly', False)]})
    Std_PolyBox = fields.Float("Standart Poly Box",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    qty_Box = fields.Float("Qty Box",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    result_mo = fields.Float("Result MO",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    company_id = fields.Many2one(comodel_name="res.company",  string="Company",  help="", required=True,track_visibility='onchange', readonly=True,states={'draft': [('readonly', False)]},default=lambda self: self.env['res.company']._company_default_get())
    dept   = fields.Char('Dept',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one(comodel_name="res.partner",  string="Customer",  help="",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    department_id = fields.Many2one(comodel_name="hr.department",  string="Department",  help="",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    parent_mo  = fields.Char('No Parent MO',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    ref   = fields.Char('Reference',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    mrplist_id = fields.One2many("mrp.list", "forecast_id", "List mrp's" ,readonly=True,states={'draft': [('readonly', False)]})
    state               = fields.Selection(string="State", 
                                        selection=SESSION_STATES,
                                        required=True,
                                        readonly=True,
                                        default=SESSION_STATES[0][0],track_visibility='onchange')
    mrp_count = fields.Integer('MRP Count', compute='_compute_mrp_count')
    one_result = fields.Boolean('Generate 1 MO ?', default=False, copy=False, help="True jika MO yg dihasilkan hanya 1 MO")
    auto_run = fields.Boolean('Auto Generate ?',track_visibility='onchange', default=False, copy=False, help="True jika MO digenerate by scheduler")
    date_planned_start = fields.Datetime('Deadline Start', copy=False, default=fields.Datetime.now,states={'draft': [('readonly', False)]}, readonly=True)

    _sql_constraints = [
        ('name', 'unique(name,company_id)',
         'Name of forecast must be unique per company'),
    ]

    @api.multi
    def _compute_mrp_count(self):
        for fr in self :
            datas = self.env['mrp.production'].search([('name', 'ilike', fr.parent_mo),('state','!=','cancel')])
            if datas:
                fr.mrp_count = len(datas)

    def get_action_mrp_forecast(self):
        views = [(self.env.ref('mrp.mrp_production_tree_view').id,'tree'),(self.env.ref('mrp.mrp_production_form_view').id,'form')]
        return {
        'name' : _('Manufacturing Order from Forecast'),
        'domain' : [('name', 'ilike', self.parent_mo),('state','!=','cancel')],
        'view_type' : 'form',
        'res_model' : 'mrp.production',
        'context' : {},
        'view_id' : False,
        'views': views,
        'view_mode' : 'tree,form',
        'type':'ir.actions.act_window',
            }

    @api.multi
    @api.onchange('product_id')
    def on_product_id(self):
        # pdb.set_trace();
        if self.product_id.id:
            picking_type = self.env['product.template'].browse(self.product_id.product_tmpl_id.id)
            self.Std_PolyBox = picking_type.Std_PolyBox
            self.qty_Box = picking_type.qty_Box
        else:
            self.Std_PolyBox = 0.00
            self.qty_Box = 0.00

    @api.multi
    def action_done(self):
        self.state = SESSION_STATES[2][0]

    @api.multi
    def action_set_to_draft(self):
        self.state = SESSION_STATES[0][0]

    @api.multi
    def action_cancel(self):
        for x in self.mrplist_id:
            x.mrp_id.action_cancel()
        self.state = SESSION_STATES[1][0]

    @api.model
    def _get_default_picking_type(self):
        return self.env['stock.picking.type'].search([
            ('code', '=', 'mrp_operation'),
            ('warehouse_id.company_id', 'in', [self.env.context.get('company_id', self.env.user.company_id.id), False])],
            limit=1).id

    @api.model
    def create(self, values): 
        picking = self._get_default_picking_type()
        picking_type_id = self.env['stock.picking.type'].browse(picking)
        if 'parent_mo' not in values or not values['parent_mo']:
            if picking_type_id:
                if 'department_id' in values and values['department_id'] != False:
                    name = str(picking_type_id.sequence_id.next_by_id())
                    deptt = self.env['hr.department'].browse(values['department_id'])
                    dept_code = deptt.code
                    end_code = name[3:]
                    name = dept_code+end_code
                    end_seq = name[:10]
                    values['parent_mo'] = end_seq+str(deptt.sequence).zfill(3) 
                    deptt.sudo().write({'sequence':deptt.sequence+1})
                    # values['parent_mo'] = str(deptt.code) + name[3:]
                else:
                    values['parent_mo'] = picking_type_id.sequence_id.next_by_id()
            else :
                raise UserError(_('Picking type mrp_production company %s tidak ditemukan.')%(self.env.user.company_id.name))
        new_id = super(forecast_mrp_new, self).create(values)
        return new_id




    # @api.multi
    # @api.onchange('product_id','qty','Std_PolyBox','qty_Box')
    # def on_change_qty(self):
    #     data = []
    #     for record in self:
    #         data.append(record.Std_PolyBox)
    #         data.append(record.qty_Box)
    #         data.append(record.qty)
    #     # pdb.set_trace();
    #     dt = 0
    #     dt1 = 0
    #     if 0.00 not in data:
    #         dt = (self.qty / self.Std_PolyBox) / self.qty_Box
    #         dt1 = round((self.qty / self.Std_PolyBox) / self.qty_Box)
    #         self.result_mo = math.ceil(dt)

    @api.onchange('product_id','qty','Std_PolyBox','qty_Box')
    def on_change_qty(self):
        for record in self:
            try :
                dt = (record.qty / record.Std_PolyBox) / record.qty_Box
                record.result_mo = math.ceil(dt)
            except ZeroDivisionError :
                record.result_mo = 0

    @api.multi
    def fill_product(self): 
        manuf_order = self.env['mrp.production']
        picking_type = self.env['stock.picking.type']
        sequence = self.env['ir.sequence']
        uid = self.env.uid
        for x in self:
            Std_PolyBox = 0
            qty_Box = 0 
            res_Box = 0
            qty = 0 
            sisa = 0
            ranges = 0
            qty_prod = 0
            # if x.mrp_count != 0 :
            #     continue
            if not x.parent_mo :
                raise ValidationError(_('Nomor parent MO %s belum di isi') % x.name)
            if not x.product_id.warna_id:
                raise ValidationError(_('Warna ID pada product ini [%s] belum di isi') % x.product_id.name)
            if not x.product_id.warna:
                raise ValidationError(_('Warna pada product ini [%s] belum di isi') % x.product_id.name)
            if not x.product_id.Std_PolyBox:
                raise ValidationError(_('Standar Polybox pada product ini [%s] belum di isi') % x.product_id.name)
            customer_code_id = self.env['product.customer.code'].search([('product_id','=',x.product_id.id),
                                                                        ('partner_id','=',x.partner_id.id),
                                                                        ('company_id','=',x.company_id.id)])
            if not customer_code_id:
                raise ValidationError(_('Product customer code pada product ini [%s] belum di isi') % x.product_id.name)
            # search parent_mo
            parent_mo_exist = self.env['forecast.forecast_mrp_new'].sudo().search([('parent_mo','=',x.parent_mo)])
            if parent_mo_exist and len(parent_mo_exist) > 1 :
                raise ValidationError(_('Nomor parent MO %s sudah ada') % x.parent_mo)
            if len(x.product_id.product_customer_code_ids.filtered(lambda ss: ss.partner_id.id == x.partner_id.id)) == 0:
                raise ValidationError(_('Customer code ( %s ), product ( %s ) , partner ( %s ) belum di set !') % (x.name,x.product_id.name,x.partner_id.name))

            if not x.bom_id.routing_id:
                raise ValidationError(_('Routing BoM atas product ini [%s] belum di set') % x.product_id.name)
            if not x.bom_id.routing_id.operation_ids:
                raise ValidationError(_('Operation (workcenter) pada routing BoM atas product ini [%s] belum di isi') % x.product_id.name)

            # for ss in x.product_id.product_customer_code_ids.filtered(lambda ss: ss.partner_id.id == x.partner_id.id):
            #     dd = ss.id    
            code_cust_id = False
            code_cust = x.product_id.product_customer_code_ids.filtered(lambda ss: ss.partner_id.id == x.partner_id.id)
            if code_cust :
                code_cust_id = code_cust[0].id 

            Std_PolyBox = x.Std_PolyBox
            qty_Box = x.qty_Box
            qty_prod = x.qty
            qty = qty_Box * Std_PolyBox
            hasil_qty = qty * x.result_mo
            if hasil_qty != x.qty:
                sisa = x.qty - ( qty * (x.result_mo - 1))
            ranges = x.result_mo
            result = []
            mrplist = []
            no_poly_box = ''
            seq = 1

            for i in range(int(ranges)):
                no_poly_box = '1/'+str(int(x.qty_Box)) 
                if sisa != 0 or sisa != 0.00:
                    if i == (ranges - 1):
                        qty = Std_PolyBox * round(sisa/Std_PolyBox)
                        no_poly_box = '1/'+str(round(sisa/Std_PolyBox))
                # tambah logic sisa 30 jul 2020. WJ
                if qty == 0.0 :
                    sisa_box = sisa/Std_PolyBox
                    sisa_box = math.ceil(sisa_box)
                    qty = Std_PolyBox*sisa_box
                    no_poly_box = '1/'+ str(sisa_box)
                ref = x.name
                if x.ref :
                    ref = x.ref
                picking_type = picking_type.sudo().search([('warehouse_id.company_id','=',x.company_id.id),('code','=','mrp_operation')], limit=1)
                code = str(sequence.next_by_code('increment_no_mo_sequence'))
                mo_name = x.parent_mo + '-' + str(seq).zfill(3)
                mo_exist = manuf_order.sudo().search([('name','=',mo_name),('company_id','=',x.company_id.id)],limit=1)
                if mo_exist :
                    seq += 1
                    continue
                if qty > 0.0 :
                    self.env.cr.execute("select create_mo_from_forecast(%d, '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', '%s', %d, %d, %d, %d, '%s', '%s')" % (x.id, mo_name, 
                        x.company_id.id, x.product_id.id, x.product_id.uom_id.id, qty,x.bom_id.id,x.bom_id.routing_id.id,picking_type.warehouse_id.id,
                        picking_type.default_location_src_id.id,picking_type.default_location_dest_id.id,code_cust_id, no_poly_box, ref, uid, x.partner_id.id, picking_type.id, int(x.result_mo), x.date_planned_start,x.date_planned_start ))

                    # data = {
                    #           'name'              : mo_name,
                    #           'product_id'        : x.product_id.id,
                    #           'product_qty'       : qty,
                    #           'product_uom_id'    : x.product_id.uom_id.id,
                    #           'bom_id'            : x.bom_id.id,
                    #           'date_planned_start': datetime.now(),
                    #           'code_cust_id'      : code_cust_id,
                    #           'no_poly_box'       : no_poly_box,
                    #           'origin'            : ref,
                    #           'user_id'           : x.env.uid,
                    #           'customer_id'       : x.partner_id.id,
                    #           'company_id'        : x.company_id.id,
                    #           'picking_type_id'   : picking_type.id,
                    #           'location_src_id'   : picking_type.default_location_src_id.id,
                    #           'location_dest_id'  : picking_type.default_location_dest_id.id,
                    #         }
                    seq += 1
                    # try :
                    #     # mo = self.env['mrp.production'].sudo().create(data)
                    #     # if mo.unreserve_visible :
                    #     #     mo.button_unreserve()
                    #     # pdb.set_trace()
                    #     line_data = [(0,0,{
                    #             'mrp_id'      : mo.id,
                    #             })]
                    #     x.write({'mrplist_id' : line_data})
                    #     self.env.cr.commit()
                    # except:
                    #     continue
                    _logger.info("SQL create MO (%s) success... "%mo_name)

            for m_list in x.mrplist_id:
                m_list.mrp_id._compute_jml_box()
                m_list.mrp_id.assign_picking()
                _logger.info("SQL create picking MO (%s) success... "%m_list.mrp_id.name)
            record_id = self.env.ref('forecast.increment_no_mo_sequence')
            record_id.number_next_actual = 0
            x.action_done()
            # self.env.cr.commit()
        #return True


    @api.multi
    def button_auto_run(self):
        #self.ensure_one()x
        for x in self:
            try :
                x.auto_run = True
            except:
                pass

    @api.multi
    def button_not_auto_run(self):
        #self.ensure_one()
        for x in self:
            try :
                x.auto_run = False
            except :
                pass

    @api.model
    def auto_wip_mo(self):
        to_exec = self.search([('state','=','draft'),('auto_run','=',True)]) 
        if to_exec :
            for fr in to_exec :
                try :
                    fr.fill_product()
                    _logger.info("Run Forecast %s ========================> success... "%str(fr.name))
                except :
                    continue
        else :
            _logger.info("Run Forecast %s ========================> failed... (no data to run) ")
        return True

forecast_mrp_new()


class mrplist(models.Model):
    _name = 'mrp.list'

    forecast_id = fields.Many2one("forecast.forecast_mrp_new", "forecast_id", ondelete="cascade")
    mrp_id = fields.Many2one(comodel_name="mrp.production",  string="List",  help="")

mrplist()

    # def export_excel(self, ):
    #     # pass
    #     header_name_a1 = ['PERENCANAAN KEBUTUHAN PELANGGAN'
    #     ]
    #     header_name_a2 = [str(self.company_id.name),
    #     ]
    #     header_name_a3 = [
    #                     str(self.periode_id.name) +' '+str(self.periode_id.tahun),
    #     ]
    #     header_name = [
    #                      'NO',
    #                      'PRODUCT',
    #                      'LOKASI PRODUKSI',
    #                      'PART CUSTOMER',
    #                      'DELIVERY MIN 3',
    #                      'DELIVERY MIN 2',
    #                      'DELIVERY MIN 1',
    #                      'SALE ORDER MIN 3',
    #                      'SALE ORDER MIN 2',
    #                      'SALE ORDER MIN 1',
    #                      'DELIVERY PLUS 0',
    #                      'DELIVERY PLUS 1',
    #                      'DELIVERY PLUS 2',
    #                      'LOSS SALES',
    #                      'ESTIMASI PLUS 1',
    #                      'ESTIMASI PLUS 2',
    #                      'ESTIMASI PLUS 3',
    #                      'TOTAL FINISH GOOD',
    #                      'CUSTOMER',
    #                      ]
    #     obj = self.env['mrp.workcenter'].search([])
    #     for x in obj:
    #         header_name.append(x.name)

    #     workbook = xlwt.Workbook()
    #     for record in self:
    #         final_data = []
    #         line_data = []
    #         worksheet = workbook.add_sheet('Report Forecast', cell_overwrite_ok=True)
    #         final_data.append(header_name_a1)
    #         final_data.append(header_name_a2)
    #         final_data.append(header_name_a3)
    #         final_data.append(header_name)
    #         # pdb.set_trace()
    #         for z in record.line_ids:
    #             plan = ""
    #             if z.plant_id.name:
    #               plan = z.plant_id.name
    #             pcus = ""
    #             if z.part_customer:
    #               pcus = z.part_customer
    #             cus = ""
    #             if z.customer_id.name:
    #               cus = z.customer_id.name
    #             line_data = [
    #                          z.name,
    #                          z.product_id.default_code + '' + z.product_id.name,
    #                          plan,
    #                          pcus,
    #                          z.delivery_min3,
    #                          z.delivery_min2,
    #                          z.delivery_min1,
    #                          z.so_min3,
    #                          z.so_min2,
    #                          z.so_min1,
    #                          z.delivery_plus0,
    #                          z.delivery_plus1,
    #                          z.delivery_plus2,
    #                          z.loss_sales,
    #                          z.estimation_plus1,
    #                          z.estimation_plus2,
    #                          z.estimation_plus3,
    #                          z.total_finish_goods,
    #                          cus,
    #                          ]
    #             for y in z.wip_qty_ids:
    #                 line_data.append(y.quantity)

    #             final_data.append(line_data)

    #         for row in range(0, len(final_data)):
    #             for col in range(0, len(final_data[row])):
    #                 value = final_data[row][col]
    #                 worksheet.write(row, col, value)

    #     output = BytesIO()
    #     workbook.save(output)
    #     output.seek(0)
    #     self.name_ex = "%s%s" % ("Report Forecast", '.xls')
    #     self.export_data = base64.b64encode(output.getvalue())
    #     output.close()
