#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import xlwt
from io import BytesIO
from xlrd import open_workbook
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import pdb

class forecast_mrp(models.Model):

    _name = "forecast.forecast_mrp"
    name = fields.Char( required=True, string="Name",  help="")
    state = fields.Char( string="State",  help="")
    periode_id = fields.Many2one(comodel_name="forecast.periode",  string="Periode",   domain=[('is_active','=',True)])
    partner_id = fields.Many2one(comodel_name="res.partner",  string="Customer",  help="")
    company_id = fields.Many2one(comodel_name="res.company",  string="Company",  help="")
    line_ids = fields.One2many(comodel_name="forecast.forecast_mrp_detail",  inverse_name="forecast_mrp_id",  string="Lines",  help="")
    salesman_id = fields.Many2one(comodel_name="res.users",  string="Salesman",  help="")
    name_txt_file   = fields.Char('File Name', readonly=True)
    export_data     = fields.Binary("Export File")
    export_data_txt = fields.Binary("Export File")
    name_ex = fields.Char( string="Nameex",  help="")

    
    @api.multi
    def fill_product(self, ):
        for x in self:
            qty_bom = 0 
            sisa = 0
            ranges = 0
            qty_prod = 0
            # pdb.set_trace();

            for y in x.line_ids:
                qty_prod = y.qty
                qty_bom = y.bom_id.product_qty
                sisa = qty_prod % qty_bom
                ranges = qty_prod // qty_bom
                result = []
                for i in range(int(ranges)):
                    picking_type = self.env['stock.picking.type'].sudo().search([('warehouse_id.company_id','=',x.company_id.id),('code','=','mrp_operation')], limit=1)
                    data = {
                              'product_id'        : y.product_id.id,
                              'product_qty'       : qty_bom,
                              'product_uom_id'    : y.product_id.uom_id.id,
                              'bom_id'            : y.bom_id.id,
                              'date_planned_start': datetime.now(),
                              'user_id'           : x.salesman_id.id,
                              'customer_id'       : x.partner_id.id,
                              'company_id'        : x.company_id.id,
                              'picking_type_id'   : picking_type.id,
                              'location_src_id'   : picking_type.default_location_src_id.id,
                              'location_dest_id'  : picking_type.default_location_dest_id.id,
                            }

                    inv = self.env['mrp.production'].sudo().create(data)
                    result.append((0, 0, {'mrp_id': inv.id}))
                    

                if sisa != 0:
                  #create mo
                  picking_type = self.env['stock.picking.type'].sudo().search([('warehouse_id.company_id','=',x.company_id.id),('code','=','mrp_operation')], limit=1)
                  data = {
                            'product_id'        : y.product_id.id,
                            'product_qty'       : sisa,
                            'product_uom_id'    : y.product_id.uom_id.id,
                            'bom_id'            : y.bom_id.id,
                            'date_planned_start': datetime.now(),
                            'user_id'           : x.salesman_id.id,
                            'customer_id'       : x.partner_id.id,
                            'company_id'        : x.company_id.id,
                            'picking_type_id'   : picking_type.id,
                            'location_src_id'   : picking_type.default_location_src_id.id,
                            'location_dest_id'  : picking_type.default_location_dest_id.id,
                          }

                  inv = self.env['mrp.production'].sudo().create(data)
                  result.append((0, 0, {'mrp_id': inv.id}))
                y.name = "Done"
                y.mrp_ref_id = result


    # def export_excel(self, ):
    #     # pass
    #     header_name_a1 = ['PERENCANAAN KEBUTUHAN PELANGGAN'
    #     ]
    #     header_name_a2 = [str(self.company_id.name),
    #     ]
    #     header_name_a3 = [
    #                     str(self.periode_id.name) +' '+str(self.periode_id.tahun),
    #     ]
    #     header_name = [
    #                      'NO',
    #                      'PRODUCT',
    #                      'LOKASI PRODUKSI',
    #                      'PART CUSTOMER',
    #                      'DELIVERY MIN 3',
    #                      'DELIVERY MIN 2',
    #                      'DELIVERY MIN 1',
    #                      'SALE ORDER MIN 3',
    #                      'SALE ORDER MIN 2',
    #                      'SALE ORDER MIN 1',
    #                      'DELIVERY PLUS 0',
    #                      'DELIVERY PLUS 1',
    #                      'DELIVERY PLUS 2',
    #                      'LOSS SALES',
    #                      'ESTIMASI PLUS 1',
    #                      'ESTIMASI PLUS 2',
    #                      'ESTIMASI PLUS 3',
    #                      'TOTAL FINISH GOOD',
    #                      'CUSTOMER',
    #                      ]
    #     obj = self.env['mrp.workcenter'].search([])
    #     for x in obj:
    #         header_name.append(x.name)

    #     workbook = xlwt.Workbook()
    #     for record in self:
    #         final_data = []
    #         line_data = []
    #         worksheet = workbook.add_sheet('Report Forecast', cell_overwrite_ok=True)
    #         final_data.append(header_name_a1)
    #         final_data.append(header_name_a2)
    #         final_data.append(header_name_a3)
    #         final_data.append(header_name)
    #         # pdb.set_trace()
    #         for z in record.line_ids:
    #             plan = ""
    #             if z.plant_id.name:
    #               plan = z.plant_id.name
    #             pcus = ""
    #             if z.part_customer:
    #               pcus = z.part_customer
    #             cus = ""
    #             if z.customer_id.name:
    #               cus = z.customer_id.name
    #             line_data = [
    #                          z.name,
    #                          z.product_id.default_code + '' + z.product_id.name,
    #                          plan,
    #                          pcus,
    #                          z.delivery_min3,
    #                          z.delivery_min2,
    #                          z.delivery_min1,
    #                          z.so_min3,
    #                          z.so_min2,
    #                          z.so_min1,
    #                          z.delivery_plus0,
    #                          z.delivery_plus1,
    #                          z.delivery_plus2,
    #                          z.loss_sales,
    #                          z.estimation_plus1,
    #                          z.estimation_plus2,
    #                          z.estimation_plus3,
    #                          z.total_finish_goods,
    #                          cus,
    #                          ]
    #             for y in z.wip_qty_ids:
    #                 line_data.append(y.quantity)

    #             final_data.append(line_data)

    #         for row in range(0, len(final_data)):
    #             for col in range(0, len(final_data[row])):
    #                 value = final_data[row][col]
    #                 worksheet.write(row, col, value)

    #     output = BytesIO()
    #     workbook.save(output)
    #     output.seek(0)
    #     self.name_ex = "%s%s" % ("Report Forecast", '.xls')
    #     self.export_data = base64.b64encode(output.getvalue())
    #     output.close()
