#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import xlwt
from io import BytesIO
from xlrd import open_workbook
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import pdb

class forecast_final(models.Model):
    _name = "forecast.forecast_final"
    _description = "Forecast Final"
    _inherit = ["mail.thread"]


    name = fields.Char( required=True, string="Name",  help="",track_visibility='onchange')
    company_id = fields.Many2one(comodel_name="res.company",  string="Company",  default=lambda self: self.env.user.company_id)
    period_id = fields.Many2one(comodel_name="forecast.periode",  string="Period",  domain=[('is_active','=',True)],track_visibility='onchange')
    line_ids = fields.One2many(comodel_name="forecast.forecast_final_detail",  inverse_name="forecast_id",  string="Lines",  help="")
    name_txt_file   = fields.Char('File Name', readonly=True)
    export_data     = fields.Binary("Export File")
    export_data_txt = fields.Binary("Export File")
    name_ex = fields.Char( string="Nameex",  help="")

    def get_delivery(self,product,diff,date_sql,awal_dtc,akhir_dtc,stat):
        # sql_d_min = "select sum(qty_done) from stock_move_line mv " \
        #                 "left join uom_uom uom on uom.id = product_uom_id " \
        #                 "left join stock_picking picking on picking.id = picking_id " \
        #                 "where %s " \
        #                 "and %s " \
        #                 "and %s " \
        #                 "and picking_type_code = 'outgoing' " \
        #                 "and mv.state = 'done' "
        # if product == 56:
        #     pdb.set_trace()
        sql_d_min = "select sum(product_uom_qty) from stock_move mv " \
                        "left join uom_uom uom on uom.id = product_uom " \
                        "left join stock_picking_type pick on pick.id = picking_type_id " \
                        "left join stock_picking picking on picking.id = picking_id " \
                        "where %s " \
                        "and %s " \
                        "and %s " \
                        "and pick.code = 'outgoing' " \
                        "and %s "


        product_s = "product_id = %s " % (product)

        awal_date_min_ob = awal_dtc + relativedelta(months=diff)
        awal_date_min = str(awal_date_min_ob.date())
        akhir_date_min_ob = akhir_dtc + relativedelta(months=diff)
        akhir_date_min = str(akhir_date_min_ob.date())

        awal_date_min_res = "%s >= '%s 00:00:00' " % (date_sql,awal_date_min)
        akhir_date_min_res = "%s <= '%s 00:00:00' " % (date_sql,akhir_date_min)
        # pdb.set_trace()
        cr=self.env.cr
        cr.execute(sql_d_min % (product_s, awal_date_min_res,akhir_date_min_res,stat))
        res_d_min = cr.fetchall()
        delivery_min = 0
        if res_d_min[0] == (None,):
            delivery_min = 0
        else:
            delivery_min = res_d_min[0][0]

        return delivery_min

    def get_saleorder(self,product,diff,date_sql,awal_dtc,akhir_dtc):
        sql_so_min = "select sum(product_uom_qty) from sale_order_line sol " \
                        "left join uom_uom uom on uom.id = product_uom " \
                        "left join sale_order so on so.id = order_id " \
                        "where %s " \
                        "and %s " \
                        "and %s " \
                        "and so.state = 'sale' "

        product_s = "product_id = %s " % (product)

        awal_date_min_ob = awal_dtc + relativedelta(months=diff)
        awal_date_min = str(awal_date_min_ob.date())
        akhir_date_min_ob = akhir_dtc + relativedelta(months=diff)
        akhir_date_min = str(akhir_date_min_ob.date())

        awal_date_min_res = "%s >= '%s 00:00:00' " % (date_sql,awal_date_min)
        akhir_date_min_res = "%s <= '%s 00:00:00' " % (date_sql,akhir_date_min)
        cr=self.env.cr
        cr.execute(sql_so_min % (product_s, awal_date_min_res,akhir_date_min_res))
        res_d_min = cr.fetchall()
        so_min = 0
        if res_d_min[0] == (None,):
            so_min = 0
        else:
            so_min = res_d_min[0][0]

        return so_min

    def summarize(self, ):
        sql = "delete from forecast_forecast_final_detail where forecast_id = %s" % ( self.id)
        cr=self.env.cr
        cr.execute(sql)
        for x in self:
            # sql = "select product_id from product_customer_code prod_code " \
            #       "left join res_partner parts on parts.id = prod_code.partner_id " \
            #       "where parts.user_id = %s" % (self.salesman_id.id)
            # cr=self.env.cr
            # cr.execute(sql)
            # res = cr.fetchall()
            sql = "select prod.id from product_product prod " \
                  "left join product_template temp on temp.id = prod.product_tmpl_id " \
                  "left join product_category categ on categ.id = temp.categ_id " \
                  "where prod.default_code notnull and categ.name = 'Finish Good' "
            cr=self.env.cr
            cr.execute(sql)
            res = cr.fetchall()
            result = []
            i = 1

            for prod in res:
                sql2 = "select move_id from stock_move_line where product_id = %s" % ( prod[0])
                cr.execute(sql2)
                res1 = cr.fetchall()

                move_ids = []
                move_ids_int = []
                if res1 and res1[0]!= None:
                    for move in res1:
                        move_ids.append(str(move[0]))
                        move_ids_int.append(move[0])
                move_string = ""
                if move_ids:
                    move_string = ','.join(x for x in move_ids)

                # date_min3_ob = dtc
                day = ''
                if str(x.period_id.bulan) == '01' or str(x.period_id.bulan) == '1':
                    day = '31'
                elif str(x.period_id.bulan) == '02' or str(x.period_id.bulan) == '2':
                    day = '28'
                elif str(x.period_id.bulan) == '03' or str(x.period_id.bulan) == '3':
                    day = '31'
                elif str(x.period_id.bulan) == '04' or str(x.period_id.bulan) == '4':
                    day = '30'
                elif str(x.period_id.bulan) == '05' or str(x.period_id.bulan) == '5':
                    day = '31'
                elif str(x.period_id.bulan) == '06' or str(x.period_id.bulan) == '6':
                    day = '30'
                elif str(x.period_id.bulan) == '07' or str(x.period_id.bulan) == '7':
                    day = '31'
                elif str(x.period_id.bulan) == '08' or str(x.period_id.bulan) == '8':
                    day = '31'
                elif str(x.period_id.bulan) == '09' or str(x.period_id.bulan) == '9':
                    day = '30'
                elif str(x.period_id.bulan) == '10':
                    day = '31'
                elif str(x.period_id.bulan) == '11':
                    day = '30'
                elif str(x.period_id.bulan) == '12':
                    day = '31'

                #date awal
                awal_dt = str(x.period_id.tahun) + '-' + str(x.period_id.bulan) +'-01 00:00:00'
                awal_dtc = datetime.strptime(awal_dt, "%Y-%m-%d 00:00:00")
                #date awal
                #date akhir
                akhir_dt = str(x.period_id.tahun) + '-' + str(x.period_id.bulan) +'-' + day + ' 00:00:00'
                akhir_dtc = datetime.strptime(akhir_dt, "%Y-%m-%d 00:00:00")
                #date akhir

                #sql deliver min

                #sql deliver min

                del_min = 'picking.date_done'
                del_plus = 'picking.scheduled_date'
                stat = "mv.state = 'done' "
                # akhir_dt = str(x.period_id.tahun) + '-' + str(x.period_id.bulan) +'-' + '25' + ' 00:00:00'
                # akhir_dtc = datetime.strptime(akhir_dt, "%Y-%m-%d 00:00:00")
                ###########################################################################################################################
                delivery_min3 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_min,diff=-3,stat=stat)
                delivery_min2 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_min,diff=-2,stat=stat)
                delivery_min1 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_min,diff=-1,stat=stat)
                avg_del = (delivery_min3 + delivery_min2 + delivery_min1) / 3
                ###########################################################################################################################
                # akhir_dt = str(x.period_id.tahun) + '-' + str(x.period_id.bulan) +'-' + '23' + ' 00:00:00'
                # akhir_dtc = datetime.strptime(akhir_dt, "%Y-%m-%d 00:00:00")
                ###########################################################################################################################
                # stat = "mv.state = 'assigned' "
                # if prod[0] == 56:
                #     pdb.set_trace()
                # delivery_plus0 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_plus,diff=0,stat=stat)
                # delivery_plus1 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_plus,diff=1,stat=stat)
                # delivery_plus2 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_plus,diff=2,stat=stat)
                ###########################################################################################################################
                #sql deliver min
                #sql so min
                so_min = 'so.date_order'
                so_min3 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=-3)
                so_min2 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=-2)
                so_min1 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=-1)
                so_plus0 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=0)
                avg_so = (so_min3 + so_min2 + so_min1) / 3
                #sql dso min
                wip = 0
                sql_wo = "select sum(qty_producing) as qty from mrp_workorder " \
                        "where product_id = %s " \
                        "and state = 'progress' "

                cr.execute(sql_wo % (prod[0]))
                res_wo = cr.fetchall()
                if res_wo and res_wo[0]!= (None,):
                    # pdb.set_trace()
                    wip = res_wo[0][0]
                loss_sales = (so_min3 + so_min2 + so_min1) - (delivery_min3 + delivery_min2 + delivery_min1)
                total_a_b = so_plus0 + loss_sales
                ##################################################################################################
                estimation_plus1 = 0
                estimation_plus2 = 0
                estimation_plus3 = 0
                sql_forec = "select sum(estimation_plus1) as est_plus1, sum(estimation_plus2) as est_plus2, sum(estimation_plus3) as est_plus3 from forecast_forecast_salesman_detail " \
                            "left join forecast_forecast_salesman forec on forec.id = forecast_salesman_id " \
                            "where periode_id = %s and forec.state = 'validate' and product_id = %s"
                cr.execute(sql_forec % (self.period_id.id, prod[0]))
                res_forec = cr.fetchall()
                # if prod[0] == 53:
                #     pdb.set_trace()
                if res_forec[0][0]!= None:
                    estimation_plus1 = res_forec[0][0]
                if res_forec[0][1]!= None:
                    estimation_plus2 = res_forec[0][1]
                if res_forec[0][2]!= None:
                    estimation_plus3 = res_forec[0][2]
                estimation_total = estimation_plus1 + estimation_plus2 + estimation_plus3
                ##################################################################################################
                qty_ava = self.env['product.product'].browse(prod[0])
                fg_local = qty_ava.qty_available
                fg_total = fg_local + wip
                sisa_plus0 = fg_total - (loss_sales + estimation_plus1)
                sisa_plus1 = fg_total - (loss_sales + estimation_plus1)
                sisa_plus2 = fg_total - (loss_sales + estimation_plus1)
                result.append((0, 0, {'product_id': prod[0],
                                      'name': str(i),
                                      'plant_id': qty_ava.plant_id.id,
                                      'so_min3': so_min3,
                                      'so_min2': so_min2,
                                      'so_min1': so_min1,
                                      'so_avg': avg_so,
                                      'delivery_min3': delivery_min3,
                                      'delivery_min2': delivery_min2,
                                      'delivery_min1': delivery_min1,
                                      'delivery_avg': avg_del,
                                      'loss_sales': loss_sales,
                                      'so_plus0': so_plus0,
                                      'total_a_b': total_a_b,
                                      'estimation_plus1': estimation_plus1,
                                      'estimation_plus2': estimation_plus2,
                                      'estimation_plus3': estimation_plus3,
                                      'estimation_total': estimation_total,
                                      'wip': wip,
                                      'fg_local': fg_local,
                                      'fg_total': fg_total,
                                      'sisa_plus0': sisa_plus0,
                                      'sisa_plus1': sisa_plus1,
                                      'sisa_plus2': sisa_plus2,
                                      }))
                i+=1
            # 
            x.line_ids  = result

    def export_excel(self, ):
        header_name_a1 = ['PERENCANAAN KEBUTUHAN PELANGGAN'
        ]
        header_name_a2 = [str(self.company_id.name),
        ]
        header_name_a3 = [
                        str(self.period_id.name) +' '+str(self.period_id.tahun),
        ]
        header_name = [
                         'NO',
                         'ST',
                         'STD PL',
                         'PRODUCT',
                         'LOKASI PRODUKSI',
                         'DELIVERY MIN 3',
                         'DELIVERY MIN 2',
                         'DELIVERY MIN 1',
                         'AVERAGE',
                         'SALE ORDER MIN 3',
                         'SALE ORDER MIN 2',
                         'SALE ORDER MIN 1',
                         'AVERAGE',
                         'LOSS SALES',
                         'SALE ORDER PLUS',
                         'TOTAL A + B',
                         'ESTIMASI PLUS 1',
                         'ESTIMASI PLUS 2',
                         'ESTIMASI PLUS 3',
                         'ESTIMASI TOTAL',
                         'WIP',
                         'FG LOCAL',
                         'FG TOTAL',
                         'SISA PLUS 1',
                         'SISA PLUS 3',
                         'SISA PLUS 3',
                         ]

        workbook = xlwt.Workbook()
        for record in self:
            final_data = []
            line_data = []
            worksheet = workbook.add_sheet('Report Forecast final', cell_overwrite_ok=True)
            final_data.append(header_name_a1)
            final_data.append(header_name_a2)
            final_data.append(header_name_a3)
            final_data.append(header_name)
            # pdb.set_trace()
            for z in record.line_ids:
                plan = ""
                if z.plant_id.name:
                  plan = z.plant_id.name
                std_pl = ""
                if z.std_pl:
                  std_pl = z.std_pl
                lt = ""
                if z.lt:
                  lt = z.lt
                line_data = [
                             z.name,
	                         lt,
	                         std_pl,
	                         z.product_id.default_code + '' + z.product_id.name,
	                         plan,
	                         z.delivery_min3,
	                         z.delivery_min2,
	                         z.delivery_min1,
	                         z.delivery_avg,
	                         z.so_min3,
	                         z.so_min1,
	                         z.so_min1,
	                         z.so_avg,
	                         z.loss_sales,
	                         z.so_plus0,
	                         z.total_a_b,
	                         z.estimation_plus1,
	                         z.estimation_plus2,
	                         z.estimation_plus3,
	                         z.estimation_total,
	                         z.wip,
	                         z.fg_local,
	                         z.fg_total,
	                         z.sisa_plus0,
	                         z.sisa_plus1,
	                         z.sisa_plus2,
	                             ]

                final_data.append(line_data)

            for row in range(0, len(final_data)):
                for col in range(0, len(final_data[row])):
                    value = final_data[row][col]
                    worksheet.write(row, col, value)

        output = BytesIO()
        workbook.save(output)
        output.seek(0)
        self.name_ex = "%s%s" % ("Report Forecast final", '.xls')
        self.export_data = base64.b64encode(output.getvalue())
        output.close()

    def posted(self, ):
        for x in self:
            for y in x.line_ids:
                data = {
                            'name'              : x.name,
                            'periode_id'        : x.period_id.id,
                            'product_id'        : y.product_id.id,
                            'qty'               : y.estimation_total,
                          }

                self.env['forecast.forecast_mrp_new'].sudo().create(data)

forecast_final()