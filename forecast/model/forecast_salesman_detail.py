#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class forecast_salesman_detail(models.Model):
    _name = "forecast.forecast_salesman_detail"
    _description = "Forecast Salesman Detail"

    name = fields.Char( required=True, string="Name", help="")
    part_customer = fields.Char( string="Part Customer",  help="")
    delivery_min3 = fields.Float( string="Delivery Min3", digits=(6,0),  help="Total delivery 3 bulan yang lalu")
    delivery_min2 = fields.Float( string="Delivery Min2", digits=(6,0), help="Total delivery 2 bulan yang lalu")
    delivery_min1 = fields.Float( string="Delivery Min1", digits=(6,0), help="Total delivery 1 bulan yang lalu")
    so_min3 = fields.Float( string="SO Min3", digits=(6,0), help="Total SO 3 bulan yang lalu")
    so_min2 = fields.Float( string="SO Min2", digits=(6,0), help="Total SO 2 bulan yang lalu")
    so_min1 = fields.Float( string="SO Min1", digits=(6,0), help="Total SO 1 bulan yang lalu")
    so_min0 = fields.Float( string="Current SO", digits=(6,0), help="Total SO bulan yang berjalan")
    delivery_plus0 = fields.Float( string="SO Plus0", digits=(6,0), help="Rencana kirim bulan ini")
    delivery_plus1 = fields.Float( string="SO Plus1", digits=(6,0), help="Rencana kirim bulan ini plus 1")
    delivery_plus2 = fields.Float( string="SO Plus2", digits=(6,0), help="Rencana kirim bulan ini plus 2")
    loss_sales = fields.Float( string="Loss Sales", digits=(6,0), help="Outstanding SO yang belum bisa dikirim sampai akhir bulan")
    estimation_plus1 = fields.Float( string="Estimation Plus0", digits=(6,0), help="Input Estimasi 1 bulan kedepan")
    estimation_plus2 = fields.Float( string="Estimation Plus1", digits=(6,0), help="Input Estimasi 2 bulan kedepan")
    estimation_plus3 = fields.Float( string="Estimation Plus2", digits=(6,0), help="Input Estimasi 3 bulan kedepan")
    total_finish_goods = fields.Float( string="Total Finish Goods", digits=(6,0), help="Qty Physical Finished Goods")
    wip = fields.Float( string="WIP", digits=(6,0), help="Stock WIP akhir bulan berjalan")
    fg_local = fields.Float( string="FG Local", digits=(6,0), help="Kode barang awal 01")
    fg_import = fields.Float( string="FG Import", digits=(6,0), help="Kode barang awal 02")
    forecast_salesman_id = fields.Many2one(comodel_name="forecast.forecast_salesman",  string="Forecast Salesman",  ondelete="CASCADE")
    product_id = fields.Many2one(comodel_name="product.product",  string="Product",  help="")
    plant_id = fields.Many2one(comodel_name="forecast.plant",  string="Plant",  help="")
    customer_id = fields.Many2one(comodel_name="res.partner",  string="Customer",  help="")
    wip_qty_ids = fields.One2many(comodel_name="forecast.wip_quantity",  inverse_name="forecast_salesman_detail_id",  string="Wip qtys",  help="")
    customer_name = fields.Char('Customer Name')

forecast_salesman_detail()