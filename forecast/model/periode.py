#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class periode(models.Model):

    _name = "forecast.periode"
    name = fields.Char( required=True, string="Name",  help="Bulan")
    bulan = fields.Char( string="Bulan",  help="")
    tahun = fields.Char( string="Tahun",  help="")
    is_active = fields.Boolean( string="Active",  default=True )
    hari_kerja = fields.Integer('Jumlah Hari Kerja', default=30)


