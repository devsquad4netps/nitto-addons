#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class wip_quantity(models.Model):
    """Melihat berapa qty product yang masih dalam proses per masing-masing tahap routing yang terkait terhadap product ini"""

    _name = "forecast.wip_quantity"

    name = fields.Integer( required=True, string="Name",  help="")
    quantity = fields.Float( string="Quantity",  help="")
    forecast_salesman_detail_id = fields.Many2one(comodel_name="forecast.forecast_salesman_detail",  string="Forecast salesman detail",  help="")
    routing_id = fields.Many2one(comodel_name="mrp.workcenter",  string="Routing",  help="")
