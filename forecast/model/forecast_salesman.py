#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import xlwt
from io import BytesIO
from xlrd import open_workbook
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
import pdb

SESSION_STATES =[('draft','Draft'),("validate", "Validate")]

class forecast_salesman(models.Model):
    _name = "forecast.forecast_salesman"
    _description = "Forecast Salesman"
    _inherit = ["mail.thread"]

    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(forecast_salesman, self).unlink()

    name = fields.Char( required=True, string="Name",  help="")
    state = fields.Selection(string="State", 
                                            selection=SESSION_STATES,
                                            required=True,
                                            readonly=True,
                                            default=SESSION_STATES[0][0])
    periode_id = fields.Many2one(comodel_name="forecast.periode",  string="Periode",   domain=[('is_active','=',True)],track_visibility='onchange')
    company_id = fields.Many2one(comodel_name="res.company",  string="Company", default=lambda self: self.env.user.company_id)
    line_ids = fields.One2many(comodel_name="forecast.forecast_salesman_detail",  inverse_name="forecast_salesman_id",  string="Lines",  help="")
    salesman_id = fields.Many2one(comodel_name="res.users",  string="Salesman",  default=lambda self: self.env.user,track_visibility='onchange')
    name_txt_file   = fields.Char('File Name', readonly=True)
    export_data     = fields.Binary("Export File")
    export_data_txt = fields.Binary("Export File")
    name_ex = fields.Char( string="Name Exp",  help="")

    @api.multi
    def action_validate(self):
        self.state = SESSION_STATES[1][0]

    @api.multi
    def action_set_to_draft(self):
        self.state = SESSION_STATES[0][0]

    def get_delivery(self,product,diff,date_sql,awal_dtc,akhir_dtc,stat):
        # sql_d_min = "select sum(qty_done) from stock_move_line mv " \
        #                 "left join uom_uom uom on uom.id = product_uom_id " \
        #                 "left join stock_picking picking on picking.id = picking_id " \
        #                 "where %s " \
        #                 "and %s " \
        #                 "and %s " \
        #                 "and picking_type_code = 'outgoing' " \
        #                 "and mv.state = 'done' "
        # if product == 56:
        #     pdb.set_trace()
        sql_d_min = "select sum(product_uom_qty) from stock_move mv " \
                        "left join uom_uom uom on uom.id = product_uom " \
                        "left join stock_picking_type pick on pick.id = picking_type_id " \
                        "left join stock_picking picking on picking.id = picking_id " \
                        "where mv.state != 'cancel' and %s " \
                        "and %s " \
                        "and %s " \
                        "and pick.code = 'outgoing' " \
                        "and %s "


        product_s = "product_id = %s " % (product)

        awal_date_min_ob = awal_dtc + relativedelta(months=diff)
        awal_date_min = str(awal_date_min_ob.date())
        akhir_date_min_ob = akhir_dtc + relativedelta(months=diff)
        akhir_date_min = str(akhir_date_min_ob.date())

        awal_date_min_res = "%s >= '%s 00:00:00' " % (date_sql,awal_date_min)
        akhir_date_min_res = "%s <= '%s 00:00:00' " % (date_sql,akhir_date_min)
        # pdb.set_trace()
        cr=self.env.cr
        cr.execute(sql_d_min % (product_s, awal_date_min_res,akhir_date_min_res,stat))
        res_d_min = cr.fetchall()
        delivery_min = 0
        if res_d_min[0] == (None,):
            delivery_min = 0
        else:
            delivery_min = res_d_min[0][0]

        return delivery_min

    def get_saleorder(self,product,diff,date_sql,awal_dtc,akhir_dtc,salesman):
        sql_so_min = "select sum(sol.product_uom_qty)," \
                        "case when %s and %s then sum(sol.product_uom_qty) else 0 end as sum1" \
                        "case when %s and %s then sum(sol.product_uom_qty) else 0 end as sum2" \
                        "case when %s and %s then sum(sol.product_uom_qty) else 0 end as sum3" \
                        "sol.order_partner_id from sale_order_line sol " \
                        "left join sale_order so on so.id = sol.order_id " \
                        "left join res_partner rp on rp.id = sol.order_partner_id " \
                        "where %s " \
                        "and %s " \
                        "and so.state in ('sale','done') " \
                        "group by sol.order_partner_id"

        product_s = "product_id = %s " % (product)
        awal_date_min_ob = awal_dtc + relativedelta(months=diff)
        awal_date_min = str(awal_date_min_ob.date())
        akhir_date_min_ob = akhir_dtc + relativedelta(months=diff)
        akhir_date_min = str(akhir_date_min_ob.date())
        self.env['product.product'].browse(product)
        awal_date_min_res = "%s >= '%s 00:00:00' " % (date_sql,awal_date_min)
        akhir_date_min_res = "%s <= '%s 23:59:59' " % (date_sql,akhir_date_min)
        sales_partner = " rp.user_id = %s " % (salesman.id)
        cr=self.env.cr
        cr.execute(sql_so_min % (product_s, awal_date_min_res, akhir_date_min_res, sales_partner))
        res_d_min = cr.fetchall()
        so_min = 0
        #if res_d_min[0] == (None,):
        if not res_d_min:
            so_min = 0
            partner_id = False
        else:
            import pdb;pdb.set_trace()
            so_min = res_d_min[0][0]
            partner_id = res_d_min[0][1]

        return so_min,partner_id

    @api.multi
    def fill_product(self):
        cr=self.env.cr
        for x in self:
            sql1 = "delete from forecast_forecast_salesman_detail where forecast_salesman_id = %s" % ( self.id)       
            cr.execute(sql1)
            sql = "select product_id from product_customer_code prod_code " \
                  "left join res_partner parts on parts.id = prod_code.partner_id " \
                  "where parts.user_id = %s" % (x.salesman_id.id)
            cr.execute(sql)
            res = cr.fetchall()
            # pdb.set_trace()
            # codes = []
            # for cd in res:
            #     codes.append(cd)

            # sql = "select prod.id from product_product prod " \
            #       "left join product_template temp on temp.id = prod.product_tmpl_id " \
            #       "left join product_category categ on categ.id = temp.categ_id " \
            #       "where prod.default_code notnull and categ.name = 'Finish Good' " \
            #       "and prod.id in %s" % ( res)

            # cr=self.env.cr
            # cr.execute(sql)
            # res = cr.fetchall()
            result = []
            i = 1
            for prod in res:
                sql2 = "select move_id from stock_move_line where state != 'cancel' and product_id = %s" % ( prod[0])
                cr.execute(sql2)
                res1 = cr.fetchall()

                # move_ids = []
                # move_ids_int = []
                # if res1 and res1[0]!= None:
                #     for move in res1:
                #         move_ids.append(str(move[0]))
                #         move_ids_int.append(move[0])
                # move_string = ""
                # if move_ids:
                #     move_string = ','.join(x for x in move_ids)

                # date_min3_ob = dtc
                day = ''
                if str(x.periode_id.bulan) == '01' or str(x.periode_id.bulan) == '1':
                    day = '31'
                elif str(x.periode_id.bulan) == '02' or str(x.periode_id.bulan) == '2':
                    day = '28'
                elif str(x.periode_id.bulan) == '03' or str(x.periode_id.bulan) == '3':
                    day = '31'
                elif str(x.periode_id.bulan) == '04' or str(x.periode_id.bulan) == '4':
                    day = '30'
                elif str(x.periode_id.bulan) == '05' or str(x.periode_id.bulan) == '5':
                    day = '31'
                elif str(x.periode_id.bulan) == '06' or str(x.periode_id.bulan) == '6':
                    day = '30'
                elif str(x.periode_id.bulan) == '07' or str(x.periode_id.bulan) == '7':
                    day = '31'
                elif str(x.periode_id.bulan) == '08' or str(x.periode_id.bulan) == '8':
                    day = '31'
                elif str(x.periode_id.bulan) == '09' or str(x.periode_id.bulan) == '9':
                    day = '30'
                elif str(x.periode_id.bulan) == '10':
                    day = '31'
                elif str(x.periode_id.bulan) == '11':
                    day = '30'
                elif str(x.periode_id.bulan) == '12':
                    day = '31'

                #date awal
                awal_dt = str(x.periode_id.tahun) + '-' + str(x.periode_id.bulan) +'-01 00:00:00'
                awal_dtc = datetime.strptime(awal_dt, "%Y-%m-%d 00:00:00")
                #date awal
                #date akhir
                akhir_dt = str(x.periode_id.tahun) + '-' + str(x.periode_id.bulan) +'-' + day + ' 00:00:00'
                akhir_dtc = datetime.strptime(akhir_dt, "%Y-%m-%d 00:00:00")
                #date akhir

                #sql deliver min

                #sql deliver min

                del_min = 'picking.date_done'
                del_plus = 'picking.scheduled_date'
                stat = "mv.state = 'done' "
                # akhir_dt = str(x.periode_id.tahun) + '-' + str(x.periode_id.bulan) +'-' + '25' + ' 00:00:00'
                # akhir_dtc = datetime.strptime(akhir_dt, "%Y-%m-%d 00:00:00")
                delivery_min3 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_min,diff=-3,stat=stat)
                delivery_min2 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_min,diff=-2,stat=stat)
                delivery_min1 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_min,diff=-1,stat=stat)
                # akhir_dt = str(x.periode_id.tahun) + '-' + str(x.periode_id.bulan) +'-' + '23' + ' 00:00:00'
                # akhir_dtc = datetime.strptime(akhir_dt, "%Y-%m-%d 00:00:00")
                stat = "mv.state = 'assigned' "
                # if prod[0] == 56:
                #     pdb.set_trace()
                delivery_plus0 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_plus,diff=0,stat=stat)
                delivery_plus1 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_plus,diff=1,stat=stat)
                delivery_plus2 = self.get_delivery(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=del_plus,diff=2,stat=stat)

                #sql deliver min
                #sql so min
                so_min = 'sol.delivery_date'
                so_min3 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=-3,salesman=x.salesman_id)
                so_min2 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=-2,salesman=x.salesman_id)
                so_min1 = self.get_saleorder(product=prod[0],awal_dtc=awal_dtc,akhir_dtc=akhir_dtc,date_sql=so_min,diff=-1,salesman=x.salesman_id)
                #sql dso min

                sql_wo = "select workcenter_id, sum(qty_producing) as qty from mrp_workorder " \
                        "where product_id = %s " \
                        "and state = 'progress' " \
                        "group by workcenter_id"

                cr.execute(sql_wo % (prod[0]))
                res_wo = cr.fetchall()
                wo_obj = []
                if res_wo and res_wo[0]!= None:
                    ind = 1
                    for wo in res_wo:
                        wo_obj.append((0, 0, {'name': str(ind),
                                              'routing_id': wo[0],
                                              'quantity': wo[1],
                                      }))
                        ind+=1


                loss_sales = (so_min3[0] + so_min2[0]  + so_min1[0] ) - (delivery_min3 + delivery_min2 + delivery_min1)
                # if prod[0] == 56:
                #     pdb.set_trace()
                qty_ava = self.env['product.product'].browse(prod[0])
                #part_cust = qty_ava.product_customer_code_ids.filtered(lambda p:p.partner_id == )
                result.append((0, 0, {'product_id': prod[0],
                                      'name': str(i),
                                      'part_customer': '',
                                      'delivery_min3': delivery_min3,
                                      'delivery_min2': delivery_min2,
                                      'delivery_min1': delivery_min1,
                                      'so_min3': so_min3[0] ,
                                      'so_min2': so_min2[0] ,
                                      'so_min1': so_min1[0] ,
                                      'delivery_plus0': delivery_plus0,
                                      'delivery_plus1': delivery_plus1,
                                      'delivery_plus2': delivery_plus2,
                                      'loss_sales': loss_sales,
                                      'total_finish_goods': qty_ava.qty_available,
                                      'plant_id': qty_ava.plant_id.id,
                                      'wip_qty_ids': wo_obj,
                                      }))
                i+=1
            x.line_ids  = result
            #x.action_validate()

    def export_excel(self, ):
        # pass
        header_name_a1 = ['PERENCANAAN KEBUTUHAN PELANGGAN'
        ]
        header_name_a2 = [str(self.company_id.name),
        ]
        header_name_a3 = [
                        str(self.periode_id.name) +' '+str(self.periode_id.tahun),
        ]
        header_name = [
                         'NO',
                         'PRODUCT',
                         'LOKASI PRODUKSI',
                         'PART CUSTOMER',
                         'DELIVERY MIN 3',
                         'DELIVERY MIN 2',
                         'DELIVERY MIN 1',
                         'SALE ORDER MIN 3',
                         'SALE ORDER MIN 2',
                         'SALE ORDER MIN 1',
                         'DELIVERY PLUS 0',
                         'DELIVERY PLUS 1',
                         'DELIVERY PLUS 2',
                         'LOSS SALES',
                         'ESTIMASI PLUS 1',
                         'ESTIMASI PLUS 2',
                         'ESTIMASI PLUS 3',
                         'TOTAL FINISH GOOD',
                         'CUSTOMER',
                         ]
        obj = self.env['mrp.workcenter'].search([])
        for x in obj:
            header_name.append(x.name)

        workbook = xlwt.Workbook()
        for record in self:
            final_data = []
            line_data = []
            worksheet = workbook.add_sheet('Report Forecast', cell_overwrite_ok=True)
            final_data.append(header_name_a1)
            final_data.append(header_name_a2)
            final_data.append(header_name_a3)
            final_data.append(header_name)
            # pdb.set_trace()
            for z in record.line_ids:
                plan = ""
                if z.plant_id.name:
                  plan = z.plant_id.name
                pcus = ""
                if z.part_customer:
                  pcus = z.part_customer
                cus = ""
                if z.customer_id.name:
                  cus = z.customer_id.name
                line_data = [
                             z.name,
                             z.product_id.default_code + '' + z.product_id.name,
                             plan,
                             pcus,
                             z.delivery_min3,
                             z.delivery_min2,
                             z.delivery_min1,
                             z.so_min3,
                             z.so_min2,
                             z.so_min1,
                             z.delivery_plus0,
                             z.delivery_plus1,
                             z.delivery_plus2,
                             z.loss_sales,
                             z.estimation_plus1,
                             z.estimation_plus2,
                             z.estimation_plus3,
                             z.total_finish_goods,
                             cus,
                             ]
                for y in z.wip_qty_ids:
                    line_data.append(y.quantity)

                final_data.append(line_data)

            for row in range(0, len(final_data)):
                for col in range(0, len(final_data[row])):
                    value = final_data[row][col]
                    worksheet.write(row, col, value)

        output = BytesIO()
        workbook.save(output)
        output.seek(0)
        self.name_ex = "%s%s" % ("Report Forecast", '.xls')
        self.export_data = base64.b64encode(output.getvalue())
        output.close()

forecast_salesman()