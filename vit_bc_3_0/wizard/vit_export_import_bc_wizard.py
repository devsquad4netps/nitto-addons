from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning

class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"

    @api.multi
    def get_datas(self):
        res = super(VitExportImportBcWizard, self).get_datas()
        if self.bc_id.name == 'BC 3.0' :
            raise Warning('Tidak ada fitur export - import untuk BC 3.0')
        return res

VitExportImportBcWizard()