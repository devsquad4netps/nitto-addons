from odoo import fields, models, api, _
from odoo.exceptions import Warning

class VitExportImportBc(models.Model):
    _inherit = 'vit.export.import.bc'

    name = fields.Selection(selection_add=[('BC 3.0','BC 3.0')])

VitExportImportBc()