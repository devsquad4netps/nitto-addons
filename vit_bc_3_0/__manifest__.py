{
    "name"          : "BC 3.0",
    "version"       : "0.2",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Bea Cukai",
    "license"       : "LGPL-3",
    "contributors"  : """
        - Widiana Juniar
    """,
    "summary"       : "Export Import BC 3.0",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "stock",
        "sale",
        "purchase",
        "vit_export_import_bc",
        "vit_kanban",
    ],
    "data"          : [
        "data/vit_export_import_bc.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}