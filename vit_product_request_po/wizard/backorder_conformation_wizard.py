# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare


class StockBackorderConfirmation(models.TransientModel):
    _inherit = 'stock.backorder.confirmation'

    # def _get_default_from(self):
    #     #import pdb;pdb.set_trace()
    #     context = self.env.context
    #     if 'params' in context :
    #         if context['params'].get('model') == 'stock.picking':
    #             if context['params'].get('id', False) :
    #                 picking_id = self.env['stock.picking'].browse(context['params'].get('id'))
    #                 if picking_id.location_id.usage == 'production' :
    #                     # jika dr produksi langsung True
    #                     return True
    #     return False

    # from_production = fields.Boolean('From Production', default=_get_default_from)
    usage = fields.Selection(related='pick_ids.location_dest_id.usage',string='Usage From',store=True)
    usage_dest = fields.Selection(related='pick_ids.location_dest_id.usage',string='Usage To',store=True)


    def process_auto_backorder(self):
        for pick in self.pick_ids :
            if pick.location_id.usage == 'production' :
                self._process(cancel_backorder=True)
            elif pick.location_dest_id.usage == 'production' :
                self._process(cancel_backorder=True)
            else :
                self._process()

StockBackorderConfirmation()