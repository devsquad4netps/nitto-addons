# -*- coding: utf-8 -*-
from odoo import models,fields, api, _
from odoo.exceptions import UserError


class ProductRequestLineWizardPO(models.TransientModel):
    _name = "vit.product.request.line.wizard.po"
    _description = "Confirm the selected PR lines"

    @api.multi
    @api.onchange('partner_id')
    def partner_id_change(self):
        if self.partner_id and self.partner_id.property_purchase_currency_id :
            wh = self.env['stock.warehouse']
            if self.partner_id.property_purchase_currency_id.name == 'IDR':
                wh_exist = wh.sudo().search([('company_id','=',self.company_id.id),('name','not like','Transit')],limit=1)
                if wh_exist :
                    self.warehouse_id = wh_exist.id
            else :
                wh_exist = wh.sudo().search([('company_id','=',self.company_id.id),('name','like','Transit')],limit=1)
                if wh_exist :
                    self.warehouse_id = wh_exist.id

    partner_id = fields.Many2one("res.partner","Supplier", domain="[('supplier','=',True)]")
    warehouse_id = fields.Many2one("stock.warehouse","Warehouse")
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
        readonly=True,
        default=lambda self: self.env.user.company_id.id)


    @api.multi
    def create_po(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['vit.product.request.line'].browse(active_ids):
            if record.state != 'open':
                raise UserError(_("Selected PR lines cannot be confirmed as they are not in 'Open' state."))
            record.action_create_po(self.partner_id,self.warehouse_id,active_ids)
        return {'type': 'ir.actions.act_window_close'}

ProductRequestLineWizardPO()