{
    'name' : 'Product Request to Purchase Order',
    'version' : '0.4',
    'category': 'Purchase Management',
    'images' : [],
    'depends' : ['stock',
                 'account',
                 'purchase',
                 'purchase_stock',
                 'purchase_requisition',
                 'hr',
                 'vit_product_request'],
    'author' : 'vitraining.com',
    "contributors"  : """
        - widianajuniar@gmail.com
    """,
    'description': """
User Purchase Request
==========================================
* Langsung create PO dari Product Request Line
* Auto backorder jika ada sisa barang masuk selain dari produksi
* Auto No backorder jika barang masuk dari produksi
    """,
    'website': 'http://www.vitraining.com',
    'data': [
        "wizard/no_backorder.xml",
        "wizard/product_request_line_wizard.xml",
        "views/product_request.xml",
        "views/purchase_order.xml",
    ],
    'test': [
    ],
    'demo': [],
    'installable': True,
    'auto_install': False
}