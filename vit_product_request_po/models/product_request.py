from odoo import tools
from odoo import fields,models,api
import time
import logging
from odoo.tools.translate import _
from collections import defaultdict
from odoo.exceptions import UserError, ValidationError, Warning

_logger = logging.getLogger(__name__)
PR_STATES =[('draft','Draft'),
    ('open','Confirmed'), 
    ('onprogress','On Progress'), 
    ('done','Done'),
    ('reject','Rejected')]
PR_LINE_STATES =[('draft','Draft'),
    ('open','Confirmed'), 
    ('onprogress','On Progress'), # Call for Bids in progress
    ('done','Done'),# Call for Bids = PO created / done
    ('reject','Rejected')]


class ProductRequest(models.Model):
    _inherit           = "vit.product.request"


    # create po sekaligus dari product request
    def action_create_po(self):
        cr = self.env.cr
        purchase_order          = self.env['purchase.order']
        purchase_order_line      = self.env['purchase.order.line']

        for prd_req in self:
            if not prd_req.partner_id :
                raise UserError(_('Supplier harus diisi !'))
            po_line_ids = []
            for lines in prd_req.product_request_line_ids:
                new_price = lines.unit_price
                price_list = line.product_id.seller_ids.filtered(lambda x:x.partner_id.id == prd_req.partner_id.id)
                for price in price_list:
                    if not price.date_end :
                        if lines.product_qty <= price.min_qty :
                            new_price = price.price
                            break
                    elif price.date_end and price.date_end <= lines.date_required :
                        if lines.product_qty <= price.min_qty :
                            new_price = price.price
                            break
                po_line_ids.append( (0,0,{
                    'product_id'     : lines.product_id.id,
                    'name'           : lines.name,
                    'product_qty'    : lines.product_qty,
                    'product_uom'    : lines.product_uom_id.id,
                    'price_unit'     : new_price,
                    'date_planned'   : lines.date_required,
                }) )
            po_id = purchase_order.create({
                'name'               : 'New',
                'picking_type_id'    : self.warehouse_id.in_type_id.id ,
                'order_line'         : po_line_ids,
                'partner_id'         : prd_req.partner_id.id,
                'origin'             : prd_req.name,
                'currency_id'        : prd_req.partner_id.property_purchase_currency_id.id,
                'date_planned'       : prd_req.date_required,
            })
            #update state dan pr_id di line product request asli
            cr.execute("update vit_product_request_line set state=%s, purchase_order_id=%s where product_request_id = %s",
             ( 'onprogress', po_id.id,  prd_req.id  ))

            self.write({'state':'onprogress'}, )

            body = _("Purchase Order Created")
            self.send_followers()

            return po_id

ProductRequest()


class ProductRequestLine(models.Model):
    _inherit         = "vit.product.request.line"


    def action_create_po(self,partner_id,warehouse,active_ids):
        
        state_not_open = self.filtered(lambda x: x.state != 'open')
        if state_not_open :
            raise UserError(_('Status product request line harus open (confirmed) !'))
        cr = self.env.cr
        ##########################################################
        # id line product_request_line yang diselect
        ##########################################################
        context=self.env.context
        active_ids = context and context.get('active_ids', False)

        ##########################################################
        # untuk setiap partner , create PO dari line PR
        ##########################################################
        prs = {}
        i=0

        sql = "select product_id, product_uom_id, department_id, warehouse_id, name, product_request_id, id, date_required, " \
              "sum(product_qty) as product_qty, avg(unit_price) as unit_price " \
              "from vit_product_request_line " \
              "where state = 'open' and purchase_requisition_id is null " \
              "and id in %s " \
              "group by product_id, product_uom_id, department_id, warehouse_id, name, product_request_id, id, date_required " \
              "order by product_id, product_uom_id, department_id, warehouse_id, name, product_request_id, id, date_required "
        cr.execute(sql, ( tuple(active_ids),))
        res = cr.dictfetchall()

        for r in res:
            product_id = r['product_id']
            department_id = r['department_id']
            product_qty = r['product_qty']
            warehouse_id = r['warehouse_id']
            if product_id in prs:
                prs[product_id]['product_qty'] += product_qty
                prs[product_id]['origins'].append( r['product_request_id'])
                prs[product_id]['order_line'].append( r['id'] )
            else:
                prs[ product_id ] = {
                    'product_qty'    : product_qty ,
                    'product_uom'    : r['product_uom_id'],
                    'date_planned'   : r['date_required'] ,
                    'origins'        : [r['product_request_id']],
                    'order_line'     : [r['id']],
                    'name'           : r['name'],
                    'price_unit'     : r['unit_price']
                }


            """
            qty_per_dept = { 
                dept_id1 : { qty: 10, request_id: 20, warehouse_id: 2} ,
                dept_id2 : { qty:  3, request_id: 21, warehouse_id: 3}
            }
            """
            if 'qty_per_dept' in prs[product_id]:
                if department_id in prs[product_id]['qty_per_dept']:
                    prs[product_id]['qty_per_dept'][department_id]['qty'] += product_qty
                    [prs[product_id]['qty_per_dept'][department_id]['request_id']].append(r['product_request_id'])
                    [prs[product_id]['qty_per_dept'][department_id]['warehouse_id']].append(r['warehouse_id'])
                else:
                    prs[product_id]['qty_per_dept'][department_id]= {'qty' : product_qty,
                                                                     'request_id': r['product_request_id'],
                                                                     'warehouse_id': r['warehouse_id']
                                                                     }

            else:
                prs[product_id]['qty_per_dept']= {department_id: {'qty':product_qty,
                                                                  'request_id': r['product_request_id'],
                                                                  'warehouse_id': r['warehouse_id']
                                                                  }}

        ##########################################################
        #create PO
        ##########################################################
        if prs :
            i = 0
            order_line = []
            for product_id in prs.keys():
                order_line += self.create_po_lines(product_id, prs[product_id])

                i = i + 1

            request = prs[product_id]
            po_id = self.create_po(request,partner_id,warehouse,order_line,active_ids)
            cr.commit()
            return {
                'type': 'ir.actions.act_window.message',
                'title': _('OK'),
                'message': 'Done creating %s RfQ ' % (i),
                'close_button_title': 'Make this window go away',
                'is_html_message': True,
                'buttons': [
                    # a button can be any action (also ir.actions.report.xml et al)
                    {
                        'type': 'ir.actions.act_window',
                        'name': 'All customers',
                        'res_model': 'res.partner',
                        'view_mode': 'form',
                        'views': [[False, 'list'], [False, 'form']],
                        'domain': [('customer', '=', True)],
                    },
                    # or if type == method, you need to pass a model, a method name and
                    # parameters
                    {
                        'type': 'method',
                        'name': _('Yes, do it'),
                        'model': self._name,
                        'method': 'myfunction',
                        # list of arguments to pass positionally
                        'args': [self.ids],
                        # dictionary of keyword arguments
                        'kwargs': {'force': True},
                        # button style
                        'classes': 'btn-primary',
                    }
                ]
            }


    def create_po_lines(self, product_id, request_line=None):
        line_department_ids = []
        for x in request_line['qty_per_dept'].keys():
            line_department_ids.append( (0,0,{
                'qty':request_line['qty_per_dept'][x]['qty'],
                'request_id':request_line['qty_per_dept'][x]['request_id'],
                'warehouse_id':request_line['qty_per_dept'][x]['warehouse_id'],
                'department_id': x}) )

        po_line_ids = [(0,0,{
            'product_id': product_id,
            'product_qty': request_line['product_qty'],
            'price_unit': request_line['price_unit'],
            'product_uom': request_line['product_uom'],
            'date_planned': request_line['date_planned'],
            'name': request_line['name'],
            'line_department_ids': line_department_ids
        })]

        return po_line_ids


    def create_po(self,request_line=None,partner_id=None,warehouse=None,order_line=None,active_ids=None):
        cr=self.env.cr
        purchase_order  = self.env['purchase.order']
        product_request = self.env['vit.product.request.line']
        origins = product_request.browse(active_ids)
        po_id = purchase_order.create({
            'name'               : 'New',
            'picking_type_id'    : warehouse.in_type_id.id ,
            'order_line'         : order_line,
            'partner_id'         : partner_id.id,
            'currency_id'        : partner_id.property_purchase_currency_id.id,
            'origin'             : ",".join( origins.mapped('product_request_id.name') )
        })

        for lines in po_id.order_line:
            #price_list = lines.product_id.seller_ids.filtered(lambda x:x.name.id == po_id.partner_id.id and x.currency_id.id == po_id.currency_id.id)
            for price in lines.product_id.seller_ids.filtered(lambda x:x.name.id == partner_id.id and x.currency_id.id == partner_id.property_purchase_currency_id.id):                
                if not price.date_end :
                    if lines.product_qty >= price.min_qty :
                        lines.price_unit = price.price
                        break
                elif price.date_end and price.date_end <= po_id.date_required :
                    if lines.product_qty >= price.min_qty :
                        lines.price_unit = price.price
                        break
        #update state dan pr_id di line product request asli
        cr.execute("update vit_product_request_line set state=%s, purchase_order_id=%s where id in %s",
         ( 'onprogress', po_id.id, tuple(active_ids)))

        return po_id

    def cancel_pr_line(self):
        self.ensure_one()
        for line in self :
            line.write({'state':'reject'})
            if not line.product_request_id.product_request_line_ids.filtered(lambda x:x.state != 'reject'):
                line.product_request_id.write({'state':'reject'})
        return True


ProductRequestLine()


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.multi
    def button_approve(self, force=False):
        res = super(PurchaseOrder,self).button_approve(force=force)
        pr_line = self.env['vit.product.request.line']
        lines = pr_line.sudo().search([('purchase_order_id','=',self.id)])
        if lines :
            for line in lines  :
                if line.state == 'onprogress' :
                    line.write({'state' : 'done'})
                    if line.product_request_id.product_request_line_ids.filtered(lambda x:x.state != 'onprogress'):
                        line.product_request_id.action_done()
        return res

    def unlink(self=None):
        for po in self :
            if not po.requisition_id :
                cr = self.env.cr
                cr.execute("update vit_product_request_line set state=%s, purchase_order_id=null where purchase_order_id = %s ", ('open', po.id)  )
        return super(PurchaseOrder, self).unlink()

PurchaseOrder()


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    def unlink(self=None):
        for po_line in self :
            if not po_line.order_id.requisition_id :
                cr = self.env.cr
                cr.execute("update vit_product_request_line set state=%s, purchase_order_id=null where product_id = %s and purchase_order_id = %s ", ('open', po_line.product_id.id,po_line.order_id.id)  )
        return super(PurchaseOrderLine, self).unlink()

PurchaseOrderLine()