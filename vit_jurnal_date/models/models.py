# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class AccountMoveDate(models.Model):
	_inherit ='account.move'


	@api.model
	def create(self, vals):
		move = super(AccountMoveDate, self.with_context(check_move_validity=False, partner_id=vals.get('partner_id'))).create(vals)
		move.assert_balanced()
		move._create_validate()
		return move

	@api.multi
	def _create_validate(self):
		obj_picking = self.env['stock.picking'].search([('name','=',self.ref),('acc_periode','!=',False)],limit=1)
		if self.ref == obj_picking.name:
			obj_move = self.env['stock.move'].search([('picking_id','=',obj_picking.id)],limit=1)
			if obj_move and obj_move.kanban_id:
				if obj_move.kanban_id.date :
					self.write({'date' : obj_move.kanban_id.date, 'ref' : obj_move.kanban_id.name + ' | ' + self.ref}) 
					self.line_ids.write({'date' : obj_move.kanban_id.date})
			else :
				if obj_picking.acc_periode :
					self.write({'date' : obj_picking.acc_periode}) 
					self.line_ids.write({'date' : obj_picking.acc_periode})

AccountMoveDate()


class StockPicking(models.Model):
	_inherit ='stock.picking'

	acc_periode = fields.Date('Accounting Periode', copy=False, track_visibility='onchange')

StockPicking()


class VitKanban(models.Model):
	_inherit ='vit.kanban'

	@api.multi
	def action_validate(self):
		for detail in self.detail_ids :
			if detail.picking_id :
				detail.picking_id.write({'acc_periode':self.date})
		return super(VitKanban, self).action_validate()

	@api.multi
	def action_confirm(self):
		for detail in self.detail_ids :
			if detail.picking_id :
				detail.picking_id.write({'acc_periode':self.date})
		return super(VitKanban, self).action_confirm()

	@api.multi
	def action_cancel(self):
		for detail in self.detail_ids :
			if detail.picking_id :
				detail.picking_id.write({'acc_periode':False})
		return super(VitKanban, self).action_cancel()

VitKanban()