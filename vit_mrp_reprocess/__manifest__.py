{
	"name": "MRP re-Process",
	"version": "0.3", 
	"depends": [
		"mrp",

	],
	"author": "http://www.vitraining.com", 
	"category": "Tool", 
	'website': 'http://www.vitraining.com',
	"description": """\

*** Reprocess MRP

""",
	"data": [
		"views/mrp.xml",
		"views/stock.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}