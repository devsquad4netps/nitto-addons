from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError,Warning
from odoo.tools import float_compare, float_round
from odoo.addons import decimal_precision as dp


class MrpProduction(models.Model):
    _inherit ='mrp.production'

    reprocess            = fields.Boolean('Re-Process', copy=False, track_visibility='on_change')
    reprocess_mo_id = fields.Many2one('mrp.production','MO Re-Process', copy=False, track_visibility='on_change')
    reprocess_picking_id = fields.Many2one('stock.picking','Picking Re-Process', copy=False, track_visibility='on_change')
    wire_id = fields.Many2one('product.product','Wire', compute="_get_wire", store=True)

    @api.depends("reprocess_mo_id","reprocess_picking_id")
    def _get_wire(self):
        for rec in self:
            if rec.reprocess_mo_id and rec.reprocess_mo_id.bom_id and rec.reprocess_mo_id.bom_id.bom_line_ids:
                wire = rec.reprocess_mo_id.bom_id.bom_line_ids.filtered(lambda i:i.product_id.categ_id.name in ('Wire','WIRE','wire'))
                if wire :
                	rec.wire_id = wire[0].product_id.id
MrpProduction()