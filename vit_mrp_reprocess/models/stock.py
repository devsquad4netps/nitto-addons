# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class StockPickingType(models.Model):
	_inherit = 'stock.picking.type'

	re_process = fields.Boolean(string='Re-Process',help='Jika di centang maka field origin di dokumen picking akan menjadi mandatory')

class StockPicking(models.Model):
	_inherit = 'stock.picking'

	re_process = fields.Boolean(string='Re-Process', related="picking_type_id.re_process", store=True)