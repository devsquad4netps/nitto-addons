# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2017 vitraining
# based on Deltatech addons
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import odoo.addons.decimal_precision as dp
from odoo.http import request

from odoo import api, models, fields, http
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import Warning

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    amount = fields.Float(
        string='Production Amount',
        compute='_calculate_amount',
        help="Total cost of production",
    )
    calculate_price = fields.Float(
        string='Calculate Price',
        compute='_calculate_amount',
        help="Cost of Production per unit finished goods",
        digits=(16, 10),
    )

    @api.model
    def create(self, vals):
        record = super(MrpProduction, self).create(vals)
        if record.bom_id and record.bom_id.company_id != record.company_id :
            bom = self.env['mrp.bom']
            bom_company = bom.sudo().search([('product_tmpl_id','=',record.product_id.product_tmpl_id.id),('company_id','=',record.company_id.id)], limit=1)
            if bom_company :
                record.bom_id = bom_company.id
        if not self.env['stock.picking'].sudo().search([('origin','=',record.name)]):
            record.assign_picking()
        return record

    @api.one
    def _calculate_amount(self):
        production = self
        calculate_price = 0.0
        amount = 0.0
        for move in production.move_raw_ids:
            t_price = abs(move.price_unit*move.quantity_done)
            amount += t_price
        if amount > 0.0 :
            move_finished = production.finished_move_line_ids.filtered(lambda i:i.state == 'done')
            if move_finished :
                total_finish = sum(move_finished.mapped('qty_done'))
            else :
                total_finish = production.product_qty
            production.calculate_price = amount/total_finish
        production.amount = amount


    def _cal_price(self, consumed_moves):
        super(MrpProduction, self)._cal_price(consumed_moves)
        self.ensure_one()
        production = self
        production.assign_picking()
        # if production.product_id.cost_method == 'standard':
        # # if production.product_id.cost_method == 'standard' and production.product_id.standard_price != production.calculate_price:
        #     product_cost = production.product_id.standard_price
        #     if product_cost > 0.0 :
        #         production.product_id.write({'standard_price': (product_cost+production.calculate_price)/2})
        #     else :
        #         production.product_id.write({'standard_price': production.calculate_price}) 
        return True

    @api.multi
    def _generate_moves(self):
        res = super(MrpProduction, self)._generate_moves()
        self.assign_picking()
        return res

    @api.multi
    def assign_picking(self):
        """
        All consumed products will merge into a picking list
        """
        # pdb.set_trace()
        for production in self:
            move_list = self.env['stock.move']
            pick_type = self.env['stock.picking.type']
            picking = False
            for move in production.move_raw_ids:
                if not move.picking_id:
                    move_list += move
                else:
                    picking = move.picking_id
            if move_list:
                picking_type = pick_type.sudo().search([('name','ilike','Consume'),
                                                    ('code','=','internal'),
                                                    ('warehouse_id.company_id','=',production.company_id.id)],limit=1)
                if not picking_type:
                    picking_type = self.env.ref('vit_mrp_cost.picking_type_consume', raise_if_not_found=True)

                if picking_type:
                    if not picking:
                        picking = self.env['stock.picking'].create({'picking_type_id': picking_type.id,
                                                                    'date': production.date_planned_start,
                                                                    'location_id':picking_type.default_location_src_id.id,
                                                                    'location_dest_id':picking_type.default_location_dest_id.id,
                                                                    'origin': production.name,
                                                                    'company_id': production.company_id.id})
                    #move_list.write({'picking_id': picking.id})
                    # write juga loc dest dan source karena sering balik lagi ke dafault odoo
                    move_list.write({'picking_id': picking.id,
                                    'location_id':picking_type.default_location_src_id.id,
                                    'location_dest_id':picking_type.default_location_dest_id.id,
                                    'company_id': production.company_id.id})
                    # picking.get_account_move_lines()  # From location
                    # picking.action_confirm()
                    # picking.action_assign()

            move_list = self.env['stock.move']
            picking = False
            for move in production.move_finished_ids:
                if not move.picking_id:
                    move_list += move
                else:
                    picking = move.picking_id
            if move_list:
                picking_type = pick_type.sudo().search([('name','ilike','Receipt from Production'),
                                                    ('code','=','internal'),
                                                    ('warehouse_id.company_id','=',production.company_id.id)],limit=1)
                if not picking_type :
                    picking_type = self.env.ref('vit_mrp_cost.picking_type_receipt_production', raise_if_not_found=True)

                if picking_type:
                    if not picking:
                        picking = self.env['stock.picking'].create({'picking_type_id': picking_type.id,
                                                                    'date': production.date_planned_start,
                                                                    'location_id':picking_type.default_location_src_id.id,
                                                                    'location_dest_id':picking_type.default_location_dest_id.id,
                                                                    'origin': production.name,
                                                                    'company_id': production.company_id.id})
                    # write juga loc dest dan source karena sering balik lagi ke dafault odoo
                    move_list.write({'picking_id': picking.id,
                                    'location_id':picking_type.default_location_src_id.id,
                                    'location_dest_id':picking_type.default_location_dest_id.id,
                                    'company_id': production.company_id.id})
        return True


    @api.multi
    def auto_assign_picking(self):
        to_exec = self.search([('state','not in',('done','cancel'))])
        for mrp in to_exec :
            mrp_picking = self.env['stock.picking'].search([
                ('group_id', '=', mrp.procurement_group_id.id),
            ])
            if not mrp_picking :
                try :
                    mrp.assign_picking()
                    self._cr.commit()
                    _logger.info("Run scheduler ========================> MO %s Created Picking "%str(mrp.name))
                except:
                    continue
        return True

    @api.multi
    def auto_mark_as_done(self):
        to_exec = self.search([('state','=','progress')])
        for mrp in to_exec :
            finnish = mrp.move_finished_ids.filtered(lambda fn:fn.state not in ('cancel','done'))
            rawmate = mrp.move_raw_ids.filtered(lambda fn:fn.state not in ('cancel','done'))
            if not finnish and not rawmate :
                try :
                    mrp.button_mark_done()
                    self._cr.commit()
                    _logger.info("Run scheduler ========================> MO %s Mark as Done "%str(mrp.name))
                except:
                    continue
        return True

MrpProduction()


class StockMove(models.Model):
    _inherit = 'stock.move'

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        if self.production_id:
            # if cost == 0.0 :
            #     cost = self.production_id.amount
            # if self.production_id.product_id.cost_method == 'standard' and self.production_id.product_id.standard_price != self.production_id.calculate_price:
            #     self.production_id.product_id.write({'standard_price': self.production_id.calculate_price})
            cost = self.production_id.amount
            if self.production_id.product_id.cost_method == 'standard':
                product_cost = self.production_id.product_id.standard_price
                if product_cost > 0.0 :
                    self.production_id.product_id.write({'standard_price': (product_cost+self.production_id.calculate_price)/2})
                else :
                    self.production_id.product_id.write({'standard_price': self.production_id.calculate_price}) 
            return super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)

        return super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)

StockMove()



class workorder(models.Model):
    _name = 'mrp.workorder'
    _inherit = 'mrp.workorder'


    @api.multi
    def record_production(self):
        res = super(workorder, self).record_production()

        cr = self.env.cr

        ######## picking_id
        sql ="""select sp.id from stock_picking sp 
            join stock_picking_type spt on sp.picking_type_id=spt.id
            where origin = %s 
            and spt.name like %s"""
        cr.execute(sql, ( self.production_id.name, 'Receipt from Production'))
        picking_id = cr.fetchone()

        if picking_id :
            ######## move_id
            sql = """select id from stock_move where picking_id=%s"""
            cr.execute(sql, (picking_id[0],))
            move_id = cr.fetchone()

            if move_id :
                ######## update stock_move_line
                sql = """update stock_move_line 
                set picking_id = %s
                where move_id = %s
                """
                cr.execute(sql, (picking_id[0], move_id[0]))

                ######## update stock_move
                sql = """update stock_move set state=%s where id = %s"""
                cr.execute(sql, ('assigned', move_id[0]))

            ######## update stock_picking
            sql ="""update stock_picking set state = %s where id=%s """
            cr.execute(sql, ('assigned', picking_id[0] ))

        return res


class MrpReceiptFromProduction(models.TransientModel):
    _name = 'mrp.receipt.from.production'

    name = fields.Char('Number',size=250, required=True)
    picking_id = fields.Many2one('stock.picking','Picking', required=True)
    warning = fields.Text('Warning')

    @api.multi
    @api.onchange('name')
    def _onchange_name(self):
        for rec in self:
            if rec.name :
                new_name = rec.name.split(';')
                if len(new_name) >= 4 :
                    new_name = new_name[3]
                else :
                    new_name = rec.name.split('#')[0]

                barcode = new_name.upper()
                rfp = self.env.ref('vit_mrp_cost.picking_type_receipt_production')
                picking = self.env['stock.picking'].sudo().search([('origin','=',barcode),('picking_type_id','=',rfp.id)],order='id desc',limit=1)
                if not picking :
                    picking = self.env['stock.picking'].sudo().search([('origin','=',barcode),('location_id.usage','=','production')],order='id desc',limit=1)
                    if not picking :
                        rec.name = False
                        self.warning = 'Receipt from production document %s not found !' % barcode
                rec.picking_id = picking.id
                

    def action_show_rfp(self):
        menu_id = self.env.ref("stock.menu_stock_root")
        url= '%s#id=%s&model=stock.picking&view_type=form&menu_id=%s'%(request.httprequest.referrer,self.picking_id.id,menu_id.id)
        res = {
                'type'     : 'ir.actions.act_url',
                'target'   : 'self',
                'url'      : url
            }
        return res

MrpReceiptFromProduction()