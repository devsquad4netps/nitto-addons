from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby
from odoo.addons.terbilang import terbilang


class VitKwitansi(models.Model):
    _name           = "vit.kwitansi"
    _inherit        = ["mail.thread"]
    _description    = 'Kwitansi'

    @api.multi
    @api.depends('total_amount','dpp')
    def _get_terbilang(self):
        for inv in self:
            amount = inv.total_amount
            if inv.dpp :
                amount = inv.amount_untaxed
            if inv.invoice_ids :
                inv.terbilang = terbilang.terbilang(amount, inv.currency_id.name, "id")


    terbilang = fields.Char('Amount in Words', compute='_get_terbilang',store=True)
    name = fields.Char('Number',required=True, default='/')
    date = fields.Date('Date', required=True, track_visibility='on_change',default=fields.Date.context_today,readonly=True,states={'draft': [('readonly', False)]})
    end_date = fields.Date('Tanggal Terima',track_visibility='on_change',readonly=True,states={'confirm': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    partner_id = fields.Many2one('res.partner','Partner',readonly=True,states={'draft': [('readonly', False)]})
    currency_id = fields.Many2one('res.currency','Currency', related='partner_id.property_product_pricelist.currency_id',store=True)
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm'),('done','Done')],default='draft', track_visibility='on_change', string="State")
    notes = fields.Text('Notes', track_visibility='on_change')
    memo = fields.Char('Memo', track_visibility='on_change',readonly=True,states={'draft': [('readonly', False)],'confirm': [('readonly', False)]})
    invoice_ids = fields.Many2many('account.invoice', string='Invoices',readonly=True,states={'draft': [('readonly', False)]})
    product_ids = fields.One2many('vit.kwitansi.details','kwitansi_id','Detail Product')
    amount_untaxed = fields.Float('Total Untaxed',readonly=True,states={'draft': [('readonly', False)]})
    amount_tax = fields.Float('Total Tax',readonly=True,states={'draft': [('readonly', False)]})
    amount_with_taxed = fields.Float('Total Amount with Tax',readonly=True,states={'draft': [('readonly', False)]})
    discount = fields.Float('Total Discount',readonly=True,states={'draft': [('readonly', False)]})
    total_amount = fields.Float('Total Amount',readonly=True,states={'draft': [('readonly', False)]})
    dpp = fields.Boolean(string='DPP',help='Dasar Perhitungan Pajak saja yang diprint')

    @api.onchange('invoice_ids')
    def _onchange_invoice_ids(self):
        if self.invoice_ids:
            self.amount_untaxed = sum(self.invoice_ids.mapped('amount_untaxed'))
            self.amount_tax = sum(self.invoice_ids.mapped('amount_tax'))
            self.amount_with_taxed = sum(self.invoice_ids.mapped('amount_total'))
            self.discount = sum(self.invoice_ids.mapped('discount'))
            self.total_amount = sum(self.invoice_ids.mapped('residual'))
            memo = self.invoice_ids.mapped('origin')
            if memo :
                self.memo = str(memo).replace('[','').replace(']','').replace("'",'')

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.kwitansi')
        elif 'name' not in vals :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.kwitansi')
        return super(VitKwitansi, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(VitKwitansi, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        product_lines = self.env['vit.kwitansi.details']
        lines = self.env['account.invoice.line']
        for inv in self.invoice_ids :
            # lines |= inv.invoice_line_ids
            for inv_line in inv.invoice_line_ids :
                product_exist = self.product_ids.filtered(lambda pro:pro.product_id.id == inv_line.product_id.id and pro.price_unit == inv_line.price_unit) 
                if product_exist:
                    product_exist.quantity += inv_line.quantity
                else :
                    product_lines.create({'kwitansi_id' : self.id,
                                        'product_id' : inv_line.product_id.id,
                                        'quantity':inv_line.quantity,
                                        'price_unit':inv_line.price_unit})

        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        self.product_ids.unlink()
        self.state = 'cancel'

    @api.multi
    def action_done(self):
        self.state = 'done'

VitKwitansi()


class VitKwitansiDetails(models.Model):
    _name           = "vit.kwitansi.details"
    _description    = "Kwitansi Details"
    _rec_name       = "customer_code"

    @api.one
    @api.depends('quantity','price_unit')
    def _calc_subtotal(self):
        subtotal = 0
        for line in self:
            line.subtotal = line.quantity*line.price_unit

    kwitansi_id = fields.Many2one('vit.kwitansi','Kwitansi', ondelete='cascade')
    customer_code = fields.Char('Customer Code', compute='_compute_customer_code',store=True)
    product_id = fields.Many2one('product.product','Product',required=False)
    quantity = fields.Float('Quantity')
    price_unit = fields.Float('Price',digits=(9,4))
    # amount_untaxed = fields.Float('Total Untaxed')
    subtotal = fields.Float('Subtotal', compute='_calc_subtotal',store=True)

    @api.depends('product_id.product_customer_code_ids')
    def _compute_customer_code(self):
        for line in self:
            if line.product_id and line.product_id.product_customer_code_ids and line.kwitansi_id.partner_id :
                code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.kwitansi_id.partner_id.id)
                if code_exist :
                    line.customer_code = code_exist[0].product_code

VitKwitansiDetails()