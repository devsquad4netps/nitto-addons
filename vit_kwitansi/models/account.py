from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare


class AccountPayment(models.Model):
    _inherit = "account.payment"

    kwitansi_ids = fields.Many2many('vit.kwitansi', string="Kwitansi")
    notes = fields.Char('Notes', size=100, track_visibility='onchange')

    @api.onchange('kwitansi_ids')
    def _onchange_kwitansi_ids(self):
        if self.kwitansi_ids:
            memo = ''
            invoice_ids = self.env['account.invoice']
            total_amount = 0.0
            for inv in self.kwitansi_ids:
                # invoice_ids |= inv.invoice_ids.filtered(lambda x:x.state == 'open' and x.currency_id == self.currency_id and x.company_id == self.company_id) 
                if self.company_id :
                    invoice_ids |= inv.invoice_ids.filtered(lambda x:x.state == 'open' and x.company_id == self.company_id)
                else :
                    invoice_ids |= inv.invoice_ids.filtered(lambda x:x.state == 'open')
                total_amount += sum(invoice_ids.mapped('residual'))
                if inv.memo :
                    sparator = " | "
                    if memo :
                        memo += (sparator+inv.memo)
                    else :
                        memo += inv.memo
            self.amount = total_amount
            self.communication = memo
            self.invoice_ids = [(6, 0, invoice_ids.ids)]
            self.currency_id = self.kwitansi_ids[0].currency_id.id
        else :
            self.invoice_ids = []
            self.communication = ''

    # @api.multi
    # def post(self):
    #     res = super(AccountPayment, self).post()
    #     if self.invoice_ids:
    #         credit_aml_id = self.move_line_ids.filtered(lambda a:a.account_id.user_type_id.type == 'receivable')
    #         for inv in self.invoice_ids.filtered(lambda x:x.state == 'open'):
    #             import pdb;pdb.set_trace()
    #             inv.assign_outstanding_credit(credit_aml_id)

AccountPayment()


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.depends('create_date', 'name', 'state')
    def _compute_po_customer(self):
        for inv in self:
            if inv.reference and inv.type == 'out_invoice' :
                so_exist = self.env['sale.order'].sudo().search([('name','=',inv.reference)],limit=1)
                if so_exist and so_exist.client_order_ref:
                    inv.po_customer = so_exist.client_order_ref
                else :
                    inv.po_customer = inv.reference
                    
    po_customer = fields.Char(string="PO Customer", compute="_compute_po_customer", store=True)

AccountInvoice()