from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportRekapInvoiceWizard(models.TransientModel):
    _name = "vit.report.rekap_inv.wizard"
    _description = "Report Rekap Invoice"


    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes")
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    partner_ids = fields.Many2many("res.partner",string='Customer', domain=[('customer','=',True)])
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    state = fields.Selection([
            ('draft','Draft'),
            ('open', 'Open'),
            #('in_payment', 'In Payment'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], string='Status', default='open')
    based_on = fields.Selection([('date_invoice','Invoice Date'),('date_due','Due Date')],string='Based on', default='date_invoice')

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'left','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000'})

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        wbf['header_month_sum'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number_val'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})

        wbf['content_number_sum_val'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['grey']})

        
        return wbf, workbook

    @api.multi
    def action_print_report(self):
        self.ensure_one()
        for report in self :
            i_type = dict(self._fields['state'].selection).get(report.state)
            report_name = 'Laporan Rekap Tagihan'
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
            company = company[:-3]
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")
            return report.action_print_report_rekap_invoice(i_type, report_name, company, companys, self.partner_ids)

    def action_print_report_rekap_invoice(self, i_type, report_name, company, companys, partners):
        for i in self :
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name+' ('+i_type+')')
            worksheet.set_column('A1:A1', 5)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 15)
            worksheet.set_column('D1:D1', 20)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 20)
            worksheet.set_column('H1:H1', 15)

            worksheet.merge_range('A1:H1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:H2', report_name + ' (' +str(i.date_start)+' s/d '+str(i.date_end)+')', wbf['company'])
            worksheet.merge_range('F3:H3', 'Printed on : '+ str(datetime.now())[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            row = 4
            invoice = self.env['account.invoice']
            grand_t_amount = 0.0
            grand_t_residual = 0.0
            for partner in partners :
                sql = "select id from account_invoice " \
                  "where (" + i.based_on + " between '%s' and '%s') and state = '%s' " \
                  "and partner_id = %s "\
                  "order by date_invoice asc , partner_id" % (str(self.date_start), str(self.date_end), i.state, partner.id)
            
                cr.execute(sql)
                datas = cr.fetchall()
                if not datas :
                    continue

                worksheet.merge_range('A%s:C%s'%(row,row), partner.name, wbf['header_table'])
                row+=1
                worksheet.write('A%s'%(row), 'No', wbf['header_month'])
                worksheet.write('B%s'%(row), 'Invoice Date', wbf['header_month'])
                worksheet.write('C%s'%(row), 'Due Date', wbf['header_month'])
                worksheet.write('D%s'%(row), 'Number', wbf['header_month'])
                worksheet.write('E%s'%(row), 'Total Amount', wbf['header_month'])
                worksheet.write('F%s'%(row), 'Residual Amount', wbf['header_month'])
                worksheet.write('G%s'%(row), 'Delivery Order', wbf['header_month'])
                worksheet.write('H%s'%(row), 'P/O Number', wbf['header_month'])

                row+=1
                no = 1
                t_amount = 0.0
                t_residual = 0.0
                
                for dt in datas :
                    data = invoice.browse(dt)
                    worksheet.write('A%s'%(row), no, wbf['title_doc'])
                    worksheet.write('B%s'%(row), str(data.date_invoice) if data.date_invoice else '', wbf['title_doc'])
                    worksheet.write('C%s'%(row), str(data.date_due), wbf['title_doc'])
                    worksheet.write('D%s'%(row), data.number, wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.amount_total, wbf['content_number_val'])
                    worksheet.write('F%s'%(row), data.residual, wbf['content_number_val'])
                    worksheet.write('G%s'%(row), data.origin or '', wbf['title_doc'])
                    worksheet.write('H%s'%(row), data.reference or '', wbf['title_doc'])

                    t_amount += data.amount_total
                    t_residual += data.residual

                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('E%s'%(row), t_amount, wbf['content_number_sum_val'])
                worksheet.write('F%s'%(row), t_residual, wbf['content_number_sum_val'])

                row += 1
            if i.notes :
                worksheet.merge_range('A%s:H%s'%(row+2,row+2), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

ReportRekapInvoiceWizard()