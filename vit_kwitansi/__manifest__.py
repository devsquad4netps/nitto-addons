{
    "name"          : "Kwitansi",
    "version"       : "1.7",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "Account",
    "summary"       : "Cetak Kwitansi Invoice",
    "description"   : """
        payment melalui form kwitansi
    """,
    "depends"       : [
        "account","payment","product_customer_code","vit_account","float_number_discount"
    ],
    "data"          : [
        "security/groups.xml",
        "security/ir.model.access.csv",
        "report/kwitansi_report.xml",
        "report/invoice_gabungan_report.xml",
        "wizard/rekap_invoice.xml",
        "views/kwitansi.xml",
        "views/account.xml",
        "data/data.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}