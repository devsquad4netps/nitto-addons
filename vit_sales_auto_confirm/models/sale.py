from odoo import api, fields, models

import logging

# _logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    auto_confirm = fields.Boolean('Auto confirm', default=False)

    def auto_confirm_sales_order(self):
        orders = self.env['sale.order'].search([('auto_confirm', '=', True), ('state', 'in', ['draft', 'sent'])])
        # _logger.info("====================")
        # _logger.info("Confirming %s orders" % len(orders))
        # _logger.info("====================")

        for order in orders:
            order.action_confirm()
