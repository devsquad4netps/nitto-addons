{
    'name': "Sales Order Auto Confirm",
    'version': '0.1',
    'author': '',
    'category': 'Sales',
    'depends': ['sale_management'],
    'data': [
        "data/ir_cron.xml",
        "views/sale_views.xml",
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}
