from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
import pdb

class company(models.Model):
    _inherit = 'res.company'

    
    format_iso = fields.Char(string='Format ISO')
#     # @api.one
#     # def _compute_po_cust(self):
#     #     obj = self.env['stock.picking.type'].sudo().search([('name','=','Consume'),
#     #                                                 ('code','=','internal'),
#     #                                                 ('warehouse_id.company_id','=',production.company_id.id)],limit=1)

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.depends('move_line_ids')
    def _compute_box(self):
        for line in self :
            if line.move_line_ids:
                line.box = len(line.move_line_ids)

    # pindah ke vit_kanban/models/stock.py
    # @api.multi
    # def write(self, vals):
    #     res = super(StockMove, self).write(vals)
    #     if self.box <= 0 :
    #         self.box = len(self.move_line_ids)
    #     return res

    code_cust   = fields.Char('Code Customer', compute='_compute_code', store=True)
    po_cust   = fields.Char('PO / Customer', compute='_compute_cust_data_lines', store=False)
    line_item   = fields.Char('Line Item', compute='_compute_cust_data_lines', store=False)
    box = fields.Integer(string='Box', help="Box (untuk report surat jalan)",)#compute="_compute_box", store=True)
    delivery_date   = fields.Char('Delivery Date', compute='_compute_cust_data_lines', store=False)

    @api.one
    @api.depends('name','product_id','state')
    def _compute_code(self):
        for x in self.product_id.product_customer_code_ids.filtered(lambda x: x.partner_id.id == self.picking_id.partner_id.id):
            self.code_cust = x.product_code

    # @api.one
    # @api.depends('name','product_id','state','write_date')
    def _compute_cust_data_lines(self):
        for i in self :
            obj_so = self.env['sale.order'].search([('name','=',i.picking_id.origin)],limit=1)
            i.po_cust = obj_so.client_order_ref
            for x in obj_so.order_line.filtered(lambda x: x.product_id.id == i.product_id.id):
                i.line_item = x.line_item
                if x.delivery_date:
                    i.delivery_date = x.delivery_date.strftime('%d %b %Y')
                else:
                    i.delivery_date = ""


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    line_item   = fields.Char('Line Item')
    delivery_date   = fields.Date('Delivery Date')
        
SaleOrderLine()


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def _action_confirm(self):
        #import pdb;pdb.set_trace()
        res = super(SaleOrder, self)._action_confirm()
        for order in self:
            for line in order.order_line.filtered(lambda i:i.delivery_date) : 
                if line.move_ids :
                    for li in line.move_ids.filtered(lambda x:x.state not in ('done','cancel')) :
                        date = datetime.combine(line.delivery_date, datetime.min.time())
                        li.date_expected = date
        return res
        

SaleOrder()