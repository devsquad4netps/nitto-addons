# coding: utf-8
# Copyright 2017 Vauxoo (https://www.vauxoo.com) info@vauxoo.com
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import api, fields, models
from odoo.exceptions import Warning

class ProductTemplate(models.Model):
    _inherit = "product.template"

    default_code = fields.Char(
        'Internal Reference', compute='_compute_default_code',
        inverse='_set_default_code', store=True)

    _sql_constraints = [
        ('default_code_uniq', 'unique (default_code)', 'The code of the product template must be unique !'),
    ]

    @api.model
    def create(self, vals):

      if not 'default_code' in vals:

        raise Warning('Internal Reference product template field is required!')
        product = super(ProductTemplate, self).create(vals)
        return product
      ref = self.env['product.template'].search([('default_code', '=', vals['default_code'])])
      if ref:
        raise Warning('Internal Reference product template field must be unique !')
      product = super(ProductTemplate, self).create(vals)
      return product
   
    @api.multi
    def write(self, vals):
      if not 'default_code' in vals:
        product = super(ProductTemplate, self).write(vals)
        return product
      ref = self.env['product.template'].search([('id','!=',self.id),('default_code', '=', vals['default_code'])])
      if ref:
        raise Warning('Internal Reference product template field must be unique!')
      product = super(ProductTemplate, self).write(vals)
      return product
#     product_customer_code_ids = fields.One2many(
#         'product.customer.code', 'product_id', string='Product customer codes',
#         copy=False, help="Different codes that has the product for each "
#         "partner.")

#     @api.model
#     def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
#         res = super(ProductTemplate, self)._name_search(
#             name, args=args, operator=operator, limit=limit,name_get_uid=name_get_uid)
#         if res or not self._context.get('partner_id'):
#             return res
#         partner_id = self._context['partner_id']
#         product_customer_code_obj = self.env['product.customer.code']
#         prod_code_ids = product_customer_code_obj.sudo().search(
#             [('product_code', '=', name),
#              ('partner_id', '=', partner_id)], limit=limit)
#         # TODO: Search for product customer name
#         res = prod_code_ids.mapped('product_id').name_get()
#         return res

class Productproduct(models.Model):
    _inherit = "product.product"

    default_code = fields.Char('Internal Reference', index=True)

    product_customer_code_ids = fields.One2many(
        'product.customer.code', 'product_id', string='Product customer codes',
        copy=False, help="Different codes that has the product for each "
        "partner.")

    _sql_constraints = [
        ('default_code_uniq', 'unique (default_code)', 'The code of the product must be unique !'),
    ]

    @api.model
    def create(self, vals):
      if not 'default_code' in vals:
        #import pdb;pdb.set_trace()
        tmpl = self.env['product.template'].browse(vals['product_tmpl_id'])
        if tmpl and tmpl.default_code :
          vals.update({'default_code':tmpl.default_code})
        else: 
          raise Warning('Internal Reference product field is required!')
        product = super(Productproduct, self).create(vals)
        return product
      ref = self.env['product.product'].search([('default_code', '=', vals['default_code'])])
      if ref:
        raise Warning('Internal Reference product field must be unique!')
      product = super(Productproduct, self).create(vals)
      return product
   
    @api.multi
    def write(self, vals):
      if not 'default_code' in vals:
        product = super(Productproduct, self).write(vals)
        return product
      ref = self.env['product.product'].search([('id','!=',self.id),('default_code', '=', vals['default_code'])])
      if ref:
        raise Warning('Internal Reference product field must be unique!')
      product = super(Productproduct, self).write(vals)
      return product
    @api.model

    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        res = super(Productproduct, self)._name_search(
            name, args=args, operator=operator, limit=limit,name_get_uid=name_get_uid)
        if res or not self._context.get('partner_id'):
            return res
        partner_id = self._context['partner_id']
        product_customer_code_obj = self.env['product.customer.code']
        prod_code_ids = product_customer_code_obj.sudo().search(
            [('product_code', 'ilike', name),
             ('partner_id', '=', partner_id)], limit=limit)
        # TODO: Search for product customer name
        res = prod_code_ids.mapped('product_id').name_get()
        return res