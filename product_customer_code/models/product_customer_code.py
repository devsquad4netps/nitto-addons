# coding: utf-8
# Copyright 2017 Vauxoo (https://www.vauxoo.com) info@vauxoo.com
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import fields, api, models

class ProductCustomerCode(models.Model):
    _name = "product.customer.code"
    _description = "Add many Customer's Code"
    _rec_name = 'product_code'

    @api.multi
    @api.depends('product_code','product_id')
    def name_get(self):
        result = []
        for res in self:
            name = res.product_code
            # if res.product_id.qty_available > 0:
            #     name = name+ ' ('+ "{:,}".format(int(res.product_id.qty_available)) +')' 
            if res.product_id.default_code :
                name = name+ ' ['+ res.product_id.default_code+']' 
            name = name+ ' '+ res.product_id.name
            result.append((res.id, name))
        return result    

    product_code = fields.Char('Customer Product Code', required=True,
                               help="This customer's product "
                               "code will be used when searching into a "
                               "request for quotation.")
    product_name = fields.Char('Customer Product Name', help="This customer's "
                               "product name will be used when searching into "
                               "a request for quotation.")
    product_id = fields.Many2one('product.product', 'Product', required=True,
                                 help='Product Identification Name')
    reference = fields.Char('Reference', related='product_id.default_code' ,store=True,
                                 help='Product Identification Number')
    partner_id = fields.Many2one('res.partner', 'Customer', required=True,
                                 help='Partner Reference')
    user_id = fields.Many2one('res.users', 'Salesperson', related='partner_id.user_id',
                                 help='Salesperson Reference', store=True)
    #qty_available = fields.Float('On Hand',related='product_id.qty_available')
    company_id = fields.Many2one(
        'res.company', 'Company', default=lambda self:
        self.env['res.company']._company_default_get('product.customer.code'))

    def _auto_init(self):
        res = super(ProductCustomerCode, self)._auto_init()
        self.env.cr.execute(
            """SELECT indexname
            FROM pg_indexes
            WHERE indexname = 'product_customer_code_index' and
            tablename = 'product_customer_code'""")
        if not self.env.cr.fetchone():
            self.env.cr.execute(
                """CREATE INDEX product_customer_code_index
                ON product_customer_code (product_code, partner_id)""")
        return res

    _sql_constraints = [
        ('unique_code', 'unique(product_code,product_id,partner_id,company_id)',
         'Product Code of customer must be unique'),
    ]
    # _sql_constraints = [
    # ('unique_code', 'Check(1=1)', 'Product Code of customer not be unique'),
    # ]

ProductCustomerCode()