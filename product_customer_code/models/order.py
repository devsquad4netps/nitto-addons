from odoo import api, fields, models
from odoo.exceptions import Warning

class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    product_customer = fields.Char('Code', compute='_compute_default_customer_code',store=True)
    product_customer_code_id = fields.Many2one('product.customer.code','Product Cust. Code')

    @api.depends('product_id')
    def _compute_default_customer_code(self):
        for line in self:
            if line.product_id and line.product_id.product_customer_code_ids and line.order_id.partner_id :
                code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.order_id.partner_id.id)
                if code_exist :
                    line.product_customer = code_exist[0].product_code

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        #import pdb;pdb.set_trace()
        res = super(SaleOrderLine,self).product_id_change()
        self.name = self.product_id.name
        return res

    @api.multi
    @api.onchange('product_customer_code_id')
    def product_customer_code_id_change(self):
        self.product_id = self.product_customer_code_id.product_id.id


SaleOrderLine()


class ResPartner(models.Model):
    _inherit = "res.partner"

    # @api.multi
    # @api.depends('ref', 'name')
    # def name_get(self):
    #     result = super(ResPartner, self).name_get()
    #     for res in self:
    #         name = res.name
    #         if res.ref :
    #             name = '['+res.ref+'] '+name
    #         result.append((res.id, name))
    #     return result

    @api.depends('is_company', 'name', 'parent_id.name', 'type', 'company_name','ref')
    def _compute_display_name(self):
        diff = dict(show_address=None, show_address_only=None, show_email=None, html_format=None, show_vat=False, show_ref=True)
        names = dict(self.with_context(**diff).name_get())
        for partner in self:
            partner.display_name = names.get(partner.id)

ResPartner()