from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning


class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"


    def get_picking_aju_header(self, aju_id, values):
        seri_barang = 1
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.picking_type_interco_id.id or picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.id) and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            jml_type_kemasan = 0
            packs = []
            for kemasan in picking_id.kemasan_ids:
                if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                    packs.append(kemasan.package_type_id.id)
                    jml_type_kemasan+=1
            if jml_type_kemasan == 0 :
                jml_type_kemasan = 1
            amount_untaxed = 0
            for move in picking_id.move_ids_without_package:
                product_exist = picking_id.subcontract_id.product_ids.filtered(lambda prod:prod.product_id.id == move.product_id.id)
                if product_exist:
                    amount_untaxed += (product_exist[0].cost_per_qty*move.quantity_done)
            values.append({
                'NOMOR AJU': aju_id.name.strip() or '',
                'KPPBC': self.bc_id.kppbc.strip() or '',
                'PERUSAHAAN': self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                'PEMASOK': '',
                'STATUS': '00',
                'KODE DOKUMEN PABEAN': self.bc_id.kode_dokumen_pabean.strip() or '',
                'NPPJK': '',
                'ALAMAT PEMASOK': '',
                'ALAMAT PEMILIK': '',
                'ALAMAT PENERIMA BARANG': '',
                'ALAMAT PENGIRIM': picking_id.partner_id.street if picking_id.partner_id.street else picking_id.partner_id.street.strip() or '',
                'ALAMAT PENGUSAHA': picking_id.company_id.street.strip() or '',
                'ALAMAT PPJK': '',
                'API PEMILIK': '',
                'API PENERIMA': '',
                'API PENGUSAHA': '',
                'ASAL DATA': self.bc_id.asal_data.strip() or '',
                'ASURANSI': '',
                'BIAYA TAMBAHAN': '',
                'BRUTO': sum(line.brutto for line in picking_id.move_ids_without_package) or '',
                'CIF': '',
                'CIF RUPIAH': '',
                'DISKON': '',
                'FLAG PEMILIK': '',
                'URL DOKUMEN PABEAN': '',
                'FOB': '',
                'FREIGHT': '',
                'HARGA BARANG LDP': '',
                'HARGA INVOICE': '',
                # 'HARGA PENYERAHAN': sum(move.purchase_line_id.price_unit * move.product_uom_qty for move in picking_id.move_ids_without_package) or '0.00',
                'HARGA PENYERAHAN': amount_untaxed,
                'HARGA TOTAL': '',
                'ID MODUL': self.bc_id.id_modul.strip() or '',
                'ID PEMASOK': '',
                'ID PEMILIK': '',
                'ID PENERIMA BARANG': '',
                'ID PENGIRIM': picking_id.partner_id.vat.strip() if picking_id.partner_id.vat else '',
                'ID PENGUSAHA': self.company_id.vat.strip() if self.company_id.vat else '',
                'ID PPJK': '',
                'JABATAN TTD': self.bc_id.jabatan_ttd.strip() if self.bc_id.jabatan_ttd else '',
                # 'JUMLAH BARANG': sum(move.quantity_done for move in picking_id.move_ids_without_package) or '',
                'JUMLAH BARANG': len(picking_id.move_ids_without_package) or '', # jumlah line
                # 'JUMLAH KEMASAN': sum(kemasan.qty for kemasan in picking_id.kemasan_ids) or '0.00',
                # jumlah type kemasan
                # 'JUMLAH KEMASAN' : jml_type_kemasan,
                'JUMLAH KEMASAN' : '1',#pasti 1 box
                'JUMLAH KONTAINER': '',
                'KESESUAIAN DOKUMEN': '',
                'KETERANGAN': '',
                'KODE ASAL BARANG': '',
                'KODE ASURANSI': '',
                'KODE BENDERA': '',
                'KODE CARA ANGKUT': '',
                'KODE CARA BAYAR': '',
                'KODE DAERAH ASAL': '',
                'KODE FASILITAS': '',
                'KODE FTZ': '',
                'KODE HARGA': '',
                'KODE ID PEMASOK': '',
                'KODE ID PEMILIK': '',
                'KODE ID PENERIMA BARANG': '',
                'KODE ID PENGIRIM': self.bc_id.kode_id_pengirim.strip() or '',
                'KODE ID PENGUSAHA': self.bc_id.kode_id_pengusaha.strip() or '',
                'KODE ID PPJK': '',
                'KODE JENIS API': '',
                'KODE JENIS API PEMILIK': '',
                'KODE JENIS API PENERIMA': '',
                'KODE JENIS API PENGUSAHA': '',
                'KODE JENIS BARANG': '',
                'KODE JENIS BC25': '',
                'KODE JENIS NILAI': '',
                'KODE JENIS PEMASUKAN01': '',
                'KODE JENIS PEMASUKAN 02': '',
                'KODE JENIS TPB': self.bc_id.kode_jenis_tpb.strip() or '',
                'KODE KANTOR BONGKAR': '',
                'KODE KANTOR TUJUAN': '',
                'KODE LOKASI BAYAR': '',
                '': '',
                'KODE NEGARA PEMASOK': '',
                'KODE NEGARA PENGIRIM': '',
                'KODE NEGARA PEMILIK': '',
                'KODE NEGARA TUJUAN': '',
                'KODE PEL BONGKAR': '',
                'KODE PEL MUAT': '',
                'KODE PEL TRANSIT': '',
                'KODE PEMBAYAR': '',
                'KODE STATUS PENGUSAHA': '',
                'STATUS PERBAIKAN': '',
                'KODE TPS': '',
                'KODE TUJUAN PEMASUKAN': '',
                'KODE TUJUAN PENGIRIMAN': self.bc_id.kode_tujuan_pengiriman.strip() or '',
                'KODE TUJUAN TPB': '',
                'KODE TUTUP PU': '',
                'KODE VALUTA': '',
                'KOTA TTD': self.bc_id.kota_ttd.strip() or '',
                'NAMA PEMILIK': '',
                'NAMA PENERIMA BARANG': '',
                'NAMA PENGANGKUT': picking_id.nama_pengangkut.strip() if picking_id.nama_pengangkut else '',
                'NAMA PENGIRIM': picking_id.partner_id.name.strip() if picking_id.partner_id else '',
                'NAMA PPJK': '',
                'NAMA TTD': self.bc_id.nama_ttd.strip() or '',
                'NDPBM': '',
                'NETTO': sum(line.netto for line in picking_id.move_ids_without_package) or '',
                'NILAI INCOTERM': '',
                'NIPER PENERIMA': '',
                'NOMOR API': '',
                'NOMOR BC11': '',
                'NOMOR BILLING': '',
                'NOMOR DAFTAR': '',
                'NOMOR IJIN BPK PEMASOK': '',
                'NOMOR IJIN BPK PENGUSAHA': '',
                'NOMOR IJIN TPB': self.bc_id.nomor_ijin_tpb.strip() or '',
                'NOMOR IJIN TPB PENERIMA': '',
                'NOMOR VOYV FLIGHT': '',
                'NPWP BILLING': '',
                'POS BC11': '',
                'SERI': self.bc_id.seri or '0',
                'SUBPOS BC11': '',
                'SUB SUBPOS BC11': '',
                'TANGGAL BC11': '',
                'TANGGAL BERANGKAT': '',
                'TANGGAL BILLING': '',
                'TANGGAL DAFTAR': '',
                'TANGGAL IJIN BPK PEMASOK': '',
                'TANGGAL IJIN BPK PENGUSAHA': '',
                'TANGGAL IJIN TPB': '',
                'TANGGAL NPPPJK': '',
                'TANGGAL TIBA': '',
                'TANGGAL TTD': self.reformat_date(picking_id.tgl_ttd.strftime('%Y-%m-%d')) if picking_id.tgl_ttd else '',
                'TANGGAL JATUH TEMPO': '',
                'TOTAL BAYAR': '',
                'TOTAL BEBAS': '',
                'TOTAL DILUNASI': '',
                'TOTAL JAMIN': '',
                'TOTAL SUDAH DILUNASI': '',
                'TOTAL TANGGUH': '',
                'TOTAL TANGGUNG': '',
                'TOTAL TIDAK DIPUNGUT': '',
                'URL DOKUMEN PABEAN2': '',
                'VERSI MODUL': self.bc_id.versi_modul.strip() or '',
                'VOLUME': sum(move.quantity_done * move.product_id.volume for move in picking_id.move_ids_without_package) or '0.0000',
                'WAKTU BONGKAR': '',
                'WAKTU STUFFING': '',
                'NOMOR POLISI': picking_id.no_polisi.strip() if picking_id.no_polisi else '',
                'CALL SIGN': '',
                'JUMLAH TANDA PENGAMAN': '',
                'KODE JENIS TANDA PENGAMAN': '',
                'KODE KANTOR MUAT': '',
                'KODE PEL TUJUAN': '',
                '': '',
                'TANGGAL STUFFING': '',
                'TANGGAL MUAT': '',
                'KODE GUDANG ASAL': '',
                'KODE GUDANG TUJUAN': '',
            })
            break
        return values

    def get_picking_aju_barang(self, aju_id, values):
        seri_barang = 1
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.picking_type_interco_id.id or picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.id) and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            seri_barang = 1
            for move in picking_id.move_ids_without_package :
                move.seri_barang = seri_barang
                seri_barang += 1
                kemasan_ids = picking_id.kemasan_ids.filtered(lambda kemasan: kemasan.product_id.id == move.product_id.id)
                cost_per_qty = 0
                product_exist = picking_id.subcontract_id.product_ids.filtered(lambda prod:prod.product_id.id == move.product_id.id)
                if product_exist:
                    cost_per_qty = product_exist[0].cost_per_qty
                values.append({
                    'NOMOR AJU': aju_id.name.strip() or '',
                    'SERI BARANG': move.seri_barang.strip() or '1',
                    'ASURANSI': '0.00',
                    'CIF': '0.00',
                    'CIF RUPIAH': '0.00',
                    'DISKON': '0.00',
                    'FLAG KENDARAAN': '',
                    'FOB': '0.00',
                    'FREIGHT': '0.00',
                    'BARANG BARANG LDP': '',
                    'HARGA INVOICE': '',
                    'HARGA PENYERAHAN': cost_per_qty * move.product_uom_qty or '0.00',
                    'HARGA SATUAN': '',
                    'JENIS KENDARAAN': '',
                    'JUMLAH BAHAN BAKU': '',
                    'JUMLAH KEMASAN': '',#sum(kemasan.qty for kemasan in kemasan_ids) or '0.00',
                    'JUMLAH SATUAN': move.quantity_done or '',
                    'KAPASITAS SILINDER': '',
                    'KATEGORI BARANG': '',
                    'KODE ASAL BARANG': '',
                    'KODE BARANG': move.product_id.default_code.strip() or '',
                    'KODE FASILITAS': '',
                    'KODE GUNA': '',
                    'KODE JENIS NILAI': '',
                    'KODE KEMASAN': '',
                    'KODE LEBIH DARI 4 TAHUN': '',
                    'KODE NEGARA ASAL': '',
                    'KODE SATUAN': move.product_uom.name.strip() or '',
                    'KODE SKEMA TARIF': '',
                    'KODE STATUS': self.bc_id.kode_status.strip() or '',
                    'KONDISI BARANG': '',
                    'MERK': picking_id.no_sj_pengirim.strip() or '',
                    # 'NETTO': sum(line.netto for line in picking_id.move_ids_without_package) or '0.00',
                    'NETTO': move.netto or '0.00', # netto per barang
                    'NILAI INCOTERM': '',
                    'NILAI PABEAN': '',
                    'NOMOR MESIN': '',
                    'POS TARIF': move.product_id.group_id.kode_tarif or '',
                    'SERI POS TARIF': '',
                    'SPESIFIKASI LAIN': move.product_id.group_id.notes.strip() if move.product_id.group_id.notes else move.product_id.group_id.name.strip() or '',
                    'TAHUN PEMBUATAN': '',
                    'TIPE': move.product_id.type_id.name.strip() or '',
                    'UKURAN': '',
                    'URAIAN': move.product_id.name.strip() or '',
                    'VOLUME': move.quantity_done * move.product_id.volume or '0.000',
                    'SERI IJIN': '',
                    'ID EKSPORTIR': '',
                    'NAMA EKSPORTIR': '',
                    'ALAMAT EKSPORTIR': '',
                    'KODE PERHITUNGAN': '',
                })
        return values

    def get_picking_aju_dokumen(self, aju_id, values):
        seri_dokumen = 1
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.picking_type_interco_id.id or picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.id) and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            for dok in picking_id.dokumen_ids :
                values.append({
                            'NOMOR AJU': aju_id.name.strip() or '',
                            'SERI DOKUMEN': seri_dokumen,
                            'FLAG URL DOKUMEN': '',
                            'KODE JENIS DOKUMEN': dok.dokumen_master_id.code.strip() if dok.dokumen_master_id else '',
                            # 'NOMOR DOKUMEN': picking_id.no_sj_pengirim or '',
                            'NOMOR DOKUMEN': dok.nomor.strip() if dok.nomor else '',
                            'TANGGAL DOKUMEN': self.reformat_date(dok.tanggal.strftime('%Y-%m-%d')) if dok.tanggal else '',
                            'TIPE DOKUMEN': self.bc_id.tipe_dokumen.strip() or '',
                            'URL DOKUMEN': '',
                            })
                seri_dokumen += 1
        return values

    def get_picking_aju_kemasan(self, aju_id, values):
        for picking_id in aju_id.picking_ids.filtered(lambda picking:(picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.picking_type_interco_id.id or picking.picking_type_id.id == picking.subcontract_id.interco_picking_type_id.id) and picking.subcontract_id.is_interco and picking.tgl_dokumen == self.date):
            for kemasan_id in picking_id.kemasan_ids.filtered(lambda x:x.qty > 0.0) :
                values.append({
                    'NOMOR AJU': aju_id.name.strip() or '',
                    'SERI KEMASAN': '',
                    'JUMLAH KEMASAN': kemasan_id.qty or '0.00',
                    'KESESUAIAN DOKUMEN': '',
                    'KETERANGAN': '',
                    'KODE JENIS KEMASAN': kemasan_id.package_type_id.code.strip() if kemasan_id.package_type_id.code else '',
                    # 'MEREK KEMASAN': kemasan_id.picking_id.no_sj_pengirim.strip() if kemasan_id.picking_id.no_sj_pengirim else '',
                    'MEREK KEMASAN':'',
                    'NIP GATE IN': '',
                    'NIP GATE OUT': '',
                    'NOMOR POLISI': '',
                    'NOMOR SEGEL': '',
                    'WAKTU GATE IN': '',
                    'WAKTU GATE OUT': '',
                })
        return values


VitExportImportBcWizard()