
from odoo import api, fields, models
import time
import datetime
from io import BytesIO
import xlsxwriter
import base64
import logging
from odoo.exceptions import Warning 
_logger = logging.getLogger(__name__)



class StockOpnameWIP(models.Model):
    _name = "stock_opname"
    _inherit = ["mail.thread"]
    _description    = 'Data Opname WIP'
    
    name = fields.Char( string="Number", readonly=True, default='New')
    date = fields.Date( string="Opname Date",  required=False, 
                        default=lambda self: time.strftime("%Y-%m-%d"),readonly=True,states={'draft': [('readonly', False)]}, track_visibility='on_change')
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.user,readonly=True)
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    notes = fields.Text( string="Notes",readonly=True,states={'draft': [('readonly', False)]}, track_visibility='on_change')
    data = fields.Binary('File')
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm')],default='draft', track_visibility='on_change', string="State")
    
    export_excels = fields.Boolean(
        string='Export Excel', dafault=False
    )
    opname_line_ids = fields.One2many('stock_opname','opname_id', 'Opname Lines')
    
    
    @api.model
    def create(self, vals):
        if not vals.get('name', False) or vals['name'] == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('stock_opname') or 'Error Number!!!'
        return super(stock_opname, self).create(vals)
    
    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(stock_opname, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

    def cell_format(self, workbook):
        cell_format = {}
        cell_format['title'] = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 20,
            'font_name': 'Arial',
        })
        cell_format['header'] = workbook.add_format({
            'bold': True,
            'align': 'center',
            'border': True,
            'font_name': 'Arial',
        })
        cell_format['content'] = workbook.add_format({
            'font_size': 11,
            'border': False,
            'font_name': 'Arial',
        })
        cell_format['content_float'] = workbook.add_format({
            'font_size': 11,
            'border': True,
            'num_format': '#,##0.00',
            'font_name': 'Arial',
        })
        cell_format['total'] = workbook.add_format({
            'bold': True,
            'num_format': '#,##0.00',
            'border': True,
            'font_name': 'Arial',
        })
        return cell_format, workbook


    @api.multi
    def print_excel(self):
        #obj_stock_opname = self.env["vit.stock_opname"].search([('export_excels','=',False)])
        
        headers = [
            "External ID",
            "Reference",
            "Work Orders/Display Name",
            "Work Orders/Work Order",
            "Work Orders/Status",
            "Work Orders/Qty [pcs]",
            "Work Orders/Qty [kg]",
        ]

        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        cell_format, workbook = self.cell_format(workbook)

        # if not obj_stock_opname :
        #     raise Warning("Data tidak ditemukan. Mohon Create SOP WIP dulu")

        worksheet = workbook.add_worksheet()
        worksheet.set_column('A:ZZ', 30)
        column_length = len(headers)

        column = 0
        row = 0
        for col in headers:
            worksheet.write(row, column, col, cell_format['header'])
            column += 1

        ########### contents
        row = 1
        final_data=[]
       
        for data in obj_stock_opname :
            final_data.append([
                "Opname"+ str(data.id),
                data.No_po.name,
                data.storage,
                data.Proses.name,
                "Last Done",
                data.Qty_pcs,
                data.Qty,
            ])
            
            data.export_excels=True
        

        for data in final_data:
            column = 0
            for col in data:
                worksheet.write(row, column, col, cell_format['content'] if column<0 else  cell_format['content_float'])
                column += 1
            row += 1

        workbook.close()
        result = base64.encodestring(fp.getvalue())
        filename = self.name + '-' + str(self.tanggal) + '%2Exlsx'
        self.write({'data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

StockOpnameWIP()


class StockOpnameLinesWIP(models.Model):
    _name = "stock_opname_line"

    no_poly_box = fields.Char("No Polybox" , size=3 ,readonly=True)
    product_id = fields.Many2one('product.product',string="Product", related='production_id.product_id',store=True )
    production_id = fields.Many2one("mrp.production", string="Manufacturing Order",required=True)
    customer_id = fields.Char('res.partner', string="Customer", related='production_id.customer_id',store=True )
    fg_qty = fields.Float(string="FG Qty",readonly=True,states={'draft': [('readonly', False)]}) 
    fg_uom_id = fields.Many2one('uom.uom','FG UoM')
    wire_qty = fields.Float(string="Wire Qty",readonly=True,states={'draft': [('readonly', False)]})
    wr_uom_id = fields.Many2one('uom.uom','Wire UoM')
    group_report = fields.Char('Workcenter Group')
    workorder_status = fields.Char('Workorder Status')
    opname_id = fields.Many2one("stock_opname", 'Opname', ondelete='Cascade')

StockOpnameLinesWIP()