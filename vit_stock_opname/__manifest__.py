#-*- coding: utf-8 -*-

{
	"name": "Stock Opname WIP",
	"version": "3.0", 
	"depends": [
		'base','stock','mrp','product','barcodes','vit_kartu_prod_add','vit_workorder'
	],
	'author': 'vitraining.com',
	'website': 'http://www.vitraining.com',
	"summary": "Data penyimpan stock opname agar tidak ada pergerakan",
	"description": """
-Menu SON WIP
-Export ke excel
""",
	"data": [
		"security/group.xml",
		"security/ir.model.access.csv",
		"view/stock_opname.xml",
		"data/sequence.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}