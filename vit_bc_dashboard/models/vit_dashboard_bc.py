from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError, ValidationError
import time, xlsxwriter, base64, pytz
from io import StringIO, BytesIO
from pytz import timezone


class DashboardBC(models.Model):
    _name = 'vit.dashboard.bc'
    _description = 'Dashboard Bea Cukai'

    def get_child_categs(self, category_id):
        ids_categ = [category_id.id]
        for child_categ in category_id.categ_ids:
            ids_categ.extend(self.get_child_categs(child_categ))
        return ids_categ

    @api.model
    def _default_name(self):
        return 'Kawasan Berikat '+ str(self.env.user.company_id.name)

    @api.depends('bc_type')
    def _default_in_out_type(self):
        for bc in self :
            if bc.bc_type and bc.bc_type.category_report:
                if bc.bc_type.category_report == 'Laporan Barang Masuk':
                    bc.in_out_type = 'In' 
                elif bc.bc_type.category_report == 'Laporan Barang Keluar':
                    bc.in_out_type = 'Out' 


    @api.depends('bc_type')
    def _get_bc_name(self):
        for bc in self :
            if bc.bc_type :
                bc.bc_type_name = bc.bc_type.name

    # @api.depends('bc_type')
    # def _get_categ_id(self):
    #     for bc in self :
    #         categ = self.env['product.category']
    #         if bc.bc_type and bc.bc_type.name in ('Mutasi Bahan Baku','Total Inventory') :
    #             categ_exist = categ.sudo().search([('complete_name','ilike','wire')], limit=1)
    #             if categ_exist:
    #                 bc.categ_id = categ_exist.id
    #         elif bc.bc_type and bc.bc_type.name in ('Mutasi Mesin dan Peralatan','Total Inventory'):
    #             categ_exist = categ.sudo().search(['|','|',('complete_name','ilike','mesin'),('complete_name','ilike','machine'),('complete_name','ilike','asset')], limit=1)
    #             if categ_exist:
    #                 bc.categ_id = categ_exist.id
    @api.onchange('bc_type')
    def onchange_bc_type(self):
        for bc in self :
            categ = self.env['product.category']
            if bc.bc_type and bc.bc_type.name == 'Mutasi Bahan Baku' :
                categ_exist = categ.sudo().search([('complete_name','ilike','wire')], limit=1)
                if categ_exist:
                    bc.categ_id = categ_exist.id
                    bc.categ2_id = False
            elif bc.bc_type and bc.bc_type.name == 'Mutasi Mesin dan Peralatan':
                categ_exist = categ.sudo().search(['|','|',('complete_name','ilike','mesin'),('complete_name','ilike','machine'),('complete_name','ilike','asset')], limit=1)
                if categ_exist:
                    bc.categ_id = categ_exist.id
                    bc.categ2_id = False
            elif bc.bc_type and bc.bc_type.name == 'Mutasi Barang Jadi' :
                categ_exist = categ.sudo().search([('complete_name','ilike','Finish Good')], limit=1)
                if categ_exist:
                    bc.categ2_id = categ_exist.id
                    bc.categ_id = False
            elif bc.bc_type and bc.bc_type.name == 'Mutasi Mesin dan Peralatan':
                categ_exist = categ.sudo().search(['|','|',('complete_name','ilike','mesin'),('complete_name','ilike','machine'),('complete_name','ilike','asset')], limit=1)
                if categ_exist:
                    bc.categ2_id = categ_exist.id
                    bc.categ_id = False
            elif bc.bc_type and bc.bc_type.name == 'Total Inventory':
                categ_exist = categ.sudo().search([('complete_name','ilike','wire')], limit=1)
                if categ_exist:
                    bc.categ_id = categ_exist.id
                categ_exist = categ.sudo().search([('complete_name','ilike','Finish Good')], limit=1)
                if categ_exist:
                    bc.categ2_id = categ_exist.id


    @api.onchange('categ_id')
    def onchange_categ_id(self):
        domain = []
        for x in self :
            domain = {'domain': {'categ_id': [('complete_name', 'ilike', 'Raw Material')]}}
        return domain


    @api.onchange('categ2_id')
    def onchange_categ2_id(self):
        domain = []
        for x in self :
            domain = {'domain': {'categ2_id': [('complete_name', 'ilike', 'Finish Good')]}}
        return domain

    # @api.depends('bc_type')
    # def _get_categ2_id(self):
    #     for bc in self :
    #         categ = self.env['product.category']
    #         if bc.bc_type and bc.bc_type.name in ('Mutasi Barang Jadi','Total Inventory') :
    #             categ_exist = categ.sudo().search([('complete_name','ilike','Finish Good')], limit=1)
    #             if categ_exist:
    #                 bc.categ2_id = categ_exist.id
    #         elif bc.bc_type and bc.bc_type.name in ('Mutasi Mesin dan Peralatan','Total Inventory'):
    #             categ_exist = categ.sudo().search(['|','|',('complete_name','ilike','mesin'),('complete_name','ilike','machine'),('complete_name','ilike','asset')], limit=1)
    #             if categ_exist:
    #                 bc.categ2_id = categ_exist.id

    @api.depends('bc_type')
    def _get_loc_id(self):
        for bc in self :
            loc = self.env['stock.location']
            if bc.bc_type and bc.bc_type.name in ('Mutasi Barang Reject dan Scrap','Total Inventory'):
                loc_exist = loc.sudo().search([('usage','=','inventory'),('scrap_location','=',True),('company_id','=',self.env.user.company_id.id)], limit=1)
                if not loc_exist:
                    loc_exist = loc.sudo().search([('usage','=','inventory'),('scrap_location','=',True)], limit=1)
                    if loc_exist :
                        bc.location_id = loc_exist.id

    @api.onchange('date_start','date_end')
    def date_change(self):
        if self.date_start > self.date_end :
            self.date_start = self.date_end 
            raise Warning("Start date tidak boleh lebih besar dari end date")

    name = fields.Char('Description',required=True, default=_default_name)
    date_start = fields.Date('Date Start',
        default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date('Date End',
        default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    bc_type = fields.Many2one('vit.export.import.bc','BC')
    bc_type_name = fields.Char('BC Name',store=True, compute="_get_bc_name")
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user)
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id)
    detail_ids = fields.One2many('vit.dashboard.bc.detail','dashboard_id')
    detail_wip_ids = fields.One2many('vit.dashboard.bc.detail_wip','dashboard_id')
    detail_mutasi_ids = fields.One2many('vit.dashboard.bc.detail_mutasi','dashboard_id')
    detail_asset_ids = fields.One2many('vit.dashboard.bc.detail_asset','dashboard_id')
    total_mutasi_ids = fields.One2many('vit.dashboard.bc.total_mutasi','dashboard_id')
    file_data = fields.Binary('File', readonly=True)
    # categ_id = fields.Many2one('product.category','Category RM', compute='_get_categ_id', store=True)
    # categ2_id = fields.Many2one('product.category','Category FG', compute='_get_categ2_id', store=True)
    categ_id = fields.Many2one('product.category','Category RM', )
    categ2_id = fields.Many2one('product.category','Category FG')
    location_id = fields.Many2one('stock.location','Location', compute='_get_loc_id', store=True)
    category_report = fields.Selection([('Laporan Barang Masuk','Laporan Barang Masuk'),('Laporan Barang Keluar','Laporan Barang Keluar'),('Laporan Inventory','Laporan Inventory')],string='Category Report')
    in_out_type = fields.Selection([('In','In'),('Out','Out')], string='Type', compute='_default_in_out_type', store=True)

    def check_history(self, transaction_type, picking):       
        movel = self.env['stock.move.line']
        for mlines in picking.move_ids_without_package:
            lot_exist = mlines.move_line_ids.filtered(lambda i:i.lot_id != False)
            if lot_exist:
                # cari MO nya
                mrp_exist = movel.sudo().search([('lot_id','=',lot_exist[0].lot_id.id),('location_id.usage','=','production')],limit=1, order='id desc')
                if mrp_exist :
                    # cari bahan bakunya
                    bahan_baku = mrp_exist.move_id.production_id.move_raw_ids.filtered(lambda raw:raw.product_id.type == 'product' and raw.product_id.product_tmpl_id.categ_id.name in ['WIRE','Wire','wire'])
                    if bahan_baku :
                        lot_bb = bahan_baku[0].move_line_ids.filtered(lambda bb:bb.lot_id != False)
                        if lot_bb :
                            po_exist = movel.sudo().search([('lot_id','=',lot_bb[0].lot_id.id),('move_id.location_id.usage','=','supplier')],limit=1, order='id desc')
                            #po_emport = lot_bb[0].move_id.purchase_order_ids.filtered(lambda p:p.purchase_type == 'Import' and p.state != 'cancel')
                            if po_exist:
                                if transaction_type == 'Kawasan Berikat' :
                                    if po_exist.move_id.purchase_line_id.order_id.purchase_type in ('Local','import','Kawasan Berikat'):
                                        if po_exist.move_id.purchase_line_id.order_id.state == 'cancel':
                                            return True
                                elif po_exist.move_id.purchase_line_id.order_id.purchase_type == transaction_type : 
                                    if po_exist.move_id.purchase_line_id.order_id.state == 'cancel':
                                        return True
                                else :
                                    return True
                            else :
                                return True
                        else :
                            return True
                    else :
                        return True
                else :
                    return True
            else :
                return True
        return False

    @api.multi
    def action_generate_details(self):
        self.ensure_one()
        if self.bc_type.name in ('BC 2.3','BC 2.5','BC 2.7','BC 2.6.1','BC 2.6.2','BC 3.0','BC 4.0','BC 4.1','Total Barang Masuk','Total Barang Keluar') :
            self.generate_details_bc()
        elif self.bc_type.name == 'Posisi WIP':
            self.generate_details_wip()
        elif self.bc_type.name == 'Mutasi Bahan Baku':
            self.generate_details_mutasi_by_categ()
        elif self.bc_type.name == 'Mutasi Barang Jadi':
            self.generate_details_mutasi_by_categ2()
        elif self.bc_type.name == 'Mutasi Barang Reject dan Scrap':
            self.generate_details_mutasi_by_location()
        elif self.bc_type.name == 'Mutasi Mesin dan Peralatan':
            self.generate_details_mutasi_asset()
        elif self.bc_type.name == 'Total Inventory':
            self.generate_details_total_inventory()

    def generate_details_bc(self):
        self._cr.execute("DELETE FROM vit_dashboard_bc_detail WHERE dashboard_id = %s" , ( [(self.id)] ))
        self.env.cr.commit()
        picking = self.env['stock.picking']
        kanban = self.env['vit.kanban']
        detail_obj = self.env['vit.dashboard.bc.detail']
        mrp = self.env['mrp.production']
        purchase = self.env['purchase.order']
        subcon = self.env['mrp.subcontract']
        sale = self.env['sale.order']
        move = self.env['stock.move']
        movel = self.env['stock.move.line']
        bc_type_name = self.bc_type.name
        datas1 = []
        datas2 = []
        if bc_type_name in ('BC 2.3','BC 2.7','BC 4.0','BC 2.6.2','Total Barang Masuk') :
            if bc_type_name == 'BC 2.7' and self.in_out_type != 'In' :
                lanjut = 'lanjut'
            else :
                datas1 = picking.sudo().search([('company_id','=',self.company_id.id),
                                                ('picking_type_id.code','in',['incoming','internal','mrp_operation']),
                                                ('state','=','done'),
                                                ('is_bea_cukai','=',True),
                                                ('tgl_dokumen','>=',self.date_start),('tgl_dokumen','<=',self.date_end)],order='tgl_dokumen asc')

        if bc_type_name in ('BC 2.5','BC 2.7','BC 3.0','BC 4.1','BC 2.6.1','Total Barang Keluar') :
            if bc_type_name == 'BC 2.7' and self.in_out_type != 'Out' :
                lanjut = 'lanjut'
            else :
                datas2 = picking.sudo().search([('company_id','=',self.company_id.id),
                                                ('picking_type_id.code','=',['outgoing','internal','mrp_operation']),
                                                ('state','=','done'),
                                                ('is_bea_cukai','=',True),
                                                ('tgl_dokumen','>=',self.date_start),('tgl_dokumen','<=',self.date_end)],order='tgl_dokumen asc')
        if datas1 and datas2 :
            datas = datas1+datas2
        elif datas1 and not datas2:
            datas = datas1
        elif datas2 and not datas1 :
            datas = datas2
        else :
            datas = False 
        if datas : 
            seq = 0
            for pick in datas :
                bc_type_name = self.bc_type.name
                # jika tidak ada group_id brti retur kanban, bisa dilanjut, jika tdk continue
                if pick.picking_type_id.code == 'incoming' and pick.group_id:

                    if bc_type_name == 'BC 2.3':
                        if not pick.bc_type :
                            po_exist = purchase.sudo().search([('name','=',pick.origin),('purchase_type','=','Import')])
                            if not po_exist :
                                continue
                        elif pick.bc_type != 'BC 2.3':
                            continue
                    elif bc_type_name == 'BC 4.0':
                        if not pick.bc_type :
                            po_exist = purchase.sudo().search([('name','=',pick.origin),('purchase_type','=','Local')])
                            if not po_exist :
                                continue
                        elif pick.bc_type != 'BC 4.0':
                            continue
                    elif bc_type_name == 'BC 2.7':
                        if not pick.bc_type :
                            # khusus retur kws berikat, cari SO nya
                            so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Kawasan Berikat')])
                            po_exist = purchase.sudo().search([('name','=',pick.origin),('purchase_type','=','Kawasan Berikat')])
                            if not so_exist and not po_exist:
                                continue
                        elif pick.bc_type != 'BC 2.7':
                            continue
                    elif bc_type_name == 'BC 2.6.2' :
                        if not pick.bc_type :
                            # so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Kawasan Berikat')])
                            # if not so_exist :
                            if pick.picking_type_id.id != pick.subcontract_id.return_type_id.id:
                                continue
                        elif pick.bc_type != 'BC 2.6.2':
                            continue
                    elif bc_type_name == 'Total Barang Masuk' :
                        if not pick.bc_type :
                            po_exist = purchase.sudo().search([('name','=',pick.origin),('purchase_type','in',['Import','Local'])])
                            if not po_exist :
                                # cari jikalau ada retur
                                so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Kawasan Berikat')])
                                if not so_exist :
                                    # subcon_exist = subcon.sudo().search([('picking_type_id','=',pick.subcontract_id.return_type_id.id)])
                                    # if not subcon_exist:
                                    #cari subcon
                                    if pick.picking_type_id.id != pick.subcontract_id.return_type_id.id:
                                        continue
                                    else :
                                        bc_type_name = 'BC 2.6.2'
                                else :
                                    bc_type_name = 'BC 2.7'
                                    if self.in_out_type != 'In':
                                        continue
                            else :
                                if po_exist.purchase_type == 'Import' :
                                    bc_type_name = 'BC 2.3'
                                elif po_exist.purchase_type == 'Local' :
                                    bc_type_name = 'BC 4.0'
                        elif pick.bc_type not in ('BC 2.3','BC 2.7','BC 4.0','BC 2.6.2'):
                            continue
                        else:
                            bc_type_name = pick.bc_type
                    else :  
                        continue
                # jika tidak ada group_id brti retur kanban, bisa dilanjut, jika tdk continue
                elif pick.picking_type_id.code == 'outgoing' and pick.group_id:
                    # jual lokal, bahan baku import
                    if bc_type_name == 'BC 2.5':
                        if not pick.bc_type :
                            so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Local')])
                            if not so_exist :
                                continue
                            by_pass = self.check_history('Import', pick)
                            if by_pass :
                                continue
                        elif pick.bc_type != 'BC 2.5':
                            continue
                    elif bc_type_name == 'BC 4.1':
                        if not pick.bc_type :
                            so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Local')])
                            if not so_exist :
                                continue
                            by_pass = self.check_history('Local', pick)
                            if by_pass :
                                continue
                        elif pick.bc_type != 'BC 4.1':
                            continue
                    elif bc_type_name == 'BC 2.7':
                        if not pick.bc_type :
                            so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Kawasan Berikat')])
                            if not so_exist :
                                continue
                            by_pass = self.check_history('Kawasan Berikat', pick)
                            if by_pass :
                                continue
                        elif pick.bc_type != 'BC 2.7':
                            continue
                    elif bc_type_name == 'BC 2.6.1':
                        so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','in',['Local','Kawasan Berikat'])])
                        if not so_exist :
                            # subcon_exist = subcon.sudo().search([('picking_type_id','=',pick.subcontract_id.picking_type_id.id)])
                            # if not subcon_exist:
                            if pick.picking_type_id.id != pick.subcontract_id.picking_type_id.id:
                                continue
                        elif pick.bc_type != 'BC 2.6.1':
                            continue
                    # jual export beli import
                    elif bc_type_name == 'BC 3.0':
                        if not pick.bc_type :
                            so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','=','Export')])
                            if not so_exist :
                                continue
                            by_pass = self.check_history('Import', pick)
                            if by_pass :
                                continue
                        elif pick.bc_type != 'BC 3.0':
                            continue
                    elif bc_type_name == 'Total Barang Keluar' :
                        if not pick.bc_type :
                            so_exist = sale.sudo().search([('name','=',pick.origin),('sale_type','in',['Local','Kawasan Berikat','Export'])])
                            if not so_exist :
                                # subcon_exist = subcon.sudo().search([('picking_type_id','=',pick.subcontract_id.picking_type_id.id)])
                                # if not subcon_exist:
                                # cari subcon
                                if pick.picking_type_id.id != pick.subcontract_id.picking_type_id.id:
                                    continue
                            else :
                                if so_exist.sale_type == 'Local' :
                                    bc_type_name = 'BC 2.5'
                                    by_pass = self.check_history('Import', pick)
                                    if by_pass :
                                        #jika bukan import brrti cek jika lokal
                                        bc_type_name = 'BC 4.1'
                                        by_pass = self.check_history('Local', pick)
                                        if by_pass :
                                            continue
                                elif so_exist.sale_type == 'Kawasan Berikat' :
                                    bc_type_name = 'BC 2.7'
                                    if self.in_out_type != 'Out':
                                        continue
                                    by_pass = self.check_history('Kawasan Berikat', pick)
                                    if by_pass :
                                        continue
                                elif so_exist.sale_type == 'Export' :
                                    bc_type_name = 'BC 3.0'
                                    if self.in_out_type != 'Out':
                                        continue
                                    by_pass = self.check_history('Import', pick)
                                    if by_pass :
                                        continue
                        elif pick.bc_type not in ('BC 2.5','BC 4.1','BC 2.7','BC 2.6.1'):
                            continue
                        else:
                            bc_type_name = pick.bc_type
                    else:
                        continue
                elif pick.picking_type_id.code in ('internal','mrp_operation') :
                    if not pick.bc_type :
                        if bc_type_name in ('BC 2.6.2','Total Barang Masuk') and pick.subcontract_id :
                            if pick.subcontract_id.return_type_id.id != pick.picking_type_id.id :
                                continue
                            bc_type_name = 'BC 2.6.2'
                        elif bc_type_name in ('BC 2.6.1','Total Barang Keluar') and pick.subcontract_id:
                            if pick.subcontract_id.picking_type_id.id != pick.picking_type_id.id :
                                continue
                            bc_type_name = 'BC 2.6.1'
                            # by_pass = self.check_history('Subcon', pick)
                            # if by_pass :
                            #     continue
                        else :
                            continue
                    elif pick.bc_type and pick.bc_type in ('BC 2.6.2','BC 2.6.1'):
                        bc_type_name = pick.bc_type
                    # else :
                    #     continue
                if bc_type_name != self.bc_type.name :
                    continue
                details = pick.move_ids_without_package
                if not details :
                    #details = pick.move_line_ids
                    details = pick.move_line_ids.mapped('move_id')
                bc_type = self.bc_type.id
                no_pabean = pick.nomor_aju_id.name
                if pick.no_pabean :
                    no_pabean = pick.no_pabean
                no_picking = pick.id
                no_doc = pick.name
                no_doc_ext = ''
                group_document_report = pick.name
                if pick.kanban_id :
                    no_doc = pick.kanban_id.name
                    group_document_report = pick.kanban_id.name
                if pick.kanban_id.no_sj_pengirim :
                    no_doc_ext = pick.kanban_id.no_sj_pengirim
                elif details :
                    try :
                        if details[0].kanban_id :
                            no_doc = details[0].kanban_id.name
                            group_document_report = details[0].kanban_id.name
                    except :
                        if details[0].move_id.kanban_id:
                            no_doc = details[0].move_id.kanban_id.name
                            group_document_report = details[0].move_id.kanban_id.name
                tgl_dokumen = pick.tgl_dokumen
                tgl_picking = pick.date_done
                partner_id = pick.partner_id.id
                new_bc_type_name = bc_type_name
                currency = False
                price_unit = 0.0
                if pick.move_lines[0].purchase_line_id:
                    currency_id = pick.move_lines[0].purchase_line_id.order_id.currency_id
                    currency = pick.move_lines[0].purchase_line_id.order_id.currency_id.name or False
                elif pick.move_lines[0].sale_line_id :
                    currency_id = pick.move_lines[0].sale_line_id.order_id.currency_id
                    currency = pick.move_lines[0].sale_line_id.order_id.currency_id.name or False
                elif pick.partner_id.property_purchase_currency_id:
                    currency_id = pick.partner_id.property_purchase_currency_id
                    currency = pick.partner_id.property_purchase_currency_id.name or False
                else :
                    currency_id = pick.partner_id.property_product_pricelist.currency_id
                    currency = pick.partner_id.property_product_pricelist.currency_id.name or False
                
                for move in details:
                    #import pdb;pdb.set_trace()
                    price_unit = 0.0
                    if move.purchase_line_id:
                        price_unit = move.purchase_line_id.price_unit
                    elif move.sale_line_id:
                        price_unit = move.sale_line_id.price_unit
                    if price_unit == 0.0 :
                        price_unit = move.price_unit
                        if price_unit == 0.0 :
                            if move.purchase_line_id.invoice_lines :
                                price_unit = move.purchase_line_id.invoice_lines[0].price_unit
                            if price_unit == 0.0 :
                                # price_unit = move.product_id.standard_price
                                # price_unit = currency_id._convert(price_unit, self.company_id.currency_id, self.company_id, self.date_end or fields.Date.today())
                                # if price_unit == 0.0 :
                                if move.kanban_id and move.kanban_id.reference:
                                    ori_doc = move.kanban_id.reference
                                    kanban_ori = kanban.sudo().search([('name','=',ori_doc)],limit=1)
                                    if kanban_ori :
                                        move_ori = kanban_ori.detail_ids.filtered(lambda k:k.product_id.id == move.product_id.id)
                                        if move_ori :
                                            if move_ori[0].purchase_line_id:
                                                price_unit = move_ori[0].purchase_line_id.price_unit
                                            elif move_ori[0].sale_line_id:
                                                price_unit = move_ori[0].sale_line_id.price_unit

                            if price_unit == 0.0 :
                                if pick.subcontract_id :
                                    price_unit = pick.subcontract_id.cost_per_qty
                    # cari dulu product yg exist supaya di sum jika kanban yg sama
                    line_kanban_exist = False
                    kanban_exist = False
                    if move.kanban_id :
                        line_kanban_exist = detail_obj.search([('dashboard_id','=',self.id),
                                                            ('product_id','=',move.product_id.id),
                                                            ('picking_number','=',group_document_report)],limit=1)
                        if line_kanban_exist :
                            line_kanban_exist.jumlah += move.quantity_done
                            line_kanban_exist.nilai += (price_unit*move.quantity_done)
                        else :
                            kanban_exist = detail_obj.search([('dashboard_id','=',self.id),
                                                            ('picking_number','=',group_document_report)],limit=1)
                            if kanban_exist :
                                detail_obj.create({'dashboard_id' : self.id,
                                        'sequence' : kanban_exist.sequence,
                                        'group_document_report' : group_document_report,
                                        'name' : '',
                                        'bc_type' : '',
                                        'no_pabean' : '',
                                        'tgl_pabean' : False,
                                        'no_picking' : False,
                                        'move_id' : move.id,
                                        'picking_number': '',
                                        'picking_number_ext': '',
                                        'tgl_picking' : False,
                                        'partner_id' : False,
                                        'product_id' : move.product_id.id,
                                        'jumlah' : move.quantity_done,
                                        'uom' : move.product_uom.name,
                                        'nilai' : abs(price_unit*move.quantity_done),
                                        'currency' :currency})
                    if not line_kanban_exist and not kanban_exist:
                        # if no_doc == 'KBN/IN/00005' :
                        #     import pdb;pdb.set_trace()
                        #     x=1
                        if not tgl_picking :
                            tgl_picking = move.date
                        
                        detail_obj.create({'dashboard_id' : self.id,
                                        'sequence': seq,
                                        'name' : self.name,
                                        'group_document_report' : group_document_report,
                                        'bc_type' : new_bc_type_name,
                                        'no_pabean' : no_pabean,
                                        'tgl_pabean' : tgl_dokumen,
                                        'no_picking' : no_picking,
                                        'move_id' : move.id,
                                        'picking_number': group_document_report,
                                        'picking_number_ext': no_doc_ext,
                                        'tgl_picking' : tgl_picking,
                                        'partner_id' : partner_id,
                                        'product_id' : move.product_id.id,
                                        'jumlah' : move.quantity_done,
                                        'uom' : move.product_uom.name,
                                        'nilai' : abs(price_unit*move.quantity_done),
                                        'currency' :currency})
                        seq+=1
                    self.env.cr.commit()
                    new_bc_type_name = False
                    bc_type = False
                    no_pabean = False
                    tgl_dokumen = False
                    no_picking = False
                    no_doc = False
                    tgl_picking = False
                    partner_id = False
        return True 


    def _search_movement_to(self, usage, product_id, company_id, type):
        result = 0
        if type == 'non inventory':
            # movements = self.env['stock.move.line'].sudo().search([('product_id','=',product_id),
            #                                             ('date','>=',self.date_start),
            #                                             ('date','<=',self.date_end),
            #                                             ('state','=','done'),
            #                                             ('location_dest_id.usage','=',usage),
            #                                             ('move_id.inventory_id','=',False),
            #                                             ('move_id.company_id','=',company_id.id)])
            sql = """select sum(sm.product_uom_qty) from stock_move sm
                    left join stock_location sl on sl.id = sm.location_dest_id
                    where sm.product_id = %s 
                    and sm.state = 'done' and (sm.date  >= '%s' and sm.date <= '%s' ) 
                    and sl.usage = '%s' and sl.comment is null and sm.inventory_id is null 
                    and sm.company_id = %s """ % (product_id,self.date_start,str(self.date_end)+' 23:59:59',usage,company_id.id)
            self._cr.execute(sql)
            res = self._cr.dictfetchone()
            if res['sum'] :
                result = res['sum']
        elif type == 'saldo':
            sql = """select sum(sml.qty_done) from stock_move_line sml 
                    left join stock_move sm on sm.id = sml.move_id
                    left join stock_location sl on sl.id = sm.location_dest_id
                    where sl.usage = '%s' and sl.comment is null and sml.product_id = %s 
                    and sml.state = 'done' and sm.date < '%s' """ % (usage,product_id,self.date_start)
            self._cr.execute(sql)
            res = self._cr.dictfetchone()
            if res['sum'] :
                result = res['sum']
        return result

    def _search_movement_from(self, usage, product_id, company_id, type):
        result = 0
        if type == 'non inventory':
            sql = """select sum(sm.product_uom_qty) from stock_move sm
                    left join stock_location sl on sl.id = sm.location_id
                    where sm.product_id = %s 
                    and sm.state = 'done' and (sm.date  >= '%s' and sm.date <= '%s' ) 
                    and sl.usage = '%s' and sl.comment is null and sm.inventory_id is null 
                    and sm.company_id = %s """ % (product_id,self.date_start,str(self.date_end)+' 23:59:59',usage,company_id.id)
            self._cr.execute(sql)
            res = self._cr.dictfetchone()
            if res['sum'] :
                result = res['sum']
            # movements = self.env['stock.move.line'].sudo().search([('product_id','=',product_id),
            #                                             ('date','>=',self.date_start),
            #                                             ('date','<=',self.date_end),
            #                                             ('state','=','done'),
            #                                             ('location_id.usage','=',usage),
            #                                             ('move_id.inventory_id','=',False),
            #                                             ('move_id.company_id','=',company_id.id)])
        elif type == 'saldo':
            sql = """select sum(sml.qty_done) from stock_move_line sml 
                    left join stock_move sm on sm.id = sml.move_id
                    left join stock_location sl on sl.id = sm.location_id
                    where sl.usage = '%s' and sl.comment is null and sml.product_id = %s 
                    and sml.state = 'done' and sm.date < '%s' """ % (usage,product_id,self.date_start)
            self._cr.execute(sql)
            res = self._cr.dictfetchone()
            if res['sum'] :
                result = res['sum']
        return result

    def _search_movement_to_scrap(self, location_dest_id, product_id, date):
        result = 0.0
        sql = """select sum(qty_done) from stock_move_line
                where location_dest_id = %s and product_id = %s 
                and state = 'done' and date <= '%s' """ % (location_dest_id.id,product_id,str(date)+' 23:59:59')
        self._cr.execute(sql)
        res = self._cr.dictfetchone()
        if res['sum'] :
            result = res['sum']
        return result

    def _search_movement_from_scrap(self, location_id, product_id, date):
        result = 0.0
        sql = """select sum(qty_done) from stock_move_line
                where location_dest_id = %s and product_id = %s 
                and state = 'done' and date <= '%s' """ % (location_id.id,product_id,str(date)+' 23:59:59')
        self._cr.execute(sql)
        res = self._cr.dictfetchone()
        if res['sum'] :
            result = res['sum']
        return result

    def _search_movement_opname(self, product_id):
        result = 0.0
        sql = """select sum(sml.qty_done) from stock_move_line sml
                left join stock_move sm on sm.id = sml.move_id
                left join stock_inventory si on si.id = sm.inventory_id
                where sml.product_id = %s and si.state = 'done' and sm.company_id = '%s' 
                and (sm.date  >= '%s' and sm.date < '%s')  """ % (product_id,self.company_id.id,self.date_start,str(self.date_end)+' 23:59:59')
        self._cr.execute(sql)
        res = self._cr.dictfetchone()
        if res['sum'] :
            result = res['sum']
        return result

    def generate_details_mutasi_asset(self):
        self._cr.execute("DELETE FROM vit_dashboard_bc_detail_asset WHERE dashboard_id = %s" , ( [(self.id)] ))
        asset = self.env['account.asset.asset']
        asset_categ = self.env['account.asset.category']
        details = self.env['vit.dashboard.bc.detail_asset']
        asset_categ_exist = asset_categ.sudo().search([('name','not ilike','building')])
        for ass in asset_categ_exist :
            asset_saldo_awal = asset.sudo().search([('date','<=',self.date_start),
                                                ('category_id','=',ass.id),('state','!=','close'),
                                                ('method_end','=',False)])
            asset_masuk = asset.sudo().search([('date','>=',self.date_start),('date','<=',self.date_end),
                                                ('category_id','=',ass.id),('state','!=','close')])
            asset_keluar = asset.sudo().search([('method_end','>=',self.date_start),('method_end','<=',self.date_end),
                                                ('category_id','=',ass.id),('state','=','close')])
            if asset_saldo_awal or asset_masuk or asset_keluar :
                if self.bc_type.name == 'Total Inventory' :
                    detail_total_obj = self.env['vit.dashboard.bc.total_mutasi']
                    detail_total_obj.create({'dashboard_id' : self.id,
                                    'code' : '',
                                    'name': ass.name,
                                    'uom' : 'Unit(s)',
                                    'saldo_awal' : len(asset_saldo_awal),
                                    'pemasukan' : len(asset_masuk),
                                    'pengeluaran' : len(asset_keluar),
                                    'penyesuaian' : 0.0,
                                    'saldo_buku' : len(asset_saldo_awal)+len(asset_masuk)-len(asset_keluar),
                                    'stock_op' : 0.0,
                                    'selisih' : 0.0})   
                else :
                    details.create({'dashboard_id' : self.id,
                                        'asset_categ_name': ass.name,
                                        'asset_categ_id' : ass.id,
                                        'uom' : 'Unit(s)',
                                        'saldo_awal' : len(asset_saldo_awal),
                                        'pemasukan' : len(asset_masuk),
                                        'pengeluaran' : len(asset_keluar),
                                        'penyesuaian' : 0.0,
                                        'saldo_buku' : len(asset_saldo_awal)+len(asset_masuk)-len(asset_keluar),
                                        'stock_op' : 0.0,
                                        'selisih' : 0.0})            

    def generate_details_wip(self):
        self._cr.execute("DELETE FROM vit_dashboard_bc_detail_wip WHERE dashboard_id = %s" , ( [(self.id)] ))
        mrp = self.env['mrp.production']
        detail_obj = self.env['vit.dashboard.bc.detail_wip']
        detail_total_obj = self.env['vit.dashboard.bc.total_mutasi']
        mrp_exist = mrp.sudo().search([('state','=','progress'),('date_planned_start','>=',self.date_start),('date_planned_start','<=',self.date_end)])
        if mrp_exist :
            for mo in mrp_exist :
                if self.bc_type.name == 'Total Inventory' :
                    # search dulu jika ada product yg sama maka di sum
                    data_exist = detail_total_obj.search([('dashboard_id','=',self.id),
                                                    ('code','=',mo.product_id.default_code),
                                                    ('name','=',mo.product_id.name)],limit=1)
                    if data_exist :
                        data_exist.pengeluaran += mo.product_qty
                    else :
                        detail_total_obj.create({'dashboard_id' : self.id,
                                        'code' : mo.product_id.default_code,
                                        'name': mo.product_id.name,
                                        'uom' : mo.product_uom_id.name,
                                        'saldo_awal' : 0.0,
                                        'pemasukan' : 0.0,
                                        'pengeluaran' : mo.product_qty,
                                        'penyesuaian' : 0.0,
                                        'saldo_buku' : 0.0,
                                        'stock_op' : 0.0,
                                        'selisih' : 0.0})
                else :
                    # search dulu jika ada product yg sama maka di sum
                    data_exist = detail_obj.search([('dashboard_id','=',self.id),
                                                    ('product_id','=',mo.product_id.id)],limit=1)
                    if data_exist :
                        data_exist.jumlah += mo.product_qty
                    else :
                        detail_obj.create({'dashboard_id' : self.id,
                                            'product_id' : mo.product_id.id,
                                            'jumlah' : mo.product_qty,
                                            'uom' : mo.product_uom_id.name,
                                            'mo' :mo.name})
                self.env.cr.commit()

    def generate_details_mutasi_by_categ(self):
        if not self.categ_id and self.bc_type_name != 'Total Inventory':
            raise UserError(_('Product category tidak ditemukan !'))
        categ_ids = self.get_child_categs(self.categ_id)
        categ_ids = str(tuple(categ_ids)).replace(',)', ')')
        self._cr.execute("DELETE FROM vit_dashboard_bc_detail_mutasi WHERE dashboard_id = %s" , ( [(self.id)] ))
        sql = """select sml.product_id
                from stock_move_line sml
                left join product_product pp on pp.id=sml.product_id
                left join product_template pt on pt.id=pp.product_tmpl_id
                left join stock_move sm on sm.id=sml.move_id
                where sm.inventory_id is null and sm.state = 'done' and sm.date between '%s' and '%s' and pt.categ_id in %s and sm.company_id=%s 
                group by sml.product_id""" % (self.date_start,str(self.date_end)+' 23:59:59',categ_ids, self.company_id.id)
        # sql = """select sm.product_id,pt.categ_id
        #         from stock_move sm
        #         left join product_product pp on pp.id=sm.product_id
        #         left join product_template pt on pt.id=pp.product_tmpl_id
        #         where sm.inventory_id is null and sm.state  = 'done' and sm.date between '%s' and '%s'
        #         group by sm.product_id,pt.categ_id""" % (self.date_start,self.date_end)        
        self._cr.execute(sql)
        res = self._cr.dictfetchall()
        if res :
            details = self.env['vit.dashboard.bc.detail_mutasi']
            product_obj = self.env['product.product']
            inv_line = self.env['stock.inventory.line']
            kemarin = timedelta(days=1)
            starting_bal = self.date_start-kemarin
            for prod in res:
                product_id = prod['product_id']
                product = product_obj.browse(product_id)
                # quantity_saldo_awal = product._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), starting_bal)
                # saldo_awal = quantity_saldo_awal[product.id]['qty_available']
                # quantity_saldo_akhir = product._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), self.date_end)
                # saldo_buku = quantity_saldo_akhir[product.id]['qty_available']
                saldo_keluar = self._search_movement_from('internal',product_id,self.company_id,'saldo')
                saldo_masuk = self._search_movement_to('internal',product_id,self.company_id,'saldo')
                saldo_awal = saldo_masuk-saldo_keluar
                # cari barang masuk dan keluar
                pemasukan = 0.0
                pengeluaran = 0.0
                penyesuaian = 0.0
                if self.bc_type_name == 'Mutasi Bahan Baku' :                    
                    pengeluaran = self._search_movement_to('production',product_id,self.company_id,'non inventory')
                    pemasukan = self._search_movement_from('supplier',product_id,self.company_id,'non inventory')
                elif self.bc_type_name == 'Mutasi Barang Jadi' :
                    pengeluaran = self._search_movement_to('customer',product_id,self.company_id,'non inventory')
                    pemasukan = self._search_movement_from('production',product_id,self.company_id,'non inventory')
                elif self.bc_type_name == 'Mutasi Mesin dan Peralatan':
                    pemasukan = self._search_movement_from('supplier',product_id,self.company_id,'non inventory')
                    # to do
                    pengeluaran = 0.0
                # stock OP
                stock_op = self._search_movement_opname(product_id)
                saldo_buku = saldo_awal+pemasukan-pengeluaran
                selisih = stock_op-saldo_buku
                if stock_op > 0.0 :
                    selisih = 0.0
                    penyesuaian = stock_op-saldo_buku

                if self.bc_type.name == 'Total Inventory' :
                    detail_total_obj = self.env['vit.dashboard.bc.total_mutasi']
                    detail_total_obj.create({'dashboard_id' : self.id,
                                    'code' : product.default_code,
                                    'name': product.name,
                                    'uom' : product.uom_id.name,
                                    'saldo_awal' : saldo_awal,
                                    'pemasukan' : pemasukan,
                                    'pengeluaran' : pengeluaran,
                                    'penyesuaian' : penyesuaian,
                                    'saldo_buku' : saldo_buku,
                                    'stock_op' : stock_op,
                                    'selisih' : 0.0})
                else :
                    details.create({'dashboard_id' : self.id,
                                        'product_id' : product_id,
                                        'uom' : product.uom_id.name,
                                        'saldo_awal' : saldo_awal,
                                        'pemasukan' : pemasukan,
                                        'pengeluaran' : pengeluaran,
                                        'penyesuaian' : penyesuaian,
                                        'saldo_buku' : saldo_buku,
                                        'stock_op' : stock_op,
                                        'selisih' : 0.0})

    def generate_details_mutasi_by_categ2(self):
        if not self.categ2_id and self.bc_type_name != 'Total Inventory':
            raise UserError(_('Product category tidak ditemukan !'))
        categ2_ids = self.get_child_categs(self.categ2_id)
        categ2_ids = str(tuple(categ2_ids)).replace(',)', ')')
        self._cr.execute("DELETE FROM vit_dashboard_bc_detail_mutasi WHERE dashboard_id = %s" , ( [(self.id)] ))
        sql = """select sml.product_id
                from stock_move_line sml
                left join product_product pp on pp.id=sml.product_id
                left join product_template pt on pt.id=pp.product_tmpl_id
                left join stock_move sm on sm.id=sml.move_id
                where sm.inventory_id is null and sm.state = 'done' and sm.date between '%s' and '%s' and pt.categ_id in %s and sm.company_id=%s 
                group by sml.product_id""" % (self.date_start,str(self.date_end)+' 23:59:59',categ2_ids, self.company_id.id)
        # sql = """select sm.product_id,pt.categ_id
        #         from stock_move sm
        #         left join product_product pp on pp.id=sm.product_id
        #         left join product_template pt on pt.id=pp.product_tmpl_id
        #         where sm.inventory_id is null and sm.state  = 'done' and sm.date between '%s' and '%s'
        #         group by sm.product_id,pt.categ_id""" % (self.date_start,self.date_end)        
        self._cr.execute(sql)
        res = self._cr.dictfetchall()
        if res :
            details = self.env['vit.dashboard.bc.detail_mutasi']
            product_obj = self.env['product.product']
            inv_line = self.env['stock.inventory.line']
            kemarin = timedelta(days=1)
            starting_bal = self.date_start-kemarin
            for prod in res:
                product_id = prod['product_id']
                product = product_obj.browse(product_id)
                # quantity_saldo_awal = product._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), starting_bal)
                # saldo_awal = quantity_saldo_awal[product.id]['qty_available']
                # quantity_saldo_akhir = product._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), self.date_end)
                # saldo_buku = quantity_saldo_akhir[product.id]['qty_available']
                saldo_keluar = self._search_movement_from('internal',product_id,self.company_id,'saldo')
                saldo_masuk = self._search_movement_to('internal',product_id,self.company_id,'saldo')
                saldo_awal = saldo_masuk-saldo_keluar
                # cari barang masuk dan keluar
                pemasukan = 0.0
                pengeluaran = 0.0
                penyesuaian = 0.0
                if self.bc_type_name == 'Mutasi Bahan Baku' :                    
                    pengeluaran = self._search_movement_to('production',product_id,self.company_id,'non inventory')
                    pemasukan = self._search_movement_from('supplier',product_id,self.company_id,'non inventory')
                elif self.bc_type_name == 'Mutasi Barang Jadi' :
                    pengeluaran = self._search_movement_to('customer',product_id,self.company_id,'non inventory')
                    pemasukan = self._search_movement_from('production',product_id,self.company_id,'non inventory')
                elif self.bc_type_name == 'Mutasi Mesin dan Peralatan':
                    pemasukan = self._search_movement_from('supplier',product_id,self.company_id,'non inventory')
                    # to do
                    pengeluaran = 0.0
                # stock OP
                stock_op = self._search_movement_opname(product_id)
                saldo_buku = saldo_awal+pemasukan-pengeluaran
                selisih = stock_op-saldo_buku
                if stock_op > 0.0 :
                    selisih = 0.0
                    penyesuaian = stock_op-saldo_buku

                if self.bc_type.name == 'Total Inventory' :
                    detail_total_obj = self.env['vit.dashboard.bc.total_mutasi']
                    detail_total_obj.create({'dashboard_id' : self.id,
                                    'code' : product.default_code,
                                    'name': product.name,
                                    'uom' : product.uom_id.name,
                                    'saldo_awal' : saldo_awal,
                                    'pemasukan' : pemasukan,
                                    'pengeluaran' : pengeluaran,
                                    'penyesuaian' : penyesuaian,
                                    'saldo_buku' : saldo_buku,
                                    'stock_op' : stock_op,
                                    'selisih' : 0.0})
                else :
                    details.create({'dashboard_id' : self.id,
                                        'product_id' : product_id,
                                        'uom' : product.uom_id.name,
                                        'saldo_awal' : saldo_awal,
                                        'pemasukan' : pemasukan,
                                        'pengeluaran' : pengeluaran,
                                        'penyesuaian' : penyesuaian,
                                        'saldo_buku' : saldo_buku,
                                        'stock_op' : stock_op,
                                        'selisih' : 0.0})

    def generate_details_mutasi_by_location(self):
        if not self.location_id :
            raise UserError(_('Lokasi scrapped tidak ditemukan !'))    
        self._cr.execute("DELETE FROM vit_dashboard_bc_detail_mutasi WHERE dashboard_id = %s" , ( [(self.id)] ))
        sql = """select product_id
                from stock_move
                where inventory_id is null and state  = 'done' and (date >= '%s' and date <='%s')
                and (location_id = %s or location_dest_id = %s)
                group by product_id""" % (self.date_start, str(self.date_end)+' 23:59:59', self.location_id.id, self.location_id.id)        
        self._cr.execute(sql)
        res = self._cr.dictfetchall()
        if res :
            details = self.env['vit.dashboard.bc.detail_mutasi']
            product_obj = self.env['product.product']
            inv_line = self.env['stock.inventory.line']
            quant = self.env['stock.quant']
            m_line = self.env['stock.quant']
            kemarin = timedelta(days=-1)
            starting_bal = self.date_start-kemarin
            for prod in res:
                saldo_buku = 0.0
                pemasukan = 0.0
                pengeluaran = 0.0
                stock_op = 0.0
                penyesuaian = 0.0
                product_id = prod['product_id']
                product = product_obj.browse(product_id)
                
                quantity_saldo_awal_masuk = self._search_movement_to_scrap(self.location_id,product_id,str(starting_bal))
                quantity_saldo_awal_keluar = self._search_movement_from_scrap(self.location_id,product_id,str(starting_bal))
                saldo_awal = quantity_saldo_awal_masuk-quantity_saldo_awal_keluar
                quantity_saldo_akhir = quant.search([('product_id','=',product_id),
                    ('location_id','=',self.location_id.id),
                    ('in_date','<=',str(self.date_end)+' 23:59:59')])
                if quantity_saldo_akhir :
                    saldo_buku = sum(quantity_saldo_akhir.mapped('quantity'))
                # cari barang masuk dan keluar
                masuk = self._search_movement_to('inventory',product_id,self.company_id)
                if masuk :
                    pemasukan = sum(masuk.mapped('qty_done'))                  
                keluar = self._search_movement_from('inventory',product_id,self.company_id)
                if keluar:
                    pengeluaran = sum(keluar.mapped('qty_done'))
                # stock OP
                op = inv_line.sudo().search([('product_id','=',product_id),
                                                ('inventory_id.state','=','done'),
                                                ('company_id','=',self.company_id.id),
                                                ('location_id','=',self.location_id.id),
                                                ('inventory_id.date','>=',self.date_start),
                                                ('inventory_id.date','<=',str(self.date_end)+' 23:59:59'),
                                                ])
                if op :
                    stock_op = sum(op.mapped('product_qty'))
                selisih = stock_op-saldo_buku
                if stock_op > 0.0 :
                    selisih = 0.0
                    penyesuaian = stock_op-saldo_buku

                if self.bc_type.name == 'Total Inventory' :
                    detail_total_obj = self.env['vit.dashboard.bc.total_mutasi']
                    detail_total_obj.create({'dashboard_id' : self.id,
                                    'code' : product.default_code,
                                    'name': product.name,
                                    'uom' : product.uom_id.name,
                                    'saldo_awal' : saldo_awal,
                                    'pemasukan' : pemasukan,
                                    'pengeluaran' : pengeluaran,
                                    'penyesuaian' : penyesuaian,
                                    'saldo_buku' : saldo_buku,
                                    'stock_op' : stock_op,
                                    'selisih' : 0.0})    
                else :
                    details.create({'dashboard_id' : self.id,
                                        'product_id' : product_id,
                                        'uom' : product.uom_id.name,
                                        'saldo_awal' : saldo_awal,
                                        'pemasukan' : pemasukan,
                                        'pengeluaran' : pengeluaran,
                                        'penyesuaian' : penyesuaian,
                                        'saldo_buku' : saldo_buku,
                                        'stock_op' : stock_op,
                                        'selisih' : 0.0})
        return True

    def generate_details_total_inventory(self):
        self._cr.execute("DELETE FROM vit_dashboard_bc_total_mutasi WHERE dashboard_id = %s" , ( [(self.id)] ))
        self.generate_details_wip()
        self.generate_details_mutasi_by_categ()
        self.generate_details_mutasi_by_categ2()
        self.generate_details_mutasi_by_location()
        self.generate_details_mutasi_asset()

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': '#FFFFDB', 'font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': colors['orange'], 'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': colors['yellow'], 'font_color': '#000000'})
        wbf['header_yellow'].set_border()

        wbf['header_no'] = workbook.add_format(
            {'bold': 1, 'align': 'center', 'bg_color': '#FFFFDB', 'font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')

        wbf['footer'] = workbook.add_format({'align': 'left'})

        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()

        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right()

        wbf['title_doc'] = workbook.add_format({'bold': 1, 'align': 'left'})
        wbf['title_doc'].set_font_size(12)

        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)

        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right()

        wbf['content_float'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.00'})
        wbf['content_float'].set_right()
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right()
        wbf['content_number'].set_left()

        wbf['content_percent'] = workbook.add_format({'align': 'right', 'num_format': '0.00%'})
        wbf['content_percent'].set_right()
        wbf['content_percent'].set_left()

        wbf['total_float'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['white_orange'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()

        wbf['total_number'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['white_orange'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()

        wbf['total'] = workbook.add_format({'bold': 1, 'bg_color': colors['white_orange'], 'align': 'center'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['yellow'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()

        wbf['total_number_yellow'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['yellow'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()

        wbf['total_yellow'] = workbook.add_format({'bold': 1, 'bg_color': colors['yellow'], 'align': 'center'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format(
            {'bold': 1, 'bg_color': colors['orange'], 'align': 'right', 'num_format': '#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()

        wbf['total_number_orange'] = workbook.add_format(
            {'align': 'right', 'bg_color': colors['orange'], 'bold': 1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()

        wbf['total_orange'] = workbook.add_format({'bold': 1, 'bg_color': colors['orange'], 'align': 'center'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()

        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()

        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()

        return wbf, workbook

    @api.multi
    def action_print_details(self):
        report_name = 'Report %s'%self.name

        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        worksheet = workbook.add_worksheet(self.name)

        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 20)
        worksheet.set_column('D1:D1', 20)
        worksheet.set_column('E1:E1', 20)
        worksheet.set_column('F1:F1', 20)
        worksheet.set_column('G1:G1', 30)
        worksheet.set_column('H1:H1', 20)
        worksheet.set_column('I1:I1', 30)
        worksheet.set_column('J1:J1', 20)
        worksheet.set_column('K1:K1', 5)
        worksheet.set_column('L1:L1', 20)
        worksheet.set_column('M1:M1', 10)

        worksheet.write('A1', self.name, wbf['company'])
        worksheet.write('A3', 'Periode %s s/d %s'%('-' if not self.date_start else self.date_start, '-' if not self.date_end else self.date_end), wbf['content_datetime'])
        if self.bc_type.name in ('BC 2.3','BC 2.5','BC 2.7','BC 2.6.1','BC 2.6.2','BC 4.0','BC 4.1','Total Barang Masuk','Total Barang Keluar') :
            hasil = self.print_bc(wbf,worksheet,workbook,fp,report_name)
        elif self.bc_type.name == 'Posisi WIP':
            hasil = self.print_wip(wbf,worksheet,workbook,fp,report_name)
        elif self.bc_type.name in ('Mutasi Bahan Baku','Mutasi Barang Jadi'):
            hasil = self.print_mutasi(wbf,worksheet,workbook,fp,report_name)
        elif self.bc_type.name == 'Mutasi Mesin dan Peralatan':
            hasil = self.print_mutasi_asset(wbf,worksheet,workbook,fp,report_name)
        elif self.bc_type.name == 'Total Inventory':
            hasil = self.print_total_mutasi(wbf,worksheet,workbook,fp,report_name)
        return hasil

    def print_bc(self,wbf,worksheet,workbook,fp,report_name):
        
        worksheet.write('A4', 'No', wbf['header'])
        worksheet.write('B4', 'Jenis Dokumen', wbf['header'])
        if self.bc_type.name in ('BC 2.5','BC 4.1','BC 2.7','BC 2.6.1','Total Barang Keluar') :
            worksheet.write('A2', 'Laporan Pengeluaran Barang Per Dokumen Pabean', wbf['company'])
            worksheet.write('C4', 'Dokumen Pabean', wbf['header'])
            worksheet.write('D4', 'Tanggal Pabean', wbf['header'])
            worksheet.write('E4', 'Bukti Pengeluaran Barang', wbf['header'])
            worksheet.write('F4', 'Bukti penerimaan barang', wbf['header'])
            worksheet.write('G4', 'Pemasok / Pengirim', wbf['header'])
        else :
            worksheet.write('A2', 'Laporan Penerimaan Barang Per Dokumen Pabean', wbf['company'])
            worksheet.write('C4', 'Dokumen Pabean', wbf['header'])
            worksheet.write('D4', 'Tanggal Pabean', wbf['header'])
            worksheet.write('E4', 'Bukti Penerimaan Barang', wbf['header'])
            worksheet.write('F4', 'Bukti Penerimaan Barang', wbf['header'])
            worksheet.write('G4', 'Pembeli / Penerima', wbf['header'])
        worksheet.write('H4', 'Kode Barang', wbf['header'])
        worksheet.write('I4', 'Nama Barang', wbf['header'])
        worksheet.write('J4', 'Jumlah', wbf['header'])
        worksheet.write('K4', 'Satuan', wbf['header'])
        worksheet.write('L4', 'Nilai Barang', wbf['header'])
        worksheet.write('M4', 'Currency', wbf['header'])

        worksheet.write('A5', '', wbf['header'])
        worksheet.write('B5', '', wbf['header'])
        worksheet.write('C5', 'Nomor', wbf['header'])
        worksheet.write('D5', 'Tanggal', wbf['header'])
        worksheet.write('E5', 'Nomor', wbf['header'])
        worksheet.write('F5', 'Tanggal', wbf['header'])
        worksheet.write('G5', '', wbf['header'])
        worksheet.write('H5', '', wbf['header'])
        worksheet.write('I5', '', wbf['header'])
        worksheet.write('J5', '', wbf['header'])
        worksheet.write('K5', '', wbf['header'])
        worksheet.write('L5', '', wbf['header'])
        worksheet.write('M5', '', wbf['header'])

        row = 6
        no = 1
        for line in self.detail_ids :
            # worksheet.write('A%s' % row, line.product_id.name_get()[0][1], wbf['content'])
            worksheet.write('A%s'%row, no, wbf['content'])
            worksheet.write('B%s'%row, line.bc_type or '', wbf['content'])
            worksheet.write('C%s'%row, line.no_pabean or '', wbf['content'])
            worksheet.write('D%s'%row, str(line.tgl_pabean or '') , wbf['content_float'])
            # worksheet.write('E%s'%row, line.no_picking.id or '', wbf['content'])
            worksheet.write('E%s'%row, line.picking_number or '', wbf['content'])
            worksheet.write('F%s'%row, str(line.tgl_picking or ''), wbf['content_float'])
            worksheet.write('G%s'%row, line.partner_id.name or '', wbf['content'])
            worksheet.write('H%s'%row, line.product_id.default_code or '', wbf['content'])
            worksheet.write('I%s'%row, line.product_id.name, wbf['content'])
            worksheet.write('J%s'%row, line.jumlah or 0, wbf['content_float'])
            worksheet.write('K%s'%row, line.uom or '', wbf['content'])
            worksheet.write('L%s'%row, line.nilai or 0 , wbf['content_float'])
            worksheet.write('M%s'%row, line.currency or '', wbf['content'])
            row += 1
            no += 1
        worksheet.merge_range('A%s:I%s' % (row, row), 'Total', wbf['header_no'])
        worksheet.write('K%s' %row, '', wbf['header_no'])
        worksheet.write('M%s' %row, '', wbf['header_no'])
        worksheet.write_formula('J%s' %row, '{=subtotal(9,J5:J%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('L%s' %row, '{=subtotal(9,L5:L%s)}'%(row-1), wbf['total_float'])
        #import pdb;pdb.set_trace()
        workbook.close()
        result = base64.encodestring(fp.getvalue())
        date_string = self.date_end
        filename = '%s %s'%(report_name,date_string)
        filename += '%2Exlsx'
        self.write({'file_data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
        return {
            'name': 'BC Summary Report',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_wip(self,wbf,worksheet,workbook,fp,report_name):
        worksheet.write('A2', 'Laporan Posisi WIP', wbf['company'])
        worksheet.write('A4', 'No', wbf['header'])
        worksheet.write('B4', 'Kode Barang', wbf['header'])
        worksheet.write('C4', 'Nama Barang', wbf['header'])
        worksheet.write('D4', 'Jumlah', wbf['header'])
        worksheet.write('E4', 'Satuan', wbf['header'])
        worksheet.write('F4', 'Manufacturing Order', wbf['header'])
        row = 5
        no = 1
        for line in self.detail_wip_ids :
            # worksheet.write('A%s' % row, line.product_id.name_get()[0][1], wbf['content'])
            worksheet.write('A%s'%row, no, wbf['content'])
            worksheet.write('B%s'%row, line.product_id.default_code or '', wbf['content'])
            worksheet.write('C%s'%row, line.product_id.name or '', wbf['content'])
            worksheet.write('D%s'%row, line.jumlah , wbf['content_float'])
            worksheet.write('E%s'%row, line.uom, wbf['content'])
            worksheet.write('F%s'%row, line.mo, wbf['content_float'])
            row += 1
            no += 1
        worksheet.merge_range('A%s:C%s' % (row, row), 'Total', wbf['header_no'])
        worksheet.write('E%s' %row, '', wbf['header_no'])
        worksheet.write('F%s' %row, '', wbf['header_no'])
        worksheet.write_formula('D%s' %row, '{=subtotal(9,D5:D%s)}'%(row-1), wbf['total_float'])
        #import pdb;pdb.set_trace()
        workbook.close()
        result = base64.encodestring(fp.getvalue())
        date_string = self.date_end
        filename = '%s %s'%(report_name,date_string)
        filename += '%2Exlsx'
        self.write({'file_data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
        return {
            'name': 'Posisi WIP Report',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_mutasi(self,wbf,worksheet,workbook,fp,report_name):
        worksheet.write('A2', 'Laporan Mutasi Barang', wbf['company'])
        worksheet.write('A4', 'No', wbf['header'])
        worksheet.write('B4', 'Kode Barang', wbf['header'])
        worksheet.write('C4', 'Nama Barang', wbf['header'])
        worksheet.write('D4', 'UoM', wbf['header'])
        worksheet.write('E4', 'Saldo Awal', wbf['header'])
        worksheet.write('F4', 'Pemasukan', wbf['header'])
        worksheet.write('G4', 'Pengeluaran', wbf['header'])
        worksheet.write('H4', 'Penyesuaian', wbf['header'])
        worksheet.write('I4', 'Stock Opname', wbf['header'])
        worksheet.write('J4', 'Selisih', wbf['header'])
        row = 5
        no = 1
        for line in self.detail_mutasi_ids :
            # worksheet.write('A%s' % row, line.product_id.name_get()[0][1], wbf['content'])
            worksheet.write('A%s'%row, no, wbf['content'])
            worksheet.write('B%s'%row, line.product_id.default_code or '', wbf['content'])
            worksheet.write('C%s'%row, line.product_id.name or '', wbf['content'])
            worksheet.write('D%s'%row, line.product_id.uom_id.name , wbf['content'])
            worksheet.write('E%s'%row, line.saldo_awal, wbf['content_float'])
            worksheet.write('F%s'%row, line.pemasukan, wbf['content_float'])
            worksheet.write('G%s'%row, line.pengeluaran, wbf['content_float'])
            worksheet.write('H%s'%row, line.penyesuaian, wbf['content_float'])
            worksheet.write('I%s'%row, line.stock_op, wbf['content_float'])
            worksheet.write('J%s'%row, line.selisih, wbf['content_float'])
            row += 1
            no += 1
        worksheet.merge_range('A%s:D%s' % (row, row), 'Total', wbf['header_no'])
        worksheet.write_formula('E%s' %row, '{=subtotal(9,E5:E%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('F%s' %row, '{=subtotal(9,F5:F%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('G%s' %row, '{=subtotal(9,G5:G%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('H%s' %row, '{=subtotal(9,H5:H%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('I%s' %row, '{=subtotal(9,I5:J%s)}'%(row-1), wbf['total_float'])
        worksheet.write('J%s' %row, '', wbf['total_float'])
        #import pdb;pdb.set_trace()
        workbook.close()
        result = base64.encodestring(fp.getvalue())
        date_string = self.date_end
        filename = '%s %s'%(report_name,date_string)
        filename += '%2Exlsx'
        self.write({'file_data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
        return {
            'name': 'Laporan Mutasi Barang',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_mutasi_asset(self,wbf,worksheet,workbook,fp,report_name):
        worksheet.write('A2', 'Laporan Mutasi Mesin dan Peralatan', wbf['company'])
        worksheet.write('A4', 'No', wbf['header'])
        worksheet.write('B4', 'Category Asset', wbf['header'])
        worksheet.write('C4', 'UoM', wbf['header'])
        worksheet.write('D4', 'Saldo Awal', wbf['header'])
        worksheet.write('E4', 'Pemasukan', wbf['header'])
        worksheet.write('F4', 'Pengeluaran', wbf['header'])
        worksheet.write('G4', 'Penyesuaian', wbf['header'])
        worksheet.write('H4', 'Saldo Buku', wbf['header'])
        worksheet.write('I4', 'Stock Opname', wbf['header'])
        worksheet.write('J4', 'Selisih', wbf['header'])
        row = 5
        no = 1
        for line in self.detail_asset_ids :
            # worksheet.write('A%s' % row, line.product_id.name_get()[0][1], wbf['content'])
            worksheet.write('A%s'%row, no, wbf['content'])
            worksheet.write('B%s'%row, line.asset_categ_name or '', wbf['content'])
            worksheet.write('C%s'%row, line.uom or '', wbf['content'])
            worksheet.write('D%s'%row, line.saldo_awal , wbf['content_float'])
            worksheet.write('E%s'%row, line.pemasukan, wbf['content_float'])
            worksheet.write('F%s'%row, line.pengeluaran, wbf['content_float'])
            worksheet.write('G%s'%row, line.penyesuaian, wbf['content_float'])
            worksheet.write('H%s'%row, line.saldo_buku, wbf['content_float'])
            worksheet.write('I%s'%row, line.stock_op, wbf['content_float'])
            worksheet.write('J%s'%row, line.selisih, wbf['content_float'])
            row += 1
            no += 1
        worksheet.merge_range('A%s:C%s' % (row, row), 'Total', wbf['header_no'])
        worksheet.write_formula('D%s' %row, '{=subtotal(9,D5:D%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('E%s' %row, '{=subtotal(9,E5:E%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('F%s' %row, '{=subtotal(9,F5:F%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('G%s' %row, '{=subtotal(9,G5:G%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('H%s' %row, '{=subtotal(9,H5:H%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('I%s' %row, '{=subtotal(9,I5:I%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('J%s' %row, '{=subtotal(9,J5:J%s)}'%(row-1), wbf['total_float'])
        #import pdb;pdb.set_trace()
        workbook.close()
        result = base64.encodestring(fp.getvalue())
        date_string = self.date_end
        filename = '%s %s'%(report_name,date_string)
        filename += '%2Exlsx'
        self.write({'file_data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
        return {
            'name': 'Laporan Mutasi Mesin dan Peralatan',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_total_mutasi(self,wbf,worksheet,workbook,fp,report_name):
        worksheet.write('A2', 'Laporan Total Mutasi Barang', wbf['company'])
        worksheet.write('A4', 'No', wbf['header'])
        worksheet.write('B4', 'Kode', wbf['header'])
        worksheet.write('C4', 'Nama', wbf['header'])
        worksheet.write('D4', 'UoM', wbf['header'])
        worksheet.write('E4', 'Saldo Awal', wbf['header'])
        worksheet.write('F4', 'Pemasukan', wbf['header'])
        worksheet.write('G4', 'Pengeluaran', wbf['header'])
        worksheet.write('H4', 'Penyesuaian', wbf['header'])
        worksheet.write('I4', 'Stock Opname', wbf['header'])
        worksheet.write('J4', 'Selisih', wbf['header'])
        row = 5
        no = 1
        for line in self.total_mutasi_ids :
            # worksheet.write('A%s' % row, line.product_id.name_get()[0][1], wbf['content'])
            worksheet.write('A%s'%row, no, wbf['content'])
            worksheet.write('B%s'%row, line.code or '', wbf['content'])
            worksheet.write('C%s'%row, line.name or '', wbf['content'])
            worksheet.write('D%s'%row, line.uom , wbf['content'])
            worksheet.write('E%s'%row, line.saldo_awal, wbf['content_float'])
            worksheet.write('F%s'%row, line.pemasukan, wbf['content_float'])
            worksheet.write('G%s'%row, line.pengeluaran, wbf['content_float'])
            worksheet.write('H%s'%row, line.penyesuaian, wbf['content_float'])
            worksheet.write('I%s'%row, line.stock_op, wbf['content_float'])
            worksheet.write('J%s'%row, line.selisih, wbf['content_float'])
            row += 1
            no += 1
        worksheet.merge_range('A%s:D%s' % (row, row), 'Total', wbf['header_no'])
        worksheet.write_formula('E%s' %row, '{=subtotal(9,E5:E%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('F%s' %row, '{=subtotal(9,F5:F%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('G%s' %row, '{=subtotal(9,G5:G%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('H%s' %row, '{=subtotal(9,H5:H%s)}'%(row-1), wbf['total_float'])
        worksheet.write_formula('I%s' %row, '{=subtotal(9,I5:J%s)}'%(row-1), wbf['total_float'])
        worksheet.write('J%s' %row, '', wbf['total_float'])
        #import pdb;pdb.set_trace()
        workbook.close()
        result = base64.encodestring(fp.getvalue())
        date_string = self.date_end
        filename = '%s %s'%(report_name,date_string)
        filename += '%2Exlsx'
        self.write({'file_data':result})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=file_data&download=true&filename="+filename
        return {
            'name': 'Laporan Total Inventory Barang',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

DashboardBC()


class DetailDashboardBC(models.Model):
    _name = 'vit.dashboard.bc.detail'
    _description = "Report BC Details"
    _order = "sequence asc, id asc"

    @api.depends('no_picking','dashboard_id','product_id')
    def _get_history(self):
        for bc in self :
            #if bc.dashboard_id.bc_type.name in ('BC 2.5','BC 4.1','BC 2.7','BC 2.6.1','BC 2.6.2'):
            if bc.no_picking.picking_type_id.code == 'outgoing' or bc.no_picking.subcontract_id:
                production = self.env['mrp.production']
                mrp_exist = production.sudo().search([('product_id','=',bc.product_id.id),
                                                    ('state','=','done')],order='date_finished desc', limit=1)
                if mrp_exist :
                    wire = mrp_exist.move_raw_ids.filtered(lambda i:i.product_id.product_tmpl_id.categ_id.name in ['WIRE','Wire','wire'] )
                    if wire :
                        bc.product_material_id = wire.product_id.id
                        bc.stock_move_line = wire.move_line_ids.ids

    sequence = fields.Integer("Sequence")
    group_document_report = fields.Char('Group Document Report')
    dashboard_id = fields.Many2one('vit.dashboard.bc','Dashboard')
    name = fields.Char('Nama Barang')
    bc_type = fields.Char('Jenis Dokumen',size=10)
    no_pabean = fields.Char('Nomor Pabean')
    tgl_pabean = fields.Date('Tanggal Pabean')
    no_picking = fields.Many2one('stock.picking','Nomor Picking')
    move_id = fields.Many2one('stock.move','Move Name', copy=False)
    picking_number = fields.Char('Nomor Document Int')
    picking_number_ext = fields.Char('Nomor Document Ext')
    tgl_picking = fields.Date('Tanggal Picking')
    partner_id = fields.Many2one('res.partner','Partner')
    product_id = fields.Many2one('product.product','Product')
    jumlah = fields.Float('Jumlah')
    uom = fields.Char('UoM')
    nilai = fields.Float('Nilai')
    currency = fields.Char('Currency')
    product_material_id = fields.Many2one('product.product','Material',compute="_get_history", store=True)
    stock_move_line = fields.Many2many('stock.move.line','stock_move_line_bc_rel','bc_line_id','move_line_id',compute="_get_history",store=True)
                    
DetailDashboardBC()


class DetailDashboardBCWIP(models.Model):
    _name = 'vit.dashboard.bc.detail_wip'

    dashboard_id = fields.Many2one('vit.dashboard.bc','Dashboard')
    product_id = fields.Many2one('product.product','Product')
    jumlah = fields.Float('Jumlah')
    uom = fields.Char('UoM')
    mo = fields.Char('Manufacturing Order')
                    
DetailDashboardBCWIP()


class DetailDashboardBCMutasi(models.Model):
    _name = 'vit.dashboard.bc.detail_mutasi'

    dashboard_id = fields.Many2one('vit.dashboard.bc','Dashboard')
    product_id = fields.Many2one('product.product','Product')
    saldo_awal = fields.Float('Saldo Awal')
    uom = fields.Char('UoM')
    pemasukan = fields.Float('Pemasukan')
    pengeluaran = fields.Float('Pengeluaran')
    penyesuaian = fields.Float('Penyesuaian')
    saldo_buku = fields.Float('Saldo Buku')
    stock_op = fields.Float('Stock Opname')
    selisih = fields.Float('Selisih')
                    
DetailDashboardBCMutasi()


class DetailDashboardBCAsset(models.Model):
    _name = 'vit.dashboard.bc.detail_asset'

    dashboard_id = fields.Many2one('vit.dashboard.bc','Dashboard')
    asset_categ_id = fields.Many2one('account.asset.category','Asset Category')
    asset_categ_name = fields.Char('Asset Category Name')
    saldo_awal = fields.Float('Saldo Awal')
    uom = fields.Char('UoM')
    pemasukan = fields.Float('Pemasukan')
    pengeluaran = fields.Float('Pengeluaran')
    penyesuaian = fields.Float('Penyesuaian')
    saldo_buku = fields.Float('Saldo Buku')
    stock_op = fields.Float('Stock Opname')
    selisih = fields.Float('Selisih')
                    
DetailDashboardBCAsset()


class DetailDashboardBCTotal(models.Model):
    _name = 'vit.dashboard.bc.total_mutasi'

    dashboard_id = fields.Many2one('vit.dashboard.bc','Dashboard')
    code = fields.Char('Code')
    name = fields.Char('Name')
    saldo_awal = fields.Float('Saldo Awal')
    uom = fields.Char('UoM')
    pemasukan = fields.Float('Pemasukan')
    pengeluaran = fields.Float('Pengeluaran')
    penyesuaian = fields.Float('Penyesuaian')
    saldo_buku = fields.Float('Saldo Buku')
    stock_op = fields.Float('Stock Opname')
    selisih = fields.Float('Selisih')
                    
DetailDashboardBCTotal()


class VitExportImportBc(models.Model):
    _inherit = "vit.export.import.bc"
    _order = "sequence asc"

    color = fields.Integer('Color')
    sequence = fields.Integer('Sequence', default=10)
    name = fields.Selection(selection_add=[('Posisi WIP','Posisi WIP'),('Mutasi Bahan Baku','Mutasi Bahan Baku'),('Mutasi Barang Jadi','Mutasi Barang Jadi'),('Mutasi Mesin dan Peralatan','Mutasi Mesin dan Peralatan'),('Mutasi Barang Reject dan Scrap','Mutasi Barang Reject dan Scrap'),('Total Barang Masuk','Total Barang Masuk'),('Total Barang Keluar','Total Barang Keluar'),('Total Inventory','Total Inventory')])
    category_report = fields.Selection([('Laporan Barang Masuk','Laporan Barang Masuk'),('Laporan Barang Keluar','Laporan Barang Keluar'),('Laporan Inventory','Laporan Inventory')],string='Category Report', copy=False)

    _sql_constraints = [
        ('unique_name', 'unique(name,category_report)', 'Jenis BC dan kategori report yang sama sudah ada, silahkan cek kembali.'),
    ]

    def _get_action(self, action_xmlid):
        # TDE TODO check to have one view + custo in methods
        action = self.env.ref(action_xmlid).read()[0]
        if self:
            action['display_name'] = self.display_name
        return action

    def get_bc_type(self):
        return self._get_action('vit_bc_dashboard.vit_dashboard_action')

VitExportImportBc()


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    bc_type = fields.Selection([('BC 2.3','BC 2.3'),('BC 2.5','BC 2.5'),('BC 2.6.1','BC 2.6.1'),('BC 2.6.2','BC 2.6.2'),('BC 2.7','BC 2.7'),('BC 3.0','BC 3.0'),('BC 4.0','BC 4.0'),('BC 4.1','BC 4.1')],string='Jenis BC', track_visibility='on_change')

StockPicking()


class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    purchase_order_ids = fields.Many2many('purchase.order', string="Purchase Orders", related='lot_id.purchase_order_ids')

StockMoveLine()

class ProductCategory(models.Model):
    _inherit = 'product.category'

    categ_ids = fields.One2many('product.category', 'parent_id', string='Locations')

ProductCategory()