from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta


class ProductCategory(models.Model):
    _inherit = 'product.category'

    default_location_id = fields.Many2one('stock.location', 'Location for Purchase', company_dependent=True)

ProductCategory()


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.model
    def create(self, vals):
        res = super(StockMove, self).create(vals)
        if res.picking_type_id.code == 'incoming' and res.picking_type_id.default_location_dest_id.usage == 'internal':
            if res.product_id.categ_id.default_location_id :
                res.location_dest_id = res.product_id.categ_id.default_location_id.id
        return res

StockMove()