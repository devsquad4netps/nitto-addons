from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
from odoo.exceptions import UserError, ValidationError
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
from odoo.addons import decimal_precision as dp

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def _get_terms(self):
        terms = 'IMPORTANT : ALL ITEMS MUST COMPLY WITH ROHS/SOC REQUIREMENTS ! \n A. MSDS \t B. ICP/Data/XRF \t C. Rohs/Soc Process Requirement \t D. Special Packaging \t E. None'
        return terms

    shipment = fields.Char('Shipment')
    notes = fields.Text('Terms and Conditions',default=_get_terms)

    @api.depends('order_line.price_total','order_line.user_cancel')
    def _amount_all(self):
        res = super(PurchaseOrder,self)._amount_all()
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line.filtered(lambda i:not i.user_cancel):
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
            })
        return res

    @api.multi
    def action_create_picking(self):
        for po in self :
            po._create_picking()
        return True

PurchaseOrder()


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    user_cancel = fields.Many2one('res.users','User Cancel',copy=False)
    cancel_date = fields.Datetime('Cancel Date',copy=False)

    @api.multi
    def action_cancel_picking(self):
        for line in self :
        	move_to_cancel = line.move_ids.filtered(lambda p:p.state not in  ('done','cancel'))
        	if move_to_cancel :
        		move_to_cancel._action_cancel()
        		line.write({'cancel_date': fields.Datetime.now(), 'user_cancel': line.env.user.id})
        	else :
        		raise UserError(_('Tidak ada move yang bisa di cancel.'))

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = super(PurchaseOrderLine, self).onchange_product_id()
        fpos = self.order_id.fiscal_position_id
        if fpos and fpos.tax_ids and self.product_id:
            self.taxes_id = False
            self.taxes_id = fpos.tax_ids.mapped('tax_src_id').ids
        return res


PurchaseOrderLine()