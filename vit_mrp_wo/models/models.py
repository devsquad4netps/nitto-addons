from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError,Warning
from odoo.tools import float_compare, float_round
from odoo.addons import decimal_precision as dp


class MrpWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    qty_produced_real = fields.Float(
        'Real Qty Produced', default=0,
        digits=(14,0))
    workorder_history_ids = fields.One2many('mrp.workorder.history','workorder_id','History Produce')

    @api.multi
    def record_production(self):
        rounding = self.production_id.product_uom_id.rounding
        real_qty_outstanding = float_round(self.production_id.product_qty - self.qty_produced, precision_rounding=rounding)
        if real_qty_outstanding < 0 :
            real_qty_outstanding = 0
        new_produce = self.qty_producing
        previous_wo = self.env['mrp.workorder'].sudo().search([('next_work_order_id','=',self.id)],limit=1)
        # create history
        self.env['mrp.workorder.history'].create({'name':new_produce,'workorder_id':self.id})
        total_produce = sum(self.workorder_history_ids.mapped('name'))
        self.qty_produced_real = total_produce
        if self.qty_producing == 0:
            self.qty_producing = real_qty_outstanding
        elif self.qty_producing > 0:
            # jika total produce wo sebelumnya lebih kecil dr wo saat ini maka dianggap full
            if previous_wo and previous_wo.qty_produced_real <= self.qty_produced_real:
                self.qty_producing = real_qty_outstanding

        res = super(MrpWorkorder, self).record_production()
        
        if previous_wo :
            if previous_wo.state != 'done':
                routings = self.production_id.workorder_ids.sorted('id')
                rout = ''
                disini = ''
                no = 1
                for r in routings:
                    state = dict(self._fields['state'].selection).get(r.state)
                    if r.id == previous_wo.id :
                        disini = ' <----'
                    rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
                    disini = ''
                    no += 1
                raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
            # write qty_producing sesuai dg real produce wo sebelumnya
            # self.qty_producing = previous_wo.qty_produced_real - total_produce
            qty_producing = previous_wo.qty_produced_real - total_produce
            if qty_producing < 0:
                qty_producing = 0
            sql = "update mrp_workorder set qty_producing=%s where id = %s " % ( qty_producing,self.id) 
            self.env.cr.execute(sql)
        if self.state == 'done' :
            # update current qty producing wo wo selanjutnya
            next_wos = self.env['mrp.workorder'].sudo().search([('production_id','=',self.production_id.id),
                                                                ('id','>',self.id),('state','!=','done')])
            for wos in next_wos :
                if total_produce < 0 :
                    wos.qty_producing = 0
                else:
                    wos.qty_producing = total_produce
        # update done qty barang jadi di moveline
        if not self.next_work_order_id and self.state == 'done' :
            move = self.env['stock.move'].sudo().search([('group_id','=',self.production_id.procurement_group_id.id),
                                                            ('location_id.usage','=','production')],limit=1)
            if move :
                if move.state != 'assigned' :
                    move.picking_id.action_assign()
                for line in move.move_line_ids :
                    line.qty_done = total_produce
                    line.lot_id = self.final_lot_id.id
                    if not line.picking_id :
                        line.picking_id = move.picking_id.id
                    # else :
                    #     line.unlink()
        return res

    @api.multi
    def button_finish(self):
        self.ensure_one()
        
        rounding = self.production_id.product_uom_id.rounding
        real_qty_outstanding = float_round(self.production_id.product_qty - self.qty_produced, precision_rounding=rounding)
        if real_qty_outstanding < 0 :
            real_qty_outstanding = 0
        new_produce = self.qty_producing
        previous_wo = self.env['mrp.workorder'].sudo().search([('next_work_order_id','=',self.id)],limit=1)
        # create history
        self.env['mrp.workorder.history'].create({'name':new_produce,'workorder_id':self.id})
        total_produce = sum(self.workorder_history_ids.mapped('name'))
        self.qty_produced_real = total_produce
        if self.qty_producing == 0:
            self.qty_producing = real_qty_outstanding
        elif self.qty_producing > 0:
            # jika total produce wo sebelumnya lebih kecil dr wo saat ini maka dianggap full
            if previous_wo and previous_wo.qty_produced_real <= self.qty_produced_real:
                self.qty_producing = real_qty_outstanding

        res = super(MrpWorkorder, self).button_finish()
        if not self.production_id.is_locked :
            self.production_id.action_toggle_is_locked()
        if previous_wo :
            if previous_wo.state != 'done':
                routings = self.production_id.workorder_ids.sorted('id')
                rout = ''
                disini = ''
                no = 1
                for r in routings:
                    state = dict(self._fields['state'].selection).get(r.state)
                    if r.id == previous_wo.id :
                        disini = ' <----'
                    rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
                    disini = ''
                    no += 1
                raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
            # write qty_producing sesuai dg real produce wo sebelumnya
            # self.qty_producing = previous_wo.qty_produced_real - total_produce
            qty_producing = previous_wo.qty_produced_real - total_produce
            if qty_producing < 0:
                qty_producing = 0
            sql = "update mrp_workorder set qty_producing=%s where id = %s " % ( qty_producing,self.id) 
            self.env.cr.execute(sql)
        if self.state == 'done' :
            # update current qty producing wo wo selanjutnya
            next_wos = self.env['mrp.workorder'].sudo().search([('production_id','=',self.production_id.id),
                                                                ('id','>',self.id),('state','!=','done')])
            for wos in next_wos :
                if total_produce < 0 :
                    wos.qty_producing = 0
                else:
                    wos.qty_producing = total_produce
        # update done qty barang jadi di moveline
        if not self.next_work_order_id and self.state == 'done' :
            move = self.env['stock.move'].sudo().search([('group_id','=',self.production_id.procurement_group_id.id),
                                                            ('location_id.usage','=','production')],limit=1)
            #import pdb;pdb.set_trace()
            if move :
                if move.state != 'assigned' :
                    move.picking_id.action_assign()
                    if not move.picking_id.is_locked :
                        move.picking_id.action_toggle_is_locked()
                for line in move.move_line_ids :
                    # if line.product_uom_qty != 0.0 :
                    line.qty_done = total_produce
                    line.lot_id = self.final_lot_id.id
                    if not line.picking_id :
                        line.picking_id = move.picking_id.id
                    # else :
                    #     line.unlink()
        return res

MrpWorkorder()


class MrpWorkorderHistory(models.Model):
    _name = 'mrp.workorder.history'

    name = fields.Float('Qty',digits=dp.get_precision('Product Unit of Measure'))
    workorder_id = fields.Many2one('mrp.workorder','Work Order',ondelete='cascade')

MrpWorkorderHistory()