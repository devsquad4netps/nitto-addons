{
    "name"          : "Kanban Methode",
    "version"       : "7.1",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Inventory",
    "license"       : "LGPL-3",
    "contributors"  : """
    """,
    "summary"       : "Mapping banyak DO dan Receipt dalam satu tampilan",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "stock_account",
        "stock",
        "sale",
        "sale_stock",
        "purchase",
        "purchase_stock",
        "procurement_jit",
        "vit_nitto_product",
        "vit_additional_bc",
        "vit_export_import_bc",
        "vit_do_add",
        "vit_po_add",
        "product_customer_code",
        "vit_stock_git",
        "sh_barcode_scanner",
        "stock_no_negative",
        # "vit_package_master",

    ],
    "data"          : [
        "security/ir.model.access.csv",
        "security/group.xml",
        "wizard/return_view.xml",
        "views/vit_kanban_view.xml",
        "views/vit_kanban_isj_view.xml",
        "views/bc_kanban_view.xml",
        "views/vit_kanban_report.xml",
        "views/product_view.xml",
        "views/stock_view.xml",
        "views/partner_view.xml",
        "data/data.xml",
        "report/informasi_do.xml",
        "report/picking_list.xml",
        "report/print_picking_besar.xml",
        "report/print_picking_kecil.xml",
        "report/pack_label_report.xml",
        "report/packing_list_export.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}