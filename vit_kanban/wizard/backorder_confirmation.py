# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare


class StockBackorderConfirmation(models.TransientModel):
    _inherit = 'stock.backorder.confirmation'

    @api.one
    def _process(self, cancel_backorder=False):
        res = super(StockBackorderConfirmation, self)._process(cancel_backorder=cancel_backorder)
        if not cancel_backorder:
            for pick_id in self.pick_ids:
                if pick_id.location_dest_id.usage == 'customer':
                    backorder_pick = self.env['stock.picking'].search([('backorder_id', '=', pick_id.id),('state','=','assigned')])
                    if backorder_pick:
                        backorder_pick.do_unreserve()
        return res

StockBackorderConfirmation()