from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError,ValidationError
import time, xlsxwriter, base64, pytz
from io import StringIO, BytesIO
from pytz import timezone
import pdb

class VitKanbanISJ(models.Model):
    _name           = 'vit.kanban.isj'
    _inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
    _order          = "name desc, id desc"
    _description    = 'ISJ Transfer'

    def _add_package(self, barcode):
        # import pdb;pdb.set_trace()
        #step 1 make sure order in proper state.
        if self and self.state in ["cancel","done"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state), self.state)
            raise UserError(_("You can not scan item in %s state.") %(value))
                
        #step 2 increaset product qty by 1 if product not in order line than create new order line.
        elif self:
            search_lines = False
            domain = []
            ids = self.detail_ids.mapped('product_id')
            domain = [('product_id','in', ids.ids)]
            search_lines_pack_small = self.package_ids.filtered(lambda kbn: kbn.package_id.name == barcode)
            if search_lines_pack_small :
                raise UserError(_("Package kecil %s sudah diinsert ") %(barcode))
            search_lines_pack_bag = self.package_big_ids.filtered(lambda kbn: kbn.package_id.name == barcode)
            if search_lines_pack_bag :
                raise UserError(_("Package besar %s sudah diinsert ") %(barcode))
            else:
                search_package = self.env["stock.quant.package"].search([('name','=',barcode)], limit = 1)
                if search_package:
                    #masuk ke big
                    if search_package.package_ids :
                        pack = search_package.package_ids[0].sorted('id')[0]
                        if pack and pack.last_product_id :
                            product = pack.last_product_id
                        elif not pack.last_product_id :
                            if pack.quant_ids :
                                quant = pack.quant_ids.sorted('in_date')[0]
                                product = quant.product_id

                        vals = {
                            'product_id': product.id,
                            'package_id': search_package.id,
                            'quantity': len(search_package.package_ids)
                            }
                        new_package_big_ids= self.package_big_ids.new(vals)
                        self.package_big_ids += new_package_big_ids
                     
                    else :
                        # ke kecil
                        products = self.detail_ids.mapped('product_id.id')
                        if search_package.quant_ids :
                            quant = search_package.quant_ids.sorted('in_date')[0]
                            if quant.product_id.id in products :
                                product = quant.product_id
                                lot_id = quant.lot_id.id
                                quantity = quant.quantity
                                vals = {
                                    'product_id': product.id,
                                    'package_id': search_package.id,
                                    'quantity':quantity,
                                    'lot_id':lot_id
                                        }
                   
                                new_package_ids= self.package_ids.new(vals)
                                self.package_ids += new_package_ids
                     
                else:
                    raise UserError(_("Scanned package code not exist in any package!"))                
            
                
    def on_barcode_scanned(self, barcode):
        self._add_package(barcode)

    @api.depends('detail_ids')
    def _compute_total(self):
        for kanban_isj in self:
            kanban_isj.total_lines = len(kanban.detail_ids)
            kanban_isj.total_initial_demand = sum(kanban.detail_ids.mapped('product_uom_qty'))
            kanban_isj.total_qty_done = sum(kanban.detail_ids.mapped('quantity_done'))

    name = fields.Char('Number',required=True, default='/')
    date_start = fields.Date('Date Start',
        default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=-2)).date()))
    date_end = fields.Date('Date End',
        default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user)
    partner_id = fields.Many2one('res.partner','Partner', track_visibility='onchange')
    transaction_type = fields.Selection(related='partner_id.transaction_type', string='Transaction Type', store=True)
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id, track_visibility='onchange')
    detail_ids = fields.Many2many('stock.move',string='Stock Move', copy=False)
    state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('confirm_aju','Confirm Aju'),('cancel','Cancel'),('done','Done')], string='State',default='draft', track_visibility='onchange')
    reference = fields.Char('Reference', track_visibility='onchange', copy=False)
    notes = fields.Text('Notes', track_visibility='onchange', copy=False)
    picking_code = fields.Selection([('outgoing','Outgoing'),('incoming','Incoming')],string='Picking Code', track_visibility='onchange')
    date = fields.Date('Effective Date', default=fields.Date.context_today, track_visibility='on_change')
    total_lines = fields.Integer('Total Lines',compute="_compute_total")
    total_initial_demand = fields.Integer('Total Initial Demand',compute="_compute_total")
    total_qty_done = fields.Integer('Total Qty Done',compute="_compute_total")
    package_ids = fields.One2many('vit.kanban.package', 'kanban_isj_id', string='Package Kecil', copy=False)
    package_big_ids = fields.One2many('vit.kanban.package.big', 'kanban_isj_id', string='Package Besar', copy=False)
    outgoing_name = fields.Char('Outgoing Number', compute='_get_outgoing_number')
    old_so_number = fields.Char('No Sales Order', compute='_get_old_so_number')
    detail_mapped_ids = fields.One2many('vit.kanban.isj.lines', 'kanban_isj_id', string='LInes Mapped', copy=False)
    
    _sql_constraints = [
        ('unique_name', 'unique(name,company_id)',
         'Number of ISJ must be unique per company'),
    ]

    @api.onchange('detail_ids')
    def onchange_detail_ids(self):
        seq = len(self.detail_ids)
        if seq < len(self._origin.detail_ids) :
            delete_id = self._origin.detail_ids.filtered(lambda d:d.id not in self.detail_ids.ids)
            if delete_id :
                delete_id.write({'isj_sequence':0})
        for i in self.detail_ids :
            if i.isj_sequence == 0 :
                i.isj_sequence = seq
                seq += 1

    def _get_outgoing_number(self):
        for isj in self:
            if isj.detail_ids:
                kanban = isj.detail_ids.mapped('kanban_id.name')
                if kanban :
                    isj.outgoing_name = str(kanban).replace("'",'').replace("[","").replace("]","")

    def _get_old_so_number(self):
        for lbk in self:
            if lbk.picking_code == 'outgoing' and lbk.detail_ids:
                move_exist = lbk.detail_ids.sorted('date_expected').mapped('sale_line_id.order_id.client_order_ref')
                if move_exist :
                    # order_name = str(move_exist).replace("'",'').replace("[","").replace("]","")
                    # if len(order_name) > 30 :
                    #     order_name = order_name[:30]+'...'
                    # lbk.old_so_number = order_name
                    lbk.old_so_number = str(move_exist[0])

    @api.model
    def create(self,vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('vit.kanban.isj')
        return super(VitKanbanISJ, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
            data.detail_ids.write({'isj_sequence':0})
        return super(VitKanbanISJ, self).unlink()

    def cek_pack_kecil(self):
        parent_pack_ids = self.package_big_ids.mapped('package_id')
        for x in self.package_ids:
            if x.package_id.package_baru_id and x.package_id.package_baru_id.id not in parent_pack_ids.ids :
                #x.unlink()
                #sql = "delete from vit_kanban_package where id = %s" % (x.id)
                #ganti delete jadi update(pake flag)
                sql = "update vit_kanban_package set manual_pack=true where id = %s" % (x.id)
                cr=self.env.cr
                cr.execute(sql)

    @api.multi
    def write(self, vals):
        for kbn in self :
            if 'detail_ids' in vals :
                jml_datails = len(kbn.detail_ids)
                if kbn.state == 'confirm' :
                    if jml_datails != len(vals['detail_ids'][0][2]):
                        raise UserError(_('Jika ingin hapus atau tambah lines detail harus pada status draft!'))
            # if 'package_big_ids' in vals :
            #     for big in vals['package_big_ids']:
            #         id_big = big[1]
            #         child_ids = kbn.package_ids.filtered(lambda i:i.package_id.package_baru_id.id == id_big)
            #         if child_ids :
            #             child_ids.unlink()
        res = super(VitKanbanISJ, self).write(vals)
        self.cek_pack_kecil()
        return res

    @api.multi
    def sync_big_package(self):
        self.ensure_one()
        pack_k = self.env['vit.kanban.package']
        for big in self :
            for s in big.package_big_ids :
                for det in s.package_ids.filtered(lambda i:i.id not in big.package_ids.mapped('package_id.id')) :
                    lot = det.last_lot_id
                    if not lot:
                        if det.quant_ids :
                            last_trans = det.quant_ids.sorted('in_date')[0]
                            if last_trans.lot_id:
                                lot = last_trans.lot_id
                    pack_exist = pack_k.sudo().search([('kanban_isj_id','=',big.id),
                                            ('package_id','=',det.id),
                                            ('product_id','=', det.last_product_id.id or s.product_id.id or False),
                                            ('lot_id','=',lot.id or False),
                                            ('quantity','=',det.last_quantity)],limit=1)
                    if not pack_exist :
                        pack_k.create({'kanban_isj_id'      : big.id,
                                        'package_id'    : det.id,
                                        'product_id'    : det.last_product_id.id or s.product_id.id or False,
                                        'lot_id'        : lot.id or False,
                                        'quantity'      : det.last_quantity})
            for det in big.detail_ids.filtered(lambda i:i.new_demand <= 0.0):
                det.new_demand = det.product_uom_qty

        return self.sync_package()

    @api.multi
    def unsync_package(self):
        self.ensure_one()
        for move in self.detail_ids :
            mapping_product = self.package_ids.filtered(lambda x:x.product_id.id == move.product_id.id)
            if mapping_product:
                move.move_line_ids.unlink()
        #self.package_ids.unlink()
        sql = "delete from vit_kanban_package where kanban_isj_id = %s" % self.id
        self.env.cr.execute(sql)
                # for pack in mapping_product:
                #     pack.write({'move_id':False})


    @api.multi
    def sync_package(self):
        move_line = self.env['stock.move.line']
        for move in self.detail_ids :
            move.write({'kanban_isj_id':self.id})
            mapping_product = self.package_ids.filtered(lambda x:x.product_id.id == move.product_id.id)
            if mapping_product:
                move.move_line_ids.unlink()
                for mapp in mapping_product :
                    if mapp.package_id.package_ids:
                        sql = "select sq.lot_id, sum(sq.quantity) from stock_quant sq \
                                left join stock_quant_package sqp on sqp.id=sq.package_id \
                                left join stock_quant_package sqp2 on sqp.package_baru_id = sqp2.id \
                                where sqp2.id=%s \
                                group by sq.lot_id " % (mapp.package_id.id)
                        self.env.cr.execute(sql)
                        result = self.env.cr.fetchall()
                        if result and result[0] != None :   
                            for lot_pack in result:
                                move_line.create({'move_id': move.id,
                                    'product_id': move.product_id.id,
                                    'lot_id':lot_pack[0],
                                    'package_id':mapp.package_id.id,
                                    'qty_done':lot_pack[1],
                                    'product_uom_id':mapp.product_id.uom_id.id,
                                    'location_id':move.location_id.id,
                                    'location_dest_id':move.location_dest_id.id})
                            mapp.write({'move_id':move.id})
                    else :
                        move_line.create({'move_id': move.id,
                                        'product_id': move.product_id.id,
                                        'lot_id':mapp.lot_id.id,
                                        'package_id':mapp.package_id.id,
                                        'qty_done':mapp.quantity,
                                        'product_uom_id':mapp.product_id.uom_id.id,
                                        'location_id':move.location_id.id,
                                        'location_dest_id':move.location_dest_id.id})
                        mapp.write({'move_id':move.id})

        return True

    # @api.multi
    # def sync_package(self):
    #     move_line = self.env['stock.move.line']
    #     for kbn in self.package_ids :
    #         # quant = kbn.package_id.quant_ids.sorted('in_date')[0]
    #         # quant.sudo().reserved_quantity = 0.0
    #         # quant.env.cr.commit()
    #         mapping_product = self.detail_ids.filtered(lambda x:x.product_id.id == kbn.product_id.id and x.quantity_done < x.new_demand and x.state not in ('draft','cancel','done'))
    #         t_qty_done = 0
    #         for mapp in mapping_product :
    #             mapp.write({'kanban_isj_id':self.id})
    #             mapp.move_line_ids.unlink()
    #             if (t_qty_done+kbn.quantity) > mapp.new_demand :
    #                 continue
    #             if kbn.move_id :
    #                 break
    #             if mapp.new_demand > 0.0 :
    #                 qty = mapp.new_demand
    #             elif mapp.reserved_availability > 0.0 :
    #                 qty = mapp.reserved_availability
    #             else :
    #                 qty = mapp.product_uom_qty

    #             if kbn.quantity > qty :
    #                 continue
    #             if not mapp.move_line_ids :
    #                 #if move_line.product_uom_qty >= move_line.qty_done :
    #                 move_line.create({'move_id': mapp.id,
    #                                 'product_id': mapp.product_id.id,
    #                                 'lot_id':kbn.lot_id.id,
    #                                 'package_id':kbn.package_id.id,
    #                                 'qty_done':kbn.quantity,
    #                                 'product_uom_id':mapp.product_id.uom_id.id,
    #                                 'location_id':mapp.location_id.id,
    #                                 'location_dest_id':mapp.location_dest_id.id})
    #                 t_qty_done += kbn.quantity
    #                 kbn.move_id = mapp.id
    #             else :
    #                 # if mapp.new_demand > 0.0 :
    #                 #     if mapp.new_demand <= sum(mapp.move_line_ids.mapped('qty_done')):
    #                 #         continue
    #                 # elif mapp.reserved_availability > 0.0 :
    #                 #     if mapp.reserved_availability <= sum(mapp.move_line_ids.mapped('qty_done')):
    #                 #         continue
    #                 # elif mapp.product_uom_qty > 0.0 :
    #                 #     if mapp.product_uom_qty <= sum(mapp.move_line_ids.mapped('qty_done')):
    #                 #         continue              
    #                 lanjut = False
    #                 for line in mapp.move_line_ids :
    #                     if not line.kanban_line_id and not line.package_id and not line.lot_id :
    #                         sql = "update stock_move_line set result_package_id=null, package_id=%s, lot_id=%s, qty_done=%s where id=%s" % ( kbn.package_id.id,kbn.lot_id.id,kbn.quantity,line.id)
    #                         line.env.cr.execute(sql)
    #                         line.kanban_line_id = kbn.id
    #                         #############################
    #                         # line.package_id = kbn.package_id.id
    #                         # line.lot_id = kbn.lot_id.id
    #                         # line.qty_done = kbn.quantity
    #                         kbn.move_id = mapp.id
    #                         lanjut = True
    #                         t_qty_done += kbn.quantity
    #                         break
    #                 if lanjut :
    #                     continue
    #                 if qty > sum(mapp.move_line_ids.mapped('qty_done')) :
    #                     #if move_line.product_uom_qty >= move_line.qty_done :
    #                     move_line.create({'move_id': mapp.id,
    #                                 'product_id': mapp.product_id.id,
    #                                 'lot_id':kbn.lot_id.id,
    #                                 'package_id':kbn.package_id.id,
    #                                 'qty_done':kbn.quantity,
    #                                 'product_uom_id':mapp.product_id.uom_id.id,
    #                                 'location_id':mapp.location_id.id,
    #                                 'location_dest_id':mapp.location_dest_id.id})
    #                     t_qty_done += kbn.quantity
    #                     kbn.move_id = mapp.id
    #     return True

    @api.multi
    def action_confirm(self):
        if not self.detail_ids:
            raise UserError(_('Tab details tidak boleh kosong'))
        #for move in self.detail_ids.filtered(lambda m:m.location_id.usage not in ('supplier','customer')) :
        for move in self.detail_ids:
            if move.state in ('cancel','done'):
                raise UserError(_('Product %s berstatus %s, proses tidak bisa dilanjutkan.')%(move.product_id.name,move.state))
            if move.kanban_isj_id :
                move.write({'kanban_isj_id': self.id})
                #raise UserError(_('Product %s sudah di assign di dokumen %s.')%(move.product_id.name,move.kanban_isj_id.name))
            if move.kanban_id :
                raise UserError(_('Product %s sudah di assign di dokumen Outgoing %s.')%(move.product_id.name,move.kanban_id.name))
            move.kanban_isj_id = self.id
            move._action_confirm()
            # move._action_assign()
            move.move_line_ids.unlink()
        self.state = 'confirm' 

    @api.multi
    def action_done(self):
        self.ensure_one()
        if not self.detail_ids:
            raise UserError(_('Tab details tidak boleh kosong'))
        for move in self.detail_ids:
            if move.state in ('cancel','done'):
                raise UserError(_('Product %s berstatus %s, proses tidak bisa dilanjutkan.')%(move.product_id.name,move.state))
            if move.kanban_isj_id :
                move.write({'kanban_isj_id': self.id})
                #raise UserError(_('Product %s sudah di assign di dokumen %s.')%(move.product_id.name,move.kanban_isj_id.name))
            if move.kanban_id :
                raise UserError(_('Product %s sudah di assign di dokumen Outgoing %s.')%(move.product_id.name,move.kanban_id.name))
            move.kanban_isj_id = self.id
            #move._action_confirm()
            if move.state == 'confirmed':
                if move.move_line_ids:
                    self.env.cr.execute("update stock_move set state = 'assigned' where id = %s ",(move.id,))
                else :
                    move._action_assign()
        #self.sync_package()
        self.state = 'done' 

    @api.multi
    def action_cancel(self):
        #self.state = 'cancel' kena protek
        sql = "update vit_kanban_isj set state='cancel' where id = %s " % ( self.id)
        self.env.cr.execute(sql)
        for move in self.detail_ids.filtered(lambda i:not i.kanban_id) :
            #move.sudo().kanban_isj_id = False # kena protek
            sql2 = "update stock_move set kanban_isj_id=null where id = %s " % ( move.id)
            self.env.cr.execute(sql2)
        if self.package_ids :
            if len(self.package_ids) == 1 :
                sql = "delete from vit_kanban_package where id = %s" % (self.package_ids.id)
            else :
                sql = "delete from vit_kanban_package where id in %s" % (tuple(self.package_ids.ids),)
            cr=self.env.cr
            cr.execute(sql)
        self.detail_ids._action_cancel()
        self.detail_ids.write({'state': 'draft'})

    @api.multi
    def action_reset_to_draft(self):
        for x in self.detail_ids :
            x.kanban_isj_id = False
        self.state = 'draft'

    @api.multi
    def action_mapped_details(self):
        self.detail_mapped_ids.unlink()
        mapped_lines = self.env['vit.kanban.isj.lines']
        max_lines = 0
        split = False
        for move in self.detail_ids :
            if max_lines == 6 :
                max_lines = 0
                split = True
            else :
                max_lines += 1
                split = False
            mapped_lines.create({'move_id':move.id, 'kanban_isj_id':self.id, 'is_split': split})

VitKanbanISJ()


class VitKanbanPackage(models.Model):
    _inherit = "vit.kanban.package"

    @api.onchange('product_id')
    def onchange_domain_product(self):
        domain = []
        for x in self :
            if x.kanban_isj_id :
                ids = x.kanban_isj_id.detail_ids.mapped('product_id')
                domain = {'domain': {'product_id': [('id', 'in', ids.ids)]}}
        return domain

    def action_cancel_package(self):
        self.sudo().unlink()

    @api.onchange('package_id','quantity')
    def package_id_change(self):
        #import pdb;pdb.set_trace()
        # active_form = self._context.get('params', False)
        active_form = self._context.get('default_kanban_isj_id', False)
        if active_form :
            # if 'id' in active_form :
                # kanban = self.env['vit.kanban'].search([('id','=',active_form['id'])])
            kanban_isj = self.env['vit.kanban.isj'].search([('id','=',active_form)])
            if kanban_isj:
                products = kanban_isj.detail_ids.mapped('product_id.id')
                if self.package_id.quant_ids :
                    quant = self.package_id.quant_ids.sorted('in_date')[0]
                    if quant.product_id.id in products :
                        self.product_id = quant.product_id.id
                        self.lot_id = quant.lot_id.id
                        # self.quantity = quant.quantity-quant.reserved_quantity
                        self.quantity = quant.quantity
                    else :
                        self.package_id = False
                        self.product_id = False
                        self.lot_id = False
                        self.quantity = 0.0
                        self.env.cr.commit()
                        raise ValidationError(_('Product package (%s) tidak ada di detail ISJ ini !')%(quant.product_id.name))
                # elif self.package_id :
                #     self.package_id = False
                #     self.product_id = False
                #     self.lot_id = False
                #     self.quantity = 0.0
                #     self.env.cr.commit()
                #     raise ValidationError(_('Package content not found !'))
                else :
                    if self.package_id.package_ids and self.package_id.package_ids[0].quant_ids:
                        quant = self.package_id.package_ids[0].quant_ids.sorted('in_date')[0]
                        self.product_id = quant.product_id.id
                        #self.lot_id = quant.lot_id.id or False
                        self.quantity = sum(self.package_id.package_ids.mapped('last_quantity'))




    kanban_isj_id = fields.Many2one('vit.kanban.isj', string='ISJ')
    kanban_isj_state = fields.Selection(related='kanban_isj_id.state', string="State",store=True)
    big_package_id = fields.Many2one('stock.quant.package',string="Parent",related="package_id.package_baru_id", store=True)
    manual_pack = fields.Boolean('Manual Pack',default=False, help="Manual pack, tanda jika pack ini di add manual")

VitKanbanPackage()


class VitKanbanPackageBig(models.Model):
    _inherit = "vit.kanban.package.big"

    @api.onchange('product_id','package_id')
    def onchange_domain_pack_besar(self):
        domain = []
        for x in self :
            if x.kanban_isj_id :
                ids = x.kanban_isj_id.detail_ids.mapped('product_id')
                domain = {'domain': {'package_id': [('package_ids', '!=', False)],'product_id': [('id', 'in', ids.ids)]}}
        return domain

    @api.onchange('package_id','quantity')
    def package_id_change(self):
        active_form = self._context.get('default_kanban_isj_id', False)
        if active_form :
            kanban_isj = self.env['vit.kanban.isj'].search([('id','=',active_form)])
            if kanban_isj:
                #import pdb;pdb.set_trace()
                if self.package_id.package_ids :
                    product = self.package_id.package_ids[0].sorted('id')[0]
                    if product and product.last_product_id :
                        self.product_id = product.last_product_id.id
                        #self.quantity = sum(product.package_ids.mapped('last_quantity'))
                    elif not product.last_product_id :
                        if product.quant_ids :
                            quant = product.quant_ids.sorted('in_date')[0]
                            self.product_id = quant.product_id.id
                        #self.quantity = sum(product.package_ids.mapped('last_quantity'))
                    #else :
                        # self.package_id = False
                        # self.product_id = False
                        # self.quantity = 0.0
                        # self.env.cr.commit()
                        # raise ValidationError(_('Package kecil tidak ditemukan !'))

    kanban_isj_id = fields.Many2one('vit.kanban.isj', string='ISJ')

VitKanbanPackageBig()


class VitKanbanISJLines(models.Model):
    _name           = 'vit.kanban.isj.lines'
    _description    = 'ISJ Transfer Mapped'

    kanban_isj_id = fields.Many2one('vit.kanban.isj', string='ISJ')
    move_id = fields.Many2one('stock.move', string='Move')
    is_split = fields.Boolean('Split', default=False)

VitKanbanISJLines()