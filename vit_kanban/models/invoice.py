# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class AccountInvoice(models.Model):
	_inherit = 'account.invoice'

	@api.onchange('invoice_line_ids')
	def _onchange_origin(self):
		old_origin = self.origin
		res = super(AccountInvoice, self)._onchange_origin()
		if old_origin :
			self.origin = old_origin
		return res

AccountInvoice()