from odoo.addons import decimal_precision as dp
from psycopg2 import OperationalError, Error
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import expression
from odoo.tools.float_utils import float_compare, float_is_zero
from datetime import date
from datetime import timedelta
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)
import base64
import io
try:
    import qrcode

except ImportError:
    _logger.debug('ImportError')



class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    group_id = fields.Many2one('procurement.group', related="move_id.group_id",string='Transaction', store=True)
    client_order_ref = fields.Char(related="move_id.sale_line_id.order_id.client_order_ref",string='PO Customer', store=True)
    kanban_id = fields.Many2one('vit.kanban', related="move_id.kanban_id",string='Kanban', store=True)
    kanban_isj_id = fields.Many2one('vit.kanban.isj', related="move_id.kanban_isj_id",string='Kanban ISJ', store=True)
    kanban_line_id = fields.Many2one("vit.kanban.package","Kanban Line ID")
    pack_label_produksi = fields.Char(string='Label Produksi')

StockMoveLine()


class StockMove(models.Model):
    _inherit = "stock.move"
    #_order = "product_id,picking_id"
    _order = "kanban_sequence, isj_sequence asc"

    @api.multi
    @api.depends('move_line_ids.product_qty')
    def _compute_reserved_availability(self):
        """ Fill the `availability` field on a stock move, which is the actual reserved quantity
        and is represented by the aggregated `product_qty` on the linked move lines. If the move
        is force assigned, the value will be 0.
        """
        s_ids = self.ids
        if not s_ids :
            try:
                s_ids = self._origin.ids
            except:
                pass
        result = {data['move_id'][0]: data['product_qty'] for data in 
            self.env['stock.move.line'].read_group([('move_id', 'in', s_ids)], ['move_id','product_qty'], ['move_id'])}
        for rec in self:
            new_rec = rec
            if not new_rec :
                try :
                    new_rec = self._origin
                except :
                    pass
            quantity = rec.product_id.uom_id._compute_quantity(result.get(new_rec.id, 0.0), rec.product_uom, rounding_method='HALF-UP')
            # if quantity == 0.0 and rec.state == 'assigned':
            #     # if rec.kanban_id.name[:3] == 'RTR' :
            #     #     quantity = rec.new_demand
            #     if new_rec.new_demand > 0.0 :
            #         quantity = new_rec.new_demand
            #     else :
            #         quantity = new_rec.product_uom_qty
            if new_rec.new_demand > 0.0 :
                if rec.state == 'assigned':
                    quantity = new_rec.new_demand
                elif rec.state == 'partially_available' :
                    if quantity > new_rec.new_demand:
                        quantity = new_rec.new_demand
            rec.reserved_availability = quantity

    @api.multi
    def action_cancel_move_kanban(self):
        #import pdb;pdb.set_trace()
        self.ensure_one()
        self._do_unreserve()
        quant = self.env['vit.kanban'].cek_quant(self.product_id,self.location_id)
        if quant :
            if self.new_demand > 0.0 :
                qty = self.new_demand
            else:
                qty = self.product_uom_qty
            quant.write({'reserved_quantity': quant.reserved_quantity-qty})
        self.kanban_id = False
        self.state = 'confirmed'

    @api.multi
    def action_confirm_move_kanban(self):
        self.ensure_one()
        old_demand = self.product_uom_qty
        self.product_uom_qty = self.new_demand
        self._action_assign()
        mv_state = self.state
        sql = "update stock_move set product_uom_qty=%s,product_qty=%s, state='draft' where id = %s " % ( old_demand,old_demand,self.id) 
        self.env.cr.execute(sql)
        self.env.cr.commit()
        sql2 = "update stock_move set state='%s' where id = %s " % ( mv_state,self.id)
        self.env.cr.execute(sql2)
        self.env.cr.commit()
        # reset package and lot
        sql = "update stock_move_line set package_id=null, result_package_id=null, lot_id=null, qty_done=0.0 where move_id = %s " % ( self.id) 
        self.env.cr.execute(sql)

    @api.multi
    def action_set_to_draft(self):
        if self.filtered(lambda m: m.state != 'cancel'):
            raise UserError(_("You can set to draft cancelled moves only !"))
        self.write({'state': 'draft'})

    @api.one
    def _compute_product_qty_available(self):
        qty_available = 0.0
        if self.location_id.usage == 'internal' :
            quant_ids = self.env['stock.quant'].sudo().search([('product_id','=',self.product_id.id),
                                                                ('reserved_quantity','=',0.0),
                                                                ('quantity','>',0.0),
                                                                ('location_id','=',self.location_id.id)])
            if quant_ids :
                qty_available = sum(quant_ids.mapped('quantity'))
        self.qty_available = qty_available

    @api.multi
    def write(self, vals):
        res = super(StockMove, self).write(vals)
        for move in self :
            if move.box <= 0 and len(move.move_line_ids) > 0:
                if move.kanban_isj_id or move.kanban_id:
                    move.box = len(move.move_line_ids)
        return res

    kanban_id = fields.Many2one('vit.kanban', string='Kanban', copy=False)
    kanban_sequence = fields.Integer(string='Kanban Sequence List', copy=False)
    isj_sequence = fields.Integer(string='ISJ Sequence List', copy=False)
    kanban_state = fields.Selection(string='Kanban State', related='kanban_id.state', store=True)
    kanban_isj_id = fields.Many2one('vit.kanban.isj', string='Kanban ISJ', copy=False)
    isj_state = fields.Selection(string='ISJ State', related='kanban_isj_id.state', store=True)
    new_demand = fields.Float('New Demand',digits=dp.get_precision('Product Unit of Measure'), copy=False)
    client_order_ref = fields.Char(related="sale_line_id.order_id.client_order_ref",string='PO Customer', store=True)
    product_customer = fields.Char('Product Customer Code', compute='_compute_default_customer_code',store=True)
    product_vendor = fields.Char('Product Vendor Name', compute='_compute_default_vendor_name',store=True)
    id_move_kbn = fields.Integer('ID Move Kanban',helps="Teknikal field untuk menyimpan id move kbn, supaya bisa di search ketika auto internal trf",default=0, copy=False)
    date_expected = fields.Datetime(
        'Delivery Date', default=fields.Datetime.now, index=True, required=True,
        states={'done': [('readonly', True)]},
        help="Scheduled date for the processing of this move")
    qty_available = fields.Float('On Hand',compute='_compute_product_qty_available')
    #purchase_price = fields.Float('Purchase Price',related='purchase_line_id.price_unit', digits=dp.get_precision('Product Price'),store=True)
    purchase_price = fields.Float('Purchase Price',compute='_compute_total_purchase_price',store=True)
    total_purchase_price = fields.Float('Total Purchase Price', compute='_compute_total_purchase_price',store=True)
    has_inputed_in_isj = fields.Float('Has Input ISJ', compute='_compute_has_inputed_in_isj', store=True )
    case = fields.Integer('Case', size=3, copy=False)
    show_case = fields.Boolean('Show Case', default=False, copy=False)
    carton_number = fields.Char('Carton No', copy=False)

    @api.depends('move_line_ids.qty_done')
    def _compute_has_inputed_in_isj(self):
        for line in self:
            line.has_inputed_in_isj = sum(line.move_line_ids.mapped('qty_done'))

    @api.depends('product_id','picking_id','state','kanban_id','purchase_price','quantity_done','purchase_line_id.price_unit')
    def _compute_total_purchase_price(self):
        for line in self:
            list_price = line.purchase_line_id.price_unit
            if line.product_uom.id != line.purchase_line_id.product_uom.id :
                if line.purchase_line_id.product_uom.uom_type == 'bigger':
                    list_price = list_price / line.purchase_line_id.product_uom.factor_inv
                elif line.purchase_line_id.product_uom.uom_type == 'smaller':
                    list_price = list_price * line.purchase_line_id.product_uom.factor
            line.purchase_price = list_price
            line.total_purchase_price = line.quantity_done*list_price

    @api.depends('product_id','picking_id','state','kanban_id','kanban_isj_id','product_id.product_customer_code_ids.product_code')
    def _compute_default_customer_code(self):
        for line in self:
            if line.product_id and line.product_id.product_customer_code_ids :
                code_exist = False 
                if line.kanban_id :
                    code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.kanban_id.partner_id.id)
                elif line.picking_id :
                    code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.picking_id.partner_id.id)
                if not code_exist and line.kanban_id and line.kanban_id.partner_id and line.kanban_id.partner_id.parent_id :
                    code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.kanban_id.partner_id.parent_id.id)
                if code_exist :
                    line.product_customer = code_exist[0].product_code

    @api.depends('product_id','picking_id','state','kanban_id','kanban_isj_id','product_id.seller_ids.product_name')
    def _compute_default_vendor_name(self):
        for line in self:
            if line.product_id and line.product_id.seller_ids  :
                code_exist = False 
                if line.kanban_id :
                    code_exist = line.product_id.seller_ids.filtered(lambda code:code.name.id == line.kanban_id.partner_id.id)
                elif line.picking_id :
                    code_exist = line.product_id.seller_ids.filtered(lambda code:code.name.id == line.picking_id.partner_id.id)
                if not code_exist and line.kanban_id and line.kanban_id.partner_id and line.kanban_id.partner_id.parent_id :
                    code_exist = line.product_id.seller_ids.filtered(lambda code:code.name.id == line.kanban_id.partner_id.parent_id.id)
                if code_exist and code_exist[0].product_name:
                    line.product_vendor = code_exist[0].product_name

    @api.onchange('new_demand')
    def onchange_new_demand(self):
        # if self._origin.kanban_isj_id and self.new_demand:
        if self.new_demand and self.location_id.usage == 'internal':
            if self.new_demand > self.qty_available and self.qty_available != 0.0:
                new_demand = self.new_demand
                self.new_demand = 0.0
                raise UserError(_("Qty new demand %s (%s) lebih besar dari qty available (%s) !")%(self.product_id.name,'{:,.2f}'.format(new_demand),'{:,.2f}'.format(self.qty_available)))


    # def _action_assign(self):
    #     cr = self.env.cr
    #     for move in self :
    #         if move.picking_code == 'outgoing' and move.new_demand > 0.0:
    #             old_demand = move.product_uom_qty
    #             move.product_uom_qty = move.new_demand
    #             res = super(StockMove, move)._action_assign()
    #             #move.product_uom_qty = old_demand
    #             cr.execute("update stock_move set product_uom_qty=%s where id = %s",( old_demand,move.id,))
    #         else :
    #             res = super(StockMove, move)._action_assign()
    #     return

    @api.model
    def create(self,vals):
        res = super(StockMove, self).create(vals)
        if res.location_dest_id.usage == 'customer' and not res.group_id and not res.origin :
            raise UserError(_("Tidak bisa buat stock move dari %s !")%(res.picking_id.name))
        return res

    def _action_confirm(self, merge=True, merge_into=False):
        if not self:
            return super(StockMove, self)._action_confirm(merge=merge, merge_into=merge_into)
        for move in self :
            if move.id_move_kbn != 0:
                res = super(StockMove, move)._action_confirm(merge=False, merge_into=merge_into)
            else:
                res = super(StockMove, move)._action_confirm(merge=merge, merge_into=merge_into)
        return res

StockMove()


class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.depends('write_date','state','move_ids_without_package.kanban_id')
    def _search_kanban(self):
        for pick in self:
            kanban_id = pick.move_ids_without_package.filtered(lambda p:p.kanban_id)
            kanban_isj_id = pick.move_ids_without_package.filtered(lambda p:p.kanban_isj_id)
            if kanban_id :
            	pick.kanban_id = kanban_id[0].kanban_id.id
            else :
            	pick.kanban_id = False
            if kanban_isj_id :
                pick.kanban_isj_id = kanban_isj_id[0].kanban_isj_id.id
            else :
                pick.kanban_isj_id = False

    @api.multi
    def print_label(self):
        now = fields.datetime.now()+ timedelta(hours=7)
        #printed = self.env.ref('vit_kanban.report_label_package_picking').with_context({'discard_logo_check': True}).report_action(self)
        pdf = self.env.ref('vit_kanban.report_label_package_picking').sudo().render_qweb_pdf([self.id])[0]
        name = "%s%s" % ("Label "+self.name+" | "+str(now)[:19], '.pdf')
        self.write({'data':base64.b64encode(pdf),'data_name':name})
        return True

    kanban_id = fields.Many2one('vit.kanban', string='Kanban', compute="_search_kanban",store=True, copy=False)
    kanban_isj_id = fields.Many2one('vit.kanban.isj', string='Kanban ISJ', compute="_search_kanban",store=True, copy=False)
    asal_bb = fields.Char('Asal Bahan Baku', compute='_compute_asal_bahan_baku')
    internal_kanban_id = fields.Many2one('vit.kanban', string='LBM Number', copy=False)
    is_internal_kanban = fields.Boolean('Is Internal Trf Kanban', copy=False)
    data = fields.Binary('File', readonly=True)
    data_name = fields.Char('Filename', readonly=True)
    usage = fields.Selection(related='location_id.usage',string="Source Location Type", Store=True)


    def _compute_asal_bahan_baku(self):
        for picking in self:
            if picking.origin :
                mrp_id = self.env['mrp.production'].sudo().search([('name','=',picking.origin)],limit=1)
                if mrp_id :
                    wire = mrp_id.move_raw_ids.filtered(lambda x:x.product_tmpl_id.categ_id.name == 'WIRE' or x.product_tmpl_id.categ_id.name == 'Wire')
                    if wire :
                        lot_id = wire[0].move_line_ids.filtered(lambda l:l.lot_id)
                        if lot_id and lot_id[0].purchase_order_ids:
                            vendor_id = lot_id[0].purchase_order_ids[0].partner_id
                            if vendor_id and vendor_id.transaction_type :
                                picking.asal_bb = vendor_id.transaction_type 

    @api.onchange('internal_kanban_id')
    def onchange_internal_kanban_id(self):
        if self.internal_kanban_id:
            self.partner_id = self.internal_kanban_id.partner_id.id
            pick_type = self.env['stock.picking.type'].sudo().search([('code','=','internal'),
                                                                        ('warehouse_id.company_id','=',self.company_id.id),
                                                                        ('barcode','=','PLTGR-INTERNAL')],limit=1)
            if pick_type :               
                self.picking_type_id = pick_type.id
                self.location_id = pick_type.default_location_src_id.id
                self.location_dest_id = pick_type.default_location_dest_id.id
            self.move_ids_without_package = False
            if not self.scheduled_date :
                dates = str(date.today())
            else :
                dates = self.scheduled_date
            data = []
            for lbm in self.internal_kanban_id.detail_ids :
                #import pdb;pdb.set_trace()
                data.append((0,0, {'product_id':lbm.product_id.id,
                                    'name':lbm.name,
                                    'origin': self.internal_kanban_id.name,
                                    'product_uom':lbm.product_uom.id,
                                    'product_uom_qty':lbm.product_qty,
                                    'price_unit': lbm.price_unit,
                                    'netto':0.1,
                                    'state':'draft',
                                    'picking_type_id':pick_type.id,
                                    'location_id':pick_type.default_location_src_id.id,
                                    'location_dest_id':pick_type.default_location_dest_id.id,
                                    'company_id': self.company_id.id,
                                    'date_expected':dates,
                                    }))  
            self.move_ids_without_package = data
            origin = self.internal_kanban_id.name
            if self.internal_kanban_id.no_sj_pengirim :
                origin = origin + ' | ' +self.internal_kanban_id.no_sj_pengirim
            self.origin = origin
            if not self.scheduled_date :
                self.scheduled_date = dates


    @api.multi
    def action_confirm(self):
        res = super(StockPicking, self).action_confirm()
        self.filtered(lambda picking: picking.location_id.usage == 'git' and picking.state == 'confirmed')\
            .mapped('move_lines')._action_assign()
        return res

    @api.multi
    def action_assign(self):
        """ Check availability of picking moves.
        This has the effect of changing the state and reserve quants on available moves, and may
        also impact the state of the picking as it is computed based on move's states.
        @return: True
        """
        self.filtered(lambda picking: picking.state == 'draft').action_confirm()
        moves = self.mapped('move_lines').filtered(lambda move: move.state not in ('draft', 'cancel', 'done'))
        # komen dulu
        # if not moves:
        #     raise UserError(_('Nothing to check the availability for.'))
        # If a package level is done when confirmed its location can be different than where it will be reserved.
        # So we remove the move lines created when confirmed to set quantity done to the new reserved ones.
        package_level_done = self.mapped('package_level_ids').filtered(lambda pl: pl.is_done and pl.state == 'confirmed')
        package_level_done.write({'is_done': False})
        if moves :
            moves._action_assign()
        package_level_done.write({'is_done': True})
        return True


StockPicking()


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    internal_picking_type_id = fields.Many2one(string='Operation Type for Internal (Import)',comodel_name='stock.picking.type')

StockPickingType()


class StockQuantPackage(models.Model):
    _inherit = "stock.quant.package"
    _order = "id asc"

    @api.multi
    @api.depends('name', 'last_quantity')
    def name_get(self):
        result = []
        #import pdb;pdb.set_trace()
        for res in self:
            name = res.name
            if res.last_quantity > 0.0 :
                qty_awal = int(res.last_quantity)
                qty = '{:,}'.format(qty_awal)
                name = name + ' --> '+str(qty)
            result.append((res.id, name))
        return result

    last_partner_id = fields.Many2one('res.partner','Last Partner', compute='_compute_default_customer_code',store=False)
    last_product_id = fields.Many2one('product.product','Last Product', compute='_compute_default_customer_code',store=False)
    last_lot_id = fields.Many2one('stock.production.lot','Last Lot', compute='_compute_default_customer_code',store=False)
    last_quantity = fields.Float('Last Quantity',digits=(9,0), compute='_compute_default_customer_code',store=False)
    product_customer = fields.Char('Customer Code', compute='_compute_default_customer_code',store=False)
    package_ids = fields.One2many('stock.quant.package', 'package_baru_id', string='Child Package',)
    package_baru_id = fields.Many2one(string='Package Baru', comodel_name='stock.quant.package',)
    active = fields.Boolean('Active', default=True)
    jenis_package = fields.Selection([
        ('besar', 'Package Besar'),
        ('kecil','Package Kecil')],
        compute ='_onchange_package', store=True)

    keterangan = fields.Text(string='Keterangan')
    qr_code = fields.Binary('QR Code', compute="_generate_qr_code")


    @api.one
    @api.depends('name')
    def _generate_qr_code(self):
        qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=20, border=4)
        if self.name :
            qr.add_data(self.name)
            qr.make(fit=True)
            img = qr.make_image()
            buffer = io.BytesIO()
            img.save(buffer, format="PNG")
            qrcode_img = base64.b64encode(buffer.getvalue())
            self.update({'qr_code': qrcode_img,})

    #@api.depends('location_id','quant_ids','name','write_date')
    def _compute_default_customer_code(self):
        for line in self:
            if line.quant_ids :
                last_quant = line.quant_ids.sorted('in_date')[0]
                line.last_quantity = last_quant.quantity
                line.last_product_id = last_quant.product_id.id
            domain = ['|', ('result_package_id', 'in', line.ids), ('package_id', 'in', line.ids)]
            last_move = self.env['stock.move.line'].sudo().search(domain,order='date desc',limit=1)
            partner_id = False
            if last_move and last_move.picking_id :
                if last_move.picking_id.partner_id :
                    partner_id = last_move.picking_id.partner_id.id
                    line.last_partner_id = partner_id
                elif last_move.move_id.customer_id:
                    partner_id = last_move.move_id.customer_id.partner_id.id
                    line.last_partner_id = partner_id
            if last_move and last_move.product_id and last_move.product_id.product_customer_code_ids and partner_id:#and last_move.picking_id.partner_id :                    
                code_exist = last_move.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == partner_id)
                if code_exist :
                    line.last_partner_id = partner_id
                    line.product_customer = code_exist[0].product_code
                    line.last_lot_id = last_move.lot_id.id or False


StockQuantPackage()


class StockQuant(models.Model):
    _inherit = "stock.quant"

    @api.model
    def _update_reserved_quantity(self, product_id, location_id, quantity, lot_id=None, package_id=None, owner_id=None, strict=False):
        """ Increase the reserved quantity, i.e. increase `reserved_quantity` for the set of quants
        sharing the combination of `product_id, location_id` if `strict` is set to False or sharing
        the *exact same characteristics* otherwise. Typically, this method is called when reserving
        a move or updating a reserved move line. When reserving a chained move, the strict flag
        should be enabled (to reserve exactly what was brought). When the move is MTS,it could take
        anything from the stock, so we disable the flag. When editing a move line, we naturally
        enable the flag, to reflect the reservation according to the edition.

        :return: a list of tuples (quant, quantity_reserved) showing on which quant the reservation
            was done and how much the system was able to reserve on it
        """
        self = self.sudo()
        rounding = product_id.uom_id.rounding
        quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
        reserved_quants = []
        
        if float_compare(quantity, 0, precision_rounding=rounding) > 0:
            # if we want to reserve
            available_quantity = self._get_available_quantity(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
            # if float_compare(quantity, available_quantity, precision_rounding=rounding) > 0:
            #     raise UserError(_('It is not possible to reserve more products of %s than you have in stock.') % product_id.display_name)
        elif float_compare(quantity, 0, precision_rounding=rounding) < 0:
            # if we want to unreserve
            available_quantity = sum(quants.mapped('reserved_quantity'))
            # if float_compare(abs(quantity), available_quantity, precision_rounding=rounding) > 0:               
            #     raise UserError(_('It is not possible to unreserve more products of %s than you have in stock.') % product_id.display_name)
        else:
            return reserved_quants
        #import pdb;pdb.set_trace()
        for quant in quants:
            if float_compare(quantity, 0, precision_rounding=rounding) > 0:
                max_quantity_on_quant = quant.quantity - quant.reserved_quantity
                if float_compare(max_quantity_on_quant, 0, precision_rounding=rounding) <= 0:
                    continue
                max_quantity_on_quant = min(max_quantity_on_quant, quantity)
                quant.reserved_quantity += max_quantity_on_quant
                reserved_quants.append((quant, max_quantity_on_quant))
                quantity -= max_quantity_on_quant
                available_quantity -= max_quantity_on_quant
            else:
                max_quantity_on_quant = min(quant.reserved_quantity, abs(quantity))
                quant.reserved_quantity -= max_quantity_on_quant
                reserved_quants.append((quant, -max_quantity_on_quant))
                quantity += max_quantity_on_quant
                available_quantity += max_quantity_on_quant

            if float_is_zero(quantity, precision_rounding=rounding) or float_is_zero(available_quantity, precision_rounding=rounding):
                break
        return reserved_quants

StockQuant()


class StockLocation(models.Model):
    _inherit = 'stock.location'

    def should_bypass_reservation(self):
        self.ensure_one()
        return self.usage in ('supplier', 'customer', 'production','git') or self.scrap_location or self.allow_negative_stock

    alias_name = fields.Char('Alias Name',size=200, help='Hanya untuk kebutuhan print out report')

StockLocation()


class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    bahan_baku_asal = fields.Char('Bahan Baku Asal', compute='_compute_bahan_baku_asal',store=True)

    @api.one
    @api.depends('name')
    def _compute_bahan_baku_asal(self):
        for line in self:
            if line.name :
                mo_exist = self.env['mrp.production'].sudo().search([('name','=',line.name)],limit=1)
                if mo_exist :
                    rawmate = mo_exist.move_raw_ids.filtered(lambda raw:raw.product_id.product_tmpl_id.categ_id.name == 'Wire')
                    if rawmate and rawmate[0].move_line_ids :
                        #ambil lot line pertama
                        lot_bb = rawmate[0].move_line_ids[0].lot_id
                        if lot_bb and lot_bb.purchase_order_ids:
                            line.bahan_baku_asal = lot_bb.purchase_order_ids.sorted('date_order')[0].purchase_type

StockProductionLot()