# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class ResPartner(models.Model):
    _inherit = 'res.partner'

    bill_type = fields.Selection([('based_on_pricelist','Based on Pricelist'),('based_on_purchase_order','Based on Purchase Order')],string="Bill Type",default='based_on_purchase_order', track_visibility='on_change')
    invoice_type = fields.Selection([('based_on_pricelist','Based on Pricelist'),('based_on_sales_order','Based on Sales Order')],string="Invoice Type",default='based_on_sales_order', track_visibility='on_change')

ResPartner()