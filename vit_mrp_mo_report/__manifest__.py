{
    'name': "Cetak Kartu vs Prod Heading",
    'version': '0.1',
    'sequence': 99,
    'author': '',
    'category': 'Manufacturing',
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'report/mo_report.xml',
        'wizard/mo_report_wizard.xml'
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}
