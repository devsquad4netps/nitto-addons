from odoo import models, fields, api
import datetime


class MOReportWizard(models.TransientModel):
    _name = "vit.mo.report.wizard"
    _description = "Cetak Kartu vs Prod Heading Wizard"

    wo_heading_month = fields.Selection([
        ('1', 'Januari'),
        ('2', 'Februari'),
        ('3', 'Maret'),
        ('4', 'April'),
        ('5', 'Mei'),
        ('6', 'Jun'),
        ('7', 'Jul'),
        ('8', 'Agustus'),
        ('9', 'September'),
        ('10', 'Oktober'),
        ('11', 'November'),
        ('12', 'Desember')
    ], 'WO Heading Month', default='1', required=True)

    wo_heading_year = fields.Selection([(num, str(num)) for num in range(2000, datetime.datetime.now().year + 1)],
                                       'WO Heading Year', required=True, default=datetime.datetime.now().year)
    company_id = fields.Many2one('res.company', required=True)

    @api.multi
    def button_export_pdf(self):
        self.ensure_one()
        return self._export('qweb-pdf')

    def _export(self, report_type):
        model = self.env['vit.mo.report']
        report = model.create({
            'wo_heading_month': int(self.wo_heading_month),
            'wo_heading_year': int(self.wo_heading_year),
            'company_id': self.company_id.id,
        })

        return report.print_report(report_type)
