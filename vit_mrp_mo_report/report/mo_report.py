from odoo import models, fields, api


class MOReportLine(models.TransientModel):
    _name = 'vit.mo.report.line'
    _description = 'MO Report Line'

    mo_product_code = fields.Char()
    mo_product_name = fields.Char()
    code = fields.Char()
    jml = fields.Integer()
    ctkkrt = fields.Integer()
    tohead = fields.Integer()
    tgl1 = fields.Integer()
    tgl2 = fields.Integer()
    tgl3 = fields.Integer()
    tgl4 = fields.Integer()
    tgl5 = fields.Integer()
    tgl6 = fields.Integer()
    tgl7 = fields.Integer()
    tgl8 = fields.Integer()
    tgl9 = fields.Integer()
    tgl10 = fields.Integer()
    tgl11 = fields.Integer()
    tgl12 = fields.Integer()
    tgl13 = fields.Integer()
    tgl14 = fields.Integer()
    tgl15 = fields.Integer()
    tgl16 = fields.Integer()
    tgl17 = fields.Integer()
    tgl18 = fields.Integer()
    tgl19 = fields.Integer()
    tgl20 = fields.Integer()
    tgl21 = fields.Integer()
    tgl22 = fields.Integer()
    tgl23 = fields.Integer()
    tgl24 = fields.Integer()
    tgl25 = fields.Integer()
    tgl26 = fields.Integer()
    tgl27 = fields.Integer()
    tgl28 = fields.Integer()
    tgl29 = fields.Integer()
    tgl30 = fields.Integer()
    tgl31 = fields.Integer()


class MOReport(models.TransientModel):
    _name = "vit.mo.report"
    _description = 'MO Report'

    wo_heading_month = fields.Integer()
    wo_heading_year = fields.Integer()
    company_id = fields.Many2one('res.company')

    lines = fields.Many2many(
        comodel_name='vit.mo.report.line',
        compute='_compute_results'
    )

    sum_jml = fields.Integer()
    sum_ctkkrt = fields.Integer()
    sum_tohead = fields.Integer()
    sum_tgl1 = fields.Integer()
    sum_tgl2 = fields.Integer()
    sum_tgl3 = fields.Integer()
    sum_tgl4 = fields.Integer()
    sum_tgl5 = fields.Integer()
    sum_tgl6 = fields.Integer()
    sum_tgl7 = fields.Integer()
    sum_tgl8 = fields.Integer()
    sum_tgl9 = fields.Integer()
    sum_tgl10 = fields.Integer()
    sum_tgl11 = fields.Integer()
    sum_tgl12 = fields.Integer()
    sum_tgl13 = fields.Integer()
    sum_tgl14 = fields.Integer()
    sum_tgl15 = fields.Integer()
    sum_tgl16 = fields.Integer()
    sum_tgl17 = fields.Integer()
    sum_tgl18 = fields.Integer()
    sum_tgl19 = fields.Integer()
    sum_tgl20 = fields.Integer()
    sum_tgl21 = fields.Integer()
    sum_tgl22 = fields.Integer()
    sum_tgl23 = fields.Integer()
    sum_tgl24 = fields.Integer()
    sum_tgl25 = fields.Integer()
    sum_tgl26 = fields.Integer()
    sum_tgl27 = fields.Integer()
    sum_tgl28 = fields.Integer()
    sum_tgl29 = fields.Integer()
    sum_tgl30 = fields.Integer()
    sum_tgl31 = fields.Integer()

    @api.multi
    def _compute_results(self):
        self.ensure_one()
        query = """
            select * from fn_mrp_kartu_vs_heading(%s, %s, %s)
        """ % (self.wo_heading_month, self.wo_heading_year, self.company_id.id)
        self._cr.execute(query)
        lines = self._cr.dictfetchall()
        report_line = self.env['vit.mo.report.line']
        self.lines = [report_line.new(line).id for line in lines]
        for line in lines:
            self.sum_jml += line.get('jml') if line.get('jml') else 0
            self.sum_ctkkrt += line.get('ctkkrt') if line.get('ctkkrt') else 0
            self.sum_tohead += line.get('tohead') if line.get('tohead') else 0
            self.sum_tgl1 += line.get('tgl1') if line.get('tgl1') else 0
            self.sum_tgl2 += line.get('tgl2') if line.get('tgl2') else 0
            self.sum_tgl3 += line.get('tgl3') if line.get('tgl3') else 0
            self.sum_tgl4 += line.get('tgl4') if line.get('tgl4') else 0
            self.sum_tgl5 += line.get('tgl5') if line.get('tgl5') else 0
            self.sum_tgl6 += line.get('tgl6') if line.get('tgl6') else 0
            self.sum_tgl7 += line.get('tgl7') if line.get('tgl7') else 0
            self.sum_tgl8 += line.get('tgl8') if line.get('tgl8') else 0
            self.sum_tgl9 += line.get('tgl9') if line.get('tgl9') else 0
            self.sum_tgl10 += line.get('tgl10') if line.get('tgl10') else 0
            self.sum_tgl11 += line.get('tgl11') if line.get('tgl11') else 0
            self.sum_tgl12 += line.get('tgl12') if line.get('tgl12') else 0
            self.sum_tgl13 += line.get('tgl13') if line.get('tgl13') else 0
            self.sum_tgl14 += line.get('tgl14') if line.get('tgl14') else 0
            self.sum_tgl15 += line.get('tgl15') if line.get('tgl15') else 0
            self.sum_tgl16 += line.get('tgl16') if line.get('tgl16') else 0
            self.sum_tgl17 += line.get('tgl17') if line.get('tgl17') else 0
            self.sum_tgl18 += line.get('tgl18') if line.get('tgl18') else 0
            self.sum_tgl19 += line.get('tgl19') if line.get('tgl19') else 0
            self.sum_tgl20 += line.get('tgl20') if line.get('tgl20') else 0
            self.sum_tgl21 += line.get('tgl21') if line.get('tgl21') else 0
            self.sum_tgl22 += line.get('tgl22') if line.get('tgl22') else 0
            self.sum_tgl23 += line.get('tgl23') if line.get('tgl23') else 0
            self.sum_tgl24 += line.get('tgl24') if line.get('tgl24') else 0
            self.sum_tgl25 += line.get('tgl25') if line.get('tgl25') else 0
            self.sum_tgl26 += line.get('tgl26') if line.get('tgl26') else 0
            self.sum_tgl27 += line.get('tgl27') if line.get('tgl27') else 0
            self.sum_tgl28 += line.get('tgl28') if line.get('tgl28') else 0
            self.sum_tgl29 += line.get('tgl29') if line.get('tgl29') else 0
            self.sum_tgl30 += line.get('tgl30') if line.get('tgl30') else 0
            self.sum_tgl31 += line.get('tgl31') if line.get('tgl31') else 0

    @api.multi
    def print_report(self, report_type='qweb'):
        self.ensure_one()
        action = self.env.ref('vit_mrp_mo_report.mo_report_pdf')
        return action.report_action(self, config=False)
