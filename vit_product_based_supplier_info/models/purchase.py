from odoo import api, fields, models, tools, _

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'


    @api.onchange('product_id')
    def onchange_product_id(self):
        result = super(PurchaseOrderLine,self).onchange_product_id()
        if self.order_id and self.order_id.partner_id :
            supp_info = self.env['product.supplierinfo'].sudo().search([('name','=',self.order_id.partner_id.id)])
            if supp_info :
                if supp_info.mapped('product_id') :
                    result['domain'] = {'product_id': [('id', 'in', supp_info.mapped('product_id.id') )]}
                elif supp_info.mapped('product_tmpl_id') :
                    product_ids = self.env['product.product'].sudo().search([('product_tmpl_id','in',supp_info.mapped('product_tmpl_id.id') )])
                    result['domain'] = {'product_id': [('id', 'in', product_ids.ids )]}
        self.name = self.product_id.name
        return result

PurchaseOrderLine()


class ProductSupplierInfo(models.Model):
    _inherit = 'product.supplierinfo'

    ref = fields.Char('Reference', related='name.ref', store=False, copy=False)

ProductSupplierInfo()

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    @api.depends('ref', 'name')
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.ref :
                name = '['+res.ref+'] '+name
            result.append((res.id, name))
        return result

ResPartner()