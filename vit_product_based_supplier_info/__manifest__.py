{
    'name': "Product Based On Supplier Info",
    'version': "12.0.0.1",
    'summary': "This module allows you to select only those product which is assigned to the supplier info.",
    'category': 'Sale',
    'description': """
         This module allows you to select only those product which is assigned to the supplier info.

    """,
    'author': "vitraining.com [Widiana Juniar]",
    'website': "www.vitraining.com",
    'depends': ['base','purchase','product'],
    'data': [
        "views/purchase.xml",
    ],
    'demo': [],
    "license": "AGPL-3",
    'installable': True,
    'application': True,
    'auto_install': False,
}
