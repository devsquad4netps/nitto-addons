{
    "name"          : "Masalah Screw",
    "version"       : "2.7",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "MRP",
    "summary"       : "Input data masalah screw",
    "description"   : """
        
    """,
    "depends"       : [
        "mrp","vit_nitto_mrp","sh_barcode_scanner","vit_subcontractor","vit_kartu_prod_add"],
    "data"          : [
        "wizard/fq_analisa_report_wizard.xml",
        "security/groups.xml",
        "security/ir.model.access.csv",
        "views/fq_analisa.xml",
        "views/res_partner_view.xml",
        "data/data.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}