from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby



class VitFQAnalisa(models.Model):
    _name           = "vit.fq_analisa"
    _inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
    _description    = 'Data Masalah Screw'
    _order          = 'date desc'

    def on_barcode_scanned(self, barcode): 
        if self and self.state in ["cancel","confirm"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state), self.state)
            raise UserError(_("You can not scan item in %s state.") %(value))
        elif self:
            user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
            if user :
                self.operator_id = user.id
                self.barcode = False
            else :
                barcode_scan = barcode.split('#')
                name = barcode_scan[0].strip()
                production = self.env['mrp.production'].sudo().search([('name','=',name)],limit=1)
                if production and production.workorder_ids:
                    self.production_id = production.id
                    group_report = []
                    for wo in production.workorder_ids.mapped('workcenter_id.group_report') : 
                        if wo in group_report:
                            continue
                        group_report.append(wo)
                        if self.fq_detail_ids.filtered(lambda i:i.proses == wo):
                            continue
                        vals = {
                                #'workcenter_id':wo.workcenter_id.id,
                                'proses':wo,

                            }
                        # if wo.workcenter_id.machine_id:
                        #     vals.update({
                        #         'machine_id' : wo.workcenter_id.machine_id.id,                            
                        #     })                      
                        new_fq_detail_ids = self.fq_detail_ids.new(vals)
                        self.fq_detail_ids |= new_fq_detail_ids
                        self.barcode = False

    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode :
            user = self.env['res.users'].sudo().search([('login','=',self.barcode)],limit=1)
            if user :
                self.operator_id = user.id
                self.barcode = False
            else :
                barcode_scan = self.barcode.split('#')
                name = barcode_scan[0].strip()
                production = self.env['mrp.production'].sudo().search([('name','=',name)],limit=1)
                if production and production.workorder_ids:
                    self.production_id = production.id
                    group_report = []
                    #import pdb;pdb.set_trace()
                    for wo in production.workorder_ids.mapped('workcenter_id.group_report') : 
                        if wo in group_report:
                            continue
                        group_report.append(wo)
                        if self.fq_detail_ids.filtered(lambda i:i.proses == wo):
                            continue
                        if wo :
                            wos = production.workorder_ids.filtered(lambda w:w.workcenter_id.group_report == wo and w.machine_id).mapped('machine_id')
                            vals = {
                                    #'workcenter_id':wo.workcenter_id.id,
                                    'machine_ids':wos.ids,
                                    'machine_id':wos[0].id if wos else False,
                                    'proses':wo,

                                }
                            # if wo.workcenter_id.machine_id:
                            #     vals.update({
                            #         'machine_id' : wo.workcenter_id.machine_id.id,                            
                            #     })                      
                            new_fq_detail_ids = self.fq_detail_ids.new(vals)
                            self.fq_detail_ids |= new_fq_detail_ids
                        self.barcode = False
                        self.warning = False
                else :
                    self.barcode = False
                    self.warning = "Barcode %s tidak ditemukan di master user dan MO !"%name

    @api.depends('production_id')
    def get_partner_subcount(self):   
        for partner in self :
            if partner.production_id and partner.production_id.workorder_ids:
                is_subcon = partner.production_id.workorder_ids.filtered(lambda s:s.is_subcontracting)
                if is_subcon :
                    move = self.env['stock.move'].sudo().search([('production_subcon_id','=',partner.production_id.id),
                                                                    ('state','!=','cancel')],limit=1)
                    if move :
                        partner.partner_subcon_id = move.picking_id.partner_id.id or False


    name = fields.Char('Number',required=True, default='/')
    production_id = fields.Many2one('mrp.production','Production',track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
    date = fields.Date('Date', required=True, track_visibility='onchange',default=fields.Date.context_today,readonly=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    operator_id = fields.Many2one('res.users','Operator', track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)    
    product_id = fields.Char( string="Item ID", related='production_id.product_id.default_code' ,store=True )
    product_name = fields.Char(related='production_id.product_id.name', string="Item Name",store=True)
    customer_id = fields.Char(related='production_id.customer_id.name', string="Customer",store=True)
    partner_subcon_id = fields.Many2one('res.partner','Partner Subcon',store=True,compute="get_partner_subcount")
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm')],default='draft', track_visibility='onchange', string="State")
    notes = fields.Text('Notes', track_visibility='onchange')
    fq_detail_ids = fields.One2many('vit.fq_analisa.line', 'fq_analisa_id', 'Details',readonly=True,states={'draft': [('readonly', False)]})
    barcode = fields.Char('Barcode', help="Jika scan otomatis bermasalah, maka letakan kursor disini",readonly=True,states={'draft': [('readonly', False)]})
    warning = fields.Text('Warning', help="warning message")
    workcenter_id = fields.Many2one('mrp.workcenter', string='Workcenter', track_visibility='onchange')

    @api.onchange('production_id')
    def onchange_production_id(self):
        if self :
            if self.production_id and self.production_id.workorder_ids:
                data = []
                group_report = []
                for wo in self.production_id.workorder_ids.mapped('workcenter_id.group_report') : 
                    if wo :
                        if wo in group_report:
                            continue
                        group_report.append(wo)
                        if self.fq_detail_ids.filtered(lambda i:i.proses == wo):
                            continue
                        wos =self.production_id.workorder_ids.filtered(lambda w:w.workcenter_id.group_report == wo and w.machine_id).mapped('machine_id')
                        data.append((0,0, {'proses':wo,'machine_ids':wos.ids,'machine_id':wos[0].id if wos else False}))
                self.fq_detail_ids = data

    @api.onchange('fq_detail_ids')
    def onchange_domain_fq_detail_ids(self):
        domain = {'domain': {'fq_detail_ids': [('machine_id', '=', 0),('machine_ids', '=', 0)]}}
        for x in self :
            if x.fq_detail_ids and x.production_id:
                ids = x.production_id.workorder_ids.mapped('machine_id')
                domain = {'domain': {'fq_detail_ids': [('machine_id', 'in', ids.ids),('machine_ids', 'in', ids.ids)]}}
        return domain

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            number = self.env['ir.sequence'].next_by_code('vit.fq_analisa')
            if number :
                vals['name'] = number
        elif 'name' not in vals :
            number = self.env['ir.sequence'].next_by_code('vit.fq_analisa')
            if number :
                vals['name'] = number
            else :
                vals['name'] = '#'
        return super(VitFQAnalisa, self).create(vals)


    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(VitFQAnalisa, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

VitFQAnalisa()


class VitFQAnalisaLine(models.Model):
    _name           = "vit.fq_analisa.line"
    _description    = 'Data Masalah Screw (Detail)'
    _rec_name       = 'proses'

    @api.onchange('machine_id')
    def onchange_domain_machine_id(self):
        #ga ngaruh
        domain = {'domain': {'machine_id': [('id', '=', 0)],'machine_ids': [('id', '=', 0)]}}
        for x in self :
            if x.fq_analisa_id :
                ids = x.fq_analisa_id.production_id.workorder_ids.mapped('machine_id')
                domain = {'domain': {'machine_id': [('id', 'in', ids.ids)],'machine_ids': [('id', 'in', ids.ids)]}}
        return domain

    @api.depends('machine_id','fq_detail2_ids','fq_detail2_ids.fq_analisa_master_id','fq_detail2_ids.value')
    def _get_info_details(self):   
        for line in self :
            info = ''
            data1 = 0
            for ln in line.fq_detail2_ids.filtered(lambda i:i.fq_analisa_master_id) :
                if data1 == 0 :
                    saparator = ''
                    data1 +=1
                else :
                    saparator = ' | '
                info += (saparator+ln.fq_analisa_master_id.name+ ' : ' +str(round(ln.value,2)))
            line.info_detail = info

    fq_analisa_id = fields.Many2one('vit.fq_analisa','FQ Analisa',ondelete='cascade')
    workcenter_id = fields.Many2one('mrp.workcenter', 'Workcenter')
    proses = fields.Char('Proses')
    machine_id = fields.Many2one('mrp.machine', 'Mesin')
    machine_ids = fields.Many2many('mrp.machine',string='Mesin-mesin')
    info_detail = fields.Char('Details', compute="_get_info_details", store=True)
    fq_detail2_ids = fields.One2many('vit.fq_analisa.line2', 'fq_analisa_detail_id', 'Details')

VitFQAnalisaLine()


class VitFQAnalisaLine2(models.Model):
    _name           = "vit.fq_analisa.line2"
    _description    = 'Data Masalah Screw (Detail 2)'

    @api.multi
    @api.depends('fq_analisa_master_id','value')
    def name_get(self):
        result = []
        for res in self:
            if res.fq_analisa_master_id :
                name = res.fq_analisa_master_id.name + ' : ' + str(round(res.value,2))
                result.append((res.id, name))
        return result 

    fq_analisa_detail_id = fields.Many2one('vit.fq_analisa.line','FQ Analisa Detail',ondelete='cascade')
    fq_analisa_master_id = fields.Many2one('vit.fq_analisa.master','Masalah', required=True)
    value = fields.Float('Value')


VitFQAnalisaLine2()


class VitFQAnalisaMaster(models.Model):
    _name           = "vit.fq_analisa.master"
    _description    = 'Master Masalah Screw'

    workcenter_id = fields.Many2one('mrp.workcenter', 'Workcenter')
    name = fields.Char('Code', required=True)

    _sql_constraints = [
        ('workcenter_uniq', 'unique (workcenter_id,name)', 'Workcenter dan nama harus unik'),
    ]

VitFQAnalisaMaster()