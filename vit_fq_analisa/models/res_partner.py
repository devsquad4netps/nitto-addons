from odoo import models, fields, api, _



class ResPartner(models.Model):
    _inherit           = "res.partner"

    alias_name_subcon = fields.Char('Alias Name Subcon', size=60, copy=False, track_visibility='onchange')

ResPartner()