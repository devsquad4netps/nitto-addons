from odoo import api, fields, models, _
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)

class ReportFQAnalisaWizard(models.TransientModel):
    _name = "vit.fq_analisa.wizard"
    _description = "Report FQ Analisa"


    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes") 
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    workcenter_id = fields.Many2one('mrp.workcenter', string='Workcenter')

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'center','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        #wbf['header_table'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_float'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.00'})
        wbf['content_footer'] = workbook.add_format({'align': 'right', 'num_format': '#,##0.00','bg_color':colors['dark_grey']})
        wbf['content_footer2'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['dark_grey']})

        wbf['content_string'] = workbook.add_format({'align': 'left'})
        
        return wbf, workbook

    def _get_base_domain(self):
        domain = False
        for comp in self.company_ids:
            domain = []
            # if comp.second_name :
            #     second_name = comp.second_name
            #     if second_name == 'NAI TGR' :
            #         domain = [('machine_id.code', '<', 800)]
            #     elif second_name == 'NAI BKS' :
            #         domain = [('machine_id.code', '>=', 800)]
        return domain

    @api.multi
    def action_print_report_fq_analisa(self):
        self.ensure_one()
        for report in self :
            cr = self.env.cr
            report_name = 'LAPORAN FQ ANALISA'
            group_report = ''
            company = ""
            domain = []
            for comp in report.company_ids:
                c_name = comp.name
                company += c_name + "+"
            company = company[:-1]
            if report.workcenter_id :
                group_report = '('+report.workcenter_id.name+')'
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")

            row = 1
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = report.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name)
            worksheet.set_column('A1:A1', 5)
            worksheet.set_column('B1:B1', 10)
            worksheet.set_column('C1:C1', 35)
            worksheet.set_column('D1:D1', 25)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 25)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 15)
            worksheet.set_column('N1:N1', 15)
            worksheet.set_column('O1:O1', 15)

            date_print = datetime.today() + timedelta(hours=+7)
            worksheet.write('A%s'%(row), str(date_print)[:19], wbf['title_doc'])
            worksheet.merge_range('A%s:N%s'%(row+1,row+1), 'LAPORAN FQ ANALISA %s PT. NITTO ALAM INDONESIA (%s)'%( group_report, company), wbf['company'])
            worksheet.merge_range('A%s:N%s'%(row+2,row+2), report.date_start.strftime("%d %b %Y")+' - '+report.date_end.strftime("%d %b %Y"), wbf['company'])
            
            #import pdb;pdb.set_trace()
            sql = "select proses from vit_fq_analisa_line group by proses"
            cr.execute(sql)
            datas = cr.fetchall()
            if not datas :
                raise Warning('Report fq analisa not found..')
            for dt in datas :
                if dt[0] == None :
                    continue
                if report.workcenter_id:
                    domain += [('fq_analisa_id.date','>=',report.date_start.strftime("%d %b %Y")),
                                ('fq_analisa_id.date','<=',report.date_end.strftime("%d %b %Y")),
                                ('proses','=',dt[0]),
                                ('fq_analisa_id.workcenter_id','=',report.workcenter_id.id),
                                ('fq_detail2_ids','!=',False),
                                ('fq_analisa_id.state', '=', 'confirm'),
                                ('fq_analisa_id.company_id','in',report.company_ids.ids)]
                    data = self.env['vit.fq_analisa.line'].sudo().search(domain)
                    if not data :
                        #reset domain
                        reset_domain = self._get_base_domain()
                        if reset_domain:
                            domain = reset_domain
                        else :
                            domain = []
                        continue
                else:
                    domain += [('fq_analisa_id.date','>=',report.date_start.strftime("%d %b %Y")),
                                ('fq_analisa_id.date','<=',report.date_end.strftime("%d %b %Y")),
                                ('proses','=',dt[0]),
                                ('fq_detail2_ids','!=',False),
                                ('fq_analisa_id.state', '=', 'confirm'),
                                ('fq_analisa_id.company_id','in',report.company_ids.ids)]
                    data = self.env['vit.fq_analisa.line'].sudo().search(domain)
                    if not data :
                        #reset domain
                        reset_domain = self._get_base_domain()
                        if reset_domain:
                            domain = reset_domain
                        else :
                            domain = []
                        continue
                # header tabel
                row+=3
                worksheet.merge_range('A%s:C%s'%(row,row), dt[0], wbf['header_no'])
                row+=1
                worksheet.write('A%s'%(row), 'NO', wbf['header_table'])
                worksheet.write('B%s'%(row), 'ITEM ID', wbf['header_table'])
                worksheet.write('C%s'%(row), 'ITEM NAME', wbf['header_table'])
                worksheet.write('D%s'%(row), 'NO MO', wbf['header_table'])
                worksheet.write('E%s'%(row), 'NO KARTU', wbf['header_table'])
                worksheet.write('F%s'%(row), 'QTY KG', wbf['header_table'])
                worksheet.write('G%s'%(row), 'MASALAH', wbf['header_table'])
                worksheet.write('H%s'%(row), 'OPERATOR FQ', wbf['header_table'])
                worksheet.write('I%s'%(row), 'SOURCE', wbf['header_table'])
                worksheet.write('J%s'%(row), 'MESIN', wbf['header_table'])
                worksheet.write('K%s'%(row), 'CEK', wbf['header_table'])
                worksheet.write('L%s'%(row), 'TANGGAL PRS', wbf['header_table'])
                worksheet.write('M%s'%(row), 'OPERATOR PRS', wbf['header_table'])
                worksheet.write('N%s'%(row), 'STATUS', wbf['header_table'])
                worksheet.write('O%s'%(row), 'NO ANALISA', wbf['header_table'])
                row+=1
                sum_start = row
                sum_box = 0
                no = 1
                for proses in data:
                    # if len(proses.fq_analisa_id.company_id) == 1:
                    #     if proses.fq_analisa_id.company_id.second_name == 'NAI TGR':
                    #         if int(proses.machine_id.code) > 800 :
                    #             continue
                    #     elif proses.fq_analisa_id.company_id.second_name == 'NAI BKS':
                    #         if int(proses.machine_id.code) <= 800 :
                    #             continue
                    for poses_detail in proses.fq_detail2_ids:
                        source = ''
                        date_hd = ''
                        operator = ''
                        warna = ''    
                        mesin = proses.machine_id.name
                        if proses.machine_ids:
                            saparator_koma = ''
                            if len(proses.machine_ids) > 1 :
                                saparator_koma = ', '
                            mesin = ''
                            for pr in proses.machine_ids:
                                mesin += pr.name +saparator_koma
                        # if report.workcenter_id:
                            # wo = proses.fq_analisa_id.production_id.workorder_ids.filtered(lambda w:w.workcenter_id.group_report == report.workcenter_id.group_report)
                            # if not wo:
                            #     continue
                        # else :
                        wo = proses.fq_analisa_id.production_id.workorder_ids.filtered(lambda w:w.workcenter_id.group_report == dt[0])
                        move = self.env['stock.move'].sudo().search([('production_subcon_id','=',proses.fq_analisa_id.production_id.id)],limit=1)
                        
                        if wo :
                            if proses.fq_analisa_id.partner_subcon_id and wo.filtered(lambda i:i.is_subcontracting) :
                                if proses.fq_analisa_id.partner_subcon_id.alias_name_subcon :
                                    source = proses.fq_analisa_id.partner_subcon_id.alias_name_subcon
                                else:
                                    source = proses.fq_analisa_id.partner_subcon_id.name
                            if not source :
                                if wo[0].subcontract_id and wo[0].is_subcontracting :
                                    source = wo[0].subcontract_id.partner_id.alias_name_subcon if wo[0].subcontract_id.partner_id.alias_name_subcon else wo[0].subcontract_id.partner_id.name
                            date_hd = str(wo[0].date_start)[:10] if wo[0].date_start else ''
                            operator = wo[0].user_id.name if wo[0].user_id else ''
                        # if not source and move:
                        #     source = move[0].subcontract_id.name if move[0].subcontract_id.name else ''
                        #     if move[0].subcontract_id.partner_id.alias_name_kartu_produksi:
                        #         mesin = move[0].subcontract_id.partner_id.alias_name_kartu_produksi
                        wo_warna = proses.fq_analisa_id.production_id.workorder_ids.filtered(lambda w:w.warna)
                        if wo_warna :
                            warna = wo_warna[0].warna
                        box = 0
                        polybox = proses.fq_analisa_id.production_id.no_poly_box
                        if polybox:
                            poly = polybox.split('/')
                            box = int(poly[1])
                        worksheet.write('A%s'%(row), no, wbf['content_number'])
                        worksheet.write('B%s'%(row), proses.fq_analisa_id.production_id.product_id.default_code or '', wbf['content_string'])
                        worksheet.write('C%s'%(row), proses.fq_analisa_id.production_id.product_id.name, wbf['content_string'])
                        worksheet.write('D%s'%(row), proses.fq_analisa_id.production_id.name, wbf['content_string'])
                        worksheet.write('E%s'%(row), polybox, wbf['content_string'])
                        worksheet.write('F%s'%(row), poses_detail.value, wbf['content_float'])
                        worksheet.write('G%s'%(row), poses_detail.fq_analisa_master_id.name, wbf['content_string'])
                        worksheet.write('H%s'%(row), proses.fq_analisa_id.operator_id.name, wbf['content_string'])
                        worksheet.write('I%s'%(row), source, wbf['content_string'])
                        worksheet.write('J%s'%(row), mesin or '', wbf['content_string'])
                        worksheet.write('K%s'%(row), str(proses.fq_analisa_id.date), wbf['content_string'])
                        worksheet.write('L%s'%(row), date_hd, wbf['content_string'])
                        worksheet.write('M%s'%(row), operator, wbf['content_string'])
                        worksheet.write('N%s'%(row), warna, wbf['content_string'])
                        worksheet.write('O%s'%(row), proses.fq_analisa_id.name, wbf['content_string'])
                        no+=1
                        row+=1
                        sum_box += box
                # worksheet.write_formula('E%s'%(row), '=sum(E%s:E%s)' % (sum_start, row-1), wbf['content_footer2'])
                worksheet.write('E%s'%(row), sum_box, wbf['content_footer2'])
                worksheet.write_formula('F%s'%(row), '=sum(F%s:F%s)' % (sum_start, row-1), wbf['content_footer'])
                #reset domain
                reset_domain = self._get_base_domain()
                if reset_domain:
                    domain = reset_domain
                else :
                    domain = []
            # worksheet.merge_range('A%s:E%s'%(row,row),'' , wbf['header_table'])
            # worksheet.write('F%s'%(row), 'TOTAL', wbf['header_table'])
            
            # worksheet.write_formula('H%s'%(row), '=sum(H%s:H%s)' % (6, row-1), wbf['content_footer'])
            # worksheet.merge_range('I%s:S%s'%(row,row),'' , wbf['header_table'])

            if report.notes :
                worksheet.merge_range('A%s:N%s'%(row+2,row+2), report.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            report.write({'file_data':result})
            filename = report_name+' ('+company+')'
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(report.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

ReportFQAnalisaWizard()