# -*- coding: utf-8 -*-
{
    'name': "Package Product",

    'summary': """
        Package Product dari MO ke dalam package box besar dan kecil""",

    'description': """
    *   version 0.1 =  Package Product dari MO ke dalam package box besar dan kecil
    *   version 0.2 =  Fix inherit form stock move
    *   version 1.0 =  Tambah fitur unpack dan repack
    *   version 1.2 =  Rubah methode create dari ORM ke SQL
    *   version 1.3 =  isi move lines ketika repack
    *   version 1.4 =  print label disamakan bentuknya dengan label produksi
    *   version 1.5 =  tambah cron update location di package
    """,

    
    'author': 'Arman Nur Hidayat <Vitraining>',
    'website': 'http://www.vitraining.com',

    
    'category': 'Stock',
    'version': '1.6',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','vit_mrp_cost','sale','vit_mrp_lot','vit_export_import_bc','vit_kanban','sh_barcode_scanner','vit_standard_cost'],

    # always loaded
    'data': [
        'data/data.xml',
        'wizard/package_wizard.xml',
        'security/groups.xml',
        'security/ir.model.access.csv',
        'report/package_label.xml',
        'report/package_label_in_out.xml',
        'views/inherit_stock.xml',
        'views/qty_product.xml',
        'views/package.xml',
        'views/unpack.xml',
        'views/repack.xml',
        "views/in_out_pack.xml",
        
        # 'data/ir_sequence_lot.xml',
        
    ],
  
}