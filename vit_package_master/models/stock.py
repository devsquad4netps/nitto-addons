from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class stockmoveline(models.Model):
	_inherit = 'stock.move.line'
	_order = "result_package_id"

	pack_label_produksi = fields.Char(string='Label Produksi')

stockmoveline()


class stockmove(models.Model):
	_inherit = 'stock.move'


	@api.onchange('customer_id','move_line_ids.qty_done')
	def _onchange_customer_id(self):
		for rec in self:
			if rec.customer_id and rec.move_line_ids :
				# qty_done = sum(rec.move_line_ids.mapped('qty_done'))
				# if qty_done > 0 :
				# 	try:
				# 		rec.qty_kecil = (qty_done // rec.customer_id.qty_besar)
				# 	except ZeroDivisionError:
				# 		rec.qty_kecil = 0
				# else :
				#rec.qty_kecil = rec.customer_id.qty_kecil
				for line in rec.move_line_ids :
					if line.product_uom_qty == 0.0 and line.qty_done == 0.0 :
						self._cr.execute("DELETE FROM stock_move_line WHERE id = %s" , ( [(line.id)] ))
						#line.unlink()
						continue
					# elif line.qty_done == 0.0 :
					# 	line.qty_done = line.product_uom_qty
					elif line.qty_done == 0.0  and line.lot_id :
						mo_exist = self.env['mrp.production'].sudo().search([('name','=',line.lot_id.name)],limit=1)
						if mo_exist :
							last_wo = mo_exist.workorder_ids.filtered(lambda w: not w.next_work_order_id)
							if last_wo :
								try :
									line.qty_done = last_wo[0].qty_produced_real
								except:
									line.qty_done = last_wo[0].qty_produced

	def _get_default_label(self):
		context = self.env.context
		if context.get('active_model') == 'stock.quant':
			return context.get('active_ids', False)
		return False


	customer_id = fields.Many2one(
		'qty.product', 'Customer ',
		states={'done': [('readonly', True)]})	
	qty_kecil = fields.Float(string='Std Qty', related='customer_id.qty_kecil',store=True)
	qty_kecil_dus = fields.Float(string='Jml Std Qty', compute="_jumlah_dus", store=True)
	qty_besar = fields.Float(string='Std Qty', related="customer_id.qty_besar", store=True)
	qty_besar_dus = fields.Float(string='Jml Std Qty', compute="_jumlah_dus", store=True)
	qty_non_std = fields.Float(string='Jml Std Qty', compute="_jumlah_dus", store=True)
	label_besar_ids = fields.Many2many(comodel_name='stock.quant.package', string="Label Besar",
										required=False, copy=False)
	unpack_id = fields.Many2one('vit.stock.pack','Unpack', copy=False)
	repack_id = fields.Many2one('vit.stock.repack','Repack', copy=False)
	# is_repack = fields.Boolean(string='Is Repack',related="picking_id.is_repack",store=True)

	@api.depends('quantity_done','qty_kecil','qty_besar','product_uom_qty')
	def _jumlah_dus(self):
		for x in self:
			qty = x.product_uom_qty
			if x.quantity_done > 0 :
				qty = x.quantity_done
			if x.qty_besar > 0:
				try:
					x.qty_besar_dus = qty // x.qty_besar
				except ZeroDivisionError:
					x.qty_besar_dus = 0
			if x.qty_kecil > 0:
				try:
					x.qty_kecil_dus = qty // x.qty_kecil
				except ZeroDivisionError:
					x.qty_kecil_dus = 0
				# x.qty_non_std = qty - (x.qty_kecil * x.qty_kecil_dus)
				x.qty_non_std = qty - (x.customer_id.qty_besar * x.qty_besar_dus)


	@api.multi
	def action_create_package(self):
		if self.label_besar_ids :
			raise UserError(_('Label dus besar sudah dibuat'))
		if len(self.move_line_ids) > 1 :
			raise UserError(_('Move lines sudah lebih dari satu baris'))
		if self.quantity_done == 0.0 :
			raise UserError(_('Quantity done %s tidak boleh kosong ')%(self.product_id.name))
		if self.qty_besar == 0.0 :
			raise UserError(_('Standard qty %s tidak boleh kosong ')%(self.product_id.name))
		if not self.lot and self.production_id :
			self.lot = self.env['stock.production.lot'].create({'name':self.production_id.name.strip(), 'product_id' : self.product_id.id})
		qty_done = self.quantity_done
		t_label_kecil = self.qty_kecil_dus*self.qty_kecil
		sisa_non_std_kecil = qty_done - t_label_kecil

		jml_looping_dus_besar = int(self.qty_besar_dus)
		besar = 0

		lot = self.lot
		# if not lot :
		# 	data = self.env['stock.production.lot'].search([
		# 				('product_id','=', self.product_id.id),
		# 				('name', '=', self.picking_id.note)
		# 				])
		# 	if not data :
		# 		raise ValidationError(_('lot tidak ditemukan, kolom notes di picking %s kosong/tidak sesuai !') % (self.picking_id.origin))
		# 	lot = data
		mo_name = self.name.replace("-","")
		pack_ids = []
		for db in range(jml_looping_dus_besar) :
			besar += 1
			# obj_package_besar = self.env['stock.quant.package'].create({
			# 	'name' : self.picking_id.name+lot.name.replace('-','')+"-B"+ str(besar)
			# })
			# sql = """INSERT INTO stock_quant_package (name,active) VALUES ('%s',true) RETURNING id 
			# 	"""%(self.picking_id.name+lot.name.replace('-','')+"-B"+ str(besar))
			sql = """INSERT INTO stock_quant_package (name,active,location_id,company_id) VALUES ('%s',true,%s,%s) RETURNING id 
				"""%(mo_name+"-B"+ str(besar)+"#"+self.product_id.default_code+"#"+str(int(self.qty_besar)), self.location_dest_id.id,self.company_id.id)
			self.env.cr.execute(sql)
			pack_id = self.env.cr.fetchone()[0]
			pack_ids.append(pack_id)
		self.label_besar_ids = [(6, 0,pack_ids)]

		jml_looping_dus_kecil = int(self.qty_kecil_dus)
		
		kecil = 0
		if self.move_line_ids :
			kecil += 1
			# obj_package = self.env['stock.quant.package'].create({
			# 	'name' : self.picking_id.name+lot.name.replace('-','')+"-K"+ str(kecil)
			# })
			# sql = """INSERT INTO stock_quant_package (name,active) VALUES ('%s',true) RETURNING id 
			# 	"""%(self.picking_id.name+lot.name.replace('-','')+"-K"+ str(kecil))
			sql = """INSERT INTO stock_quant_package (name,active,location_id,company_id) VALUES ('%s',true,%s,%s) RETURNING id 
				"""%(mo_name+"-K"+ str(kecil)+"#"+self.product_id.default_code+"#"+ str(int(self.qty_kecil)),self.location_dest_id.id,self.company_id.id)
			self.env.cr.execute(sql)
			pack_id = self.env.cr.fetchone()[0]
			for x in self.move_line_ids :
				x.qty_done = self.qty_kecil
				x.lot_id = lot.id
				x.result_package_id = pack_id
				#x.pack_label_produksi = self.picking_id.name+lot.name.replace('-','')+"-K"+ str(kecil)
				x.pack_label_produksi = mo_name+"-K"+ str(kecil)+"#"+self.product_id.default_code+"#"+str(int(self.qty_kecil))
				jml_looping_dus_kecil -= 1 

		for ml in range(jml_looping_dus_kecil) :
			kecil += 1
			# obj_package = self.env['stock.quant.package'].create({
			# 	'name' : self.picking_id.name+lot.name.replace('-','')+"-K"+ str(kecil)
			# })
			# pack_name = self.picking_id.name+lot.name.replace('-','')+"-K"+ str(kecil)
			pack_name = mo_name+"-K"+ str(kecil)+"#"+self.product_id.default_code+"#"+ str(int(self.qty_kecil))
			sql = """INSERT INTO stock_quant_package (name,active,location_id,company_id) VALUES ('%s',true,%s,%s) RETURNING id 
				"""%(pack_name,self.location_dest_id.id,self.company_id.id)
			self.env.cr.execute(sql)
			pack_id = self.env.cr.fetchone()[0]
			self.move_line_ids.create({
				'product_id' : self.product_id.id,
				'product_uom_id' : self.product_id.uom_id.id,
				'qty_done' : self.qty_kecil,
				'product_uom_id' : self.product_id.uom_id.id,
				'location_id' : self.location_id.id,
				'picking_id' : self.picking_id.id,
				'location_dest_id' : self.location_dest_id.id,
				'move_id' : self.id,
				'lot_id' : lot.id,
				'result_package_id' : pack_id,
				'pack_label_produksi' : pack_name
			})
			# sql = """INSERT INTO stock_quant_package (name) VALUES (%s) RETURNING id 
   #              """%(pack_name)
   #          self.env.cr.execute(sql)

		# create jika ada qty standar dibawah std qty kecil
		if sisa_non_std_kecil > 0.0 :
			kecil += 1
			# pack_non_std_name = self.picking_id.name+lot.name.replace('-','')+"-K"+ str(kecil)
			pack_non_std_name = mo_name+"-K"+ str(kecil)+"#"+self.product_id.default_code+"#"+ str(int(self.qty_kecil))
			# obj_package = self.env['stock.quant.package'].create({
			# 	'name' : pack_non_std_name
			# 	#'name' : self.picking_id.name+"-K"+ str(kecil)
			# })
			sql = """INSERT INTO stock_quant_package (name,active,location_id,company_id) VALUES ('%s',true,%s,%s) RETURNING id 
				"""%(pack_non_std_name,self.location_dest_id.id,self.company_id.id)
			self.env.cr.execute(sql)
			pack_id = self.env.cr.fetchone()[0]
			self.move_line_ids.create({
				'product_id' : self.product_id.id,
				'product_uom_id' : self.product_id.uom_id.id,
				'qty_done' : sisa_non_std_kecil,
				'product_uom_id' : self.product_id.uom_id.id,
				'location_id' : self.location_id.id,
				'picking_id' : self.picking_id.id,
				'location_dest_id' : self.location_dest_id.id,
				'move_id' : self.id,
				'lot_id' : lot.id,
				'result_package_id' : pack_id,
				'pack_label_produksi' : pack_non_std_name

			})
			
		# non_std = kecil
		# if self.qty_non_std > 0 :
		# 	non_std += 1
		# 	obj_package_non = self.env['stock.quant.package'].create({
		# 		'name' : self.picking_id.name+lot.name.replace('-','')+"-K"+ str(non_std)
		# 		#'name' : self.picking_id.name+"-K"+ str(non_std)
		# 	})
		# 	self.move_line_ids.create({
		# 		'product_id' : self.product_id.id,
		# 		'product_uom_id' : self.product_id.uom_id.id,
		# 		'qty_done' : self.qty_non_std,
		# 		'product_uom_id' : self.product_id.uom_id.id,
		# 		'location_id' : self.location_id.id,
		# 		'picking_id' : self.picking_id.id,
		# 		'location_dest_id' : self.location_dest_id.id,
		# 		'move_id' : self.id,
		# 		'lot_id' : lot.id,
		# 		'result_package_id' : obj_package_non.id,
		# 		'pack_label_produksi' : obj_package_non.name
		# 	})
		if self.qty_kecil > 0.0 and self.qty_besar > 0.0:
			total_dus_kecil_di_dus_besar = self.qty_besar/self.qty_kecil
			obj_kecil = self.env['stock.quant.package'].search([('name','ilike',mo_name+"-K")])
			#obj_kecil = self.env['stock.quant.package'].search([('name','ilike',self.picking_id.name+"-K")],order='id asc')
			
			i = 0
			for bs in self.label_besar_ids.sorted('id') :
				name_besar = bs.name.split('-')
				kode_besar = name_besar[1].split('#')[0]
				data_kecil = []
				
				for j in range(int(total_dus_kecil_di_dus_besar)) :
					try :
						data_kecil.append((4,obj_kecil[i].id))
						name_kecil = obj_kecil[i].name.split('-')
						obj_kecil[i].name = name_kecil[0]+"-"+str(kode_besar)+"-"+name_kecil[1]
						# obj_kecil[i].name = bs.name+"-" + str(kode_ujung)
						# ambil yg paling ujung 
						# kode_ujung = name_kecil[-1]
						# obj_kecil[i].name = bs.name+"-" + str(kode_ujung)
						# obj_kecil[i].name = bs.name+"-" + name_kecil[4]+"-" +name_kecil[5]
						i += 1 
					except:
						pass
				bs.package_ids = data_kecil

stockmove()