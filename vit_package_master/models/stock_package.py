from odoo import api, fields, models, _
import time
import datetime
from odoo.exceptions import UserError
from odoo.http import request
import logging
_logger = logging.getLogger(__name__)


class Package_Quant(models.Model):
	_inherit = "stock.quant.package"
	
	package_ids = fields.One2many('stock.quant.package', 'package_baru_id', string='Child Package',)
	package_baru_id = fields.Many2one(string='Parent Package', comodel_name='stock.quant.package', store=True)
	active = fields.Boolean('Active', default=True)
	jenis_package = fields.Selection([
		('besar', 'Package Besar'),
		('kecil','Package Kecil')],
		compute ='_onchange_package', store=True)
	keterangan = fields.Text(string='Keterangan')
	partner_from_repack_id = fields.Many2one(string='Partner from Repack',comodel_name='res.partner')# teknikal field untuk repack product
	production_from_repack_id = fields.Many2one(string='MO from Repack',comodel_name='mrp.production')# teknikal field untuk repack product
	product_from_repack_id = fields.Many2one(string='Product from Repack',comodel_name='product.product')# teknikal field untuk repack product
	code_cust   = fields.Char('Code Customer', compute='_compute_cust_code', store=True)#keperluan print label dari menu repack
	asal_bb = fields.Char('Asal Bahan Baku', compute='_compute_asal_bahan_baku')


	def _compute_asal_bahan_baku(self):
		for pack in self:
			if pack.production_from_repack_id :
				mrp_id = self.env['mrp.production'].sudo().search([('id','=',pack.production_from_repack_id.id)],limit=1)
				if mrp_id :
					wire = mrp_id.move_raw_ids.filtered(lambda x:x.product_tmpl_id.categ_id.name == 'WIRE' or x.product_tmpl_id.categ_id.name == 'Wire')
					if wire :
						lot_id = wire[0].move_line_ids.filtered(lambda l:l.lot_id)
						if lot_id and lot_id[0].purchase_order_ids:
							vendor_id = lot_id[0].purchase_order_ids[0].partner_id
							if vendor_id and vendor_id.transaction_type :
								pack.asal_bb = vendor_id.transaction_type 

	@api.one
	@api.depends('name','package_ids','product_from_repack_id','partner_from_repack_id')
	def _compute_cust_code(self):
		for det in self :
			cust_code = det.product_from_repack_id.product_customer_code_ids.filtered(lambda x: x.partner_id.id == det.partner_from_repack_id.id)
			if cust_code :
				det.code_cust = cust_code[0].product_code	


	_sql_constraints = [
		('default_name_uniq', 'unique (name)', 'The name of the product package be unique !'),
	]
		
	# @api.one
	# @api.constrains('package_ids')
	@api.depends('package_ids','name','active')
	def _onchange_package(self):
		for rec in self:
			if rec.package_ids:
				rec.jenis_package = 'besar'
			else:
				rec.jenis_package = 'kecil'


	@api.multi
	def create_package_ids(self):
		action = self.env.ref('vit_package_master.action_repack_wizard')
		result = action.read()[0]

		id_pack = []
		for x in self.package_ids:
			id_pack.append(x.id)

		result['context'] = {
			'default_package_ids': id_pack,
		}	
		return result



	@api.model
	def update_package_info(self):
		cr = self.env.cr
		pack_exist = self.search([('location_id','=',False),('quant_ids','!=',False)])
		if pack_exist:
			_logger.info("Stock quant package to updated location (%s datas)"%(str(len(pack_exist))))
		data = 1
		for pack in pack_exist:
			#pack.location_id = pack.quant_ids[0].location_id.id
			loc_sql = "UPDATE stock_quant_package SET location_id=%s WHERE id=%s " % (pack.quant_ids[0].location_id.id, pack.id)
			cr.execute(loc_sql)
			if not pack.company_id :
				#pack.company_id = pack.quant_ids[0].company_id.id
				comp_sql = "UPDATE stock_quant_package SET company_id=%s WHERE id=%s " % (pack.quant_ids[0].company_id.id, pack.id)
				cr.execute(comp_sql)
			if not pack.owner_id and pack.quant_ids[0].owner_id:
				#pack.owner_id = pack.quant_ids[0].owner_id.id
				own_sql = "UPDATE stock_quant_package SET owner_id=%s WHERE id=%s " % (pack.quant_ids[0].owner_id.id, pack.id)
				cr.execute(own_sql)
			cr.commit()
			_logger.info("Stock quant package %s updated location to ======> %s (count : %s)"%(pack.name, pack.location_id.complete_name,str(data)))
			data += 1
		return True


Package_Quant()


class StockPickingType(models.Model):
	_inherit = 'stock.picking.type'

	repack = fields.Boolean(string='Repack',help='Picking type khusus repack', copy=False)

StockPickingType()