# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date, datetime, time
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import UserError, ValidationError


class StockPicking(models.Model):
    _inherit = 'stock.picking'


    @api.model_cr
    def init(self):
        _logger.info("creating vit_turbo_button_validate_picking_production function...")
        self.env.cr.execute("""CREATE OR REPLACE FUNCTION public.vit_turbo_button_validate_picking_production(
                v_stock_picking_id integer,
                v_company_id integer,
                v_calculate_price numeric,
                uid integer)
                RETURNS void
                LANGUAGE 'plpgsql'

                COST 100
                VOLATILE 
                
            AS $BODY$
            DECLARE
                v_stock_picking record;
                v_stock_move record;
                v_immediate_id integer;
                v_location record;
                v_location_dest record;
                v_is_create_am boolean;
                v_journal record;
                v_sequence_id integer;
                v_sequence record;
                v_next_sequence record;
                v_prefix text;
                v_am_name text;
                v_am_id integer;
                v_product record;
                v_cost_price numeric;
                v_stock_account_input_id integer;
                v_stock_valuation_account_id integer;
                v_amount numeric;
                v_account record;
                v_user_type_id integer;
                v_aml_id integer;
                v_quantity_done numeric=0.0;
                v_amount_total numeric=0.0;
                v_default_location_loss_id integer;
                v_location_loss_id integer;
                v_location_loss record;
                v_default_location_production_id integer;
                v_location_production_id integer;
                v_location_production record;
                v_sm_line record;
                
            BEGIN
                INSERT INTO "stock_immediate_transfer" ("id", "create_uid", "create_date", "write_uid", "write_date") VALUES (nextval('stock_immediate_transfer_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC')) RETURNING id INTO v_immediate_id;
                
                -- browse v_stock_picking
                SELECT * from stock_picking where id = v_stock_picking_id into v_stock_picking;
                
                -- browse stock location source
                SELECT * from stock_location where id = v_stock_picking.location_id into v_location;
                
                -- browse stock location destination
                SELECT * from stock_location where id = v_stock_picking.location_dest_id into v_location_dest;
                
                
                IF v_location.usage = 'production'
                THEN
                    v_is_create_am = true;
                    --- browse journal -------------------
                    SELECT * from account_journal where code ilike '%STJ%' and company_id=v_company_id INTO v_journal;
                    v_sequence_id = v_journal.sequence_id;	
                    --- create account move -------------------------
                    SELECT * FROM "ir_sequence" WHERE "ir_sequence".id = v_sequence_id AND company_id=v_company_id INTO v_sequence;
                    SELECT * FROM "ir_sequence_date_range" WHERE ((("ir_sequence_date_range"."sequence_id" = v_sequence_id)  AND  ("ir_sequence_date_range"."date_from" <= CURRENT_DATE))  AND  ("ir_sequence_date_range"."date_to" >= CURRENT_DATE)) ORDER BY "ir_sequence_date_range"."id"  FOR UPDATE NOWAIT limit 1 INTO v_next_sequence;
                    UPDATE ir_sequence_date_range SET number_next=number_next+1 WHERE id=v_next_sequence.id ;
                    v_prefix = replace(v_sequence.prefix, '%(range_year)s', extract(year from now())::text);
                    v_prefix = replace(v_prefix, '%(year)s', extract(year from now())::text);
                    v_prefix = replace(v_prefix, '%(month)s', extract(month from now())::text);			
                    v_am_name = coalesce(v_prefix,'') || LPAD(v_next_sequence.number_next::text, v_sequence.padding, '0')|| coalesce(v_sequence.suffix,'');
                    INSERT INTO "account_move" ("id", "create_uid", "create_date", "write_uid", "write_date", "auto_reverse", "date", "journal_id", "name", "narration", "ref", "state", "partner_id", "company_id") 
                        VALUES (nextval('account_move_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), false, CURRENT_DATE, v_journal.id, v_am_name, NULL, v_stock_picking.name, 'posted', null, v_company_id) RETURNING id INTO v_am_id;
                END IF;

                
                -- looping stock move under stock picking and update stock move line below it
                FOR v_stock_move IN SELECT * FROM stock_move where picking_id=v_stock_picking_id
                LOOP
                    -- browse produk
                    SELECT pp.*, pt.* FROM product_product pp JOIN product_template pt ON pp.product_tmpl_id = pt.id WHERE pp.id = v_stock_move.product_id INTO v_product;
                     
                    -- update stock move
                    UPDATE "stock_move" SET "is_done"=true,"date"=(now() at time zone 'UTC'),"state"='done',"write_uid"=uid,"write_date"=(now() at time zone 'UTC') WHERE id = v_stock_move.id;
                
                    -- insert or update stock_quant for this product and source location -----------------------
                    FOR v_sm_line in SELECT * from stock_move_line where move_id = v_stock_move.id
                    LOOP
                        RAISE NOTICE 'v_sm_line.lot_id=%', v_sm_line.lot_id;
                        
                        -- insert stock_quant for this product and DESTINATION location 
                        INSERT INTO stock_quant (product_id, location_id, lot_id, in_date, reserved_quantity, quantity, package_id, company_id) 
                        VALUES (v_stock_move.product_id,v_stock_picking.location_dest_id, v_sm_line.lot_id, (now() at time zone 'UTC'), 0,  v_sm_line.qty_done, v_sm_line.result_package_id, v_stock_move.company_id);	
                        
                        -- update state move_line
                        UPDATE "stock_move_line" set "state"='done' where id=v_sm_line.id;

                    END LOOP; -- stock move line
                    
                    -- cari cost price
                    SELECT ir.value_float as cost_price from ir_property ir
                        WHERE name='standard_price' and (res_id = 'product.product,'||v_product.id::text) 
                        and (ir.company_id = v_company_id or ir.company_id is null) limit 1 INTO v_cost_price;		

                    -- cari account Interim In produk ini di categ nya
                    SELECT substr(value_reference, length('account.account')+2)::integer from ir_property 
                        WHERE name='property_stock_account_input_categ_id' and (res_id is null or res_id = 'product.category,'||v_product.categ_id::text) 
                        and (company_id = v_company_id or company_id is null) order by res_id desc limit 1 INTO v_stock_account_input_id;		

                    -- cari account Stock Valuation ACcount produk ini di categ nya
                    SELECT substr(value_reference, length('account.account')+2)::integer from ir_property 
                        WHERE name='property_stock_valuation_account_id' and (res_id is null or res_id = 'product.category,'||v_product.categ_id::text) 
                        and (company_id = v_company_id or company_id is null) order by res_id desc limit 1 INTO v_stock_valuation_account_id;		
                    
                    -- hitung value price
                    SELECT sum(qty_done) from stock_move_line where move_id=v_stock_move.id into v_quantity_done;
                    -- v_amount = v_cost_price * v_quantity_done;	
                    v_amount = v_calculate_price * v_quantity_done;
                    v_amount_total = v_amount_total + v_amount;
                    
                    --- create account move line
                    IF v_is_create_am = true
                        THEN
                            -- cari inventory location default
                            SELECT substr(value_reference, length('stock.location')+2)::integer from ir_property 
                            WHERE name='property_stock_inventory' and (res_id is null) and (company_id = v_company_id or company_id is null) 
                            limit 1 INTO v_default_location_loss_id;

                            -- cari inventory location produk ini jika ada, else pake default_inventory_location
                            SELECT substr(value_reference, length('stock.location')+2)::integer from ir_property 
                                WHERE name='property_stock_inventory' and (res_id = 'product.template,'||v_product.product_tmpl_id::text) 
                                and (company_id = v_company_id or company_id is null) limit 1 INTO v_location_loss_id;
                            v_location_loss_id = COALESCE(v_location_loss_id, v_default_location_loss_id);

                            -- cari production location default
                            SELECT substr(value_reference, length('stock.location')+2)::integer from ir_property 
                            WHERE name='property_stock_production' and (res_id is null) and (company_id = v_company_id or company_id is null) 
                            limit 1 INTO v_default_location_production_id;

                            -- cari production location produk ini jika ada, else pake default_inventory_location
                            SELECT substr(value_reference, length('stock.location')+2)::integer from ir_property 
                                WHERE name='property_stock_production' and (res_id = 'product.template,'||v_product.product_tmpl_id::text) 
                                and (company_id = v_company_id or company_id is null) limit 1 INTO v_location_production_id;
                            v_location_production_id = COALESCE(v_location_production_id, v_default_location_production_id);

                            SELECT * from stock_location where id = v_location_production_id INTO v_location_production;    

                            -- cari account Interim In produk ini di categ nya
                            SELECT substr(value_reference, length('account.account')+2)::integer from ir_property 
                                WHERE name='property_stock_account_input_categ_id' and (res_id is null or res_id = 'product.category,'||v_product.categ_id::text) 
                                and value_reference is not null
                                and (company_id = v_company_id or company_id is null) order by res_id desc limit 1 INTO v_stock_account_input_id;       

                            -- insert sisi credit = Production Location
                            SELECT * from account_account where id =v_location_production.valuation_in_account_id INTO v_account;
                            v_user_type_id = v_account.user_type_id;
                            INSERT INTO "account_move_line" ("id", "create_uid", "create_date", "write_uid", "write_date", "account_id", "amount_currency", "analytic_account_id", "blocked", "company_currency_id", "credit", "currency_id", "date_maturity", "debit", "invoice_id", "move_id", "name", "partner_id", "product_id", "product_uom_id", "quantity", "reconciled", "tax_exigible", "tax_line_id","journal_id", "date", "balance", "ref", "user_type_id", "company_id") 
                                VALUES (nextval('account_move_line_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), v_account.id, 0.0, NULL, false, 12, v_amount, NULL, CURRENT_DATE, '0.00' , null, v_am_id, v_product.name, null, v_stock_move.product_id, v_stock_move.product_uom, v_quantity_done, false, true, NULL, v_journal.id, CURRENT_DATE, 0.0, v_stock_picking.name, v_user_type_id, v_company_id) RETURNING id INTO v_aml_id;

                            -- insert sisi debit = stock
                            SELECT * from account_account where id = v_stock_account_input_id INTO v_account;
                            v_user_type_id = v_account.user_type_id;				
                            INSERT INTO "account_move_line" ("id", "create_uid", "create_date", "write_uid", "write_date", "account_id", "amount_currency", "analytic_account_id", "blocked", "company_currency_id", "credit", "currency_id", "date_maturity", "debit", "invoice_id", "move_id", "name", "partner_id", "product_id", "product_uom_id", "quantity", "reconciled", "tax_exigible", "tax_line_id","journal_id", "date", "balance", "ref", "user_type_id", "company_id") 
                                VALUES (nextval('account_move_line_id_seq'), uid, (now() at time zone 'UTC'), uid, (now() at time zone 'UTC'), v_stock_valuation_account_id, 0.0, NULL, false, 12, '0.0', NULL, CURRENT_DATE, v_amount, null, v_am_id, v_product.name, null, v_stock_move.product_id, v_stock_move.product_uom, v_quantity_done, false, true, NULL, v_journal.id, CURRENT_DATE, 0.0, v_stock_picking.name, v_user_type_id, v_company_id) RETURNING id INTO v_aml_id;
                                    
                    END IF; -- v_is_create_am
                    
                END LOOP; -- stock_move

                --update stock_picking
                UPDATE "stock_picking" SET "state"='done',"date_done"=(now() at time zone 'UTC'), "write_uid"=uid,"write_date"=(now() at time zone 'UTC') WHERE id = v_stock_picking.id;
                
                -- update total account move, etc --------------------------------------------------
                UPDATE "account_move" SET "amount"=v_amount_total,"currency_id"=12 WHERE id = v_am_id;

            END;
            $BODY$;
      """)


    def turbo_button_validate_picking_production(self):
        company_id = self.company_id.id
        if self.location_id.usage != 'production':
            raise ValidationError("Hanya dokumen dari lokasi produksi yang bisa di eksekusi !")
        mo =self.env['mrp.production'].search([('name','=',self.origin),('company_id','=',company_id)])
        if not mo:
            raise ValidationError(_('Nomor Manufacturing Order %s tidak ditemukan!') % self.origin)
        cost = 0.0
        if mo.bom_id.standard_cost_ids:
            standard_cost = mo.bom_id.standard_cost_ids.filtered(lambda x:x.date <= date.today())
            if standard_cost and standard_cost.total_cost > 0.0:
                if mo.product_id.cost_method == 'standard':
                    product_cost = mo.product_id.standard_price
                    cost = standard_cost.total_cost
                    if product_cost > 0.0 :
                        mo.product_id.write({'standard_price': (product_cost+cost)/2})
                    else :
                        mo.product_id.write({'standard_price': cost}) 
        if cost == 0.0 :
            cost = mo.calculate_price
        self.env.cr.execute("select vit_turbo_button_validate_picking_production(%s, %s, %s, %s)" % (self.id, company_id, cost, self.env.uid) )

StockPicking()