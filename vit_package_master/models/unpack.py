from odoo import api, fields, models, _
import time
import datetime
from odoo.exceptions import UserError,Warning
import logging
from odoo.http import request
_logger = logging.getLogger(__name__)


class vit_stock_pack(models.Model):
    _name           = "vit.stock.pack"
    _inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
    _description    = 'Unpack Package'

    def on_barcode_scanned(self, barcode): 
        if self and self.state in ["cancel","confirm"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state), self.state)
            raise UserError(_("You can not scan item in %s state.") %(value))
        elif self:
            user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
            if user :
                self.user_id = user.id
            else :
                barcode_scan = barcode.split('#')
                name = barcode_scan[0].strip()
                pack = self.env['stock.quant.package'].search([('name','=',name)],limit=1)
                if pack:
                    if self.pack_detail_ids.filtered(lambda i:i.package_id.id == pack.id) :
                        raise Warning('Pack (%s) sudah diinsert/scan.' % (name)) 
                    vals = {'package_id':pack.id
                            }  
                    new_detail_ids = self.pack_detail_ids.new(vals)
                    self.pack_detail_ids |= new_detail_ids

                else :
                    raise UserError(_("Package %s tidak ditemukan.") % (name))



    name = fields.Char('Number', default='/')
    date = fields.Date('Date', required=True, track_visibility='onchange',default=fields.Date.context_today,readonly=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    operator_id = fields.Many2one('res.users','Operator', track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)    
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm'),('unpack','Unpack'),('done','Done')],default='draft', track_visibility='onchange', string="State")
    notes = fields.Text('Notes', track_visibility='onchange')
    pack_detail_ids = fields.One2many('vit.stock.pack.detail', 'pack_id', 'Details',readonly=True,states={'draft': [('readonly', False)]})
    move_package_ids = fields.Many2many('stock.move', string='Move unpack associated to this document', compute='_compute_move_ids',)
    unpack_count = fields.Integer(string='Unpack Moves', compute='_compute_move_ids')

    @api.depends('state')
    def _compute_move_ids(self):
      for move in self:
          move_ids = self.env['stock.move'].search([('unpack_id', '=', move.id)])
          move.move_package_ids = move_ids
          move.unpack_count = len(move_ids)

    def action_view_move_unpack(self):
      self.ensure_one()
      action = self.env.ref('stock.stock_move_action').read()[0]
      moves = self.env['stock.move'].search([('unpack_id', '=', self.id)])
      if moves:
          action['domain'] = [('id', 'in', moves.ids)]
      return action


    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.stock.pack')
        elif 'name' not in vals :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.stock.pack')
        return super(vit_stock_pack, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(vit_stock_pack, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        if not self.pack_detail_ids :
            raise UserError(_('Detail package harus diisi !'))
        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        for pack in self.pack_detail_ids :
            pack.package_id.active = True

    @api.multi
    def action_unpack(self):
        self.ensure_one()
        move_obj = self.env['stock.move']
        for pack in self.pack_detail_ids :
            #create movement ke scrap
            scrap_loc_id = self.env.ref('vit_package_master.stock_location_scrapped_repack').id 
            if pack.last_quantity > 0.0 and pack.last_lot_id and pack.package_id.location_id and pack.product_id:
                if not scrap_loc_id :
                    raise UserError(_('Lokasi scrap tidak ditemukan !'))
                datas = {'product_id'   : pack.product_id.id,
                        'location_id' : pack.package_id.location_id.id,
                        'location_dest_id' : scrap_loc_id,
                        'name' : pack.product_id.name,
                        'reference' : pack.package_id.name,
                        'origin' : self.name,
                        'product_uom_qty' : pack.last_quantity,
                        'product_uom' : pack.package_id.quant_ids[0].product_uom_id.id,
                        'unpack_id' : self.id
                         }
                move_id = move_obj.create(datas)
                move_id._action_confirm()
                move_id._action_assign()
                for line in move_id.move_line_ids :
                    line.source_package_id = pack.package_id.id
                    line.lot_id = pack.last_lot_id.id
                    line.qty_done = pack.last_quantity
                move_id._action_done()
                if move_id.state != 'done':
                    domain = ['|', ('result_package_id', 'in', pack.package_id.ids), ('package_id', 'in', pack.package_id.ids), ('state', 'not in', ['done','cancel'])]
                    pickings = self.env['stock.move.line'].search(domain)
                    if pickings :
                        pickings_name = str(pickings.mapped('picking_id.name'))
                    else :
                        pickings_name = 'None'
                    raise Warning('Movement %s dari %s ke %s tidak bisa done (sudah di assign ke dokumen %s) !' % (move_id.product_id.name,move_id.location_id.complete_name,move_id.location_dest_id.complete_name, pickings_name)) 
                # buat movement dr scrap ke stock
                return_move_id = move_id.copy({'location_id' : move_id.location_dest_id.id,
                                                'location_dest_id' : move_id.location_id.id,
                                                'origin_returned_move_id' : move_id.id,
                                                'unpack_id' : self.id})
                return_move_id._action_confirm()
                return_move_id._action_assign()
                for line in return_move_id.move_line_ids :
                    line.write({'lot_id':pack.last_lot_id.id, 'qty_done':line.product_uom_qty,'source_package_id':False})
                    # line.source_package_id = False
                    # line.lot_id = pack.last_lot_id.id
                    # line.qty_done = pack.last_quantity
                return_move_id.sudo()._action_done()
                if return_move_id.state != 'done':
                    raise Warning('Movement (return_move_id) %s dari %s ke %s tidak bisa done !' % (return_move_id.product_id.name,return_move_id.location_id.complete_name,return_move_id.location_dest_id.complete_name))              
            pack.package_id.active = False
        self.state = 'unpack'

vit_stock_pack()


class vit_stock_pack_detail(models.Model):
    _name           = "vit.stock.pack.detail"
    _description    = 'Repack Package Details'

    pack_id = fields.Many2one('vit.stock.pack','Pack', ondelete="cascade")
    package_id = fields.Many2one('stock.quant.package','Package')
    is_active = fields.Boolean('Active', related="package_id.active", store=True)
    product_id = fields.Many2one('product.product','Product', related="package_id.last_product_id")
    last_partner_id = fields.Many2one('res.partner','Last Partner', related="package_id.last_partner_id")
    last_lot_id = fields.Many2one('stock.production.lot','Last Lot', related="package_id.last_lot_id")
    last_quantity = fields.Float('Last Quantity',digits=(9,0), related="package_id.last_quantity")
    jenis_package = fields.Selection(related="package_id.jenis_package", string="Jenis Pack")

vit_stock_pack_detail()