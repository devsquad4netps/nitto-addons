from odoo import api, fields, models, _
import time
import datetime
from odoo.exceptions import UserError,Warning
import logging
from odoo.http import request
_logger = logging.getLogger(__name__)


class vit_in_out_pack(models.Model):
	_name           = "vit.in.out.pack"
	_inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
	_description    = 'In Out Package'

	def on_barcode_scanned(self, barcode): 
		if self and self.state in ["cancel","confirm"]:
			selections = self.fields_get()["state"]["selection"]
			value = next((v[1] for v in selections if v[0] == self.state), self.state)
			raise UserError(_("You can not scan item in %s state.") %(value))
		elif self:
			user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
			if user :
				self.user_id = user.id
			else :
				barcode_scan = barcode.split('#')
				name = barcode_scan[0].strip()
				pack = self.env['stock.quant.package'].search([('name','=',name)],limit=1)
				if pack:
					# if not self.product_id:
					# 	raise Warning('Pack (%s) tidak memiliki konten product.' % (name)) 
					if pack.package_ids :
						self.package_id = pack.id
						self.package_ids = [(6, 0, self.package_id.package_ids.sorted('id',reverse=False).ids)]
					else :
						if pack.last_product_id :
							self.product_id = pack.last_product_id.id
							if pack.last_product_id.id != self.product_id.id :
								raise Warning('product Pack/dus besar (%s) tidak sama dengan barcode (%s).' % (self.product_id.name,pack.last_product_id.name)) 
							else :
								new_ids = self.package_ids 
								new_ids |= pack
								self.package_ids = [(6, 0, new_ids.ids)]
						else:
							raise Warning('Pack (%s) tidak memiliki konten product !' % (name)) 
				else :
					raise UserError(_("Package %s tidak ditemukan.") % (name))

	name = fields.Char('Number', default='/')
	date = fields.Date('Date', required=True, track_visibility='onchange',default=fields.Date.context_today,readonly=True,states={'draft': [('readonly', False)]})
	user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
	operator_id = fields.Many2one('res.users','Operator', track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
	company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)    
	state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm'),('repack','Repack'),('done','Done')],default='draft', track_visibility='onchange', string="State")
	notes = fields.Text('Notes', track_visibility='onchange')
	package_id = fields.Many2one('stock.quant.package','Package',track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
	package_ids = fields.Many2many('stock.quant.package', 'package_id', string='Child Package',track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
	partner_id = fields.Many2one('res.partner','Partner',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
	product_id = fields.Many2one('product.product','Product')

	@api.onchange('package_id')
	def _onchange_package_id(self):
		if self.package_id and self.package_id.package_ids :
			self.package_ids = [(6, 0, self.package_id.package_ids.sorted('id',reverse=False).ids)]
			pack = self.package_id.package_ids.filtered(lambda p:p.last_product_id)
			if pack :
				self.product_id = pack[0].last_product_id.id

	@api.onchange('package_ids')
	def _onchange_package_ids(self):
		domain = []
		#import pdb;pdb.set_trace()
		if self.package_ids :
			pack = False
			last_product_id = self.package_id.package_ids.filtered(lambda p:p.last_product_id)
			last_partner_id = self.package_id.package_ids.filtered(lambda p:p.last_partner_id)
			if last_partner_id and last_product_id:
				pack = self.env['stock.quant.package'].search([('last_product_id','=',last_product_id[0].id),
																('last_partner_id','=',last_partner_id[0].id)])
			elif last_product_id :
				pack = self.env['stock.quant.package'].search([('last_product_id','=',last_product_id[0].id)])
				
			if pack :
				domain = {'domain': {'package_ids': [('id', 'in', pack.ids)]}}
		return domain


	@api.model
	def create(self,vals):
		if 'name' in vals and vals['name'] == '/' :
			vals['name'] = self.env['ir.sequence'].next_by_code('vit.in.out.pack')
		elif 'name' not in vals :
			vals['name'] = self.env['ir.sequence'].next_by_code('vit.in.out.pack')
		return super(vit_in_out_pack, self).create(vals)

	@api.multi
	def unlink(self):
		for data in self:
			if data.state != 'draft':
				raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
		return super(vit_in_out_pack, self).unlink()

	@api.multi
	def action_set_to_draft(self):
		self.state = 'draft'

	@api.multi
	def action_confirm(self):
		for old_pack in self.package_id.package_ids :
			old_pack.package_baru_id = False
		for new_pack in self.package_ids :
			new_pack.package_baru_id = self.package_id.id
		self.state = 'confirm'

	@api.multi
	def action_cancel(self):
		self.state = 'cancel'

	@api.multi
	def action_done(self):
		self.state = 'done'


vit_in_out_pack()