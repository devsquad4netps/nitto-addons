from odoo import api, fields, models, _
import time
import datetime
from odoo.exceptions import UserError,Warning
import logging
from odoo.http import request
_logger = logging.getLogger(__name__)


class vit_stock_repack(models.Model):
	_name           = "vit.stock.repack"
	_inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
	_description    = 'Repack Package'

	def on_barcode_scanned(self, barcode): 
		if self and self.state in ["cancel","confirm"]:
			selections = self.fields_get()["state"]["selection"]
			value = next((v[1] for v in selections if v[0] == self.state), self.state)
			raise UserError(_("You can not scan item in %s state.") %(value))
		elif self:
			user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
			if user :
				self.user_id = user.id
			else :
				barcode_scan = barcode.split('#')
				name = barcode_scan[0].strip()
				pack = self.env['stock.quant.package'].search([('name','=',name)],limit=1)
				if pack:
					if self.pack_detail_ids.filtered(lambda i:i.package_id.id == pack.id) :
						raise Warning('Pack (%s) sudah diinsert/scan.' % (name)) 
					vals = {'package_id':pack.id
							}  
					new_detail_ids = self.pack_detail_ids.new(vals)
					self.pack_detail_ids |= new_detail_ids
				else :
					raise UserError(_("Package %s tidak ditemukan.") % (name))

	name = fields.Char('Number', default='/')
	partner_id = fields.Many2one('res.partner','Partner', track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
	date = fields.Date('Date', required=True, track_visibility='onchange',default=fields.Date.context_today,readonly=True,states={'draft': [('readonly', False)]})
	user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
	operator_id = fields.Many2one('res.users','Operator', track_visibility='onchange', required=True,readonly=True,states={'draft': [('readonly', False)]})
	company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)    
	state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm'),('repack','Repack'),('done','Done')],default='draft', track_visibility='onchange', string="State")
	notes = fields.Text('Notes', track_visibility='onchange')
	pack_detail_ids = fields.One2many('vit.stock.repack.detail', 'pack_id', 'Details',readonly=True,states={'draft': [('readonly', False)]})
	pack_result_ids = fields.One2many('vit.stock.repack.result', 'pack_id', 'Result',readonly=True,states={'repack': [('readonly', False)]})
	move_package_ids = fields.Many2many('stock.move', string='Move repack associated to this document', compute='_compute_move_ids',)
	repack_count = fields.Integer(string='Repack Moves', compute='_compute_move_ids')
	qty_kecil = fields.Float(string='Std Qty',digits=(9,0),compute="_compute_dus")
	qty_kecil_dus = fields.Float(string='Jml Std Qty',digits=(9,0), default=0.0, track_visibility='onchange')
	qty_besar = fields.Float(string='Std Qty',digits=(9,0), track_visibility='onchange')
	qty_besar_dus = fields.Float(string='Jml Std Qty',digits=(9,0), default=0.0, track_visibility='onchange')
	qty_non_std = fields.Float(string='Jml Std Qty',digits=(9,0), compute="_compute_dus")
	label_besar_ids = fields.Many2many(comodel_name='stock.quant.package', string="Label Besar",
										required=False, copy=False,readonly=True,states={'repack': [('readonly', False)]})
	

	@api.depends('qty_kecil_dus','qty_besar')
	def _compute_dus(self):
		for pack in self:
			real = sum(pack.pack_detail_ids.mapped('quantity'))
			try :
				pack.qty_kecil = real/pack.qty_kecil_dus
			except ZeroDivisionError :
				pack.qty_kecil = 0.0
			t_dus_besar = pack.qty_besar*pack.qty_besar_dus
			pack.qty_non_std = real-t_dus_besar
			if pack.qty_non_std < 0.0:
				raise UserError(_('Jumlah sisa non std qty (%s) tidak boleh minus!')%(str(pack.qty_non_std)))


	@api.onchange('qty_besar_dus')
	def _onchange_qty_besar(self):
		real = sum(self.pack_detail_ids.mapped('quantity'))
		if self.qty_besar_dus :
			try :
				self.qty_besar = real/self.qty_besar_dus
			except ZeroDivisionError :
				self.qty_besar  = 0.0


	@api.depends('state')
	def _compute_move_ids(self):
		for move in self:
			move_ids = self.env['stock.move'].search([('repack_id', '=', move.id)])
			move.move_package_ids = move_ids
			move.repack_count = len(move_ids)

	def action_view_move_repack(self):
		self.ensure_one()
		action = self.env.ref('stock.stock_move_action').read()[0]
		moves = self.env['stock.move'].search([('repack_id', '=', self.id)])
		if moves:
			action['domain'] = [('id', 'in', moves.ids)]
		return action

	@api.model
	def create(self,vals):
		if 'name' in vals and vals['name'] == '/' :
			vals['name'] = self.env['ir.sequence'].next_by_code('vit.stock.repack')
		elif 'name' not in vals :
			vals['name'] = self.env['ir.sequence'].next_by_code('vit.stock.repack')
		return super(vit_stock_repack, self).create(vals)

	@api.multi
	def unlink(self):
		for data in self:
			if data.state != 'draft':
				raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
		return super(vit_stock_repack, self).unlink()

	@api.multi
	def action_set_to_draft(self):
		self.state = 'draft'

	@api.multi
	def action_confirm(self):
		if not self.pack_detail_ids :
			raise UserError(_('Detail package harus diisi !'))
		if self.pack_detail_ids.filtered(lambda x:x.quantity <= 0.0):
			raise UserError(_('Quantity tidak boleh bernilai nol !'))
		self.state = 'confirm'

	@api.multi
	def action_cancel(self):
		self.state = 'cancel'

	@api.multi
	def action_done(self):
		self.state = 'done'

	@api.multi
	def action_repack(self):
		self.ensure_one()
		move_obj = self.env['stock.move']
		package_id = False

		for pack in self.pack_detail_ids :
			if pack.quantity <= 0.0:
				raise UserError(_('Quantity product %s harus lebih dari nol !')%(pack.product_id.name))
			#create movement ke scrap
			scrap_loc_id = self.env.ref('vit_package_master.stock_location_scrapped_repack').id 
			stock_location_id = pack.lot_id.quant_ids.filtered(lambda i:i.location_id.usage == 'internal').sorted('in_date')
			if not stock_location_id :
				raise UserError(_('Lokasi stock product %s tidak ditemukan !')%(pack.product_id.name))
			package_id = stock_location_id[0].package_id.id
			if not scrap_loc_id :
				raise UserError(_('Lokasi scrap tidak ditemukan !'))
			datas = {'product_id'   : pack.product_id.id,
					'location_id' : stock_location_id[0].location_id.id,
					'location_dest_id' : scrap_loc_id,
					'name' : pack.product_id.name,
					'reference' : pack.product_id.name,
					'origin' : self.name,
					'product_uom_qty' : pack.quantity,
					'product_uom' : pack.lot_id.quant_ids[0].product_uom_id.id,
					'repack_id' : self.id
					 }
			move_id = move_obj.create(datas)
			move_id._action_confirm()
			move_id._action_assign()
			for line in move_id.move_line_ids :
				line.write({'lot_id':pack.lot_id.id, 'qty_done':line.product_uom_qty,'source_package_id':False})
			# for line in move_id.move_line_ids :
			# 	line.source_package_id = False
			# 	line.lot_id = pack.lot_id.id
			# 	line.qty_done = pack.quantity
			move_id.sudo()._action_done()
			if move_id.state != 'done' :
				raise Warning('Movement (move_id) %s dari %s ke %s tidak bisa done !' % (move_id.product_id.name,move_id.location_id.complete_name,move_id.location_dest_id.complete_name)) 
			# if package_id :
			# 	stock_location_id[0].package_id.active = False
			
			# create new pack nya
			mrp = self.env['mrp.production'].sudo().search([('name','=',pack.lot_id.name)],limit=1)
			if mrp :
				mrp_id = mrp.id
			else :
				mrp_id = False
			if not self.label_besar_ids :
				jml_looping_dus_besar = int(self.qty_besar_dus)
				besar = 0
				for db in range(jml_looping_dus_besar) :
					besar += 1
					package_besar_id = self.env['stock.quant.package'].create({
						'name' : self.name+pack.lot_id.name.replace('-','')+"-B"+ str(besar),
						'jenis_package' : 'besar',
						'partner_from_repack_id': self.partner_id.id,
						'product_from_repack_id': pack.product_id.id,
						'production_from_repack_id':mrp_id
					})
					
					self.label_besar_ids = [(4,package_besar_id.id)]
			max_qty = pack.quantity
			try:
				jml_looping_dus_kecil = int(pack.quantity/self.qty_kecil)
			except:
				jml_looping_dus_kecil = 1

			
			kecil = 0
			# buat return dr scrap
			max_qty_dus_besar = self.qty_besar_dus
			return_move_id = move_id.copy({'location_id' : move_id.location_dest_id.id,
											'location_dest_id' : move_id.location_id.id,
											'origin_returned_move_id' : move_id.id,
											'repack_id' : self.id})
			return_move_id._action_confirm()
			return_move_id._action_assign()
			total_qty_done = 0.0
			for line in return_move_id.move_line_ids :
				if pack.quantity > total_qty_done :
					kecil += 1
					package_kecil_id = self.env['stock.quant.package'].create({
						'name' : self.name+pack.lot_id.name.replace('-','')+"-K"+ str(kecil),
						'jenis_package' : 'kecil',
						'partner_from_repack_id': self.partner_id.id,
						'product_from_repack_id': pack.product_id.id,
						'production_from_repack_id':mrp_id
					})
					line.result_package_id = package_kecil_id.id
					line.lot_id = pack.lot_id.id
					line.qty_done = self.qty_kecil
					self.pack_result_ids.create({'pack_id':self.id,
												'lot_id':pack.lot_id.id, 
												'package_id': package_kecil_id.id, 
												'quantity': self.qty_kecil})
					total_qty_done += self.qty_kecil
			if kecil < jml_looping_dus_kecil :
				for ml in range(jml_looping_dus_kecil-kecil) :
					if pack.quantity > total_qty_done :
						kecil += 1
						package_kecil_id = self.env['stock.quant.package'].create({
							'name' : self.name+pack.lot_id.name.replace('-','')+"-K"+ str(kecil),
							'jenis_package' : 'kecil',
							'partner_from_repack_id': self.partner_id.id,
							'product_from_repack_id': pack.product_id.id,
							'production_from_repack_id':mrp_id
						})
						
						self.env['stock.move.line'].create({
							'move_id' : return_move_id.id,
							'product_id' : return_move_id.product_id.id,
							'product_uom_id' : return_move_id.product_id.uom_id.id,
							'qty_done' : self.qty_kecil,
							'product_uom_id' : return_move_id.product_id.uom_id.id,
							'location_id' : return_move_id.location_id.id,
							'picking_id' : False,
							'location_dest_id' : return_move_id.location_dest_id.id,
							'lot_id' : pack.lot_id.id,
							'result_package_id' : package_kecil_id.id,
							'pack_label_produksi' : package_kecil_id.name
						})
						self.pack_result_ids.create({'pack_id':self.id,
												'lot_id':pack.lot_id.id, 
												'package_id': package_kecil_id.id, 
												'quantity': self.qty_kecil})
						total_qty_done += self.qty_kecil
			for line in return_move_id.move_line_ids :
				line.write({'lot_id':pack.lot_id.id, 'qty_done':line.product_uom_qty})
			return_move_id.sudo()._action_done()
			if return_move_id.state != 'done' :
				raise Warning('Movement (return_move_id) %s dari %s ke %s tidak bisa done !' % (return_move_id.product_id.name,return_move_id.location_id.complete_name,return_move_id.location_dest_id.complete_name)) 
			# assign ke pack besar
			for big in self.label_besar_ids.sorted('id'):
				total = 0
				initial_b = big.name.split('-')
				for small in self.pack_result_ids.filtered(lambda sm:not sm.big_pack_id) :
					if total < self.qty_besar and len(big.package_ids) < int(self.qty_besar/self.qty_kecil) :
						big.package_ids = [(4,small.package_id.id)]
						small.big_pack_id = big.id
						split_k = small.package_id.name.split('-')
						small.package_id.name = split_k[0]+'-'+initial_b[1]+'-'+split_k[1]
						total += self.qty_kecil
					else :
						break
		self.state = 'repack'
		return True

vit_stock_repack()


class vit_stock_repack_detail(models.Model):
	_name           = "vit.stock.repack.detail"
	_description    = 'Repack Package Details'

	pack_id = fields.Many2one('vit.stock.repack','Pack', ondelete="cascade")
	product_id = fields.Many2one('product.product','Product',related="lot_id.product_id")
	lot_id = fields.Many2one('stock.production.lot','Lot')
	quantity = fields.Float('Quantity',digits=(9,0))
	
vit_stock_repack_detail()


class vit_stock_repack_result(models.Model):
	_name           = "vit.stock.repack.result"
	_description    = 'Repack Package Result'

	pack_id = fields.Many2one('vit.stock.repack','Pack', ondelete="cascade")
	package_id = fields.Many2one('stock.quant.package','Package')
	product_id = fields.Many2one('product.product','Product',related="lot_id.product_id")
	lot_id = fields.Many2one('stock.production.lot','Lot')
	quantity = fields.Float('Quantity',digits=(9,0))
	big_pack_id = fields.Many2one('stock.quant.package','Dus Besar')
	code_cust   = fields.Char('Code Customer', compute='_compute_cust_code', store=True)

	@api.one
	@api.depends('product_id','pack_id.partner_id','pack_id.state')
	def _compute_cust_code(self):
		for det in self :
			cust_code = det.product_id.product_customer_code_ids.filtered(lambda x: x.partner_id.id == det.pack_id.partner_id.id)
			if cust_code :
				det.code_cust = cust_code[0].product_code

vit_stock_repack_result()