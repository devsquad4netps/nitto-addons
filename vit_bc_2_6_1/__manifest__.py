{
    "name"          : "BC 2.6.1",
    "version"       : "1.4",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Bea Cukai",
    "license"       : "LGPL-3",
    "contributors"  : """
        - Miftahussalam <https://blog.miftahussalam.com>
    """,
    "summary"       : "Export Import BC 2.6.1",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "stock",
        "sale",
        "purchase",
        "vit_export_import_bc",
        "vit_subcontractor",
    ],
    "data"          : [
        "data/vit_export_import_bc.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}