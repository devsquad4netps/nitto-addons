from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning

class VitExportImportBcWizard(models.TransientModel):
    _inherit = "vit.export.import.bc.wizard"

    @api.multi
    def get_datas(self):
        res = super(VitExportImportBcWizard, self).get_datas()

        if self.bc_id.name == 'BC 2.6.1' :
            no_aju_ids = self.env['vit.nomor.aju']
            # sale_ids = self.env['sale.order'].search([
            #     ('company_id','=',self.company_id.id),
            #     ('state','in',['sale','done']),
            #     ('sale_type','=','Subcon'),
            # ])
            # for sale_id in sale_ids :
            #     # for picking_id in sale_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer'):
            #     for picking_id in sale_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.location_dest_id.usage == 'customer' and picking.tgl_dokumen == self.date):
            #         picking_id.write({'has_export':True})
            #         no_aju_ids |= picking_id.nomor_aju_id
            pickings = self.env['stock.picking'].search([('company_id','=',self.company_id.id),
                                                        ('state','=','done'),
                                                        ('tgl_dokumen','=',self.date),
                                                        ('subcontract_id','!=',False)])
            if pickings :
                for picking_id in pickings.filtered(lambda picking:picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id):
                    picking_id.write({'has_export':True})
                    no_aju_ids |= picking_id.nomor_aju_id

            no_aju_ids = no_aju_ids.filtered(lambda aju: not aju.has_export)

            headers = [
                'NOMOR AJU',
                'KPPBC',
                'PERUSAHAAN',
                'PEMASOK',
                'STATUS',
                'KODE DOKUMEN PABEAN',
                'NPPJK',
                'ALAMAT PEMASOK',
                'ALAMAT PEMILIK',
                'ALAMAT PENERIMA BARANG',
                'ALAMAT PENGIRIM',
                'ALAMAT PENGUSAHA',
                'ALAMAT PPJK',
                'API PEMILIK',
                'API PENERIMA',
                'API PENGUSAHA',
                'ASAL DATA',
                'ASURANSI',
                'BIAYA TAMBAHAN',
                'BRUTO',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG PEMILIK',
                'URL DOKUMEN PABEAN',
                'FOB',
                'FREIGHT',
                'HARGA BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA TOTAL',
                'ID MODUL',
                'ID PEMASOK',
                'ID PEMILIK',
                'ID PENERIMA BARANG',
                'ID PENGIRIM',
                'ID PENGUSAHA',
                'ID PPJK',
                'JABATAN TTD',
                'JUMLAH BARANG',
                'JUMLAH KEMASAN',
                'JUMLAH KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE ASAL BARANG',
                'KODE ASURANSI',
                'KODE BENDERA',
                'KODE CARA ANGKUT',
                'KODE CARA BAYAR',
                'KODE DAERAH ASAL',
                'KODE FASILITAS',
                'KODE FTZ',
                'KODE HARGA',
                'KODE ID PEMASOK',
                'KODE ID PEMILIK',
                'KODE ID PENERIMA BARANG',
                'KODE ID PENGIRIM',
                'KODE ID PENGUSAHA',
                'KODE ID PPJK',
                'KODE JENIS API',
                'KODE JENIS API PEMILIK',
                'KODE JENIS API PENERIMA',
                'KODE JENIS API PENGUSAHA',
                'KODE JENIS BARANG',
                'KODE JENIS BC25',
                'KODE JENIS NILAI',
                'KODE JENIS PEMASUKAN01',
                'KODE JENIS PEMASUKAN 02',
                'KODE JENIS TPB',
                'KODE KANTOR BONGKAR',
                'KODE KANTOR TUJUAN',
                'KODE LOKASI BAYAR',
                '',
                'KODE NEGARA PEMASOK',
                'KODE NEGARA PENGIRIM',
                'KODE NEGARA PEMILIK',
                'KODE NEGARA TUJUAN',
                'KODE PEL BONGKAR',
                'KODE PEL MUAT',
                'KODE PEL TRANSIT',
                'KODE PEMBAYAR',
                'KODE STATUS PENGUSAHA',
                'STATUS PERBAIKAN',
                'KODE TPS',
                'KODE TUJUAN PEMASUKAN',
                'KODE TUJUAN PENGIRIMAN',
                'KODE TUJUAN TPB',
                'KODE TUTUP PU',
                'KODE VALUTA',
                'KOTA TTD',
                'NAMA PEMILIK',
                'NAMA PENERIMA BARANG',
                'NAMA PENGANGKUT',
                'NAMA PENGIRIM',
                'NAMA PPJK',
                'NAMA TTD',
                'NDPBM',
                'NETTO',
                'NILAI INCOTERM',
                'NIPER PENERIMA',
                'NOMOR API',
                'NOMOR BC11',
                'NOMOR BILLING',
                'NOMOR DAFTAR',
                'NOMOR IJIN BPK PEMASOK',
                'NOMOR IJIN BPK PENGUSAHA',
                'NOMOR IJIN TPB',
                'NOMOR IJIN TPB PENERIMA',
                'NOMOR VOYV FLIGHT',
                'NPWP BILLING',
                'POS BC11',
                'SERI',
                'SUBPOS BC11',
                'SUB SUBPOS BC11',
                'TANGGAL BC11',
                'TANGGAL BERANGKAT',
                'TANGGAL BILLING',
                'TANGGAL DAFTAR',
                'TANGGAL IJIN BPK PEMASOK',
                'TANGGAL IJIN BPK PENGUSAHA',
                'TANGGAL IJIN TPB',
                'TANGGAL NPPPJK',
                'TANGGAL TIBA',
                'TANGGAL TTD',
                'TANGGAL JATUH TEMPO',
                'TOTAL BAYAR',
                'TOTAL BEBAS',
                'TOTAL DILUNASI',
                'TOTAL JAMIN',
                'TOTAL SUDAH DILUNASI',
                'TOTAL TANGGUH',
                'TOTAL TANGGUNG',
                'TOTAL TIDAK DIPUNGUT',
                'URL DOKUMEN PABEAN',
                'VERSI MODUL',
                'VOLUME',
                'WAKTU BONGKAR',
                'WAKTU STUFFING',
                'NOMOR POLISI',
                'CALL SIGN',
                'JUMLAH TANDA PENGAMAN',
                'KODE JENIS TANDA PENGAMAN',
                'KODE KANTOR MUAT',
                'KODE PEL TUJUAN',
                '',
                'TANGGAL STUFFING',
                'TANGGAL MUAT',
                'KODE GUDANG ASAL',
                'KODE GUDANG TUJUAN'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    move_line_ids = picking_id.move_ids_without_package.mapped('move_line_ids')
                    sale_line_ids = picking_id.move_ids_without_package.mapped('sale_line_id')
                    # if not sale_line_ids :
                    #     continue

                    valuta = ''
                    invoice_ids = self.env['account.invoice']
                    if picking_id.origin : # pake inv/order PO dulu
                        sale_exist = self.env['purchase.order'].search([('name','=',picking_id.origin)])
                        sale_ids = sale_line_ids.mapped('order_id')
                        if sale_ids:
                            valuta = sale_ids[0].currency_id.name
                            invoice_ids = sale_ids.mapped('invoice_ids')
                    jml_type_kemasan = 0
                    packs = []
                    for kemasan in picking_id.kemasan_ids:
                        if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                            packs.append(kemasan.package_type_id.id)
                            jml_type_kemasan+=1
                    cif = self.env.user.convert_to_usd(amount=sum(line.price_subtotal for line in sale_line_ids), rate_date=picking_id.scheduled_date)
                    values.append({
                        'NOMOR AJU': aju_id.name.strip() or '',
                        'KPPBC': self.bc_id.kppbc.strip() or '',
                        'PERUSAHAAN': self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                        'PEMASOK': '',
                        'STATUS': '00',
                        'KODE DOKUMEN PABEAN': self.bc_id.kode_dokumen_pabean.strip() or '',
                        'NPPJK': '',
                        'ALAMAT PEMASOK': '',
                        'ALAMAT PEMILIK': '',#picking_id.company_id.street.strip() or '',
                        'ALAMAT PENERIMA BARANG': picking_id.partner_id.street.strip() or '',
                        'ALAMAT PENGIRIM': '',
                        'ALAMAT PENGUSAHA': picking_id.company_id.street.strip() or '',
                        'ALAMAT PPJK': '',
                        'API PEMILIK': '',#self.bc_id.api_pemilik.strip() or '',
                        'API PENERIMA': '',
                        'API PENGUSAHA': '',#self.bc_id.api_pengusaha.strip() or '',
                        'ASAL DATA': self.bc_id.asal_data.strip() or '',
                        'ASURANSI': '0.00',
                        'BIAYA TAMBAHAN': '0.00',
                        'BRUTO': sum(line.brutto for line in picking_id.move_ids_without_package) or '',
                        'CIF': cif or '0.00',
                        'CIF RUPIAH': self.env.user.convert_to_ndpbm(amount=cif, aju_id=aju_id, rate_date=picking_id.scheduled_date) or '0.00',
                        'DISKON': '0.00',
                        'FLAG PEMILIK': '',
                        'URL DOKUMEN PABEAN': '',
                        'FOB': '0.00',
                        'FREIGHT': picking_id.freight,
                        'HARGA BARANG LDP': '0.00',
                        'HARGA INVOICE': '0.00',
                        'HARGA PENYERAHAN': sum(inv.amount_total for inv in invoice_ids) or '0.00',
                        'HARGA TOTAL': '0.00',
                        'ID MODUL': self.bc_id.id_modul.strip() or '',
                        'ID PEMASOK': '',
                        'ID PEMILIK': '',#self.company_id.vat.strip() or '',
                        'ID PENERIMA BARANG': picking_id.partner_id.vat.strip() if picking_id.partner_id.vat else '',
                        'ID PENGIRIM':'',
                        'ID PENGUSAHA': self.company_id.vat.strip() or '',
                        'ID PPJK': '',
                        'JABATAN TTD': self.bc_id.jabatan_ttd.strip() or '',
                        'JUMLAH BARANG': len(picking_id.mapped('move_lines.production_subcon_id')) or 0,
                        'JUMLAH KEMASAN': jml_type_kemasan,
                        'JUMLAH KONTAINER': len(picking_id.kontainer_ids) or '',
                        'KESESUAIAN DOKUMEN': '',
                        'KETERANGAN': '',
                        'KODE ASAL BARANG': '',
                        'KODE ASURANSI': '',
                        'KODE BENDERA': '',
                        'KODE CARA ANGKUT': picking_id.kode_cara_angkut.strip() or '',
                        'KODE CARA BAYAR': '',
                        'KODE DAERAH ASAL': '',
                        'KODE FASILITAS': '',
                        'KODE FTZ': '',
                        'KODE HARGA': '',
                        'KODE ID PEMASOK': '',
                        'KODE ID PEMILIK': '',#self.bc_id.kode_id_pemilik.strip() or '',
                        'KODE ID PENERIMA BARANG': self.bc_id.kode_id_penerima.strip() or '',
                        'KODE ID PENGIRIM': '',
                        'KODE ID PENGUSAHA': self.bc_id.kode_id_pengusaha.strip() or '',
                        'KODE ID PPJK': '',
                        'KODE JENIS API': '',
                        'KODE JENIS API PEMILIK': '2',
                        'KODE JENIS API PENERIMA': '',#'2',
                        'KODE JENIS API PENGUSAHA': '2',
                        'KODE JENIS BARANG': '',
                        'KODE JENIS BC25': '',
                        'KODE JENIS NILAI': '',
                        'KODE JENIS PEMASUKAN01': '',
                        'KODE JENIS PEMASUKAN 02': '',
                        'KODE JENIS TPB': '',#self.bc_id.kode_jenis_tpb.strip() or '',
                        'KODE KANTOR BONGKAR': '',
                        'KODE KANTOR TUJUAN': '',
                        'KODE LOKASI BAYAR': '',#'1',
                        '': '',
                        'KODE NEGARA PEMASOK': '',
                        'KODE NEGARA PENGIRIM': '',
                        'KODE NEGARA PEMILIK': '',
                        'KODE NEGARA TUJUAN': '',
                        'KODE PEL BONGKAR': '',
                        'KODE PEL MUAT': '',
                        'KODE PEL TRANSIT': '',
                        'KODE PEMBAYAR': '',#'1',
                        'KODE STATUS PENGUSAHA': '',
                        'STATUS PERBAIKAN': '',
                        'KODE TPS': '',
                        'KODE TUJUAN PEMASUKAN': '',
                        'KODE TUJUAN PENGIRIMAN': self.bc_id.kode_tujuan_pengiriman.strip() if self.bc_id.kode_tujuan_pengiriman else '',
                        'KODE TUJUAN TPB': '',
                        'KODE TUTUP PU': '',
                        'KODE VALUTA': 'USD',
                        'KOTA TTD': self.bc_id.kota_ttd.strip() or '',
                        'NAMA PEMILIK': '',#self.company_id.bc_name.strip() if self.company_id.bc_name else self.company_id.name.strip(),
                        'NAMA PENERIMA BARANG': picking_id.partner_id.name.strip() or '',
                        'NAMA PENGANGKUT': '',
                        'NAMA PENGIRIM': '',
                        'NAMA PPJK': '',
                        'NAMA TTD': self.bc_id.nama_ttd.strip() or '',
                        'NDPBM': picking_id.subcontract_id.name or '',
                        'NETTO': sum(move.product_id.weight * move.product_qty for move in picking_id.move_ids_without_package) or '',
                        'NILAI INCOTERM': '0.0000',
                        'NIPER PENERIMA': '',
                        'NOMOR API': '',
                        'NOMOR BC11': '',
                        'NOMOR BILLING': '',
                        'NOMOR DAFTAR': '',
                        'NOMOR IJIN BPK PEMASOK': '',
                        'NOMOR IJIN BPK PENGUSAHA': '',
                        'NOMOR IJIN TPB': self.bc_id.nomor_ijin_tpb.strip() or '',
                        'NOMOR IJIN TPB PENERIMA': '',
                        'NOMOR VOYV FLIGHT': '',
                        'NPWP BILLING': '',#'010615326055000',
                        'POS BC11': '',
                        'SERI': self.bc_id.seri.strip() or '',
                        'SUBPOS BC11': '',
                        'SUB SUBPOS BC11': '',
                        'TANGGAL BC11': '',
                        'TANGGAL BERANGKAT': '',
                        'TANGGAL BILLING': '',
                        'TANGGAL DAFTAR': '',
                        'TANGGAL IJIN BPK PEMASOK': '',
                        'TANGGAL IJIN BPK PENGUSAHA': '',
                        'TANGGAL IJIN TPB': '',
                        'TANGGAL NPPPJK': '',
                        'TANGGAL TIBA': '',
                        'TANGGAL TTD': self.reformat_date(picking_id.tgl_ttd.strftime('%Y-%m-%d')) if picking_id.tgl_ttd else '',
                        'TANGGAL JATUH TEMPO': '',
                        'TOTAL BAYAR': '0.00',
                        'TOTAL BEBAS': '0.00',
                        'TOTAL DILUNASI': '0.00',
                        'TOTAL JAMIN': picking_id.subcontract_id.nilai_jaminan or '0.00',
                        'TOTAL SUDAH DILUNASI': '0.00',
                        'TOTAL TANGGUH': '',
                        'TOTAL TANGGUNG': '0.00',
                        'TOTAL TIDAK DIPUNGUT': '0.00',
                        'URL DOKUMEN PABEAN': '',
                        'VERSI MODUL': self.bc_id.versi_modul.strip() or '',
                        'VOLUME': '0.0000',
                        'WAKTU BONGKAR': '',
                        'WAKTU STUFFING': '',
                        'NOMOR POLISI': '',#picking_id.no_polisi.strip() if picking_id.no_polisi else '',
                        'CALL SIGN': '',
                        'JUMLAH TANDA PENGAMAN': '0',
                        'KODE JENIS TANDA PENGAMAN': '',
                        'KODE KANTOR MUAT': '',
                        'KODE PEL TUJUAN': '',
                        '': '',
                        'TANGGAL STUFFING': '',
                        'TANGGAL MUAT': '',
                        'KODE GUDANG ASAL': '',
                        'KODE GUDANG TUJUAN': '',
                    })
            res.append(['Header'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'CIF',
                'CIF RUPIAH',
                'HARGA PENYERAHAN',
                'HARGA PEROLEHAN',
                'JENIS SATUAN',
                'JUMLAH SATUAN',
                'KODE ASAL BAHAN BAKU',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE JENIS DOK ASAL',
                'KODE KANTOR',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'MERK',
                'NDPBM',
                'NETTO',
                'NOMOR AJU DOKUMEN ASAL',
                'NOMOR DAFTAR DOKUMEN ASAL',
                'POS TARIF',
                'SERI BARANG DOKUMEN ASAL',
                'SPESIFIKASI LAIN',
                'TANGGAL DAFTAR DOKUMEN ASAL',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'SERI IJIN'
            ]
            values = []
            #import pdb;pdb.set_trace()
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    seri_barang = 1
                    for move in picking_id.move_ids_without_package :
                        if not move.production_subcon_id :
                            continue
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        for line in move.move_line_ids :
                            # finished_move_line_id = self.env['stock.move.line'].search([
                            #     ('product_id','=',line.product_id.id),
                            #     ('lot_id','=',line.lot_id.id),
                            #     ('move_id.production_id','!=',False),
                            #     # ('state','=','done'),
                            # ], limit=1)
                            # if not finished_move_line_id :
                                # continue
                            # ratio = line.qty_done/finished_move_line_id.production_id.product_qty
                            # for raw_line in finished_move_line_id.production_id.move_raw_ids :
                            # naik dulu ke move_id baru ke production_id
                            # ratio = line.qty_done/finished_move_line_id.move_id.production_id.product_qty
                            ratio = line.qty_done/move.production_subcon_id.product_qty
                            for raw_line in move.production_subcon_id.move_raw_ids :
                                for move_line in raw_line.active_move_line_ids :
                                    incoming_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',move_line.product_id.id),
                                        ('lot_id','=',move_line.lot_id.id),
                                        ('move_id.production_id','=',False),
                                        ('state','=','done'),
                                        ('move_id.location_id.usage','=','supplier'),
                                    ], limit=1)
                                    if not incoming_move_line_id :
                                        continue
                                    if incoming_move_line_id.move_id.seri_barang :
                                        seri_bb = incoming_move_line_id.move_id.seri_barang.strip()
                                    else:
                                        seri_bb = ''
                                    if incoming_move_line_id.picking_id.no_pabean:
                                        dok_asal = incoming_move_line_id.picking_id.no_pabean.strip()
                                    else :
                                        dok_asal = ''
                                    values.append({
                                        'NOMOR AJU': aju_id.name.strip() or '',
                                        'SERI BARANG': move.seri_barang or '',
                                        'SERI BAHAN BAKU': '1',
                                        'CIF': incoming_move_line_id.qty_done * incoming_move_line_id.move_id.purchase_line_id.price_unit or '0.00',
                                        'CIF RUPIAH': incoming_move_line_id.move_id.purchase_line_id.currency_id.compute(incoming_move_line_id.qty_done * incoming_move_line_id.move_id.purchase_line_id.price_unit, incoming_move_line_id.move_id.purchase_line_id.company_id.currency_id) or '0.00',
                                        'HARGA PENYERAHAN': incoming_move_line_id.move_id.purchase_line_id.price_unit or '',
                                        'HARGA PEROLEHAN': '',
                                        'JENIS SATUAN': incoming_move_line_id.move_id.product_uom.name or '',
                                        # 'JUMLAH SATUAN': incoming_move_line_id.move_id.quantity_done or '',
                                        'JUMLAH SATUAN': move.netto_wire if move.netto_wire != 0.0 else incoming_move_line_id.move_id.quantity_done or '',
                                        'KODE ASAL BAHAN BAKU': self.bc_id.kode_asal_bahan_baku or '',
                                        'KODE BARANG': incoming_move_line_id.move_id.product_id.default_code or '',
                                        'KODE FASILITAS': '',
                                        'KODE JENIS DOK ASAL': self.bc_id.kode_riwayat_barang or '',
                                        'KODE KANTOR': self.bc_id.kode_kantor_pabean_asal or '',
                                        'KODE SKEMA TARIF': '',
                                        'KODE STATUS': self.bc_id.kode_status or '',
                                        'MERK': '',
                                        'NDPBM': picking_id.ndpbm_id.get_rate(str(picking_id.scheduled_date)) or '',
                                        'NETTO': incoming_move_line_id.netto or '0.0000',
                                        'NOMOR AJU DOKUMEN ASAL': incoming_move_line_id.picking_id.nomor_aju_id.name or '',
                                        'NOMOR DAFTAR DOKUMEN ASAL': dok_asal,
                                        'POS TARIF': incoming_move_line_id.move_id.product_id.group_id.kode_tarif or '',
                                        'SERI BARANG DOKUMEN ASAL': incoming_move_line_id.picking_id.seri_dokumen or '',
                                        'SPESIFIKASI LAIN': incoming_move_line_id.move_id.product_id.group_id.notes.strip() if incoming_move_line_id.move_id.product_id.group_id.notes else incoming_move_line_id.move_id.product_id.group_id.name or '',
                                        'TANGGAL DAFTAR DOKUMEN ASAL': incoming_move_line_id.picking_id.tgl_pabean.strftime("%Y-%m-%d") if incoming_move_line_id.picking_id.tgl_pabean else '',
                                        'TIPE': incoming_move_line_id.move_id.product_id.type_id.name or '',
                                        'UKURAN': '',
                                        'URAIAN': incoming_move_line_id.move_id.product_id.name or '',
                                        'SERI IJIN': '',
                                    })
            res.append(['BahanBaku'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE ASAL BAHAN BAKU',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'KODE SATUAN',
                'KODE TARIF',
                'NILAI BAYAR',
                'NILAI FASILITAS',
                'NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    seri_barang = 1
                    for move in picking_id.move_ids_without_package :
                        if not move.production_subcon_id :
                            continue
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        for line in move.move_line_ids :
                            # finished_move_line_id = self.env['stock.move.line'].search([
                            #     ('product_id','=',line.product_id.id),
                            #     ('lot_id','=',line.lot_id.id),
                            #     ('move_id.production_id','!=',False),
                            #     #('state','=','done'),
                            # ], limit=1)
                            # if not finished_move_line_id :
                            #     continue
                            # # ratio = line.qty_done/finished_move_line_id.production_id.product_qty
                            # # for raw_line in finished_move_line_id.production_id.move_raw_ids :
                            # # naik dulu ke move_id baru ke production_id
                            # ratio = line.qty_done/finished_move_line_id.move_id.production_id.product_qty
                            ratio = line.qty_done/move.production_subcon_id.product_qty
                            for raw_line in move.production_subcon_id.move_raw_ids :
                                for move_line in raw_line.active_move_line_ids :
                                    incoming_move_line_id = self.env['stock.move.line'].search([
                                        ('product_id','=',move_line.product_id.id),
                                        ('lot_id','=',move_line.lot_id.id),
                                        ('move_id.production_id','=',False),
                                        ('state','=','done'),
                                        ('move_id.location_id.usage','=','supplier'),
                                    ], limit=1)
                                    if not incoming_move_line_id :
                                        continue
                                    jenis_tarif = False
                                    if incoming_move_line_id.product_id.group_id.tarif_bm :
                                        tarif = incoming_move_line_id.product_id.group_id.tarif_bm
                                        jenis_tarif = 'BM'
                                        kode_fasilitas = 2
                                    elif incoming_move_line_id.product_id.group_id.ppn :
                                        tarif = incoming_move_line_id.product_id.group_id.ppn
                                        jenis_tarif = 'PPN'
                                        kode_fasilitas = 5
                                    elif incoming_move_line_id.product_id.group_id.pph :
                                        tarif = incoming_move_line_id.product_id.group_id.pph
                                        jenis_tarif = 'PPH'
                                        kode_fasilitas = 5
                                    if not jenis_tarif:
                                        raise Warning('Product group belum di set (%s) ' % incoming_move_line_id.product_id.name)
                                    for tax_id in incoming_move_line_id.move_id.product_id.supplier_taxes_id :
                                        values.append({
                                            'NOMOR AJU': aju_id.name or '',
                                            'SERI BARANG': move.seri_barang or '',
                                            'SERI BAHAN BAKU': incoming_move_line_id.move_id.seri_barang or '',
                                            'JENIS TARIF': jenis_tarif,
                                            'JUMLAH SATUAN': '', #TODO: ditanyakan dulu ke pihak BC
                                            'KODE ASAL BAHAN BAKU': '1' if incoming_move_line_id.move_id.purchase_line_id.order_id.purchase_type == 'Local' else '0',
                                            'KODE FASILITAS': '4' if incoming_move_line_id.move_id.purchase_line_id.order_id.purchase_type == 'Local' else '0',
                                            'KODE KOMODITI CUKAI': '',
                                            'KODE SATUAN': '',
                                            'KODE TARIF': incoming_move_line_id.product_id.group_id.kode_bm or '',
                                            'NILAI BAYAR': tax_id._compute_amount(incoming_move_line_id.qty_done*incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.qty_done) or '0.00',
                                            'NILAI FASILITAS': tax_id._compute_amount(incoming_move_line_id.qty_done*incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.move_id.purchase_line_id.price_unit,incoming_move_line_id.qty_done) or '0.00',
                                            'NILAI SUDAH DILUNASI': '',
                                            'TARIF': tax_id.amount or '',
                                            'TARIF FASILITAS': '100.00',
                                        })
            res.append(['BahanBakuTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI BAHAN BAKU',
                'SERI DOKUMEN',
                'KODE ASAL BAHAN BAKU'
            ]
            values = []
            res.append(['BahanBakuDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'ASURANSI',
                'CIF',
                'CIF RUPIAH',
                'DISKON',
                'FLAG KENDARAAN',
                'FOB',
                'FREIGHT',
                'BARANG BARANG LDP',
                'HARGA INVOICE',
                'HARGA PENYERAHAN',
                'HARGA SATUAN',
                'JENIS KENDARAAN',
                'JUMLAH BAHAN BAKU',
                'JUMLAH KEMASAN',
                'JUMLAH SATUAN',
                'KAPASITAS SILINDER',
                'KATEGORI BARANG',
                'KODE ASAL BARANG',
                'KODE BARANG',
                'KODE FASILITAS',
                'KODE GUNA',
                'KODE JENIS NILAI',
                'KODE KEMASAN',
                'KODE LEBIH DARI 4 TAHUN',
                'KODE NEGARA ASAL',
                'KODE SATUAN',
                'KODE SKEMA TARIF',
                'KODE STATUS',
                'KONDISI BARANG',
                'MERK',
                'NETTO',
                'NILAI INCOTERM',
                'NILAI PABEAN',
                'NOMOR MESIN',
                'POS TARIF',
                'SERI POS TARIF',
                'SPESIFIKASI LAIN',
                'TAHUN PEMBUATAN',
                'TIPE',
                'UKURAN',
                'URAIAN',
                'VOLUME',
                'SERI IJIN',
                'ID EKSPORTIR',
                'NAMA EKSPORTIR',
                'ALAMAT EKSPORTIR',
                'KODE PERHITUNGAN'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    jml_type_kemasan = 0
                    packs = []
                    for kemasan in picking_id.kemasan_ids:
                        if kemasan.package_type_id and kemasan.package_type_id.id not in packs:
                            packs.append(kemasan.package_type_id.id)
                            jml_type_kemasan+=1
                        seri_barang = 1
                        jumlah_bahan_baku = 0
                    for move in picking_id.move_ids_without_package:
                        for line in move.move_line_ids :
                            # finished_move_line_id = self.env['stock.move.line'].search([
                            #     ('product_id','=',line.product_id.id),
                            #     ('lot_id','=',line.lot_id.id),
                            #     ('move_id.production_id','!=',False),
                            #     #('state','=','done'),
                            # ], limit=1)
                            if not move.production_subcon_id :
                                continue
                            jumlah_bahan_baku += len(move.production_subcon_id.move_raw_ids)
                        cost_exist = picking_id.subcontract_id.product_ids.filtered(lambda item:item.product_id.id == move.product_id.id)
                        if cost_exist :
                            cost_per_qty = cost_exist[0].cost_per_qty
                        else :
                            cost_per_qty = 0.00
                        move.seri_barang = seri_barang
                        seri_barang += 1
                        kode_kemasan = 'BX'
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI BARANG': move.seri_barang or '',
                            'ASURANSI': '',
                            'CIF': move.quantity_done * cost_per_qty, #* move.sale_line_id.price_unit or '0.00',
                            'CIF RUPIAH': move.quantity_done * cost_per_qty,#move.sale_line_id.currency_id.compute(move.quantity_done * move.sale_line_id.price_unit, move.company_id.currency_id) or '0.00',
                            'DISKON': '',
                            'FLAG KENDARAAN': '',
                            'FOB': '',
                            'FREIGHT': '',
                            'BARANG BARANG LDP': '',
                            'HARGA INVOICE': '',
                            'HARGA PENYERAHAN': '',#move.sale_line_id.price_unit or '',
                            'HARGA SATUAN': cost_per_qty,#self.env.user.convert_to_usd(amount=move.sale_line_id.price_unit, rate_date=move.picking_id.tgl_dokumen),
                            'JENIS KENDARAAN': '',
                            'JUMLAH BAHAN BAKU': jumlah_bahan_baku,
                            'JUMLAH KEMASAN': jml_type_kemasan,
                            # 'JUMLAH SATUAN': move.quantity_done or '',
                            'JUMLAH SATUAN': move.netto_wire or '',
                            'KAPASITAS SILINDER': '',
                            'KATEGORI BARANG': '', #todo
                            'KODE ASAL BARANG': '4',
                            'KODE BARANG': move.product_id.default_code.strip() or '',
                            'KODE FASILITAS': '',
                            'KODE GUNA': '', #todo
                            'KODE JENIS NILAI': '',
                            'KODE KEMASAN': kode_kemasan or '',
                            'KODE LEBIH DARI 4 TAHUN': '0',
                            'KODE NEGARA ASAL': 'ID',
                            'KODE SATUAN': move.product_uom.name or '',
                            'KODE SKEMA TARIF': '',
                            'KODE STATUS': self.bc_id.kode_status or '',
                            'KONDISI BARANG': '', #todo
                            'MERK': move.product_id.default_code or '',
                            'NETTO': move.netto or '',
                            'NILAI INCOTERM': '',
                            'NILAI PABEAN': '',
                            'NOMOR MESIN': '',
                            'POS TARIF': move.product_id.group_id.kode_tarif or '',
                            'SERI POS TARIF': '',
                            'SPESIFIKASI LAIN': move.product_id.group_id.notes if move.product_id.group_id.notes else move.product_id.group_id.name.strip() or '',
                            'TAHUN PEMBUATAN': '',
                            'TIPE': move.product_id.type_id.name or '',
                            'UKURAN': '',
                            'URAIAN': move.product_id.name or '',
                            'VOLUME': move.quantity_done * move.product_id.volume or '',
                            'SERI IJIN': '',
                            'ID EKSPORTIR': '',
                            'NAMA EKSPORTIR': '',
                            'ALAMAT EKSPORTIR': '',
                            'KODE PERHITUNGAN': '0',
                        })
            res.append(['Barang'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'JENIS TARIF',
                'JUMLAH SATUAN',
                'KODE FASILITAS',
                'KODE KOMODITI CUKAI',
                'TARIF KODE SATUAN',
                'TARIF KODE TARIF',
                'TARIF NILAI BAYAR',
                'TARIF NILAI FASILITAS',
                'TARIF NILAI SUDAH DILUNASI',
                'TARIF',
                'TARIF FASILITAS'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    seri_barang = 1
                    for move in picking_id.move_ids_without_package :
                        # if not move.sale_line_id :
                        #     continue
                        jenis_tarif = False
                        if move.product_id.group_id.tarif_bm :
                            tarif = move.product_id.group_id.tarif_bm
                            jenis_tarif = 'BM'
                            kode_fasilitas = 2
                        elif move.product_id.group_id.ppn :
                            tarif = move.product_id.group_id.ppn
                            jenis_tarif = 'PPN'
                            kode_fasilitas = 5
                        elif move.product_id.group_id.pph :
                            tarif = move.product_id.group_id.pph
                            jenis_tarif = 'PPH'
                            kode_fasilitas = 5
                        if not jenis_tarif:
                            raise Warning('Product group belum di set (%s) ' % move.product_id.name)
                        for tax_id in move.product_id.taxes_id :
                            values.append({
                                'NOMOR AJU': aju_id.name or '',
                                'SERI BARANG': seri_barang,
                                'JENIS TARIF': jenis_tarif or '',
                                'JUMLAH SATUAN': '',
                                'KODE FASILITAS': '',
                                'KODE KOMODITI CUKAI': '',
                                'TARIF KODE SATUAN': '',
                                'TARIF KODE TARIF': move.product_id.group_id.kode_bm or '',
                                'TARIF NILAI BAYAR': '0.00',
                                'TARIF NILAI FASILITAS': '',#tax_id._compute_amount(move.quantity_done*move.sale_line_id.price_unit,move.sale_line_id.price_unit,move.quantity_done) or '',
                                'TARIF NILAI SUDAH DILUNASI': '',
                                'TARIF': tax_id.amount or '',
                                'TARIF FASILITAS': '100.00',
                            })
                        seri_barang += 1
            res.append(['BarangTarif'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI BARANG',
                'SERI DOKUMEN'
            ]
            values = []
            res.append(['BarangDokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI DOKUMEN',
                'FLAG URL DOKUMEN',
                'KODE JENIS DOKUMEN',
                'NOMOR DOKUMEN',
                'TANGGAL DOKUMEN',
                'TIPE DOKUMEN',
                'URL DOKUMEN'
            ]
            values = []
            for aju_id in no_aju_ids :
                kanban_id = self.env.user.get_kanban(aju_id=aju_id)
                if kanban_id :
                    for doc in kanban_id.dokumen_ids :
                        values.append({
                            'NOMOR AJU': aju_id.name or '',
                            'SERI DOKUMEN': doc.seri or '',
                            'FLAG URL DOKUMEN': '',
                            'KODE JENIS DOKUMEN': doc.jenis_dokumen or '',
                            'NOMOR DOKUMEN': doc.nomor or '',
                            'TANGGAL DOKUMEN': self.reformat_date(doc.tanggal.strftime(
                                '%Y-%m-%d')) if doc.tanggal else '',
                            'TIPE DOKUMEN': self.bc_id.tipe_dokumen or '',
                            'URL DOKUMEN': '',
                        })
            res.append(['Dokumen'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KEMASAN',
                'JUMLAH KEMASAN',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE JENIS KEMASAN',
                'MEREK KEMASAN',
                'NIP GATE IN',
                'NIP GATE OUT',
                'NOMOR POLISI',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT',
            ]
            values = []
            for aju_id in no_aju_ids :
                kemasan_ids = aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date).mapped('kemasan_ids')
                for kemasan_id in kemasan_ids.filtered(lambda x:x.qty > 0.0) :
                    values.append({
                        'NOMOR AJU': aju_id.name or '',
                        'SERI KEMASAN': '',
                        'JUMLAH KEMASAN': kemasan_id.qty or '0.00',
                        'KESESUAIAN DOKUMEN': '',
                        'KETERANGAN': '',
                        'KODE JENIS KEMASAN': kemasan_id.package_type_id.code.strip() if kemasan_id.package_type_id.code else '',
                        'MEREK KEMASAN': kemasan_id.picking_id.no_sj_pengirim.strip() if kemasan_id.picking_id.no_sj_pengirim else '',
                        'NIP GATE IN': '',
                        'NIP GATE OUT': '',
                        'NOMOR POLISI': '',
                        'NOMOR SEGEL': '',
                        'WAKTU GATE IN': '',
                        'WAKTU GATE OUT': '',
                    })
            res.append(['Kemasan'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE JENIS JAMINAN',
                'KODE KANTOR',
                'NILAI JAMINAN',
                'NOMOR BPJ',
                'NOMOR JAMINAN',
                'PENJAMIN',
                'TANGGAL BPJ',
                'TANGGAL JAMINAN',
                'TANGGAL JATUH TEMPO'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    if picking_id.subcontract_id :
                        subcontract_id = picking_id.subcontract_id
                    values.append({
                        'NOMOR AJU': aju_id.name.strip() or '',
                        'KODE JENIS JAMINAN': '',
                        'KODE KANTOR': self.bc_id.kode_kantor_pabean_asal or '',
                        'NILAI JAMINAN': subcontract_id.nilai_jaminan or '0.00',
                        'NOMOR BPJ': subcontract_id.no_bpj or '',
                        'NOMOR JAMINAN': subcontract_id.no_jaminan or '',
                        'PENJAMIN': subcontract_id.partner_id.display_name or '',
                        'TANGGAL BPJ': self.reformat_date(subcontract_id.tgl_bpj.strftime('%Y-%m-%d')) if subcontract_id.tgl_bpj else '',
                        'TANGGAL JAMINAN': self.reformat_date(subcontract_id.tgl_subkontrak.strftime('%Y-%m-%d')) if subcontract_id.tgl_subkontrak else '',
                        'TANGGAL JATUH TEMPO': self.reformat_date(subcontract_id.jt_tgl_subkontrak.strftime('%Y-%m-%d')) if subcontract_id.jt_tgl_subkontrak else '',
                    })
            res.append(['Jaminan'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'SERI KONTAINER',
                'KESESUAIAN DOKUMEN',
                'KETERANGAN',
                'KODE STUFFING',
                'KODE TIPE KONTAINER',
                'KODE UKURAN KONTAINER',
                'FLAG GATE IN',
                'FLAG GATE OUT',
                'NOMOR POLISI',
                'NOMOR KONTAINER',
                'NOMOR SEGEL',
                'WAKTU GATE IN',
                'WAKTU GATE OUT'
            ]
            values = []
            res.append(['Kontainer'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON',
                'TANGGAL RESPON',
                'WAKTU RESPON',
                'BYTE STRAM PDF'
            ]
            values = []
            res.append(['Respon'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'KODE RESPON',
                'NOMOR RESPON'
            ]
            values = []
            res.append(['Status'] + [headers, values])

            headers = [
                'NOMOR AJU',
                'JENIS TARIF',
                'KODE FASILITAS',
                'NILAI PUNGUTAN'
            ]
            values = []
            for aju_id in no_aju_ids :
                for picking_id in aju_id.picking_ids.filtered(lambda picking: picking.state == 'done' and picking.picking_type_id.id == picking.subcontract_id.picking_type_id.id and picking.tgl_dokumen == self.date):
                    for move in picking_id.move_ids_without_package :
                        # if not move.sale_line_id :
                        #     continue
                        jenis_tarif = False
                        if move.product_id.group_id.tarif_bm :
                            tarif = move.product_id.group_id.tarif_bm
                            jenis_tarif = 'BM'
                            kode_fasilitas = 2
                        elif move.product_id.group_id.ppn :
                            tarif = move.product_id.group_id.ppn
                            jenis_tarif = 'PPN'
                            kode_fasilitas = 5
                        elif move.product_id.group_id.pph :
                            tarif = move.product_id.group_id.pph
                            jenis_tarif = 'PPH'
                            kode_fasilitas = 5
                        if not jenis_tarif:
                            raise Warning('Product group belum di set (%s) ' % move.product_id.name)
                        for tax_id in move.product_id.taxes_id :
                            values.append({
                                'NOMOR AJU': aju_id.name or '',
                                'JENIS TARIF': jenis_tarif or '',
                                'KODE FASILITAS': kode_fasilitas or '',
                                'NILAI PUNGUTAN': '',#tax_id._compute_amount(move.quantity_done*move.sale_line_id.price_unit,move.sale_line_id.price_unit,move.quantity_done) or '',
                            })
            res.append(['Pungutan'] + [headers, values])

            no_aju_ids.write({'has_export':True})
            no_aju_ids.mapped('picking_ids').write({'has_export':True})

        return res
