# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError


class MRPBoMLine(models.Model):
    _inherit = 'mrp.bom.line'
    
    alternative_product_ids = fields.Many2many('product.product',string='Alternative Component', copy=False)

MRPBoMLine()


class MRPBoM(models.Model):
    _inherit = 'mrp.bom'

    @api.model
    def create(self, vals):
        code = False
        if 'code' in vals :
            code = vals['code']
        routing_id = False
        if 'routing_id' in vals :
            routing_id = vals['routing_id']
        bom_exist = self.env['mrp.bom'].sudo().search([('product_tmpl_id', '=', vals['product_tmpl_id']),('code','=',code),('routing_id','=',routing_id)])
        if bom_exist:
            raise Warning('BoM (Product, Reference and Routing) must be unique!')
        bom = super(MRPBoM, self).create(vals)
        return bom
   
    @api.multi
    def write(self, vals):
        code = self.code
        if 'code' in vals :
            code = vals['code']
        product_tmpl_id = self.product_tmpl_id.id
        if 'product_tmpl_id' in vals :
            product_tmpl_id = vals['product_tmpl_id']
        routing_id = self.routing_id.id if self.routing_id else False
        if 'routing_id' in vals :
            routing_id = vals['routing_id']
        bom_exist = self.env['mrp.bom'].sudo().search([('id','!=',self.id),('product_tmpl_id', '=', product_tmpl_id),('code','=',code),('routing_id','=',routing_id)])
        if bom_exist:
            raise Warning('BoM (Product, Reference and Routing) must be unique!')
        bom = super(MRPBoM, self).write(vals)
        return bom

MRPBoM()