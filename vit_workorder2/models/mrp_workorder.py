# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import exceptions
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning, ValidationError
from odoo.addons import decimal_precision as dp


class MrpWorkorder(models.Model):
    _name = 'mrp.workorder'
    _inherit = ["barcodes.barcode_events_mixin", "mrp.workorder"]

    @api.multi
    def open_re_scan(self):
        self.ensure_one()
        action = {
                'name': 'Re Scan',
                'type': 'ir.actions.act_window',
                'res_model': 'mrp.workorder.old',
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new',
        };
        
        return action
        # return {
        #     'type': 'ir.actions.client',
        #     'tag': 'reload_context',
        #     'target': 'new',
        #     'params': {
        #         'model': 'mrp.workorder.old',
        #         'workcenter_id': self.workcenter_id.id,
        #     }
        #     # 'params': {'menu_id': self.env.ref('vit_workorder2.menu_show_old_workorder').id, 'workcenter_id': self.workcenter_id.id},
        # }


    # @api.depends('state','product_id','qty_produced')
    # def _get_outstanding_sale(self):
    #     cr = self.env.cr
    #     for wo in self:
    #         outstanding = 0.0
    #         sql = """select (sum(product_uom_qty)-sum(qty_delivered)) as outstanding 
    #                 from sale_order_line 
    #                 where product_id = %s and state in ('sale','done') """ % (wo.product_id.id)
    #         cr.execute(sql)
    #         res = cr.dictfetchone()
    #         if res :
    #             outstanding = res['outstanding']
    #             if outstanding and outstanding < 0.0 :
    #                 outstanding = 0.0 
    #         self.outstanding_sales = 50

    @api.model
    def create(self,vals):
        if vals['state'] == 'ready' and 'date_planned_start' not in vals :
            vals['date_planned_start'] = fields.Datetime.now()
        return super(MrpWorkorder, self).create(vals)

    # @api.multi
    # def write(self, vals):
    #     cr = self.env.cr
    #     for wo in self :
    #         if wo.product_id :
    #             outstanding = 0.0
    #             sql = """select (sum(product_uom_qty)-sum(qty_delivered)) as outstanding 
    #                     from sale_order_line 
    #                     where product_id = %s and state in ('sale','done') """ % (wo.product_id.id)
    #             cr.execute(sql)
    #             res = cr.dictfetchone()
    #             if res :
    #                 outstanding = res['outstanding']
    #                 if outstanding and outstanding < 0.0 :
    #                     outstanding = 0.0 
    #             #cari customer SO
    #             customer = ''
    #             sql_c = """select rp.name as customer,(sum(sol.product_uom_qty)-sum(sol.qty_delivered)) as outstanding from sale_order_line sol 
    #                         left join sale_order so on so.id=sol.order_id
    #                         left join res_partner rp on rp.id=so.partner_id
    #                         where sol.state in ('sale','done') and sol.product_id = %s 
    #                         group by rp.name """ % (wo.product_id.id)
    #             cr.execute(sql_c)
    #             resc = cr.dictfetchall()
    #             if resc :
    #                 for cus in resc :
    #                     if cus['outstanding'] > 0 :
    #                         customer += cus['customer']+','
    #             cr.execute("update mrp_workorder set outsanding_sales=%s, customer=%s where product_id = %s and production_id= %s ",( outstanding,customer,wo.product_id.id,wo.production_id.id,))
    #     return super(MrpWorkorder, self).write(vals)
                
    # outsanding_sales = fields.Float('Outstanding Sales', compute="_get_outstanding_sale",digits=0, store=True)
    outsanding_sales = fields.Float('Outstanding Sales', digits=dp.get_precision('Product Unit(s)'))
    customer = fields.Char('Customers')
    allow_split = fields.Boolean('Allow Split', related='workcenter_id.allow_split',store=True)
    workorder_split_id = fields.Many2one('mrp.workorder','Workorder Split')
    qty_onhand = fields.Float(string='On Hand', digits=dp.get_precision('Product Unit(s)'))
    # bawaan
    qty_production = fields.Float('Original Production Quantity', readonly=True, related='production_id.product_qty', digits=dp.get_precision('Product Unit(s)'))
    qty_remaining = fields.Float('Quantity To Be Produced', compute='_compute_qty_remaining', digits=dp.get_precision('Product Unit(s)'), store=True)
    qty_produced = fields.Float(
        'Quantity', default=0.0,
        readonly=True,
        digits=dp.get_precision('Product Unit(s)'),
        help="The number of products already handled by this work order")
    qty_producing = fields.Float(
        'Currently Produced Quantity', default=1.0,
        digits=dp.get_precision('Product Unit(s)'),
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    picking_ids = fields.Many2many('stock.picking', compute='_compute_picking_ids', string='Picking consume associated to this work order')
    delivery_count = fields.Integer(string='Delivery Orders', compute='_compute_picking_ids')
    is_wip = fields.Boolean(string="Is Balance WIP", copy=False)
    next_work_order_id_state = fields.Selection(string='Next WO state',related="next_work_order_id.state")
    polybox_ids = fields.One2many('mrp.workorder.polybox','workorder_id',string='Polybox Details',copy=False)
    barcode = fields.Char('Barcode',copy=False)
    warning = fields.Text('Warning', help="warning message")
    picking_id = fields.Many2one('stock.picking','Consume ID', compute='_compute_picking_ids', store=True)
    move_line_ids = fields.One2many(string='Consume Lines', related="picking_id.move_line_ids", store=False)
    consume_ready = fields.Boolean('Consume Ready', copy=False, default=False)
    barcode_wire = fields.Char('Barcode Wire',copy=False)
    force_rw = fields.Boolean('Force Raw Materials',copy=False, default=False)
    lot_temp_line_ids = fields.One2many('stock.production.lot.temp','workorder_id',string='Lot not Found')
    cek_dimensi     = fields.Text(string='Cek Dimensi', track_visibility='onchange',copy=False)
    retur = fields.Char('Retur', track_visibility='onchange',copy=False)
    warna = fields.Selection([('Putih','Putih'),('Kuning','Kuning'),('Hijau','Hijau'),('Pink','Pink')], string='Warna',copy=False)
    workorder_date = fields.Date('Production Date', copy=False,)
    shift = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift", copy=False)
    user_scan_id = fields.Many2one('res.users','User Scan', copy=False)
    bom_wire = fields.Many2one('product.product','BoM Wire', compute='_get_wire_name')
    no_lot   = fields.Char(string='NoLot', track_visibility='onchange',copy=False)

    def _get_wire_name(self):
        for wo in self:
            wire_exist = wo.production_id.move_raw_ids.filtered(lambda move:move.product_id.categ_id.name in ('WIRE','Wire','Wire'))
            if wire_exist:
                # wo.bom_wire =  '['+wire_exist[0].product_id.default_code+'] '+wire_exist[0].product_id.name
                wo.bom_wire = wire_exist[0].product_id.id

    @api.depends('production_id.procurement_group_id')
    def _compute_picking_ids(self):
        for order in self:
            order.picking_ids = self.env['stock.picking'].search([
                ('group_id', '=', order.production_id.procurement_group_id.id),('picking_type_id.name','=','Consume')
            ])
            order.delivery_count = len(order.picking_ids)
            if order.picking_ids :
                order.picking_id = order.picking_ids[0].id

    def action_view_mo_delivery(self):
        """ This function returns an action that display picking related to
        manufacturing order orders. It can either be a in a list or in a form
        view, if there is only one picking to show.
        """
        self.ensure_one()
        action = self.env.ref('stock.action_picking_tree_all').read()[0]
        pickings = self.mapped('picking_ids')
        if len(pickings) > 1:
            action['domain'] = [('id', 'in', pickings.ids)]
        elif pickings:
            action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
            action['res_id'] = pickings.id
        return action


    @api.multi
    def button_start(self):
        self.ensure_one()
        if not self.env.user.has_group('vit_workorder2.group_force_finish_previous_wo') :
            previous_wo = self.search([('next_work_order_id','=',self.id)],limit=1)
            #if self.workcenter_id.routing_validation > 0 :
            now = datetime.now()
            if previous_wo :
                if not previous_wo.date_finished :
                    # if previous_wo.state == 'done':
                    #     previous_wo.date_finished = now
                    #     if not previous_wo.date_start:
                    #         previous_wo.date_start = now
                    # else:
                    routings = self.production_id.workorder_ids.sorted('id')
                    rout = ''
                    disini = ''
                    no = 1
                    for r in routings:
                        state = dict(self._fields['state'].selection).get(r.state)
                        if r.id == previous_wo.id :
                            disini = ' <----'
                        rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
                        disini = ''
                        no += 1
                    if rout :
                        raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
                    # komen dulu, di rubah aturan nya
                    # x = now-previous_wo.date_finished
                    # gap = x.seconds/60
                    # if gap > self.workcenter_id.routing_validation :
                    #     new_gap = round(gap/60,2)
                    #     raise Warning('Proses tidak bisa dilanjutkan karena jeda perpindahan work order melebihi %s jam' % str(new_gap))
            # if not previous_wo :
            #     # jika wo pertama cek materialnya apa sdh di validate
            #     raw_exec = self.production_id.move_raw_ids.filtered(lambda mv:mv.state == 'done')
            #     if not raw_exec :
            #         raise UserError(_("Proses tidak bisa dimulai karena bahan baku belum masuk ke virtual production"))
            for st in self.production_id.routing_id.operation_ids.filtered(lambda i:i.is_consume and i.workcenter_id.id == self.workcenter_id.id) :
                #jika is consume true cek materialnya apa sdh di validate
                raw_exec = self.production_id.move_raw_ids.filtered(lambda mv:mv.state == 'done')
                if not raw_exec and not self.force_rw:
                    #raise exceptions.ValidationError('Not valid message')
                    #raise exceptions.except_orm(_('My Title'), _('My message + lorem ipsum'))
                    #raise exceptions.Warning('Warning message')
                    raise Warning('Proses %s tidak bisa dimulai karena bahan baku belum masuk ke virtual production' % self.workcenter_id.name)
                    #raise UserError(_("Proses %s tidak bisa dimulai karena bahan baku belum masuk ke virtual production")%(self.workcenter_id.name))
        return super(MrpWorkorder, self).button_start()


    @api.multi
    def button_finish(self):
        self.ensure_one()
        if self.workcenter_id.routing_validation > 0 :
            polybox = self.production_id.no_poly_box
            jml_polybox = int(polybox[-1:])
            if len(self.polybox_ids) != jml_polybox :
                raise Warning('Jumlah polybox yang di scan (%s) tidak sama dengan total polybox (%s)' % (len(self.polybox_ids), jml_polybox))
        if not self.env.user.has_group('vit_workorder2.group_force_finish_previous_wo') :
            previous_wo = self.search([('next_work_order_id','=',self.id)],limit=1)
            if previous_wo and previous_wo.state not in ('done','cancel'):
                routings = self.production_id.workorder_ids.sorted('id')
                rout = ''
                disini = ''
                no = 1
                for r in routings:
                    state = dict(self._fields['state'].selection).get(r.state)
                    if r.id == previous_wo.id :
                        disini = ' <----'
                    rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
                    disini = ''
                    no += 1
                raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
                #raise UserError(_("Work Order sebelumnya belum selesai (%s)")%(previous_wo.name))
            if previous_wo :
                sql = "update mrp_workorder set is_wip=false where id=%s" % ( previous_wo.id)
                self.env.cr.execute(sql)
                #previous_wo.write({'is_wip' : False})
            self.write({'is_wip' : True})
            if self.next_work_order_id :
                self.next_work_order_id.write({'date_planned_start' : fields.Datetime.now()})
        else :
            # cek wo sebelumnya yg belum done
            prev_wo = self.search([('production_id','=',self.production_id.id),('id','<',self.id)],order='id asc')
            for prev in prev_wo :
                if prev.state == 'ready' :
                    prev.button_start()
                    prev.record_production()
                elif prev.state == 'progress' :
                    prev.record_production()
        return super(MrpWorkorder, self).button_finish()

    @api.multi
    def record_production(self):
        res = super(MrpWorkorder, self).record_production()
        if self.state == 'done':
            previous_wo = self.search([('next_work_order_id','=',self.id)],limit=1)
            if previous_wo and previous_wo.state not in ('done','cancel'):
                if not self.env.user.has_group('vit_workorder2.group_force_finish_previous_wo') :
                    routings = self.production_id.workorder_ids.sorted('id')
                    rout = ''
                    disini = ''
                    no = 1
                    for r in routings:
                        state = dict(self._fields['state'].selection).get(r.state)
                        if r.id == previous_wo.id :
                            disini = ' <----'
                        rout += (str(no) +'. '+r.name + ' ('+state+') '+ disini +' \n')
                        disini = ''
                        no += 1
                    raise Warning('Work Order sebelumnya belum selesai (%s) \n Routings : \n %s' % (previous_wo.name, rout))
                    #raise UserError(_("Work Order sebelumnya belum selesai (%s)")%(previous_wo.name))
            if previous_wo :
                sql = "update mrp_workorder set is_wip=false where id=%s" % ( previous_wo.id)
                self.env.cr.execute(sql)
                #previous_wo.write({'is_wip' : False})
            #self.write({'is_wip' : True})
            sql = "update mrp_workorder set is_wip=true where id=%s" % ( self.id)
            self.env.cr.execute(sql)
            if self.next_work_order_id :
                self.next_work_order_id.write({'date_planned_start' : fields.Datetime.now()})
        return res

    @api.multi
    def button_split_workorder(self,split_qty):
        new_wo = self.copy({'qty_producing': split_qty})
        self.qty_producing = self.qty_producing-split_qty
        self.workorder_split_id = new_wo.id
        return new_wo

    @api.multi
    def button_force_rw(self):
        self.ensure_one()
        if not self.lot_temp_line_ids:
            raise UserError(_("Lot harus di scan terlebih dahulu !"))
        else:
            #self.force_rw = True
            sql = "update mrp_workorder set force_rw=true where production_id=%s" % ( self.production_id.id)
            self.env.cr.execute(sql)
        return True

    def _add_user_and_machine(self, barcode):
        #step 1 make sure order in proper state.
        if self and self.state in ["cancel","done"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state), self.state)
            raise UserError(_("You can not scan item in %s state.") %(value))
        #step 2 increaset product qty by 1 if product not in order line than create new order line.
        elif self:
            # cr = self.env.cr
            # cr.execute("SELECT login FROM res_users WHERE login=%s", (barcode))
            # user = cr.fetchone()
            # if user and user != None:
            #     user = user[0]
            #     self.iduser = user
            # self.barcodewo = barcode
            # self.iduser = barcode
            machine = self.env['mrp.machine'].sudo().search([('code','=',barcode)],limit=1)
            if machine :
                if machine.workcenter_id and machine.workcenter_id.id != self.workcenter_id.id :
                    raise UserError(_("Workcenter mesin %s berbeda dengan workcenter ini (%s)") %(machine.name,self.workcenter_id.name))
                self.barcodewo = machine.code
                self.machine_id = machine.id
            user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
            # if not user :
            #     raise ValidationError("Nomor operator / mesin tidak di temukan")
            if user :
                self.iduser = user.login
                self.user_id = user.id
            else :
                barcode_scan = barcode.split('#')
                mo_number = barcode_scan[0].strip()
                production_id = self.env['mrp.production'].sudo().search([('company_id','=', self.env.user.company_id.id),('name','=',mo_number)],limit=1)
                if not production_id :
                    production_id = self.env['mrp.production'].sudo().search([('company_id','!=', self.env.user.company_id.id),('name','=',mo_number)],limit=1)
                if not production_id :
                    raise Warning('Barcode %s tidak di temukan di data Manufacturing Order' % (mo_number))
                if production_id.id != self.production_id.id :
                    raise Warning('Barcode %s berbeda dengan nomor MO dokumen ini ' % (mo_number,self.production_id.name))
                poly = False
                if len(barcode_scan) > 1 :
                    poly = barcode_scan[1].strip()
                if not poly :
                    raise Warning('Barcode %s tidak ditemukan no polybox' % (mo_number))
                vals = {
                        'workorder_id':self.id,
                        'polybox': poly[:3],
                        'mo_name': mo_number,
                        'name': self.barcode,
                        'date': fields.Datetime.now(),
                    }
                if self.polybox_ids.filtered(lambda i:i.mo_name == mo_number and i.polybox == poly) :
                    raise Warning('Barcode (%s) sudah diinsert/scan' % (barcode))                      
                new_polybox_line = self.polybox_ids.new(vals)
                self.polybox_ids |= new_polybox_line
                self.barcode = False
                self.polybox_ids.env.cr.commit()


    def on_barcode_scanned(self, barcode):   
        #self._add_user_and_machine(barcode)
        self.barcode = barcode

    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode:
            # <800 dan company login =TGR -- > Valid
            # <800 dan company login =BKS -- > Tidak Valid
            # >=800 dan company login = BKS -->  Valid
            # >=800 dan company login = TGR --> Tdk Valid
            try :
                int_barcode = int(self.barcode)
                if int_barcode < 800 and self.env.user.company_id.second_name != 'NAI TGR' :
                    self.warning = "Workcenter mesin %s harus login di NAI TGR !"%(int_barcode)
                    return
                elif int_barcode >= 800 and self.env.user.company_id.second_name != 'NAI BKS' :
                    self.warning = "Workcenter mesin %s harus login di NAI BKS !"%(int_barcode)
                    return
                machine = self.env['mrp.machine'].sudo().search([('code','=',self.barcode)],limit=1)
                if machine :
                    if machine.workcenter_id and machine.workcenter_id.id != self.workcenter_id.id :
                        self.warning = "Workcenter mesin %s berbeda dengan workcenter ini (%s) !"%(machine.name,self.workcenter_id.name)
                        return
                        #raise UserError(_("Workcenter mesin %s berbeda dengan workcenter ini (%s)") %(machine.name,self.workcenter_id.name))
                    self.barcodewo = machine.code
                    self.machine_id = machine.id
                    self.barcode = False
                    self.warning = False
            except:
                # if not user :
                #     raise ValidationError("Nomor operator / mesin tidak di temukan")
                user = self.env['res.users'].sudo().search([('login','=',self.barcode)],limit=1)
                lot = self.env['stock.production.lot'].sudo().search([('name','=',self.barcode)],limit=1)
                if user :
                    self.iduser = user.login
                    self.user_id = user.id
                    self.barcode = False
                    self.warning = False
                elif lot :
                    if self.picking_id and self.picking_id.move_line_ids:
                        if self.picking_id.move_line_ids[0].product_id != lot.product_id :
                            self.warning ="Product consume %s berbeda dengan lot %s "%(self.product_id.name, lot.product_id.name)
                            self.barcode = False
                        self.picking_id.move_line_ids[0].write({'lot_id':lot.id,
                                                                'qty_done':self.picking_id.move_line_ids[0].move_id.product_uom_qty})
                        # jika update lintas objek harus pake write
                        # self.picking_id.move_line_ids[0].lot_id = lot.id
                        #self.picking_id.move_line_ids[0].qty_done = self.picking_id.move_line_ids[0].product_uom_qty
                        self.consume_ready = True
                        self.barcode = False
                        self.warning = False
                    else :
                        self.warning ="Dokumen consume %s berstatus %s "%(self.picking_id.name, self.picking_id.state)
                        self.barcode = False
                else :
                    barcode_scan = self.barcode.split('#')
                    mo_number = barcode_scan[0].strip()
                    production_id = self.env['mrp.production'].sudo().search([('company_id','=', self.env.user.company_id.id),('name','=',mo_number)],limit=1)
                    if not production_id :
                        production_id = self.env['mrp.production'].sudo().search([('company_id','!=', self.env.user.company_id.id),('name','=',mo_number)],limit=1)
                    if not production_id :
                        self.barcode = False
                        self.warning = "Barcode %s tidak di temukan di data Manufacturing Order !"%mo_number
                        return
                        # raise Warning('Barcode %s tidak di temukan di data Manufacturing Order' % (mo_number))
                    if production_id.id != self.production_id.id :
                        self.barcode = False
                        self.warning = "Barcode %s berbeda dengan nomor MO dokumen ini !"%mo_number
                        return
                        #raise Warning('Barcode %s berbeda dengan nomor MO dokumen ini ' % (mo_number,self.production_id.name))
                    poly = False
                    if len(barcode_scan) > 1 :
                        poly = barcode_scan[1].strip()
                    if not poly :
                        self.barcode = False
                        self.warning = "Barcode %s tidak ditemukan no polybox !"%mo_number
                        return
                        #raise Warning('Barcode %s tidak ditemukan no polybox' % (mo_number))
                    # vals = {
                    #         'workorder_id':self._origin.id,
                    #         'polybox': poly[:3],
                    #         'name': self.barcode,
                    #         'mo_name': mo_number,
                    #         'date': fields.Datetime.now(),
                    #     }
                    try:
                        poly = poly[:3]
                    except :
                        self.barcode = False
                        self.warning = "No Polybox %s tidak dikenal !"%(poly)
                        return
                    vals = {
                            'workorder_id':self.id,
                            'polybox': poly[:3],
                            'name': self.barcode,
                            'mo_name': mo_number,
                            'date': fields.Datetime.now(),
                        }
                    if self.polybox_ids.filtered(lambda i:i.mo_name == mo_number and i.polybox == poly) :
                        self.barcode = False
                        self.warning = "No MO (%s) dengan no polybox (%s) sudah diinsert/scan !"%(mo_number,poly)
                        return
                        #raise Warning('No MO (%s) dengan no polybox (%s) sudah diinsert/scan' % (mo_number,poly)) 

                    #new_polybox_line = self.polybox_ids.create(vals)
                    new_polybox_line = self.polybox_ids.new(vals)
                    self.polybox_ids |= new_polybox_line
                    self.barcode = False
                    self.warning = False
                    # langsung commit
                    #self.polybox_ids.env.cr.commit()

    @api.onchange('barcode_wire')
    def onchange_barcode_wire(self):
        if self.barcode_wire:
            note = ''
            wire_tidak_sesuai = False
            wire = self.barcode_wire.split("#")
            if len(wire) == 6 :
                barcode_wire = '#'.join(wire[:5])
            else :
                barcode_wire=self.barcode_wire
            lot_wire = self.env['stock.production.lot.temp'].sudo().search([('name','=',self.barcode_wire)],limit=1)
            if lot_wire :
                self.warning ="Barcode wire %s sudah pernah di assign di Manufacturing Order %s (%s) "%(self.barcode_wire, lot_wire.workorder_id.production_id.name,lot_wire.workorder_id.name)
                self.barcode_wire = False
            else :
                alternative_product_ids = []
                if self.production_id.bom_id.bom_line_ids and self.production_id.bom_id.bom_line_ids.filtered(lambda alt:alt.alternative_product_ids) :
                    alternative_product_ids = self.production_id.bom_id.bom_line_ids.filtered(lambda alt:alt.alternative_product_ids)[0].alternative_product_ids.ids
                lot = self.env['stock.production.lot'].sudo().search([('name','=',barcode_wire)],limit=1)
                if lot :
                    if self.picking_id and self.picking_id.move_line_ids:
                        if self.picking_id.move_line_ids[0].product_id != lot.product_id :
                            self.warning ="Product consume %s berbeda dengan lot %s "%(self.product_id.name, lot.product_id.name)
                            self.barcode_wire = False
                        self.picking_id.move_line_ids[0].write({'lot_id':lot.id,
                                                                'qty_done':self.picking_id.move_line_ids[0].move_id.product_uom_qty,
                                                                'workorder_id':self.id})
                        # jika update lintas objek harus pake write
                        # self.picking_id.move_line_ids[0].lot_id = lot.id
                        #self.picking_id.move_line_ids[0].qty_done = self.picking_id.move_line_ids[0].product_uom_qty
                        self.consume_ready = True
                        self.barcode_wire = False
                        self.warning = False
                    else :
                        if self.lot_temp_line_ids.filtered(lambda i:i.name == lot.name):
                            self.barcode_wire = False
                        else:
                            status = self.picking_id.state
                            if status == 'confirmed':
                                status = 'Waiting Available'
                            if alternative_product_ids and lot.product_id.id not in alternative_product_ids and lot.product_id.id != self.bom_wire.id :
                                #status  = "Wire tidak sesuai "+'['+lot.product_id.default_code+'] '+lot.product_id.name
                                self.warning ="Wire tidak sesuai dengan BoM dan alternative wire "+'['+lot.product_id.default_code+'] '+lot.product_id.name
                                self.barcode_wire = False
                                return
                            #vals = {'name': lot.name, 'note': status,'product_lot_id': lot.product_id.id}
                            vals = {'name': self.barcode_wire, 'note': status,'product_lot_id': lot.product_id.id,'qty': self.product_id.kg_pal}
                            if alternative_product_ids and lot.product_id.id in alternative_product_ids:
                                status  = "Wire menggunakan alternative component "
                                vals.update({'alternative_product_ids': [(6, 0, alternative_product_ids)], 'note': status})
                            new_lot_line = self.lot_temp_line_ids.new(vals)
                            self.lot_temp_line_ids |= new_lot_line
                            self.barcode_wire = False
                            self.warning = False
                            # self.warning ="Dokumen consume %s berstatus %s "%(self.picking_id.name, status)
                            # self.barcode_wire = False
                        
                else :
                    if self.lot_temp_line_ids.filtered(lambda i:i.name == self.barcode_wire):
                        self.barcode_wire = False
                    elif self.env['stock.production.lot.temp'].sudo().search([('name','=',self.barcode_wire)],limit=1) :
                        self.warning ="Barcode wire %s sudah pernah di assign di Manufacturing Order %s - %s "%(self.barcode_wire, self.production_id.name,self.name)
                        self.barcode_wire = False
                    else:
                        note = 'Lot tidak ditemukan di master data'
                        wire_id = False
                        if wire[0] != self.bom_wire.default_code :
                            wire_id = self.env['product.product'].search([('default_code','=',wire[0])],limit=1)
                            if wire_id :
                                if alternative_product_ids and wire_id.id in alternative_product_ids:
                                    note  = "Wire menggunakan alternative component "
                                else :
                                    self.warning ="Wire tidak sesuai dengan BoM dan alternative wire "+'['+wire_id.default_code+'] '+wire_id.name
                                    self.barcode_wire = False
                                    return
                                    # note = "Wire tidak sesuai "+'['+wire_id.default_code+'] '+wire_id.name
                                    # wire_tidak_sesuai = True
                        vals = {'name': self.barcode_wire, 'note': note, 'qty': self.product_id.kg_pal,'wire_tidak_sesuai':wire_tidak_sesuai}
                        if alternative_product_ids and wire_id and wire_id.id in alternative_product_ids:
                            note  = "Wire menggunakan alternative component "
                            vals.update({'alternative_product_ids': [(6, 0, alternative_product_ids)],'note': note})
                        product_exist = self.env['product.product'].sudo().search([('default_code','=',wire[0])],limit=1)
                        if product_exist:
                            if alternative_product_ids and product_exist.id not in alternative_product_ids and product_exist.id != self.bom_wire.id :
                                self.warning ="Wire tidak sesuai dengan BoM dan alternative wire "+'['+product_exist.default_code+'] '+ product_exist.name
                                self.barcode_wire = False
                            vals.update({'product_lot_id': product_exist.id})
                        if len(wire) == 6 :
                            self.warning = False
                            new_lot_line = self.lot_temp_line_ids.new(vals)
                            self.lot_temp_line_ids |= new_lot_line
                        else :
                            self.warning = "Jumlah kolom (dengan saparator #) barcode wire tidak berjumlah 6 kolom [%s]"%(self.barcode_wire)
                        self.barcode_wire = False

    @api.multi
    def reload(self):
        return {'type': 'ir.actions.client', 'tag': 'reload'}

    @api.multi
    def validate_consume(self):
        self.picking_id.action_done()

    def action_confirm_workorder_repair(self):
        view = self.env.ref('vit_workorder2.view_workorder_repair_confirmation')
        wiz = self.env['mrp.workorder.repair'].create({'workorder_id': self.id})
        return {
            'name': _('Workorder Data Repair'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mrp.workorder.repair',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz.id,
            'context': self.env.context,
        }


MrpWorkorder()


class MrpWorkcenter(models.Model):
    """
    mrp.workcenter inherit
    Inherited from model mrp.workcenter for some reasons.
    """
    _inherit = 'mrp.workcenter'

    allow_split = fields.Boolean('Allow Split')
    routing_validation = fields.Integer('Routing Validation Time (Minutes)')
    user_ids = fields.Many2many('res.users',string='Allowed Users')

MrpWorkcenter() 


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.depends('workorder_ids.no_lot')
    def _get_no_lot(self):
        for mrp in self:
            if mrp.workorder_ids and mrp.workorder_ids.filtered(lambda wo:wo.no_lot):
                mrp.no_lot = mrp.workorder_ids.filtered(lambda wo:wo.no_lot)[0].no_lot
                
    no_lot   = fields.Char(string='NoLot', compute='_get_no_lot',store=True)
    product_qty = fields.Float(
        'Quantity To Produce',
        default=1.0, digits=dp.get_precision('Product Unit(s)'),
        readonly=True, required=True, track_visibility='onchange',
        states={'confirmed': [('readonly', False)]})
    product_uom_qty = fields.Float(string='Total Quantity', compute='_compute_product_uom_qty', store=True,digits=dp.get_precision('Product Unit(s)'))

    @api.multi
    def button_plan(self):
        res = super(MrpProduction, self).button_plan()
        for x in self :
            wo_id = x.workorder_ids.sorted('id')[0]
            wo_id.write({'is_wip' : True})
        return res

MrpProduction()


class MrpRoutingWorkcenter(models.Model):
    _inherit = 'mrp.routing.workcenter'

    is_consume = fields.Boolean('Is Consume')

MrpRoutingWorkcenter()


class MrpWorkorderPolybox(models.Model):
    _name = 'mrp.workorder.polybox'
    _description = 'Workorder Polybox List'

    workorder_id = fields.Many2one('mrp.workorder','Workorder', ondelete='cascade')
    name = fields.Char('barcode', required=True)
    mo_name = fields.Char('Manufacturing Order', required=False)
    polybox = fields.Char('No Polybox', required=False)
    date = fields.Datetime('Date')
    duration = fields.Float('Duration (Minutes)',compute="_get_duration_and_status", store=True)
    status = fields.Char('Status',compute="_get_duration_and_status", store=True)

    _sql_constraints = [
        ('name', 'unique(name,polybox,workorder_id)',
         'Barcode of name, polybox, and workorder must be unique'),
    ]

    @api.depends('date','mo_name','polybox','workorder_id')
    def _get_duration_and_status(self):
        for wo in self:
            wo.status = '-'
            if wo.workorder_id.workcenter_id.routing_validation > 0 :
                previous_wo = self.env['mrp.workorder.polybox'].search([('workorder_id.next_work_order_id','=',wo.workorder_id.id),
                                                                        ('mo_name','=',wo.mo_name),
                                                                        ('polybox','=',wo.polybox)],limit=1)
                #now = datetime.now()
                now = wo.date
                if previous_wo :
                    x = now-previous_wo.date
                    gap = x.seconds/60
                    if x.days :
                        gap = gap+ (x.days*1440)
                    wo.duration = round(gap,2)
                    if gap > wo.workorder_id.workcenter_id.routing_validation :
                        wo.status = 'NOT GOOD'
                    else :
                        wo.status = 'GOOD'

MrpWorkorderPolybox()