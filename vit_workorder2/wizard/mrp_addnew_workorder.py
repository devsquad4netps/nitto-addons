# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning, ValidationError
import time

class MrpWorkorderOld(models.TransientModel):
    _name = 'mrp.workorder.old'

    @api.model
    def _get_default_workcenter_id(self):
        workcenter_id = False
        if 'active_model' in self._context :
            if self._context.get('active_model') == 'mrp.workorder' :
                workorder = self.env['mrp.workorder'].browse(self._context.get('active_id'))
                workcenter_id = workorder.workcenter_id.id
        if not workcenter_id :
            workcenter_id = self._context.get('active_id',False)
        if workcenter_id :
            return workcenter_id

    name = fields.Char('Number',size=250)
    workorder_id = fields.Many2one('mrp.workorder','Workorder')
    production_id = fields.Many2one('mrp.production','Production')
    workcenter_id = fields.Many2one('mrp.workcenter','Workcenter', default=_get_default_workcenter_id)

    @api.multi
    def insert_log(self,text_scan,errordesc):
        env = self.env
        create_date = time.strftime("%Y-%m-%d %H:%M:%S")
        for rec in self:
            sql = """INSERT INTO "mrp_workorder_log" (
                "create_uid", "write_uid", "create_date", "write_date",
                "user_id", 
                "text_scan",
                "errordesc",
                "workcenter_id"
                ) VALUES     
                """
            sql = sql + str((env.uid, env.uid, create_date, create_date, env.uid, text_scan, errordesc, self.workcenter_id.id))
            env.cr.execute(sql)
            env.cr.commit()


    @api.multi
    @api.onchange('name')
    def _onchange_name(self):
        for rec in self:
            if rec.name :
                new_name = rec.name.split(';')
                if len(new_name) >= 4 :
                    new_name = new_name[3]
                else :
                    new_name = rec.name.split('#')[0]

                # if len(rec.name) < 17 :
                #     raise Warning('Digit barcode minimal 10 digit !')
                # old_wo = False
                # if len(rec.name) > 17 :
                old_mo = False
                barcode = new_name.upper()
                old_wo = self.env['mrp.workorder'].sudo().search([('origin','=',barcode),('workcenter_id','=',rec.workcenter_id.id)],order='id desc',limit=1)
                if not old_wo :
                    #raise Warning('Old Work Order %s not found !' % barcode)
                    old_wo = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode),('workcenter_id','=',rec.workcenter_id.id)],order='id desc',limit=1)
                if not old_wo :
                    old_wo = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode)],order='id desc',limit=1)
                    if old_wo and rec.workcenter_id.id not in old_wo.production_id.workorder_ids.mapped('workcenter_id').ids:
                        raise Warning("Tidak ada proses '%s' di nomor MO %s, silahkan scan diproses yang lain." % (rec.workcenter_id.name, barcode))
                    else: 
                        rec.insert_log(barcode.upper(),'Old Work Order '+barcode.upper()+' not found')
                        raise Warning('Manufacturing Order %s belum tersedia di sistem.' % barcode.upper())
                #     old_wo = self.env['mrp.workorder'].sudo().search([('origin','=',barcode)],limit=1)
                # if not old_wo :
                #     old_wo = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode)],limit=1)
                # if not old_wo :
                #     old_wo = self.env['mrp.workorder'].sudo().search([('production_id.origin','=',barcode)],limit=1)
                # if not old_wo :
                #     old_mo = self.env['mrp.production'].sudo().search([('origin','=',barcode)],limit=1)
                #     rec.production_id = old_mo.id
                #     if not old_mo :
                #         old_mo = self.env['mrp.production'].sudo().search([('name','=',barcode)],limit=1)
                #         rec.production_id = old_mo.id
                if not old_wo and not old_mo:
                    rec.insert_log(barcode.upper(),'Work Order '+rec.name.upper()+' not found')
                    raise Warning('Manufacturing Order %s belum tersedia di sistem.' % rec.name.upper())

                if old_wo :
                    rec.workorder_id = old_wo.id
                    if len(old_wo) > 1 :
                        list_wo = old_wo.mapped('production_id.name')
                        rec.insert_log(barcode.upper(),'Multiple Work Order number found '+list_wo)
                        raise Warning('Manufacturing Order ini belum tersedia di sistem. %s !' % list_wo)
                    view_id = self.env.ref('mrp.mrp_production_workorder_form_view_inherit').id
                    return rec.action_show_wo()
                else :
                    view_id = self.env.ref('mrp.mrp_production_workorder_form_view_inherit').id
                    return rec.action_show_wo()

    @api.multi
    def action_show_wo(self):
         for rec in self:
            # if rec.name :
            #     # if len(rec.name) < 17 :
            #     #     raise Warning('Digit barcode minimal 10 digit !')
            #     old_wo = False
            #     if len(rec.name) > 17 :
            #         barcode = rec.name[:17]
            #         old_wo = self.env['mrp.workorder'].sudo().search([('origin','=',barcode),('workcenter_id','=',rec.workcenter_id.id)])
            #     if not old_wo :
            #         #raise Warning('Old Work Order %s not found !' % barcode)
            #         old_wo = self.env['mrp.workorder'].sudo().search([('production_id.name','=',rec.name),('workcenter_id','=',rec.workcenter_id.id)])
            #     if not old_wo :
            #         raise Warning('Old Work Order %s not found !' % rec.name)
            #     if len(old_wo) > 1 :
            #         list_wo = old_wo.mapped('production_id.name')
            #         raise Warning('Multiple Work Order number found %s !' % list_wo)
            #     else :

            if rec.workorder_id :
                old_wo = rec.workorder_id
                # tambah logic set wo date
                now = datetime.date(datetime.now())
                # lot_scan = self.env['mrp.workorder'].sudo().search([('workcenter_id','=',old_wo.workcenter_id.id),
                #                                                     ('user_scan_id','=',self.env.user.id),
                #                                                     ('workorder_date','!=',False)],order='workorder_date desc',limit=1)
                lot_scan = self.env['mrp.workorder'].sudo().search([('user_scan_id','=',self.env.user.id),
                                                    ('workorder_date','!=',False)],order='write_date desc, workorder_date desc',limit=1)
                if lot_scan :
                    gap = (now-lot_scan.workorder_date).days
                    old_wo.shift = lot_scan.shift
                    if gap <= 1 :
                        old_wo.workorder_date = lot_scan.workorder_date
                        #old_wo.shift = lot_scan.shift
                    else:
                        old_wo.workorder_date = now
                        #old_wo.shift = False
                else :
                    old_wo.workorder_date = now
                    old_wo.shift = False
                old_wo.user_scan_id = self.env.user.id
                view_id = self.env.ref('mrp.mrp_production_workorder_form_view_inherit').id
                res = {
                        'type': 'ir.actions.act_window',
                        'tag': 'reload',
                        'name': 'Work Order',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'mrp.workorder',
                        'views': [(view_id, 'form')],
                        'res_id': old_wo.id,
                        'target': 'current',
                        'context' : {'workorder_date': now}
                    }
            elif rec.production_id : 
                old_mo = rec.production_id
                view_id = self.env.ref('mrp.mrp_production_form_view').id
                res = {
                        'type': 'ir.actions.act_window',
                        'name': 'Manufacturing Order',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'mrp.production',
                        'views': [(view_id, 'form')],
                        'res_id': old_mo.id,
                        'target': 'current',
                    }
            return res


MrpWorkorderOld()


class MrpWorkorderAddWizard(models.TransientModel):
    _name = 'mrp.workorder.add.wizard'
    _description = 'Add New Workorder'

    @api.model
    def _get_default_workorder_id(self):
        return self._context.get('active_id')

    @api.onchange('new_workcenter_id')
    def onchange_domain_new_workcenter_id(self):
        domain = []
        #all_wc = self.env['mrp.workcenter'].sudo().search([('id','not in',[self.workorder_id.workcenter_id.id,self.workorder_id.next_work_order_id.workcenter_id.id])])
        if self.workorder_id.next_work_order_id :
            all_wc = self.env['mrp.workcenter'].sudo().search([('id','not in',[self.workorder_id.next_work_order_id.workcenter_id.id])])
            domain = {'domain': {'new_workcenter_id': [('id', 'in', all_wc.ids)]}}
        return domain


    new_workcenter_id = fields.Many2one('mrp.workcenter', 'Workcenter')
    workorder_id = fields.Many2one('mrp.workorder', 'Work Order', required=True, default=_get_default_workorder_id)

    @api.multi
    def add_new_workorder(self):
        self.ensure_one()
        qty_producing = self.workorder_id.qty_produced_real
        if qty_producing <= 0.0 :
            qty_producing = self.workorder_id.qty_produced
        new_workorder_id = self.workorder_id.copy({'workcenter_id' : self.new_workcenter_id.id, 
                                                    'state': 'ready',
                                                    'name' : self.new_workcenter_id.name,
                                                    'qty_produced' : 0.0,
                                                    'qty_produced_real' : 0.0,
                                                    'qty_producing' : qty_producing,
                                                    'date_start' : False,
                                                    'date_finished' : False,
                                                    'capacity': self.new_workcenter_id.capacity,
                                                    'date_planned_start' :fields.Datetime.now()})
        sql = "update mrp_workorder set next_work_order_id=%s where id=%s" % ( new_workorder_id.id,self.workorder_id.id)
        self.env.cr.execute(sql)
        self.workorder_id.next_work_order_id.write({'state':'pending','date_planned_start':False})
        return {
                'name': _('New Workorder'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'mrp.workorder',
                'res_id': new_workorder_id.id,
                'type': 'ir.actions.act_window',
            }

MrpWorkorderAddWizard()


#Log scan WO
class MrpWorkorderLog(models.Model):
    _name = 'mrp.workorder.log'
    _desc = 'MRP Scan Log'

    logdate = fields.Datetime('Date')
    workorder_id = fields.Integer('Workorder')
    workcenter_id = fields.Integer('Workcenter')
    user_id = fields.Integer('User')
    text_scan = fields.Char('Scan Text')
    errordesc = fields.Char('Error Text')


MrpWorkorderLog()