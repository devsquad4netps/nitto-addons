# Copyright 2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models,fields


class UUDP(models.Model):
    _name = "uudp"
    _inherit = ['uudp', 'tier.validation']
    _state_from = ['draft']
    _state_to = ['confirm']

    partner_id = fields.Many2one('res.partner','Partner')#syarat module tier harus ada partner_id(untuk kirim message)

UUDP()