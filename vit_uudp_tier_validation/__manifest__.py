# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "UUDP Tier Validation",
    "summary": "Extends the functionality of UUDP to "
               "support a tier validation process.",
    "version": "12.0.1.0.0",
    "category": "Expense",
    "website": "https://vitraining.com",
    "author": "vITraining",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "vit_uudp",
        "base_tier_validation",
    ],
    "data": [
        "views/uudp_view.xml",
    ],
}
