from odoo import api, fields, models
from datetime import datetime, date, timedelta

class ReportSalesWithAmount(models.AbstractModel):
    _name = 'report.bmo_report_so_po.report_sales_with_amount_view'
    
    @api.model
    def _get_report_values(self, docids, data=None):
        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        name = 'bmo_report_so_po.template_bmo_report_sale_wizard'
        
        report = self.env['ir.actions.report']._get_report_from_name(name)
        docs = self.env[report.model].browse(docids)
        print('XXXXXXXXXXXXXXXXXXXXXXXX')
        
        return {
            'doc_model': 'bmo.report.sale.wizard',
        }