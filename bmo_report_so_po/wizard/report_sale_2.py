from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import itertools
import base64
import pytz
import xlwt
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportSale2Wizard(models.TransientModel):
    _inherit = "vit.report.sale2.wizard"
    
    @api.onchange('type')
    def onchange_type(self):
        if self.type:
            if self.type == 'balance_so' :
                self.date_start =fields.Date.to_string(date.today().replace(day=1))
                # self.date_start = '2019-01-01'
            else :
                self.date_start =fields.Date.to_string(date.today().replace(day=1))

    @api.multi
    def action_print_report_amount(self):
        book = xlwt.Workbook(encoding="utf-8")
        title = self._description

        sheet = book.add_sheet(title, cell_overwrite_ok=True)
        sheet.normal_magn = 80
        sheet.show_grid = False
        
        style_header = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz center;borders: bottom_color black,bottom thin;')
        style_judul = xlwt.easyxf('font: colour black, bold 1, height 280, name Helvetica Neue; align: vert centre, horz left;')
        style_judul_2 = xlwt.easyxf('font: colour black, bold 1, height 180, name Helvetica Neue; align: vert center, horz left;')
        style_garis = xlwt.easyxf('align: vert centre, horz center; borders: top_color black,top double;')
        style_no_bold = xlwt.easyxf('align: vert centre, horz center;')
        style_isi_bold = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz left;')
        style_isi_kiri = xlwt.easyxf('align: vert centre, horz left;')
        style_isi_kanan = xlwt.easyxf('align: vert centre, horz right;')
        style_isi_kanan.num_format_str = '#,##0.0000'
        style_isi_kanan_bold = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz right; borders: top thin;')
        style_isi_kanan_bold.num_format_str = '#,##0.0000'
        style_isi_kanan_new = xlwt.easyxf('align: vert centre, horz right;')
        style_isi_kanan_new.num_format_str = '#,##0'
        style_isi_kanan_bold_new = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz right; borders: top thin;')
        style_isi_kanan_bold_new.num_format_str = '#,##0'
        
        col_width = 256 * 40
        row = 8
        try:
            for i in itertools.count():
                sheet.col(i).width = col_width
                sheet.col(0).width = 256 * 5
                sheet.col(1).width = 256 * 40
                sheet.col(2).width = 256 * 20
                sheet.col(3).width = 256 * 20
                sheet.col(4).width = 256 * 15
                sheet.col(5).width = 256 * 15
                sheet.col(6).width = 256 * 20
                sheet.col(7).width = 256 * 20
                sheet.col(8).width = 256 * 20
                sheet.col(9).width = 256 * 20
                sheet.col(10).width = 256 * 20
                sheet.row(i).height = 5 * 75
        except ValueError:
            pass
        
        list_partner = []
        data = []
        where_partner = '1=1'
        where_company = '1=1'
        where_period = '1=1'
        company = ""
        if self.partner_ids:
            if len(self.partner_ids) > 1:
                where_partner = "so.partner_id IN %s" % str(tuple(self.partner_ids.ids))
            else:
                where_partner = "so.partner_id = %s" % str(self.partner_ids.id)
        if self.company_ids:
            for comp in self.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
                company = company[:-3]
            if len(self.company_ids) > 1:
                where_company = "so.company_id IN %s" % str(tuple(self.company_ids.ids))
            else:
                where_company = "so.company_id = %s" % str(self.company_ids.id)
        if self.date_start and self.date_end:
            if self.date_start > self.date_end:
                raise UserError('Start Date harus sebelum End Date.')
            else:
                date_start = (datetime.strptime(str(self.date_start), '%Y-%m-%d') - timedelta(hours=7)).strftime("%Y-%m-%d %H:%M:%S")
                where_period = "so.date_order >= '%s' AND so.date_order <= '%s 17:00:00'" % (date_start, self.date_end)

        sql = """
            SELECT
                partner.id as cust_id,
                pt.name as item_name,
                so.id as so_id,
                so.name as name_po,
                so.date_order as tgl_po,
                s_line.id as line_id,
                s_line.product_id as product_id,
                s_line.product_uom_qty as qty_po,
                s_line.qty_delivered as quantity_done,
                s_line.product_uom_qty - s_line.qty_delivered as outstanding,
                s_line.price_unit as pric,
                s_line.product_uom_qty * s_line.price_unit as amount_po,
                so.client_order_ref as po_customer,
                s_move.picking_id as picking_id
            FROM sale_order_line s_line
                LEFT JOIN product_product pp on(pp.id=s_line.product_id)
                LEFT JOIN product_template pt on(pt.id=pp.product_tmpl_id)
                LEFT JOIN product_customer_code p_code on(p_code.id=s_line.product_customer_code_id)
                LEFT JOIN sale_order so on(s_line.order_id=so.id)
                LEFT JOIN res_partner partner on(so.partner_id=partner.id)
                LEFT JOIN stock_move s_move on(so.name=s_move.origin)
            WHERE so.state IN ('sale','done') AND %s AND %s AND %s
            ORDER BY partner.name asc
        """ % (where_partner, where_company, where_period)
        self.env.cr.execute(sql)
        results = self.env.cr.dictfetchall()
        for x in results:
            picking = self.env['stock.picking'].browse(x['picking_id'])
            x['scheduled_date'] = picking.scheduled_date
            x['amount_selisih'] = x['outstanding'] * x['amount_po']
        no = 1
        for i in results:
            if i['scheduled_date']:
                scheduled_date = i['scheduled_date'].strftime("%Y-%m-%d")
            else:
                scheduled_date =''
            if i['tgl_po']:
                tgl_po = i['tgl_po'].strftime("%Y-%m-%d")
            else:
                tgl_po = ''
            data.append({
                'cust_id'       : i['cust_id'],
                'No'            : str(no),
                'Item name'     : i['item_name'],
                'No PO'         : str(i['name_po']),
                'TGL PO'        : tgl_po,
                'QTY PO'        : int(i['qty_po']),
                'DELIVERY'      : i['quantity_done'],
                'OUTSTANDING'   : i['outstanding'],
                'Price'         : i['pric'],
                'Amount PO'     : i['amount_po'],
                'Amount Sisa'   : i['amount_selisih'],
                'TGL DELIVERY'  : scheduled_date,
            })
            no += 1
        
        header = [
            'No','Item name', 'No PO', 'TGL PO', 'QTY PO', 'DELIVERY', 
            'OUTSTANDING', 'Price', 'Amount PO', 'Amount Sisa','TGL DELIVERY'
        ]
        dict_data = {}
        for x in data:
            if x['cust_id'] not in dict_data:
                dict_data[x['cust_id']] = [x]
            else:
                if x['cust_id'] in dict_data:
                    if x['cust_id'] not in dict_data[x['cust_id']]:
                        dict_data[x['cust_id']].append(x)
        sheet.write_merge(0, 0, 0, len(header) - 1, 'PT. NITTO ALAM INDONESIA (%s)'%company, style_judul)
        sheet.write_merge(1, 1, 0, len(header) - 1, title, style_judul)
        sheet.write_merge(2, 2, len(header) - 3, len(header) -1, str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', style_judul)
        sheet.write_merge(3, 3, 0, len(header) - 1, '', style_garis)
        
        no_h = 5
        no_isi = 7
        no_1 = 7
        no = 0
        
        for k, v in dict_data.items():
            partner = self.env['res.partner'].browse(k)
            sheet.write_merge(no_isi-2, no_isi-2, 1, 2, '%s  %s'%(partner.ref,partner.name), style_isi_bold)
            sheet.write_merge(no_isi-2, no_isi-2, 5, 5, '%s'%(partner.user_id.name), style_isi_bold)
            sheet.write_merge(no_isi-2, no_isi-2, 7, 7, '%s'%(partner.alias_name), style_isi_bold)
            sheet.write_merge(no_isi-2, no_isi-2, 9, 9, '%s'%(partner.property_purchase_currency_id.name), style_isi_bold)
            colh = -1
            for h in header:
                colh += 1
                sheet.write(no_isi, colh, h, style_header)
            qty = kirim = sisa = amount_po = amount_sisa = 0
            for d in v:
                no_isi += 1
                sheet.write(no_isi, 0, d['No'], style_no_bold)
                sheet.write(no_isi, 1, d['Item name'], style_isi_kiri)
                sheet.write(no_isi, 2, d['No PO'], style_no_bold)
                sheet.write(no_isi, 3, d['TGL PO'], style_no_bold)
                sheet.write(no_isi, 4, d['QTY PO'], style_isi_kanan_new)
                sheet.write(no_isi, 5, d['DELIVERY'], style_isi_kanan_new)
                sheet.write(no_isi, 6, d['OUTSTANDING'], style_isi_kanan_new)
                sheet.write(no_isi, 7, d['Price'], style_isi_kanan)
                sheet.write(no_isi, 8, d['Amount PO'], style_isi_kanan)
                sheet.write(no_isi, 9, d['Amount Sisa'], style_isi_kanan)
                sheet.write(no_isi, 10, d['TGL DELIVERY'], style_no_bold)
                qty += d['QTY PO']
                kirim += d['DELIVERY']
                sisa += d['OUTSTANDING']
                amount_po += d['Amount PO']
                amount_sisa += d['Amount Sisa']
            sheet.write(no_isi+1, 4, qty, style_isi_kanan_bold_new)
            sheet.write(no_isi+1, 5, kirim, style_isi_kanan_bold_new)
            sheet.write(no_isi+1, 6, sisa, style_isi_kanan_bold_new)
            sheet.write(no_isi+1, 8, amount_po, style_isi_kanan_bold)
            sheet.write(no_isi+1, 9, amount_sisa, style_isi_kanan_bold)
            no_isi += 5

        file_data = BytesIO()
        book.save(file_data)

        filename = '%s_on_%s.xls' % \
            (title, datetime.now().strftime("%Y-%m-%d"))

        out = base64.encodestring(file_data.getvalue())
        self.write({'file_data': out, 'name': filename})
        
        view = self.env.ref('bmo_report_so_po.vit_report_sale2_wizard_form_view_inherit_bmo')

        return {
            'view_type': 'form',
            'views': [(view.id, 'form')],
            'view_mode': 'form',
            'res_id': self.id,
            'res_model': 'vit.report.sale2.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        } 
# ReportSale2Wizard()