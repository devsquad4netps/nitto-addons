from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import itertools
import base64
import pytz
import xlwt
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)

class ReportPurchaseWizardBmo(models.TransientModel):
    _name = "bmo.report.purchase.wizard"
    _description = 'Laporan Barang Masuk'
    
    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    partner_ids = fields.Many2many('res.partner', string='Partner')
    date_start = fields.Date('Date Start', default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date('Date End', default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    type_print = fields.Selection([('excel','Excel'),('pdf','Pdf')], default='excel', string='Type')
    data_file = fields.Binary('File')
    name = fields.Char('File Name')

    @api.multi
    def action_print_report(self):
        if self.type_print == 'pdf':
            data = {
                'form': {
                    'date_start': self.date_start,
                    'date_end': self.date_end,
                },
            }
            return self.env.ref('bmo_report_so_po.action_print_po_with_amount').report_action(self, data=data)
        book = xlwt.Workbook(encoding="utf-8")
        title = 'Laporan Barang Masuk'

        sheet = book.add_sheet(title, cell_overwrite_ok=True)
        sheet.normal_magn = 80
        sheet.show_grid = False

        style_header = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz center;borders: bottom_color black,bottom thin;')
        style_judul = xlwt.easyxf('font: colour black, bold 1, height 280, name Helvetica Neue; align: vert centre, horz left;')
        style_judul_2 = xlwt.easyxf('font: colour black, bold 1, height 180, name Helvetica Neue; align: vert center, horz left;')
        style_garis = xlwt.easyxf('align: vert centre, horz center; borders: top_color black,top double;')
        style_no_bold = xlwt.easyxf('align: vert centre, horz center;')
        style_isi_bold = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz left;')
        style_isi_kiri = xlwt.easyxf('align: vert centre, horz left;')
        style_isi_kanan = xlwt.easyxf('align: vert centre, horz right;')
        style_isi_kanan.num_format_str = '#,##0.0000'
        style_isi_kanan_cur = xlwt.easyxf('align: vert centre, horz right;')
        style_isi_kanan_cur.num_format_str = '#,##0.00'
        style_isi_kiri_bold = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz left; borders: top thin,bottom thin;')
        style_isi_kanan_bold_cur = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz right; borders: top thin,bottom thin;')
        style_isi_kanan_bold_cur.num_format_str = '#,##0.00'
        style_isi_kanan_bold = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz right; borders: top thin,bottom thin;')
        style_isi_kanan_bold.num_format_str = '#,##0.0000'
        style_isi_kanan_new = xlwt.easyxf('align: vert centre, horz right;')
        style_isi_kanan_new.num_format_str = '#,##0'
        style_isi_kanan_bold_new = xlwt.easyxf('font: colour black, bold 1; align: vert centre, horz center; borders: top thin;')
        style_isi_kanan_bold_new.num_format_str = '#,##0'
        
        col_width = 256 * 40
        row = 8
        try:
            for i in itertools.count():
                sheet.col(i).width = col_width
                sheet.col(0).width = 256 * 5
                sheet.col(1).width = 256 * 20
                sheet.col(2).width = 256 * 25
                sheet.col(3).width = 256 * 35
                sheet.col(4).width = 256 * 15
                sheet.col(5).width = 256 * 40
                sheet.col(6).width = 256 * 15
                sheet.col(7).width = 256 * 25
                sheet.col(8).width = 256 * 25
                sheet.col(9).width = 256 * 20
                sheet.col(10).width = 256 * 20
                sheet.row(i).height = 5 * 75
                sheet.set_panes_frozen(True)
                sheet.set_horz_split_pos(5)
                sheet.set_remove_splits(True)
        except ValueError:
            pass
        
        list_partner = []
        data = []
        where_partner = '1=1'
        where_company = '1=1'
        where_period = '1=1'
        company = ""
        if self.partner_ids:
            if len(self.partner_ids) > 1:
                where_partner = "ai.partner_id IN %s" % str(tuple(self.partner_ids.ids))
            else:
                where_partner = "ai.partner_id = %s" % str(self.partner_ids.id)
        if self.company_ids:
            for comp in self.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
                company = company[:-3]
            if len(self.company_ids) > 1:
                where_company = "po.company_id IN %s" % str(tuple(self.company_ids.ids))
            else:
                where_company = "po.company_id = %s" % str(self.company_ids.id)
        if self.date_start and self.date_end:
            if self.date_start > self.date_end:
                raise UserError('Start Date harus sebelum End Date.')
            else:
                date_start = (datetime.strptime(str(self.date_start), '%Y-%m-%d') - timedelta(hours=7)).strftime("%Y-%m-%d %H:%M:%S")
                where_period = "po.date_order >= '%s' AND po.date_order <= '%s 17:00:00'" % (date_start, self.date_end)
        sql = """
            select 
                ai.id as inv_id,
                po.date_order as date,
                po.company_id as company_id,
                ai.reference as invoice,
                rp.name as partner,
                pt.default_code as item,
                pt.name as item_name,
                ai.currency_id as currency_id,
                sm.id as move_Id,
                sm.date as move_date,
                sm.purchase_price as price
            from stock_move sm
                left join product_product pp on(pp.id=sm.product_id)
                left join product_template pt on(pt.id=pp.product_tmpl_id)
                left join stock_picking sp on sp.id=sm.picking_id 
                left join vit_kanban vk on vk.id=sm.kanban_id 
                left join purchase_order po on po.name=sm.origin
                left join res_partner rp on rp.id=po.partner_id 
                left join account_invoice ai on ai.id=vk.invoice_id
            where ai.state not in ('cancel') AND ai.type In ('in_invoice') AND %s AND %s AND %s
            order by 
                rp.name asc, po.date_order desc  
        """ % (where_partner, where_company, where_period)
        self.env.cr.execute(sql)
        print(sql)
        results = self.env.cr.dictfetchall()
        for x in results:
            x['quantity'] = self.env['stock.move'].browse(x['move_id']).quantity_done
            x['price_unit'] = x['quantity'] * x['price']
        no = 1
        for i in results:
            if i['date']:
                date = i['date'].strftime("%Y-%m-%d")
            else:
                scheduled_date =''
            data.append({
                'inv_id'            : i['inv_id'],
                'No'                : str(no),
                'Date'              : date,
                'Invoice'           : str(i['invoice']),
                'Name'              : str(i['partner']),
                'Item'              : str(i['item']),
                'Item name'         : i['item_name'],
                'Quantity'          : i['quantity'],
                'Amount currency'   : i['price_unit'],
                'currency_id'       : i['currency_id'],
                'company_id'        : i['company_id'],
                'move_date'         : i['move_date'],
            })
            no += 1
        
        header = ['No','Date', 'Invoice', 'Name', 'Item', 'Item name', 'Quantity', 'Amount currency']
        dict_data = {}
        for x in data:
            if x['inv_id'] not in dict_data:
                dict_data[x['inv_id']] = [x]
            else:
                if x['inv_id'] in dict_data:
                    if x['inv_id'] not in dict_data[x['inv_id']]:
                        dict_data[x['inv_id']].append(x)
        sheet.write_merge(0, 0, 0, len(header), 'PT. NITTO ALAM INDONESIA (%s)'%company, style_judul)
        sheet.write_merge(1, 1, 0, len(header), title, style_judul)
        sheet.write_merge(2, 2, len(header) - 1, len(header), str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', style_judul)
        sheet.write_merge(3, 3, 0, len(header), '', style_garis)
        
        no_isi = 3
        colh = -1
        for h in header:
            colh += 1
            sheet.write(4, colh, h, style_header)

        # no_h = 5
        no_isi = 4
        # no_1 = 7
        # no = 0
        
        for k, v in dict_data.items():
            inv_obj = self.env['account.invoice'].browse(k)
            qty = amount = amount_cur = 0
            for d in v:
                currency_awal = self.env['res.currency'].browse(d['currency_id'])
                if currency_awal.name == 'IDR':
                    amount_currency = d['Amount currency']
                else:
                    currency_idr = self.env['res.currency'].search([('name', '=', 'IDR')])
                    com = self.env['res.company'].browse(d['company_id'])
                    # amount_currency = currency_awal.compute(d['Amount currency'], currency_idr)
                    amount_currency = currency_awal._convert(d['Amount currency'], currency_idr, com, d['move_date'])
                no_isi += 1
                sheet.write(no_isi, 0, d['No'], style_no_bold)
                sheet.write(no_isi, 1, d['Date'], style_isi_kiri)
                sheet.write(no_isi, 2, d['Invoice'], style_no_bold)
                sheet.write(no_isi, 3, d['Name'], style_isi_kiri)
                sheet.write(no_isi, 4, d['Item'], style_no_bold)
                sheet.write(no_isi, 5, d['Item name'], style_isi_kiri)
                sheet.write(no_isi, 6, d['Quantity'], style_no_bold)
                sheet.write(no_isi, 7, d['Amount currency'], style_isi_kanan)
                sheet.write(no_isi, 8, amount_currency, style_isi_kanan_cur)
                qty += d['Quantity']
                amount += d['Amount currency']
                amount_cur += amount_currency
            sheet.write(no_isi+1, 1, 'Invoice:%s'%(inv_obj.reference), style_isi_kiri_bold)
            sheet.write(no_isi+1, 6, qty, style_isi_kanan_bold_new)
            sheet.write(no_isi+1, 7, amount, style_isi_kanan_bold)
            sheet.write(no_isi+1, 8, amount_cur, style_isi_kanan_bold_cur)
            no_isi += 2
        file_data = BytesIO()
        book.save(file_data)

        filename = '%s_on_%s.xls' % \
            (title, datetime.now().strftime("%Y-%m-%d"))

        out = base64.encodestring(file_data.getvalue())
        self.write({'data_file': out, 'name': filename})

        view = self.env.ref('bmo_report_so_po.bmo_report_purchase_wizard_form_view')

        return {
            'view_type': 'form',
            'views': [(view.id, 'form')],
            'view_mode': 'form',
            'res_id': self.id,
            'res_model': 'bmo.report.purchase.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        } 
ReportPurchaseWizardBmo()