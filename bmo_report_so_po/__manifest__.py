# -*- coding: utf-8 -*-
{
    'name'                  : "Report Sale Order and Purchase Order",
    'summary'               : """""",
    'description'           : """
                                -Report Sale Orders
                                -Report Purchase Orders
                              """,
    'author'                : "bemosoft",
    'website'               : "https://bemosoft.com/",
    'category'              : 'custom',
    'version'               : '12.1',
    'depends'               : ['base','sale','purchase','vit_report_sale','account'],
    'data'                  : [
                                # 'security/ir.model.access.csv',
                                'wizard/report_sale_2_wizard_view.xml',
                                'wizard/report_purchase_wizard_view.xml',
                              ],
    'installable'           : True,    
    'application'           : True,    
    'autoinstall'           : False,
}