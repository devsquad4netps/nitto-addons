from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError,ValidationError
import time, xlsxwriter, base64, pytz
from io import StringIO, BytesIO
from pytz import timezone
import logging
_logger = logging.getLogger(__name__)
import base64
import io
try:
    import qrcode

except ImportError:
    _logger.debug('ImportError')

# class StockMove(models.Model):
#     _inherit           = 'stock.move'


#     data = fields.Binary('File', readonly=True)
#     data_name = fields.Char('Filename', readonly=True)

#     @api.multi
#     def print_wire(self):
#         now = fields.datetime.now()+ timedelta(hours=7)
#         pdf = self.env.ref('vit_kanban_barcode.report_print_kanban_barcode_move').sudo().render_qweb_pdf([self.id])[0]
#         name = "%s%s" % ("Barcode Wire"+self.name+" | "+str(now)[:19], '.pdf')
#         self.write({'data':base64.b64encode(pdf),'data_name':name})
#         return True

# StockMove()


class StockMoveLine(models.Model):
    _inherit           = 'stock.move.line'


    @api.model
    def create(self, vals):
        res = super(StockMoveLine, self).create(vals)
        if res.location_id.usage == 'supplier' and res.product_id.categ_id.name == 'Wire':
            res.barcode_ids.unlink()
            lot_name = res.lot_name
            if res.lot_id :
                lot_name = res.lot_id.name
            if lot_name :
                urut = 1
                count = int(res.product_id.kg_pal)
                if count < 1 :
                    count = 20
                for line in range(int(res.qty_done/count+2)) :
                    res.barcode_ids.create({'no_urut':urut,'barcode':lot_name+'#'+str(urut),'move_line_id':res.id})
                    urut += 1
        return res
   
    @api.multi
    def write(self, vals):
        res = super(StockMoveLine, self).write(vals)
        for i in self :
            if i.location_id.usage == 'supplier' and i.product_id.categ_id.name == 'Wire':
                i.barcode_ids.unlink()
                lot_name = i.lot_name
                if i.lot_id :
                    lot_name = i.lot_id.name
                if lot_name :
                    urut = 1
                    count = int(i.product_id.kg_pal)
                    if count < 1 :
                        count = 20
                    for line in range(int(i.qty_done/count+2)) :
                        i.barcode_ids.create({'no_urut':urut,'barcode':lot_name+'#'+str(urut),'move_line_id':i.id})
                        urut += 1
        return res

    barcode_ids = fields.One2many('stock.move.line.barcode','move_line_id', string="Borcode Wire", ondelete="cascade")

StockMoveLine()



class StockMoveLineBarcode(models.Model):
    _name           = 'stock.move.line.barcode'
    _order          = 'no_urut asc'

    qr_code = fields.Binary('QR Code', compute="_generate_qr_code")
    no_urut = fields.Integer('No Urut', copy=False)
    barcode = fields.Char('Barcode', copy=False)
    move_line_id = fields.Many2one('stock.move.line','Move Lines')


    @api.one
    @api.depends('move_line_id','barcode','no_urut')
    def _generate_qr_code(self):
        qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=20, border=4)
        if self.barcode :
            qr.add_data(self.barcode)
            qr.make(fit=True)
            img = qr.make_image()
            buffer = io.BytesIO()
            img.save(buffer, format="PNG")
            qrcode_img = base64.b64encode(buffer.getvalue())
            self.update({'qr_code': qrcode_img,})    

StockMoveLineBarcode()