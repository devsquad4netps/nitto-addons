from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError,ValidationError
import time, xlsxwriter, base64, pytz
from io import StringIO, BytesIO
from pytz import timezone
import logging
_logger = logging.getLogger(__name__)
import base64


class VitKanban(models.Model):
    _inherit           = 'vit.kanban'


    data = fields.Binary('File', readonly=True)
    data_name = fields.Char('Filename', readonly=True)

    @api.multi
    def print_wire(self):
        now = fields.datetime.now()+ timedelta(hours=7)
        pdf = self.env.ref('vit_kanban_barcode.report_print_kanban_barcode').sudo().render_qweb_pdf([self.id])[0]
        name = "%s%s" % ("Barcode Wire"+self.name+" | "+str(now)[:19], '.pdf')
        self.write({'data':base64.b64encode(pdf),'data_name':name})
        return True

VitKanban()