{
    "name"          : "Eksekusi Work Order",
    "version"       : "0.1",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "MRP",
    "summary"       : "Eksekusi banyak WO dalam satu waktu",
    "description"   : """
        
    """,
    "depends"       : [
        "mrp","vit_turbo_mrp","vit_report_balance"
    ],
    "data"          : [
        "security/group.xml",
        "views/workorder_view.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}