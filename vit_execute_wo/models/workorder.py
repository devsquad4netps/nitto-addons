from odoo import api, fields, models

class MRPWorkorder(models.Model):
    _inherit = "mrp.workorder"

    wo_group_ids = fields.Many2many("mrp.workorder","workorder_group_id","workorder_group_id2",string="Workorder to execute", help="Workorder dengan status sama dan group report sama")

    
    @api.onchange('wo_group_ids','barcode')
    def onchange_wo_group_ids(self):
        domain = {'domain': {'wo_group_ids': [('id', '=', 0)]}}
        obj_wo = self.env['mrp.workorder'].search([('id','!=',self._origin.id),('production_id','=',self.production_id.id),('workcenter_id.group_report','=',self.workcenter_id.group_report),('state','=',self.state)])
        domain = {'domain': {'wo_group_ids': [('id', 'in', obj_wo.ids)]}}
        if domain :
            domain = domain
        return domain

    def write(self, vals):
        res = super(MRPWorkorder, self).write(vals)
        for wo in self:
            for i in wo.wo_group_ids.sorted('id') :
                if not i.workcenter_id.group_report or i.workcenter_id.group_report != wo.workcenter_id.group_report :
                    wo.update({'wo_group_ids':[(3, i.id)]})
        return res

    @api.multi
    def button_start(self):
        res = super(MRPWorkorder, self).button_start()
        for wo in self.wo_group_ids.sorted('id'):
            wo.button_start()
        return res

    @api.multi
    def record_production(self):
        res = super(MRPWorkorder, self).record_production()
        for wo in self.wo_group_ids.sorted('id'):
            wo.record_production()
        return res

    @api.multi
    def button_turbo_finish(self):
        res = super(MRPWorkorder, self).button_turbo_finish()
        for wo in self.wo_group_ids.sorted('id'):
            wo.button_turbo_finish()
        return res


MRPWorkorder()