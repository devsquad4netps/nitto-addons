from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)

class mrp_production(models.Model):
    _inherit = 'mrp.production'


    @api.model
    def update_state_mrp(self):
        mo_exist = self.search([('state','=','progress')])
        for mo in mo_exist:
            outstanding_pick = mo.move_finished_ids.filtered(lambda prod:prod.state not in ('cancel','done'))
            if not outstanding_pick:
                try :
                    mo.button_mark_done()
                    _logger.info("Manufacturing Order %s updated state to ======> %s (ORM)"%(mo.name, mo.state))
                except:
                    try :
                        self.env.cr.execute("UPDATE mrp_production SET state='done' WHERE id = %d"%(mo.id))
                        self.env.cr.commit() 
                        _logger.info("Manufacturing Order %s updated state to ======> %s (direct query)"%(mo.name, mo.state))
                    except:
                        pass
                self.env.cr.commit() 
        return True

    @api.model
    def update_total_lot_mrp(self):
        fr_exist = self.env['forecast.forecast_mrp_new'].search([('state','=','validate'),('result_mo','>',1)])
        for fr in fr_exist:
            mos = self.search([('name', 'ilike', fr.parent_mo)])
            for mrp in mos:
                self.env.cr.execute("UPDATE mrp_production SET jml_lot=%d WHERE id = %d"%(int(fr.result_mo),mrp.id))
                self.env.cr.commit()
                _logger.info("Manufacturing Order %s updated Total lot to ======> %s "%(mrp.name, str(fr.result_mo)))
        return True


mrp_production()

