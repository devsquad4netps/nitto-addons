from odoo import models, fields, api
from odoo import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
from odoo.exceptions import UserError, ValidationError
import pdb

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    invoice_id = fields.Many2one(comodel_name="account.invoice", string="Invoice", copy=False)
    bc_type = fields.Selection([('BC 2.3','BC 2.3'),('BC 2.5','BC 2.5'),('BC 2.6.1','BC 2.6.1'),('BC 2.6.2','BC 2.6.2'),('BC 2.7','BC 2.7'),('BC 4.0','BC 4.0'),('BC 4.1','BC 4.1')],string='Jenis BC')


    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        kdoc = ""
        inc_aju = ""
        date = ""
        type_ln = ""
        id_modul = False
        for rest in self:
            if not rest.company_id.bea_cukai :
                continue
            kppbc = False
            if rest.date_done:
                dt_y = rest.date_done.year
                dt_m = '{:02d}'.format(rest.date_done.month)
                dt_d = '{:02d}'.format(rest.date_done.day)
                date = str(dt_y) + str(dt_m) + str(dt_d)
                date_p = str(rest.date_done)
            else:
                dt = datetime.now().date()
                dt_y = dt.year
                dt_m = '{:02d}'.format(dt.month)
                dt_d = '{:02d}'.format(dt.day)
                date = str(dt_y) + str(dt_m) + str(dt_d)
                date_p = str(datetime.now().date())

            rest.tgl_dokumen = date_p
            rest.tgl_ttd = date_p

            loop = False
            acc_id = False
            if not rest.bc_type :
                if rest.picking_type_id.code == "outgoing":
                    # search SO
                    so = self.env['sale.order']
                    so_exist = so.sudo().search([('name','=',rest.origin)],limit=1)
                    if so_exist :
                        obj = False
                        if so_exist.sale_type == 'Local' :
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 4.1')],limit=1)
                        elif so_exist.sale_type == 'Export':
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 3.0')],limit=1)
                        elif so_exist.sale_type == 'Kawasan Berikat':
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.7')],limit=1)
                        elif so_exist.sale_type == 'Subcon':
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1')],limit=1)
                        if not obj :
                            raise ValidationError(_('Sales type %s belum di set atau master aju %s belum dibuat !') % (so_exist.name,so_exist.sale_type))
                        if obj.kode_dokumen_bc:
                            kppbc = str(obj.kode_dokumen_bc)
                        else :
                            kppbc = str(obj.kppbc)
                        id_modul = str(obj.id_modul)
                    else :
                        # jika returan SO
                        if rest.partner_id and rest.partner_id.transaction_type :
                            if rest.partner_id.transaction_type == 'Local' :
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 4.0')],limit=1)
                            elif rest.partner_id.transaction_type == 'Import':
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.3')],limit=1)
                            elif rest.partner_id.transaction_type == 'Kawasan Berikat':
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.7')],limit=1)
                            elif rest.partner_id.transaction_type == 'Subcon':
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2')],limit=1)
                            if not obj :
                                raise ValidationError(_('Purchase type %s belum di set atau master aju %s belum dibuat !') % (po_exist.name,po_exist.purchase_type))
                            if obj.kode_dokumen_bc:
                                kppbc = str(obj.kode_dokumen_bc)
                            else :
                                kppbc = str(obj.kppbc)

                        else :
                            raise ValidationError(_('Sales Order %s not found !') % rest.origin)
                    type_ln = 'out_invoice'
                    journal = self.env['account.journal'].sudo().search([('company_id','=',rest.company_id.id),('type','=','sale')], limit=1)
                    if rest.partner_id.property_account_receivable_id:
                        acc_id = rest.partner_id.property_account_receivable_id.id
                    if not acc_id:
                        raise ValidationError(_('account pada customer ( %s ) belum di set!!') % rest.partner_id.name)
                elif rest.picking_type_id.code == "incoming":
                    # search PO
                    po = self.env['purchase.order']
                    po_exist = po.sudo().search([('name','=',rest.origin)],limit=1)
                    if po_exist :
                        obj = False
                        if po_exist.purchase_type == 'Local' :
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 4.0')],limit=1)
                        elif po_exist.purchase_type == 'Import':
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.3')],limit=1)
                        elif po_exist.purchase_type == 'Kawasan Berikat':
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.7')],limit=1)
                        elif po_exist.purchase_type == 'Subcon':
                            obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2')],limit=1)
                        if not obj :
                            raise ValidationError(_('Purchase type %s belum di set atau master aju %s belum dibuat !') % (po_exist.name,po_exist.purchase_type))
                        if obj.kode_dokumen_bc:
                            kppbc = str(obj.kode_dokumen_bc)
                        else :
                            kppbc = str(obj.kppbc)
                        id_modul = str(obj.id_modul)
                    else :
                        # jika returan PO
                        if rest.partner_id and rest.partner_id.transaction_type :
                            obj = False
                            if rest.partner_id.transaction_type == 'Local' :
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 4.1')],limit=1)
                            elif rest.partner_id.transaction_type == 'Export':
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 3.0')],limit=1)
                            elif rest.partner_id.transaction_type == 'Kawasan Berikat':
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.7')],limit=1)
                            elif rest.partner_id.transaction_type == 'Subcon':
                                obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1')],limit=1)
                            if not obj :
                                raise ValidationError(_('Nomor transaksi %s tidak memiliki PO (%s)!') % (rest.name, rest.partner_id.transaction_type))
                            if obj.kode_dokumen_bc:
                                kppbc = str(obj.kode_dokumen_bc)
                            else :
                                kppbc = str(obj.kppbc)
                            id_modul = str(obj.id_modul)
                        else :
                            raise ValidationError(_('Purchase Order %s not found !') % rest.origin)
                    type_ln = 'in_invoice'
                    journal = self.env['account.journal'].sudo().search([('company_id','=',rest.company_id.id),('type','=','purchase')], limit=1)
                    if rest.partner_id.property_account_payable_id:
                        acc_id = rest.partner_id.property_account_payable_id.id
                    if not acc_id:
                        raise ValidationError(_('account pada customer ( %s ) belum di set!!') % rest.partner_id.name)
            else :
                obj = self.env['vit.export.import.bc'].search([('name','=',rest.bc_type)],limit=1)
                if not obj :
                    raise ValidationError(_('Master aju %s belum dibuat !') % (rest.bc_type))
                id_modul = str(obj.id_modul)
                if obj.kode_dokumen_bc:
                    kppbc = str(obj.kode_dokumen_bc)
                else :
                    kppbc = str(obj.kppbc)
                if rest.picking_type_id.code == 'outgoing':
                    type_ln = 'out_invoice'
                    journal = self.env['account.journal'].sudo().search([('company_id','=',rest.company_id.id),('type','=','sale')], limit=1)
                    if rest.partner_id.property_account_receivable_id:
                        acc_id = rest.partner_id.property_account_receivable_id.id
                    if not acc_id:
                        raise ValidationError(_('account pada customer ( %s ) belum di set!!') % rest.partner_id.name)
            if not rest.nomor_aju_id and kppbc:
                loop = True
                obj_aju = self.env['vit.nomor.aju'].search([('company_id','=',rest.company_id.id),('name','like',kppbc+'%'),('has_export','=',False)],order="name desc", limit=1)
                if obj_aju :
                    if rest.nomor_aju_id.id == False:
                        count = len(obj_aju.picking_ids)
                        if count < 5:
                            types = []
                            partners = []
                            for z in obj_aju.picking_ids:
                                if z.partner_id and z.partner_id.id != rest.partner_id.id :
                                    partners.append(z.partner_id.id)
                                if z.picking_type_id.code not in types:
                                    types.append(z.picking_type_id.code)
                            # jika picking type sama dan partner nya sama semua maka bisa pake nomor aju ini
                            if len(types) == 1 and len(partners) < 1:
                                rest.nomor_aju_id = obj_aju.id

            if loop and kppbc and not rest.nomor_aju_id.id:
                if not id_modul :
                    raise ValidationError(_('ID Modul %s tidak ditemukan') % rest.partner_id.transaction_type)
                inc_aju = str(rest.env['ir.sequence'].next_by_code('increment_no_aju'))
                id_aju = self.env['vit.nomor.aju'].create({'name': kppbc + '0' + id_modul + date + inc_aju})
                rest.nomor_aju_id = id_aju

            # pdb.set_trace();
            # create hanya jika DO aja
            # komen dulu karena semua DO pake kanban
            # if rest.picking_type_id.code == "outgoing":
            #     move_lines = []
            #     sale_line_exist = False
            #     for ln in rest.move_line_ids:
            #         if ln.lot_id:
            #             ln.lot_id.no_aju = rest.nomor_aju_id.name

            #         acc_line_id = False
            #         if ln.product_id.categ_id.property_account_income_categ_id:
            #             acc_line_id = ln.product_id.categ_id.property_account_income_categ_id.id
            #         if not acc_line_id:
            #             raise ValidationError(_('Income account pada product ( %s ) belum di set!!') % ln.product_id.name)
            #         obj_so = self.env['sale.order'].search([('name','=',rest.origin)],limit=1)
            #         sale_line_exist = obj_so.order_line.filtered(lambda gh: gh.product_id.id == ln.product_id.id)
            #         if sale_line_exist :
            #             tax = []
            #             sale = []
            #             for gh in sale_line_exist:
            #                 if gh.tax_id :
            #                     tax = [(4, tx.id) for tx in gh.tax_id]
            #                 sale = [(4, gh.id)]
            #                 if not ln.move_id.invoiced :
            #                     move_lines.append((0,0,{
            #                     'product_id'        : ln.product_id.id,
            #                     'price_unit'        : gh.price_unit,
            #                     'quantity'          : ln.qty_done ,
            #                     'name'              : ln.product_id.name,
            #                     'account_id'        : acc_line_id,
            #                     'invoice_line_tax_ids': tax,
            #                     'sale_line_ids': sale
            #                     }))
            #                     ln.move_id.invoiced = True
            #                     ln.invoiced = True

            #     if move_lines and sale_line_exist:
            #         data = {
            #                 'partner_id': rest.partner_id.id,
            #                 'journal_id': journal.id,
            #                 'payment_journal_id': journal.id,
            #                 'account_id': acc_id,
            #                 'invoice_line_ids': move_lines,
            #                 'type': 'out_invoice',
            #                 'origin': rest.name,
            #                 'currency_id': sale_line_exist[0].currency_id.id,
            #                 'user_id': rest.env.user.id,
            #                 'company_id': rest.company_id.id,
            #                 }
            #         inv = self.env['account.invoice'].sudo().create(data)

            #         rest.invoice_id = inv.id
            #         inv.action_invoice_open()
            #         dokumen_inv = self.env['vit.dokumen.master'].sudo().search([('code','=','380')],limit=1)
            #         if dokumen_inv :
            #             rest.dokumen_ids = [(0,0, {'dokumen_master_id':dokumen_inv.id,'nomor':inv.number,'tanggal':inv.date_invoice})]

        return res


class StockMove(models.Model):
    _inherit = 'stock.move'

    invoiced = fields.Boolean(string="Invoiced", copy=False)

StockMove()

class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    invoiced = fields.Boolean(string="Invoiced", copy=False)

StockMoveLine()