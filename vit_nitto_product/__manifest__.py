{
	"name": "Nitto Product",
	"version": "1.5", 
	"depends": [
		"base",
		"account",
		"sale",
		"stock",
		"product",
		"sale_management",
		"product",
	],
	"author": "http://www.vitraining.com", 
	"category": "Product", 
	'website': 'http://www.vitraining.com',
	"description": """


""",
	"data": [
		"security/ir.model.access.csv",
		"views/stock_production_lot_view.xml",
		"views/product_master.xml",
		"views/product_template.xml",
		"views/stock_move_view.xml",
		"views/stock_quant_view.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}