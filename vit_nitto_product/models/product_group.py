from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class ProductGroup(models.Model):
    _name = 'product.template.group'
    _description = 'Kode Tarif HS'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    notes = fields.Text('Notes')
    kode_tarif = fields.Char('KD Tarif')
    kode_bm = fields.Char('Kd. BM')
    tarif_bm = fields.Char('Tarif BM (%)')
    ppn = fields.Char('PPn (%)')
    pph = fields.Char('PPh (%)')

ProductGroup()


class ProductGroup2(models.Model):
    _name = 'product.template.group2'
    _description = 'Product Group'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    notes = fields.Text('Notes')

ProductGroup2()