from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class QuantPackage(models.Model):
    _name = 'stock.quant.package.type'
    _description = 'Stock Quant Package Type'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    weight = fields.Float('Weight')
    notes = fields.Text('Notes')
    
QuantPackage()


class StockQuantPackage(models.Model):
    _inherit = 'stock.quant.package'

    package_type_id = fields.Many2one('stock.quant.package.type', 'Type')

StockQuantPackage()