from odoo import models, fields, api
from odoo.addons import decimal_precision as dp


class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    no_aju = fields.Char('Nomor Ajuan', size=100)
    no_pabean = fields.Char('Nomor Pabean',size=100)
    tgl_pabean = fields.Date('Tanggal Pabean')
    ex_lot = fields.Char('Coil Number', size=100)
    ex_lot2 = fields.Char('Heat Number', size=100)
    seri = fields.Integer('Seri', size=3)

    _sql_constraints = [('name_ref_uniq', 'unique (name, product_id)', 'The combination of serial number ,Coil Number and product must be unique !')]
    # _sql_constraints = [
    #     ('name_ref_uniq', 'unique (name, product_id, ex_lot)', 'The combination of serial number and product must be unique !'),
    # ] 
# ALTER TABLE stock_production_lot DROP CONSTRAINT stock_production_lot_name_ref_uniq;
# ALTER TABLE stock_production_lot ADD CONSTRAINT name_ref_uniq unique (name, product_id);
# ALTER TABLE stock_production_lot ADD CONSTRAINT name_ref_uniq UNIQUE (name, product_id);
StockProductionLot()