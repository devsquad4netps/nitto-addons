from odoo import models, fields, api
from odoo.addons import decimal_precision as dp


class StockMove(models.Model):
	_name = "stock.move"
	_inherit = ['barcodes.barcode_events_mixin', 'stock.move']   

	@api.depends('move_line_ids','move_line_ids.product_uom_qty','move_line_ids.qty_done','state')
	def _compute_bruto_netto(self):
		for berat in self:
			#import pdb;pdb.set_trace()
			if berat.move_line_ids :
				berat.netto = sum(berat.move_line_ids.mapped('netto'))
				berat.brutto = sum(berat.move_line_ids.mapped('bruto'))

	netto = fields.Float(string='Netto', digits=dp.get_precision('Stock Weight'), help="netto perbarang (untuk laporan bc)", compute="_compute_bruto_netto", store=True)
	brutto = fields.Float(string='Bruto', digits=dp.get_precision('Stock Weight'), help="bruto perbarang (untuk laporan bc)", compute="_compute_bruto_netto", store=True)

	# komen dulu, bentrok dg fitur create package di vit_package_master
	# @api.multi
	# def write(self, vals):
	# 	res = super(StockMove, self).write(vals)
	# 	for w in self :
	# 		if w.production_id and w.move_line_ids:
	# 			package = self.env['stock.quant.package']
	# 			package_type = self.env['stock.quant.package.type']
	# 			box_type = False
	# 			box = package_type.search([('code','=','BX')],limit=1)
	# 			if box :
	# 				box_type = box.id
	# 			no = len(w.move_line_ids.filtered(lambda x:x.result_package_id))
	# 			for mv in w.move_line_ids :
	# 				no += 1
	# 				if not mv.result_package_id :
	# 					if len(str(no)) == 1 :
	# 						number = '0'+str(no)
	# 					else :
	# 						number = str(no)
	# 					package_number = w.production_id.name+'#'+number
	# 					# cek dulu jika ada pack dg no sama
	# 					pack_exist = package.search([('name','=',package_number)],limit=1)
	# 					if pack_exist :
	# 						res_pack = pack_exist
	# 					else :
	# 						res_pack = package.create({'company_id': w.production_id.company_id.id,
	# 													'location_id': mv.location_dest_id.id,
	# 													'name':package_number,
	# 													'package_type_id': box_type,
	# 													'weight':0.8})
	# 					mv.result_package_id = res_pack.id	
	# 	return res	

	@api.onchange('move_line_ids','move_line_ids.bruto','move_line_ids.netto')
	def onchange_moves(self):
		netto = sum(self.move_line_ids.mapped('netto'))
		brutto = sum(self.move_line_ids.mapped('bruto'))
		if self._origin.id :
			sql = "update stock_move set netto = %s, brutto = %s where id = %s" % (netto, brutto, self._origin.id )
			self.env.cr.execute(sql)
			self.env.cr.commit()


	# def on_barcode_scanned(self, barcode):  
	# 	for mv in self :
	#     	if mv.picking_id :
	#     		if mv.picking_id.picking_type_id.code == 'incoming':
	#     			for mvl in mv.move_line_ids :
	#     				if not mvl.lot_name :
	#     					mvl.lot_name = barcode
	#     				elif not mvl.lot_name2 :
	#     					mvl.lot_name2 = barcode
	#     				mvl.qty_done = 1

StockMove()


class StockMoveLine(models.Model):
	_inherit = "stock.move.line"

	@api.onchange('lot_name1','lot_name2','qty_done')
	def onchange_lot_name(self):
		for x in self:
			kg_pal = x.product_id.kg_pal or 20
			lot1 = x.lot_name1
			if not lot1 :
				lot1 = ''
			lot2 = x.lot_name2
			if not lot2 :
				lot2 = ''
			lot3 = x.picking_id.kanban_id
			if not lot3:
				lot3 = x.move_id.kanban_id
			if lot3 :
				lot3 = '#'+lot3.name
			else :
				lot3 = ''
			#x.lot_name = str(x.product_id.default_code)+'#'+lot1+'#'+ lot2 +'#'+str(int(x.qty_done))+'#'+str(kg_pal)+lot3
			# x.lot_name = str(x.product_id.default_code)+'#'+lot1+'#'+ lot2 +'#'+str(int(x.qty_done))+ lot3 +'#'+str(kg_pal)
			#x.lot_name = str(x.product_id.default_code)+'#'+lot1+'#'+ lot2 +'#'+str(int(x.qty_done))+ lot3
			x.lot_name = str(x.product_id.default_code)+'#'+lot1+'#'+ lot2 + lot3 +'#'+str(int(x.qty_done))

			#KODE BARANG # HEAT NUMBER # COIL NUMBER # QTYPERCOIL # QTY PER BOX# LBM
			# ref 12 jul 21
			#KODE BARANG # HEAT NUMBER # COIL NUMBER # QTYPERCOIL #LBM# QTY PER BOX# NOMOR URUT
			# ref 12 jul 21 (2)
			# KODE BARANG # HEAT NUMBER # COIL NUMBER # QTYPERCOIL #LBM# NOMOR URUT
			# ref 17 jan 22 (3)
			# KODE BARANG # HEAT NUMBER # COIL NUMBER # LBM # QTYPERCOIL # NOMOR URUT

	@api.multi
	def unlink(self):
		for i in self :
			if i.move_id and i.move_id.production_id:
				del_pack = i.result_package_id
				i.result_package_id = False
				del_pack.unlink()
		return super(StockMoveLine, self).unlink()

	@api.depends('package_id','qty_done','product_uom_qty','state','move_id.product_id.weight')
	def _compute_bruto_netto(self):
		for berat in self:
			product = berat.move_id.product_id
			product_berat = product.weight
			qty = berat.qty_done
			if qty == 0.0:
				qty = berat.product_uom_qty
			pack_berat = 0.0
			if berat.package_id :
				if berat.package_id.package_type_id :
					pack_berat = berat.package_id.package_type_id.weight
				if pack_berat == 0.0 :
					pack_berat = berat.package_id.weight
			berat.netto = product_berat*qty
			berat.bruto = product_berat*qty+pack_berat

	lot_name1 = fields.Char('Coil Number')
	lot_name2 = fields.Char('Heat Number')
	# netto = fields.Float(string="Netto",  required=False, compute="_compute_bruto_netto", store=True)
	# bruto = fields.Float(string="Bruto",  required=False, compute="_compute_bruto_netto", store=True)
	netto = fields.Float(string="Netto",  required=False,)
	bruto = fields.Float(string="Bruto",  required=False,)
	picking_type_code = fields.Selection([('incoming', 'Vendors'), ('outgoing', 'Customers'), ('internal', 'Internal')],"Picking Code", related="picking_id.picking_type_id.code", store=True)

	@api.onchange('bruto','netto','qty_done','package_id','result_package_id','move_id.product_id.weight','lot_id')
	def onchange_move_lines(self):
		if not self.picking_id and self.move_id and self.move_id.picking_id :
			self.picking_id = self.move_id.picking_id.id
		if not self.product_id.categ_id.manual_weight:
			netto = (self.move_id.product_id.weight/1000)*self.qty_done
			brutto = (self.move_id.product_id.weight/1000)*self.qty_done
			if self.package_id and self.package_id.package_type_id :
				if self.package_id.package_type_id.weight > 0.0 :
					brutto = brutto+(self.package_id.package_type_id.weight*self.qty_done)
			elif self.result_package_id and self.result_package_id.package_type_id :
				if self.result_package_id.weight > 0.0 :
					brutto = brutto+(self.result_package_id.package_type_id.weight*self.qty_done)
			elif self.package_id and self.package_id.weight > 0.0:
				brutto = brutto+(self.package_id.weight*self.qty_done)
			elif self.result_package_id and self.result_package_id.weight > 0.0:
				brutto = brutto+(self.result_package_id.weight*self.qty_done)
			if netto > 0.0 :
				self.netto = netto
			if brutto > 0.0 :
				self.bruto = brutto
			if self._origin.id :
				sql = "update stock_move_line set netto = %s, bruto = %s where id = %s" % (self.netto , self.bruto, self._origin.id )
				self.env.cr.execute(sql)
				self.env.cr.commit()

	@api.model
	def create(self,vals):
		res = super(StockMoveLine,self ).create(vals)
		product = res.move_id.product_id
		product_berat = product.weight
		qty = res.qty_done
		if qty == 0.0:
			qty = res.product_uom_qty
		pack_berat = 0.0
		if res.package_id :
			if res.package_id.package_type_id :
				pack_berat = res.package_id.package_type_id.weight
			if pack_berat == 0.0 :
				pack_berat = res.package_id.weight
		netto = product_berat*qty
		if netto > 0.0 :
			res.netto = netto
		brutto = product_berat*qty+pack_berat
		if brutto > 0.0 :
			res.bruto = brutto
		return res

	# def _action_done(self):
	# 	res = super(StockMoveLine, self)._action_done()
	# 	for ml in self :
	# 		if ml.lot_id :
	# 			# ml.lot_id.ex_lot = ml.lot_name
	# 			lot1=''
	# 			lot2=''
	# 			if ml.lot_name1 :
	# 				ml.lot_id.ex_lot = ml.lot_name1
	# 				lot1=ml.lot_name1
	# 			if ml.lot_name2 :
	# 				ml.lot_id.ex_lot2 = ml.lot_name2
	# 				lot2=ml.lot_name2
	# 			sn = lot1+lot2
	# 			if sn :
	# 				ml.lot_id.name = sn
	# 			# ml.lot_id.name = self.env['ir.sequence'].next_by_code('stock.lot.serial')
	# 	return res

StockMoveLine()