from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class ProductType(models.Model):
    _name = 'product.template.type'
    _description = 'Product Type'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    notes = fields.Text('Notes')
    
ProductType()