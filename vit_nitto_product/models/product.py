from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    type_id = fields.Many2one('product.template.type','Internal Type')
    group_id = fields.Many2one('product.template.group','Kode Tarif HS')
    group_id2 = fields.Many2one('product.template.group2','Internal Group')
    # weight = fields.Float(
    #     'Weight', compute='_compute_weight', digits=dp.get_precision('Stock Weight'),
    #     inverse='_set_weight', store=True,
    #     help="The weight of the contents in Kg, not including any packaging, etc.")
    weight = fields.Float(
        'Weight', compute='_compute_weight', digits=(3,5),
        inverse='_set_weight', store=True,
        help="The weight of the contents in Kg, not including any packaging, etc.")

    @api.multi
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        # TDE FIXME: should probably be copy_data
        self.ensure_one()
        if default is None:
            default = {}
        if 'default_code' not in default:
            default['default_code'] = _("%s (copy)") % self.default_code
        return super(ProductTemplate, self).copy(default=default)

ProductTemplate()


class ProductProduct(models.Model):
    _inherit = "product.product"

    weight = fields.Float(
        'Weight', digits=(3,5),
        help="Weight of the product, packaging not included. The unit of measure can be changed in the general settings")


ProductProduct()


class ProductCategory(models.Model):
    _inherit = "product.category"

    manual_weight = fields.Boolean(help="Bisa input manual Netto Brutto di LBM")


ProductCategory()