from odoo import fields, models, api
import time
from io import BytesIO
from collections import OrderedDict
import pytz
import xlsxwriter
import base64
from datetime import datetime
from pytz import timezone
from odoo.exceptions import Warning
import base64
from xlrd import open_workbook

month_range = {
    'Jan' : '01',
    'Feb' : '02',
    'Mar' : '03',
    'Apr' : '04',
    'May' : '05',
    'Jun' : '06',
    'Jul' : '07',
    'Aug' : '08',
    'Sep' : '09', 
    'Oct' : '10',
    'Nov' : '11',
    'Dec' : '12',
}

class VitExportImportBcWizard(models.TransientModel):
    _name = "vit.export.import.bc.wizard"
    _description = "Export Import BC"
    _rec_name = "bc_id"

    def get_default_date(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone(self.env.user.tz or 'UTC')).date()

    def default_bc(self):
        # return self.env['vit.export.import.bc'].search([], limit=1)
        return False

    data = fields.Binary('File')
    name = fields.Char('Filename')
    bc_id = fields.Many2one('vit.export.import.bc', string='Jenis BC', ondelete='cascade', required=True, default=default_bc)
    # action = fields.Selection([('export','Export'),('import','Import')], default='import', string='Action', required=True)
    action = fields.Selection([('export','Export'),('import','Import')], string='Action', required=True)
    company_id = fields.Many2one('res.company', string='Perusahaan', ondelete='cascade', required=True, default=lambda self: self.env.user.company_id)
    date = fields.Date('Date', default=get_default_date, required=True)
    file_data = fields.Binary(string='Import File')
    filename = fields.Char(string='File Name')

    def cell_format(self, workbook):
        cell_format = {}
        cell_format['title'] = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 20,
            'font_name': 'Arial',
        })
        cell_format['header'] = workbook.add_format({
            'bold': True,
            'align': 'center',
            'border': True,
            'font_name': 'Arial',
        })
        cell_format['content'] = workbook.add_format({
            'font_size': 11,
            'border': False,
            'font_name': 'Arial',
        })
        cell_format['content_float'] = workbook.add_format({
            'font_size': 11,
            'border': True,
            'num_format': '#,##0.00',
            'font_name': 'Arial',
        })
        cell_format['total'] = workbook.add_format({
            'bold': True,
            'num_format': '#,##0.00',
            'border': True,
            'font_name': 'Arial',
        })
        return cell_format, workbook

    @api.multi
    def action_import(self):
        try :
            if not self.file_data :
                raise Warning("Silahkan upload file.")
            wb = open_workbook(file_contents=base64.decodestring(self.file_data))
            values = []
            for s in wb.sheets():
                if s.name == 'Header' :
                    for row in range(s.nrows):
                        col_value = []
                        for col in range(s.ncols):
                            value = (s.cell(row, col).value)
                            col_value.append(value)
                        values.append(col_value)
            row = 1
            for data in values :
                if row != 1 :
                    to_update = {}
                    if data[103]:
                        to_update['no_pabean'] = data[103]
                    if data[117]:
                        to_update['tgl_pabean'] = self.make_date_valid(data[117])
                    if to_update :
                        picking_ids = self.env['stock.picking'].search([('nomor_aju_id.name','=',data[0])])
                        lot_ids = self.env['stock.production.lot'].search([('no_aju','=',data[0])])
                        kanban_ids = self.env['vit.kanban'].search([('nomor_aju_id.name','=',data[0])])
                    
                        if picking_ids :
                            picking_ids.write(to_update)
                        
                        if lot_ids :
                            lot_ids.write(to_update)
                        if kanban_ids :
                            kanban_ids.write(to_update)
                            
                row += 1
        except Exception as e :
            raise Warning(e)
        # print("\n values",values)
        # raise Warning("mantoel gan.")

    @api.multi
    def get_datas(self):
        self.ensure_one()
        return []

    @api.multi
    def action_export(self):
        datas = self.get_datas()
        if not datas :
            raise Warning("Data tidak ditemukan.")
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        cell_format, workbook = self.cell_format(workbook)
        sheet_added = []
        for data in datas :
            sheet, headers, contents = data
            if sheet in sheet_added :
                continue
            worksheet = workbook.add_worksheet(sheet)
            sheet_added.append(sheet)
            worksheet.set_column('A:ZZ', 30)
            column_length = len(headers)

            column = 0
            for col in headers :
                worksheet.write(0, column, col, cell_format['content'])
                column += 1
            row = 1
            for data in contents :
                # print("\ndata",data)
                column = 0
                for header in headers :
                    # if sheet == 'Header' :
                    #     print("\nkey",key)
                    #     print("\nvalue",value)
                    # print("\nheader",header)
                    try:
                        worksheet.write(row, column, data[header], cell_format['content'])
                        column += 1
                    except:
                        column += 1
                        continue
                row += 1
        print('\n sheet_added',sheet_added)

        workbook.close()
        result = base64.encodestring(fp.getvalue())
        filename = self.bc_id.name + '%2Exlsx'
        self.write({'data':result, 'name':filename})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def reformat_date(self, date_string):
        date_format = datetime.strptime(date_string, '%Y-%m-%d')
        y, m, d = date_string.split('-')
        return '%s %s %s'%(d, date_format.strftime('%b'), y)

    def make_date_valid(self, date_string):
        date_valid = '%s-%s-%s'%(date_string[-4:], month_range[date_string[3:6]], date_string[0:2])
        return date_valid

class VitTarifHs(models.TransientModel):
    _name = "vit.tarif.hs"
    _description = "Tarif/HS"

    picking_id = fields.Many2one('stock.picking', string='Picking', ondelete='cascade')
    name = fields.Char('Kategori/Jenis Barang', required=True)
    kode_tarif = fields.Char('KD Tarif')
    kode_bm = fields.Char('Kd. BM')
    tarif_bm = fields.Char('Tarif BM')
    ppn = fields.Char('PPn')
    pph = fields.Char('PPh')
