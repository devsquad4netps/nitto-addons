from odoo import fields, models, api, _
from odoo.exceptions import Warning

class VitNomorAju(models.Model):
	_name = "vit.nomor.aju"
	_inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
	_description = "Nomor Aju"

	name = fields.Char('Nomor Aju', required=True, track_visibility='onchange')
	company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, track_visibility='onchange')
	picking_ids = fields.One2many('stock.picking', 'nomor_aju_id', strig='Picking')
	has_export = fields.Boolean('Has Export', copy=False, track_visibility='onchange')

	@api.multi
	def unlink(self):
		for data in self:
			if len(data.picking_ids) >= 1:
				raise UserError(_('No aju yang sudah di assign ke dokumen picking tidak bisa dihapus !'))
		return super(VitNomorAju, self).unlink()


VitNomorAju()