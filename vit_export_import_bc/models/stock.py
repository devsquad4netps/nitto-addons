from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def action_confirm(self):
        for pick in self :
            product_ids = pick.move_ids_without_package.mapped('product_id')
            for prod in product_ids :
                if not pick.kemasan_ids :
                    self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
                elif pick.kemasan_ids.filtered(lambda p:p.product_id.id != prod.id) :
                    self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
        return super(StockPicking, self).action_confirm()

    @api.multi
    def action_assign(self):
        for pick in self :
            product_ids = pick.move_ids_without_package.mapped('product_id')
            for prod in product_ids :
                if not pick.kemasan_ids :
                    self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
                elif pick.kemasan_ids.filtered(lambda p:p.product_id.id != prod.id) :
                    self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
        return super(StockPicking, self).action_assign()

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        for pick in self :
            for kemasan in pick.kemasan_ids :
                if kemasan.product_id.id not in pick.move_ids_without_package.mapped('product_id.id'):
                    kemasan.unlink()
            if pick.picking_type_id and pick.picking_type_id.code == 'outgoing' :
                tgl = False
                if pick.tgl_dokumen:
                    tgl = pick.tgl_dokumen
                elif pick.date_done:
                    tgl = str(pick.date_done)[:10]
                if not pick.dokumen_ids :
                    data = []
                    dokumen_sj = self.env['vit.dokumen.master'].sudo().search([('code','=','640')],limit=1)
                    if dokumen_sj :
                        data.append((0,0, {'dokumen_master_id':dokumen_sj.id,'nomor':pick.name,'tanggal':tgl}))
                    dokumen_inv = self.env['vit.dokumen.master'].sudo().search([('code','=','380')],limit=1)
                    if dokumen_inv :
                        inv = self.env['account.invoice'].sudo().search([('origin','=',pick.name),('type','=','out_invoice')])
                        if inv :
                            data.append((0,0, {'dokumen_master_id':dokumen_inv.id,'nomor':inv.number,'tanggal':tgl}))
                    pick.dokumen_ids = data
                if pick.sj_external :
                    pick.no_sj_pengirim = pick.sj_external
        return res

    nomor_aju_id = fields.Many2one('vit.nomor.aju', string='No Aju', ondelete='restrict', track_visibility='on_change', copy=False)
    nama_pengangkut = fields.Char(string='Nama Pengangkut', track_visibility='on_change', copy=False)
    tgl_ttd = fields.Date('Tanggal TTD', track_visibility='on_change', copy=False)
    no_polisi = fields.Char('Nomor Polisi', track_visibility='on_change', copy=False)
    seri_dokumen = fields.Char('Seri Dokumen', track_visibility='on_change', copy=False)
    tgl_dokumen = fields.Date('Tanggal Dokumen', track_visibility='on_change', copy=False)
    no_sj_pengirim = fields.Char('No SJ Pengirim', track_visibility='on_change', copy=False)
    no_pabean = fields.Char('Nomor Pabean', size=100, track_visibility='on_change', copy=False)
    tgl_pabean = fields.Date('Tanggal Pabean', track_visibility='on_change', copy=False)
    status_aju = fields.Char('Status Dokumen', track_visibility='on_change', copy=False)
    kode_cara_angkut = fields.Char('Kode Cara Angkut', default='3', track_visibility='on_change', copy=False)
    kontainer_ids = fields.One2many('vit.kontainer', 'picking_id', string='Kontainer', track_visibility='on_change', copy=False)
    kemasan_ids = fields.One2many('vit.kemasan', 'picking_id', string='Kemasan', track_visibility='on_change', copy=False)
    dokumen_ids = fields.One2many('vit.dokumen', 'picking_id', string='Dokumen', track_visibility='on_change', copy=False)
    has_export = fields.Boolean('Has Export', copy=False, track_visibility='on_change')
    kode_negara_pemasok_id = fields.Many2one('vit.kode.negara.pemasok', 'Kode Negara Pemasok', track_visibility='on_change', copy=False)
    pelabuhan_bongkar_id = fields.Many2one('vit.pelabuhan.bongkar', 'Pelabuhan Bongkar', track_visibility='on_change', copy=False)
    pelabuhan_transit_id = fields.Many2one('vit.pelabuhan.transit', 'Pelabuhan Transit', track_visibility='on_change', copy=False)
    pelabuhan_muat_id = fields.Many2one('vit.pelabuhan.muat', 'Pelabuhan Muat', track_visibility='on_change', copy=False)
    freight = fields.Float('Freight', track_visibility='on_change', copy=False)
    ndpbm_id = fields.Many2one('vit.kurs.pajak', 'Kurs Pajak/NDPBM', track_visibility='on_change', copy=False)
    tempat_penimbun_id = fields.Many2one('vit.tempat.penimbun', 'Tempat Penimbun', track_visibility='on_change', copy=False)
    kode_harga_id = fields.Many2one('vit.kode.harga', 'Kode Harga', track_visibility='on_change', copy=False)
    jenis_angkut_id = fields.Many2one('vit.jenis.angkut', string='Jenis Angkut', track_visibility='on_change', copy=False)
    tarif_hs_ids = fields.One2many('vit.tarif.hs', 'picking_id', string='Tarif/HS', track_visibility='on_change', copy=False)
    source_usage = fields.Selection(related='location_id.usage', string='Source Usage')
    dest_usage = fields.Selection(related='location_dest_id.usage', string='Destination Usage')
    is_bea_cukai = fields.Boolean('Is Bea Cukai', track_visibility='on_change')
    has_export_aju = fields.Boolean(related='nomor_aju_id.has_export', string='Has Export Aju',store=True)
    sj_external = fields.Char('SJ Eksternal', copy=False)

    @api.onchange('no_sj_pengirim','sj_external')
    def onchange_sj_external(self):
        if self.picking_type_id and self.picking_type_id.code == 'incoming' :
            if not self.dokumen_ids :
                data = []
                dokumen_sj = self.env['vit.dokumen.master'].sudo().search([('code','=','640')],limit=1)
                if dokumen_sj :
                    data.append((0,0, {'dokumen_master_id':dokumen_sj.id,'nomor':self.no_sj_pengirim}))
                dokumen_inv = self.env['vit.dokumen.master'].sudo().search([('code','=','380')],limit=1)
                if dokumen_inv :
                    inv = self.env['account.invoice'].sudo().search([('reference','=',self.no_sj_pengirim),('origin','=',self.name),('reference','!=',False)])
                    if inv :
                        data.append((0,0, {'dokumen_master_id':dokumen_inv.id,'nomor':inv.reference}))
                self.dokumen_ids = data
            if self.sj_external :
                self.no_sj_pengirim = self.sj_external
        elif self.picking_type_id and self.picking_type_id.code == 'outgoing' :
            if not self.dokumen_ids :
                data = []
                dokumen_sj = self.env['vit.dokumen.master'].sudo().search([('code','=','640')],limit=1)
                if dokumen_sj :
                    data.append((0,0, {'dokumen_master_id':dokumen_sj.id,'nomor':self.name}))
                dokumen_inv = self.env['vit.dokumen.master'].sudo().search([('code','=','380')],limit=1)
                if dokumen_inv :
                    inv = self.env['account.invoice'].sudo().search([('origin','=',self.name),('type','=','out_invoice')])
                    if inv :
                        data.append((0,0, {'dokumen_master_id':dokumen_inv.id,'nomor':inv.number}))
                self.dokumen_ids = data
            if self.sj_external :
                self.no_sj_pengirim = self.sj_external


class StockMove(models.Model):
    _inherit = 'stock.move'


    seri_barang = fields.Char('Seri Barang')
    # netto = fields.Float(string='Netto', digits=dp.get_precision('Stock Weight'), help="netto perbarang (untuk laporan bc)", store=True)
    # brutto = fields.Float(string='Brutto',digits=dp.get_precision('Stock Weight'), help="netto perbarang (untuk laporan bc)", store=True)
    picking_code = fields.Selection(related='picking_id.picking_type_id.code', readonly=True, store=True)


class StockQuantPackage(models.Model):
    _inherit = 'stock.quant.package'

    weight = fields.Float(string='Weight', digits=dp.get_precision('Stock Weight'), default=0.8)
    merk = fields.Char('Merk')
    seri_dokumen = fields.Char('Seri Dokumen')
    
    
class VitKontainer(models.Model):
    _name = "vit.kontainer"
    _description = "Kontainer"

    picking_id = fields.Many2one('stock.picking', string='Picking', ondelete='cascade')
    name = fields.Char('Nomor Kontainer', required=True)
    seri_kontainer = fields.Char('Seri Kontainer')
    kesesuaian_dokumen = fields.Char('Kesesuaian Dokumen')
    keterangan = fields.Char('Keterangan')
    kode_stuffing = fields.Char('Kode Stuffing')
    kode_tipe_kontainer = fields.Char('Kode Tipe Kontainer')
    kode_ukuran_kontainer = fields.Char('Kode Ukuran kontainer')
    flag_gate_in = fields.Char('Flag Gate In')
    flag_gate_out = fields.Char('Flag Gate Out')
    nomor_polisi = fields.Char('Nomor Polisi')
    nomor_segel = fields.Char('Nomor Segel')
    waktu_gate_in = fields.Date('Waktu Gate In')
    waktu_gate_out = fields.Date('Waktu Gate Out')

class VitKemasan(models.Model):
    _name = "vit.kemasan"
    _description = "Kemasan"
    _rec_name = 'product_id'

    picking_id = fields.Many2one('stock.picking', string='Picking', ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Product')
    package_type_id = fields.Many2one('stock.quant.package.type', string='Package')
    qty = fields.Integer('Jumlah')
    seri = fields.Integer('Seri')

class VitDokumen(models.Model):
    _name = "vit.dokumen"
    _description = "Detail Dokumen"

    @api.depends("sequence")
    def _get_seri(self):
        for dok in self:
            if dok.sequence:
                dok.seri = dok.sequence+1
            else :
                dok.seri = 1

    @api.multi
    @api.onchange('dokumen_master_id')
    def onchange_dokumen_master_id(self):
        picking = False
        if self.dokumen_master_id and self.dokumen_master_id.code == '380':
            if not self.picking_id.name and 'params' in self._context:
                if self._context['params'] and self._context['params']['model'] and self._context['params']['model'] == 'stock.picking':
                    picking_id = self._context['params']['id']
                    picking = self.env['stock.picking'].browse(picking_id)     
            else : 
                picking = self.picking_id
            if picking:    
                if picking.picking_type_id.code == 'incoming':
                    dokumen_inv = self.env['account.invoice'].sudo().search([('origin','=',picking.origin),
                                                                            ('reference','!=',False),
                                                                            ('type','=','in_invoice'),
                                                                            ('partner_id','=',picking.partner_id.id)],limit=1)
                    if dokumen_inv and dokumen_inv.reference:
                        self.nomor = dokumen_inv.reference
                elif picking.picking_type_id.code == 'outgoing':
                    dokumen_inv = self.env['account.invoice'].sudo().search([('origin','=',picking.name),
                                                                ('type','=','out_invoice'),
                                                                ('partner_id','=',picking.partner_id.id)],limit=1)
                    if dokumen_inv and dokumen_inv.number:
                        self.nomor = dokumen_inv.number or '/'

    picking_id = fields.Many2one('stock.picking', string='Picking', ondelete='cascade')
    seri = fields.Integer('Seri', compute="_get_seri", store=True)
    sequence = fields.Integer('Seq')
    dokumen_master_id = fields.Many2one('vit.dokumen.master',string='Kode Dokumen', required=True)
    #dokumen_master_id = fields.Many2one('vit.kode.dokumen',string='Kode Dokumen', required=True)
    jenis_dokumen = fields.Char(related='dokumen_master_id.name',string='Jenis Dokumen', store=True)
    nomor = fields.Char('Nomor')
    tanggal = fields.Date('Tanggal')


class VitDokumenMaster(models.Model):
    _name = "vit.dokumen.master"
    _description = "Master Kode Dokumen"
    _rec_name = "code"

    @api.multi
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.code :
                name = res.code+' - '+name
            result.append((res.id, name))
        return result

    code = fields.Char('Kode Dokumen', required=True)
    name = fields.Char('Jenis Dokumen', required=True)
    notes = fields.Text('Notes')
