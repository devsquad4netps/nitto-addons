from odoo import fields, models, api, _
from odoo.exceptions import Warning

class VitExportImportBc(models.Model):
    _name = "vit.export.import.bc"
    _description = "Export Import BC"

    name = fields.Selection([], string='Jenis BC', required=True)
    kppbc = fields.Char('KPPBC')
    status = fields.Char('Status')
    kode_dokumen_pabean = fields.Char('Kode Dokumen Pabean')
    kode_dokumen_bc = fields.Char('Kode Dokumen BC')
    asal_data = fields.Char('Asal Data')
    id_modul = fields.Char('ID Modul')
    jabatan_ttd = fields.Char('Jabatan TTD')
    kode_id_pengirim = fields.Char('Kode ID Pengirim')
    kode_id_penerima = fields.Char('Kode ID Penerima')
    kode_id_pengusaha = fields.Char('Kode ID Pengusaha')
    kode_id_pemilik = fields.Char('Kode ID Pemilik', default='1')
    kode_jenis_tpb = fields.Char('Kode Jenis TPB')
    kode_tujuan_pengiriman = fields.Char('Kode Tujuan Pengiriman')
    kode_tujuan_tpb = fields.Char('Kode Tujuan TPB')
    kota_ttd = fields.Char('Kota TTD')
    nama_ttd = fields.Char('Nama TTD')
    nomor_ijin_tpb = fields.Char('Nomor Ijin TPB')
    seri = fields.Char('Seri')
    versi_modul = fields.Char('Versi Modul')
    kode_status = fields.Char('Kode Status')
    kode_jenis_dokumen_sj = fields.Char('Kode Jenis Dokumen SJ')
    kode_jenis_dokumen_inv = fields.Char('Kode Jenis Dokumen Invoice')
    kode_jenis_dokumen_package = fields.Char('Kode Jenis Dokumen Packing List')
    kode_jenis_dokumen_bahan_baku = fields.Char('Kode Jenis Dokumen Bahan Baku')
    tipe_dokumen = fields.Char('Tipe Dokumen')
    tipe_dokumen2 = fields.Char('Tipe Dokumen 2')
    kode_asal_bahan_baku = fields.Char('Kode Asal Bahan Baku', default=1)
    kode_riwayat_barang = fields.Char('Kode Riwayat Barang')
    kode_kantor_pabean_asal = fields.Char('Kode Kantor Pabean Asal')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    api_pemilik = fields.Char('API Pemilik')
    api_pengusaha = fields.Char('API Pengusaha')
    kode_jenis_api_pemilik = fields.Char('Kode Jenis API Pemilik', default='2')
    kode_jenis_api_pengusaha = fields.Char('Kode Jenis API Pengusaha', default='2')
    pos_tarif = fields.Char('POS Tarif') #salah
    

    _sql_constraints = [
        ('unique_name', 'unique(name)', 'Jenis BC sudah ada, silahkan cek kembali.'),
    ]
