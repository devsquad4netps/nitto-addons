from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import datetime

class ResUsers(models.Model):
    _inherit = 'res.users'

    def get_rate(self, rate_date, currency_id=False):
        if not currency_id :
            currency_id = self.env.ref('base.USD')
        if not isinstance(rate_date, str):
            rate_date = rate_date.strftime('%Y-%m-%d')
        rate_date = rate_date[:10]
        rate_id = self.env['vit.kurs.pajak.rate'].search([
            ('kurs_pajak_id.currency_id', '=', currency_id.id),
            ('date', '=', rate_date),
        ], limit=1)
        return rate_id

    def convert_to_usd(self, amount, rate_date):
        if isinstance(amount, bool):
            amount = 0
        if not rate_date or not amount :
            return amount
        rate_id = self.get_rate(rate_date=rate_date)
        if rate_id and rate_id.rate :
            amount = amount / rate_id.rate
        return amount

    def convert_to_ndpbm(self, amount, aju_id, rate_date):
        if not aju_id or not amount or not rate_date :
            return amount
        usd_id = self.env.ref('base.USD')
        kanban_id = self.get_kanban(aju_id=aju_id)
        if kanban_id :
            if kanban_id.ndpbm_id and kanban_id.ndpbm_id.currency_id != usd_id :
                rate_id = self.get_rate(rate_date=rate_date)
                new_rate_id = self.get_rate(rate_date=rate_date, currency_id=kanban_id.ndpbm_id.currency_id)
                if rate_id and rate_id.rate and new_rate_id and new_rate_id.rate :
                    amount = amount * rate_id.rate
                    amount = amount / new_rate_id.rate
        return amount

    def get_kanban(self, aju_id, picking_code='incoming'):
        kanban_id = self.env['vit.kanban'].search([
            ('nomor_aju_id', '=', aju_id.id),
            ('picking_code', '=', picking_code),
        ], limit=1)
        return kanban_id
