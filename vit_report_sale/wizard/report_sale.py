from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportSaleWizard(models.TransientModel):
    _name = "vit.report.sale.wizard"
    _description = "Report Sales"


    @api.onchange('type','company_ids')
    def onchange_type(self):
        if self.type and self.type == 'amount' or self.company_ids:
            acc = self.env['account.account'].sudo().search([('code','=','411-00-000'),('company_id','in',self.company_ids.ids)])
            if acc :
                self.account_ids = [(6, 0, acc.ids)]
        else :
            self.account_ids = False

    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)  
    notes = fields.Text("Notes") 
    type = fields.Selection([('amount','AMOUNT'),('qty','QTY'),('amount_qty','AMOUNT & QTY')], string="Type",required=True)
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    year = fields.Selection([(num, str(num)) for num in reversed(range((datetime.now().year)-4, (datetime.now().year)+1 ))], string='Year', default=str((datetime.now().year)),required=True)
    account_ids = fields.Many2many("account.account",string='Product Sales Account')

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'left','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000'})
        wbf['header_table'].set_border()

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        wbf['header_month_sum'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_border()
        wbf['content_number'].set_right() 

        wbf['content_number_sum'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['dark_grey']})
        wbf['content_number_sum'].set_border()
        wbf['content_number_sum'].set_right() 
        
        return wbf, workbook

    @api.multi
    def action_print_report(self):
        self.ensure_one()
        for report in self :
            i_type = dict(self._fields['type'].selection).get(report.type)
            report_name = 'LAPORAN SALES %s (%s)'%(report.year,i_type)
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + "+"
            company = company[:-1]
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")
            accounts = str(tuple(report.account_ids.ids)).replace(",)",")")
            if i_type == 'AMOUNT & QTY' :
                report_name = 'LAPORAN SALES %s (%s)'%(report.year,'AMOUNT+QTY') # rename karena keterbatasan karakter di sheet excel
                #raise Warning('Report Sales (%s) masih onprogress...' % (i_type))
                return report.action_print_report_qty_and_amount(i_type, report_name, company, companys, accounts)
            else :
                return report.action_print_report_qty_or_amount(i_type, report_name, company, companys, accounts)

    def action_print_report_qty_or_amount(self, i_type, report_name, company, companys, accounts):
        for i in self :
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name+' ('+i_type+')')
            worksheet.set_column('A1:A1', 10)
            worksheet.set_column('B1:B1', 10)
            worksheet.set_column('C1:C1', 10)
            worksheet.set_column('D1:D1', 15)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:J2', report_name, wbf['company'])
            row = 4

            # JAN JUN
            worksheet.merge_range('A%s:C%s'%(row,row), '', wbf['header_table'])
            worksheet.write('D%s'%(row), 'JAN', wbf['header_month'])
            worksheet.write('E%s'%(row), 'FEB', wbf['header_month'])
            worksheet.write('F%s'%(row), 'MAR', wbf['header_month'])
            worksheet.write('G%s'%(row), 'APR', wbf['header_month'])
            worksheet.write('H%s'%(row), 'MEI', wbf['header_month'])
            worksheet.write('I%s'%(row), 'JUN', wbf['header_month'])
            worksheet.write('J%s'%(row), 'JAN-JUN', wbf['header_month_sum'])

            worksheet.merge_range('A%s:A%s'%(row+1,row+5), 'NAI', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+1,row+1), 'DPIL', wbf['header_no'])
            worksheet.merge_range('B%s:B%s'%(row+2,row+4), 'KB', wbf['header_no'])
            worksheet.write('C%s'%(row+2), 'EPSON', wbf['header_no'])
            worksheet.write('C%s'%(row+3), 'OTHERS', wbf['header_no'])
            worksheet.write('C%s'%(row+4), 'TOTAL', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+5,row+5), 'TOTAL', wbf['header_no'])

            worksheet.merge_range('A%s:A%s'%(row+6,row+10), 'NON NAI', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+6,row+6), 'DPIL', wbf['header_no'])
            worksheet.merge_range('B%s:B%s'%(row+7,row+9), 'KB', wbf['header_no'])
            worksheet.write('C%s'%(row+7), 'EPSON', wbf['header_no'])
            worksheet.write('C%s'%(row+8), 'OTHERS', wbf['header_no'])
            worksheet.write('C%s'%(row+9), 'TOTAL', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+10,row+10), 'TOTAL', wbf['header_no'])

            worksheet.write('A%s'%(row+11), 'NAI', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+11,row+11), 'EXPORT', wbf['header_no'])
            worksheet.merge_range('A%s:C%s'%(row+12,row+12), 'TOTAL', wbf['header_no'])

            # Create Value
            for alpha in ['D','E','F','G','H','I'] :
                mon = self.convert_month(alpha, 'qty_or_amount')
                if i.type == 'qty' :
                    #NAI DPIL QTY
                    qty = self.get_qty_dpil(alpha,i.year,companys,'not ilike')
                    digit = qty['qty'] if qty and qty['qty'] != None else 0
                    #NAI KB EPSON QTY
                    qty2 = self.get_qty_kb_epson(alpha,i.year,companys,'not ilike')
                    digit2 = qty2['qty'] if qty2 and qty2['qty'] != None else 0
                    #NAI KB NON EPSON QTY
                    qty3 = self.get_qty_kb_others(alpha,i.year,companys,'not ilike')
                    digit3 = qty3['qty'] if qty3 and qty3['qty'] != None else 0
                elif i.type == 'amount' :
                    #NAI DPIL AMOUNT
                    amount = self.get_amount_dpil(alpha,i.year,companys,'not ilike', accounts)
                    digit = amount['amount'] if amount and amount['amount'] != None else 0
                    # NAI KB EPSON AMOUNT
                    amount2 = self.get_amount_kb_epson(alpha,i.year,companys,'not ilike', accounts)
                    digit2 = amount2['amount'] if amount2 and amount2['amount'] != None else 0
                    #NAI KB NON EPSON AMOUNT
                    amount3 = self.get_amount_kb_others(alpha,i.year,companys,'not ilike', accounts)
                    digit3 = amount3['amount'] if amount3 and amount3['amount'] != None else 0
                worksheet.write(alpha+'%s'%(row+1), digit, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+2), digit2, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+3), digit3, wbf['content_number'])

                #Total NAI KB EPSON DAN NON EPSON
                worksheet.write_formula(alpha+'%s'%(row+4), '=sum(%s%s:%s%s)' % (alpha,row+2,alpha, row+3), wbf['content_number'])
                worksheet.write_formula(alpha+'%s'%(row+5), '=sum(%s%s:%s%s)' % (alpha,row+1,alpha, row+3), wbf['content_number'])
                if i.type == 'qty' :
                    #NON NAI DPIL QTY
                    qty4 = self.get_qty_dpil(alpha,i.year,companys,'ilike')
                    digit4 = qty4['qty'] if qty4 and qty4['qty'] != None else 0
                    #NON NAI KB EPSON QTY
                    qty5 = self.get_qty_kb_epson(alpha,i.year,companys,'ilike')
                    digit5 = qty5['qty'] if qty5 and qty5['qty'] != None else 0  
                    #NON NAI KB NON EPSON QTY
                    qty6 = self.get_qty_kb_others(alpha,i.year,companys,'ilike')
                    digit6 = qty6['qty'] if qty6 and qty6['qty'] != None else 0
                    #Total NAI dan Export QTY
                    qty7 = self.get_qty_export(alpha,i.year,companys,'ilike')
                    digit7 = qty7['qty'] if qty7 and qty7['qty'] != None else 0
                if i.type == 'amount' :
                    #NON NAI DPIL AMOUNT
                    amount4 = self.get_amount_dpil(alpha,i.year,companys,'ilike', accounts)
                    digit4 = amount4['amount'] if amount4 and amount4['amount'] != None else 0
                    #NON NAI KB EPSON AMOUNT
                    amount5 = self.get_amount_kb_epson(alpha,i.year,companys,'ilike', accounts)
                    digit5 = amount5['amount'] if amount5 and amount5['amount'] != None else 0  
                    #NON NAI KB NON EPSON AMOUNT
                    amount6 = self.get_amount_kb_others(alpha,i.year,companys,'ilike', accounts)
                    digit6 = amount6['amount'] if amount6 and amount6['amount'] != None else 0
                    #Total NAI dan Export AMOUNT
                    qty7 = self.get_amount_export(alpha,i.year,companys,'ilike', accounts)
                    digit7 = qty7['amount'] if qty7 and qty7['amount'] != None else 0
                    
                worksheet.write(alpha+'%s'%(row+6), digit4, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+7), digit5, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+8), digit6, wbf['content_number'])
                #Total NON NAI KB EPSON DAN NON EPSON QTY
                worksheet.write_formula(alpha+'%s'%(row+9), '=sum(%s%s:%s%s)' % (alpha,row+7,alpha, row+8), wbf['content_number'])
                worksheet.write_formula(alpha+'%s'%(row+10), '=sum(%s%s:%s%s)' % (alpha,row+6,alpha, row+8), wbf['content_number'])
                #Total NAI dan Export QTY
                worksheet.write(alpha+'%s'%(row+11), digit7, wbf['content_number'])
                #Total ALL QTY
                worksheet.write_formula(alpha+'%s'%(row+12), '=sum(%s%s:%s%s:%s%s)' % (alpha,row+5,alpha, row+10,alpha, row+11), wbf['content_number'])

            worksheet.write_formula('J%s'%(row+1), '=sum(D%s:I%s)' % (row+1, row+1), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+2), '=sum(D%s:I%s)' % (row+2, row+2), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+3), '=sum(D%s:I%s)' % (row+3, row+3), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+4), '=sum(D%s:I%s)' % (row+4, row+4), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+5), '=sum(D%s:I%s)' % (row+5, row+5), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+6), '=sum(D%s:I%s)' % (row+6, row+6), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+7), '=sum(D%s:I%s)' % (row+7, row+7), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+8), '=sum(D%s:I%s)' % (row+8, row+8), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+9), '=sum(D%s:I%s)' % (row+9, row+9), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+10), '=sum(D%s:I%s)' % (row+10, row+10), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+11), '=sum(D%s:I%s)' % (row+11, row+11), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+12), '=sum(D%s:I%s)' % (row+12, row+12), wbf['content_number_sum'])

            row += 14
            # JUL DEC
            worksheet.merge_range('A%s:C%s'%(row,row), '', wbf['header_table'])
            worksheet.write('D%s'%(row), 'JUL', wbf['header_month'])
            worksheet.write('E%s'%(row), 'AUG', wbf['header_month'])
            worksheet.write('F%s'%(row), 'SEP', wbf['header_month'])
            worksheet.write('G%s'%(row), 'OCT', wbf['header_month'])
            worksheet.write('H%s'%(row), 'NOV', wbf['header_month'])
            worksheet.write('I%s'%(row), 'DEC', wbf['header_month'])
            worksheet.write('J%s'%(row), 'JUL-DEC', wbf['header_month_sum'])

            worksheet.merge_range('A%s:A%s'%(row+1,row+5), 'NAI', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+1,row+1), 'DPIL', wbf['header_no'])
            worksheet.merge_range('B%s:B%s'%(row+2,row+4), 'KB', wbf['header_no'])
            worksheet.write('C%s'%(row+2), 'EPSON', wbf['header_no'])
            worksheet.write('C%s'%(row+3), 'OTHERS', wbf['header_no'])
            worksheet.write('C%s'%(row+4), 'TOTAL', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+5,row+5), 'TOTAL', wbf['header_no'])

            worksheet.merge_range('A%s:A%s'%(row+6,row+10), 'NON NAI', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+6,row+6), 'DPIL', wbf['header_no'])
            worksheet.merge_range('B%s:B%s'%(row+7,row+9), 'KB', wbf['header_no'])
            worksheet.write('C%s'%(row+7), 'EPSON', wbf['header_no'])
            worksheet.write('C%s'%(row+8), 'OTHERS', wbf['header_no'])
            worksheet.write('C%s'%(row+9), 'TOTAL', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+10,row+10), 'TOTAL', wbf['header_no'])

            worksheet.write('A%s'%(row+11), 'NAI', wbf['header_no'])
            worksheet.merge_range('B%s:C%s'%(row+11,row+11), 'EXPORT', wbf['header_no'])
            worksheet.merge_range('A%s:C%s'%(row+12,row+12), 'TOTAL', wbf['header_no'])

            # Create Value
            for alpha in ['D','E','F','G','H','I'] :
                mon = self.convert_month(alpha+'2', 'qty_or_amount')
                if i.type == 'qty' :
                    #NAI DPIL QTY
                    qty = self.get_qty_dpil(mon,i.year,companys,'not ilike')
                    digit = qty['qty'] if qty and qty['qty'] != None else 0
                    #NAI KB EPSON QTY
                    qty2 = self.get_qty_kb_epson(mon,i.year,companys,'not ilike')
                    digit2 = qty2['qty'] if qty2 and qty2['qty'] != None else 0
                    #NAI KB NON EPSON QTY
                    qty3 = self.get_qty_kb_others(mon,i.year,companys,'not ilike')
                    digit3 = qty3['qty'] if qty3 and qty3['qty'] != None else 0
                elif i.type == 'amount' :
                    #NAI DPIL AMOUNT
                    amount = self.get_amount_dpil(mon,i.year,companys,'not ilike', accounts)
                    digit = amount['amount'] if amount and amount['amount'] != None else 0
                    #NAI KB EPSON AMOUNT
                    amount2 = self.get_amount_kb_epson(mon,i.year,companys,'not ilike', accounts)
                    digit2 = amount2['amount'] if amount2 and amount2['amount'] != None else 0
                    #NAI KB NON EPSON AMOUNT
                    amount3 = self.get_amount_kb_others(mon,i.year,companys,'not ilike', accounts)
                    digit3 = amount3['amount'] if amount3 and amount3['amount'] != None else 0
                worksheet.write(alpha+'%s'%(row+1), digit, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+2), digit2, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+3), digit3, wbf['content_number'])

                # TOTAL NAI EPSON dan NON EPSON
                worksheet.write_formula(alpha+'%s'%(row+4), '=sum(%s%s:%s%s)' % (alpha,row+2,alpha, row+3), wbf['content_number'])
                worksheet.write_formula(alpha+'%s'%(row+5), '=sum(%s%s:%s%s)' % (alpha,row+1,alpha, row+3), wbf['content_number'])
                if i.type == 'qty' :
                    # NON NAI DPIL QTY
                    qty4 = self.get_qty_dpil(mon,i.year,companys,'ilike')
                    digit4 = qty4['qty'] if qty4 and qty4['qty'] != None else 0
                    #NON NAI KB EPSON QTY
                    qty5 = self.get_qty_kb_epson(mon,i.year,companys,'ilike')
                    digit5 = qty5['qty'] if qty5 and qty5['qty'] != None else 0
                    #NON NAI KB NON EPSON QTY
                    qty6 = self.get_qty_kb_others(mon,i.year,companys,'ilike')
                    digit6 = qty6['qty'] if qty6 and qty6['qty'] != None else 0
                    #Total NAI dan Export QTY
                    qty7 = self.get_qty_export(mon,i.year,companys,'ilike')
                    digit7 = qty7['qty'] if qty7 and qty7['qty'] != None else 0
                elif i.type == 'amount' :
                    # NON NAI DPIL AMOUNT
                    amount4 = self.get_amount_dpil(mon,i.year,companys,'ilike', accounts)
                    digit4 = amount4['amount'] if amount4 and amount4['amount'] != None else 0
                    #NON NAI KB EPSON AMOUNT
                    amount5 = self.get_amount_kb_epson(mon,i.year,companys,'ilike', accounts)
                    digit5 = amount5['amount'] if amount5 and amount5['amount'] != None else 0
                    #NON NAI KB NON EPSON AMOUNT
                    amount6 = self.get_amount_kb_others(mon,i.year,companys,'ilike', accounts)
                    digit6 = amount6['amount'] if amount6 and amount6['amount'] != None else 0
                    #Total NAI dan Export AMOUNT
                    amount7 = self.get_amount_export(mon,i.year,companys, 'ilike', accounts)
                    digit7 = amount7['amount'] if amount7 and amount7['amount'] != None else 0

                worksheet.write(alpha+'%s'%(row+6), digit4, wbf['content_number'])                
                worksheet.write(alpha+'%s'%(row+7), digit5, wbf['content_number'])
                worksheet.write(alpha+'%s'%(row+8), digit6, wbf['content_number'])
                #Total NON NAI KB EPSON DAN NON EPSON
                worksheet.write_formula(alpha+'%s'%(row+9), '=sum(%s%s:%s%s)' % (alpha,row+7,alpha, row+8), wbf['content_number'])
                worksheet.write_formula(alpha+'%s'%(row+10), '=sum(%s%s:%s%s)' % (alpha,row+6,alpha, row+8), wbf['content_number'])
                
                worksheet.write(alpha+'%s'%(row+11), digit7, wbf['content_number'])
                #Total ALL
                worksheet.write_formula(alpha+'%s'%(row+12), '=sum(%s%s:%s%s:%s%s)' % (alpha,row+5,alpha, row+10,alpha, row+11), wbf['content_number'])

            worksheet.write_formula('J%s'%(row+1), '=sum(D%s:I%s)' % (row+1, row+1), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+2), '=sum(D%s:I%s)' % (row+2, row+2), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+3), '=sum(D%s:I%s)' % (row+3, row+3), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+4), '=sum(D%s:I%s)' % (row+4, row+4), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+5), '=sum(D%s:I%s)' % (row+5, row+5), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+6), '=sum(D%s:I%s)' % (row+6, row+6), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+7), '=sum(D%s:I%s)' % (row+7, row+7), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+8), '=sum(D%s:I%s)' % (row+8, row+8), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+9), '=sum(D%s:I%s)' % (row+9, row+9), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+10), '=sum(D%s:I%s)' % (row+10, row+10), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+11), '=sum(D%s:I%s)' % (row+11, row+11), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row+12), '=sum(D%s:I%s)' % (row+12, row+12), wbf['content_number_sum'])
            
            if i.notes :
                worksheet.merge_range('A%s:J%s'%(row+14,row+14), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def action_print_report_qty_and_amount(self, i_type, report_name, company, companys, accounts):
        alphas = ['C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        for i in self :
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name+' ('+i_type+')')
            worksheet.set_column('A1:A1', 10)
            worksheet.set_column('B1:B1', 10)
            worksheet.set_column('C1:C1', 15)
            worksheet.set_column('D1:D1', 15)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 15)
            worksheet.set_column('N1:N1', 15)
            worksheet.set_column('O1:O1', 15)
            worksheet.set_column('P1:P1', 15)
            worksheet.set_column('Q1:Q1', 15)
            worksheet.set_column('R1:R1', 15)
            worksheet.set_column('S1:S1', 15)
            worksheet.set_column('T1:T1', 15)
            worksheet.set_column('U1:U1', 15)
            worksheet.set_column('V1:V1', 15)
            worksheet.set_column('W1:W1', 15)
            worksheet.set_column('X1:X1', 15)
            worksheet.set_column('Y1:Y1', 15)
            worksheet.set_column('Z1:Z1', 15)

            worksheet.merge_range('A1:Z1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:Z2', report_name, wbf['company'])
            row = 4

            # JAN DEC
            worksheet.merge_range('A%s:B%s'%(row,row+1), 'SALES', wbf['header_no'])
            worksheet.merge_range('C%s:D%s'%(row,row), 'JAN', wbf['header_no'])
            worksheet.merge_range('E%s:F%s'%(row,row), 'FEB', wbf['header_no'])
            worksheet.merge_range('G%s:H%s'%(row,row), 'MAR', wbf['header_no'])
            worksheet.merge_range('I%s:J%s'%(row,row), 'APR', wbf['header_no'])
            worksheet.merge_range('K%s:L%s'%(row,row), 'MEI', wbf['header_no'])
            worksheet.merge_range('M%s:N%s'%(row,row), 'JUN', wbf['header_no'])
            worksheet.merge_range('O%s:P%s'%(row,row), 'JUL', wbf['header_no'])
            worksheet.merge_range('Q%s:R%s'%(row,row), 'AUG', wbf['header_no'])
            worksheet.merge_range('S%s:T%s'%(row,row), 'SEP', wbf['header_no'])
            worksheet.merge_range('U%s:V%s'%(row,row), 'OCT', wbf['header_no'])
            worksheet.merge_range('W%s:X%s'%(row,row), 'NOV', wbf['header_no'])
            worksheet.merge_range('Y%s:Z%s'%(row,row), 'DES', wbf['header_no'])

            worksheet.write('C%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('D%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('E%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('F%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('G%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('H%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('I%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('J%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('K%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('L%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('M%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('N%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('O%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('P%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('Q%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('R%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('S%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('T%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('U%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('V%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('W%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('X%s'%(row+1), 'AMOUNT', wbf['header_no'])
            worksheet.write('Y%s'%(row+1), 'QTY', wbf['header_no'])
            worksheet.write('Z%s'%(row+1), 'AMOUNT', wbf['header_no'])

            #TABEL 1
            # NAI
            worksheet.merge_range('A%s:A%s'%(row+2,row+6), 'NAI', wbf['header_no'])
            worksheet.write('B%s'%(row+2), 'KB', wbf['header_no'])
            worksheet.write('B%s'%(row+3), 'KB EPSON', wbf['header_no'])
            worksheet.write('B%s'%(row+4), 'DPIL', wbf['header_no'])
            worksheet.write('B%s'%(row+5), 'EXPORT', wbf['header_no'])
            worksheet.write('B%s'%(row+6), 'TOTAL', wbf['header_no'])
            # NON NAI
            worksheet.merge_range('A%s:A%s'%(row+8,row+12), 'NON NAI', wbf['header_no'])
            worksheet.write('B%s'%(row+8), 'KB', wbf['header_no'])
            worksheet.write('B%s'%(row+9), 'KB EPSON', wbf['header_no'])
            worksheet.write('B%s'%(row+10), 'DPIL', wbf['header_no'])
            worksheet.write('B%s'%(row+11), 'EXPORT', wbf['header_no'])
            worksheet.write('B%s'%(row+12), 'TOTAL', wbf['header_no'])
            # TOTAL NAI dan NON NAI
            worksheet.merge_range('A%s:A%s'%(row+14,row+17), 'TOTAL', wbf['header_no'])
            worksheet.write('B%s'%(row+14), 'KB', wbf['header_no'])
            worksheet.write('B%s'%(row+15), 'KB EPSON', wbf['header_no'])
            worksheet.write('B%s'%(row+16), 'DPIL', wbf['header_no'])
            worksheet.write('B%s'%(row+17), 'EXPORT', wbf['header_no'])
            # GRAND TOTAL
            worksheet.merge_range('A%s:B%s'%(row+18,row+18), 'GRAND TOTAL', wbf['header_no'])

            # TABEL 2
            # JAN JUN / JUL DEC
            worksheet.merge_range('A%s:B%s'%(row+22,row+23), 'SALES', wbf['header_no'])
            worksheet.merge_range('C%s:D%s'%(row+22,row+22), 'JAN - JUN', wbf['header_no'])
            worksheet.merge_range('E%s:F%s'%(row+22,row+22), 'JUL - DEC', wbf['header_no'])
            worksheet.merge_range('G%s:H%s'%(row+22,row+22), 'JAN - DEC', wbf['header_no'])

            worksheet.write('C%s'%(row+23), 'QTY', wbf['header_no'])
            worksheet.write('D%s'%(row+23), 'AMOUNT', wbf['header_no'])
            worksheet.write('E%s'%(row+23), 'QTY', wbf['header_no'])
            worksheet.write('F%s'%(row+23), 'AMOUNT', wbf['header_no'])
            worksheet.write('G%s'%(row+23), 'QTY', wbf['header_no'])
            worksheet.write('H%s'%(row+23), 'AMOUNT', wbf['header_no'])

            # NAI
            worksheet.merge_range('A%s:A%s'%(row+24,row+28), 'NAI', wbf['header_no'])
            worksheet.write('B%s'%(row+24), 'KB', wbf['header_no'])
            worksheet.write('B%s'%(row+25), 'KB EPSON', wbf['header_no'])
            worksheet.write('B%s'%(row+26), 'DPIL', wbf['header_no'])
            worksheet.write('B%s'%(row+27), 'EXPORT', wbf['header_no'])
            worksheet.write('B%s'%(row+28), 'TOTAL', wbf['header_no'])
            # NON NAI
            worksheet.merge_range('A%s:A%s'%(row+30,row+34), 'NON NAI', wbf['header_no'])
            worksheet.write('B%s'%(row+30), 'KB', wbf['header_no'])
            worksheet.write('B%s'%(row+31), 'KB EPSON', wbf['header_no'])
            worksheet.write('B%s'%(row+32), 'DPIL', wbf['header_no'])
            worksheet.write('B%s'%(row+33), 'EXPORT', wbf['header_no'])
            worksheet.write('B%s'%(row+34), 'TOTAL', wbf['header_no'])
            # TOTAL NAI dan NON NAI
            worksheet.merge_range('A%s:A%s'%(row+36,row+39), 'TOTAL', wbf['header_no'])
            worksheet.write('B%s'%(row+36), 'KB', wbf['header_no'])
            worksheet.write('B%s'%(row+37), 'KB EPSON', wbf['header_no'])
            worksheet.write('B%s'%(row+38), 'DPIL', wbf['header_no'])
            worksheet.write('B%s'%(row+39), 'EXPORT', wbf['header_no'])
            # GRAND TOTAL
            worksheet.merge_range('A%s:B%s'%(row+40,row+40), 'GRAND TOTAL', wbf['header_no'])

            # start value
            is_qty = True
            # import pdb;pdb.set_trace()
            for alpha in alphas :
                mon = self.convert_month(alpha, 'qty_and_amount')
                if is_qty :
                    #NAI KB NON EPSON QTY
                    qty = self.get_qty_kb_others(mon,i.year,companys,'not ilike')
                    digit = qty['qty'] if qty and qty['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+2), digit, wbf['content_number'])
                    #NAI KB EPSON QTY
                    qty2 = self.get_qty_kb_epson(mon,i.year,companys,'not ilike')
                    digit2 = qty2['qty'] if qty2 and qty2['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+3), digit2, wbf['content_number'])
                    #NAI DPIL QTY
                    qty3 = self.get_qty_dpil(mon,i.year,companys,'not ilike')
                    digit3 = qty3['qty'] if qty3 and qty3['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+4), digit3, wbf['content_number'])
                    #NAI EXPORT QTY
                    qty4 = self.get_qty_export(mon,i.year,companys,'not ilike')
                    digit4 = qty4['qty'] if qty4 and qty4['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+5), digit4, wbf['content_number'])

                    #NON NAI KB NON EPSON QTY
                    qty = self.get_qty_kb_others(mon,i.year,companys,'ilike')
                    digit = qty['qty'] if qty and qty['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+8), digit, wbf['content_number'])
                    #NON NAI KB EPSON QTY
                    qty2 = self.get_qty_kb_epson(mon,i.year,companys,'ilike')
                    digit2 = qty2['qty'] if qty2 and qty2['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+9), digit2, wbf['content_number'])
                    #NON NAI DPIL QTY
                    qty3 = self.get_qty_dpil(mon,i.year,companys,'ilike')
                    digit3 = qty3['qty'] if qty3 and qty3['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+10), digit3, wbf['content_number'])
                    #NON NAI EXPORT QTY
                    qty4 = self.get_qty_export(mon,i.year,companys,'ilike')
                    digit4 = qty4['qty'] if qty4 and qty4['qty'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+11), digit4, wbf['content_number'])

                    # tabel 2
                    if alpha == 'C': #QTY
                        # NAI
                        worksheet.write_formula('%s%s'%(alpha,row+24), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+2,row+2,row+2,row+2,row+2,row+2), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+25), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+3,row+3,row+3,row+3,row+3,row+3), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+26), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+4,row+4,row+4,row+4,row+4,row+4), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+27), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+5,row+5,row+5,row+5,row+5,row+5), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+28), '=sum(%s%s:%s%s)'% (alpha,row+24,alpha,row+27), wbf['content_number'])
                        # NON NAI
                        worksheet.write_formula('%s%s'%(alpha,row+30), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+8,row+8,row+8,row+8,row+8,row+8), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+31), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+9,row+9,row+9,row+9,row+9,row+9), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+32), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+10,row+10,row+10,row+10,row+10,row+10), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+33), '=sum(C%s,E%s,G%s,I%s,K%s,M%s)'% (row+11,row+11,row+11,row+11,row+11,row+11), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+34), '=sum(%s%s:%s%s)'% (alpha,row+30,alpha,row+33), wbf['content_number']) 
                        # TOTAL
                        worksheet.write_formula('%s%s'%(alpha,row+36), '=sum(%s%s,%s%s)'% (alpha,row+24,alpha,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+37), '=sum(%s%s,%s%s)'% (alpha,row+25,alpha,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+38), '=sum(%s%s,%s%s)'% (alpha,row+26,alpha,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+39), '=sum(%s%s,%s%s)'% (alpha,row+27,alpha,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+40), '=sum(%s%s:%s%s)'% (alpha,row+36,alpha,row+39), wbf['content_number'])

                    elif alpha == 'E': #QTY
                        # NAI
                        worksheet.write_formula('%s%s'%(alpha,row+24), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'% (row+2,row+2,row+2,row+2,row+2,row+2), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+25), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'% (row+3,row+3,row+3,row+3,row+3,row+3), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+26), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'% (row+4,row+4,row+4,row+4,row+4,row+4), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+27), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'%  (row+5,row+5,row+5,row+5,row+5,row+5), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+28), '=sum(%s%s:%s%s)'% (alpha,row+24,alpha,row+27), wbf['content_number'])
                        # NON NAI
                        worksheet.write_formula('%s%s'%(alpha,row+30), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'% (row+8,row+8,row+8,row+8,row+8,row+8), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+31), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'% (row+9,row+9,row+9,row+9,row+9,row+9), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+32), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'%  (row+10,row+10,row+10,row+10,row+10,row+10), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+33), '=sum(O%s,Q%s,S%s,U%s,W%s,Y%s)'% (row+11,row+11,row+11,row+11,row+11,row+11), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+34), '=sum(%s%s:%s%s)'% (alpha,row+30,alpha,row+33), wbf['content_number']) 
                        # TOTAL
                        worksheet.write_formula('%s%s'%(alpha,row+36), '=sum(%s%s,%s%s)'% (alpha,row+24,alpha,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+37), '=sum(%s%s,%s%s)'% (alpha,row+25,alpha,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+38), '=sum(%s%s,%s%s)'% (alpha,row+26,alpha,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+39), '=sum(%s%s,%s%s)'% (alpha,row+27,alpha,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+40), '=sum(%s%s:%s%s)'% (alpha,row+36,alpha,row+39), wbf['content_number'])

                    elif alpha == 'G' :
                        # NAI JAN-DEC
                        worksheet.write_formula('%s%s'%(alpha,row+24), '=sum(C%s,E%s)'% (row+24,row+24), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+25), '=sum(C%s,E%s)'% (row+25,row+25), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+26), '=sum(C%s,E%s)'% (row+26,row+26), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+27), '=sum(C%s,E%s)'% (row+27,row+27), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+28), '=sum(%s%s:%s%s)'% (alpha,row+24,alpha,row+27), wbf['content_number'])
                        # NON NAI JAN-DEC
                        worksheet.write_formula('%s%s'%(alpha,row+30), '=sum(C%s,E%s)'% (row+30,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+31), '=sum(C%s,E%s)'% (row+31,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+32), '=sum(C%s,E%s)'% (row+32,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+33), '=sum(C%s,E%s)'% (row+33,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+34), '=sum(%s%s:%s%s)'% (alpha,row+30,alpha,row+33), wbf['content_number'])
                        # TOTAL
                        worksheet.write_formula('%s%s'%(alpha,row+36), '=sum(%s%s,%s%s)'% (alpha,row+24,alpha,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+37), '=sum(%s%s,%s%s)'% (alpha,row+25,alpha,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+38), '=sum(%s%s,%s%s)'% (alpha,row+26,alpha,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+39), '=sum(%s%s,%s%s)'% (alpha,row+27,alpha,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+40), '=sum(%s%s:%s%s)'% (alpha,row+36,alpha,row+39), wbf['content_number'])
                    # ganti ke false buat diisi amount
                    is_qty = False
                else :
                    #NAI KB NON EPSON AMOUNT
                    amount = self.get_amount_kb_others(mon,i.year,companys,'not ilike', accounts)
                    digit = amount['amount'] if amount and amount['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+2), digit, wbf['content_number'])
                    #NAI KB EPSON AMOUNT
                    amount2 = self.get_amount_kb_epson(mon,i.year,companys,'not ilike', accounts)
                    digit2 = amount2['amount'] if amount2 and amount2['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+3), digit2, wbf['content_number'])
                    #NAI DPIL AMOUNT
                    amount3 = self.get_amount_dpil(mon,i.year,companys,'not ilike', accounts)
                    digit3 = amount3['amount'] if amount3 and amount3['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+4), digit3, wbf['content_number'])
                    #NAI EXPORT AMOUNT
                    amount4 = self.get_amount_export(mon,i.year,companys,'not ilike', accounts)
                    digit4 = amount4['amount'] if amount4 and amount4['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+5), digit4, wbf['content_number'])

                    #NON NAI KB NON EPSON AMOUNT
                    amount = self.get_amount_kb_others(mon,i.year,companys,'ilike', accounts)
                    digit = amount['amount'] if amount and amount['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+8), digit, wbf['content_number'])
                    #NON NAI KB EPSON QTY
                    amount2 = self.get_amount_kb_epson(mon,i.year,companys,'ilike', accounts)
                    digit2 = amount2['amount'] if amount2 and amount2['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+9), digit2, wbf['content_number'])
                    #NON NAI DPIL QTY
                    amount3 = self.get_amount_dpil(mon,i.year,companys,'ilike', accounts)
                    digit3 = amount3['amount'] if amount3 and amount3['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+10), digit3, wbf['content_number'])
                    #NON NAI EXPORT QTY
                    amount4 = self.get_amount_export(mon,i.year,companys,'ilike', accounts)
                    digit4 = amount4['amount'] if amount4 and amount4['amount'] != None else 0
                    worksheet.write('%s%s'%(alpha,row+11), digit4, wbf['content_number'])

                    if alpha == 'D': #AMOUNT
                        # NAI
                        worksheet.write_formula('%s%s'%(alpha,row+24), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+2,row+2,row+2,row+2,row+2,row+2), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+25), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+3,row+3,row+3,row+3,row+3,row+3), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+26), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+4,row+4,row+4,row+4,row+4,row+4), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+27), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+5,row+5,row+5,row+5,row+5,row+5), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+28), '=sum(%s%s:%s%s)'% (alpha,row+24,alpha,row+27), wbf['content_number'])
                        # NON NAI
                        worksheet.write_formula('%s%s'%(alpha,row+30), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+8,row+8,row+8,row+8,row+8,row+8), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+31), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+9,row+9,row+9,row+9,row+9,row+9), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+32), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+10,row+10,row+10,row+10,row+10,row+10), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+33), '=sum(D%s,F%s,H%s,J%s,L%s,N%s)'% (row+11,row+11,row+11,row+11,row+11,row+11), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+34), '=sum(%s%s:%s%s)'% (alpha,row+30,alpha,row+33), wbf['content_number']) 
                        # TOTAL
                        worksheet.write_formula('%s%s'%(alpha,row+36), '=sum(%s%s,%s%s)'% (alpha,row+24,alpha,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+37), '=sum(%s%s,%s%s)'% (alpha,row+25,alpha,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+38), '=sum(%s%s,%s%s)'% (alpha,row+26,alpha,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+39), '=sum(%s%s,%s%s)'% (alpha,row+27,alpha,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+40), '=sum(%s%s:%s%s)'% (alpha,row+36,alpha,row+39), wbf['content_number'])
                    elif alpha == 'F': #AMOUNT
                        # NAI
                        worksheet.write_formula('%s%s'%(alpha,row+24), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'% (row+2,row+2,row+2,row+2,row+2,row+2), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+25), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'% (row+3,row+3,row+3,row+3,row+3,row+3), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+26), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'% (row+4,row+4,row+4,row+4,row+4,row+4), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+27), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'%  (row+5,row+5,row+5,row+5,row+5,row+5), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+28), '=sum(%s%s:%s%s)'% (alpha,row+24,alpha,row+27), wbf['content_number'])
                        # NON NAI
                        worksheet.write_formula('%s%s'%(alpha,row+30), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'% (row+8,row+8,row+8,row+8,row+8,row+8), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+31), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'% (row+9,row+9,row+9,row+9,row+9,row+9), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+32), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'%  (row+10,row+10,row+10,row+10,row+10,row+10), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+33), '=sum(P%s,R%s,T%s,V%s,X%s,Z%s)'% (row+11,row+11,row+11,row+11,row+11,row+11), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+34), '=sum(%s%s:%s%s)'% (alpha,row+30,alpha,row+33), wbf['content_number'])
                        # TOTAL
                        worksheet.write_formula('%s%s'%(alpha,row+36), '=sum(%s%s,%s%s)'% (alpha,row+24,alpha,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+37), '=sum(%s%s,%s%s)'% (alpha,row+25,alpha,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+38), '=sum(%s%s,%s%s)'% (alpha,row+26,alpha,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+39), '=sum(%s%s,%s%s)'% (alpha,row+27,alpha,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+40), '=sum(%s%s:%s%s)'% (alpha,row+36,alpha,row+39), wbf['content_number']) 

                    elif alpha == 'H' :
                        # NAI JAN-DEC
                        worksheet.write_formula('%s%s'%(alpha,row+24), '=sum(D%s,F%s)'% (row+24,row+24), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+25), '=sum(D%s,F%s)'% (row+25,row+25), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+26), '=sum(D%s,F%s)'% (row+26,row+26), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+27), '=sum(D%s,F%s)'% (row+27,row+27), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+28), '=sum(%s%s:%s%s)'% (alpha,row+24,alpha,row+27), wbf['content_number'])
                        # NON NAI JAN-DEC
                        worksheet.write_formula('%s%s'%(alpha,row+30), '=sum(D%s,F%s)'% (row+30,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+31), '=sum(D%s,F%s)'% (row+31,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+32), '=sum(D%s,F%s)'% (row+32,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+33), '=sum(D%s,F%s)'% (row+33,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+34), '=sum(%s%s:%s%s)'% (alpha,row+30,alpha,row+33), wbf['content_number'])
                        # TOTAL
                        worksheet.write_formula('%s%s'%(alpha,row+36), '=sum(%s%s,%s%s)'% (alpha,row+24,alpha,row+30), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+37), '=sum(%s%s,%s%s)'% (alpha,row+25,alpha,row+31), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+38), '=sum(%s%s,%s%s)'% (alpha,row+26,alpha,row+32), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+39), '=sum(%s%s,%s%s)'% (alpha,row+27,alpha,row+33), wbf['content_number'])
                        worksheet.write_formula('%s%s'%(alpha,row+40), '=sum(%s%s:%s%s)'% (alpha,row+36,alpha,row+39), wbf['content_number'])
                   
                    # ganti true buat diisi qty
                    is_qty = True

                # TOTAL
                worksheet.write_formula('%s%s'%(alpha,row+6), '=sum(%s%s:%s%s)' % (alpha,row+2, alpha,row+5), wbf['content_number'])
                worksheet.write_formula('%s%s'%(alpha,row+12), '=sum(%s%s:%s%s)' % (alpha,row+8, alpha,row+11), wbf['content_number'])

                #SUM TOTAL
                worksheet.write_formula('%s%s'%(alpha,row+14), '=sum(%s%s,%s%s)' % (alpha,row+2, alpha,row+8), wbf['content_number'])
                worksheet.write_formula('%s%s'%(alpha,row+15), '=sum(%s%s,%s%s)' % (alpha,row+3, alpha,row+9), wbf['content_number'])
                worksheet.write_formula('%s%s'%(alpha,row+16), '=sum(%s%s,%s%s)' % (alpha,row+4, alpha,row+10), wbf['content_number'])
                worksheet.write_formula('%s%s'%(alpha,row+17), '=sum(%s%s,%s%s)' % (alpha,row+5, alpha,row+11), wbf['content_number'])

                #GRAND TOTAL
                worksheet.write_formula('%s%s'%(alpha,row+18), '=sum(%s%s:%s%s)' % (alpha,row+14, alpha,row+17), wbf['content_number'])


            # lewati 1 baris (pemisah) untuk table 1
            worksheet.merge_range('A%s:Z%s'%(row+7,row+7), '', wbf['title_doc'])
            worksheet.merge_range('A%s:Z%s'%(row+13,row+13), '', wbf['title_doc'])
            # lewati 1 baris (pemisah) untuk table 2
            worksheet.merge_range('A%s:H%s'%(row+29,row+29), '', wbf['title_doc'])
            worksheet.merge_range('A%s:H%s'%(row+35,row+35), '', wbf['title_doc'])
            
            if i.notes :
                worksheet.merge_range('A%s:Z%s'%(row+42,row+42), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def convert_month(self, mon, type):
        if type == 'qty_or_amount':
            # jika JAN-JUN
            if mon == 'D' :
                mon = 'JAN'
            elif mon == 'E':
                mon = 'FEB'
            elif mon == 'F':
                mon = 'MAR'
            elif mon == 'G':
                mon = 'APR'
            elif mon == 'H':
                mon = 'MEI'
            elif mon == 'I':
                mon = 'JUN'
            # jika JUL-DEC
            elif mon == 'D2':
                mon = 'JUL'
            elif mon == 'E2':
                mon = 'AUG'
            elif mon == 'F2':
                mon = 'SEP' 
            elif mon == 'G2':
                mon = 'OCT'
            elif mon == 'H2':
                mon = 'NOV'
            elif mon == 'I2':
                mon = 'DEC'
        # jika qty dan amount
        else :
            if mon in ('C','D') :
                mon = 'JAN'
            elif mon in ('E','F') :
                mon = 'FEB'
            elif mon in ('G','H') :
                mon = 'MAR'
            elif mon in ('I','J') :
                mon = 'APR'
            elif mon in ('K','L') :
                mon = 'MEI'
            elif mon in ('M','N') :
                mon = 'JUN'
            elif mon in ('O','P') :
                mon = 'JUL'
            elif mon in ('Q','R') :
                mon = 'AUG'
            elif mon in ('S','T') :
                mon = 'SEP'
            elif mon in ('U','V') :
                mon = 'OCT'
            elif mon in ('W','X') :
                mon = 'NOV'
            elif mon in ('Y','Z') :
                mon = 'DEC'

        return mon

    def get_qty_dpil(self, mon, year,company_ids, operator):
        sql = """select sum(sm.product_uom_qty) as qty from stock_move sm 
                left join product_product pp on pp.id = sm.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = sm.partner_id
                left join res_partner_group rpg on rpg.id = rp.group_partner_id
                where pc.name %s 'trading'
                and rpg.name ilike 'dpil'
                and sm.sale_line_id is not null
                and sm.state = 'done'
                and to_char(date_trunc('month', sm.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from sm.date::date) = '%s' --untuk tahun
                and sm.company_id in %s
                group by to_char(date_trunc('month', sm.date::date), 'MON') """ % (operator, mon, year, company_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_amount_dpil(self, mon, year,company_ids, operator, account_ids):
        sql = """select sum(aml.credit) as amount from account_move_line aml 
                left join product_product pp on pp.id = aml.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = aml.partner_id
                left join res_partner_group rpg on rpg.id = rp.group_partner_id
                where pc.name %s 'trading'
                and rpg.name ilike 'dpil'
                and to_char(date_trunc('month', aml.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from aml.date::date) = '%s' --untuk tahun
                and aml.company_id in %s
                and aml.account_id in %s
                group by to_char(date_trunc('month', aml.date::date), 'MON') """ % (operator, mon, year, company_ids, account_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_qty_kb_epson(self, mon, year,company_ids, operator):
        sql = """select sum(sm.product_uom_qty) as qty from stock_move sm 
                left join product_product pp on pp.id = sm.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = sm.partner_id
                left join res_partner_type rpt on rpt.id = rp.type_partner_id
                where pc.name %s 'trading'
                and rp.transaction_type = 'Kawasan Berikat'
                and rpt.name ilike 'epson'
                and sm.sale_line_id is not null
                and sm.state = 'done'
                and to_char(date_trunc('month', sm.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from sm.date::date) = '%s' --untuk tahun
                and sm.company_id in %s
                group by to_char(date_trunc('month', sm.date::date), 'MON') """ % (operator, mon, year, company_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_amount_kb_epson(self, mon, year,company_ids, operator, account_ids):
        sql = """select sum(aml.credit) as amount from account_move_line aml 
                left join product_product pp on pp.id = aml.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = aml.partner_id
                left join res_partner_type rpt on rpt.id = rp.type_partner_id
                where pc.name %s 'trading'
                and rp.transaction_type = 'Kawasan Berikat'
                and rpt.name ilike 'epson'
                and to_char(date_trunc('month', aml.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from aml.date::date) = '%s' --untuk tahun
                and aml.company_id in %s
                and aml.account_id in %s
                group by to_char(date_trunc('month', aml.date::date), 'MON') """ % (operator, mon, year, company_ids, account_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_qty_kb_others(self, mon, year, company_ids, operator):
        sql = """select sum(sm.product_uom_qty) as qty from stock_move sm 
                left join product_product pp on pp.id = sm.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = sm.partner_id
                left join res_partner_type rpt on rpt.id = rp.type_partner_id
                where pc.name %s 'trading'
                and rp.transaction_type = 'Kawasan Berikat'
                and rpt.name not ilike 'epson'
                and sm.sale_line_id is not null
                and sm.state = 'done'
                and to_char(date_trunc('month', sm.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from sm.date::date) = '%s' --untuk tahun
                and sm.company_id in %s
                group by to_char(date_trunc('month', sm.date::date), 'MON') """ % (operator, mon, year, company_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_amount_kb_others(self, mon, year, company_ids, operator, account_ids):
        sql = """select sum(aml.credit) as amount from account_move_line aml 
                left join product_product pp on pp.id = aml.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = aml.partner_id
                left join res_partner_type rpt on rpt.id = rp.type_partner_id
                where pc.name %s 'trading'
                and rp.transaction_type = 'Kawasan Berikat'
                and rpt.name not ilike 'epson'
                and to_char(date_trunc('month', aml.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from aml.date::date) = '%s' --untuk tahun
                and aml.company_id in %s
                and aml.account_id in %s
                group by to_char(date_trunc('month', aml.date::date), 'MON') """ % (operator, mon, year, company_ids, account_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_qty_export(self, mon, year, company_ids, operator):
        sql = """select sum(sm.product_uom_qty) as qty from stock_move sm 
                left join product_product pp on pp.id = sm.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = sm.partner_id
                left join res_partner_type rpt on rpt.id = rp.type_partner_id
                where pc.name %s 'trading'
                and rp.transaction_type = 'Export'
                and sm.sale_line_id is not null
                and sm.state = 'done'
                and to_char(date_trunc('month', sm.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from sm.date::date) = '%s' --untuk tahun
                and sm.company_id in %s
                group by to_char(date_trunc('month', sm.date::date), 'MON') """ % (operator, mon, year, company_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

    def get_amount_export(self, mon, year, company_ids,operator, account_ids):
        sql = """select sum(aml.credit) as amount from account_move_line aml 
                left join product_product pp on pp.id = aml.product_id
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join product_category pc on pc.id = pt.categ_id
                left join res_partner rp on rp.id = aml.partner_id
                left join res_partner_type rpt on rpt.id = rp.type_partner_id
                where pc.name %s 'trading'
                and rp.transaction_type = 'Export'
                and to_char(date_trunc('month', aml.date::date), 'MON') = '%s' --untuk bulan
                and extract(year from aml.date::date) = '%s' --untuk tahun
                and aml.company_id in %s
                and aml.account_id in %s
                group by to_char(date_trunc('month', aml.date::date), 'MON') """ % (operator, mon, year, company_ids, account_ids)
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchone()

ReportSaleWizard()