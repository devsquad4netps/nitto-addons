#-*- coding: utf-8 -*-

{
	"name": "Report Sales",
	"version": "1.1", 
	"depends": [
		'base',
		'account',
		'sale',
		'product_customer_code',
		'vit_kwitansi',
		'vit_do_add',
		'vit_stock_card_pro',
	],
	'author': 'widianajuniar@gmail.com',
	'website': 'http://www.vitraining.com',
	"summary": "",
	"description": """

""",
	"data": [
		"wizard/report_sale.xml",
		"wizard/report_sale_2.xml",
		"views/sale_view.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}