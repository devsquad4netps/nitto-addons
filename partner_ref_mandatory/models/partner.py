# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning


class Partner(models.Model):
    _inherit = 'res.partner'

    ref = fields.Char('Reference',required=False)

    _sql_constraints = [
        ('ref_uniq', 'unique (ref)', 'The code/ref of the partner must be unique !'),
    ]

    @api.model
    def create(self, vals):
      if not 'ref' in vals:
        vals['ref'] = vals['name']
        name_uniq = self.env['res.partner'].search([('name', '=', vals['name'])])
        if name_uniq:
          raise Warning('Partner name field must be unique!')
        partner = super(Partner, self).create(vals)
        return partner
      ref = self.env['res.partner'].search([('ref', '=', vals['ref'])])
      if ref:
        raise Warning('Internal Reference field must be unique!')
      partner = super(Partner, self).create(vals)
      return partner
   
    @api.multi
    def write(self, vals):
      if not 'ref' in vals:
        partner = super(Partner, self).write(vals)
        return partner
      ref = self.env['res.partner'].search([('id','!=',self.id),('ref', '=', vals['ref'])])
      if ref:
        raise Warning('Internal Reference field must be unique!')
      partner = super(Partner, self).write(vals)
      if not self.ref :
        raise Warning('Internal Reference partner field is required!')
      return partner
