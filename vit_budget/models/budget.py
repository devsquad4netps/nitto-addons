# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2018  Odoo SA  (http://www.vitraining.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
from odoo.tools import ustr
from odoo.exceptions import UserError

class CrossoveredBudget(models.Model):
    _inherit = "crossovered.budget"

    @api.model
    def _get_default_department_id(self):
        employee = self.env['hr.employee'].sudo().search([('user_id','=',self._uid)])
        if employee and employee.department_id:
            return employee.department_id.id

    @api.multi
    def action_budget_confirm(self):
        # jika bukan group manager budget harus diconfirm user kepala dept
        if not self.env.user.has_group('vit_budget_purchase.group_budget_manager_accounting') :
            # cek jika harus confirm pimpinan dept
            if not self.department_id.manager_id :
                raise UserError(_('Manager untuk department ini belum di set ! (%s) ') % (str(self.department_id.name)))
            elif self.department_id.manager_id.user_id.id != self._uid :
                raise UserError(_('Anda tidak punya akses untuk menyetujui dokumen ini ! (Manager Dept : %s) ') % (str(self.department_id.manager_id.user_id.name)))
        self.write({'state': 'confirm'})


    name = fields.Char('Budget Name', required=True, states={'done': [('readonly', True)]}, track_visibility='onchange')
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user, track_visibility='onchange')
    date_from = fields.Date('Start Date', required=True, states={'done': [('readonly', True)]}, track_visibility='onchange')
    date_to = fields.Date('End Date', required=True, states={'done': [('readonly', True)]}, track_visibility='onchange')
    company_id = fields.Many2one('res.company', 'Company', required=True,
        default=lambda self: self.env['res.company']._company_default_get('account.budget.post'), track_visibility='onchange')
    department_id = fields.Many2one(comodel_name='hr.department', string='Department',default=_get_default_department_id, track_visibility='onchange')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', related="department_id.analytic_account_id", store=True, track_visibility='onchange')
    total_budget = fields.Float(string="Total Budget",compute="_calc_total_total_budget",store=True, track_visibility='onchange',)
    type = fields.Selection([], string='Budget Type', required=False)

    @api.depends('crossovered_budget_line','crossovered_budget_line.planned_amount' )
    def _calc_total_total_budget(self):
        total = 0.0
        for rec in self:
            for line in rec.crossovered_budget_line:
                total += line.planned_amount
            rec.total_budget = total

    @api.multi
    def write(self, vals):
        _super = super(CrossoveredBudget, self).write(vals)
        if 'analytic_account_id' in vals :
            budget_line = self.env['crossovered.budget.lines'].search([('crossovered_budget_id','=',self.id)])
            for line in budget_line:
                line.write({'analytic_account_id' : vals['analytic_account_id']})
        return _super


    @api.multi
    def unlink(self):
        for bg in self:
            if bg.state != 'draft' :
                raise UserError(_('Data yang bisa dihapus hanya yg berstatus draft'))
        return super(CrossoveredBudget, self).unlink()

CrossoveredBudget()


class AccountBudgetPost(models.Model):
    _inherit = "account.budget.post"


    @api.model
    def create(self, vals):
        if 'account_ids' in vals :
            if vals['account_ids'][0][0] == 1 :
                acc_ids = []
                for i in vals['account_ids'] :
                    acc_ids.append(i[1])
                vals['account_ids'] = [[6,False,acc_ids]]
        return super(AccountBudgetPost, self).create(vals)

    category_id    = fields.Many2one('product.category','Category')
    account_id      = fields.Many2one('account.account','Account')

    @api.onchange('account_id')
    def onchange_account_id(self):
        if self.account_id:
            code = self.account_id.code
            account_ids = []
            account_ids = self.env['account.account'].search([('code', '=like', str(code[:3])+'%'),('company_id','=',self.company_id.id),('id','!=',self.account_id.id)])
            if account_ids :
                self.account_ids = account_ids
            self.name = self.account_id.name

AccountBudgetPost()

class CrossoveredBudgetLines(models.Model):
    _inherit = "crossovered.budget.lines"

    @api.model
    def create(self, vals):
        if vals.get('crossovered_budget_id', False):
            budget = self.env['crossovered.budget'].browse(vals['crossovered_budget_id'])
            if budget.analytic_account_id:
                vals['analytic_account_id'] = budget.analytic_account_id.id
        _super = super(CrossoveredBudgetLines, self)
        return _super.create(vals)

    @api.multi
    def write(self, vals):
        vals['analytic_account_id'] = self.crossovered_budget_id.analytic_account_id.id
        _super = super(CrossoveredBudgetLines, self)
        return _super.write(vals)

    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account' )
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related="crossovered_budget_id.department_id", store=True)
    user_id = fields.Many2one(comodel_name='res.users', string='User', related="crossovered_budget_id.user_id", store=True)


CrossoveredBudgetLines()