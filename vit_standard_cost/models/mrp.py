from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby


class MRPBom(models.Model):
    _inherit   = "mrp.bom"

    standard_cost_ids = fields.One2many('vit.standard.cost','bom_id', 'Standard Cost')

MRPBom()