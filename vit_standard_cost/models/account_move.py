from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby

class StockMove(models.Model):
    _inherit = 'stock.move'

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        if self.production_id and self.production_id.bom_id.standard_cost_ids:
            standard_cost = self.production_id.bom_id.standard_cost_ids.filtered(lambda x:x.date <= date.today())
            if standard_cost and standard_cost.total_cost > 0.0:
                if self.production_id.product_id.cost_method == 'standard':
                    product_cost = self.production_id.product_id.standard_price
                    if product_cost > 0.0 :
                        self.production_id.product_id.write({'standard_price': (product_cost+standard_cost.total_cost)/2})
                    else :
                        self.production_id.product_id.write({'standard_price': standard_cost.total_cost}) 
        return super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)

StockMove()


class AccountMoveLine(models.Model):
    _inherit   = "account.move.line"

    @api.model
    def create(self, vals):
        if 'name' in vals and 'product_id' in vals :
            mo_exist = self.env['mrp.production'].sudo().search([('name','=',vals['name'])], limit=1)
            if mo_exist :
                if mo_exist.bom_id.standard_cost_ids :
                    exist_cost = mo_exist.bom_id.standard_cost_ids.filtered(lambda x:x.date <= date.today())
                    if exist_cost :
                        new_cost = exist_cost.sorted('date',reverse=True)[0].total_cost * vals['quantity']
                        if new_cost > 0.0 :
                            product_id = self.env['product.product'].browse(vals['product_id'])
                            if product_id.categ_id.name in ('Finish Good','FINISH GOOD','finish good') :
                                if vals['credit'] > 0.0 :
                                    vals['credit'] = new_cost
                                elif vals['debit'] > 0.0 :
                                    vals['debit'] = new_cost
                else :
                    raise ValidationError(_('BoM pada nomor MO %s tidak ada standard costing')%(mo_exist.name))

        moves = super(AccountMoveLine, self).create(vals)
        return moves

AccountMoveLine()