from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby


class VitStandardCost(models.Model):
    _name           = "vit.standard.cost"
    _inherit        = ["mail.thread"]
    _description    = 'Standard Cost Master'
    _rec_name       = 'bom_id'
    _order          = 'date desc'

    @api.multi
    @api.depends('bom_id', 'name')
    def name_get(self):
        result = []
        for res in self:
            name = res.bom_id.display_name
            if res.name :
                name = res.name+': '+name
            result.append((res.id, name))
        return result

    name = fields.Char('Reference', track_visibility='on_change')
    bom_id = fields.Many2one('mrp.bom','BoM',required=True, track_visibility='on_change')
    routing_id = fields.Many2one('mrp.routing','Routing',related='bom_id.routing_id', store=True)
    date = fields.Date('Start Date', required=True, track_visibility='on_change',default=fields.Date.context_today)
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    notes = fields.Text('Notes', track_visibility='on_change')
    detail_ids = fields.One2many('vit.standard.cost.details','standard_cost_id','Routings', ondelete='cascade')
    detail2_ids = fields.One2many('vit.standard.cost.details2','standard_cost_id','Other', ondelete='cascade')
    total_cost = fields.Float('Total Cost', compute="_compute_total_cost", store=True, track_visibility='on_change')

    _sql_constraints = [
        ('name', 'unique(bom_id,date,company_id)',
         'BoM and date must be unique per company'),
    ]

    @api.depends('detail_ids.cost','detail2_ids.cost')
    def _compute_total_cost(self):
        for i in self :
            i.total_cost = sum(i.detail_ids.mapped('cost'))+sum(i.detail2_ids.mapped('cost'))

    @api.onchange('bom_id')
    def onchange_bom_id(self):
        if self.bom_id.routing_id:
            self.detail_ids = False
            routing = []
            group_report = self.bom_id.routing_id.operation_ids.mapped('workcenter_id.group_report')
            if group_report :
                for op in list(dict.fromkeys(group_report)) :
                    if op :
                        routing.append([0,0,{'group_report': op}])
            self.detail_ids = routing
            if not self.detail2_ids :
                self.detail2_ids = [[0,0,{'name': 'Material'}],[0,0,{'name': 'Washer','group_report':'Final Quality'}],[0,0,{'name': 'Cost Washer','group_report':'Final Quality'}]]

VitStandardCost()


class VitStandardCostDetails(models.Model):
    _name           = "vit.standard.cost.details"
    _description    = 'Standard Cost Details Master'


    standard_cost_id = fields.Many2one('vit.standard.cost','Standard Cost')
    sequence = fields.Float('Sequence',default=10)
    #group_report = fields.Selection([('CF','CF'),('Furnished','Furnished'),('Final Quality','Final Quality'),('Heading','Heading'),('Machine','Machine'),('Packing','Packing'),('Plating','Plating'),('Rolling','Rolling'),('Three Bond','Three Bond'),('Washing','Washing')],"Group Workcenter",required=True)
    group_report = fields.Selection([('CF','CF'),                                   
                                ('Heading','Heading'),
                                ('Machine','Machining'),
                                ('Rolling','Rolling'),
                                ('Trimming','Trimming'),
                                ('Sloting','Sloting'),
                                ('Cutting','Cutting'),
                                ('Washing','Washing'),
                                ('Furnished','Furnace'),
                                ('Plating','Plating'),
                                ('Final Quality','Final Quality'),
                                ('Three Bond','Three Bond'),
                                ('Packing','Packing')],"Group")
    workcenter_id = fields.Many2one('mrp.workcenter','Workcenter',required=False)
    cost = fields.Float('Cost',default=0.0)

VitStandardCostDetails()


class VitStandardCostDetails2(models.Model):
    _name           = "vit.standard.cost.details2"
    _description    = 'Standard Cost Details Master 2'


    standard_cost_id = fields.Many2one('vit.standard.cost','Standard Cost')
    name = fields.Char('Name',required=True)
    group_report = fields.Selection([('CF','CF'),                                   
                            ('Heading','Heading'),
                            ('Machine','Machining'),
                            ('Rolling','Rolling'),
                            ('Trimming','Trimming'),
                            ('Sloting','Sloting'),
                            ('Cutting','Cutting'),
                            ('Washing','Washing'),
                            ('Furnished','Furnace'),
                            ('Plating','Plating'),
                            ('Final Quality','Final Quality'),
                            ('Three Bond','Three Bond'),
                            ('Packing','Packing')],"Group")
    cost = fields.Float('Cost',default=0.0)

VitStandardCostDetails2()