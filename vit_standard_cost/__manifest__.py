{
    "name"          : "Standard Cost",
    "version"       : "0.4",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "Account",
    "summary"       : "Standard Cost Nitto",
    "description"   : """
        
    """,
    "depends"       : [
        "account","mrp","vit_report_balance"
    ],
    "data"          : [
        "security/ir.model.access.csv",
        "wizard/standard_cost_wizard.xml",
        "views/standard_cost.xml",
        "views/mrp.xml",
    ],

    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}