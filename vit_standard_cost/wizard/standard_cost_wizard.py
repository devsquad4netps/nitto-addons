from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ReportStandardCostWizard(models.TransientModel):
    _name = "vit.report.standardcost.wizard"
    _description = "Report Standard Cost (WIP)"


    company_ids = fields.Many2many("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.ids)
    company_id = fields.Many2one("res.company",string="Company",required=True,default=lambda self: self.env.user.company_id.id)  
    notes = fields.Text("Notes") 
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')
    date_start = fields.Date( string="Date start", required=True, default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date( string="Date end", required=True, default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'center','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['yellow']})
        wbf['header_table'].set_border()

        wbf['content_string'] = workbook.add_format({'align': 'left'})
        wbf['content_string'].set_border()

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['yellow']})
        wbf['header_month_sum'].set_border()
        
        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
        
        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_border()
        wbf['content_number'].set_right() 

        wbf['content_number_sum'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['yellow']})
        wbf['content_number_sum'].set_border()
        wbf['content_number_sum'].set_right() 
        
        return wbf, workbook

    sql_wo = """select 
            mwo.product_id as product_id
            from
                mrp_workorder mwo
                left join product_product pp on pp.id = mwo.product_id 
                left join mrp_production mpo on mwo.production_id = mpo.id
                left join product_template pt on pp.product_tmpl_id = pt.id 
                left join product_category pc on pt.categ_id = pc.id 
                where mwo.state = 'done' and mwo.is_wip = true
                and pc.name ilike '%%Finish Good%%' 
                and mpo.company_id = %s
                and mwo.date_finished between %s and %s
                group by mwo.product_id
            """ 
    sql_heading = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Heading'
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_machine = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end  
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Machining' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_rolling = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Rolling' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_trimming= """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Trimming' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_sloting = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Sloting' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_cutting = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Cutting' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_washing = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Washing' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_furnace = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Furnished' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_plating = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Plating' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_fq = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Final Quality' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    sql_tb = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Three Bond' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null) 
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """
    
    sql_packing = """select case when sum(qty_produced_real) > 0 then sum(qty_produced_real) else sum(qty_produced) end 
                 from mrp_workorder wo
                 left join mrp_workcenter mw on wo.workcenter_id = mw.id
                 left join mrp_production mp on wo.production_id = mp.id
                 where wo.state = 'done' and wo.is_wip = true and mw.group_report = 'Packing' 
                 and (wo.is_subcontracting = false or wo.is_subcontracting is null)
                 and mp.state not in ('cancel','done')
                 and wo.product_id=%s and mp.company_id = %s and wo.date_finished between %s and %s 
                """

    @api.multi
    def action_print_report_wip_cost(self):
        self.ensure_one()
        for report in self :
            DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
            date1 = datetime.strptime(str(self.date_start)+" 00:00:00",DATETIME_FORMAT)
            date2 = datetime.strptime(str(self.date_end)+" 23:59:59",DATETIME_FORMAT)
            date_start = date1 + timedelta(hours=-7)
            date_end = date2 + timedelta(hours=-7)
            report_name = 'REPORT WIP COST %s to %s'%(report.date_start.strftime("%d-%b-%Y"),report.date_end.strftime("%d-%b-%Y"))
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + "+"
            company = company[:-1]
            #import pdb;pdb.set_trace()
            companys = str(tuple(report.company_ids.ids)).replace(",)",")")
            row = 1
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = report.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet('REPORT WIP COST')
            worksheet.set_column('A1:A1', 5)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 30)
            worksheet.set_column('D1:D1', 5)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 15)
            worksheet.set_column('N1:N1', 15)
            worksheet.set_column('O1:O1', 15)
            worksheet.set_column('P1:P1', 15)
            worksheet.set_column('Q1:Q1', 15)
            worksheet.set_column('R1:R1', 15)
            worksheet.set_column('S1:S1', 15)
            worksheet.set_column('T1:T1', 15)
            worksheet.set_column('U1:U1', 15)

            date_print = datetime.today() + timedelta(hours=+7)
            worksheet.write('A%s'%(row), str(date_print)[:19], wbf['title_doc'])
            worksheet.merge_range('A%s:U%s'%(row+1,row+1), 'REPORT WIP COST PT. NITTO ALAM INDONESIA (%s)'%(company), wbf['company'])
            worksheet.merge_range('A%s:U%s'%(row+2,row+2), report.date_start.strftime("%d %b %Y")+' - '+report.date_end.strftime("%d %b %Y"), wbf['company'])
            
            row +=4
            worksheet.write('A%s'%(row), 'NO', wbf['header_table'])
            worksheet.write('B%s'%(row), 'ITEM ID', wbf['header_table'])
            worksheet.write('C%s'%(row), 'ITEM NAME', wbf['header_table'])
            worksheet.write('D%s'%(row), 'CURR', wbf['header_table'])
            worksheet.write('E%s'%(row), 'WIRE', wbf['header_table'])
            worksheet.write('F%s'%(row), 'HEADING', wbf['header_table'])
            worksheet.write('G%s'%(row), 'MACHINING', wbf['header_table'])
            worksheet.write('H%s'%(row), 'ROLLING', wbf['header_table'])
            worksheet.write('I%s'%(row), 'TRIMMING', wbf['header_table'])
            worksheet.write('J%s'%(row), 'SLOTING', wbf['header_table'])
            worksheet.write('K%s'%(row), 'CUTTING', wbf['header_table'])
            worksheet.write('L%s'%(row), 'WASHING', wbf['header_table'])
            worksheet.write('M%s'%(row), 'FURNACE', wbf['header_table'])
            worksheet.write('N%s'%(row), 'PLATING', wbf['header_table'])
            worksheet.write('O%s'%(row), 'FINAL QUALITY', wbf['header_table'])
            worksheet.write('P%s'%(row), 'THREEBOND', wbf['header_table'])
            worksheet.write('Q%s'%(row), 'PACKING', wbf['header_table'])
            worksheet.write('R%s'%(row), 'WASHER', wbf['header_table'])
            worksheet.write('S%s'%(row), 'COST WASH', wbf['header_table'])
            worksheet.write('T%s'%(row), 'TOTAL', wbf['header_table'])
            # worksheet.write('U%s'%(row), 'COMPANY', wbf['header_table'])
            #import pdb;pdb.set_trace()
            cr = self.env.cr
            cr.execute(report.sql_wo, (report.company_id.id,date_start, date_end ))
            result = cr.dictfetchall()
            product = self.env['product.product']
            
            row +=1
            no=1
            for res in result:
                product_id = product.browse(res['product_id'])
                worksheet.write('A%s'%(row), no, wbf['content_number'])
                worksheet.write('B%s'%(row), product_id.default_code or '', wbf['content_string'])
                worksheet.write('C%s'%(row), product_id.name or '', wbf['content_string'])
                worksheet.write('D%s'%(row), 'IDR', wbf['content_string'])
                # heading dan wire/bb
                cr.execute(report.sql_heading, (res['product_id'],report.company_id.id, date_start, date_end ))
                heading = cr.dictfetchone()
                heading = heading['sum'] if heading and heading['sum'] != None else 0
                cost_heading = report._get_routings_cost(product_id, date_end, 'Heading')
                cost_material = report._get_material_cost(product_id, date_end, 'Material')
                worksheet.write('E%s'%(row), heading*cost_material, wbf['content_number'])
                worksheet.write('F%s'%(row), heading*cost_heading, wbf['content_number'])
                # machining
                cr.execute(report.sql_machine, (res['product_id'],report.company_id.id, date_start, date_end ))
                machining = cr.dictfetchone()
                machining = machining['sum'] if machining and machining['sum'] != None else 0
                cost_machining = report._get_routings_cost(product_id, date_end, 'Machining')
                worksheet.write('G%s'%(row), machining*cost_machining, wbf['content_number'])
                # rolling
                cr.execute(report.sql_rolling, (res['product_id'],report.company_id.id, date_start, date_end ))
                rolling = cr.dictfetchone()
                rolling = rolling['sum'] if rolling and rolling['sum'] != None else 0
                cost_rolling = report._get_routings_cost(product_id, date_end, 'Rolling')
                worksheet.write('H%s'%(row), rolling*cost_rolling, wbf['content_number'])
                # trimming
                cr.execute(report.sql_trimming, (res['product_id'],report.company_id.id, date_start, date_end ))
                trimming = cr.dictfetchone()
                trimming = trimming['sum'] if trimming and trimming['sum'] != None else 0
                cost_trimming = report._get_routings_cost(product_id, date_end, 'Trimming')
                worksheet.write('I%s'%(row), trimming*cost_trimming, wbf['content_number'])
                # sloting
                cr.execute(report.sql_sloting, (res['product_id'],report.company_id.id, date_start, date_end ))
                sloting = cr.dictfetchone()
                sloting = sloting['sum'] if sloting and sloting['sum'] != None else 0
                cost_sloting = report._get_routings_cost(product_id, date_end, 'Sloting')
                worksheet.write('J%s'%(row), sloting*cost_sloting, wbf['content_number'])
                # cutting
                cr.execute(report.sql_cutting, (res['product_id'],report.company_id.id, date_start, date_end ))
                cutting = cr.dictfetchone()
                cutting = cutting['sum'] if cutting and cutting['sum'] != None else 0
                cost_cutting = report._get_routings_cost(product_id, date_end, 'Cutting')
                worksheet.write('K%s'%(row), cutting*cost_cutting, wbf['content_number'])
                # washing
                cr.execute(report.sql_washing, (res['product_id'],report.company_id.id, date_start, date_end ))
                washing = cr.dictfetchone()
                washing = washing['sum'] if washing and washing['sum'] != None else 0
                cost_washing = report._get_routings_cost(product_id, date_end, 'Washing')
                worksheet.write('L%s'%(row), washing*cost_washing, wbf['content_number'])
                # furnace
                cr.execute(report.sql_furnace, (res['product_id'],report.company_id.id, date_start, date_end ))
                furnace = cr.dictfetchone()
                furnace = furnace['sum'] if furnace and furnace['sum'] != None else 0
                cost_furnace = report._get_routings_cost(product_id, date_end, 'Furnished')
                worksheet.write('M%s'%(row), furnace*cost_furnace, wbf['content_number'])
                 # plating
                cr.execute(report.sql_plating, (res['product_id'],report.company_id.id, date_start, date_end ))
                plating = cr.dictfetchone()
                plating = plating['sum'] if plating and plating['sum'] != None else 0
                cost_plating = report._get_routings_cost(product_id, date_end, 'Plating')
                worksheet.write('N%s'%(row), plating*cost_plating, wbf['content_number'])
                 # FQ, washer dan cost washer
                cr.execute(report.sql_fq, (res['product_id'],report.company_id.id, date_start, date_end ))
                fq = cr.dictfetchone()
                fq = fq['sum'] if fq and fq['sum'] != None else 0
                cost_fq = report._get_routings_cost(product_id, date_end, 'Final Quality')
                cost_material = report._get_material_cost(product_id, date_end, 'Washer')
                cost_material_man_power = report._get_material_cost(product_id, date_end, 'Cost Washer')
                worksheet.write('O%s'%(row), fq*cost_fq, wbf['content_number'])
                worksheet.write('R%s'%(row), fq*cost_material, wbf['content_number'])
                worksheet.write('S%s'%(row), fq*cost_material_man_power, wbf['content_number'])
                 # threebond
                cr.execute(report.sql_tb, (res['product_id'],report.company_id.id, date_start, date_end ))
                tb = cr.dictfetchone()
                tb = tb['sum'] if tb and tb['sum'] != None else 0
                cost_tb = report._get_routings_cost(product_id, date_end, 'Three Bond')
                worksheet.write('P%s'%(row), tb*cost_tb, wbf['content_number'])
                 # packing
                cr.execute(report.sql_packing, (res['product_id'],report.company_id.id, date_start, date_end ))
                packing = cr.dictfetchone()
                packing = packing['sum'] if packing and packing['sum'] != None else 0
                cost_packing = report._get_routings_cost(product_id, date_end, 'Packing')
                worksheet.write('Q%s'%(row), packing*cost_packing, wbf['content_number'])

                #Total 
                worksheet.write_formula('T%s'%(row), '=sum(E%s:S%s)' % (row, row), wbf['content_number'])

                row+=1
                no+=1
            worksheet.merge_range('A%s:D%s'%(row,row),'Total' , wbf['header_table'])
            worksheet.write_formula('E%s'%(row), '=sum(E%s:E%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('F%s'%(row), '=sum(F%s:F%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('G%s'%(row), '=sum(G%s:G%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('H%s'%(row), '=sum(H%s:H%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('I%s'%(row), '=sum(I%s:I%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('J%s'%(row), '=sum(J%s:J%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('K%s'%(row), '=sum(K%s:K%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('L%s'%(row), '=sum(L%s:L%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('M%s'%(row), '=sum(M%s:M%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('N%s'%(row), '=sum(N%s:N%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('O%s'%(row), '=sum(O%s:O%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('P%s'%(row), '=sum(P%s:P%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('Q%s'%(row), '=sum(Q%s:Q%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('R%s'%(row), '=sum(R%s:R%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('S%s'%(row), '=sum(S%s:S%s)' % (5, row-1), wbf['content_number_sum'])
            worksheet.write_formula('T%s'%(row), '=sum(T%s:T%s)' % (5, row-1), wbf['content_number_sum'])

            row += 1
            if report.notes :
                worksheet.merge_range('A%s:S%s'%(row+2,row+2), report.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            report.write({'file_data':result})
            filename = report_name+' ('+company+')'
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(report.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def _get_routings_cost(self, product_id, date_end, group_report):
        cost_master = self.env['vit.standard.cost']
        bom_cost = cost_master.search([('bom_id.product_tmpl_id','=',product_id.product_tmpl_id.id),('date','<=',date_end)],order='date desc', limit=1)
        cost = 0.0
        if bom_cost :
            cost_exist = bom_cost.detail_ids.filtered(lambda x:x.group_report == group_report)
            if cost_exist :
                cost = cost_exist[0].cost
        return cost

    def _get_material_cost(self, product_id, date_end, material):
        cost_master = self.env['vit.standard.cost']
        bom_cost = cost_master.search([('bom_id.product_tmpl_id','=',product_id.product_tmpl_id.id),('date','<=',date_end)],order='date desc', limit=1)
        cost = 0.0
        if bom_cost :
            cost_exist = bom_cost.detail2_ids.filtered(lambda x:x.name == material)
            if cost_exist :
                cost = cost_exist[0].cost
        return cost

ReportStandardCostWizard()