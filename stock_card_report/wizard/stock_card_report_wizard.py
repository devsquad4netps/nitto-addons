# Copyright 2019 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models
from odoo.tools.safe_eval import safe_eval
from odoo.tools import pycompat
from odoo.osv import expression

class Product(models.Model):
	_inherit = 'product.product'

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		if self._context.get('stock_card'):
			domain = []
			name_split = name.split()
			if name_split:
				domain = ['|','|',('barcode','ilike', name),('default_code','ilike', name)]
				for x in name_split:
					domain.append(('name','ilike', x))
			args = (args or []) + domain
			product_ids = self._search(domain+ (args or []), limit=limit, access_rights_uid=name_get_uid)
			return self.browse(product_ids).name_get()
		return super(Product, self)._name_search(name, args=args, operator=operator, limit=limit, name_get_uid=name_get_uid)

class StockCardReportWizard(models.TransientModel):
	_name = 'stock.card.report.wizard'
	_description = 'Stock Card Report Wizard'

	tipe_report = fields.Selection([('stock', 'Stock Card Report'),
		('stock_amount', 'Stock With Amount'),
		('rekap', 'Rekap Stock Card')], default='stock', string='Jenis Laporan')

	date_range_id = fields.Many2one(
		comodel_name='date.range',
		string='Periode',
	)
	date_from = fields.Date(
		string='Tanggal Awal',
	)
	date_to = fields.Date(
		string='Tanggal Akhir',
	)
	location_id = fields.Many2one(
		comodel_name='stock.location',
		string='Lokasi'
	)
	location_ids = fields.Many2many(comodel_name='stock.location', string='Lokasi')
	product_ids = fields.Many2many(
		comodel_name='product.product',
		string='Products'
	)
	categ_ids = fields.Many2many('product.category', string='Product Category')
	product_group_ids = fields.Many2many(comodel_name='product.template.group2', string='Item Groups')
	hide_product = fields.Boolean('Hide Product')
	product_key = fields.Char('Product Keyword')

	@api.onchange('categ_ids')
	def _onchange_product_group_ids(self):
		self.hide_product = False
		self.product_group_ids = False
		self.product_ids = False
		product = self.env['product.product']
		if self.categ_ids:
			product_ids = product.search([('categ_id', 'child_of', self.categ_ids.ids)])
			# product_ids = 
			product_group_ids = product_ids.mapped('group_id2')
			print(product_group_ids)
			self.product_group_ids = [(6,0,product_group_ids.ids)]
			self.product_ids = [(6,0,product_ids.ids)]
			self.hide_product = True
			# print(self.categ_ids.mapped('product_line'))

	# @api.onchange('product_group_ids')
	# def _onchange_product_ids(self):
	#     self.product_ids = False
	#     produk_grup = False
	#     product_obj = self.env['product.template']
	#     if self.product_group_ids:
	#         produk_grup = tuple(self.product_group_ids.ids)
	#         produk_grup_string = str(produk_grup).replace(',','')
	#         # print(produk_grup_string)
	#         product_tmpl_ids = product_obj.search([('group_id2', 'in', self.product_group_ids.ids)]).ids
	#         products = self.env['product.product'].search([('categ_id', 'in', self.categ_ids.ids)]).ids
	#         self.product_ids = [(6, 0, products)]

	@api.onchange('date_range_id')
	def _onchange_date_range_id(self):
		self.date_from = self.date_range_id.date_start
		self.date_to = self.date_range_id.date_end

	@api.multi
	def button_export_html(self):
		self.ensure_one()
		action = self.env.ref(
			'stock_card_report.action_report_stock_card_report_html')
		vals = action.read()[0]
		context = vals.get('context', {})
		if isinstance(context, pycompat.string_types):
			context = safe_eval(context)
		model = self.env['report.stock.card.report']
		report = model.create(self._prepare_stock_card_report())
		# print(report)
		
		context['active_id'] = report.id
		context['active_ids'] = report.ids
		vals['context'] = context
		return vals

	@api.multi
	def button_export_pdf(self):
		self.ensure_one()
		report_type = 'qweb-pdf'
		return self._export(report_type)

	@api.multi
	def button_export_xlsx(self):
		self.ensure_one()
		report_type = 'xlsx'
		return self._export(report_type)

	def _prepare_stock_card_report(self):
		self.ensure_one()
		group_ids = self.product_group_ids.ids
		product_ids = self.product_ids.ids
		if not self.product_group_ids:
			group_ids = self.product_ids.mapped('group_id2').ids
		vals = {
			'date_from': self.date_from,
			'date_to': self.date_to or fields.Date.context_today(self),
			'product_ids': [(6, 0, product_ids)],
			'product_group_ids': [(6, 0, group_ids)],
			'location_id': self.location_id.id,
			'tipe_report': self.tipe_report,
		}
		# print(vals)
		return vals

	def _export(self, report_type):
		model = self.env['report.stock.card.report']
		report = model.create(self._prepare_stock_card_report())
		return report.print_report(report_type)
