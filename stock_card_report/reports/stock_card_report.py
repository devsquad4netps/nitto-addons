# Copyright 2019 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models


class StockCardView(models.TransientModel):
	_name = 'stock.card.view'
	_description = 'Stock Card View'
	_order = 'date'

	date = fields.Datetime()
	product_id = fields.Many2one(comodel_name='product.product')
	group_id2 = fields.Many2one(comodel_name='product.template.group2')
	kanban_id = fields.Many2one(comodel_name='vit.kanban')
	account_move = fields.Char()
	product_qty = fields.Float()
	product_uom_qty = fields.Float()
	price_in = fields.Float()
	price_out = fields.Float()
	value_in = fields.Float()
	value_out = fields.Float()
	outstanding_so = fields.Float()
	adjustment = fields.Float()
	inventory_id = fields.Many2one(comodel_name='stock.inventory')
	product_uom = fields.Many2one(comodel_name='uom.uom')
	reference = fields.Char()
	location_id = fields.Many2one(comodel_name='stock.location')
	location_dest_id = fields.Many2one(comodel_name='stock.location')
	partner_id = fields.Many2one(comodel_name='res.partner')
	origin = fields.Char()
	is_initial = fields.Boolean()
	product_in = fields.Float()
	product_out = fields.Float()


class StockCardReport(models.TransientModel):
	_name = 'report.stock.card.report'
	_description = 'Stock Card Report'

	# Filters fields, used for data computation
	tipe_report = fields.Selection([('stock', 'Stock Card Report'),
		('stock_amount', 'Stock With Amount'),
		('rekap', 'Rekap Stock Card')], default='stock', string='Jenis Laporan')
	date_from = fields.Date()
	date_to = fields.Date()
	product_ids = fields.Many2many(
		comodel_name='product.product',
	)
	location_id = fields.Many2one(
		comodel_name='stock.location',
	)
	product_group_ids = fields.Many2many(comodel_name='product.template.group2')

	# Data fields, used to browse report data
	results = fields.Many2many(
		comodel_name='stock.card.view',
		compute='_compute_results',
		help='Use compute fields, so there is nothing store in database',
	)

	def _get_product_product(self):
		product_product_ids = []
		if self.product_group_ids:
			print('------- _get_product_product() --------')
			# print(self.product_group_ids)
			query = "SELECT p.id from product_product p left join product_template pt on pt.id = p.product_tmpl_id "
			if len(self.product_group_ids) == 1:
				where = "where pt.group_id2 = %s and p.active = True group by p.id" % tuple(self.product_group_ids.ids) 
			else:
				where = "where pt.group_id2 in {group_id2} and p.active = True group by p.id" .format(group_id2 = tuple(self.product_group_ids.ids))
			query += where
			# print(query)
			self._cr.execute(query)
			fetch = self._cr.dictfetchall()
			print(fetch)
			product_product_ids = [x['id'] for x in fetch]
		# print(product_product_ids)
		return product_product_ids


	@api.multi
	def _compute_results(self):
		self.ensure_one()
		date_from = self.date_from or '0001-01-01'
		self.date_to = self.date_to or fields.Date.context_today(self)
		locations = self.env['stock.location'].search(
			[('id', '=', [self.location_id.id])])
		products = self._get_product_product()
		# print(self.product_ids)
		# for location in locations:
		if self.tipe_report in ('stock','stock_amount'):
			self._cr.execute("""
				SELECT pt.group_id2, move.date, move.price_unit, move.product_id, move.product_qty, move.value,
					move.product_uom_qty, move.product_uom, move.reference,move.kanban_id, move.origin,
					move.location_id, move.location_dest_id,move.inventory_id, am.name as account_move,
					case when move.location_dest_id = %s
						then move.product_qty end as product_in,
					case when move.location_dest_id = %s
						then move.value end as value_in,
					case when move.location_dest_id = %s
						then move.price_unit end as price_in,
					case when move.location_id = %s
						then move.product_qty end as product_out,
					case when move.location_id = %s
						then move.value * -1 end as value_out,
					case when move.location_id = %s
						then move.price_unit * -1 end as price_out,
					case when move.date < %s then True else False end as is_initial,
					case when move.kanban_id is not null then kanban.partner_id else move.partner_id end as partner_id
				FROM stock_move move
				left join product_product p on p.id = move.product_id
				left join product_template pt on pt.id = p.product_tmpl_id
				left join account_move am on move.id = am.stock_move_id
				left join vit_kanban kanban on kanban.id = move.kanban_id
				WHERE (move.location_id = %s or move.location_dest_id = %s)
					and move.state = 'done' and move.product_id in %s
					and CAST(move.date AS date) <= %s and pt.group_id2 in %s
				group by pt.group_id2,move.date, move.product_id, move.product_qty, am.name,
					move.product_uom_qty, move.product_uom, move.reference, move.location_id, move.value, 
					move.location_dest_id, move.inventory_id, move.origin, move.kanban_id, move.price_unit, kanban.partner_id, move.partner_id
				ORDER BY move.date, move.reference
			""", (
				self.location_id.id, self.location_id.id,
				self.location_id.id, self.location_id.id,
				self.location_id.id, self.location_id.id, date_from,
				self.location_id.id, self.location_id.id,
				tuple(products), self.date_to, tuple(self.product_group_ids.ids)))
		else:
			self._cr.execute("""SELECT id as product_id, sum(product_in) product_in, sum(product_out) product_out, 
				(sum(adjustment_in) - sum(adjustment_out)) * -1 as adjustment, 
				is_initial, sum(outstanding_so) as outstanding_so from (
				select p.id, 
				case when move.location_id = %s and move.inventory_id is null
					then sum(product_qty) else 0 end as product_out,
				0 as product_in, 
				case when move.date < %s then True else False end as is_initial,
				case when move.location_id = %s and move.inventory_id is not null 
					then sum(product_qty) else 0 end as adjustment_in,
				0 as adjustment_out,
				(sum(sl.product_uom_qty) - sum(sl.qty_delivered)) as outstanding_so
				from stock_move move
				left join product_product p on (p.id = move.product_id)
				left join product_template pt on (pt.id = p.product_tmpl_id)
				left join sale_order_line sl on (sl.id = move.sale_line_id)
				where move.location_id = %s and move.state = 'done' and move.product_id in %s
				and CAST(move.date AS date) <= %s and pt.group_id2 in %s
				group by p.id, move.location_id, move.inventory_id, is_initial
				union all
				select p.id, 0 as product_out, 
				case when move.location_dest_id = %s and move.inventory_id is null
					then sum(product_qty) else 0 end as product_in,
				case when move.date < %s then True else False end as is_initial, 
				case when move.location_dest_id = %s and move.inventory_id is not null 
					then sum(product_qty) * -1 else 0 end as adjustment_out,
				0 as adjutment_in,
				(sum(sl.product_uom_qty) - sum(sl.qty_delivered)) as outstanding_so
				from stock_move move
				left join product_product p on (p.id = move.product_id)
				left join product_template pt on (pt.id = p.product_tmpl_id)
				left join sale_order_line sl on (sl.id = move.sale_line_id)
				where move.location_dest_id = %s and move.state = 'done' and move.product_id in %s
				and CAST(move.date AS date) <= %s and pt.group_id2 in %s
				group by p.id, move.location_dest_id, move.inventory_id, is_initial) alias
				group by id, is_initial
			""",(self.location_id.id, date_from, self.location_id.id, self.location_id.id, tuple(products), self.date_to, tuple(self.product_group_ids.ids),
				self.location_id.id, date_from, self.location_id.id, self.location_id.id, tuple(products), self.date_to, tuple(self.product_group_ids.ids)))
			# print('STOCK CARD REPORT')
		stock_card_results = self._cr.dictfetchall()
		print(stock_card_results)
		ReportLine = self.env['stock.card.view']
		self.results = [ReportLine.new(line).id for line in stock_card_results]
		# print(self.results)

	@api.multi
	def _get_initial(self, product_line):
		product_input_qty = sum(product_line.mapped('product_in'))
		product_output_qty = sum(product_line.mapped('product_out'))
		return product_input_qty - product_output_qty

	@api.multi
	def print_report(self, report_type='qweb'):
		self.ensure_one()
		action = report_type == 'xlsx' and self.env.ref(
			'stock_card_report.action_stock_card_report_xlsx') or \
			self.env.ref('stock_card_report.action_stock_card_report_pdf')
		return action.report_action(self, config=False)

	def _get_html(self):
		result = {}
		rcontext = {}
		report = self.browse(self._context.get('active_id'))
		if report:
			rcontext['o'] = report
			result['html'] = self.env.ref(
				'stock_card_report.report_stock_card_report_html').render(
					rcontext)
		return result

	@api.model
	def get_html(self, given_context=None):
		return self.with_context(given_context)._get_html()
