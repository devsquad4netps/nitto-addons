# Copyright 2019 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import logging
from odoo import models

_logger = logging.getLogger(__name__)


class ReportStockCardReportXlsx(models.AbstractModel):
	_name = 'report.stock_card_report.report_stock_card_report_xlsx'
	_inherit = 'report.report_xlsx.abstract'

	def generate_xlsx_report(self, workbook, data, objects):
		self._define_formats(workbook)
		for group_ids in objects.results.mapped('product_id').mapped('group_id2'):
			# for product in objects.product_ids:
			for ws_params in self._get_ws_params(workbook, data, objects, group_ids):
				ws_name = ws_params.get('ws_name')
				ws_name = self._check_ws_name(ws_name)
				ws = workbook.add_worksheet(ws_name)
				# for product in objects.product_ids:
				product_ids = objects.product_ids.filtered(lambda p:p.product_tmpl_id.group_id2 == group_ids)
				generate_ws_method = getattr(
				self, ws_params['generate_ws_method'])
				generate_ws_method(
					workbook, ws, ws_params, data, objects, product_ids)

	def _get_stock_card_template(self, objects):
		stock_card_template = {
			'1_date': {
				'header': {
					'value': 'Date',
				},
				'data': {
					'value': self._render('date'),
					'format': self.format_tcell_date_center,
				},
				'width': 15,
			},
			'2_reference': {
				'header': {
					'value': 'Reference',
				},
				'data': {
					'value': self._render('reference'),
					'format': self.format_tcell_left,
				},
				'width': 30,
			},
			'3_input': {
				'header': {
					'value': 'Stock In',
				},
				'data': {
					'value': self._render('input'),
				},
				'width': 15,
			},
			'4_output': {
				'header': {
					'value': 'Stock Out',
				},
				'data': {
					'value': self._render('output'),
				},
				'width': 15,
			},
			'5_balance': {
				'header': {
					'value': 'Adjustment',
				},
				'data': {
					'value': self._render('adjustment'),
				},
				'width': 15,
			},
			'6_balance': {
				'header': {
					'value': 'Saldo',
				},
				'data': {
					'value': self._render('balance'),
				},
				'width': 15,
			},
			'7_partner': {
				'header': {
					'value': 'Partner',
				},
				'data': {
					'value': self._render('partner'),
					'format': self.format_tcell_left,
				},
				'width': 25,
			},
			'8_origin': {
				'header': {
					'value': 'Description',
				},
				'data': {
					'value': self._render('origin'),
				},
				'width': 25,
			},
		}
		if objects.tipe_report == 'stock_amount':
			stock_card_template = {
				'01_date': {
					'header': {
						'value': 'Date',
					},
					'data': {
						'value': self._render('date'),
						'format': self.format_tcell_date_center,
					},
					'width': 15,
				},
				'02_reference': {
					'header': {
						'value': 'Reference',
					},
					'data': {
						'value': self._render('reference'),
						'format': self.format_tcell_left,
					},
					'width': 30,
				},
				'03_input': {
					'header': {
						'value': 'Stock In',
					},
					'data': {
						'value': self._render('input'),
					},
					'width': 15,
				},
				'04_input_price': {
					'header': {
						'value': 'Price In',
					},
					'data': {
						'value': self._render('price_in'),
					},
					'width': 15,
				},
				'05_input_amount': {
					'header': {
						'value': 'Cost In',
					},
					'data': {
						'value': self._render('input_amount'),
					},
					'width': 15,
				},
				'06_output': {
					'header': {
						'value': 'Stock Out',
					},
					'data': {
						'value': self._render('output'),
					},
					'width': 15,
				},
				'07_output_price': {
					'header': {
						'value': 'Price Out',
					},
					'data': {
						'value': self._render('price_out'),
					},
					'width': 15,
				},
				'08_output_amount': {
					'header': {
						'value': 'Cost Out',
					},
					'data': {
						'value': self._render('output_amount'),
					},
					'width': 15,
				},
				'09_adjustment': {
					'header': {
						'value': 'Adjustment',
					},
					'data': {
						'value': self._render('adjustment'),
					},
					'width': 15,
				},
				'10_adjustment_price': {
					'header': {
						'value': 'Price Adj',
					},
					'data': {
						'value': self._render('adjustment_price'),
					},
					'width': 15,
				},
				'11_adjustment_amount': {
					'header': {
						'value': 'Amount Adj.',
					},
					'data': {
						'value': self._render('adjustment_amount'),
					},
					'width': 15,
				},
				'10_balance': {
					'header': {
						'value': 'Saldo',
					},
					'data': {
						'value': self._render('balance'),
					},
					'width': 15,
				},
				'11_partner': {
					'header': {
						'value': 'Partner',
					},
					'data': {
						'value': self._render('partner'),
						'format': self.format_tcell_left,
					},
					'width': 25,
				},
				'12_origin': {
					'header': {
						'value': 'Description',
					},
					'data': {
						'value': self._render('origin'),
					},
					'width': 25,
				},
			}
		elif objects.tipe_report == 'rekap':
			stock_card_template = {
				'01_item_number': {
					'header': {
						'value': 'Item Number',
					},
					'data': {
						'value': self._render('default_code'),
						'format': self.format_tcell_left,
					},
					'width': 15,
				},
				'02_item_name': {
					'header': {
						'value': 'Item Name',
					},
					'data': {
						'value': self._render('item_name'),
						'format': self.format_tcell_left,
					},
					'width': 30,
				},
				'03_begining_balance': {
					'header': {
						'value': 'Begin Bal.',
					},
					'data': {
						'value': self._render('begining_balance'),
					},
					'width': 15,
				},
				'04_stock_in': {
					'header': {
						'value': 'Stock In',
					},
					'data': {
						'value': self._render('stock_in'),
					},
					'width': 15,
				},
				'05_stock_out': {
					'header': {
						'value': 'Stock Out',
					},
					'data': {
						'value': self._render('stock_out'),
					},
					'width': 15,
				},
				'06_adjustment': {
					'header': {
						'value': 'Adjustment',
					},
					'data': {
						'value': self._render('adjustment'),
					},
					'width': 15,
				},
				'07_ending_balance': {
					'header': {
						'value': 'End Bal.',
					},
					'data': {
						'value': self._render('ending_balance'),
					},
					'width': 15,
				},
				'08_outstanding_so': {
					'header': {
						'value': 'Outstanding SO',
					},
					'data': {
						'value': self._render('outstanding_so'),
					},
					'width': 15,
				},
				'09_available_qty': {
					'header': {
						'value': 'Available Qty',
					},
					'data': {
						'value': self._render('available_qty'),
					},
					'width': 25,
				},
			}

		return stock_card_template



	def _get_ws_params(self, wb, data, objects, group):
		filter_template = {
			'1_date_from_judul': {
				'data': {
					'value': 'Date',
					'format': self.format_tcell_left_bold,
				},
			},
			'2_date_from': {
				'data': {
					'value': self._render('date_from'),
					'format': self.format_tcell_date_center,
				},
			},
			'3_to': {
				'data': {
					'value': 'To',
					'format': self.format_tcell_center,
				},
			},
			'4_date_to': {
				'data': {
					'value': self._render('date_to'),
					'format': self.format_tcell_date_center,
				},
			},
		}
		initial_template = {
			'1_ref': {
				'data': {
					'value': 'Begining Balance',
					'format': self.format_tcell_left_bold,
				}
			},
			'2_balance': {
				'data': {
					'value': self._render('balance'),
					'format': self.format_integer_conditional_right,
				},
			},
		}

		product_filter_template = {
			'1_judul': {
				'data': {
					'value': 'Item Name',
					'format': self.format_tcell_left_bold,
				}
			},
			'2_product_name': {
				'data': {
					'value': self._render('product_name'),
					'format': self.format_tcell_left,
				}
			}
		}
		location_template = {
			'1_judul': {
				'data': {
					'value': 'Location',
					'format': self.format_tcell_left_bold,
				}
			},
			'2_location': {
				'data': {
					'value': self._render('location'),
					'format': self.format_tcell_left,
				}
			}
		}

		title = 'Stock Card - {}'.format(group.name)
		if objects.tipe_report == 'stock_amount':
			title = 'Stock Card With Amount - {}'.format(group.name)
		elif objects.tipe_report == 'rekap':
			title = 'Rekap Stock Card - {}'.format(group.name)

		
		stock_card_template = self._get_stock_card_template(objects)

		ws_params = {
			'ws_name': group.name,
			'generate_ws_method': '_stock_card_report',
			'title': title,
			'wanted_list_filter': [k for k in sorted(filter_template.keys())],
			'col_specs_filter': filter_template,
			'wanted_list_initial': [k for k in sorted(initial_template.keys())],
			'col_specs_initial': initial_template,
			'wanted_list': [k for k in sorted(stock_card_template.keys())],
			'col_specs': stock_card_template,
			'product_filter' : [k for k in sorted(product_filter_template.keys())],
			'col_specs_product' : product_filter_template,
			'location_filter' : [k for k in sorted(location_template.keys())],
			'colspecs_location' : location_template,
		}
		return [ws_params]

	def _stock_card_report(self, wb, ws, ws_params, data, objects, product_ids):
		ws.set_portrait()
		ws.fit_to_pages(1, 0)
		ws.set_header(self.xls_headers['standard'])
		ws.set_footer(self.xls_footers['standard'])
		self._set_column_width(ws, ws_params)
		total_balance = 0
		total_stock_in = 0
		total_stock_out = 0
		total_adjustment = 0
		total_ending_balance = 0
		total_outstanding_so = 0
		total_qty_available = 0
		# Title
		row_pos = 0
		row_pos = self._write_ws_title(ws, row_pos, ws_params, True)
		# Filter Table
		row_pos = self._write_line(
			ws, row_pos, ws_params, col_specs_section='data',
			render_space={
				'date_from': objects.date_from or '~',
				'date_to': objects.date_to or '',
			},
			col_specs='col_specs_filter', wanted_list='wanted_list_filter')
		row_pos = self._write_line(
				ws, row_pos, ws_params, col_specs_section='data',
				render_space={'location': objects.location_id.name_get()[0][1]}, col_specs='colspecs_location',
				wanted_list='location_filter')
		row_pos += 1

		# Tampilkan Stock Card Template Header jika tipe report adalah Rekap Stock Card
		if objects.tipe_report == 'rekap':
			row_pos = self._write_line(
					ws, row_pos, ws_params, col_specs_section='header',
					default_format=self.format_theader_blue_center)
		# Stock Card Table
		ws.freeze_panes(row_pos, 0)
		# for group_id in objects.product_group_ids:
		product_list = objects.results.mapped('product_id').ids
		for product in product_ids:
			# if product.product_tmpl_id.group_id2 == group_id:
			if objects.tipe_report in ('stock','stock_amount') and product.id in product_list:
				print(product_list, ' ===== ', product)
				row_pos += 1
				row_pos = self._write_line(
					ws, row_pos, ws_params, col_specs_section='data',
					render_space={'product_name': product.name}, col_specs='col_specs_product',
					wanted_list='product_filter')

				balance = objects._get_initial(objects.results.filtered(
					lambda l: l.product_id == product and l.is_initial))
				row_pos = self._write_line(
					ws, row_pos, ws_params, col_specs_section='data',
					render_space={'balance': balance}, col_specs='col_specs_initial',
					wanted_list='wanted_list_initial')

				row_pos = self._write_line(
					ws, row_pos, ws_params, col_specs_section='header',
					default_format=self.format_theader_blue_center)

				product_lines = objects.results.filtered(
					lambda l: l.product_id == product and not l.is_initial)
				price = 0.0
				total_ending_balance = 0.0
				for line in product_lines:
					balance += line.product_in - line.product_out
					qty_adjustment = 0
					adjustment_amount = 0.0
					adjustment_price = 0.0
					if line.inventory_id:
						qty_adjustment = line.product_in if line.product_in > 0 else line.product_out * -1
						adjustment_price = line.price_in if line.price_in > 0 else line.price_out * -1
						adjustment_amount = line.value_in if line.value_in > 0 else line.value_out * -1

					if objects.tipe_report == 'stock':
						row_pos = self._write_line(
							ws, row_pos, ws_params, col_specs_section='data',
							render_space={
								'date': line.date or '',
								'reference': line.reference or '',
								'input': line.product_in if not line.inventory_id else 0,
								'output': line.product_out if not line.inventory_id else 0,
								'adjustment' : qty_adjustment,
								'balance': balance,
								'partner': line.partner_id.name or '-',
								'origin': line.account_move or '-',
							},
							default_format=self.format_integer_conditional_right)
					elif objects.tipe_report == 'stock_amount':
						price = line.price_in if line.price_in > 0 else line.price_out
						row_pos = self._write_line(
							ws, row_pos, ws_params, col_specs_section='data',
							render_space={
								'date': line.date or '',
								'reference': line.reference or '',
								'input': line.product_in if not line.inventory_id else 0,
								'price_in' : line.price_in if not line.inventory_id else 0,
								'input_amount': line.value_in if not line.inventory_id else 0,
								'output': line.product_out if not line.inventory_id else 0,
								'price_out': line.price_out if not line.inventory_id else 0,
								'output_amount': line.value_out if not line.inventory_id else 0,
								'adjustment' : qty_adjustment,
								'adjustment_amount' : adjustment_amount,
								'adjustment_price' : adjustment_price,
								'balance': balance,
								'partner': line.partner_id.name or '-',
								'origin': line.account_move or '-',
							},
							default_format=self.format_integer_conditional_right)

				if objects.tipe_report == 'stock_amount':
					row_pos = self._write_line(
							ws, row_pos, ws_params, col_specs_section='data',
							render_space={
								'date': '',
								'reference': 'Ending Balance',
								'input': balance,
								'price_in' : price,
								'input_amount': balance * price,
								'output': '',
								'price_out': '',
								'output_amount': '',
								'adjustment' : '',
								'adjustment_amount' : '',
								'adjustment_price' : '',
								'balance': '',
								'partner': '',
								'origin': '',
							},
							default_format=self.format_integer_conditional_right)

				row_pos += 2
			elif objects.tipe_report == 'rekap' and product.id in product_list:
				balance = objects._get_initial(objects.results.filtered(
					lambda l: l.product_id == product and l.is_initial))
				product_lines = objects.results.filtered(
					lambda l: l.product_id == product and not l.is_initial)
				for line in product_lines:
					ending_balance = balance + line.product_in - line.product_out + line.adjustment
					outstanding_so = line.outstanding_so or 0
					available_qty = ending_balance - outstanding_so
					row_pos = self._write_line(
							ws, row_pos, ws_params, col_specs_section='data',
							render_space={
								'default_code': line.product_id.default_code or '-',
								'item_name': line.product_id.name or '-',
								'begining_balance': balance,
								'stock_in': line.product_in or 0,
								'stock_out' : line.product_out or 0,
								'ending_balance': ending_balance,
								'adjustment': line.adjustment or 0,
								'outstanding_so': outstanding_so, # Sementara Dikosongkan, karena belum jelas
								'available_qty': ending_balance - outstanding_so,
							},
							default_format=self.format_integer_conditional_right)
					total_balance += balance
					total_stock_in += line.product_in
					total_stock_out += line.product_out
					total_adjustment += line.adjustment
					total_ending_balance += ending_balance
					total_outstanding_so += outstanding_so
					total_qty_available += available_qty

		if objects.tipe_report == 'rekap':
			row_pos = self._write_line(
				ws, row_pos, ws_params, col_specs_section='data',
				render_space={
					'default_code': '',
					'item_name': '',
					'begining_balance': total_balance,
					'stock_in': total_stock_in,
					'stock_out' : total_stock_out,
					'ending_balance': total_ending_balance,
					'adjustment': total_adjustment,
					'outstanding_so': total_outstanding_so, # Sementara Dikosongkan, karena belum jelas
					'available_qty': total_qty_available,
				},
				default_format=self.format_integer_conditional_right)




