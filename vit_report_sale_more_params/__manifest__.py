{
    'name': "Report Sales - More Parameters",
    'version': '0.1',
    'author': '',
    'depends': ['vit_report_sale'],
    'data': [
        "wizard/report_sale.xml",
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}
