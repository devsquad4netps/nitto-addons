from odoo import api, fields, models
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import Warning
from odoo.exceptions import UserError, Warning,ValidationError
import logging
logger = logging.getLogger(__name__)


class ReportSaleWizard(models.TransientModel):
    _inherit = "vit.report.sale2.wizard"

    date_start = fields.Date(string="Order Date", required=True,
                             default=lambda self: fields.Date.to_string(date.today().replace(day=1)))

    customer_group_id = fields.Many2one("res.partner.group", string="Customer Group")
    salesman_id = fields.Many2one("res.users", string="Salesman")
    delivery_date_start = fields.Date(string="Delivery Date")
    delivery_date_end = fields.Date(string="Delivery Date End")
    delivery_remaining = fields.Boolean(string="Delivery Remaining > 0", default=False)
    product_category_ids = fields.Many2many("product.category", string="Product Categories")
    product_id = fields.Many2one("product.template", string="Product")
    order_number = fields.Char(string="Order Number")

    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

    def _format_date_range(self, date_start, date_end):
        date1 = datetime.strptime(str(date_start) + " 00:00:00", self.DATETIME_FORMAT)
        date2 = datetime.strptime(str(date_end) + " 23:59:59", self.DATETIME_FORMAT)
        return date1, date2

    @api.multi
    def action_print_report(self):
        self.ensure_one()
        for report in self:
            order_date_range = self._format_date_range(self.date_start, self.date_end)
            date_start = order_date_range[0] + timedelta(hours=-7)
            date_end = order_date_range[1] + timedelta(hours=-7)
            i_type = dict(self._fields['type'].selection).get(report.type)
            report_name = 'Laporan Sales %s (%s)'%(report.date_end,i_type)
            company = ""
            for comp in report.company_ids:
                c_name = comp.name
                if comp.second_name :
                    c_name = comp.second_name
                company += c_name + " + "
            company = company[:-3]
            companies = str(tuple(report.company_ids.ids)).replace(",)",")")
            if self.partner_ids :
                # partners = str(tuple(report.partner_ids.ids)).replace(",)",")")
                partners = self.partner_ids
            else :
                partners = self.env['res.partner'].search([('customer','=',True)])
                # partners = str(tuple(partner_ids.ids)).replace(",)",")")
            if i_type == 'By customer and by item' :
                report_name = 'Laporan Sales  %s (%s)'%(report.date_end,'Cust+Item') # rename karena keterbatasan karakter di sheet excel
                #raise Warning('Report Sales (%s) masih onprogress...' % (i_type))
                return report.action_print_report_by_customer_by_item(i_type, report_name, company, companies, partners, date_start, date_end)
            elif i_type == 'Balance SO' :
                return report.action_print_report_balance_so(i_type, report_name, company, companies, partners, date_start, date_end)
            else :
                return report.action_print_report_sales_after_ca(i_type, report_name, company, companies, partners, date_start, date_end)

    def action_print_report_by_customer_by_item(self, i_type, report_name, company, companies, partners, date_start, date_end):
        for i in self:
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name + ' (' + i_type + ')')
            worksheet.set_column('A1:A1', 4)
            worksheet.set_column('B1:B1', 10)
            worksheet.set_column('C1:C1', 25)
            worksheet.set_column('D1:D1', 25)
            worksheet.set_column('E1:E1', 25)
            worksheet.set_column('F1:F1', 18)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 25)
            worksheet.set_column('M1:M1', 10)

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)' % company, wbf['company'])
            worksheet.merge_range('A2:J2', 'Laporan Sales By Customer By Product Periode ' + str(date_start) + ' to ' + str(date_end), wbf['company'])
            worksheet.merge_range('H3:J3', 'Printed on : ' + str(datetime.now() - timedelta(hours=-7))[:19] + ' (' + self.env.user.name + ')', wbf['header_table'])
            row = 4

            order_line = self.env['sale.order.line']
            for partner in partners:
                sql = '''
                    select
                        sol.id as id
                    from
                        sale_order_line sol
                    inner join stock_move sm on
                        sol.id = sm.sale_line_id
                    left join product_product pp on 
                        sol.product_id = pp.id
                    left join product_template pt on
                        pp.product_tmpl_id = pt.id
                    left join product_category pc on
                        pt.categ_id = pc.id
                    left join sale_order so on
                        sol.order_id = so.id
                    left join stock_picking sp on 
                        sm.picking_id = sp.id
                    left join res_partner rp on
                        so.partner_id = rp.id
                    left join res_users ru on
                        so.user_id = ru.id
                    left join res_partner rp2 on
                        ru.partner_id = rp2.id
                    where so.state in ('sale', 'done')
                    and sol.order_partner_id = %s
                    and (so.date_order between %s and %s)
                '''

                params = [partner.id, str(date_start), str(date_end)]

                if self.delivery_remaining:
                    sql += " and sol.product_uom_qty > sol.qty_delivered"

                if self.customer_group_id:
                    sql += " and rp.group_partner_id = %s"
                    params.append(self.customer_group_id.id)

                if self.salesman_id:
                    sql += " and sol.salesman_id = %s"
                    params.append(self.salesman_id.id)

                if self.delivery_date_start and self.delivery_date_end:
                    delivery_date_range = self._format_date_range(self.delivery_date_start, self.delivery_date_end)
                    delivery_date_start = delivery_date_range[0] + timedelta(hours=-7)
                    delivery_date_end = delivery_date_range[1] + timedelta(hours=-7)

                    sql += " and (sp.date_done between %s and %s)"
                    params.append(str(delivery_date_start))
                    params.append(str(delivery_date_end))
                elif self.delivery_date_start and (not self.delivery_date_end):
                    sql += " and (sp.date_done >= %s)"
                    delivery_date_start = datetime.strptime(str(self.delivery_date_start) + " 00:00:00",
                                                            self.DATETIME_FORMAT)
                    params.append(str(delivery_date_start))
                elif self.delivery_date_end and (not self.delivery_date_start):
                    sql += " and (sp.date_done <= %s)"  # Effective date
                    delivery_date_end = datetime.strptime(str(self.delivery_date_end) + " 00:00:00",
                                                          self.DATETIME_FORMAT)
                    params.append(str(delivery_date_end))

                if self.product_id:
                    sql += " and pt.id = %s"
                    params.append(self.product_id.id)

                if self.order_number:
                    sql += " and so.name ilike %s"
                    params.append('%' + self.order_number + '%')

                sql += " and so.company_id in " + companies

                if self.product_category_ids:
                    sql += " and pc.id in " + str(tuple(self.product_category_ids.ids)).replace(",)", ")")

                sql += " order by sol.delivery_date asc, sol.order_partner_id"

                cr.execute(sql, tuple(params))

                datas = cr.fetchall()
                if not datas:
                    continue
                worksheet.merge_range('A%s:C%s' % (row, row), partner.name, wbf['header_table'])
                row += 1
                worksheet.write('A%s' % (row), 'No', wbf['header_month'])
                worksheet.write('B%s' % (row), 'Date', wbf['header_month'])
                worksheet.write('C%s' % (row), 'Order Number', wbf['header_month'])
                worksheet.write('D%s' % (row), 'Picking Slip', wbf['header_month'])
                worksheet.write('E%s' % (row), 'Item Name', wbf['header_month'])
                worksheet.write('F%s' % (row), 'Item Code Ext', wbf['header_month'])
                worksheet.write('G%s' % (row), 'Quantity', wbf['header_month'])
                worksheet.write('H%s' % (row), 'Unit Price', wbf['header_month'])
                worksheet.write('I%s' % (row), 'Amount IDR', wbf['header_month'])
                worksheet.write('J%s' % (row), 'Amount Valas', wbf['header_month'])
                worksheet.write('K%s' % (row), 'Currency', wbf['header_month'])
                worksheet.write('L%s' % (row), 'P/O Customer', wbf['header_month'])
                worksheet.write('M%s' % (row), 'Status', wbf['header_month'])
                row += 1
                no = 1
                t_qty = 0.0
                t_amount = 0.0
                t_valas = 0.0
                for dt in datas:
                    data = order_line.browse(dt)
                    origin_price = data.price_subtotal
                    valas_price = data.price_subtotal
                    currency = data.order_id.currency_id
                    company_currency = data.order_id.company_id.currency_id
                    if currency.name != 'IDR':
                        valas_price = currency._convert(data.price_subtotal, company_currency,
                                                        data.order_id.company_id, data.order_id.date_order)
                    worksheet.write('A%s' % (row), no, wbf['title_doc'])
                    worksheet.write('B%s' % (row), str(data.order_id.date_order), wbf['title_doc'])
                    worksheet.write('C%s' % (row), data.order_id.name, wbf['title_doc'])
                    worksheet.write('D%s' % (row), data.order_id.origin, wbf['title_doc'])
                    worksheet.write('E%s' % (row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('F%s' % (row), data.product_customer_code_id.display_name, wbf['title_doc'])
                    worksheet.write('G%s' % (row), data.product_uom_qty, wbf['content_number'])
                    worksheet.write('H%s' % (row), data.price_unit, wbf['content_number'])
                    worksheet.write('I%s' % (row), origin_price, wbf['content_number'])
                    worksheet.write('J%s' % (row), valas_price, wbf['content_number'])
                    worksheet.write('K%s' % (row), data.order_id.currency_id.name, wbf['title_doc'])
                    worksheet.write('L%s' % (row), data.order_id.client_order_ref, wbf['title_doc'])
                    worksheet.write('M%s' % (row), data.order_id.state, wbf['title_doc'])
                    t_qty += data.product_uom_qty
                    t_amount += origin_price
                    t_valas += valas_price
                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('G%s' % (row), t_qty, wbf['content_number_sum'])
                worksheet.write('I%s' % (row), t_amount, wbf['content_number_sum'])
                worksheet.write('J%s' % (row), t_valas, wbf['content_number_sum'])
                row += 1

            if i.notes:
                worksheet.merge_range('A%s:M%s' % (row + 2, row + 2), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data': result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model=" + self._name + "&id=" + str(i.id) + "&field=file_data&download=true&filename=" + filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def action_print_report_balance_so(self, i_type, report_name, company, companies, partners, date_start, date_end):
        for i in self:
            if not self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report') and i.based == 'Amount':
                raise Warning('Anda tidak diizinkan print dengan type amount')
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name + ' (' + i_type + ')')
            worksheet.set_column('A1:A1', 5)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 30)
            worksheet.set_column('D1:D1', 15)
            worksheet.set_column('E1:E1', 15)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 15)
            worksheet.set_column('N1:N1', 15)

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)' % company, wbf['company'])
            worksheet.merge_range('A2:J2', report_name, wbf['company'])
            if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report'):
                worksheet.merge_range('J3:L3', 'Printed on : ' + str(datetime.now() - timedelta(hours=-7))[:19] + ' (' + self.env.user.name + ')', wbf['header_table'])
            else:
                worksheet.merge_range('I3:K3', 'Printed on : ' + str(datetime.now() - timedelta(hours=-7))[:19] + ' (' + self.env.user.name + ')', wbf['header_table'])
            row = 4
            order_line = self.env['sale.order.line']
            grand_t_qty = 0.0
            grand_t_amount = 0.0
            grand_t_delivered = 0.0
            grand_t_selisih = 0.0
            grand_t_outstanding = 0.0
            for partner in partners:
                sql = '''
                    select
                        sol.id
                    from
                        sale_order_line sol
                    left join sale_order so on
                        so.id = sol.order_id
                    left join res_partner rp on
                        sol.order_partner_id = rp.id
                    left join product_product pp on
                        sol.product_id = pp.id
                    left join product_template pt on
                        pp.product_tmpl_id = pt.id
                    left join product_category pc on
                        pt.categ_id = pc.id
                    where
                        so.state in ('sale', 'done')
                        and sol.order_partner_id = %s
                        and (so.date_order between %s and %s)
                '''

                params = [partner.id, str(date_start), str(date_end)]

                if self.delivery_remaining:
                    sql += " and sol.product_uom_qty > sol.qty_delivered"

                if self.customer_group_id:
                    sql += " and rp.group_partner_id = %s"
                    params.append(self.customer_group_id.id)

                if self.salesman_id:
                    sql += " and sol.salesman_id = %s"
                    params.append(self.salesman_id.id)

                if self.delivery_date_start and self.delivery_date_end:
                    delivery_date_range = self._format_date_range(self.delivery_date_start, self.delivery_date_end)
                    delivery_date_start = delivery_date_range[0] + timedelta(hours=-7)
                    delivery_date_end = delivery_date_range[1] + timedelta(hours=-7)

                    sql += " and (sol.delivery_date between %s and %s)"
                    params.append(str(delivery_date_start))
                    params.append(str(delivery_date_end))
                elif self.delivery_date_start and (not self.delivery_date_end):
                    sql += " and (sol.delivery_date >= %s)"
                    delivery_date_start = datetime.strptime(str(self.delivery_date_start) + " 00:00:00",
                                                            self.DATETIME_FORMAT)
                    params.append(str(delivery_date_start))
                elif self.delivery_date_end and (not self.delivery_date_start):
                    sql += " and (sol.delivery_date <= %s)"
                    delivery_date_end = datetime.strptime(str(self.delivery_date_end) + " 00:00:00",
                                                            self.DATETIME_FORMAT)
                    params.append(str(delivery_date_end))

                if self.product_id:
                    sql += " and pt.id = %s"
                    params.append(self.product_id.id)

                if self.order_number:
                    sql += " and so.name ilike %s"
                    params.append('%' + self.order_number + '%')

                sql += " and so.company_id in " + companies

                if self.product_category_ids:
                    sql += " and pc.id in " + str(tuple(self.product_category_ids.ids)).replace(",)", ")")

                sql += " order by sol.delivery_date asc, sol.order_partner_id"

                cr.execute(sql, tuple(params))
                datas = cr.fetchall()
                if not datas:
                    continue

                worksheet.merge_range('A%s:C%s' % (row, row), partner.name, wbf['header_table'])
                worksheet.merge_range('D%s:E%s' % (row, row), partner.user_id.name or '', wbf['header_table'])
                worksheet.merge_range('F%s:G%s' % (row, row), partner.alias_name or '', wbf['header_table'])
                row += 1
                worksheet.write('A%s' % (row), 'No', wbf['header_month'])
                worksheet.write('B%s' % (row), 'Item Code', wbf['header_month'])
                worksheet.write('C%s' % (row), 'Item Name', wbf['header_month'])
                worksheet.write('D%s' % (row), 'Customer Code', wbf['header_month'])
                worksheet.write('E%s' % (row), 'P/O Customer', wbf['header_month'])
                worksheet.write('F%s' % (row), 'Order Date', wbf['header_month'])
                worksheet.write('G%s' % (row), 'Order Qty', wbf['header_month'])
                if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report'):
                    # worksheet.write('H%s'%(row), 'Delivered Qty', wbf['header_month'])
                    # worksheet.write('I%s'%(row), 'Outstanding Qty', wbf['header_month'])
                    worksheet.write('H%s' % (row), 'Unit Price', wbf['header_month'])
                    worksheet.write('I%s' % (row), 'Total Amount', wbf['header_month'])
                    worksheet.write('J%s' % (row), 'Outsanding Amount', wbf['header_month'])
                    worksheet.write('K%s' % (row), 'Delivery Date', wbf['header_month'])
                    worksheet.write('L%s' % (row), 'On Hand', wbf['header_month'])
                else:
                    worksheet.write('H%s' % (row), 'Delivered Qty', wbf['header_month'])
                    worksheet.write('I%s' % (row), 'Outstanding Qty', wbf['header_month'])
                    worksheet.write('J%s' % (row), 'Delivery Date', wbf['header_month'])
                    worksheet.write('K%s' % (row), 'On Hand', wbf['header_month'])
                row += 1
                no = 1
                t_qty = 0.0
                t_amount = 0.0
                t_delivered = 0.0
                t_selisih = 0.0
                t_outstanding = 0.0

                for dt in datas:
                    data = order_line.browse(dt)
                    code = ''
                    origin_amount = data.price_subtotal
                    price_unit = data.price_unit
                    outstanding_amount = origin_amount - (data.qty_delivered * price_unit)
                    selisih = data.product_uom_qty - data.qty_delivered
                    currency = data.order_id.currency_id
                    company_currency = data.order_id.company_id.currency_id
                    if currency.name != 'IDR':
                        origin_amount = currency._convert(data.price_subtotal, company_currency, data.order_id.company_id,
                                                          data.delivery_date if data.delivery_date else data.order_id.validity_date)
                        price_unit = currency._convert(price_unit, company_currency, data.order_id.company_id,
                                                       data.delivery_date if data.delivery_date else data.order_id.validity_date)
                        outstanding_amount = origin_amount - (data.qty_delivered * price_unit)
                    date_order = data.date_order + timedelta(hours=7)

                    worksheet.write('A%s' % (row), no, wbf['title_doc'])
                    worksheet.write('B%s' % (row), data.product_id.default_code, wbf['title_doc'])
                    worksheet.write('C%s' % (row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('D%s' % (row), data.product_customer_code_id.product_code or '', wbf['title_doc'])
                    worksheet.write('E%s' % (row), data.order_id.client_order_ref or '', wbf['title_doc'])
                    worksheet.write('F%s' % (row), str(date_order), wbf['title_doc'])
                    worksheet.write('G%s' % (row), data.product_uom_qty, wbf['content_number'])
                    if self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report'):
                        # worksheet.write('H%s'%(row), data.qty_delivered, wbf['content_number'])
                        # worksheet.write('I%s'%(row), selisih, wbf['content_number'])
                        worksheet.write('H%s' % (row), price_unit, wbf['content_number'])
                        worksheet.write('I%s' % (row), origin_amount, wbf['content_number'])
                        worksheet.write('J%s' % (row), outstanding_amount, wbf['content_number'])
                        worksheet.write('K%s' % (row), str(data.delivery_date) if data.delivery_date else '',
                                        wbf['title_doc'])
                        worksheet.write('L%s' % (row), data.product_id.qty_available, wbf['content_number'])
                    else:
                        worksheet.write('H%s' % (row), data.qty_delivered, wbf['content_number'])
                        worksheet.write('I%s' % (row), selisih, wbf['content_number'])
                        worksheet.write('J%s' % (row), str(data.delivery_date) if data.delivery_date else '',
                                        wbf['title_doc'])
                        worksheet.write('K%s' % (row), data.product_id.qty_available, wbf['content_number'])
                    t_qty += data.product_uom_qty
                    t_amount += origin_amount
                    t_delivered += data.qty_delivered
                    t_selisih += selisih
                    t_outstanding += outstanding_amount

                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('G%s' % (row), t_qty, wbf['content_number_sum'])
                if not self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report'):
                    worksheet.write('H%s' % (row), t_delivered, wbf['content_number_sum'])
                    worksheet.write('I%s' % (row), t_selisih, wbf['content_number_sum'])
                else:
                    worksheet.write('I%s' % (row), t_amount, wbf['content_number_sum'])
                    worksheet.write('J%s' % (row), t_outstanding, wbf['content_number_sum'])
                # sum grand total
                grand_t_qty += t_qty
                grand_t_amount += t_amount
                grand_t_delivered += t_delivered
                grand_t_selisih += t_selisih
                grand_t_outstanding += t_outstanding
                row += 1

            worksheet.merge_range('A%s:C%s' % (row + 1, row + 1), 'Grand Total', wbf['header_table'])
            worksheet.write('G%s' % (row + 1), grand_t_qty, wbf['content_number_sum'])
            if not self.env.user.has_group('vit_stock_card_pro.group_stock_amount_report'):
                worksheet.write('H%s' % (row + 1), grand_t_delivered, wbf['content_number_sum'])
                worksheet.write('I%s' % (row + 1), grand_t_selisih, wbf['content_number_sum'])
            else:
                worksheet.write('I%s' % (row + 1), grand_t_amount, wbf['content_number_sum'])
                worksheet.write('J%s' % (row + 1), grand_t_outstanding, wbf['content_number_sum'])

            if i.notes:
                worksheet.merge_range('A%s:M%s' % (row + 3, row + 3), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data': result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model=" + self._name + "&id=" + str(
                i.id) + "&field=file_data&download=true&filename=" + filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def action_print_report_sales_after_ca(self, i_type, report_name, company, companies, partners, date_start, date_end):
        for i in self:
            cr = self.env.cr
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(report_name + ' (' + i_type + ')')
            worksheet.set_column('A1:A1', 4)
            worksheet.set_column('B1:B1', 15)
            worksheet.set_column('C1:C1', 35)
            worksheet.set_column('D1:D1', 25)
            worksheet.set_column('E1:E1', 25)
            worksheet.set_column('F1:F1', 15)
            worksheet.set_column('G1:G1', 15)
            worksheet.set_column('H1:H1', 15)
            worksheet.set_column('I1:I1', 15)
            worksheet.set_column('J1:J1', 15)

            worksheet.merge_range('A1:J1', 'PT. NITTO ALAM INDONESIA (%s)' % company, wbf['company'])
            worksheet.merge_range('A2:J2', report_name + ' ' + str(date_start) + ' to ' + str(date_end), wbf['company'])
            worksheet.merge_range('H3:J3', 'Printed on : ' + str(datetime.now() - timedelta(hours=-7))[:19] + ' (' + self.env.user.name + ')', wbf['header_table'])
            row = 4

            invoice = self.env['account.invoice']
            for partner in partners:
                sql = '''
                    select
                        ai.id as id
                    from
                        account_invoice_line ail
                    inner join sale_order_line_invoice_rel sorel on
                        ail.id = sorel.invoice_line_id
                    inner join sale_order_line sol on
                        sorel.order_line_id = sol.id
                    inner join sale_order so on
                        sol.order_id = so.id
                    inner join account_invoice ai on
                        ail.invoice_id = ai.id
                    inner join res_partner rp on
                        so.partner_id = rp.id
                    inner join stock_move sm on
                        sorel.order_line_id = sm.sale_line_id
                    inner join stock_picking sp on
                        sm.picking_id = sp.id
                    left join product_product pp on 
                        sol.product_id = pp.id
                    left join product_template pt on
                        pp.product_tmpl_id = pt.id
                    left join product_category pc on
                        pt.categ_id = pc.id
                    where
                        so.partner_id = %s
                        and (so.date_order between %s and %s)
                '''
                params = [partner.id, str(date_start), str(date_end)]

                if self.delivery_remaining:
                    sql += " and sol.product_uom_qty > sol.qty_delivered"

                if self.salesman_id:
                    sql += " and sol.salesman_id = %s"
                    params.append(self.salesman_id.id)

                if self.delivery_date_start and self.delivery_date_end:
                    delivery_date_range = self._format_date_range(self.delivery_date_start, self.delivery_date_end)
                    delivery_date_start = delivery_date_range[0] + timedelta(hours=-7)
                    delivery_date_end = delivery_date_range[1] + timedelta(hours=-7)

                    sql += " and (sp.date_done between %s and %s)"
                    params.append(str(delivery_date_start))
                    params.append(str(delivery_date_end))
                elif self.delivery_date_start and (not self.delivery_date_end):
                    sql += " and (sp.date_done >= %s)"
                    delivery_date_start = datetime.strptime(str(self.delivery_date_start) + " 00:00:00", self.DATETIME_FORMAT)
                    params.append(str(delivery_date_start))
                elif self.delivery_date_end and (not self.delivery_date_start):
                    sql += " and (sp.date_done <= %s)"
                    delivery_date_end = datetime.strptime(str(self.delivery_date_end) + " 00:00:00", self.DATETIME_FORMAT)
                    params.append(str(delivery_date_end))

                if self.product_id:
                    sql += " and pt.id = %s"
                    params.append(self.product_id.id)

                if self.order_number:
                    sql += " and so.name ilike %s"
                    params.append('%' + self.order_number + '%')

                sql += " and so.company_id in " + companies

                if self.product_category_ids:
                    sql += " and pc.id in " + str(tuple(self.product_category_ids.ids)).replace(",)", ")")

                sql += " group by ai.id"

                cr.execute(sql, tuple(params))
                datas = cr.fetchall()
                if not datas:
                    continue
                worksheet.merge_range('A%s:C%s' % (row, row), partner.name, wbf['header_table'])
                row += 1
                worksheet.write('A%s' % (row), 'No', wbf['header_month'])
                worksheet.write('B%s' % (row), 'Customer Code', wbf['header_month'])
                worksheet.write('C%s' % (row), 'Customer Name', wbf['header_month'])
                worksheet.write('D%s' % (row), 'Date', wbf['header_month'])
                worksheet.write('E%s' % (row), 'Invoice Number', wbf['header_month'])
                worksheet.write('F%s' % (row), 'Invoice Qty', wbf['header_month'])
                worksheet.write('G%s' % (row), 'Amount IDR', wbf['header_month'])
                worksheet.write('H%s' % (row), 'Amount Valas', wbf['header_month'])
                worksheet.write('I%s' % (row), 'P/O Number', wbf['header_month'])
                worksheet.write('J%s' % (row), 'Status', wbf['header_month'])
                row += 1
                no = 1
                t_qty = 0.0
                t_amount = 0.0
                for dt in datas:
                    data = invoice.browse(dt)
                    origin_price = data.amount_total
                    valas_price = data.amount_total
                    quantity = sum(data.invoice_line_ids.mapped('quantity'))
                    currency = data.currency_id
                    company_currency = data.company_id.currency_id
                    if currency.name != 'IDR':
                        valas_price = currency._convert(origin_price, company_currency, data.company_id, data.date_invoice)

                    worksheet.write('A%s' % (row), no, wbf['title_doc'])
                    worksheet.write('B%s' % (row), data.partner_id.ref, wbf['title_doc'])
                    worksheet.write('C%s' % (row), data.partner_id.name, wbf['title_doc'])
                    worksheet.write('D%s' % (row), str(data.date_invoice) if data.date_invoice else '', wbf['title_doc'])
                    worksheet.write('E%s' % (row), data.number, wbf['title_doc'])
                    worksheet.write('F%s' % (row), quantity, wbf['content_number'])
                    worksheet.write('G%s' % (row), origin_price, wbf['content_number'])
                    worksheet.write('H%s' % (row), valas_price, wbf['content_number'])
                    worksheet.write('I%s' % (row), data.po_customer if data.po_customer else '', wbf['title_doc'])
                    worksheet.write('J%s' % (row), data.state, wbf['title_doc'])

                    t_qty += quantity
                    t_amount += origin_price
                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('F%s' % (row), t_qty, wbf['content_number_sum'])
                worksheet.write('G%s' % (row), t_amount, wbf['content_number_sum'])
                row += 1

            if i.notes:
                worksheet.merge_range('A%s:J%s' % (row + 2, row + 2), i.notes, wbf['title_doc'])
            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data': result})
            filename = report_name
            filename = filename + '%2Exlsx'
            url = "web/content/?model=" + self._name + "&id=" + str(i.id) + "&field=file_data&download=true&filename=" + filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }
