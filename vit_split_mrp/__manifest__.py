# -*- coding: utf-8 -*-
{
    'name': "Split MO",

    'summary': """
        Split MO when production was running
    """,

    'description': """
        Long description of module's purpose
    """,

    'author': "vITraining",
    'website': "https://vitraining.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Manufacturing',
    'version': '2.4',

    # any module necessary for this one to work correctly
    'depends': ['base','mrp','vit_kartu_prod_add',"vit_turbo_mrp","forecast","vit_subcontractor","vit_mrp_wo","vit_workorder","vit_report_balance"],

    # always loaded
    'data': [
        'wizard/vit_split_mo_wizard.xml',
        'data/data.xml',
        'views/mrp_production_view.xml',
        # 'views/templates.xml',
        
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}
