# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    # @api.depends('date_planned_start')
    # def _get_planned_date(self):
    #     for mrp in self:
    #         import pdb;pdb.set_trace()
    #         mrp.date_planned = str(mrp.date_planned_start)[:10]
            
    # date_planned = fields.Date(
    #     'Date Start', copy=False, store=True, compute='_get_planned_date')
    no_poly_box = fields.Char(string="No Poly Box",copy=False, size=6)
    origin_mo_id = fields.Many2one('mrp.production','Origin Split',copy=False)
    

    def action_split_wizard(self):
        action = self.env.ref('vit_split_mrp.action_split_mo_wizard').read()[0]
        return action

    @api.multi
    def action_workorder_split(self):
        if self.origin_mo_id :
            old_wo_state = self.origin_mo_id.workorder_ids.filtered(lambda x:x.state == 'done')
            if old_wo_state :
                for work_order in self.workorder_ids.filtered(lambda i:i.workcenter_id.id in old_wo_state.mapped('workcenter_id').ids):
                    try:
                        #self.env.cr.execute("SELECT vit_done_wo(%s,%s,%s)" % (work_order.id, work_order.production_id.id, self.env.uid) )
                        self.env.cr.execute("UPDATE mrp_workorder SET state='done', qty_produced=%d, qty_producing=%d, qty_produced_real=%d WHERE id = %d"%(self.product_qty,0.0,self.product_qty, work_order.id))
                        self.env.cr.commit()
                    except:
                        pass
                    
                    if work_order.user_id:
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET user_id=%d WHERE id = %d"%( self.origin_mo_id.user_id.id ,work_order.id))
                            self.env.cr.commit()
                        except:
                            pass
                    if work_order.barcodewo :
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET barcodewo='%s' WHERE id = %d"%(self.origin_mo_id.barcodewo,work_order.id))
                            self.env.cr.commit()
                        except:
                            pass
                    if work_order.machine_id :
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET machine_id=%d WHERE id = %d"%(self.origin_mo_id.machine_id.id,work_order.id))
                            self.env.cr.commit()
                        except:
                            pass
                    if work_order.final_lot_id :
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET final_lot_id=%d WHERE id = %d"%(self.origin_mo_id.final_lot_id.id,work_order.id))
                            self.env.cr.commit()
                        except:
                            pass
                    if work_order.is_subcontracting:
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET is_subcontracting=true WHERE id = %d"%(work_order.id))
                            self.env.cr.commit()
                        except:
                            pass
                    if work_order.date_start:
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET date_start='%s' WHERE id = %d"%(str(self.origin_mo_id.date_start),work_order.id))
                            self.env.cr.commit()
                        except:
                            pass
                    if work_order.date_finished:
                        try:
                            self.env.cr.execute("UPDATE mrp_workorder SET date_finished='%s' WHERE id = %d"%(str(self.origin_mo_id.date_finished),work_order.id))
                            self.env.cr.commit()
                        except:
                            pass

    @api.multi
    def action_cancel(self):
        """ Cancels production order, unfinished stock moves and set procurement
        orders in exception """
        # if any(workorder.state == 'progress' for workorder in self.mapped('workorder_ids')):
        #     raise UserError(_('You can not cancel production order, a work order is still in progress.'))
        work_progress = self.mapped('workorder_ids').filtered(lambda wo: wo.state == 'progress')
        if work_progress :
            work_progress.record_production()

        documents = {}
        for production in self:
            for move_raw_id in production.move_raw_ids.filtered(lambda m: m.state not in ('done', 'cancel')):
                iterate_key = self._get_document_iterate_key(move_raw_id)
                if iterate_key:
                    document = self.env['stock.picking']._log_activity_get_documents({move_raw_id: (move_raw_id.product_uom_qty, 0)}, iterate_key, 'UP')
                    for key, value in document.items():
                        if documents.get(key):
                            documents[key] += [value]
                        else:
                            documents[key] = [value]
            
            # production.workorder_ids.filtered(lambda x: x.state != 'cancel').action_cancel()
            workorder_ids = production.workorder_ids.filtered(lambda x: x.state not in ('cancel','done'))
            if workorder_ids :
                self._cr.execute("UPDATE mrp_workorder SET state='cancel' WHERE id IN %s"%str(tuple(workorder_ids.ids)).replace(',)',')'))

            finish_moves = production.move_finished_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
            raw_moves = production.move_raw_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
            (finish_moves | raw_moves)._action_cancel()
            picking_ids = production.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
            picking_ids.action_cancel()
        self.write({'state': 'cancel', 'is_locked': True})
        if documents:
            filtered_documents = {}
            for (parent, responsible), rendering_context in documents.items():
                if not parent or parent._name == 'stock.picking' and parent.state == 'cancel' or parent == self:
                    continue
                filtered_documents[(parent, responsible)] = rendering_context
            self._log_manufacture_exception(filtered_documents, cancel=True)
        return True

    @api.multi
    def post_inventory(self):
        for order in self :
            moves_not_to_do = order.move_raw_ids.filtered(lambda x: x.state == 'done')
            moves_to_do = order.move_raw_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
            for move in moves_to_do.filtered(lambda m: m.product_qty == 0.0 and m.quantity_done > 0):
                move.product_uom_qty = move.quantity_done
            moves_to_do._action_done()
            