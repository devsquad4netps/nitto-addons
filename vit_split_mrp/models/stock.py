# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)

class StockMove(models.Model):
    _inherit = 'stock.move'

    has_split_mrp = fields.Boolean(string="Hasil SPilt MRP",copy=False, default=False)


    @api.model
    def update_stok_move_from_split_mrp(self):
        mv_exist = self.search([('has_split_mrp','=',True),('state','not in',['cancel','draft','done'])])
        for mv in mv_exist:
            try :
                mv._action_done()
                _logger.info("Stock move %s updated state to ======> %s "%(mv.name, mv.state))
            except:
                pass
        return True

    def _quantity_done_set(self):
        quantity_done = self[0].quantity_done  # any call to create will invalidate `move.quantity_done`
        for move in self:
            move_lines = move._get_move_lines()
            if not move_lines:
                if quantity_done:
                    # do not impact reservation here
                    move_line = self.env['stock.move.line'].create(dict(move._prepare_move_line_vals(), qty_done=quantity_done))
                    move.write({'move_line_ids': [(4, move_line.id)]})
            elif len(move_lines) == 1:
                move_lines[0].qty_done = quantity_done

StockMove()