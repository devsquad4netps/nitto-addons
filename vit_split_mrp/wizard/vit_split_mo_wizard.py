from odoo import models, fields, api, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)


class VitSplitMoWizard(models.TransientModel):
    _name = "vit.split.mo.wizard"
    _description = "Split Manufacture Wizard"

    @api.multi
    @api.depends('lines','production_id.product_qty')
    def _get_qty(self):
        for wizard in self :
            product_count = len(wizard.lines) + 1
            wizard.product_count = product_count
            wizard.remaining_qty = wizard.remaining_qty2 = wizard.production_id.product_qty - sum(line.qty for line in wizard.lines)

    name = fields.Char('Name')
    production_id = fields.Many2one('mrp.production', string='Manufacture', default=lambda self: self._context['active_id'])
    lines = fields.One2many('vit.split.mo.line.wizard', 'wizard_id', string='Lines')
    product_count = fields.Integer(string='Total Product', compute='_get_qty', store=True, digit=dp.get_precision('Product Unit of Measure'))
    remaining_qty = fields.Float(string='Remaining Qty Ori', compute='_get_qty', store=True, digit=dp.get_precision('Product Unit of Measure'))
    remaining_qty2 = fields.Float(string='Remaining Qty', compute='_get_qty', store=True, digit=dp.get_precision('Product Unit of Measure'))



    @api.multi
    def action_split(self):
        for wizard in self :
            uid = self.env.uid
            if wizard.remaining_qty < 0 :
                raise Warning("Total qty cannot bigger than production qty.")
            production_id = wizard.production_id
            name = production_id.name
            x = 1
            remaining_qty = wizard.remaining_qty
            return_qty = production_id.product_qty - remaining_qty

            # catat WO yg lama yg statusnya sdh ready
            wo_ready = production_id.workorder_ids.filtered(lambda redi:redi.state == 'ready')

            # cek nomor di sistem
            for wiz in wizard.lines :
                if wiz.manual_number :
                    nomor_exist = self.env['mrp.production'].sudo().search([('name','=',wiz.manual_number.upper())],limit=1)
                    if nomor_exist :
                        raise Warning("Manufacturing order number %s sudah ada di sistem" % wiz.manual_number)
            if remaining_qty :
                production_id.write({
                    'product_qty': remaining_qty,
                })
                for wo in production_id.workorder_ids:
                    if wo.state not in ('done','cancel') :
                        wo.write({'qty_producing' : remaining_qty})
                    else:
                        sql_wo = "update mrp_workorder set qty_produced=%d, qty_produced_real=%d where id=%s" % (remaining_qty,remaining_qty, wo.id)
                        self.env.cr.execute(sql_wo)
                _logger.info("Loading...... 1: edit remaining_qty")
            picking_id = False
            new_move_id = False
            lot_reversed = {}
            for raw in production_id.move_raw_ids :
                new_qty = raw.product_uom_qty
                bom_line_id = self.env['mrp.bom.line'].search([
                        ('bom_id','=',production_id.bom_id.id),
                        ('product_id','=',raw.product_id.id),
                    ], limit=1)
                if not bom_line_id :
                    product_alternative = production_id.bom_id.bom_line_ids.filtered(lambda x:x.alternative_product_ids)
                    if product_alternative :
                        bom_line_id = product_alternative[0]
                if not bom_line_id :
                    raise Warning(_("Product %s is not exist in bom %s"%(raw.product_id.name_get()[0][1],production_id.bom_id.name_get()[0][1])))
                if remaining_qty > 0.0 :
                    new_qty = bom_line_id.product_qty/bom_line_id.bom_id.product_qty*return_qty
                if raw.state == 'done' :
                    if not raw.reference :
                        raise Warning("Picking belum tercreate, klik tombol 'Create Picking' di pojok kanan atas form MO ini ")
                    if raw.picking_id and not picking_id :
                        pick_exist = self.env['stock.picking'].sudo().search([('name','=','RE-'+raw.picking_id.name)])
                        for pick in pick_exist:
                            self.env.cr.execute("UPDATE stock_picking SET name='%s'  WHERE id = %d"%(raw.picking_id.name+' (FAIL SPLIT)',pick.id))
                            # delete qty account move line
                            for move_pick in pick.move_ids_without_package.filtered(lambda i:i.move_line_ids and i.state not in ('cancel','done')):
                                move_pick.move_line_ids.unlink()
                            if pick.state not in ('done','cancel'):
                                pick.action_cancel()
                        try :
                            picking_id = raw.picking_id.copy({
                                'location_id': raw.location_dest_id.id,
                                'location_dest_id': raw.location_id.id,
                                'name': 'RE-'+raw.picking_id.name,
                            })
                            _logger.info("Loading...... 2: copy picking raw materials")
                        except :
                            raise Warning(_("Dokumen %s sudah ada."%('RE-'+raw.picking_id.name)))
                        if picking_id.move_ids_without_package :
                            picking_id.move_ids_without_package.unlink()
                            _logger.info("Loading...... 3: hapus move picking raw material yang awal")
                        picking_id = picking_id.id
                    
                    vals = {
                        'product_uom_qty': new_qty,
                        'state': 'draft',
                        'date_expected': fields.Datetime.now(),
                        'location_id': raw.location_dest_id.id,
                        'location_dest_id': raw.location_id.id,
                        'origin_returned_move_id': raw.id,
                        'procure_method': 'make_to_stock',
                        'origin': raw.origin + ' (return)' if raw.origin else '',
                        'reference': raw.reference + ' (return)' if raw.reference else '',
                        'picking_id': picking_id,
                        'has_split_mrp':True,
                    }
                    new_move_id = raw.copy(vals)
                    _logger.info("Loading...... 4: Copy rawmaterials awal")
                    new_move_id._action_confirm()
                    _logger.info("Loading...... 5: Confirm (Mark as to do) rawmaterials awal")
                    new_move_id._action_assign()
                    _logger.info("Loading...... 6: Assign (Check Avaibility) rawmaterials awal")

                old_consume_state = raw.picking_id.state
                # kurangi initial demand raw materials awal
                if raw.picking_id.state == 'assigned':
                    raw.picking_id.do_unreserve()
                raw.write({'product_uom_qty':raw.product_uom_qty-new_qty, 'quantity_done':raw.product_uom_qty-new_qty})
                if old_consume_state == 'assigned':
                    try:
                        raw.picking_id.action_assign()
                    except:
                        pass
                
                lot_qty = {}
                
                if new_move_id and new_move_id.product_id not in lot_reversed :
                    lot_reversed[new_move_id.product_id] = {}

                for line in raw.move_line_ids :
                    quantity = line.qty_done if line.qty_done > 0.0 else 0.0
                    if quantity <= 0 and line.product_uom_qty > 0.0:
                        quantity = line.product_uom_qty
                    lot_qty[line.lot_id] = lot_qty.get(line.lot_id, 0) + quantity

                if new_move_id and new_move_id.move_line_ids :                 
                    new_move_id.move_line_ids.unlink()
                
                if new_move_id :
                    new_move_qty = round(new_move_id.product_uom_qty,0)
                    # while new_move_qty > 0 and lot_qty:
                    if new_move_qty > 0 :
                        is_break = False
                        for lot, qty in lot_qty.items():
                            if qty > 0.0 :
                                if qty < new_move_qty :
                                    new_move_line_qty = qty
                                    new_move_qty -= qty
                                elif qty >= new_move_qty :
                                    new_move_line_qty = new_move_qty
                                    new_move_qty = 0
                                    is_break = True
                                new_move_id.move_line_ids.create({
                                    'move_id': new_move_id.id,
                                    'product_id': new_move_id.product_id.id,
                                    'lot_id': lot.id,
                                    'location_id': new_move_id.location_id.id,
                                    'location_dest_id': new_move_id.location_dest_id.id,
                                    'product_uom_id': new_move_id.product_uom.id,
                                    'product_uom_qty': new_move_line_qty,
                                    'qty_done': new_move_line_qty if new_move_line_qty > 0.0 else 0.0,
                                })
                                _logger.info("Loading...... 7: Create Move Lines")
                                lot_reversed[new_move_id.product_id][lot] = lot_reversed[new_move_id.product_id].get(lot, 0) + new_move_line_qty
                                if is_break :
                                    break
                if raw.picking_id.state == 'done' and new_move_id:
                    try :
                        new_move_id.write({'quantity_done':new_move_id.product_uom_qty})
                        self.env.cr.execute("UPDATE stock_move SET has_split_mrp=true  WHERE id = %d"%(new_move_id.id))
                        try :
                            new_move_id._action_done()
                        except:
                            pass
                        _logger.info("Loading...... 8: Done move id (rawmaterials awal) baru")
                    except :
                        pass
                        #raise Warning("Tidak bisa retur product %s dari lokasi %s."%(new_move_id.product_id.name_get()[0][1],new_move_id.location_id.name_get()[0][1]))

            finish_move_id = self.env['stock.move'].search([('production_id','=',production_id.id)], limit=1)
            if finish_move_id :
                if remaining_qty > 0.0 :
                    finish_move_id.write({'product_uom_qty':remaining_qty})
                    finish_move_id._action_assign()
                else :
                    _logger.info("Loading...... 9: Cancel move barang jadi")
                    try :
                        finish_move_id._action_cancel()
                    except:
                        pass
            for line in wizard.lines :
                new_name = '%s-%d'%(name,x)
                if line.manual_number:
                    new_name = line.manual_number
                if self.env['mrp.production'].search([('name','=',new_name)]):
                    raise Warning(_("Tidak bisa split MO lebih dari satu kali (%s)."%(new_name)))
                    # if line.manual_number :
                    #     raise Warning(_("Tidak bisa split MO lebih dari satu kali (%s)."%(new_name)))
                    # else :
                    #     continue

                self.env.cr.execute("select create_mo_from_forecast(%s, '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', '%s', %d, %d, %d, %d, '%s', '%s')" % ('NULL', new_name, 
                    production_id.company_id.id, line.product_id.id, line.product_id.uom_id.id, line.qty, line.bom_id.id, line.routing_id.id, production_id.picking_type_id.warehouse_id.id,
                    production_id.location_src_id.id,production_id.location_dest_id.id,production_id.code_cust_id.id, line.no_poly_box, production_id.name, uid, production_id.customer_id.id, 
                    production_id.picking_type_id.id, 1, production_id.date_planned_start, production_id.date_planned_finished) )

                # new_production_id = production_id.copy({
                #     'name': new_name,
                #     'product_id': line.product_id.id,
                #     'bom_id': line.bom_id.id,
                #     'product_qty': line.qty,
                #     'no_poly_box': line.no_poly_box,
                # })
                # _logger.info("Loading...... 10: Create MO baru")
                
                # move_orig_ids = self.env['stock.move']
                # for raw in new_production_id.move_raw_ids :
                #     if raw.move_orig_ids :
                #         move_orig_ids += raw.move_orig_ids
                #         raw.write({
                #             'procure_method': 'make_to_stock',
                #             'move_orig_ids':[(5, 0, 0)],
                #         })
                #         _logger.info("Loading...... 11: Write procure_method Rawmate MO baru")
                
                # move_orig_ids._action_cancel()
                # new_production_id.action_assign()
                # new_production_id.action_toggle_is_locked()
                # for raw in new_production_id.move_raw_ids :
                #     if raw.move_line_ids :
                #         raw.move_line_ids.unlink()
                #         _logger.info("Loading...... 12: Hapus move_line_ids Raw materials")
                #     new_move_qty = raw.product_uom_qty
                #     while new_move_qty > 0 and lot_reversed[raw.product_id].items():
                #         is_break = False
                #         for lot, qty in lot_reversed[raw.product_id].items():
                #             if qty < new_move_qty :
                #                 new_move_line_qty = qty
                #                 new_move_qty -= qty
                #                 lot_reversed[raw.product_id].pop(lot)
                #             elif qty >= new_move_qty :
                #                 new_move_line_qty = new_move_qty
                #                 new_move_qty = 0
                #                 lot_reversed[raw.product_id][lot] = lot_reversed[raw.product_id][lot] - new_move_qty
                #                 is_break = True
                #             raw.move_line_ids.create({
                #                 'move_id': raw.id,
                #                 'product_id': raw.product_id.id,
                #                 'lot_id': lot.id,
                #                 'location_id': raw.location_id.id,
                #                 'location_dest_id': raw.location_dest_id.id,
                #                 'product_uom_id': raw.product_uom.id,
                #                 'product_uom_qty': new_move_line_qty,
                #                 'qty_done': new_move_line_qty,
                #             })
                #             _logger.info("Loading...... 13: Create move_line_ids")
                #             if is_break :
                #                 break
                #     raw.write({'quantity_done':raw.product_uom_qty})
                #     _logger.info("Loading...... 14: edit quantity_done move")
                # new_production_id.action_toggle_is_locked()
                # _logger.info("Loading...... 15: action_toggle_is_locked new MO")
                # try :
                #     new_production_id.post_inventory()
                #     _logger.info("Loading...... 16: post_inventory new MO")
                # except:
                #     pass
                #new_production_id.button_plan()
                # new_production_id.button_plan_turbo()
                # _logger.info("Planned turbo New MO success.............")
                # 
                # new_production_id_workorder_ids = self.env['mrp.workorder'].search([
                #         ('production_id','=',new_production_id.id)])

                new_mo = self.env['mrp.production'].search([('name','=',new_name)])
                if new_mo :
                    # karena ada konsep alternative wire maka, samakan kembali wire asal dan hasil split
                    product_wire_asal = production_id.move_raw_ids[0]
                    product_wire_hasil_split = new_mo.move_raw_ids[0]
                    if product_wire_asal.product_id != product_wire_hasil_split.product_id :
                        self.env.cr.execute("UPDATE stock_move SET product_id=%s WHERE id = %d"%(product_wire_asal.product_id.id, product_wire_hasil_split.id))
                    new_mo.assign_picking()
                    new_mo.action_assign()

                    move_exec = production_id.move_raw_ids.filtered(lambda raw:raw.state == 'assigned')
                    for new_raw in move_exec:
                        for raw_line in new_raw.move_line_ids :
                            if not raw_line.lot_id:
                                raw_line.unlink()
                                continue
                            if raw_line.qty_done <= 0.0 and raw_line.product_uom_qty > 0.0:
                                raw_line.qty_done = raw_line.product_uom_qty
                        try :
                            new_raw._action_done()
                        except:
                            pass

                    new_move_exec = self.env['stock.move'].search([('name','=',new_name)])
                    for move in new_move_exec:
                        if move.state == 'assigned':
                            for move_line in move.move_line_ids :
                                if move_line.qty_done <= 0.0 and move_line.product_uom_qty > 0.0:
                                    move_line.qty_done = move_line.product_uom_qty
                            if move.state == 'done' :
                                self.env.cr.execute("UPDATE stock_move SET has_split_mrp=true  WHERE id = %d"%(move.id))
                                try :
                                    move._action_done()
                                except:
                                    pass
                        elif move.state == 'draft':
                            move._action_confirm()
                    
                    new_mo.button_plan_turbo()
                    # cek data parent split sampai 3 tingkat
                    origin_mo_id = production_id
                    if production_id.origin_mo_id:
                        origin_mo_id = production_id.origin_mo_id
                        if production_id.origin_mo_id.origin_mo_id:
                            origin_mo_id = production_id.origin_mo_id.origin_mo_id
                            if production_id.origin_mo_id.origin_mo_id.origin_mo_id:
                                origin_mo_id = production_id.origin_mo_id.origin_mo_id.origin_mo_id
                    new_mo.write({'state':'progress','origin_mo_id':origin_mo_id.id})
                    if new_mo.picking_ids :
                        lot_original = False
                        for lot_ori in origin_mo_id.move_raw_ids:
                            if not lot_original :
                                for line_ori in lot_ori.move_line_ids.filtered(lambda h:h.lot_id):
                                    lot_original = line_ori.lot_id
                                    break
                        for new_consume in new_mo.picking_ids.filtered(lambda x:x.location_dest_id.usage == 'production'):
                            new_consume.action_confirm()
                            if new_consume.state == 'confirmed' :
                                for new_cons_move in new_consume.move_ids_without_package:
                                    new_cons_move.move_line_ids.create({
                                        'move_id': new_cons_move.id,
                                        'product_id': new_cons_move.product_id.id,
                                        'lot_id': lot_original.id if lot_original else False,
                                        'location_id': new_cons_move.location_id.id,
                                        'location_dest_id': new_cons_move.location_dest_id.id,
                                        'product_uom_id': new_cons_move.product_uom.id,
                                        'product_uom_qty': new_cons_move.product_uom_qty if new_cons_move.product_uom_qty > 0.0 else 0.0,
                                        'qty_done': new_cons_move.product_uom_qty if new_cons_move.product_uom_qty > 0.0 else 0.0,
                                    })
                            if product_wire_asal.state == 'done':
                                try :
                                    new_consume.action_done()
                                except :
                                    pass                
                    # picking_draft = self.env['stock.picking'].search([('group_id','=',production_id.procurement_group_id.id),('state','=','draft')])
                    # if picking_draft:
                    #     picking_draft.action_confirm()
                    old_wo_state = production_id.workorder_ids.filtered(lambda x:x.state == 'done')
                    if old_wo_state :
                        for work_order in self.env['mrp.workorder'].search([('production_id','=',new_mo.id),('workcenter_id','in',old_wo_state.mapped('workcenter_id').ids)]):
                            old_wo = production_id.workorder_ids.filtered(lambda x:x.workcenter_id.id == work_order.workcenter_id.id)
                            if old_wo :
                                try:
                                    #self.env.cr.execute("SELECT vit_done_wo(%s,%s,%s)" % (work_order.id, work_order.production_id.id, self.env.uid) )
                                    self.env.cr.execute("UPDATE mrp_workorder SET state='done', qty_produced=%d, qty_producing=%d, qty_produced_real=%d WHERE id = %d"%(new_mo.product_qty,0.0,new_mo.product_qty, work_order.id))
                                    self.env.cr.commit()
                                except:
                                    pass
                                
                                if old_wo[0].user_id:
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET user_id=%d WHERE id = %d"%( old_wo[0].user_id.id ,work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].barcodewo :
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET barcodewo='%s' WHERE id = %d"%(old_wo[0].barcodewo,work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].machine_id :
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET machine_id=%d WHERE id = %d"%(old_wo[0].machine_id.id,work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].final_lot_id :
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET final_lot_id=%d WHERE id = %d"%(old_wo[0].final_lot_id.id,work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].is_subcontracting:
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET is_subcontracting=true WHERE id = %d"%(work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].date_start:
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET date_start='%s' WHERE id = %d"%(str(old_wo[0].date_start),work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].date_finished:
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET date_finished='%s' WHERE id = %d"%(str(old_wo[0].date_finished),work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].is_wip:
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET is_wip=true WHERE id = %d"%(work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                                if old_wo[0].is_wip_report:
                                    try:
                                        self.env.cr.execute("UPDATE mrp_workorder SET is_wip_report=true WHERE id = %d"%(work_order.id))
                                        self.env.cr.commit()
                                    except:
                                        pass
                        # if not old_workorder :
                        #     old_workorder = self.env['mrp.workorder'].search([
                        #         ('production_id','=',production_id.id),
                        #         ('state','!=','cancel'),
                        #         ('workcenter_id','=',work_order.workcenter_id.id),
                        #     ], limit=1)
                        #     if not old_workorder :
                        #         continue
                        # if old_workorder.state == 'ready' :
                        #     continue
                        # elif old_workorder.state == 'progress' :
                        #     work_order.button_start()
                        #     if not old_workorder.is_user_working :
                        #         work_order.button_pending()
                        #         _logger.info("Loading...... 17: Pending WO")
                        # elif old_workorder.state == 'done' :
                        #     work_order.button_start()
                        #     _logger.info("Loading...... 18: start (button_start) WO")
                        #     work_order.record_production()
                        #     _logger.info("Loading...... 19: finish (record_production) WO ")
                        #     if work_order.state != 'done' :
                        #         #work_order.button_finish()
                        #         work_order.button_turbo_finish()
                        #         _logger.info("Loading...... 20: Turbo done WO success......... ")
                    if wo_ready:
                        _logger.info("Loading...... 10: Cari status WO terakhir ")
                        for wor in self.env['mrp.workorder'].search([('production_id','=',new_mo.id),('workcenter_id','=',wo_ready[0].workcenter_id.id),('state','=','pending')]):
                            try:
                                _logger.info("Loading...... 11: Jalankan / Start WO terakhir ")
                                #wor.button_start()
                                self.env.cr.execute("UPDATE mrp_workorder SET state='ready' WHERE id = %d"%(wor.id))
                                self.env.cr.commit()
                            except:
                                pass


                x += 1
            if remaining_qty <= 0.0 :
                _logger.info("Loading...... 21: Cancel MO lama jika qty sisanya nol")
                try:
                    production_id.action_cancel()
                except :
                    for line in production_id.move_raw_ids.filtered(lambda x:x.state not in ('done','cancel')):
                        line.move_line_ids.unlink()
                    try:
                        production_id.action_cancel()
                    except:
                        pass


class VitSplitMoLineWizard(models.TransientModel):
    _name = "vit.split.mo.line.wizard"
    _description = "Split Manufacture Line Wizard"

    # @api.multi
    # @api.depends('bom_id.routing_id', 'bom_id.routing_id.operation_ids')
    # def _compute_routing(self):
    #     for production in self:
    #         if production.bom_id.routing_id.operation_ids:
    #             production.routing_id = production.bom_id.routing_id.id
    #         else:
    #             production.routing_id = False

    def _get_bom_id(self):
        context = self.env.context
        if context.get('active_id', False):
            return self.env['mrp.production'].browse(context.get('active_id')).bom_id.id
        else :
            return False

    def _get_routing_id(self):
        context = self.env.context
        if context.get('active_id', False):
            return self.env['mrp.production'].browse(context.get('active_id')).routing_id.id
        else :
            return False

    # @api.onchange('product_id')
    # def onchange_domain_product_id(self):
    #     for x in self :
    #         # product_tmpl = self.env['mrp.bom'].search([('routing_id','=',x.wizard_id.production_id.routing_id.id),
    #         #                                             ('company_id','=',x.wizard_id.production_id.company_id.id)])
    #         product = x.wizard_id.production_id.bom_id.bom_line_ids.mapped('product_id')
    #         # bom_line = self.env['mrp.bom.line'].search([('product_id','in',product.ids),
    #         #                                             ('bom_id.company_id','=',x.wizard_id.production_id.company_id.id)])
    #         bom_line = self.env['mrp.bom.line'].search([('product_id','in',product.ids)])
    #         if bom_line :
    #             products = self.env['product.product'].search([('product_tmpl_id','in',bom_line.mapped('bom_id.product_tmpl_id').ids)])
    #             domain = {'domain': {'product_id': [('id', 'in', products.ids)]}}
    #         else :
    #             domain = {'domain': {'product_id': [('id', '=', x.wizard_id.production_id.product_id.id)]}}
    #     return domain

    wizard_id = fields.Many2one('vit.split.mo.wizard', string='Wizard', ondelete='cascade')
    product_id = fields.Many2one(
        'product.product', 'Product',
        domain=[('type', '=', 'product')],
        required=True)
    bom_id = fields.Many2one(
        'mrp.bom', 'Bill of Material',
        required=True, default=_get_bom_id, store=True)
    routing_id = fields.Many2one('mrp.routing', 'Routing', default=_get_routing_id)
        #compute='_compute_routing', store=True)
    qty = fields.Float(string='Qty', required=True, digit=dp.get_precision('Product Unit of Measure'))
    no_poly_box = fields.Char(string="No Poly Box", size=3)
    manual_number = fields.Char('Manual Number')

    @api.onchange('manual_number')
    def set_upper(self):    
        if self.manual_number:
            self.manual_number = str(self.manual_number).upper()

    # @api.onchange('product_id','bom_id')
    # def onchange_product_id(self):
    #     """ Finds UoM of changed product. """
    #     if not self.product_id:
    #         self.bom_id = False
    #     else:
    #         # bom = self.env['mrp.bom']._bom_find(product=self.product_id, picking_type=self.wizard_id.production_id.picking_type_id, company_id=self.wizard_id.production_id.company_id.id)
    #         # if bom.type == 'normal' :
    #         #     self.bom_id = bom.id
    #         # else:
    #         #     self.bom_id = False
    #         self.bom_id = self.wizard_id.production_id.bom_id.id
