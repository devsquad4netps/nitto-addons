{
    'name': "Validate Multiple Transfers",
    'version': '0.1',
    'author': '',
    'category': 'Warehouse',
    'depends': ['stock'],
    'data': [
        "views/stock_picking_views.xml",
        "wizard/stock_picking_wizard.xml",
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}
