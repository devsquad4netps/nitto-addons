from odoo import models, fields, api, tools, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_compare, float_is_zero, float_round

import logging

_logger = logging.getLogger(__name__)


class StockPickingValidateWizard(models.TransientModel):
    _name = "stock.picking.validate.wizard"

    def validate_pickings(self):
        picking_ids = self.env.context.get('active_ids', [])
        pickings = self.env['stock.picking'].browse(picking_ids)
        for picking in pickings:
            self.validate(picking)

    def validate(self, picking):
        self.ensure_one()
        if not picking.move_lines and not picking.move_line_ids:
            raise UserError(_('Please add some items to move.'))

        # If no lots when needed, raise error
        picking_type = picking.picking_type_id
        precision_digits = picking.env['decimal.precision'].precision_get('Product Unit of Measure')
        no_quantities_done = all(float_is_zero(move_line.qty_done, precision_digits=precision_digits) for move_line in
                                 picking.move_line_ids.filtered(lambda m: m.state not in ('done', 'cancel')))
        no_reserved_quantities = all(
            float_is_zero(move_line.product_qty, precision_rounding=move_line.product_uom_id.rounding) for move_line in
            picking.move_line_ids)
        if no_reserved_quantities and no_quantities_done:
            raise UserError(
                _('You cannot validate a transfer if no quantites are reserved nor done. To force the transfer, '
                  'switch in edit more and encode the done quantities.'))

        if picking_type.use_create_lots or picking_type.use_existing_lots:
            lines_to_check = picking.move_line_ids
            if not no_quantities_done:
                lines_to_check = lines_to_check.filtered(
                    lambda line: float_compare(line.qty_done, 0, precision_rounding=line.product_uom_id.rounding)
                )

            for line in lines_to_check:
                product = line.product_id
                if product and product.tracking != 'none':
                    if not line.lot_name and not line.lot_id:
                        raise UserError(
                            _('You need to supply a Lot/Serial number for product %s.') % product.display_name)

        # immediate transfer
        if no_quantities_done:
            wiz = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, picking.id)]})
            return wiz.process()

        # Check backorder should check for other barcodes
        if picking._check_backorder():
            wiz = self.env['stock.backorder.confirmation'].create({'pick_ids': [(4, p.id) for p in picking]})
            return wiz.process()

        picking.action_done()
        return
