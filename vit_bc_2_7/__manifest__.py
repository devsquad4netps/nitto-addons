{
    "name"          : "BC 2.7",
    "version"       : "1.7",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Bea Cukai",
    "license"       : "LGPL-3",
    "contributors"  : """
        - Miftahussalam <https://blog.miftahussalam.com>
    """,
    "summary"       : "Export Import BC 2.7",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "stock",
        "sale",
        "purchase",
        "vit_export_import_bc",
        "vit_kanban",
        "product_customer_code"
    ],
    "data"          : [
        "data/vit_export_import_bc.xml",
        "wizard/vit_export_import_bc_wizard.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}