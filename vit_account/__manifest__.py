{
    "name"          : "Name Get Account",
    "version"       : "1.0",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "Account",
    "summary"       : "set name_gate account with company",
    "description"   : """
        
    """,
    "depends"       : [
        "account"
    ],
    "data"          : [
        "views/account.xml",
        "reports/payment.xml"
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}