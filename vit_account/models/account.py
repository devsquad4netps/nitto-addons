from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare
from odoo.addons import decimal_precision as dp
from itertools import groupby
import math

# class AccountInvoiceLine(models.Model):
#     _inherit = "account.invoice.line"

#     @api.model
#     def create(self, vals):
#         if 'price_unit' in vals:
#             price_unit = vals['price_unit']
#             inv_Line = super(AccountInvoiceLine, self).create(vals)
#             x = math.modf(price_unit)
#             if x[0] < 0.00001:    
#                 sql = "update account_invoice_line set price_unit=%s where id = %s" % (x[1],inv_Line.id)
#                 self.env.cr.execute(sql)
#             return inv_Line
#         return super(AccountInvoiceLine, self).create(vals)

#     @api.multi
#     def write(self, vals):
#         if 'price_unit' in vals:
#             price_unit = vals['price_unit']
#             inv_Line = super(AccountInvoiceLine, self).write(vals)
#             x = math.modf(price_unit)
#             if x[0] < 0.00001:    
#                 sql = "update account_invoice_line set price_unit=%s where id = %s" % (x[1],self.id)
#                 self.env.cr.execute(sql)
#             return inv_Line
#         return super(AccountInvoiceLine, self).write(vals)

#     @api.onchange('price_unit')
#     def _onchange_price_unit(self):
#         if self.price_unit:
#             x = math.modf(self.price_unit)
#             if x[0] < 0.00001:    
#                 self.price_unit = x[1]

# AccountInvoiceLine()

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        res = super(AccountInvoice, self)._onchange_partner_id()
        if self.partner_id.property_product_pricelist and self.type in ('out_invoice','out_refund'):
            self.currency_id = self.partner_id.property_product_pricelist.currency_id.id
        return res

    # _sql_constraints = [
    #     ('number_uniq', 'unique(number, company_id, journal_id, type)', 'Invoice Number must be unique per Company!'),
    # ]
    _sql_constraints = [
        ('number_uniq', 'Check(1=1)', 'Invoice number must be unique per company!'),
    ]

AccountInvoice()

class AccountAccount(models.Model):
    _inherit = "account.account"

    # @api.multi
    # def name_get(self):
    #     result = super(AccountAccount, self).name_get()
    #     for account in self:
    #         name = account.code + ' ' + account.name
    #         if account.company_id:
    #             name = name + ' ('+account.company_id.name+')'
    #             result = []
    #         result.append((account.id, name))
    #     return result
    second_name = fields.Char('Second Name')

    @api.multi
    @api.depends('name', 'code','company_id')
    def name_get(self):
        result = []
        for account in self:
            name = account.code + ' ' + account.name
            if account.company_id :
                if account.company_id.second_name :
                    company = ' ('+account.company_id.second_name+')'
                    result.append((account.id, name+company))
                else :
                    company = ' ('+account.company_id.name+')'
                    result.append((account.id, name+company))
            else :
                result.append((account.id, name))
        return result

AccountAccount()


class AccountJournal(models.Model):
    _inherit = "account.journal"

    @api.multi
    def name_get(self):
        res = []
        for journal in self:
            currency = journal.currency_id or journal.company_id.currency_id
            name = "%s (%s)" % (journal.name, currency.name)
            if journal.bank_account_id :
                name = "[%s] %s (%s)" % (journal.bank_account_id.acc_number ,journal.name, currency.name)
            res += [(journal.id, name)]
        return res

AccountJournal()


class AccountPayment(models.Model):
    _inherit = "account.payment"

    invoices = fields.Char('Invoices', compute="_get_invoice_number", store=True)

    @api.one
    @api.depends('state','invoice_ids','invoice_ids.state')
    def _get_invoice_number(self):
        for payment in self:
            inv = payment.invoice_ids.mapped('number')
            residual = payment.invoice_ids.mapped('residual')
            if inv :
                inv_string = str(inv).replace("[","").replace("]","").replace("'","")
                payment.invoices = inv_string
                payment.amount = sum(residual)

AccountPayment()


class AccountRegisterPayments(models.TransientModel):
    _inherit = "account.register.payments"


    @api.multi
    def create_payments_draft(self):
        '''Create payments according to the invoices.
        Having invoices with different commercial_partner_id or different type (Vendor bills with customer invoices)
        leads to multiple payments.
        In case of all the invoices are related to the same commercial_partner_id and have the same type,
        only one payment will be created.

        :return: The ir.actions.act_window to show created payments.
        '''

        Payment = self.env['account.payment']
        payments = Payment
        for payment_vals in self.get_payments_vals():
            payments += Payment.create(payment_vals)
        # inv = payments.invoice_ids.mapped('number')
        # if inv :
        #     inv_string = str(inv).replace("[","").replace("]","").replace("'","")
        #     payments.invoices = inv_string
        payment_type = payments.mapped('payment_type')
        if payment_type[0] == 'inbound' :
            #hanya jika receive money maka langsung post
            payments.post()
        action_vals = {
            'name': _('Payments'),
            'domain': [('id', 'in', payments.ids), ('state', '=', 'posted')],
            'view_type': 'form',
            'res_model': 'account.payment',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }
        if len(payments) == 1:
            action_vals.update({'res_id': payments[0].id, 'view_mode': 'form'})
        else:
            action_vals['view_mode'] = 'tree,form'
        return action_vals

AccountRegisterPayments()


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    #overide fungsi create karena ketika proses import, mapping coa ga otomatis sesuai coa company headernya
    @api.model
    def create(self, vals):
        acc= self.env['account.account']
        account = acc.browse(vals['account_id'])
        move = self.env['account.move'].browse(vals['move_id'])
        new_account = acc.sudo().search([('code','=',account.code),('company_id','=',move.company_id.id)])
        if new_account :
            vals['account_id'] = new_account.id
            vals['company_id'] = new_account.company_id.id
        return super(AccountMoveLine, self).create(vals)


AccountMoveLine()


class AccountPaymentTerm(models.Model):
    _inherit = "account.payment.term"

    @api.multi
    def name_get(self):
        res = []
        for term in self:
            name = term.name
            if term.company_id :
                name = name + ' ['+term.company_id.name+']'
            res += [(term.id, name)]
        return res

AccountPaymentTerm()