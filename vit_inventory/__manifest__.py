{
    "name"          : "Security Inventory",
    "version"       : "0.2",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Inventory",
    "license"       : "LGPL-3",
    "contributors"  : """
    """,
    "summary"       : "Tampilan overview (picking type) inventory sesuai user akses",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "stock",
    ],
    "data"          : [
        #"security/group.xml",
        "views/res_users.xml",
        "views/stock.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}