# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    user_ids = fields.Many2many('res.users',string='Allowed Users')

StockPickingType()


class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    @api.multi
    @api.onchange('lot_id')
    def product_id_change(self):
        if self.lot_id and self.picking_id and self.picking_id.picking_type_id.code == 'internal':
            if self.product_uom_qty <= self.lot_id.product_qty :
                self.qty_done = self.product_uom_qty
            else:
                self.qty_done = self.lot_id.product_qty 
            domain = {'domain': {'lot_id': [('product_qty', '>', 0.0)]}}
            return domain

StockMoveLine()