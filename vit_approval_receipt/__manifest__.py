# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2020  Odoo SA  (http://www.vitraining.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Approval Goods Receipt",
    "version": "0.1",
    "category": "Warehouse",
    "sequence": 14,
    "author":  "vITraining",
    "website": "www.vitraining.com",
    "license": "AGPL-3",
    "summary": "",
    "description": """
Ada approval jika ketika penerimaan barang melebihi order (parameter set di objek partner)

    """,
    "depends": [
        "stock",
        "purchase",
        "purchase_stock",
        "vit_kanban",
    ],
    "data": [
        "wizard/approval_request.xml",
        "data/data.xml",
        "security/groups.xml",
        "security/ir.model.access.csv",
        "views/res_partner.xml",
        "views/approval_receipt.xml",
    ],
    "demo": [
    ],
    "test": [
    ],
    "installable": True,
    "auto_install": False,
    "application": False,
}