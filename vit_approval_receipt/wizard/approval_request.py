#-*- coding: utf-8 -*-
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)

class ApprovalRequestWizard(models.TransientModel):
    _name = "approval.request.wizard"
    _description = "Approval Request Receipt"

    name = fields.Char('Name')
    partner_id = fields.Many2one('res.partner','Partner')
    max_receipt = fields.Float('Max Toleransi (%)', related='partner_id.max_receipt',store=True)
    request_ids = fields.One2many('approval.request.wizard.line','approval_id', string="Details")
    kanban_id = fields.Many2one('vit.kanban','LBM')


    @api.multi
    def button_send_approval_request(self):
        approval = self.env['approval.receipt']
        for app in self.request_ids :
            if app.approval_receipt_id and app.approval_receipt_id.state == 'rejected' or not app.approval_receipt_id :
                req_id = approval.create({'origin' : self.kanban_id.name+app.purchase_id.name,
                                        'partner_id' : app.approval_id.partner_id.id,
                                        'product_id' : app.product_id.id,
                                        'purchase_line_id': app.purchase_line_id.id,
                                        'order_qty': app.order_qty,
                                        'remaining_qty':app.remaining_qty,
                                        'gap':app.gap,
                                        'uom_id':app.uom_id.id,
                                        'unit_price':app.move_id.price_unit,
                                        'kanban_id': self.kanban_id.id,
                                        'notes':self.name or False,
                                        'name':'/'
                                        })
                app.move_id.approval_receipt_id = req_id.id
        warning_mess = {
            'title': _('Info'),
            'message' : _('Data berhasil di submit !'),
        }
        return {'warning': warning_mess}


ApprovalRequestWizard()


class ApprovalRequestWizardLine(models.TransientModel):
    _name = "approval.request.wizard.line"
    _description = "Approval Request Receipt Lines"

    approval_id = fields.Many2one('approval.request.wizard',string="Approval")
    purchase_line_id = fields.Many2one('purchase.order.line','Purchase Line')
    order_qty = fields.Float('Order Qty')
    receipt_qty = fields.Float('Receipt Qty')
    remaining_qty = fields.Float('Remaining Qty')
    uom_id = fields.Many2one('uom.uom', 'UoM')
    gap = fields.Float('Gap (%)')
    product_id = fields.Many2one('product.product','Product',related="purchase_line_id.product_id", store=True)
    purchase_id = fields.Many2one('purchase.order','Purchase',related="purchase_line_id.order_id", store=True)
    move_id = fields.Many2one('stock.move', 'Move')
    approval_receipt_id = fields.Many2one('approval.receipt','Approval Number')

ApprovalRequestWizardLine()