#-*- coding: utf-8 -*-
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)

class StockMove(models.Model):
    _inherit     = "stock.move"

    approval_receipt_id = fields.Many2one('approval.receipt','Approval Document', copy=False)


StockMove()