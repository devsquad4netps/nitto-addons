#-*- coding: utf-8 -*-
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)

class ApprovalReceipt(models.Model):
    _name           = "approval.receipt"
    _description    = "Approval Receipt"

    @api.multi
    @api.depends('name','state')
    def name_get(self):
        result = []
        for res in self:
            name = res.name + ' (' + res.state + ')'
            result.append((res.id, name))
        return result 

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            vals['name'] = self.env['ir.sequence'].next_by_code('approval.receipt')
        elif 'name' not in vals :
            vals['name'] = self.env['ir.sequence'].next_by_code('approval.receipt')
        return super(ApprovalReceipt, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'to_approve':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus to approve !'))
        return super(ApprovalReceipt, self).unlink()

    name = fields.Char('Name',required=False, default='/')
    date = fields.Date('Date', default=fields.Date.context_today,required=True)
    origin = fields.Char('Source Document')
    order_qty = fields.Float('Order Qty')
    receipt_qty = fields.Float('Receipt Qty')
    remaining_qty = fields.Float('Remaining Qty')
    uom_id = fields.Many2one('uom.uom', 'UoM')
    unit_price = fields.Float('Unit Price')
    gap = fields.Float('Gap (%)')
    notes = fields.Char('Notes')
    active = fields.Boolean('Active', default=True, copy=False)
    user_approved_id = fields.Many2one('res.users','Approved/Rejected by')
    partner_id = fields.Many2one('res.partner','Partner',related="purchase_line_id.partner_id", store=True)
    product_id = fields.Many2one('product.product','Product',related="purchase_line_id.product_id", store=True)
    kanban_id = fields.Many2one('vit.kanban','LBM')
    purchase_id = fields.Many2one('purchase.order','Purchase',related="purchase_line_id.order_id", store=True)
    purchase_line_id = fields.Many2one('purchase.order.line','Purchase Line')
    company_id = fields.Many2one('res.company','Company', related="purchase_line_id.company_id", store=True)
    state = fields.Selection([('to_approve','to Approve'),('approved','Approved'),('rejected','Rejected')],string='State', default='to_approve')

    @api.multi
    def action_approve(self):
        for i in self:
            i.state = 'approved'
            i.user_approved_id = self.env.uid
            i.active = False
        warning_mess = {
            'title': _('Info'),
            'message' : _('Data berhasil di approve!'),
        }
        return {'warning': warning_mess}

    @api.multi
    def action_reject(self):
        for i in self:
            i.state = 'rejected'
            i.user_approved_id = self.env.uid
            i.active = False
        warning_mess = {
            'title': _('Info'),
            'message' : _('Data berhasil di reject!'),
        }
        return {'warning': warning_mess}

ApprovalReceipt()