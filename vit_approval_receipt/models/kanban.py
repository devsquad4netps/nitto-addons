# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2020  Odoo SA  (http://www.vitraining.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
from dateutil import relativedelta
import time
import logging
_logger = logging.getLogger(__name__)
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare, float_round, float_is_zero


class VitKanban(models.Model):
    _inherit = 'vit.kanban'


    @api.multi
    def action_validate(self):
        quantities_to_approve = []
        toleransi = self.partner_id.max_receipt
        for detail in self.detail_ids.filtered(lambda x:x.purchase_line_id):
            #import pdb;pdb.set_trace()
            order_qty = detail.purchase_line_id.product_uom._compute_quantity(detail.purchase_line_id.product_qty, detail.product_uom, rounding_method='HALF-UP')
            receipt_qty = detail.purchase_line_id.product_uom._compute_quantity(detail.purchase_line_id.qty_received, detail.product_uom, rounding_method='HALF-UP')
            total_receipt = receipt_qty+detail.quantity_done
            total_percent = (total_receipt*100)/order_qty
            gap_percent = round((total_percent - 100),2)
            if gap_percent > toleransi :
                if detail.approval_receipt_id and detail.approval_receipt_id.state == 'approved':
                    continue
                quantities_to_approve.append((0, 0, {'product_id': detail.product_id.id ,
                                                'remaining_qty' : detail.quantity_done,
                                                'order_qty' : order_qty,
                                                'receipt_qty' : receipt_qty,
                                                'uom_id' : detail.product_uom.id,
                                                'gap' : gap_percent,
                                                'move_id' : detail.id,
                                                'approval_receipt_id': detail.approval_receipt_id.id,
                                                'purchase_line_id' : detail.purchase_line_id.id
                                                    }))
        if quantities_to_approve:
            view = self.env.ref('vit_approval_receipt.approval_request_wizard_form_view')
            wiz = self.env['approval.request.wizard'].create({'partner_id': self.partner_id.id,
                                                                'kanban_id' : self.id,
                                                                'request_ids' : quantities_to_approve})
            return {
                'name': _('Konfirmasi Toleransi Penerimaan'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'approval.request.wizard',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        res = super(VitKanban, self).action_validate()
        return res

    @api.multi
    def action_cancel(self):
        for i in self.detail_ids :
            if i.approval_receipt_id:
                i.approval_receipt_id.active = False 
                i.approval_receipt_id = False
        return uper(VitKanban, self).action_cancel()


VitKanban()
