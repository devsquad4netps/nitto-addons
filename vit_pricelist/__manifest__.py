{
    "name"          : "Custome Pricelist",
    "version"       : "1.1",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Sale",
    "license"       : "LGPL-3",
    "contributors"  : """
    """,
    "summary"       : "Dalam satu company contact bisa berbeda pricelist",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "product",
        "account",
        "stock",
        "sale",
        "sale_management",
        "purchase",
    ],
    "data"          : [
        "views/vit_partner_pricelist_view.xml",
        "views/sale_view.xml",
        "views/pricelist_view.xml"
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}