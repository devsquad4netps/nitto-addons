{
    "name"          : "Opname WIP",
    "version"       : "3.2",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "MRP",
    "summary"       : "Input data no MO untuk opname",
    "description"   : """
        *1. Tujuan Form Inputan ini untuk menampung data hasil stock opname WIP yang nanti data ini sebagai histrorikal saldo Awal dimasing masing proses nya

        *2. Output dari Form ini adalah report komparasi  Data system VS Stock Opname. ada dua kemungkinan hasil dari report ini

            a. Bisa saja nomor MO di lapangan tidak di temukan tetapi di sistem masih berada di WIP

            b. Bisa saja nomor MO di lapangan WOnya ada yang berbeda dengan WO di System terhadap nomor MO yang sama.

        *3. Dari point 2 tersebut lah baru di lakukan Adjusment manual tiap MO. action yang dilakukan

            a. akan di lakukan investigasi dahulu, lalu dilakukan transaksi scrap

            b. lakukan upate manual di WO
        *4. ambil data workcenter dan group report dari field is_wip dan is_wip_report di mrp_workorder
        
    """,
    "depends"       : [
        "mrp","vit_report_mrp","vit_report_balance"
    ],
    "data"          : [
        "security/groups.xml",
        "security/ir.model.access.csv",
        "wizard/comparison_report.xml",
        "data/data.xml",
        "views/opname_wip_view.xml",
        "views/cutoff_wip_view.xml"
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}