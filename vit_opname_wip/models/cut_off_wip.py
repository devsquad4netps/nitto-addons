from odoo import models, fields, api, _
import time
import datetime
from io import BytesIO
import xlsxwriter
import base64
from xlrd import open_workbook
import logging
from odoo.exceptions import Warning, UserError
_logger = logging.getLogger(__name__)



class CutOffWIP(models.Model):
    _name = "vit.cutoff.wip"
    _inherit = ["mail.thread"]
    _description    = 'Data Cut Off WIP'
    
    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode :
            user = self.env['res.users'].sudo().search([('login','=',self.barcode)],limit=1)
            if user :
                self.operator_id = user.id
                self.barcode = False
                return
            else :
                self.warning = "Barcode %s tidak ditemukan di master user !"%(self.barcode)
                self.barcode = False
                return

    name = fields.Char( string="Number", readonly=True, default='New')
    operator_id = fields.Many2one('res.users','Operator', track_visibility='on_change', required=True,readonly=True,states={'draft': [('readonly', False)]})
    date = fields.Datetime( string="Cut off Date",  required=False, 
                        default=lambda self: time.strftime("%Y-%m-%d"),readonly=True,states={'draft': [('readonly', False)]}, track_visibility='on_change')
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.user,readonly=True)
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    notes = fields.Text( string="Notes",readonly=True,states={'draft': [('readonly', False)]}, track_visibility='on_change')
    data = fields.Binary('File')
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm')],default='draft', track_visibility='on_change', string="State")
    shift = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift",readonly=True,states={'draft': [('readonly', False)]})
    export_excels = fields.Boolean(
        string='Export Excel', dafault=False
    )
    barcode = fields.Char('Barcode', help="Jika scan otomatis bermasalah, maka letakan kursor disini",readonly=True,states={'draft': [('readonly', False)]})
    warning = fields.Text('Warning', help="warning message")
    cutoff_line_ids = fields.One2many('vit.cutoff.wip.line','cut_off_id', string='Cut Off Lines')
    adj_line_ids = fields.One2many('vit.cutoff.wip.adjustment','cut_off_id', string='Adjustment Lines')
    opname_ids = fields.One2many("vit.opname.wip","cut_off_id",string='Opname')
    filename = fields.Char(string="File Name", copy=False)
    file = fields.Binary(string="File", copy=False)
    
    
    @api.model
    def create(self, vals):
        if not vals.get('name', False) or vals['name'] == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.cutoff.wip') or 'Error Number!!!'
        return super(CutOffWIP, self).create(vals)
    
    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(CutOffWIP, self).unlink()

    @api.multi
    def get_datas(self):
        workorder = self.env['mrp.workorder'].sudo().search([('production_id.company_id','=',self.company_id.id),
                                                        ('is_wip_report','=',True),
                                                        ('workorder_date','!=',False),
                                                        ('production_id.state','!=','cancel')])
        if not workorder :
            raise UserError(_('Data not found !'))
        cr = self.env.cr
        sql = "delete from vit_cutoff_wip_line where cut_off_id=%s"
        cr.execute(sql, (self.id,) )
        lines = self.env['vit.cutoff.wip.line']
        datas = 1
        # for work in workorder.filtered(lambda w:(w.state == 'done' and w.date_finished != False and w.date_finished <= self.date) or (w.state == 'progress' and w.date_start != False and w.date_start <= self.date) and not w.cut_off_id):
        for work in workorder.filtered(lambda w:not w.cut_off_id):
            # date = work.date_start
            # if work.date_finished:
            #     date = work.date_finished
            group_report = ''
            # is_wip_wo = work.production_id.workorder_ids.filtered(lambda wo:wo.is_wip_report)
            # if is_wip_wo:
            #     group_report = is_wip_wo[0].workcenter_id.group_report
            lines.create({'production_id' : work.production_id.id,
                        'fg_qty' : work.qty_produced_real,
                        'fg_uom_id' : work.product_uom_id.id,
                        'no_poly_box' : work.production_id.no_poly_box,
                        'workorder_id' : work.id,
                        'group_report' : work.workcenter_id.group_report,
                        #'group_report' : group_report,
                        'workorder_status': work.state,
                        'date' : work.workorder_date,
                        'cut_off_id': self.id})
            _logger.info("(%s) Lot %s berhasil diinsert......"%(datas,str(work.production_id.name)))
            datas += 1

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        for x in self.cutoff_line_ids:
            if x.workorder_id.id!=False:            
                sql = "update mrp_workorder set cut_off_id=%s where id=%s" % (self.id,x.workorder_id.id)
                self.env.cr.execute(sql)

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        for x in self.cutoff_line_ids:
            if x.workorder_id.id!=False:  
                sql = "update mrp_workorder set cut_off_id=null where id=%s" % (x.workorder_id.id)
                self.env.cr.execute(sql)

    def download_examples_file(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        base_url = base_url.replace('(null)', 'https') if 'null' in base_url else base_url
        _logger.info('BASE_URL : %s' % (base_url))

        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + "/vit_opname_wip/static/doc/Template-Cut-Off-Adjustment.xlsx",
            "target": "new",
        }

    @api.multi
    def action_import_adjustment(self):
        for rec in self:
            adj_obj = self.env['vit.cutoff.wip.adjustment']
            if not rec.file :
                raise UserError("Silahkan masukan file terlebih dahulu.")
            if not rec.filename.lower().endswith(('.xls', '.xlsx')):
                raise UserError(_("File yang akan diimport (%s) harus ber ekstension .xls atau .xlsx")%(rec.filename))

            rec.adj_line_ids.unlink()
            wb = open_workbook(file_contents=base64.decodestring(rec.file))
            values = []
            for s in wb.sheets():
                for row in range(s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = (s.cell(row, col).value)
                        col_value.append(value)
                    values.append(col_value)

            production_name = []
            for data in values:
                if data[0] and data[1]:
                    if data[1] not in production_name:
                        kolom1 = data[0].strip()
                        if kolom1 == 'Adjustment Type':
                            continue
                        if kolom1 not in ('Add MO','Cancel MO','Adjust Position'):
                            raise UserError(_("Adjustment type %s tidak dikenali sistem")%(kolom1))
                        data_exist = adj_obj.search([('cut_off_id','=',rec.id),('production_name','=',data[1])])
                        if not data_exist :
                            adj_obj.create({'adj_type' : kolom1,
                                            'production_name' : data[1],
                                            'company_name' : data[2],
                                            'group_report' : data[3],
                                            'workorder_name' : data[4],
                                            'cut_off_id' : rec.id})
                            production_name.append(data[1])
            return True

CutOffWIP()


class CutOffWIPLines(models.Model):
    _name = "vit.cutoff.wip.line"
    _description    = 'Data Cut Off WIP Lines'
    _order = 'date asc'

    no_poly_box = fields.Char("No Polybox" , size=3 ,readonly=True)
    product_id = fields.Many2one('product.product',string="Product", related='production_id.product_id',store=True )
    production_id = fields.Many2one("mrp.production", string="Manufacturing Order",required=True)
    customer_id = fields.Many2one('res.partner', string="Customer", related='production_id.customer_id',store=True )
    fg_qty = fields.Float(string="Qty",readonly=True) 
    fg_uom_id = fields.Many2one('uom.uom','UoM')
    group_report = fields.Char('Workcenter Group')
    workorder_id = fields.Many2one('mrp.workorder','Workorder')
    workcenter_id = fields.Many2one('mrp.workcenter','Workcenter',related="workorder_id.workcenter_id", store=True)
    workorder_status = fields.Char('Workorder Status')
    date = fields.Date('Date')
    notes = fields.Char('Notes')
    cut_off_id = fields.Many2one("vit.cutoff.wip", 'Cut Off', ondelete='Cascade')
    opname_line_id = fields.Many2one("vit.opname.wip.line", 'Opname Line')
    

CutOffWIPLines()


class CutOffWIPAdjustment(models.Model):
    _name = "vit.cutoff.wip.adjustment"
    _description    = 'Data Cut Off WIP Adjustment'
    _order = 'production_name asc'

    adj_type = fields.Selection([('Add MO','Add MO'),('Cancel MO','Cancel MO'),('Adjust Position','Adjust Position MO')],'Adjustment Type',required=True)
    production_name = fields.Char(string="Manufacturing Order Name", required=True)
    company_name = fields.Char('Company')
    group_report = fields.Char('Adj Report Group')
    workorder_name = fields.Char('Adj Last WO')
    cut_off_id = fields.Many2one("vit.cutoff.wip", 'Cut Off', ondelete='Cascade')
    production_id = fields.Many2one("mrp.production", 'Manufacturing Order',compute="_compute_mo_details")
    production_state = fields.Selection(related="production_id.state",string="Current MO State")
    workorder_id = fields.Many2one("mrp.workorder", 'Latest WO', compute="_compute_mo_details")
    workorder_state = fields.Selection(related="workorder_id.state",string="Latest WO State")
    workorder_group_report = fields.Selection(related="workorder_id.workcenter_id.group_report",string='Current Report Group')
    
    @api.multi
    def _compute_mo_details(self):
        mrp_obj = self.env['mrp.production']
        for line in self:
            mo = mrp_obj.sudo().search([('name','=',line.production_name)],limit=1)
            if mo :
                line.production_id = mo.id
                if mo.workorder_ids:
                    wo_wip = mo.workorder_ids.filtered(lambda w:w.is_wip)
                    wo_ready = mo.workorder_ids.filtered(lambda w:w.state=='ready')
                    wo_progress = mo.workorder_ids.filtered(lambda w:w.state=='progress')
                    wo_done = mo.workorder_ids.filtered(lambda w:w.state=='done')
                    if wo_wip :
                        wo_wip.sorted('id',reverse=True)[0]
                        line.workorder_id = wo_wip.id
                    elif wo_ready:
                        line.workorder_id = wo_ready[0].id
                    elif wo_progress:
                        line.workorder_id = wo_progress[0].id
                    elif wo_done:
                        wo_done.sorted('id',reverse=True)[0]
                        line.workorder_id = wo_done[0].id
                    else:
                        line.workorder_id = mo.workorder_ids.sorted('id', reverse=False)[0].id


    
CutOffWIPAdjustment()