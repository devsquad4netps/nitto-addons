from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time


class UpnameWIP(models.Model):
    _name           = "vit.opname.wip"
    _inherit        = ["mail.thread"]
    _description    = 'Data Opname WIP'


    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode :
            user = self.env['res.users'].sudo().search([('login','=',self.barcode)],limit=1)
            if user :
                self.operator_id = user.id
                self.barcode = False
                return
            else :
                if not self.cut_off_id :
                    self.warning = "Field Cut off harus diisi terlebih dahulu !"
                    self.barcode = False
                    return
                barcode_scan = self.barcode.split(';')
                if len(barcode_scan) >= 4 :
                    barcode = barcode_scan[3]
                    polybox = barcode_scan[4]
                    qty_kg = barcode_scan[5]
                    qty_pcs = barcode_scan[6].replace(',','')
                else :
                    barcode = self.barcode
                    polybox = ''
                    qty_kg = 0
                    qty_pcs = 0
                vals = {
                        'name':barcode,
                        'polybox_mo': polybox,
                        'qty_kg' : qty_kg,
                        'qty_pcs' : qty_pcs,
                        'workcenter_opname_id':self.workcenter_opname_id,
                    }
                if self.opname_wip_detail_ids.filtered(lambda i:i.name == barcode) :
                    self.barcode = False
                    self.warning = "Barcode %s sudah diinsert/scan !"%(barcode)
                    return
                new_wip_line = self.opname_wip_detail_ids.new(vals)
                self.opname_wip_detail_ids |= new_wip_line
                self.barcode = False
                self.warning = False

    @api.onchange('date')
    def _onchange_date(self):
        return {'domain': {'cut_off_id': [('company_id','=',self.company_id.id),('date','>=',str(self.date)+' 00:00:00'),('date','<=',str(self.date)+' 23:59:59')]}}

    name = fields.Char('Number',required=True, default='/')
    name = fields.Char('Number',required=True, default='/')
    date = fields.Date('Date', default=fields.Date.context_today, track_visibility='on_change',required=True,readonly=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    operator_id = fields.Many2one('res.users','Operator', track_visibility='on_change', required=True,readonly=True,states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    shift = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift",readonly=True,states={'draft': [('readonly', False)]})
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm')],default='draft', track_visibility='on_change', string="State")
    notes = fields.Text('Notes', track_visibility='on_change')
    opname_wip_detail_ids = fields.One2many('vit.opname.wip.line', 'opname_id', 'Details',readonly=True,states={'draft': [('readonly', False)]})
    barcode = fields.Char('Barcode', help="Jika scan otomatis bermasalah, maka letakan kursor disini",readonly=True,states={'draft': [('readonly', False)]})
    warning = fields.Text('Warning', help="warning message")
    cut_off_id = fields.Many2one("vit.cutoff.wip", 'Cut Off', track_visibility='on_change',readonly=True,states={'draft': [('readonly', False)]})
    workcenter_opname_id = fields.Many2one('vit.master.store.opname', string='Store Opname')
    group_report = fields.Char(string='Group Report')

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.opname.wip')
        elif 'name' not in vals :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.opname.wip')
        return super(UpnameWIP, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(UpnameWIP, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        cr = self.env.cr
        for i in self.opname_wip_detail_ids :
            sql = "select cut.id \
                from vit_cutoff_wip_line as cut \
                left join mrp_production mrp on mrp.id = cut.production_id \
                where mrp.name = '%s' and company_id = %s and cut_off_id = %s limit 1"

            cr.execute(sql % (i.name,self.company_id.id, self.cut_off_id.id))
            res = cr.fetchone()
            if res :
                sql2 = "update vit_cutoff_wip_line set opname_line_id=%s where id = %s " % ( i.id,res[0])
                cr.execute(sql2)

        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        ids = str(tuple(self.opname_wip_detail_ids.ids)).replace(',)',')')
        sql_del = "update vit_cutoff_wip_line set opname_line_id=null where opname_line_id in %s " % (ids,)
        self.env.cr.execute(sql_del)
        self.state = 'cancel'

    @api.multi
    def action_calculate_mo(self):
        for i in self.opname_wip_detail_ids.filtered(lambda x:x.name):
            i._compute_mo_wo()

UpnameWIP()


class OpnameWIPLine(models.Model):
    _name           = "vit.opname.wip.line"
    _description    = 'Data opname WIP (Detail)'


    @api.depends('name')
    def _compute_mo_wo(self):
        cr = self.env.cr
        for line in self:
            wo = self.env['vit.cutoff.wip.line']
            line.workcenter_opname_id = line.opname_id.workcenter_opname_id
            if line.name and line.opname_id and line.opname_id.cut_off_id:
                # barcode_scan = self.barcode.split('#')
                # barcode = barcode_scan[0].strip()
                #wo_exist = line.opname_id.cut_off_id.cutoff_line_ids.filtered(lambda x:x.production_id.name == line.name)
                sql = "select cut.id, cut.workorder_id, cut.group_report, mrp.no_poly_box, mrp.product_qty, pt.kg_pal \
                    from vit_cutoff_wip_line as cut \
                    left join mrp_production mrp on mrp.id = cut.production_id \
                    left join product_product pp on mrp.product_id = pp.id \
                    left join product_template pt on pp.product_tmpl_id = pt.id \
                    where mrp.name = '%s' and mrp.company_id = %s and cut_off_id = %s limit 1"

                cr.execute(sql % (line.name,line.opname_id.company_id.id, line.opname_id.cut_off_id.id))
                res = cr.fetchall()
                if not res or res[0] == 'None':
                    return
                line.workorder_id = res[0][1]
                line.group_report = res[0][2] or ''
                line.polybox_mo = res[0][3] or ''
                line.qty_pcs = res[0][4] or 0
                qty_kg = res[0][5] or 0
                if line.workorder_id :
                    if line.workorder_id.production_id.no_poly_box:
                        line.qty_kg = int(line.workorder_id.production_id.no_poly_box.strip()[-1:])*res[0][5] or 0
                    if line.workorder_id.production_id.workorder_ids.filtered(lambda x:x.is_wip):
                        line.latest_workcenter_id = line.workorder_id.production_id.workorder_ids.filtered(lambda x:x.is_wip)[0].workcenter_id.id
                #wo.browse(res[0][0]).write({'opname_line_id':line.id})


    name = fields.Char('Name', required=True)
    opname_id = fields.Many2one('vit.opname.wip','Opname',ondelete='cascade')
  #  production_id = fields.Many2one('mrp.production','Manufacturing Order',related="workorder_id.production_id", store=True)

    production_id = fields.Many2one('mrp.production','production id')
 #   _logger.info("=============> 100 -->  production_id =  %s ) "%(production_id))
    workorder_id = fields.Many2one('mrp.workorder','Workorder',compute="_compute_mo_wo", store=True)
    latest_workcenter_id = fields.Many2one('mrp.workcenter','Latest Workorder',compute="_compute_mo_wo", store=True)
    # group_report = fields.Char('Workcenter',compute="_compute_mo_wo", store=True)
    workcenter_opname_id = fields.Many2one('vit.master.store.opname', string='Store Opname')
    group_report = fields.Char(string='Group Report', compute=False)
    state = fields.Selection(related="workorder_id.state", string="State", store=True)
    polybox_mo = fields.Char(string='Polybox',)#compute="_compute_mo_wo", store=True)
    polybox = fields.Char('Polybox SoN',size=3)
    nokar = fields.Char('Nomor Kartu SoN',size=30)
    description = fields.Char('Notes',size=200)
    qty_pcs = fields.Float('Qty Pcs',digits=(10,0))
    qty_kg = fields.Float('Qty Kg',digits=(3,0))

OpnameWIPLine()