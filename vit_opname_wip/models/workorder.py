from odoo import api, fields, models

class MRPWorkorder(models.Model):
    _inherit = "mrp.workorder"

    cut_off_id = fields.Many2one("vit.cutoff.wip", 'Cut Off')

MRPWorkorder()