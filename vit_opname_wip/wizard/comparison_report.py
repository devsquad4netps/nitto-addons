from odoo import api, fields, models, _
import datetime
from datetime import date, datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64
import pytz
from pytz import timezone
from odoo.exceptions import UserError, Warning,ValidationError
import logging
_logger = logging.getLogger(__name__)


class ComparisonReportWizard(models.TransientModel):
    _name = "vit.comparison.report.wizard"
    _description = "Comparison Report MRP"

    @api.model
    def _get_default_active_id(self):
        ctx = self._context
        if ctx.get('active_model') == 'vit.opname.wip':
            return ctx.get('active_id')

    opname_id = fields.Many2one("vit.opname.wip",string="Opname Name",required=True,default=_get_default_active_id)
    file_data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename')

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['company'] = workbook.add_format({'align': 'left','bold': 1,})
        wbf['company'].set_font_size(13)

        wbf['header_table'] = workbook.add_format({'align': 'center','font_color': '#000000'})
        #wbf['header_table'].set_border()

        wbf['header_month'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['grey']})
        wbf['header_month'].set_border()

        wbf['header_month_sum'] = workbook.add_format({'align': 'center','font_color': '#000000','bg_color':colors['dark_grey']})
        wbf['header_month_sum'].set_border()

        wbf['header_no'] = workbook.add_format({'align': 'center'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')

        wbf['title_doc'] = workbook.add_format({'align': 'left'})
        wbf['title_doc'].set_font_size(11)
        wbf['title_doc'].set_align('vcenter')

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})

        wbf['content_number_sum'] = workbook.add_format({'align': 'right', 'num_format': '#,##0','bg_color':colors['grey']})

        return wbf, workbook


    @api.multi
    def action_campare(self):
        for i in self :
            cr = self.env.cr
            cut_line = self.env['vit.cutoff.wip.line']
            opname_line = self.env['vit.opname.wip.line']
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet('Comparison WIP')
            worksheet.set_column('A1:A1', 6)
            worksheet.set_column('B1:B1', 12)
            worksheet.set_column('C1:C1', 35)
            worksheet.set_column('D1:D1', 10)
            worksheet.set_column('E1:E1', 20)
            worksheet.set_column('F1:F1', 35)
            worksheet.set_column('G1:G1', 8)
            worksheet.set_column('H1:H1', 10)
            worksheet.set_column('I1:I1', 10)
            worksheet.set_column('J1:J1', 15)
            worksheet.set_column('K1:K1', 15)
            worksheet.set_column('L1:L1', 15)
            worksheet.set_column('M1:M1', 15)
            worksheet.set_column('N1:N1', 15)
            worksheet.set_column('O1:O1', 15)
            worksheet.set_column('P1:P1', 25)
            worksheet.set_column('Q1:Q1', 15)
            worksheet.set_column('R1:R1', 15)
            worksheet.set_column('S1:S1', 15)
            worksheet.set_column('T1:T1', 15)
            worksheet.set_column('U1:U1', 15)
            worksheet.set_column('V1:V1', 15)
            worksheet.set_column('W1:W1', 15)
            worksheet.set_column('X1:X1', 15)
            worksheet.set_column('Y1:Y1', 15)

            #worksheet.set_column('V1:V1', 15)

            company = i.opname_id.company_id.name
            if i.opname_id.company_id.second_name :
                company = i.opname_id.company_id.second_name

            worksheet.merge_range('A1:Y1', 'PT. NITTO ALAM INDONESIA (%s)'%company, wbf['company'])
            worksheet.merge_range('A2:Y2', 'Laporan Hasil Stock Opname WIP Periode '+str(i.opname_id.date), wbf['company'])
            worksheet.merge_range('W3:Y3', 'Printed on : '+ str(datetime.now()-timedelta(hours=-7))[:19] + ' ('+self.env.user.name+')', wbf['header_table'])
            row = 4

            # tabel 1

            sql = "select cutl.id \
            from vit_cutoff_wip_line cutl \
            left join vit_cutoff_wip cut on cut.id = cutl.cut_off_id \
            where cutl.cut_off_id = %s and cutl.opname_line_id is not null and cut.state = 'confirm' order by cutl.date asc"

            cr.execute(sql % (self.opname_id.cut_off_id.id))
            res = cr.fetchall()
            if res :
                worksheet.merge_range('A%s:C%s'%(row,row), 'Number Match By System (Scanned)', wbf['header_table'])
                row+=1
                worksheet.write('A%s'%(row), 'No', wbf['header_month'])
                worksheet.write('B%s'%(row), 'Product Code', wbf['header_month'])
                worksheet.write('C%s'%(row), 'Product Name', wbf['header_month'])
                worksheet.write('D%s'%(row), 'Group', wbf['header_month'])
                worksheet.write('E%s'%(row), 'Manufacturing Order', wbf['header_month'])
                worksheet.write('F%s'%(row), 'Customer', wbf['header_month'])
                worksheet.write('G%s'%(row), 'Polybox', wbf['header_month'])
                worksheet.write('H%s'%(row), 'Qty Pcs', wbf['header_month'])
                worksheet.write('I%s'%(row), 'Qty Kg', wbf['header_month'])
                worksheet.write('J%s'%(row), 'Store Opname', wbf['header_month'])
                worksheet.write('K%s'%(row), 'Polybox SoN', wbf['header_month'])
                worksheet.write('L%s'%(row), 'Workcenter', wbf['header_month'])
                worksheet.write('M%s'%(row), 'Workcenter SoN', wbf['header_month'])
                worksheet.write('N%s'%(row), 'Operator', wbf['header_month'])
                worksheet.write('O%s'%(row), 'Date', wbf['header_month'])
                worksheet.write('P%s'%(row), 'Notes', wbf['header_month'])
                worksheet.write('Q%s'%(row), 'Heading', wbf['header_month'])
                worksheet.write('R%s'%(row), 'Rolling', wbf['header_month'])
                worksheet.write('S%s'%(row), 'Washing', wbf['header_month'])
                worksheet.write('T%s'%(row), 'Furnace', wbf['header_month'])
                worksheet.write('U%s'%(row), 'Plating', wbf['header_month'])
                worksheet.write('V%s'%(row), 'Three Bond', wbf['header_month'])
                worksheet.write('W%s'%(row), 'Machining', wbf['header_month'])
                worksheet.write('X%s'%(row), 'Final Quality', wbf['header_month'])
                worksheet.write('Y%s'%(row), 'Packing', wbf['header_month'])


                row+=1
                no = 1
                polybox = 0
                for dt in res :
                    data = cut_line.browse(dt)
                    if data.no_poly_box :
                        pbx = data.no_poly_box.split('/')
                        if len(pbx) == 2 :
                            polybox += int(pbx[1])
                    worksheet.write('A%s'%(row), no, wbf['title_doc'])
                    worksheet.write('B%s'%(row), data.product_id.default_code, wbf['title_doc'])
                    worksheet.write('C%s'%(row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('D%s'%(row), data.product_id.group_id2.name or '', wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.production_id.name, wbf['title_doc'])
                    worksheet.write('F%s'%(row), data.production_id.customer_id.name, wbf['title_doc'])
                    worksheet.write('G%s'%(row), data.no_poly_box, wbf['title_doc'])
                    worksheet.write('H%s'%(row), data.opname_line_id.qty_pcs, wbf['content_number'])
                    worksheet.write('I%s'%(row), data.opname_line_id.qty_kg, wbf['content_number'])
                    worksheet.write('J%s'%(row), data.opname_line_id.workcenter_opname_id.name or '', wbf['title_doc'])
                    worksheet.write('K%s'%(row), data.opname_line_id.polybox if data.opname_line_id.polybox else '', wbf['title_doc'])
                    worksheet.write('L%s'%(row), data.group_report, wbf['title_doc'])
                    worksheet.write('M%s'%(row), data.opname_line_id.workcenter_opname_id.name if data.opname_line_id.workcenter_opname_id else '', wbf['title_doc'])
                    worksheet.write('N%s'%(row), data.opname_line_id.opname_id.operator_id.name if data.opname_line_id.opname_id.operator_id else '', wbf['title_doc'])
                    worksheet.write('O%s'%(row), str(data.date), wbf['title_doc'])
                    worksheet.write('P%s'%(row), data.opname_line_id.description if data.opname_line_id.description else '', wbf['title_doc'])
                    worksheet.write('Q%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Heading' else '', wbf['content_number'])
                    worksheet.write('R%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Rolling' else '', wbf['content_number'])
                    worksheet.write('S%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Washing' else '', wbf['content_number'])
                    worksheet.write('T%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Furnace' else '', wbf['content_number'])
                    worksheet.write('U%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Plating' else '', wbf['content_number'])
                    worksheet.write('V%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Three Bond' else '', wbf['content_number'])
                    worksheet.write('W%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Machine' else '', wbf['content_number'])
                    worksheet.write('X%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Final Quality' else '', wbf['content_number'])
                    worksheet.write('Y%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Packing' else '', wbf['content_number'])
                    #worksheet.write('V%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'CF' else '', wbf['content_number'])

                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('G%s' % row, polybox, wbf['content_number_sum'])

                worksheet.write_formula('H%s' % row, '{=subtotal(9,H5:H%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('I%s' % row, '{=subtotal(9,I5:I%s)}' % (row-1), wbf['content_number_sum'])

                worksheet.write_formula('Q%s' % row, '{=subtotal(9,Q5:Q%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('R%s' % row, '{=subtotal(9,R5:R%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('S%s' % row, '{=subtotal(9,S5:S%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('T%s' % row, '{=subtotal(9,T5:T%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('U%s' % row, '{=subtotal(9,U5:U%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('V%s' % row, '{=subtotal(9,V5:V%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('W%s' % row, '{=subtotal(9,W5:W%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('X%s' % row, '{=subtotal(9,X5:X%s)}' % (row-1), wbf['content_number_sum'])
                worksheet.write_formula('Y%s' % row, '{=subtotal(9,Y5:Y%s)}' % (row-1), wbf['content_number_sum'])

            # tabel 2
            row +=1
            row3nd = row+1
            # sql3 = "select wipl.id \
            # from vit_opname_wip_line wipl \
            # left join vit_opname_wip wip on wip.id = wipl.opname_id \
            # where wipl.opname_id = %s and wipl.production_id is null and wip.state = 'confirm' order by id asc"
            # cr.execute(sql3 % (self.opname_id.id))
            sql3 = "select wipl.id \
            from vit_opname_wip_line wipl \
            left join vit_opname_wip wip on wip.id = wipl.opname_id \
            where wipl.production_id is null and wip.state = 'confirm' order by wipl.id asc"

            cr.execute(sql3)
            res3 = cr.fetchall()
            if res3 :
                worksheet.merge_range('A%s:C%s'%(row,row), 'Number Match By System (not found in the system)', wbf['header_table'])
                row+=1
                no = 1
                polybox = 0
                for dt in res3 :
                    data = opname_line.browse(dt)
                    if data.polybox :
                        pbx = data.polybox.split('/')
                        if len(pbx) == 2 :
                            polybox += int(pbx[1])
                    worksheet.write('A%s'%(row), no, wbf['title_doc'])
                    worksheet.write('B%s'%(row), '', wbf['title_doc'])
                    worksheet.write('C%s'%(row), '', wbf['title_doc'])
                    worksheet.write('D%s'%(row), '', wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.name, wbf['title_doc'])
                    worksheet.write('F%s'%(row), '', wbf['title_doc'])
                    worksheet.write('G%s'%(row), data.polybox if data.polybox else '', wbf['title_doc'])
                    worksheet.write('H%s'%(row), data.qty_pcs, wbf['title_doc'])
                    worksheet.write('I%s'%(row), data.qty_kg , wbf['title_doc'])
                    worksheet.write('J%s'%(row), data.workcenter_opname_id.name if data.workcenter_opname_id else '', wbf['title_doc'])
                    worksheet.write('K%s'%(row), '', wbf['title_doc'])
                    worksheet.write('L%s'%(row), '', wbf['title_doc'])
                    worksheet.write('M%s'%(row), '', wbf['content_number'])
                    worksheet.write('N%s'%(row), data.opname_id.operator_id.name if data.opname_id.operator_id else '', wbf['title_doc'])
                    worksheet.write('O%s'%(row), data.opname_id.name, wbf['content_number'])
                    worksheet.write('P%s'%(row), data.description if data.description else '', wbf['title_doc'])
                    worksheet.write('Q%s'%(row), '', wbf['content_number'])
                    worksheet.write('R%s'%(row), '', wbf['content_number'])
                    worksheet.write('S%s'%(row), '', wbf['content_number'])
                    worksheet.write('T%s'%(row), '', wbf['content_number'])
                    worksheet.write('U%s'%(row), '', wbf['content_number'])
                    worksheet.write('V%s'%(row), '', wbf['content_number'])
                    worksheet.write('W%s'%(row), '', wbf['content_number'])
                    worksheet.write('X%s'%(row), '', wbf['content_number'])
                    worksheet.write('Y%s'%(row), '', wbf['content_number'])

                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('G%s' % row, polybox, wbf['content_number_sum'])

                worksheet.write_formula('H%s' % row, '{=subtotal(9,H%s:H%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('I%s' % row, '{=subtotal(9,I%s:I%s)}' % (row3nd, row-1), wbf['content_number_sum'])

                worksheet.write_formula('Q%s' % row, '{=subtotal(9,Q%s:Q%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('R%s' % row, '{=subtotal(9,R%s:R%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('S%s' % row, '{=subtotal(9,S%s:S%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('T%s' % row, '{=subtotal(9,T%s:T%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('U%s' % row, '{=subtotal(9,U%s:U%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('V%s' % row, '{=subtotal(9,V%s:V%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('W%s' % row, '{=subtotal(9,W%s:W%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('X%s' % row, '{=subtotal(9,X%s:X%s)}' % (row3nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('Y%s' % row, '{=subtotal(9,Y%s:Y%s)}' % (row3nd, row-1), wbf['content_number_sum'])

            #tabel 3
            row +=1
            row2nd = row+1
            sql2 = "select cutl.id \
            from vit_cutoff_wip_line cutl \
            left join vit_cutoff_wip cut on cut.id = cutl.cut_off_id \
            where cutl.cut_off_id = %s and cutl.opname_line_id is null and cut.state = 'confirm' order by cutl.date asc"

            cr.execute(sql2 % (self.opname_id.cut_off_id.id))
            res2 = cr.fetchall()
            if res2 :
                worksheet.merge_range('A%s:C%s'%(row,row), 'Number Not Match By System (not scanned)', wbf['header_table'])
                row+=1
                no = 1
                polybox = 0
                for dt in res2 :
                    data = cut_line.browse(dt)
                    if data.no_poly_box :
                        pbx = data.no_poly_box.split('/')
                        if len(pbx) == 2 :
                            polybox += int(pbx[1])
                    qty_pcs = 0
                    qty_kg = 0
                    if data.production_id and data.production_id.no_poly_box:
                        qty_kg = int(data.production_id.no_poly_box.strip()[-1:])*data.product_id.kg_pal
                        qty_pcs = data.production_id.product_qty

                    worksheet.write('A%s'%(row), no, wbf['title_doc'])
                    worksheet.write('B%s'%(row), data.product_id.default_code, wbf['title_doc'])
                    worksheet.write('C%s'%(row), data.product_id.name, wbf['title_doc'])
                    worksheet.write('D%s'%(row), data.product_id.group_id2.name or '', wbf['title_doc'])
                    worksheet.write('E%s'%(row), data.production_id.name, wbf['title_doc'])
                    worksheet.write('F%s'%(row), data.production_id.customer_id.name, wbf['title_doc'])
                    worksheet.write('G%s'%(row), data.no_poly_box, wbf['title_doc'])
                    worksheet.write('H%s'%(row), qty_pcs, wbf['content_number'])
                    worksheet.write('I%s'%(row), qty_kg, wbf['content_number'])
                    worksheet.write('J%s'%(row), '', wbf['title_doc'])
                    worksheet.write('K%s'%(row), '', wbf['title_doc'])
                    worksheet.write('L%s'%(row), data.group_report, wbf['title_doc'])
                    worksheet.write('M%s'%(row), '', wbf['title_doc'])
                    worksheet.write('N%s'%(row), data.cut_off_id.operator_id.name if data.cut_off_id.operator_id else '', wbf['title_doc'])
                    worksheet.write('O%s'%(row), str(data.date), wbf['title_doc'])
                    worksheet.write('P%s'%(row), data.cut_off_id.name, wbf['title_doc'])
                    worksheet.write('Q%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Heading' else '', wbf['content_number'])
                    worksheet.write('R%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Rolling' else '', wbf['content_number'])
                    worksheet.write('S%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Washing' else '', wbf['content_number'])
                    worksheet.write('T%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Furnace' else '', wbf['content_number'])
                    worksheet.write('U%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Plating' else '', wbf['content_number'])
                    worksheet.write('V%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Three Bond' else '', wbf['content_number'])
                    worksheet.write('W%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Machine' else '', wbf['content_number'])
                    worksheet.write('X%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Final Quality' else '', wbf['content_number'])
                    worksheet.write('Y%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'Packing' else '', wbf['content_number'])
                    #worksheet.write('V%s'%(row), data.workorder_id.qty_production if data.workorder_id.workcenter_id.group_report == 'CF' else '', wbf['content_number'])

                    no += 1
                    row += 1
                # sum total perpartner
                worksheet.write('G%s' % row, polybox, wbf['content_number_sum'])

                worksheet.write_formula('H%s' % row, '{=subtotal(9,H%s:H%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('I%s' % row, '{=subtotal(9,I%s:I%s)}' % (row2nd, row-1), wbf['content_number_sum'])

                worksheet.write_formula('Q%s' % row, '{=subtotal(9,Q%s:Q%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('R%s' % row, '{=subtotal(9,R%s:R%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('S%s' % row, '{=subtotal(9,S%s:S%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('T%s' % row, '{=subtotal(9,T%s:T%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('U%s' % row, '{=subtotal(9,U%s:U%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('V%s' % row, '{=subtotal(9,V%s:V%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('W%s' % row, '{=subtotal(9,W%s:W%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('X%s' % row, '{=subtotal(9,X%s:X%s)}' % (row2nd, row-1), wbf['content_number_sum'])
                worksheet.write_formula('Y%s' % row, '{=subtotal(9,Y%s:Y%s)}' % (row2nd, row-1), wbf['content_number_sum'])

            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = 'Laporan Opname WIP'
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

ComparisonReportWizard()