# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Production Subcontractor',
    'website': 'http://www.vitraining.com',
    'author': 'vITraining',
    'version': '5.1',
    'support': 'widianajuniar@gmail.com',
    'license': 'AGPL-3',
    'images': [],
    'category': 'Manufacturing',
    'sequence': 14,
    'summary': 'Subcontractor Workorder',
    'depends': ['mrp','stock','vit_export_import_bc','vit_workorder','vit_kartu_prod_add','vit_do_add','vit_nitto_mrp','sh_barcode_scanner'],
    'description': "Fitur untuk subcon workorder versi nitto",
    'data': [
        'security/group.xml',
        'data/data.xml',
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/workorder_views.xml',
        'views/subcontract_views.xml', 
        'views/picking_views.xml',   
        'views/workcenter_views.xml',
        'views/workorder_subcon.xml',
        'report/surat_perintah_kerja.xml',
        'report/rekap_subcon.xml'
    ],
    'test': [],
    'application': True,
}