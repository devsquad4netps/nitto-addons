# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning,ValidationError


class MrpWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    @api.multi
    def write(self, values):
        if list(values.keys()) != ['time_ids'] and any(workorder.state == 'done' for workorder in self):
            return False
            # raise UserError(_('You can not change the finished work order.'))
        return super(MrpWorkorder, self).write(values)

    is_subcontracting = fields.Boolean('Is Subcontracting', track_visibility='onchange',copy=False, default=False)
    stock_move_id = fields.Many2one('stock.move','Move', help="move from subcon",copy=False)
    subcontract_id = fields.Many2one('mrp.subcontract','Subcontract',copy=False)
    wo_subcon_id = fields.Many2one('mrp.workorder.subcon','Workorder Subcon',copy=False)

    @api.multi
    def button_subcontractor(self):
        #self.ensure_one()
        for x in self:
            cr = self.env.cr
            try :
                x.is_subcontracting = True
            except:
                try:
                    cr.execute("UPDATE mrp_workorder SET is_subcontracting=true WHERE id=%s",(x.id))
                except:
                    pass

    @api.multi
    def button_unsubcontractor(self):
        #self.ensure_one()
        for x in self:
            cr = self.env.cr
            try :
                x.is_subcontracting = False
                x.subcontract_id = False
                x.wo_subcon_id = False
            except :
                try:
                    cr.execute("UPDATE mrp_workorder SET is_subcontracting=false,subcontract_id=null,wo_subcon_id=null  WHERE id=%s",(x.id))
                except:
                    pass

    @api.multi
    def button_finish(self):
        self.ensure_one()
        if self.is_subcontracting :
            if not self.stock_move_id and not self.subcontract_id :
                raise UserError(_("Work Order subcon ini belum ditarik di form subcontractor order !"))
        return super(MrpWorkorder, self).button_finish()


MrpWorkorder()