# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning,ValidationError


class MrpWorkorderSubcon(models.Model):
    _name           = 'mrp.workorder.subcon'
    _inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]

    @api.multi
    @api.depends('name','partner_id')
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.partner_id :
                name = '('+res.name+ ') '+ res.partner_id.name
            result.append((res.id, name))
        return result 
        
    def _scan_subcon(self, barcode): 
       # import pdb;pdb.set_trace()
        if self and self.state in ["cancel","confirm"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state), self.state)
            raise UserError(_("You can not scan item in %s state.") %(value))
        elif self:
            user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
            if user :
                self.user_id = user.id
            else :
                barcode_scan = barcode.split('#')
                barcode = barcode_scan[0].strip()
                if self.contract_id.company_id != self.company_id :
                    #jika beda company cari yg done
                    workorder = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode),('state','=','done'),('workcenter_id','in',self.contract_id.workcenter_ids.ids)],limit=1)
                else :
                    #jika same company cari yg ready
                    workorder = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode),('state','=','ready'),('workcenter_id','in',self.contract_id.workcenter_ids.ids)],limit=1)
                #data = []
                if workorder :
                    if workorder.production_id.company_id != self.company_id :
                        raise Warning('Company %s (%s) berbeda (%s)' % (barcode,workorder.production_id.company_id.name,self.company_id.name))
                    if workorder.workcenter_id.id not in self.contract_id.workcenter_ids.ids :
                        raise Warning('Workcenter %s (%s) berbeda (%s)' % (barcode,workorder.workcenter_id.name,str(self.contract_id.workcenter_ids.mapped('name'))))
                    if workorder.is_subcontracting and workorder.production_id.company_id == self.company_id:
                        raise Warning('Workorder %s (%s) sudah di tag subcon oleh dokumen lain' % barcode,workorder.workcenter_id.name)
                    if workorder.state in ('cancel','done') :
                        raise Warning('Workorder %s sudah berstatus %s' % (barcode,workorder.state))
                    self.workorder_ids |= workorder
                    #self.workorder_ids = [(6, 0, wos.ids)]

                else :
                    raise Warning('Manufacturing Order %s tidak sesuai dengan kontrak subcon' % (barcode))

    def on_barcode_scanned(self, barcode): 
        self._scan_subcon(barcode)


    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode :
            if self and self.state in ["cancel","confirm","done"]:
                selections = self.fields_get()["state"]["selection"]
                value = next((v[1] for v in selections if v[0] == self.state), self.state)
                raise UserError(_("You can not scan item in %s state.") %(value))
            elif self:
                barcode = self.barcode
                user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
                if user :
                    self.operator_id = user.id
                    self.barcode = False
                    self.warning = False
                    return
                else :
                    barcode_scan = barcode.split('#')
                    barcode = barcode_scan[0].strip()
                    if self.contract_id.company_id != self.company_id :
                        #jika beda company cari yg done
                        workorders = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode),('state','=','done'),('workcenter_id','in',self.contract_id.workcenter_ids.ids)])
                    else :
                        #jika same company cari yg ready
                        workorders = self.env['mrp.workorder'].sudo().search([('production_id.name','=',barcode),('state','=','ready'),('workcenter_id','in',self.contract_id.workcenter_ids.ids)])

                    #data = []
                    if workorders :
                        for workorder in workorders :
                            if workorder.production_id.company_id != self.company_id and not self.contract_id.is_interco:
                                self.warning = "Company %s (%s) berbeda (%s)!"%(barcode,workorder.production_id.company_id.name,self.company_id.name)
                                self.barcode = False
                                return
                            if workorder.workcenter_id.id not in self.contract_id.workcenter_ids.ids :
                                self.warning = 'Workcenter %s (%s) berbeda (%s)' % (barcode,workorder.workcenter_id.name,str(self.contract_id.workcenter_ids.mapped('name')))
                                self.barcode = False
                                return
                            if workorder.is_subcontracting and workorder.production_id.company_id == self.company_id and workorder.subcontract_id and workorder.workcenter_id.id in self.contract_id.workcenter_ids.ids and workorder.wo_subcon_id:
                                if workorder.wo_subcon_id.workorder_ids.filtered(lambda wo:wo.workcenter_id.id == workorder.workcenter_id.id) :
                                    self.warning = 'Workorder %s (%s) sudah di tag subcon oleh dokumen lain (%s) - %s' % (barcode,workorder.workcenter_id.name,workorder.subcontract_id.name, workorder.subcontract_id.partner_id.name)
                                    self.barcode = False
                                    return
                            if workorder.state in ('cancel','done') and self.contract_id.company_id == self.company_id:
                                self.warning = 'Workorder %s sudah berstatus %s' % (barcode,workorder.state)
                                self.barcode = False
                                return
                            #import pdb;pdb.set_trace()
                            # for item in workorder.production_id.bom_id.bom_line_ids:
                            #     if item.product_id.id not in self.contract_id.product_ids.mapped('product_id.id'):
                            #         self.warning = 'Bahan baku %s tidak ada didalam kontrak %s' % (item.product_id.name,self.contract_id.name)
                            #         self.barcode = False
                            #         return
                            if workorder.product_id.id not in self.contract_id.product_ids.mapped('product_id.id'):
                                self.warning = 'Bahan baku %s tidak ada didalam kontrak %s' % (workorder.production_id.product_id.name,self.contract_id.name)
                                self.barcode = False
                                return
                            self.workorder_ids |= workorder
                            self.barcode = False
                            self.warning = False
                            #self.workorder_ids = [(6, 0, wos.ids)]
                    else :
                        self.warning = 'Manufacturing Order %s tidak sesuai dengan kontrak subcon' % (barcode)
                        self.barcode = False
                        return

    @api.onchange('contract_id')
    def onchange_domain_contract_id(self):
        if self.contract_id :
            contract = self.env['mrp.subcontract']
            contracts = contract.sudo().search([('company_id','=',self.company_id.id),('partner_id','=',self.partner_id.id),('jt_tgl_subkontrak','>=',self.date),('state','=','confirm')])
            if contracts :
                contract |= contracts
            interco_contracts = contract.sudo().search([('is_interco','=',True),('company_id','!=',self.company_id.id)])
            if interco_contracts :
                contract |= interco_contracts
            return {'domain': {'contract_id': [('id','in',contract.ids)]}}


    # @api.onchange('workorder_ids')
    # def onchange_domain_workorder_ids(self):
    #     if self.workorder_ids :
    #         return {'domain': {'workorder_ids': [('production_id.company_id','=',self.company_id.id),('workcenter_id','in',self.contract_id.workcenter_ids.ids),('is_subcontracting','=',False),('state','=','ready')]}}

    @api.onchange('contract_id')
    def _onchange_contract_id(self):
        if self.contract_id and self.contract_id.workcenter_ids :
            self.workcenter_ids = [(6, 0, self.contract_id.workcenter_ids.sorted('id',reverse=False).ids)]
        return {'domain': {'workorder_ids': [('production_id.company_id','=',self.company_id.id),('workcenter_id','in',self.workcenter_ids.ids),('is_subcontracting','=',False),('state','=','ready')]}}
    
    name = fields.Char('Number',required=True, default='/')
    partner_id = fields.Many2one('res.partner','Partner', track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    date = fields.Date('Date', default=fields.Date.context_today, track_visibility='on_change',required=True,readonly=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    workcenter_id = fields.Many2one('mrp.workcenter','Workcenter', track_visibility='on_change', required=False,readonly=True,states={'draft': [('readonly', False)]})
    operator_id = fields.Many2one('res.users','Operator', track_visibility='on_change', required=True,readonly=True,states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    shift = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift",readonly=True,states={'draft': [('readonly', False)]})
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm'),('done','Done')],default='draft', track_visibility='on_change', string="State")
    notes = fields.Text('Notes', track_visibility='on_change')
    workorder_ids = fields.Many2many('mrp.workorder', string='Workorder',readonly=True,states={'draft': [('readonly', False)]})
    contract_id = fields.Many2one('mrp.subcontract','Contract',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
    workcenter_ids = fields.Many2many('mrp.workcenter',string='Workcenters', track_visibility='onchange')
    barcode = fields.Char('Barcode',copy=False)
    warning = fields.Text('Warning', help="warning message")
    retur = fields.Boolean('Return', copy=False, track_visibility='onchange')

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            vals['name'] = self.env['ir.sequence'].next_by_code('mrp.workorder.subcon')
        elif 'name' not in vals :
            vals['name'] = self.env['ir.sequence'].next_by_code('mrp.workorder.subcon')
        return super(MrpWorkorderSubcon, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(MrpWorkorderSubcon, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        for wo in self.workorder_ids :
            cr = self.env.cr
            sql = "select mrp_workorder_subcon_id from mrp_workorder_mrp_workorder_subcon_rel where mrp_workorder_id=%s and mrp_workorder_subcon_id != %s"% (wo.id,self.id)
            cr.execute(sql)
            data_exist = cr.dictfetchall()
            if data_exist :
                for dt in data_exist:
                    other_data = self.browse(dt['mrp_workorder_subcon_id'])
                    if other_data.company_id == self.company_id :
                        raise Warning('Manufacturing number %s sudah di assign di Workorder Subcon %s' % (wo.production_id.name,other_data.name))
            if wo.workcenter_id.group_report :
                wo_group =self.env['mrp.workorder'].sudo().search([('production_id','=',wo.production_id.id),
                                                                        ('workcenter_id.group_report','=',wo.workcenter_id.group_report)])
                wo_group.button_subcontractor()
                for wo in wo_group:
                    cr.execute("UPDATE mrp_workorder SET subcontract_id=%s,wo_subcon_id=%s WHERE id=%s",(self.contract_id.id,self.id,wo.id))
                    if not wo.workorder_date:
                        cr.execute("UPDATE mrp_workorder SET workorder_date=%s WHERE id=%s",(str(self.date),wo.id))
                    if not wo.shift:
                        shift = self.shift
                        if not shift:
                            import datetime
                            now = datetime.datetime.now()
                            self.env.cr.execute("SELECT fn_get_shift_date (%s,%s, cast('%s' as timestamp))" % (self.company_id.id, wo.workcenter_id.id, str(now)[:19]) )
                            cek_shift = self.env.cr.fetchone()
                            if cek_shift :
                                cek_shift = cek_shift[0]
                                shift = cek_shift.split('#')[1]
                                if shift:
                                    cr.execute("UPDATE mrp_workorder SET shift=%s WHERE id=%s",(str(self.shift),wo.id))
            else:
                wo.button_subcontractor()
                cr.execute("UPDATE mrp_workorder SET subcontract_id=%s,wo_subcon_id=%s WHERE id=%s",(self.subcontract_id.id,self.id,wo.id))
                if not wo.workorder_date:
                    cr.execute("UPDATE mrp_workorder SET workorder_date=%s WHERE id=%s",(str(self.date),wo.id))

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        for wo in self.workorder_ids :
            if wo.workcenter_id.group_report :
                wo_group =self.env['mrp.workorder'].sudo().search([('production_id','=',wo.production_id.id),('workcenter_id.group_report','=',wo.workcenter_id.group_report)])
                wo_group.button_unsubcontractor()
            else:
                wo.button_unsubcontractor()
                cr.execute("UPDATE mrp_workorder SET subcontract_id=null,wo_subcon_id=null WHERE id=%s",(wo.id))

MrpWorkorderSubcon()