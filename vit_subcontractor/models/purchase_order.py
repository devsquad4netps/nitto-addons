# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    workorder_id = fields.Many2one('mrp.workorder','Workorder')

PurchaseOrder()