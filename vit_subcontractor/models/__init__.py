# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from . import stock
from . import purchase_order
from . import workcenter
from . import workorder
from . import subcontract
from . import workorder_subcon