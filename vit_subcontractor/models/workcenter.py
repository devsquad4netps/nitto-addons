# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import xlsxwriter
import base64
from io import BytesIO
import pytz
from pytz import timezone
import PIL
import io
from odoo import api, fields, models, _
import datetime
from odoo.exceptions import UserError


class MrpWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    is_subcontracting = fields.Boolean('Is Subcontracting')
    partner_id = fields.Many2one('res.partner','Partner')
    product_id = fields.Many2one('product.product','Product')
    cost_per_qty = fields.Float('Cost per Qty')
    picking_type_id = fields.Many2one('stock.picking.type','Picking Type')
    return_type_id = fields.Many2one('stock.picking.type','Return Type')
    file_data = fields.Binary('File', readonly=True)
    no_subkontrak = fields.Char('No Subkontrak')
    tgl_subkontrak = fields.Date('Tgl Subkontrak')
    tgl_kontrak = fields.Date('Tgl Kontrak Kerja')
    jt_tgl_subkontrak = fields.Date('Jatuh Tempo Subkontrak')
    no_bpj = fields.Char('No BPJ')
    tgl_bpj = fields.Date('Tgl BPJ')
    no_jaminan = fields.Char('No Jaminan')
    nilai_jaminan = fields.Float('Nilai Jaminan')
    max_movement = fields.Integer('Max Movement')
    movement = fields.Float(
        'Movement', compute='_compute_movement',
        help='Movement datas', digits=(16, 0))
    total = fields.Float('Total')
    uom_id = fields.Many2one('uom.uom','UoM')
    wip_ids = fields.Many2many('mrp.workorder', compute="_search_workorder_ids")

    @api.multi
    def _compute_movement(self):
        for movement in self :
            datas = self.env['stock.picking'].search([('workorder_id.workcenter_id', '=', movement.id)])
            if datas:
                movement.movement = len(datas)

    @api.multi
    def _search_workorder_ids(self):
        for movement in self :
            datas = self.env['mrp.workorder'].search([('workcenter_id', '=', movement.id)])
            if datas:
                movement.wip_ids = datas

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format({'bold': 1,'align': 'center','font_color': '#000000'})

        wbf['header2'] = workbook.add_format({'bold': 1,'align': 'left','font_color': '#000000'})

        wbf['header_table'] = workbook.add_format({'align': 'left','font_color': '#000000'})
        wbf['header_table'].set_border()
        
        wbf['header_no'] = workbook.add_format({'bold': 1,'align': 'center','font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
                
        wbf['footer'] = workbook.add_format({'align':'left'})
        
        wbf['title_doc'] = workbook.add_format({'bold': 1,'align': 'left'})
        wbf['title_doc'].set_font_size(12)
        
        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)
        
        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        
        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right() 
          
        
        wbf['total'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'center'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()            
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()         
        
        return wbf, workbook

    @api.multi
    def action_print(self):
        for i in self :
            if not i.is_subcontracting :
                raise exceptions.UserError(_('Report yang bisa ditarik hanya workcenter subcon'))
            report_name = 'Monitoring Subcon %s'%(i.partner_id.name)
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(i.name)
            worksheet.set_column('A1:A1', 3)
            worksheet.set_column('B1:B1', 30)
            worksheet.set_column('C1:C1', 10)
            worksheet.set_column('D1:D1', 10)
            worksheet.set_column('E1:E1', 10)
            worksheet.set_column('F1:F1', 10)
            worksheet.set_column('G1:G1', 10)
            worksheet.set_column('H1:H1', 10)
            worksheet.set_column('I1:I1', 10)
            worksheet.set_column('J1:J1', 10)
            worksheet.set_column('K1:K1', 10)
            worksheet.set_column('L1:L1', 10)
            worksheet.set_column('M1:M1', 10)
            worksheet.set_column('N1:N1', 10)
            worksheet.set_column('O1:O1', 10)
            worksheet.set_column('P1:P1', 10)
            worksheet.set_column('Q1:Q1', 10)
            worksheet.set_column('R1:R1', 10)
            worksheet.set_column('S1:S1', 10)
            worksheet.set_column('T1:T1', 10)
            worksheet.set_column('U1:U1', 10)
            worksheet.set_column('V1:V1', 10)
            worksheet.set_column('W1:W1', 10)
            worksheet.set_column('X1:X1', 10) 

            worksheet.merge_range('A1:X1', 'Monitoring Pelaksanaan Subkontrak', wbf['header'])
            worksheet.write('A2', 'Nama PKB/PDKB', wbf['header2'])
            worksheet.write('A3', 'Nama Penerima Subkontrak', wbf['header2'])
            worksheet.write('A4', 'Nomor dan Tgl Persetujuan Subkontrak', wbf['header2'])
            worksheet.write('A5', 'Nomor dan Tgl Kontrak Kerja', wbf['header2'])
            worksheet.write('A6', 'Jatuh Tempo Pelaksanaan Subkontrak', wbf['header2'])
            worksheet.write('C2', ': %s'%i.company_id.name, wbf['header2'])
            worksheet.write('C3', ': %s'%i.partner_id.name, wbf['header2'])
            worksheet.write('C4', ': %s (%s)'%(i.no_subkontrak,datetime.datetime.strptime(str(i.tgl_subkontrak), '%Y-%m-%d').strftime('%d-%m-%Y')), wbf['header2'])
            worksheet.write('C5', ': %s (%s)'%(i.code,datetime.datetime.strptime(str(i.tgl_kontrak), '%Y-%m-%d').strftime('%d-%m-%Y')), wbf['header2'])
            worksheet.write('C6', ': '+datetime.datetime.strptime(str(i.jt_tgl_subkontrak), '%Y-%m-%d').strftime('%d-%m-%Y'), wbf['header2'])

            worksheet.write('R2', 'Nomor dan Tgl BPJ', wbf['header2'])
            worksheet.write('R3', 'Nomor Jaminan', wbf['header2'])
            worksheet.write('R4', 'Nilai Jaminan', wbf['header2'])
            worksheet.write('U2', ': %s (%s)'%(i.no_bpj,datetime.datetime.strptime(str(i.tgl_bpj), '%Y-%m-%d').strftime('%d-%m-%Y')), wbf['header2'])
            worksheet.write('U3', ': '+i.no_jaminan, wbf['header2'])
            # worksheet.write('U4', ': '+f"{i.nilai_jaminan:,}", wbf['header2'])
            worksheet.write('U4', ': '+str(i.nilai_jaminan), wbf['header2'])

            worksheet.merge_range('A8:A10', 'No', wbf['header_no'])
            worksheet.merge_range('B8:B10', 'Jenis Barang Yang Akan Dikeluarkan', wbf['header_no'])
            worksheet.merge_range('C8:D9', 'Jenis', wbf['header_no'])
            worksheet.merge_range('E8:X8', 'Pengeluaran', wbf['header_no'])
            worksheet.merge_range('E9:F9', 'I', wbf['header_no'])
            worksheet.merge_range('G9:H9', 'II', wbf['header_no'])
            worksheet.merge_range('I9:J9', 'III', wbf['header_no'])
            worksheet.merge_range('K9:L9', 'IV', wbf['header_no'])
            worksheet.merge_range('M9:N9', 'V', wbf['header_no'])
            worksheet.merge_range('O9:P9', 'VI', wbf['header_no'])
            worksheet.merge_range('Q9:R9', 'VII', wbf['header_no'])
            worksheet.merge_range('S9:T9', 'VIII', wbf['header_no'])
            worksheet.merge_range('U9:V9', 'IX', wbf['header_no'])
            worksheet.merge_range('W9:X9', 'X', wbf['header_no'])

            worksheet.write('C10', 'Jumlah', wbf['header_no'])
            worksheet.write('D10', 'Jenis', wbf['header_no'])

            worksheet.write('E10', 'Jumlah', wbf['header_no'])
            worksheet.write('F10', 'Sisa', wbf['header_no'])
            worksheet.write('G10', 'Jumlah', wbf['header_no'])
            worksheet.write('H10', 'Sisa', wbf['header_no'])
            worksheet.write('I10', 'Jumlah', wbf['header_no'])
            worksheet.write('J10', 'Sisa', wbf['header_no'])
            worksheet.write('K10', 'Jumlah', wbf['header_no'])
            worksheet.write('L10', 'Sisa', wbf['header_no'])
            worksheet.write('M10', 'Jumlah', wbf['header_no'])
            worksheet.write('N10', 'Sisa', wbf['header_no'])
            worksheet.write('O10', 'Jumlah', wbf['header_no'])
            worksheet.write('P10', 'Sisa', wbf['header_no'])
            worksheet.write('Q10', 'Jumlah', wbf['header_no'])
            worksheet.write('R10', 'Sisa', wbf['header_no'])
            worksheet.write('S10', 'Jumlah', wbf['header_no'])
            worksheet.write('T10', 'Sisa', wbf['header_no'])
            worksheet.write('U10', 'Jumlah', wbf['header_no'])
            worksheet.write('V10', 'Sisa', wbf['header_no'])
            worksheet.write('W10', 'Jumlah', wbf['header_no'])
            worksheet.write('X10', 'Sisa', wbf['header_no'])

            # search out
            datas = self.env['stock.picking'].search([('workorder_id.workcenter_id', '=', i.id),('location_dest_id.usage','=','production'),('state','!=','cancel')], order="name asc")
            if datas :
                mrp = self.env['mrp.production']
                no = 1
                row = 11
                for data in datas :
                    mrp_exist = mrp.sudo().search([('name','=',data.origin)],limit=1)
                    if mrp_exist :
                        rawmate = mrp_exist.move_raw_ids.filtered(lambda mv:mv.product_id.type == 'product')
                        if rawmate :
                            worksheet.write('A%s'%row, no, wbf['content_number'])
                            worksheet.merge_range('B%s:X%s'%(row,row), rawmate[0].product_id.name, wbf['content'])



        # #konten di sini
        # if self.type == 'detail' :
        #     worksheet.set_column('A1:A1', 20)
        #     worksheet.set_column('B1:B1', 30)
        #     worksheet.set_column('C1:C1', 40)
        #     worksheet.set_column('D1:D1', 20)
        #     worksheet.set_column('E1:E1', 20)
        #     worksheet.set_column('F1:F1', 20)
        #     worksheet.set_column('G1:G1', 20)
        #     worksheet.set_column('H1:H1', 20)
        #     worksheet.set_column('I1:I1', 20)
        #     worksheet.set_column('J1:J1', 20)

        #     worksheet.write('A1', 'Code', wbf['header'])
        #     worksheet.write('B1', 'Product Name', wbf['header'])
        #     worksheet.write('C1', 'Product Category', wbf['header'])
        #     worksheet.write('D1', 'On hand all locations', wbf['header'])
        #     worksheet.write('E1', 'Start', wbf['header'])
        #     worksheet.write('F1', 'Qty In', wbf['header'])
        #     worksheet.write('G1', 'Qty Out', wbf['header'])
        #     worksheet.write('H1', 'Balance', wbf['header'])
        #     worksheet.write('I1', 'HPP', wbf['header'])
        #     worksheet.write('J1', 'HPJ', wbf['header'])
        #     row = 2
        #     for line in summary_id.summary_line :
        #         # worksheet.write('A%s' % row, line.product_id.name_get()[0][1], wbf['content'])
        #         worksheet.write('A%s' % row, line.product_id.default_code, wbf['content'])
        #         worksheet.write('B%s'%row, line.product_id.name, wbf['content'])
        #         worksheet.write('C%s' % row, line.product_id.categ_id.name_get()[0][1], wbf['content'])
        #         worksheet.write('D%s'%row, line.qty_available, wbf['content_float'])
        #         worksheet.write('E%s'%row, line.qty_start, wbf['content_float'])
        #         worksheet.write('F%s'%row, line.qty_in, wbf['content_float'])
        #         worksheet.write('G%s'%row, line.qty_out, wbf['content_float'])
        #         worksheet.write('H%s'%row, line.qty_balance, wbf['content_float'])
        #         worksheet.write('I%s'%row, line.hpp, wbf['content_float'])
        #         worksheet.write('J%s'%row, line.hpj, wbf['content_float'])
        #         row += 1

        #     worksheet.merge_range('A%s:C%s' %(row,row), 'Total', wbf['header_no'])
        #     worksheet.write_formula('D%s' %row, '{=subtotal(9,D2:D%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('E%s' %row, '{=subtotal(9,E2:E%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('F%s' %row, '{=subtotal(9,F2:F%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('G%s' %row, '{=subtotal(9,G2:G%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('H%s' %row, '{=subtotal(9,H2:H%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('I%s' %row, '{=subtotal(9,I2:I%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('J%s' %row, '{=subtotal(9,J2:J%s)}'%(row-1), wbf['total_float'])
        # else :
        #     worksheet.set_column('A1:A1', 40)
        #     worksheet.set_column('B1:B1', 20)
        #     worksheet.set_column('C1:C1', 20)
        #     worksheet.set_column('D1:D1', 20)
        #     worksheet.set_column('E1:E1', 20)
        #     worksheet.set_column('F1:F1', 20)

        #     worksheet.write('A1', 'No Transaksi', wbf['header'])
        #     worksheet.write('B1', 'Tanggal', wbf['header'])
        #     worksheet.write('C1', 'Kode Barang', wbf['header'])
        #     worksheet.write('D1', 'Qty', wbf['header'])
        #     worksheet.write('E1', 'Harga', wbf['header'])
        #     worksheet.write('F1', 'Total', wbf['header'])
        #     row = 2
        #     domain = [('summary_id','=',summary_id.id)]
        #     if self.history == 'in' :
        #         domain.append(('type','=','in'))
        #     elif self.history == 'out' :
        #         domain.append(('type','=','out'))
        #     history_ids = self.env['stock.summary.line.new.history'].search(domain)
        #     for history in history_ids :
        #         worksheet.write('A%s'%row, history.name, wbf['content'])
        #         worksheet.write('B%s'%row, history.date, wbf['content'])
        #         worksheet.write('C%s'%row, history.product_code, wbf['content'])
        #         worksheet.write('D%s'%row, history.qty, wbf['content_float'])
        #         worksheet.write('E%s'%row, history.price, wbf['content_float'])
        #         worksheet.write('F%s'%row, history.total, wbf['content_float'])
        #         row += 1
        #     worksheet.merge_range('A%s:C%s'%(row,row), 'Total', wbf['total'])
        #     worksheet.write_formula('D%s' %row, '{=subtotal(9,D2:D%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('E%s' %row, '{=subtotal(9,E2:E%s)}'%(row-1), wbf['total_float'])
        #     worksheet.write_formula('F%s' %row, '{=subtotal(9,F2:F%s)}'%(row-1), wbf['total_float'])
        #sampai sini

            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = i.name
            if i.code :
                filename = i.code+'-'+filename
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }


MrpWorkcenter()