# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import date


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    # def on_barcode_scanned(self, barcode):
    #     res = super(StockPicking, self).on_barcode_scanned(barcode)
    #     #jika MO
    #     search_mo = self.env["mrp.production"].search([('name','=',barcode)], limit = 1)
    #     if search_mo :
    #         x=1
    #     return res

    @api.multi
    def write(self, vals):
        picking = self.env['stock.picking.type']
        for pick in self :
            if pick.company_id.bea_cukai :
                picking_type = pick.picking_type_id
                if vals.get('picking_type_id',False) :
                    pick_type = picking.browse(vals.get('picking_type_id')) 
                if picking_type.code in ('incoming','outgoing') or picking_type.name in ('Consume','Receipt from Production'):
                    vals['is_bea_cukai'] = True
                elif picking_type.code == 'internal' and not pick.subcontract_id:
                    vals['is_bea_cukai'] = False
                elif pick.subcontract_id: 
                    vals['is_bea_cukai'] = True
            # cek company moves
            for line in pick.move_ids_without_package.filtered(lambda pi:pi.company_id.id != pick.company_id.id) :
                line.write({'company_id':pick.company_id.id})
            # isi kemasan
            # product_ids = pick.move_ids_without_package.mapped('product_id')
            # for prod in product_ids :
            #     if not pick.kemasan_ids :
            #         self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
            #     elif self.kemasan_ids.filtered(lambda p:p.product_id.id != prod.id) :
            #         pick.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
        _super = super(StockPicking, self).write(vals)
        return _super

    @api.multi
    def action_assign(self):
        res = super(StockPicking, self).action_assign()
        for pick in self :
            if pick.subcontract_id :
                if pick.subcontract_id.return_type_id.id == pick.picking_type_id.id:
                    for move in pick.move_ids_without_package :
                        if move.production_subcon_id:
                            lot = self.env['stock.production.lot'].sudo().search([('name', '=', move.production_subcon_id.name)],limit=1)
                            if lot :
                                first_line = False
                                for line in move.move_line_ids:
                                    if not line.lot_id :
                                        line.lot_id = lot.id
                                    if line.product_uom_qty == 0.0 :
                                        if not first_line :
                                            line.product_uom_qty = move.reserved_availability
                                            first_line = True
                                    line.qty_done = line.product_uom_qty
                            move.box = move.production_subcon_id.jml_box

            # if self.subcontract_id.picking_type_id.id == self.picking_type_id.id:
            #     purchase = self.env['purchase.order']
            #     picking = self.env['stock.picking']
            #     po_exist = purchase.sudo().search([('origin','=',self.name)])
            #     if not po_exist :
            #         if not self.partner_id.transaction_type :
            #             raise UserError(_('Default transaction type partner belum di set !'))
            #         # create po detail
            #         po_datas = {'product_id'   : self.subcontract_id.product_id.id,
            #                     'product_uom': self.subcontract_id.product_id.uom_po_id.id,
            #                     'date_planned': fields.Datetime.now(),
            #                     'name'      : 'Subcon '+ self.subcontract_id.name,
            #                     'product_qty' : sum(self.move_ids_without_package.mapped('netto')),
            #                     'price_unit' : self.subcontract_id.cost_per_qty,}
            #         # create PO
            #         po_id = purchase.sudo().create({'partner_id'    : self.partner_id.id,
            #                                         'company_id'    : self.company_id.id,
            #                                         'origin'        : self.name,
            #                                         'picking_type_id' : self.picking_type_id.id,
            #                                         'purchase_type'  : self.partner_id.transaction_type,
            #                                         'order_line'    : [(0,0, po_datas)]})
            #         po_id.button_confirm()
            #         self.origin = po_id.name
            #     self.origin = po_exist.name
        return res

    @api.multi
    def button_validate(self):
        # for mv in self.move_ids_without_package:
        #     # for mline in mv.move_line_ids.filtered(lambda pick:pick.picking_id == False) :
        #     for mline in mv.move_line_ids:
        #         if not mline.picking_id :
        #             mline.picking_id = self.id
        #             self.env.cr.commit()               
        #             #not_assign_picking.write({'picking_id':self.id})
        #     if mv.quantity_done == 0.0 :
        #         qty_line = sum(mv.move_line_ids.mapped('qty_done'))
        #         if qty_line > 0.0:
        #             mv.quantity_done = qty_line
        #             self.env.cr.commit() 
        res = super(StockPicking, self).button_validate()
        # if self.subcontract_id and self.subcontract_id.picking_type_id.id == self.picking_type_id.id:
        #     purchase = self.env['purchase.order']
        #     picking = self.env['stock.picking']
        #     po_exist = purchase.sudo().search([('origin','=',self.name)])
        #     if not po_exist :
        #         if not self.partner_id.transaction_type :
        #             raise UserError(_('Default transaction type partner belum di set !'))
        #         # create po detail
        #         po_datas = {'product_id'   : self.subcontract_id.product_id.id,
        #                     'product_uom': self.subcontract_id.product_id.uom_po_id.id,
        #                     'date_planned': fields.Datetime.now(),
        #                     'name'      : 'Subcon '+ self.subcontract_id.name or ''+'#'+self.wo_subcon_id.name or '',
        #                     'product_qty' : sum(self.move_ids_without_package.mapped('netto')),
        #                     'price_unit' : self.subcontract_id.cost_per_qty,}
        #         # create PO
        #         po_id = purchase.sudo().create({'partner_id'    : self.partner_id.id,
        #                                         'company_id'    : self.company_id.id,
        #                                         'origin'        : self.name,
        #                                         'picking_type_id' : self.picking_type_id.id,
        #                                         'purchase_type'  : self.partner_id.transaction_type,
        #                                         'order_line'    : [(0,0, po_datas)]})
        #         po_id.button_confirm()
        #         self.origin = po_id.name
        #     else :
        #         self.origin = po_exist.name
        return res

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        
        for i in self :
            date_done = i.date_done
            if i.is_bea_cukai and i.is_subcontracting and not i.subcontract_id:
                raise UserError(_('No contract subcon belum diisi !'))
            if i.subcontract_id and i.company_id.bea_cukai:
                i.is_bea_cukai = True

            # if i.subcontract_id and i.subcontract_id.picking_type_id.id == i.picking_type_id.id:
            #     # buat receive
            #     return_id = i.copy({'location_id':i.location_dest_id.id,
            #                     'location_dest_id':i.location_id.id,
            #                     'picking_type_id':i.subcontract_id.return_type_id.id,
            #                     'origin': i.name,
            #                     'is_bea_cukai':True})
            #     return_id.action_confirm()
            #     return_id.action_assign()
            if date_done:
                i.date_done = date_done
                for mv in i.move_ids_without_package :
                    mv.date = date_done
                    for mvl in mv.move_line_ids:
                        mvl.date = date_done
            aju = False
            if i.subcontract_id:
                # create no aju
                if not i.nomor_aju_id and i.company_id.bea_cukai:
                    kppbc = False
                    if i.date_done:
                        dt_y = i.date_done.year
                        dt_m = '{:02d}'.format(i.date_done.month)
                        dt_d = '{:02d}'.format(i.date_done.day)
                        date = str(dt_y) + str(dt_m) + str(dt_d)
                        date_p = str(i.date_done)
                    else:
                        dt = datetime.now().date()
                        dt_y = dt.year
                        dt_m = '{:02d}'.format(dt.month)
                        dt_d = '{:02d}'.format(dt.day)
                        date = str(dt_y) + str(dt_m) + str(dt_d)
                        date_p = str(datetime.now().date())

                    i.tgl_dokumen = date_p
                    i.tgl_ttd = date_p
                    #jika interco
                    if i.subcontract_id.is_interco:
                        if not i.company_id.bea_cukai :
                            if i.subcontract_id.picking_type_id.id == i.picking_type_id.id :
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.1'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                                aju_name = '4.1'
                                if not aju:
                                    if i.subcontract_id.picking_type_id.picking_type_interco_id.id == i.picking_type_id.id :
                                        aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.1'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                                        # aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1'),
                                        #                                 ('company_id','=',i.company_id.id)], limit=1)
                                        # aju_name = '2.6.1'
                                    if not aju:
                                        raise ValidationError(_('Master aju BC %s belum dibuat !')%(aju_name))
                            if i.subcontract_id.return_type_id.id == i.picking_type_id.id :
                                aju_name = '4.0'
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.0'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                            if i.subcontract_id.picking_type_id.picking_type_interco_id.id == i.picking_type_id.id :
                                aju_name = '4.1'
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.1'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                            if i.subcontract_id.interco_picking_type_id.picking_type_interco_id.id == i.picking_type_id.id :
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.0'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                                aju_name = '4.0'
                                # aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2'),
                                #                                         ('company_id','=',i.company_id.id)], limit=1)
                        elif i.company_id.bea_cukai :
                            if i.subcontract_id.picking_type_id.id == i.picking_type_id.id :
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                                aju_name = '2.6.1'
                                if not aju:
                                    if i.subcontract_id.picking_type_id.picking_type_interco_id.id == i.picking_type_id.id :
                                        aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                                        # aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1'),
                                        #                                 ('company_id','=',i.company_id.id)], limit=1)
                                        # aju_name = '2.6.1'
                                    if not aju:
                                        raise ValidationError(_('Master aju BC %s belum dibuat !')%(aju_name))
                            if i.subcontract_id.return_type_id.id == i.picking_type_id.id :
                                aju_name = '2.6.2'
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                            if i.subcontract_id.picking_type_id.picking_type_interco_id.id == i.picking_type_id.id :
                                aju_name = '2.6.1'
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                            if i.subcontract_id.interco_picking_type_id.picking_type_interco_id.id == i.picking_type_id.id :
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                                aju_name = '2.6.2'
                        if not aju:
                            aju_name = '2.6.2'
                            if i.subcontract_id.interco_picking_type_id.id == i.picking_type_id.id :
                                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.0'),
                                                                        ('company_id','=',i.company_id.id)], limit=1)
                            if not aju:
                                raise ValidationError(_('Master aju BC %s belum dibuat !')%(aju_name))
                    else:
                        if i.subcontract_id.picking_type_id.id == i.picking_type_id.id :
                            aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.1'),
                                                                    ('company_id','=',i.company_id.id)], limit=1)
                            if not aju:
                                raise ValidationError(_('Master aju BC 2.6.1 belum dibuat !'))
                        if i.subcontract_id.return_type_id.id == i.picking_type_id.id :
                            aju = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2'),
                                                                    ('company_id','=',i.company_id.id)], limit=1)
                        if not aju:
                            raise ValidationError(_('Master aju BC 2.6.2 belum dibuat !'))

                    if aju.kode_dokumen_bc:
                        kppbc = str(aju.kode_dokumen_bc)
                    else :
                        kppbc = str(aju.kppbc)
                    if not kppbc :
                        raise ValidationError(_('KPPBC BC 2.6.1 / 2.6.2  belum di set !'))
                    id_modul = str(aju.id_modul)
                    if not id_modul :
                        raise ValidationError(_('Id module BC 2.6.1 / 2.6.2  belum di set !'))
                    obj_aju = self.env['vit.nomor.aju'].search([('company_id','=',i.company_id.id),('name','like',kppbc+'%'),('has_export','=',False)],order="name desc", limit=1)
                    if obj_aju :
                        count = len(obj_aju.picking_ids)
                        if count < 5:
                            types = []
                            partners = []
                            for z in obj_aju.picking_ids:
                                if z.partner_id and z.partner_id.id != i.partner_id.id :
                                    partners.append(z.partner_id.id)
                                if z.picking_type_id.code not in types:
                                    types.append(z.picking_type_id.code)
                            # jika picking type sama dan partner nya sama semua maka bisa pake nomor aju ini
                            if len(types) == 1 and len(partners) < 1:
                                i.nomor_aju_id = obj_aju.id

                    if kppbc and not i.nomor_aju_id:
                        inc_aju = str(self.env['ir.sequence'].next_by_code('increment_no_aju'))
                        id_aju = self.env['vit.nomor.aju'].create({'name': kppbc + '0' + id_modul + date + inc_aju})
                        i.nomor_aju_id = id_aju
        
            # interco pengiriman pertama
            if i.subcontract_id and i.subcontract_id.is_interco and i.subcontract_id.picking_type_id.id == i.picking_type_id.id :
                new_pick = i.create_picking_interco(i.subcontract_id.interco_picking_type_id)
                i.picking_interco_id = new_pick.id
                po_id = i.create_po_interco()
                if i.company_id.bea_cukai:
                    nomor_aju = self.create_aju_interco_4041()
                    if nomor_aju :
                        i.nomor_aju_id = nomor_aju.id
            # interco pengiriman kedua (balik)
            elif i.wo_subcon_id.contract_id and i.wo_subcon_id.contract_id.is_interco:
                if not i.wo_subcon_id.contract_id.picking_type_id.picking_type_interco_id:
                    raise ValidationError(_('Default picking type interco sisi lawan pada contract belum di set (contract --> picking type --> picking type interco)!'))
                if i.wo_subcon_id.contract_id.picking_type_id.picking_type_interco_id == i.picking_type_id :
                    if not i.wo_subcon_id.contract_id.interco_picking_type_id.picking_type_interco_id:
                        raise ValidationError(_('Default picking type interco sisi lawan pada contract belum di set (contract --> Picking Type (Interco) --> Picking Type Interco)!'))
                    new_pick = i.create_picking_interco(i.wo_subcon_id.contract_id.interco_picking_type_id.picking_type_interco_id)
                    i.picking_interco_id = new_pick.id
                    inv_id = i.create_invoice(i.wo_subcon_id.contract_id)
                    inv_id.action_invoice_open()
                    if i.company_id.bea_cukai:
                        nomor_aju = self.create_aju_interco_4041()
                        if nomor_aju :
                            i.nomor_aju_id = nomor_aju.id
        return res

    def create_aju_interco_4041(self):
        # create no aju
        nomor_aju = False
        if not self.nomor_aju_id :
            kppbc = False
            if self.date_done:
                dt_y = self.date_done.year
                dt_m = '{:02d}'.format(self.date_done.month)
                dt_d = '{:02d}'.format(self.date_done.day)
                date = str(dt_y) + str(dt_m) + str(dt_d)
                date_p = str(self.date_done)
            else:
                dt = datetime.now().date()
                dt_y = dt.year
                dt_m = '{:02d}'.format(dt.month)
                dt_d = '{:02d}'.format(dt.day)
                date = str(dt_y) + str(dt_m) + str(dt_d)
                date_p = str(datetime.now().date())

            self.tgl_dokumen = date_p
            self.tgl_ttd = date_p
            if self.subcontract_id.picking_type_id.id == self.picking_type_id.id :
                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.1'),
                                                        ('company_id','=',self.company_id.id)], limit=1)
                if not aju:
                    raise ValidationError(_('Master aju BC 4.1 belum dibuat !'))
            if self.subcontract_id.return_type_id.id == self.picking_type_id.id :
                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.0'),
                                                        ('company_id','=',self.company_id.id)], limit=1)
                if not aju:
                    raise ValidationError(_('Master aju BC 4.0 belum dibuat !'))
            # untik auto interco
            if self.subcontract_id.interco_picking_type_id and self.subcontract_id.interco_picking_type_id.picking_type_interco_id and self.subcontract_id.interco_picking_type_id.picking_type_interco_id.id == self.picking_type_id.id :
                aju = self.env['vit.export.import.bc'].search([('name','=','BC 4.0'),
                                                        ('company_id','=',self.company_id.id)], limit=1)
                if not aju:
                    raise ValidationError(_('Master aju BC 4.0 (interco) belum dibuat !'))
            if aju.kode_dokumen_bc:
                kppbc = str(aju.kode_dokumen_bc)
            else :
                kppbc = str(aju.kppbc)
            if not kppbc :
                raise ValidationError(_('KPPBC BC 4.0 / 4.1 belum di set !'))
            id_modul = str(aju.id_modul)
            if not id_modul :
                raise ValidationError(_('Id module BC 4.0 / 4.1 belum di set !'))

            obj_aju = self.env['vit.nomor.aju'].search([('company_id','=',self.company_id.id),('has_export','=',False)],order="name desc", limit=1)
            if obj_aju :
                count = len(obj_aju.picking_ids)
                if count < 5:
                    types = []
                    partners = []
                    for z in obj_aju.picking_ids:
                        if z.partner_id and z.partner_id.id != self.partner_id.id :
                            partners.append(z.partner_id.id)
                        if z.picking_type_id.code not in types:
                            types.append(z.picking_type_id.code)
                    # jika picking type sama dan partner nya sama semua maka bisa pake nomor aju ini
                    if len(types) == 1 and len(partners) < 1:
                        nomor_aju= obj_aju

            if kppbc and not self.nomor_aju_id:
                inc_aju = str(self.env['ir.sequence'].next_by_code('increment_no_aju'))
                id_aju = self.env['vit.nomor.aju'].create({'name': kppbc + '0' + id_modul + date + inc_aju})
                nomor_aju = id_aju
        return nomor_aju

    subcontract_id = fields.Many2one('mrp.subcontract','Subcontract',copy=False)
    wo_subcon_id = fields.Many2one('mrp.workorder.subcon','WO Subcon',copy=False)
    wo_subcon_ids = fields.Many2many('mrp.workorder.subcon',string='WO Subcon',copy=False)
    is_subcontracting = fields.Boolean('Is Subcontracting',copy=False)
    picking_interco_id = fields.Many2one('stock.picking','Picking Interco',copy=False)
    picking_type_interco_id = fields.Many2one('stock.picking.type','Manual Interco',related='picking_type_id.picking_type_interco_id', store=True)
    is_manual_trf= fields.Boolean('Manual Transfer Subcon',copy=False)
    barcode = fields.Char('Barcode', help="Jika scan otomatis bermasalah, maka letakan kursor disini",readonly=True,states={'draft': [('readonly', False)]})
    barcode2 = fields.Char('Barcode 2', help="Jika scan otomatis bermasalah, maka letakan kursor disini",readonly=True,states={'draft': [('readonly', False)]})
    warning = fields.Text('Warning', help="warning message")
    is_incoming_subcon = fields.Boolean('Incoming Subcon', copy=False, default=False)
    retur = fields.Boolean('Return', copy=False, track_visibility='onchange')
    remark = fields.Char('Remark', copy=False, track_visibility='onchange', size=100)
    remark_id = fields.Many2one('stock.remark','Remark',track_visibility='onchange')
    waktu = fields.Selection([('Pagi','Pagi'),('Dhuha','Dhuha'),('Siang','Siang'),('Sore','Sore'),('Malam','Malam')],'Waktu')
    delivery_date = fields.Date('Delivery Date', copy=False, track_visibility='onchange')
    date_of_return = fields.Date('Date of Return', copy=False, track_visibility='onchange')
    scan_compare_ids = fields.One2many('stock.scan.compare','picking_id','Scan Compare')

    def search_subcon_header(self, production_subcon_id):
        stock_move = self.env['stock.move']
        move_out = stock_move.sudo().search([('production_subcon_id','=',production_subcon_id.id),
                                            ('picking_id.name','like','OUT'),
                                            ('company_id','=',self.company_id.id),
                                            ('state','!=','cancel')], order='create_date desc', limit=1)
        if move_out:
            self.partner_id = move_out.picking_id.partner_id.id
            self.onchange_wo_partner_id()
            self.subcontract_id = move_out.picking_id.subcontract_id.id
            self.onchange_subcontract_id()
            self.wo_subcon_id = move_out.picking_id.wo_subcon_id.id
            self.wo_subcon_ids = move_out.picking_id.wo_subcon_ids.ids
            self.onchange_wo_subcon_ids()
            self.picking_type_id = move_out.picking_id.subcontract_id.return_type_id.id
            self.scheduled_date = fields.datetime.now()
            return True
        return False

    def _insert_mo_scan(self, production_id):
        if production_id not in self.scan_compare_ids.mapped('production_id'):
            vals = {'production_id':production_id.id,
                        'name':production_id.product_id.name}
            new_scan_compare_ids = self.scan_compare_ids.new(vals)
            self.scan_compare_ids |= new_scan_compare_ids
        for line in self.move_ids_without_package.filtered(lambda mo:mo.production_subcon_id.id == production_id.id):
            line.update({'has_scan_mo' : True})


    @api.onchange('barcode','barcode2')
    def onchange_barcode(self):
        if self.barcode or self.barcode2:
            barcode = self.barcode or self.barcode2
            new_barcode = barcode.split('#')[0]
            production = self.env['mrp.production'].sudo().search([('name','=',new_barcode)],limit=1)
            if not production :
                self.warning = "Lot %s tidak ditemukan !"%(new_barcode)
                self.barcode = False
                return
            if self.state == 'assigned' :
                self._insert_mo_scan(production)
                self.barcode = False
                self.barcode2 = False
                self.warning = False
            else :
                # barcode_scan = self.barcode.split('#')
                # barcode = barcode_scan[0].strip()
                # poly = False
                # if len(barcode_scan) > 1 :
                # poly = barcode_scan[1].strip()
                # if not poly :
                #     poly = production.no_poly_box.strip()

                # isi otomatis header (picking)
                data_header = self.search_subcon_header(production)
                if not data_header:
                    self.warning = "Item lot %s (%s) tidak ditemukan di dokumen subcon out !"%(new_barcode, production.product_id.name)
                    self.barcode = False
                    self.barcode2 = False
                    return
                # cek jika item yg di scan tidak ada dalam contract
                item_contract_exist = self.subcontract_id.product_ids.filtered(lambda i:i.product_id.id == production.product_id.id)
                if not item_contract_exist :
                    self.warning = "Item lot %s (%s) tidak ditemukan di contract %s !"%(new_barcode, production.product_id.name, self.subcontract_id.name)
                    self.barcode = False
                    self.barcode2 = False
                    return
                price_unit = item_contract_exist[0].cost_per_qty
                wo_subcon = self.env['mrp.workorder.subcon']
                wo_subcon_exist = wo_subcon.sudo().search([('contract_id','=',self.subcontract_id.id),
                                                                ('company_id','=',self.company_id.id)])
                if not wo_subcon_exist:
                    self.barcode = False
                    self.barcode2 = False
                    self.warning = "WO subcon contract %s tidak ditemukan !"%(self.subcontract_id.name)
                    return
                product_id = False
                for wo_line in wo_subcon_exist:
                    lot_exist = wo_line.workorder_ids.filtered(lambda x:x.production_id.name == new_barcode)
                    if lot_exist :
                        product_id = lot_exist[0].product_id
                        break
                if not product_id:
                    self.warning = "Item %s tidak ditemukan di WO subcon manapun !"%(self.production.product_id.name)
                    self.barcode = False
                    self.barcode2 = False
                    return
                mo_exist = self.env['stock.move'].sudo().search([('production_subcon_id','=',production.id),
                                                                        ('picking_id.is_incoming_subcon','=',True),
                                                                        ('state','!=','cancel'),
                                                                        ('picking_id.partner_id','=',self.partner_id.id),
                                                                        ('location_id','=',self.location_id.id),
                                                                        ('company_id','=',self.company_id.id)],limit=1)
                if mo_exist :
                    self.warning = 'Lot %s sudah pernah diinput di dokumen %s ' % (production.name,mo_exist.picking_id.name)
                    self.barcode = False
                    self.barcode2 = False
                    # raise Warning('Lot %s sudah pernah diinput di dokumen %s ' % (production.name,mo_exist.picking_id.name))

                vals = {'product_id':product_id.id,
                        'name':production.product_id.name,
                        'origin': production.name,
                        'product_uom':production.product_uom_id.id,
                        'product_uom_qty':production.product_qty,
                        'price_unit': price_unit,
                        'netto':0.1,
                        'state':'draft',
                        'picking_type_id':self.picking_type_id.id,
                        'location_id':self.location_id.id,
                        'location_dest_id':self.location_dest_id.id,
                        'company_id': self.company_id.id,
                        'date_expected':self.scheduled_date,
                        'production_subcon_id':production.id,
                        'workorder_id': lot_exist[0].id,
                        'subcontract_id':self.subcontract_id.id
                                        }
                if self.move_ids_without_package.filtered(lambda i:i.workorder_id.id == lot_exist[0].id) :
                    self.barcode = False
                    self.barcode2 = False
                    self.warning = "Barcode %s sudah diinsert/scan !"%(production.name)
                    return
                #     raise Warning('Barcode (%s) sudah diinsert/scan (%s)' % (production.name,barcode))
                self._insert_mo_scan(production)                 
                new_move_ids_without_package = self.move_ids_without_package.new(vals)
                self.move_ids_without_package |= new_move_ids_without_package
                self.wo_subcon_ids =  [(4, wo_line.id)] 
                self.barcode = False
                self.barcode2 = False
                self.warning = False

    @api.multi
    def create_invoice(self,contract_id):
        self.ensure_one()
        if not contract_id.product_id.categ_id.property_account_income_categ_id:
            raise ValidationError(_('account income pada product ( %s ) belum di set!!') % contract_id.product_id.name)
        journal = self.env['account.journal'].sudo().search([('company_id','=',self.company_id.id),('type','=','sale')], limit=1)
        acc_line_id = contract_id.product_id.categ_id.property_account_income_categ_id.id
        move_lines = []
        for line in self.move_ids_without_package :
            product_exist = contract_id.product_ids.filtered(lambda prod:prod.product_id.id == line.product_id.id and prod.cost_per_qty > 0.0)
            if not product_exist :
                raise UserError(_('Product %s tidak ditemukan di kontrak %s atau cost per qty bernilai nol !')%(line.product_id.name,contract_id.name))
            move_lines.append((0,0,{
                    'product_id'        : line.product_id.id,
                    'price_unit'        : product_exist[0].cost_per_qty,
                    'quantity'          : line.netto_wire if line.netto_wire > 0.0 else line.netto,
                    'name'              : contract_id.product_id.name + ' | ' +line.product_id.name,
                    'account_id'        : acc_line_id,
                    'invoice_line_tax_ids': False,
                    }))
        data = {
                'partner_id': self.partner_id.id,
                'journal_id': journal.id,
                'account_id': self.partner_id.property_account_receivable_id.id,
                'invoice_line_ids': move_lines,
                'type': 'out_invoice',
                'origin': self.name,
                'user_id': self.env.user.id,
                'company_id': self.company_id.id,
                'date_invoice' :str(self.date_done)[:10],
                'reference': self.subcontract_id.name,
                'currency_id': self.env.ref('base.main_company').currency_id.id
                }
        inv = self.env['account.invoice'].sudo().create(data)
        return inv

    @api.multi
    def create_picking_interco(self,picking_type_id):
        if not picking_type_id.warehouse_id  :
            raise UserError(_('Warehouse pada picking type interco belum di set'))
        new_picking = self.copy({'picking_type_id' :picking_type_id.id,
                                'location_id' : picking_type_id.default_location_src_id.id,
                                'location_dest_id' : picking_type_id.default_location_dest_id.id,
                                'picking_interco_id' : self.id,
                                'wo_subcon_ids' : [(6, 0, self.wo_subcon_ids.ids)],
                                'wo_subcon_id' : self.wo_subcon_id.id,
                                'subcontract_id' : self.subcontract_id.id,
                                'is_subcontracting' : True,
                                'is_incoming_subcon': True,
                                'company_id': picking_type_id.warehouse_id.company_id.id})
        for line in new_picking.move_ids_without_package:
            if line.company_id.id != new_picking.company_id.id :
                line.company_id = new_picking.company_id.id
        new_picking.action_confirm()
        new_picking.action_assign()
        return new_picking

    @api.multi
    def create_po_interco(self):
        purchase = self.env['purchase.order']
        picking = self.env['stock.picking']
        po_exist = purchase.sudo().search([('origin','=',self.name)])
        if not po_exist :
            if not self.partner_id.transaction_type :
                raise UserError(_('Default transaction type partner %s belum di set !')%(self.partner_id.name))
            # create po detail
            po_datas = []
            for detail in self.move_ids_without_package:
                product_exist = self.subcontract_id.product_ids.filtered(lambda prod:prod.product_id.id == detail.product_id.id and prod.cost_per_qty > 0.0)
                if not product_exist :
                    raise UserError(_('Product %s tidak ditemukan di kontrak %s atau cost per qty bernilai nol !')%(detail.product_id.name,self.subcontract_id.name))
                po_datas.append((0,0,{'product_id'   : self.subcontract_id.product_id.id,
                                    'product_uom'   : detail.product_uom.id ,#self.subcontract_id.product_id.uom_po_id.id,
                                    'date_planned'  : fields.Datetime.now(),
                                    'name'          : self.subcontract_id.product_id.name +' | '+ detail.product_id.name,
                                    'product_qty'   : detail.netto_wire if detail.netto_wire > 0.0 else detail.netto,#sum(self.move_ids_without_package.mapped('netto')),
                                    'price_unit'    : product_exist[0].cost_per_qty}))#self.subcontract_id.cost_per_qty,})
            # create PO
            po_id = purchase.sudo().create({'name'          : self.wo_subcon_id.name+'-PO',
                                            'partner_id'    : self.partner_id.id,
                                            'company_id'    : self.company_id.id,
                                            'origin'        : 'Subcon '+ self.subcontract_id.name+' # '+self.name if self.subcontract_id.name else self.subcontract_id.nomor+'#'+self.wo_subcon_id.name or '' +' # '+self.name,
                                            'picking_type_id' : self.picking_type_id.id,
                                            'purchase_type'  : self.partner_id.transaction_type,
                                            'order_line'    : po_datas})
            po_id.button_confirm()
            self.origin = po_id.name
        else :
            self.origin = po_exist.name

    @api.onchange('subcontract_id')
    def onchange_subcontract_id(self):
        if self.subcontract_id:
            if self.subcontract_id.company_id != self.company_id and self.subcontract_id.is_interco:
                self.picking_type_id = self.subcontract_id.interco_picking_type_id.id
                self.location_id = self.subcontract_id.interco_picking_type_id.default_location_src_id.id
                self.location_dest_id = self.subcontract_id.interco_picking_type_id.default_location_dest_id.id
                self.partner_id = self.subcontract_id.partner_id.id
                self.is_incoming_subcon = False
                if self.subcontract_id.interco_picking_type_id.picking_type_interco_id :
                    self.picking_type_id = self.subcontract_id.interco_picking_type_id.picking_type_interco_id.id
                    self.location_id = self.subcontract_id.interco_picking_type_id.picking_type_interco_id.default_location_src_id.id
                    self.location_dest_id = self.subcontract_id.interco_picking_type_id.picking_type_interco_id.default_location_dest_id.id

            else :
                self.partner_id = self.subcontract_id.partner_id.id
                self.picking_type_id = self.subcontract_id.picking_type_id.id
                self.location_id = self.subcontract_id.picking_type_id.default_location_src_id.id
                self.location_dest_id = self.subcontract_id.picking_type_id.default_location_dest_id.id

            active_form = self._context.get('params', False)
            if active_form :
                if 'model' in active_form and 'id' in active_form :
                    if active_form['model'] == 'mrp.subcontract':
                        subcon = self.env['mrp.subcontract'].browse(active_form['id'])
                        self.picking_type_id = subcon.return_type_id.id
                        self.location_id = subcon.return_type_id.default_location_dest_id.id
                        self.location_dest_id = subcon.return_type_id.default_location_src_id.id
                        self.partner_id = subcon.partner_id.id
                        self.is_incoming_subcon = True
            elif self._context.get('active_model', False) :
                active_form = self._context.get('active_model', False)
                if active_form == 'mrp.subcontract' :
                    active_id = self._context.get('active_id', False)
                    if active_id :
                        subcon = self.env['mrp.subcontract'].browse(active_id)
                        self.picking_type_id = subcon.return_type_id.id
                        self.location_id = subcon.return_type_id.default_location_dest_id.id
                        self.location_dest_id = subcon.return_type_id.default_location_src_id.id
                        self.partner_id = subcon.partner_id.id
                        self.is_incoming_subcon = True
            else :
                if self.is_subcontracting and self.is_incoming_subcon and self.env.ref('vit_subcontractor.vit_subcontractor_action_in') :
                    self.picking_type_id = self.subcontract_id.return_type_id.id
                    self.location_id = self.subcontract_id.return_type_id.default_location_dest_id.id
                    self.location_dest_id = self.subcontract_id.return_type_id.default_location_src_id.id
                    self.partner_id = self.subcontract_id.partner_id.id
                    self.is_incoming_subcon = True
                elif self.is_subcontracting and not self.is_incoming_subcon and self.env.ref('vit_subcontractor.vit_subcontractor_action') :
                    if self.subcontract_id.company_id == self.company_id and not self.subcontract_id.is_interco:
                        self.picking_type_id = self.subcontract_id.picking_type_id.id
                        self.location_id = self.subcontract_id.picking_type_id.default_location_dest_id.id
                        self.location_dest_id = self.subcontract_id.picking_type_id.default_location_src_id.id
                        self.partner_id = self.subcontract_id.partner_id.id
                        self.is_incoming_subcon = False
                    else :
                        self.picking_type_id = self.subcontract_id.interco_picking_type_id.id
                        self.location_id = self.subcontract_id.interco_picking_type_id.default_location_src_id.id
                        self.location_dest_id = self.subcontract_id.interco_picking_type_id.default_location_dest_id.id
                        self.partner_id = self.subcontract_id.partner_id.id
                        self.is_incoming_subcon = False
                        if self.subcontract_id.interco_picking_type_id.picking_type_interco_id:
                            self.picking_type_id = self.subcontract_id.interco_picking_type_id.picking_type_interco_id.id
                            self.location_id = self.subcontract_id.interco_picking_type_id.picking_type_interco_id.default_location_src_id.id
                            self.location_dest_id = self.subcontract_id.interco_picking_type_id.picking_type_interco_id.default_location_dest_id.id
                

            # #if not self.partner_id :
            # wo_ids = self.env['mrp.workorder'].search([('is_subcontracting', '=', True),
            #     ('production_id.company_id','=',self.company_id.id),
            #     ('workcenter_id','=',self.subcontract_id.workcenter_id.id),
            #     ('state','not in',('cancel','done')),
            #     ('stock_move_id','=',False)])
            # data = []

            # self.move_ids_without_package = False
            # if not self.scheduled_date :
            #     dates = str(date.today())
            # else :
            #     dates = self.scheduled_date
            # if wo_ids :
            #     self.move_ids_without_package = []
            # for wo in wo_ids :
            #     #import pdb;pdb.set_trace()
            #     if wo.product_id.id in self.subcontract_id.product_ids.mapped('product_id.id'):
            #         data.append((0,0, {'product_id':wo.product_id.id,
            #                             'name':wo.production_id.name,
            #                             'workorder_id':wo.id,
            #                             'subcontract_id':self.subcontract_id.id,
            #                             'product_uom':wo.product_uom_id.id,
            #                             'product_uom_qty':wo.qty_production,
            #                             'netto':0.1,
            #                             'state':'draft',
            #                             'production_subcon_id':wo.production_id.id,
            #                             'picking_type_id':self.picking_type_id.id,
            #                             'location_id':self.location_id.id,
            #                             'location_dest_id':self.location_dest_id.id,
            #                             'company_id': wo.production_id.company_id.id,
            #                             'date_expected':dates,
            #                             }))  
            # self.move_ids_without_package = data

    @api.onchange('wo_subcon_ids')
    def onchange_wo_subcon_ids(self):
        if self.wo_subcon_ids and not self.is_incoming_subcon:
            self.wo_subcon_id = self.wo_subcon_ids[0].id
            if self.is_subcontracting :
                self.partner_id = self.wo_subcon_ids[0].partner_id.id
                self.subcontract_id = self.wo_subcon_ids[0].contract_id.id
                self.picking_type_id = self.wo_subcon_ids[0].contract_id.picking_type_id.id
                self.location_id = self.wo_subcon_ids[0].contract_id.picking_type_id.default_location_src_id.id
                self.location_dest_id = self.wo_subcon_ids[0].contract_id.picking_type_id.default_location_dest_id.id
                self.is_bea_cukai = True
                # # cari WO di company yg sama
                # wo_ids = self.env['mrp.workorder'].search([('is_subcontracting', '=', True),
                #     ('production_id.company_id','=',self.company_id.id),
                #     ('workcenter_id','in',self.wo_subcon_id.contract_id.workcenter_ids.ids),
                #     ('state','=','ready'),
                #     ('stock_move_id','=',False)])
            
            if self.is_manual_trf :
                pick_type = self.env['stock.picking.type'].sudo().search([('warehouse_id.company_id','=',self.company_id.id),('picking_type_interco_id','!=',False)],limit=1)
                if pick_type :
                    if self.wo_subcon_ids[0].contract_id.company_id.id != self.company_id.id :
                        self.picking_type_id = self.wo_subcon_ids[0].contract_id.picking_type_id.picking_type_interco_id.id
                        self.location_id = self.wo_subcon_ids[0].contract_id.picking_type_id.picking_type_interco_id.default_location_src_id.id
                        self.location_dest_id = self.wo_subcon_ids[0].contract_id.picking_type_id.picking_type_interco_id.default_location_dest_id.id                        
                    else :
                        self.picking_type_id = pick_type.picking_type_interco_id.id
                        self.location_id = pick_type.picking_type_interco_id.default_location_src_id.id
                        self.location_dest_id = pick_type.picking_type_interco_id.default_location_dest_id.id
            # cari WO di company sebelah
            wo_ids = self.wo_subcon_ids[0].workorder_ids
            active_form = self._context.get('params', False)
            if active_form :
                if 'model' in active_form and 'id' in active_form :
                    if active_form['model'] == 'mrp.subcontract':
                        subcon = self.env['mrp.subcontract'].browse(active_form['id'])
                        self.picking_type_id = subcon.return_type_id.id
                        self.location_id = subcon.picking_type_id.default_location_dest_id.id
                        self.location_dest_id = subcon.picking_type_id.default_location_src_id.id
                        self.partner_id = subcon.partner_id.id
            else :
                active_form = self._context.get('active_model', False)
                if active_form and active_form == 'mrp.subcontract' :
                    active_id = self._context.get('active_id', False)
                    if active_id :
                        subcon = self.env['mrp.subcontract'].browse(active_id)
                        self.picking_type_id = subcon.return_type_id.id
                        self.location_id = subcon.picking_type_id.default_location_dest_id.id
                        self.location_dest_id = subcon.picking_type_id.default_location_src_id.id
                        self.partner_id = subcon.partner_id.id            

            if not self.scheduled_date :
                dates = str(date.today())
            else :
                dates = self.scheduled_date

            self.move_ids_without_package = False
            if wo_ids :
                self.move_ids_without_package = []
            data = []
            for wo in wo_ids :
                #loop workorder_subcon_ids nya
                for sub in self.wo_subcon_ids :
                    if wo.product_id.id in sub.contract_id.product_ids.mapped('product_id.id'):
                        data.append((0,0, {'product_id':wo.product_id.id,
                                            'name':wo.production_id.name,
                                            'workorder_id':wo.id,
                                            'subcontract_id':sub.contract_id.id,
                                            'product_uom':wo.product_uom_id.id,
                                            'product_uom_qty':wo.qty_production,
                                            'netto':0.1,
                                            'state':'draft',
                                            'production_subcon_id':wo.production_id.id,
                                            'picking_type_id':self.picking_type_id.id,
                                            'location_id':self.location_id.id,
                                            'location_dest_id':self.location_dest_id.id,
                                            'company_id': wo.production_id.company_id.id,
                                            'date_expected':dates,
                                            }))  
                # else :
                #     raise UserError(_('Product %s belum terdaftar di contract subcon %s')%(wo.product_id.name,self.wo_subcon_id.name))
            self.move_ids_without_package = data 

    @api.onchange('partner_id','scheduled_date')
    def onchange_wo_partner_id(self):
        domain = []
        # tambah domain
        # if self.is_subcontracting :
        #     domain = {'domain': {'wo_subcon_id': [('company_id','=',self.company_id.id),('partner_id','=',self.partner_id.id),('contract_id.jt_tgl_subkontrak','>=',self.scheduled_date),('state','=','confirm')]}}
        # if self.is_manual_trf :
        #     wo_subcon = self.env['mrp.workorder.subcon'].sudo().search([('company_id','!=',self.company_id.id),('company_id.partner_id','=',self.partner_id.id),('contract_id.jt_tgl_subkontrak','>=',self.scheduled_date),('state','=','confirm')], limit=1)
        #     if wo_subcon :
        #         self.wo_subcon_id = wo_subcon.id
        #     domain = {'domain': {'wo_subcon_id': [('company_id','!=',self.company_id.id),('company_id.partner_id','=',self.partner_id.id),('contract_id.jt_tgl_subkontrak','>=',self.scheduled_date),('state','=','confirm')]}}
        # else :
        domain = {'domain': {'wo_subcon_ids': [('company_id','=',self.company_id.id),('partner_id','=',self.partner_id.id),('contract_id.jt_tgl_subkontrak','>=',self.scheduled_date),('state','=','confirm')]}}
        return domain

StockPicking()


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.onchange('product_id','production_subcon_id','picking_id.subcontract_id')
    def onchange_domain_product_id(self):
        domain = []
        if self.picking_id and self.picking_id.wo_subcon_id and self.picking_id.wo_subcon_id.contract_id.product_ids :
            # wo_ids = self.env['mrp.workorder'].search([('is_subcontracting', '=', True),
            #     ('production_id.company_id','=',self.company_id.id),
            #     ('workcenter_id','=',self.picking_id.subcontract_id.workcenter_id.id),
            #     ('stock_move_id','=',False)])

            wo_ids = self.picking_id.wo_subcon_id.contract_id.wip_ids
            if wo_ids :
                p_ids = []
                m_ids = []
                for wo in wo_ids.filtered(lambda w:w.state not in ('cancel','done')) :
                    if wo.product_id.id in self.picking_id.wo_subcon_id.contract_id.product_ids.mapped('product_id.id'):
                        p_ids.append(wo.product_id.id )
                        m_ids.append(wo.production_id.id)
                domain = {'domain': {'product_id': [('id', 'in', p_ids)],'production_subcon_id': [('id', 'in', m_ids)]}}
        if self.production_subcon_id :
            self.product_id = self.production_subcon_id.product_id.id
            self.product_uom_qty = self.production_subcon_id.product_qty
            if self.picking_id.scheduled_date :
                self.date_expected = self.picking_id.scheduled_date
            else :
                raise UserError(_('Scheduled date harus diisi terlebih dahulu !'))
        return domain

    @api.onchange('workorder_id')
    def onchange_workorder_id(self):
        if self._origin.workorder_id:
            self.workorder_id = self._origin.workorder_id.id

    @api.depends('product_id','picking_id.is_subcontracting')
    def _compute_netto_wire(self):
        for nett in self:
            if nett.production_subcon_id and nett.production_subcon_id.no_poly_box:
                #net = nett.production_subcon_id.move_raw_ids.filtered(lambda x:x.state == 'done' and x.product_id.categ_id.name in ('WIRE','Wire','wire'))
                net = int(nett.production_subcon_id.no_poly_box.strip()[-1:])*nett.production_subcon_id.product_id.kg_pal
                if net :
                    nett.netto_wire = net
                    # nett.netto_wire = sum(net.mapped('product_uom_qty'))

    workorder_id = fields.Many2one('mrp.workorder','Workorder', copy=True)
    production_subcon_id = fields.Many2one('mrp.production','Manufacturing Order Subcon', copy=True)
    subcontract_id = fields.Many2one('mrp.subcontract','Subcontract',related="picking_id.subcontract_id")
    netto_wire = fields.Float('Netto/Lot (KG)',compute='_compute_netto_wire', store=True)
    plt = fields.Char('PLT',related="product_id.plt")
    remark_id = fields.Many2one('stock.remark','Remark')
    has_scan_mo = fields.Boolean('Has Scan MO', default=False, copy=False)

    def _action_assign(self):
        res = super(StockMove, self)._action_assign()
        for move in self :
            if move.production_subcon_id :
                # cek wo yg di assign subcon tp blm ada stock_move_id
                wos = move.production_subcon_id.workorder_ids.filtered(lambda w:w.is_subcontracting and not w.stock_move_id)
                for wo in wos :
                    wo.stock_move_id = move.id
                lot = self.env['stock.production.lot'].sudo().search([('name', '=', move.production_subcon_id.name)],limit=1)
                if lot :
                    first_line = False
                    for line in move.move_line_ids:
                        cr = self.env.cr
                        cr.execute("update stock_move_line set lot_id=%s where id= %s ", (lot.id,line.id)  )
                        if not line.lot_id :
                            line.lot_id = lot.id
                        if line.product_uom_qty == 0.0 :
                            if not first_line :
                                line.product_uom_qty = move.reserved_availability
                                first_line = True
                        line.qty_done = line.product_uom_qty
                move.box = move.production_subcon_id.jml_box
        return res

    def _merge_moves(self, merge_into=False):
        if self and self[0].production_subcon_id :
            return
        return super(StockMove, self)._merge_moves(merge_into=merge_into)

    @api.model
    def create(self, vals):
        res = super(StockMove, self).create(vals)
        if res.production_subcon_id:
            wos = res.production_subcon_id.workorder_ids.filtered(lambda w:w.is_subcontracting)
            if wos :
                for wo in wos :
                    try :
                        wo.stock_move_id = res.id
                    except :
                        pass
                    if not res.workorder_id:
                        try :
                            res.workorder_id = wo.id
                        except :
                            pass
            if res.picking_id and res.picking_id.is_incoming_subcon :
                mo_exist = self.env['stock.move'].sudo().search([('production_subcon_id','=',res.production_subcon_id.id),
                                                                    ('picking_id.is_incoming_subcon','=',True),
                                                                    ('state','not in',('cancel','done')),
                                                                    ('id','!=',res.id),
                                                                    ('location_id','=',res.location_id.id),
                                                                    ('company_id','=',res.picking_id.company_id.id)],limit=1)
                if mo_exist :
                    raise Warning('Lot %s sudah pernah diinput di dokumen %s dengan lokasi asal %s (%s)' % (res.production_subcon_id.name,mo_exist.picking_id.name, res.location_id.complete_name,res.company_id.name))
        return res

    @api.multi
    def write(self, vals):
        _super = super(StockMove, self).write(vals)
        for i in self :
            if i.production_subcon_id :
                wos = i.production_subcon_id.workorder_ids.filtered(lambda w:w.is_subcontracting and not w.stock_move_id)
                for wo in wos :
                    wo.stock_move_id = i.id
        return _super


    @api.multi
    def _action_cancel(self):
        res = super(StockMove, self)._action_cancel()
        for x in self :
            if x.workorder_id and x.workorder_id.state or not ('done','cancel'):
                if x.workorder_id.stock_move_id :
                    x.workorder_id.stock_move_id = False
                if x.workorder_id.subcontract_id :
                    x.workorder_id.subcontract_id = False
        return res

StockMove()


class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    # @api.model
    # def create(self, vals):

    #     res = super(StockMoveLine, self).create(vals)
    #     mo = res.move_id.production_subcon_id
    #     if mo :
    #         lot = self.env['stock.production.lot'].sudo().search([('name', '=', res.move_id.production_subcon_id.name)],limit=1)
    #         if lot and not res.lot_id :
    #             res.lot_id = lot.id
    #     return res

    @api.depends('package_id','qty_done','product_uom_qty','state','move_id.product_id.weight')
    def _compute_bruto_netto(self):
        res = super(StockMoveLine, self)._compute_bruto_netto()
        for berat in self:
            pack_berat = 0.0
            if berat.package_id :
                if berat.package_id.package_type_id :
                    pack_berat = berat.package_id.package_type_id.weight
                if pack_berat == 0.0 :
                    pack_berat = berat.package_id.weight
            if berat.picking_id and berat.picking_id.subcontract_id :
                berat.netto = berat.netto/1000
                pallet = 1 # +1 kg
                if berat.move_id and berat.move_id.workorder_id :
                    pallet = berat.move_id.workorder_id.production_id.jml_box
                berat.bruto = berat.netto+pallet
        return res

StockMoveLine()


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    picking_type_interco_id = fields.Many2one('stock.picking.type','Picking Type Interco')

StockPickingType()


class StockRemark(models.Model):
    _name = 'stock.remark'
    _description = 'Stock Remark'

    @api.model
    def create(self, vals):
        vals['name'] = vals['name'].title().strip()
        return super(StockRemark, self).create(vals)

    name = fields.Char('Remark', size=15, required=True)

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Nama remark harus unik'),
    ]

StockRemark()


class StockScanCompare(models.Model):
    _name = 'stock.scan.compare'
    _description = 'Stock Compare'

    picking_id = fields.Many2one('stock.picking','Picking',ondelete='cascade')
    name = fields.Char('Name', size=15, required=True)
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order')
    product_id = fields.Many2one('product.product','Product',related='production_id.product_id', store=True)
    move_id = fields.Many2one('stock.move','Move', compute="_get_move_id", store=True)

    @api.one
    @api.depends('picking_id','picking_id.state','picking_id.move_ids_without_package')
    def _get_move_id(self):
        for rec in self:
            moves = rec.picking_id.move_ids_without_package.mapped('production_subcon_id')
            if moves and rec.production_id.id in moves.ids :
                rec.move_id = rec.picking_id.move_ids_without_package.filtered(lambda x:x.production_subcon_id.id == rec.production_id.id)

StockScanCompare()