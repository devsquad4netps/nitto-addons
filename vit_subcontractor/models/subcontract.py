# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import xlsxwriter
import base64
from io import BytesIO
import pytz
from pytz import timezone
import PIL
import io
from odoo import api, fields, models, _
import datetime
from odoo.exceptions import UserError, Warning
from odoo.addons import decimal_precision as dp


class MRPSubcontractor(models.Model):
    _name = 'mrp.subcontract'
    _order = 'name desc'
    _description = 'Subcontractor'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    @api.multi
    @api.depends('name','partner_id')
    def name_get(self):
        result = []
        for res in self:
            if res.name :
                name = '('+res.name+ ') '+ res.partner_id.name
            else :
                name = res.no_subkontrak
                #raise UserError(_('Sequence subcon company %s belum di set !')% (self.env.user.company_id.name))
            result.append((res.id, name))
        return result 

    @api.model
    def _get_default_picking_type_id(self):
        picking_type_id = self.env['stock.picking.type'].search([('name','=','Subcont Transfer'),
                                                                    ('warehouse_id.company_id','=',self.env.user.company_id.id),
                                                                    ('return_picking_type_id','!=',False)],limit=1)
        if picking_type_id :
            return picking_type_id.id

    @api.model
    def _get_default_return_type_id(self):
        picking_type_id = self.env['stock.picking.type'].search([('name','=','Subcont Transfer'),
                                                                    ('warehouse_id.company_id','=',self.env.user.company_id.id),
                                                                    ('return_picking_type_id','!=',False)],limit=1)
        if picking_type_id.return_picking_type_id :
            return picking_type_id.return_picking_type_id.id

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            vals['name'] = self.env['ir.sequence'].next_by_code('mrp.subcontract')
        elif 'name' not in vals :
            vals['name'] = self.env['ir.sequence'].next_by_code('mrp.subcontract')
        return super(MRPSubcontractor, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(MRPSubcontractor, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        for sub in self.wo_subcon_ids :
            if sub.state != 'confirm' :
                raise UserError(_('Workorder Subcont %s berstatus %s !')%(sub.name,sub.state))
            sub.state = 'done'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        for sub in self.wo_subcon_ids :
            sub.state = 'confirm'

    name = fields.Char('Code', default='/', track_visibility='onchange')
    nomor = fields.Char('Nomor', track_visibility='onchange')
    partner_id = fields.Many2one('res.partner','Partner', track_visibility='onchange')
    job = fields.Char('Job Position', track_visibility='onchange', size=60)
    responsible = fields.Char('Responsible', track_visibility='onchange', size=128)
    product_id = fields.Many2one('product.product','Product', track_visibility='onchange')
    cost_per_qty = fields.Float('Cost per Qty', track_visibility='onchange')
    picking_type_id = fields.Many2one('stock.picking.type','Picking Type', track_visibility='onchange',default=_get_default_picking_type_id)
    return_type_id = fields.Many2one('stock.picking.type','Return Type', track_visibility='onchange',default=_get_default_return_type_id)
    file_data = fields.Binary('File', readonly=True)
    no_subkontrak = fields.Char('No Subkontrak', track_visibility='onchange')
    tgl_subkontrak = fields.Date('Tgl Subkontrak', track_visibility='onchange')
    tgl_kontrak = fields.Date('Tgl Kontrak Kerja', track_visibility='onchange')
    jt_tgl_subkontrak = fields.Date('Jatuh Tempo Subkontrak', track_visibility='onchange')
    no_bpj = fields.Char('No BPJ', track_visibility='onchange')
    tgl_bpj = fields.Date('Tgl BPJ', track_visibility='onchange')
    no_jaminan = fields.Char('No Jaminan', track_visibility='onchange')
    nilai_jaminan = fields.Float('Nilai Jaminan', track_visibility='onchange')
    max_movement = fields.Integer('Max Movement')
    movement = fields.Float(
        'Movement', compute='_compute_movement',
        help='Movement datas', digits=(16, 0))
    total = fields.Float('Total')
    uom_id = fields.Many2one('uom.uom','UoM')
    wip_ids = fields.Many2many('mrp.workorder', compute="_search_workorder_ids")
    # product_ids = fields.Many2many('product.product', string="Products")
    product_ids = fields.One2many(comodel_name='mrp.subcontract.product', inverse_name='subcontract_id',string="Products")
    product_add_ids = fields.One2many(comodel_name='mrp.subcontract.add.product', inverse_name='subcontract_id', string="Additional Products")
    product_scrap_ids = fields.One2many(comodel_name='mrp.subcontract.scrap', inverse_name='subcontract_id', string="Scrap Products")
    company_id = fields.Many2one('res.company','Company', track_visibility='onchange',default=lambda self: self.env['res.company']._company_default_get())
    tolerance = fields.Float("Toleransi (%)", track_visibility='onchange')
    report_ids = fields.One2many("mrp.subcontract.reports","subcontract_id",string="Reports Summary Out")
    report_in_ids = fields.One2many("mrp.subcontract.reports.in","subcontract_id",string="Reports Summary In")
    stock_move_ids = fields.One2many("stock.move","subcontract_id",string="Stock Moves")
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm')],default='draft', track_visibility='on_change', string="State")
    wo_subcon_id = fields.Many2one('mrp.workorder.subcon', 'Workorder Subcont',required=False, track_visibility='onchange')
    workcenter_id = fields.Many2one('mrp.workcenter','Workcenter', related="wo_subcon_id.workcenter_id")
    workcenter_ids = fields.Many2many('mrp.workcenter',string='Workcenters', track_visibility='onchange')
    wo_subcon_ids = fields.One2many('mrp.workorder.subcon','contract_id', string='Workorder Subcon')
    is_interco = fields.Boolean('Intercompany?')
    interco_picking_type_id = fields.Many2one('stock.picking.type','Picking Type (Interco)', track_visibility='onchange')
    # other info
    jenis_barang = fields.Char('Jenis barang', track_visibility='onchange', size=60)
    jenis_dikirim = fields.Char('Jenis Barang yang dikirim', track_visibility='onchange', size=128)
    jenis_dikembalikan = fields.Char('Jenis Barang yang dikembalikan', track_visibility='onchange', size=128)
    penyelesaian = fields.Char('Penyelesaian', track_visibility='onchange', size=60)
    scrap = fields.Char('Scrap', track_visibility='onchange', size=60)
    lain = fields.Char('Lain-lain', track_visibility='onchange', size=128)
    non_commercial_value = fields.Char('Non Commercial Value',help='Pengeluaran dan pemasukan returnable packages dengan transaksi (Non Commercial Value)', track_visibility='onchange', size=128)
    picking_ids = fields.One2many('stock.picking','subcontract_id', string="Picking's")
    ketentuan_kualitas = fields.Boolean('Ketentuan Kualitas', default=False, help='Print Ketentuan Kualitas di excel kontrak kerja')


    @api.onchange('wo_subcon_id')
    def _onchange_wo_subcon_id(self):
        data = []
        if self.wo_subcon_id:
            for sub in self.wo_subcon_id.workorder_ids:
                data.append((0,0, {'workorder_id':sub.id,
                                        'name':sub.production_id.product_id.id,
                                        'name':sub.qty_producing
                                        })) 
        self.product_ids = data

    @api.multi
    def _compute_movement(self):
        for movement in self :
            datas = self.env['stock.picking'].search([('subcontract_id', '=', movement.id)])
            if datas:
                movement.movement = len(datas)

    @api.multi
    def _search_workorder_ids(self):
        for movement in self :
            moves = self.env['stock.move'].search([('picking_id.subcontract_id','=',movement.id)])
            if moves :
                datas = self.env['mrp.workorder'].search([('stock_move_id','in',moves.ids)])
                if datas:
                    movement.wip_ids = datas

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
            'dark_grey': '#BDC3C7',
            'grey': '#F0EFEE'
        }

        wbf = {}
        wbf['header'] = workbook.add_format({'bold': 1,'align': 'center','font_color': '#000000'})

        wbf['header2'] = workbook.add_format({'bold': 1,'align': 'left','font_color': '#000000'})

        wbf['header3'] = workbook.add_format({'bold': 1,'align': 'center','font_color': '#000000'})
        wbf['header3'].set_align('vcenter')
        wbf['header3'].set_font_size(22)

        wbf['header_table'] = workbook.add_format({'align': 'left','font_color': '#000000'})
        wbf['header_table'].set_border()
        wbf['header_table2'] = workbook.add_format({'align': 'center', 'bg_color':colors['dark_grey']})
        wbf['header_table2'].set_border()
        wbf['header_table2'].set_font_size(14)
        
        wbf['header_no'] = workbook.add_format({'bold': 1,'align': 'center','font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')

        wbf['header_no2'] = workbook.add_format({'bold': 1,'align': 'center','font_color': '#000000'})
        wbf['header_no2'].set_border()
        wbf['header_no2'].set_font_size(14)
        wbf['header_no2'].set_align('vcenter')
                
        wbf['footer'] = workbook.add_format({'align':'left'})
        
        wbf['title_doc'] = workbook.add_format({'bold': 1,'align': 'left'})
        wbf['title_doc'].set_font_size(12)
        wbf['title_doc2'] = workbook.add_format({'align': 'left'})
        wbf['title_doc2'].set_font_size(14)
        wbf['title_doc3'] = workbook.add_format({'align': 'center'})
        wbf['title_doc3'].set_font_size(14)
        wbf['title_doc4'] = workbook.add_format({'align': 'left'})
        wbf['title_doc4'].set_font_size(12)

        wbf['title_doc_tbl_left'] = workbook.add_format({'align': 'left'})
        wbf['title_doc_tbl_left'].set_font_size(14)
        wbf['title_doc_tbl_left'].set_align('vcenter')
        wbf['title_doc_tbl_left'].set_border()
        wbf['title_doc_tbl_center'] = workbook.add_format({'align': 'center'})
        wbf['title_doc_tbl_center'].set_font_size(14)
        wbf['title_doc_tbl_center'].set_align('vcenter')
        wbf['title_doc_tbl_center'].set_border()
        wbf['title_doc_tbl_right'] = workbook.add_format({'align': 'right','num_format': '#,##0'})
        wbf['title_doc_tbl_right'].set_font_size(14)
        wbf['title_doc_tbl_right'].set_align('vcenter')
        wbf['title_doc_tbl_right'].set_border()
        wbf['title_doc_tbl_top'] = workbook.add_format({'align': 'left'})
        wbf['title_doc_tbl_top'].set_font_size(14)
        wbf['title_doc_tbl_top'].set_top()
        wbf['title_doc_tbl_top'].set_left()
        wbf['title_doc_tbl_top'].set_right()
        wbf['title_doc_tbl_bottom'] = workbook.add_format({'align': 'left'})
        wbf['title_doc_tbl_bottom'].set_font_size(14)
        wbf['title_doc_tbl_bottom'].set_bottom()
        wbf['title_doc_tbl_bottom'].set_left()
        wbf['title_doc_tbl_bottom'].set_right()
        
        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)
        
        wbf['content'] = workbook.add_format({'bold': 1,'align': 'left','font_color': '#000000'})
        wbf['content'].set_border()
        wbf['content'].set_left()
        
        wbf['content_number'] = workbook.add_format({'bold': 1,'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_border()
        wbf['content_number'].set_right() 
          
        
        wbf['total'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'center'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()            
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()         
        
        return wbf, workbook

    @api.multi
    def action_print(self):
        for i in self :
            # if not i.is_subcontracting :
            #     raise exceptions.UserError(_('Report yang bisa ditarik hanya workcenter subcon'))
            report_name = 'Monitoring Subcon %s'%(i.partner_id.name)
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(i.name)
            worksheet.set_column('A1:A1', 3)
            worksheet.set_column('B1:B1', 45)
            worksheet.set_column('C1:C1', 10)
            worksheet.set_column('D1:D1', 10)
            worksheet.set_column('E1:E1', 10)
            worksheet.set_column('F1:F1', 10)
            worksheet.set_column('G1:G1', 10)
            worksheet.set_column('H1:H1', 10)
            worksheet.set_column('I1:I1', 10)
            worksheet.set_column('J1:J1', 10)
            worksheet.set_column('K1:K1', 10)
            worksheet.set_column('L1:L1', 10)
            worksheet.set_column('M1:M1', 10)
            worksheet.set_column('N1:N1', 10)
            worksheet.set_column('O1:O1', 10)
            worksheet.set_column('P1:P1', 10)
            worksheet.set_column('Q1:Q1', 10)
            worksheet.set_column('R1:R1', 10)
            worksheet.set_column('S1:S1', 10)
            worksheet.set_column('T1:T1', 10)
            worksheet.set_column('U1:U1', 10)
            worksheet.set_column('V1:V1', 10)
            worksheet.set_column('W1:W1', 10)
            worksheet.set_column('X1:X1', 10)
            # 11-20
            worksheet.set_column('Y1:Y1', 10)
            worksheet.set_column('Z1:Z1', 10)
            worksheet.set_column('AA1:AA1', 10)
            worksheet.set_column('AB1:AB1', 10)
            worksheet.set_column('AC1:AC1', 10)
            worksheet.set_column('AD1:AD1', 10)
            worksheet.set_column('AE1:AE1', 10)
            worksheet.set_column('AF1:AF1', 10)
            worksheet.set_column('AG1:AG1', 10)
            worksheet.set_column('AH1:AH1', 10)
            worksheet.set_column('AI1:AI1', 10)
            worksheet.set_column('AJ1:AJ1', 10)
            worksheet.set_column('AK1:AK1', 10)
            worksheet.set_column('AL1:AL1', 10)
            worksheet.set_column('AM1:AM1', 10)
            worksheet.set_column('AN1:AN1', 10)
            worksheet.set_column('AO1:AO1', 10)
            worksheet.set_column('AP1:AP1', 10)
            worksheet.set_column('AQ1:AQ1', 10)
            worksheet.set_column('AR1:AR1', 10)
            
            # 21-30
            worksheet.set_column('AS1:AS1', 10)
            worksheet.set_column('AT1:AT1', 10)
            worksheet.set_column('AU1:AU1', 10)
            worksheet.set_column('AV1:AV1', 10)
            worksheet.set_column('AW1:AW1', 10)
            worksheet.set_column('AX1:AX1', 10)
            worksheet.set_column('AY1:AY1', 10)
            worksheet.set_column('AZ1:AZ1', 10)            
            worksheet.set_column('BA1:BA1', 10)
            worksheet.set_column('BB1:BB1', 10)
            worksheet.set_column('BC1:BC1', 10)
            worksheet.set_column('BD1:BD1', 10)
            worksheet.set_column('BE1:BE1', 10)
            worksheet.set_column('BF1:BF1', 10)
            worksheet.set_column('BG1:BG1', 10)
            worksheet.set_column('BH1:BH1', 10)
            worksheet.set_column('BI1:BI1', 10)
            worksheet.set_column('BJ1:BJ1', 10)
            worksheet.set_column('BK1:BK1', 10)
            worksheet.set_column('BL1:BL1', 10)


            worksheet.merge_range('A1:BL1', 'Monitoring Pelaksanaan Subkontrak', wbf['header'])
            worksheet.write('A2', 'Nama PKB/PDKB', wbf['header2'])
            worksheet.write('A3', 'Nama Penerima Subkontrak', wbf['header2'])
            worksheet.write('A4', 'Nomor dan Tgl Persetujuan Subkontrak', wbf['header2'])
            worksheet.write('A5', 'Nomor dan Tgl Kontrak Kerja', wbf['header2'])
            worksheet.write('A6', 'Jatuh Tempo Pelaksanaan Subkontrak', wbf['header2'])
            worksheet.write('C2', ': %s'%i.company_id.name, wbf['header2'])
            worksheet.write('C3', ': %s'%i.partner_id.name, wbf['header2'])
            worksheet.write('C4', ': %s (%s)'%(i.no_subkontrak,datetime.datetime.strptime(str(i.tgl_subkontrak), '%Y-%m-%d').strftime('%d-%m-%Y')), wbf['header2'])
            worksheet.write('C5', ': %s (%s)'%(i.name,datetime.datetime.strptime(str(i.tgl_kontrak), '%Y-%m-%d').strftime('%d-%m-%Y')), wbf['header2'])
            worksheet.write('C6', ': '+datetime.datetime.strptime(str(i.jt_tgl_subkontrak), '%Y-%m-%d').strftime('%d-%m-%Y'), wbf['header2'])

            worksheet.write('R2', 'Nomor dan Tgl BPJ', wbf['header2'])
            worksheet.write('R3', 'Nomor Jaminan', wbf['header2'])
            worksheet.write('R4', 'Nilai Jaminan', wbf['header2'])
            no_bpj = '-'
            tgl_bpj = '-'
            if i.no_bpj :
                no_bpj = i.no_bpj
            if i.tgl_bpj:
                tgl_bpj = datetime.datetime.strptime(str(i.tgl_bpj), '%Y-%m-%d').strftime('%d-%m-%Y')
            worksheet.write('U2', ': %s (%s)'%(no_bpj,tgl_bpj), wbf['header2'])
            no_jaminan = '-'
            if i.no_jaminan :
                no_jaminan = i.no_jaminan
            worksheet.write('U3', ': '+no_jaminan, wbf['header2'])
            # worksheet.write('U4', ': '+f"{i.nilai_jaminan:,}", wbf['header2'])
            worksheet.write('U4', ': '+str(i.nilai_jaminan), wbf['header2'])
            row = 8
            row = self.print_out(row,wbf,workbook,worksheet,i)
            row = self.print_in(row,wbf,workbook,worksheet,i)

            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = i.name+'-'+i.no_subkontrak
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_out(self,row,wbf,workbook,worksheet,contract_id):
        worksheet.merge_range('A%s:A%s'%(row,row+2), 'No', wbf['header_no'])
        worksheet.merge_range('B%s:B%s'%(row,row+2), 'Jenis Barang Yang Akan Dikeluarkan', wbf['header_no'])
        worksheet.merge_range('C%s:D%s'%(row,row+1), 'Jenis', wbf['header_no'])
        worksheet.merge_range('E%s:BL%s'%(row,row), 'Pengeluaran', wbf['header_no'])
        worksheet.merge_range('E%s:F%s'%(row+1,row+1), 'I', wbf['header_no'])
        worksheet.merge_range('G%s:H%s'%(row+1,row+1), 'II', wbf['header_no'])
        worksheet.merge_range('I%s:J%s'%(row+1,row+1), 'III', wbf['header_no'])
        worksheet.merge_range('K%s:L%s'%(row+1,row+1), 'IV', wbf['header_no'])
        worksheet.merge_range('M%s:N%s'%(row+1,row+1), 'V', wbf['header_no'])
        worksheet.merge_range('O%s:P%s'%(row+1,row+1), 'VI', wbf['header_no'])
        worksheet.merge_range('Q%s:R%s'%(row+1,row+1), 'VII', wbf['header_no'])
        worksheet.merge_range('S%s:T%s'%(row+1,row+1), 'VIII', wbf['header_no'])
        worksheet.merge_range('U%s:V%s'%(row+1,row+1), 'IX', wbf['header_no'])
        worksheet.merge_range('W%s:X%s'%(row+1,row+1), 'X', wbf['header_no'])
        #11-20
        worksheet.merge_range('Y%s:Z%s'%(row+1,row+1), 'XI', wbf['header_no'])
        worksheet.merge_range('AA%s:AB%s'%(row+1,row+1), 'XII', wbf['header_no'])
        worksheet.merge_range('AC%s:AD%s'%(row+1,row+1), 'XIII', wbf['header_no'])
        worksheet.merge_range('AE%s:AF%s'%(row+1,row+1), 'XIV', wbf['header_no'])
        worksheet.merge_range('AG%s:AH%s'%(row+1,row+1), 'XV', wbf['header_no'])
        worksheet.merge_range('AI%s:AJ%s'%(row+1,row+1), 'XVI', wbf['header_no'])
        worksheet.merge_range('AK%s:AL%s'%(row+1,row+1), 'XVII', wbf['header_no'])
        worksheet.merge_range('AM%s:AN%s'%(row+1,row+1), 'XVIII', wbf['header_no'])
        worksheet.merge_range('AO%s:AP%s'%(row+1,row+1), 'XIX', wbf['header_no'])
        worksheet.merge_range('AQ%s:AR%s'%(row+1,row+1), 'XX', wbf['header_no'])
        #21-30
        worksheet.merge_range('AS%s:AT%s'%(row+1,row+1), 'XXI', wbf['header_no'])
        worksheet.merge_range('AU%s:AV%s'%(row+1,row+1), 'XXII', wbf['header_no'])
        worksheet.merge_range('AW%s:AX%s'%(row+1,row+1), 'XXIII', wbf['header_no'])
        worksheet.merge_range('AY%s:AZ%s'%(row+1,row+1), 'XXIV', wbf['header_no'])
        worksheet.merge_range('BA%s:BB%s'%(row+1,row+1), 'XXV', wbf['header_no'])
        worksheet.merge_range('BC%s:BD%s'%(row+1,row+1), 'XXVI', wbf['header_no'])
        worksheet.merge_range('BE%s:BF%s'%(row+1,row+1), 'XXVII', wbf['header_no'])
        worksheet.merge_range('BG%s:BH%s'%(row+1,row+1), 'XXVIII', wbf['header_no'])
        worksheet.merge_range('BI%s:BJ%s'%(row+1,row+1), 'XXIX', wbf['header_no'])
        worksheet.merge_range('BK%s:BL%s'%(row+1,row+1), 'XXX', wbf['header_no'])

        worksheet.write('C%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('D%s'%(row+2), 'Jenis', wbf['header_no'])

        worksheet.write('E%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('F%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('G%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('H%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('I%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('J%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('K%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('L%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('M%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('N%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('O%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('P%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('Q%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('R%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('S%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('T%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('U%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('V%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('W%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('X%s'%(row+2), 'Sisa', wbf['header_no'])
        # 11-20
        worksheet.write('Y%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('Z%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AA%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AB%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AC%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AD%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AE%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AF%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AG%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AH%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AI%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AJ%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AK%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AL%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AM%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AN%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AO%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AP%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AQ%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AR%s'%(row+2), 'Sisa', wbf['header_no'])
        # 21-30
        worksheet.write('AS%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AT%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AU%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AV%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AW%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AX%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AY%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AZ%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BA%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BB%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BC%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BD%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BE%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BF%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BG%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BH%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BI%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BJ%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BK%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BL%s'%(row+2), 'Sisa', wbf['header_no'])

        no = 1
        row +=3
        for data in contract_id.report_ids :
            if data.name[:3] != ' - ' :
                worksheet.write('A%s'%row, no, wbf['content_number'])
                worksheet.merge_range('B%s:BL%s'%(row,row), data.name, wbf['content'])
                no += 1
                row += 1
            else :
                worksheet.write('A%s'%row, '-', wbf['content_number'])
                worksheet.write('B%s'%row, data.name[3:], wbf['content'])
                worksheet.write('C%s'%row, data.jumlah, wbf['content_number'])
                worksheet.write('D%s'%row, data.jenis, wbf['content'])
                worksheet.write('E%s'%row, data.jumlah1 or '', wbf['content_number'])
                worksheet.write('F%s'%row, data.sisa1 or '', wbf['content_number'])
                worksheet.write('G%s'%row, data.jumlah2 or '', wbf['content_number'])
                worksheet.write('H%s'%row, data.sisa2 or '', wbf['content_number'])
                worksheet.write('I%s'%row, data.jumlah3 or '', wbf['content_number'])
                worksheet.write('J%s'%row, data.sisa3 or '', wbf['content_number'])
                worksheet.write('K%s'%row, data.jumlah4 or '', wbf['content_number'])
                worksheet.write('L%s'%row, data.sisa4 or '', wbf['content_number'])
                worksheet.write('M%s'%row, data.jumlah5 or '', wbf['content_number'])
                worksheet.write('N%s'%row, data.sisa5 or '', wbf['content_number'])
                worksheet.write('O%s'%row, data.jumlah6 or '', wbf['content_number'])
                worksheet.write('P%s'%row, data.sisa6 or '', wbf['content_number'])
                worksheet.write('Q%s'%row, data.jumlah7 or '', wbf['content_number'])
                worksheet.write('R%s'%row, data.sisa7 or '', wbf['content_number'])
                worksheet.write('S%s'%row, data.jumlah8 or '', wbf['content_number'])
                worksheet.write('T%s'%row, data.sisa8 or '', wbf['content_number'])
                worksheet.write('U%s'%row, data.jumlah9 or '', wbf['content_number'])
                worksheet.write('V%s'%row, data.sisa9 or '', wbf['content_number'])
                worksheet.write('W%s'%row, data.jumlah10 or '', wbf['content_number'])
                worksheet.write('X%s'%row, data.sisa10 or '', wbf['content_number'])
                #11-20
                worksheet.write('Y%s'%row, data.jumlah11 or '', wbf['content_number'])
                worksheet.write('Z%s'%row, data.sisa11 or '', wbf['content_number'])
                worksheet.write('AA%s'%row, data.jumlah12 or '', wbf['content_number'])
                worksheet.write('AB%s'%row, data.sisa12 or '', wbf['content_number'])
                worksheet.write('AC%s'%row, data.jumlah13 or '', wbf['content_number'])
                worksheet.write('AD%s'%row, data.sisa13 or '', wbf['content_number'])
                worksheet.write('AE%s'%row, data.jumlah14 or '', wbf['content_number'])
                worksheet.write('AF%s'%row, data.sisa14 or '', wbf['content_number'])
                worksheet.write('AG%s'%row, data.jumlah15 or '', wbf['content_number'])
                worksheet.write('AH%s'%row, data.sisa15 or '', wbf['content_number'])
                worksheet.write('AI%s'%row, data.jumlah16 or '', wbf['content_number'])
                worksheet.write('AJ%s'%row, data.sisa16 or '', wbf['content_number'])
                worksheet.write('AK%s'%row, data.jumlah17 or '', wbf['content_number'])
                worksheet.write('AL%s'%row, data.sisa17 or '', wbf['content_number'])
                worksheet.write('AM%s'%row, data.jumlah18 or '', wbf['content_number'])
                worksheet.write('AN%s'%row, data.sisa18 or '', wbf['content_number'])
                worksheet.write('AO%s'%row, data.jumlah19 or '', wbf['content_number'])
                worksheet.write('AP%s'%row, data.sisa19 or '', wbf['content_number'])
                worksheet.write('AQ%s'%row, data.jumlah20 or '', wbf['content_number'])
                worksheet.write('AR%s'%row, data.sisa20 or '', wbf['content_number'])
                #21-30
                worksheet.write('AS%s'%row, data.jumlah21 or '', wbf['content_number'])
                worksheet.write('AT%s'%row, data.sisa21 or '', wbf['content_number'])
                worksheet.write('AU%s'%row, data.jumlah22 or '', wbf['content_number'])
                worksheet.write('AV%s'%row, data.sisa22 or '', wbf['content_number'])
                worksheet.write('AW%s'%row, data.jumlah23 or '', wbf['content_number'])
                worksheet.write('AX%s'%row, data.sisa23 or '', wbf['content_number'])
                worksheet.write('AY%s'%row, data.jumlah24 or '', wbf['content_number'])
                worksheet.write('AZ%s'%row, data.sisa24 or '', wbf['content_number'])
                worksheet.write('BA%s'%row, data.jumlah25 or '', wbf['content_number'])
                worksheet.write('BB%s'%row, data.sisa25 or '', wbf['content_number'])
                worksheet.write('BC%s'%row, data.jumlah26 or '', wbf['content_number'])
                worksheet.write('BD%s'%row, data.sisa26 or '', wbf['content_number'])
                worksheet.write('BE%s'%row, data.jumlah27 or '', wbf['content_number'])
                worksheet.write('BF%s'%row, data.sisa27 or '', wbf['content_number'])
                worksheet.write('BG%s'%row, data.jumlah28 or '', wbf['content_number'])
                worksheet.write('BH%s'%row, data.sisa28 or '', wbf['content_number'])
                worksheet.write('BI%s'%row, data.jumlah29 or '', wbf['content_number'])
                worksheet.write('BJ%s'%row, data.sisa29 or '', wbf['content_number'])
                worksheet.write('BK%s'%row, data.jumlah20 or '', wbf['content_number'])
                worksheet.write('BL%s'%row, data.sisa20 or '', wbf['content_number'])
                row += 1

        worksheet.merge_range('A%s:D%s'%(row,row), 'Nomor / Tanggal BC 2.6.1', wbf['header_no'])
        worksheet.merge_range('E%s:F%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('G%s:H%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('I%s:J%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('K%s:L%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('M%s:N%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('O%s:P%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('Q%s:R%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('S%s:T%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('U%s:V%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('W%s:X%s'%(row,row), '', wbf['header_no'])
        #11-20
        worksheet.merge_range('Y%s:Z%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AA%s:AB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AC%s:AD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AE%s:AF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AG%s:AH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AI%s:AJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AK%s:AL%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AM%s:AN%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AO%s:AP%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AQ%s:AR%s'%(row,row), '', wbf['header_no'])
        #21-30
        worksheet.merge_range('AS%s:AT%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AU%s:AV%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AW%s:AX%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AY%s:AZ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BA%s:BB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BC%s:BD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BE%s:BF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BG%s:BH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BI%s:BJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BK%s:BL%s'%(row,row), '', wbf['header_no'])
        row += 1
        worksheet.merge_range('A%s:D%s'%(row,row), 'Paraf dan Tanggal Keluar', wbf['header_no'])
        worksheet.merge_range('E%s:F%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('G%s:H%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('I%s:J%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('K%s:L%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('M%s:N%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('O%s:P%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('Q%s:R%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('S%s:T%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('U%s:V%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('W%s:X%s'%(row,row), '', wbf['header_no'])
        #11-20
        worksheet.merge_range('Y%s:Z%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AA%s:AB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AC%s:AD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AE%s:AF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AG%s:AH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AI%s:AJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AK%s:AL%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AM%s:AN%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AO%s:AP%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AQ%s:AR%s'%(row,row), '', wbf['header_no'])
        #21-30
        worksheet.merge_range('AS%s:AT%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AU%s:AV%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AW%s:AX%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AY%s:AZ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BA%s:BB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BC%s:BD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BE%s:BF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BG%s:BH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BI%s:BJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BK%s:BL%s'%(row,row), '', wbf['header_no'])
        row+=3
        return row

    def print_in(self,row,wbf,workbook,worksheet,contract_id):
        worksheet.merge_range('A%s:A%s'%(row,row+2), 'No', wbf['header_no'])
        worksheet.merge_range('B%s:B%s'%(row,row+2), 'Jenis Barang Yang Akan Dihasilkan / Sisa dan / Potongan', wbf['header_no'])
        worksheet.merge_range('C%s:D%s'%(row,row+1), 'Jenis', wbf['header_no'])
        worksheet.merge_range('E%s:BL%s'%(row,row), 'Pemasukan', wbf['header_no'])
        worksheet.merge_range('E%s:F%s'%(row+1,row+1), 'I', wbf['header_no'])
        worksheet.merge_range('G%s:H%s'%(row+1,row+1), 'II', wbf['header_no'])
        worksheet.merge_range('I%s:J%s'%(row+1,row+1), 'III', wbf['header_no'])
        worksheet.merge_range('K%s:L%s'%(row+1,row+1), 'IV', wbf['header_no'])
        worksheet.merge_range('M%s:N%s'%(row+1,row+1), 'V', wbf['header_no'])
        worksheet.merge_range('O%s:P%s'%(row+1,row+1), 'VI', wbf['header_no'])
        worksheet.merge_range('Q%s:R%s'%(row+1,row+1), 'VII', wbf['header_no'])
        worksheet.merge_range('S%s:T%s'%(row+1,row+1), 'VIII', wbf['header_no'])
        worksheet.merge_range('U%s:V%s'%(row+1,row+1), 'IX', wbf['header_no'])
        worksheet.merge_range('W%s:X%s'%(row+1,row+1), 'X', wbf['header_no'])
        #11-20
        worksheet.merge_range('Y%s:Z%s'%(row+1,row+1), 'XI', wbf['header_no'])
        worksheet.merge_range('AA%s:AB%s'%(row+1,row+1), 'XII', wbf['header_no'])
        worksheet.merge_range('AC%s:AD%s'%(row+1,row+1), 'XIII', wbf['header_no'])
        worksheet.merge_range('AE%s:AF%s'%(row+1,row+1), 'XIV', wbf['header_no'])
        worksheet.merge_range('AG%s:AH%s'%(row+1,row+1), 'XV', wbf['header_no'])
        worksheet.merge_range('AI%s:AJ%s'%(row+1,row+1), 'XVI', wbf['header_no'])
        worksheet.merge_range('AK%s:AL%s'%(row+1,row+1), 'XVII', wbf['header_no'])
        worksheet.merge_range('AM%s:AN%s'%(row+1,row+1), 'XVIII', wbf['header_no'])
        worksheet.merge_range('AO%s:AP%s'%(row+1,row+1), 'XIX', wbf['header_no'])
        worksheet.merge_range('AQ%s:AR%s'%(row+1,row+1), 'XX', wbf['header_no'])
        #21-30
        worksheet.merge_range('AS%s:AT%s'%(row+1,row+1), 'XXI', wbf['header_no'])
        worksheet.merge_range('AU%s:AV%s'%(row+1,row+1), 'XXII', wbf['header_no'])
        worksheet.merge_range('AW%s:AX%s'%(row+1,row+1), 'XXIII', wbf['header_no'])
        worksheet.merge_range('AY%s:AZ%s'%(row+1,row+1), 'XXIV', wbf['header_no'])
        worksheet.merge_range('BA%s:BB%s'%(row+1,row+1), 'XXV', wbf['header_no'])
        worksheet.merge_range('BC%s:BD%s'%(row+1,row+1), 'XXVI', wbf['header_no'])
        worksheet.merge_range('BE%s:BF%s'%(row+1,row+1), 'XXVII', wbf['header_no'])
        worksheet.merge_range('BG%s:BH%s'%(row+1,row+1), 'XXVIII', wbf['header_no'])
        worksheet.merge_range('BI%s:BJ%s'%(row+1,row+1), 'XXIX', wbf['header_no'])
        worksheet.merge_range('BK%s:BL%s'%(row+1,row+1), 'XXX', wbf['header_no'])

        worksheet.write('C%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('D%s'%(row+2), 'Jenis', wbf['header_no'])

        worksheet.write('E%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('F%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('G%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('H%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('I%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('J%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('K%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('L%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('M%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('N%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('O%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('P%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('Q%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('R%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('S%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('T%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('U%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('V%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('W%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('X%s'%(row+2), 'Sisa', wbf['header_no'])
        # 11-20
        worksheet.write('Y%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('Z%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AA%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AB%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AC%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AD%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AE%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AF%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AG%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AH%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AI%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AJ%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AK%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AL%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AM%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AN%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AO%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AP%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AQ%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AR%s'%(row+2), 'Sisa', wbf['header_no'])
        # 21-30
        worksheet.write('AS%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AT%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AU%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AV%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AW%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AX%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('AY%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('AZ%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BA%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BB%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BC%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BD%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BE%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BF%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BG%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BH%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BI%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BJ%s'%(row+2), 'Sisa', wbf['header_no'])
        worksheet.write('BK%s'%(row+2), 'Jumlah', wbf['header_no'])
        worksheet.write('BL%s'%(row+2), 'Sisa', wbf['header_no'])

        no = 1
        row +=3
        for data in contract_id.report_in_ids :
            if data.name[:3] != ' - ' :
                worksheet.write('A%s'%row, no, wbf['content_number'])
                worksheet.merge_range('B%s:BL%s'%(row,row), data.name, wbf['content'])
                no += 1
                row += 1
            else :
                worksheet.write('A%s'%row, '-', wbf['content_number'])
                worksheet.write('B%s'%row, data.name[3:], wbf['content'])
                worksheet.write('C%s'%row, data.jumlah, wbf['content_number'])
                worksheet.write('D%s'%row, data.jenis, wbf['content'])
                worksheet.write('E%s'%row, data.jumlah1 or '', wbf['content_number'])
                worksheet.write('F%s'%row, data.sisa1 or '', wbf['content_number'])
                worksheet.write('G%s'%row, data.jumlah2 or '', wbf['content_number'])
                worksheet.write('H%s'%row, data.sisa2 or '', wbf['content_number'])
                worksheet.write('I%s'%row, data.jumlah3 or '', wbf['content_number'])
                worksheet.write('J%s'%row, data.sisa3 or '', wbf['content_number'])
                worksheet.write('K%s'%row, data.jumlah4 or '', wbf['content_number'])
                worksheet.write('L%s'%row, data.sisa4 or '', wbf['content_number'])
                worksheet.write('M%s'%row, data.jumlah5 or '', wbf['content_number'])
                worksheet.write('N%s'%row, data.sisa5 or '', wbf['content_number'])
                worksheet.write('O%s'%row, data.jumlah6 or '', wbf['content_number'])
                worksheet.write('P%s'%row, data.sisa6 or '', wbf['content_number'])
                worksheet.write('Q%s'%row, data.jumlah7 or '', wbf['content_number'])
                worksheet.write('R%s'%row, data.sisa7 or '', wbf['content_number'])
                worksheet.write('S%s'%row, data.jumlah8 or '', wbf['content_number'])
                worksheet.write('T%s'%row, data.sisa8 or '', wbf['content_number'])
                worksheet.write('U%s'%row, data.jumlah9 or '', wbf['content_number'])
                worksheet.write('V%s'%row, data.sisa9 or '', wbf['content_number'])
                worksheet.write('W%s'%row, data.jumlah10 or '', wbf['content_number'])
                worksheet.write('X%s'%row, data.sisa10 or '', wbf['content_number'])
                #11-20
                worksheet.write('Y%s'%row, data.jumlah11 or '', wbf['content_number'])
                worksheet.write('Z%s'%row, data.sisa11 or '', wbf['content_number'])
                worksheet.write('AA%s'%row, data.jumlah12 or '', wbf['content_number'])
                worksheet.write('AB%s'%row, data.sisa12 or '', wbf['content_number'])
                worksheet.write('AC%s'%row, data.jumlah13 or '', wbf['content_number'])
                worksheet.write('AD%s'%row, data.sisa13 or '', wbf['content_number'])
                worksheet.write('AE%s'%row, data.jumlah14 or '', wbf['content_number'])
                worksheet.write('AF%s'%row, data.sisa14 or '', wbf['content_number'])
                worksheet.write('AG%s'%row, data.jumlah15 or '', wbf['content_number'])
                worksheet.write('AH%s'%row, data.sisa15 or '', wbf['content_number'])
                worksheet.write('AI%s'%row, data.jumlah16 or '', wbf['content_number'])
                worksheet.write('AJ%s'%row, data.sisa16 or '', wbf['content_number'])
                worksheet.write('AK%s'%row, data.jumlah17 or '', wbf['content_number'])
                worksheet.write('AL%s'%row, data.sisa17 or '', wbf['content_number'])
                worksheet.write('AM%s'%row, data.jumlah18 or '', wbf['content_number'])
                worksheet.write('AN%s'%row, data.sisa18 or '', wbf['content_number'])
                worksheet.write('AO%s'%row, data.jumlah19 or '', wbf['content_number'])
                worksheet.write('AP%s'%row, data.sisa19 or '', wbf['content_number'])
                worksheet.write('AQ%s'%row, data.jumlah20 or '', wbf['content_number'])
                worksheet.write('AR%s'%row, data.sisa20 or '', wbf['content_number'])
                #21-30
                worksheet.write('AS%s'%row, data.jumlah21 or '', wbf['content_number'])
                worksheet.write('AT%s'%row, data.sisa21 or '', wbf['content_number'])
                worksheet.write('AU%s'%row, data.jumlah22 or '', wbf['content_number'])
                worksheet.write('AV%s'%row, data.sisa22 or '', wbf['content_number'])
                worksheet.write('AW%s'%row, data.jumlah23 or '', wbf['content_number'])
                worksheet.write('AX%s'%row, data.sisa23 or '', wbf['content_number'])
                worksheet.write('AY%s'%row, data.jumlah24 or '', wbf['content_number'])
                worksheet.write('AZ%s'%row, data.sisa24 or '', wbf['content_number'])
                worksheet.write('BA%s'%row, data.jumlah25 or '', wbf['content_number'])
                worksheet.write('BB%s'%row, data.sisa25 or '', wbf['content_number'])
                worksheet.write('BC%s'%row, data.jumlah26 or '', wbf['content_number'])
                worksheet.write('BD%s'%row, data.sisa26 or '', wbf['content_number'])
                worksheet.write('BE%s'%row, data.jumlah27 or '', wbf['content_number'])
                worksheet.write('BF%s'%row, data.sisa27 or '', wbf['content_number'])
                worksheet.write('BG%s'%row, data.jumlah28 or '', wbf['content_number'])
                worksheet.write('BH%s'%row, data.sisa28 or '', wbf['content_number'])
                worksheet.write('BI%s'%row, data.jumlah29 or '', wbf['content_number'])
                worksheet.write('BJ%s'%row, data.sisa29 or '', wbf['content_number'])
                worksheet.write('BK%s'%row, data.jumlah20 or '', wbf['content_number'])
                worksheet.write('BL%s'%row, data.sisa20 or '', wbf['content_number'])
                row += 1

        worksheet.merge_range('A%s:D%s'%(row,row), 'Nomor / Tanggal BC 2.6.2', wbf['header_no'])
        worksheet.merge_range('E%s:F%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('G%s:H%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('I%s:J%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('K%s:L%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('M%s:N%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('O%s:P%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('Q%s:R%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('S%s:T%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('U%s:V%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('W%s:X%s'%(row,row), '', wbf['header_no'])
        #11-20
        worksheet.merge_range('Y%s:Z%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AA%s:AB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AC%s:AD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AE%s:AF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AG%s:AH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AI%s:AJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AK%s:AL%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AM%s:AN%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AO%s:AP%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AQ%s:AR%s'%(row,row), '', wbf['header_no'])
        #21-30
        worksheet.merge_range('AS%s:AT%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AU%s:AV%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AW%s:AX%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AY%s:AZ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BA%s:BB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BC%s:BD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BE%s:BF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BG%s:BH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BI%s:BJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BK%s:BL%s'%(row,row), '', wbf['header_no'])
        row += 1
        worksheet.merge_range('A%s:D%s'%(row,row), 'Paraf dan Tanggal Masuk', wbf['header_no'])
        worksheet.merge_range('E%s:F%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('G%s:H%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('I%s:J%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('K%s:L%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('M%s:N%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('O%s:P%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('Q%s:R%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('S%s:T%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('U%s:V%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('W%s:X%s'%(row,row), '', wbf['header_no'])
        #11-20
        worksheet.merge_range('Y%s:Z%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AA%s:AB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AC%s:AD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AE%s:AF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AG%s:AH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AI%s:AJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AK%s:AL%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AM%s:AN%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AO%s:AP%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AQ%s:AR%s'%(row,row), '', wbf['header_no'])
        #21-30
        worksheet.merge_range('AS%s:AT%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AU%s:AV%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AW%s:AX%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('AY%s:AZ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BA%s:BB%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BC%s:BD%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BE%s:BF%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BG%s:BH%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BI%s:BJ%s'%(row,row), '', wbf['header_no'])
        worksheet.merge_range('BK%s:BL%s'%(row,row), '', wbf['header_no'])
        row+=1
        return row

    @api.multi
    def update_reports_summary(self):
        self.update_reports_summary_out()
        self.update_reports_summary_in()
        self._cr.commit()
        message = {'title': _('Information'),
                'message' : "Reports Summary Updated !"  }
        return {'warning': message}

    def update_reports_summary_out(self):
        self.ensure_one()
        for sub in self :
            self._cr.execute("DELETE FROM mrp_subcontract_reports WHERE subcontract_id = %s" , ( [(sub.id)] ))
            # search bahan baku (wire)
            picking = self.env['stock.picking']
            move = self.env['stock.move']
            rpt_details = self.env['mrp.subcontract.reports']
            materials = sub.product_ids.mapped('product_material_id')
            for wire in materials :
                # header (bahan bakunya)
                rpt_details.create({'subcontract_id':sub.id,
                                    'name' : wire.name,
                                    })
                datas = sub.product_ids.filtered(lambda i:i.product_material_id.id == wire.id)
                for data in datas :
                    # Finnish goodnya
                    jumlah1, jumlah2, jumlah3, jumlah4, jumlah5, jumlah6, jumlah7, jumlah8, jumlah9, jumlah10 = (0,)*10
                    jumlah11, jumlah12, jumlah13, jumlah14, jumlah15, jumlah16, jumlah17, jumlah18, jumlah19, jumlah20 = (0,)*10
                    jumlah21, jumlah22, jumlah23, jumlah24, jumlah25, jumlah26, jumlah27, jumlah28, jumlah29, jumlah30 = (0,)*10
                    sisa1, sisa2, sisa3, sisa4, sisa5, sisa6, sisa7, sisa8, sisa9, sisa10 = (0,)*10
                    sisa11, sisa12, sisa13, sisa14, sisa15, sisa16, sisa17, sisa18, sisa19, sisa20 = (0,)*10
                    sisa21, sisa22, sisa23, sisa24, sisa25, sisa26, sisa27, sisa28, sisa29, sisa30 = (0,)*10
                    notes1, notes2, notes3, notes4, notes5, notes6, notes7, notes8, notes9, notes10 = ('',)*10
                    notes11, notes12, notes13, notes14, notes15, notes16, notes17, notes18, notes19, notes20 = ('',)*10
                    notes21, notes22, notes23, notes24, notes25, notes26, notes27, notes28, notes29, notes30 = ('',)*10
                    jumlah = data.name
                    t_masuk = 0.0
                    count = 1
                    pickings = self.env['stock.picking'].search([('name','ilike','%OUT%'),('subcontract_id', '=', sub.id),('state','=','done'),('company_id','=',sub.company_id.id)], order='date_done asc')
                    # for pick in self.env['stock.picking'].search([('subcontract_id', '=', sub.id),
                    #                                                 ('picking_type_id', '=', sub.picking_type_id.id,),
                    #                                                 ('state','=','done')], order='date_done asc'):
                    for pick in pickings:
                        jumlah_basic = 0.0
                        for det in pick.move_ids_without_package.filtered(lambda mv:mv.product_id.id == data.product_id.id):
                            jumlah_basic += det.netto_wire if det.netto_wire > 0.0 else det.netto

                        if count == 1 :
                            jumlah1 = jumlah_basic
                            t_masuk += jumlah1
                            sisa1 = jumlah-t_masuk
                            notes1 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 2 :
                            jumlah2 = jumlah_basic
                            t_masuk += jumlah2
                            sisa2 = sisa1-jumlah2
                            notes2 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 3 :
                            jumlah3 = jumlah_basic
                            t_masuk += jumlah3
                            sisa3 = sisa2-jumlah3
                            notes3 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 4 :
                            jumlah4 = jumlah_basic
                            t_masuk += jumlah4
                            sisa4 = sisa3-jumlah4
                            notes4 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 5 :
                            jumlah5 = jumlah_basic
                            t_masuk += jumlah5
                            sisa5 = sisa4-jumlah5
                            notes5 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 6 :
                            jumlah6 = jumlah_basic
                            t_masuk += jumlah6
                            sisa6 = sisa5-jumlah6
                            notes6 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 7 :
                            jumlah7 = jumlah_basic
                            t_masuk += jumlah7
                            sisa7 = sisa6-jumlah7
                            notes7 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 8 :
                            jumlah8 = jumlah_basic
                            t_masuk += jumlah8
                            sisa8 = sisa7-jumlah8
                            notes8 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 9 :
                            jumlah9 = jumlah_basic
                            t_masuk += jumlah9
                            sisa9 = sisa8-jumlah9
                            notes9 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 10 :
                            jumlah10 = jumlah_basic
                            t_masuk += jumlah10
                            sisa10 = sisa9-jumlah10
                            notes10 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 11 :
                            jumlah11 = jumlah_basic
                            t_masuk += jumlah11
                            sisa11 = sisa10-jumlah11
                            notes11 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 12 :
                            jumlah12 = jumlah_basic
                            t_masuk += jumlah12
                            sisa12 = sisa11-jumlah12
                            notes12 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 13 :
                            jumlah13 = jumlah_basic
                            t_masuk += jumlah13
                            sisa13 = sisa12-jumlah13
                            notes13 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 14 :
                            jumlah14 = jumlah_basic
                            t_masuk += jumlah14
                            sisa14 = sisa13-jumlah14
                            notes14 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 15 :
                            jumlah15 = jumlah_basic
                            t_masuk += jumlah15
                            sisa15 = sisa14-jumlah15
                            notes15 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 16 :
                            jumlah16 = jumlah_basic
                            t_masuk += jumlah16
                            sisa16 = sisa15-jumlah16
                            notes16 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 17 :
                            jumlah17 = jumlah_basic
                            t_masuk += jumlah17
                            sisa17 = sisa16-jumlah17
                            notes17 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 18 :
                            jumlah18 = jumlah_basic
                            t_masuk += jumlah18
                            sisa18 = sisa17-jumlah18
                            notes18 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 19 :
                            jumlah19 = jumlah_basic
                            t_masuk += jumlah19
                            sisa19 = sisa18-jumlah19
                            notes19 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 20 :
                            jumlah20 = jumlah_basic
                            t_masuk += jumlah20
                            sisa20 = sisa19-jumlah20
                            notes20 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 21 :
                            jumlah21 = jumlah_basic
                            t_masuk += jumlah21
                            sisa21 = sisa20-jumlah21
                            notes21 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 22 :
                            jumlah22 = jumlah_basic
                            t_masuk += jumlah22
                            sisa22 = sisa21-jumlah22
                            notes22 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 23 :
                            jumlah23 = jumlah_basic
                            t_masuk += jumlah23
                            sisa23 = sisa22-jumlah23
                            notes23 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 24 :
                            jumlah24 = jumlah_basic
                            t_masuk += jumlah24
                            sisa24 = sisa23-jumlah24
                            notes24 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 25 :
                            jumlah25 = jumlah_basic
                            t_masuk += jumlah25
                            sisa25 = sisa24-jumlah25
                            notes25 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 26 :
                            jumlah26 = jumlah_basic
                            t_masuk += jumlah26
                            sisa26 = sisa25-jumlah26
                            notes26 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 27 :
                            jumlah27 = jumlah_basic
                            t_masuk += jumlah27
                            sisa27 = sisa26-jumlah27
                            notes27 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 28 :
                            jumlah28 = jumlah_basic
                            t_masuk += jumlah28
                            sisa28 = sisa27-jumlah28
                            notes28 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 29 :
                            jumlah29 = jumlah_basic
                            t_masuk += jumlah29
                            sisa29 = sisa28-jumlah29
                            notes29 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 30 :
                            jumlah30 = jumlah_basic
                            t_masuk += jumlah30
                            sisa30 = sisa29-jumlah30
                            notes30 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        else :
                            break
                    
                    rpt_details.create({'subcontract_id':sub.id,
                                    'name' : ' - '+data.product_id.name,
                                    'jumlah' : data.name,
                                    'jenis' : data.uom_id.name,
                                    'jumlah1': jumlah1,
                                    'sisa1': sisa1,
                                    'notes1': notes1,
                                    'jumlah2': jumlah2,
                                    'sisa2': sisa2,
                                    'notes2': notes2,
                                    'jumlah3': jumlah3,
                                    'sisa3': sisa3,
                                    'notes3': notes3,
                                    'jumlah4': jumlah4,
                                    'sisa4': sisa4,
                                    'notes4': notes4,
                                    'jumlah5': sisa5,
                                    'sisa5': sisa5,
                                    'notes5': notes5,
                                    'jumlah6': jumlah6,
                                    'sisa6': sisa6,
                                    'notes6': notes6,
                                    'jumlah7': jumlah7,
                                    'sisa7': sisa7,
                                    'notes7': notes7,
                                    'jumlah8': jumlah8,
                                    'sisa8': sisa8,
                                    'notes8': notes8,
                                    'jumlah9': jumlah9,
                                    'sisa9': sisa9,
                                    'notes9': notes9,
                                    'jumlah10': jumlah10,
                                    'sisa10': sisa10,
                                    'notes10': notes10,
                                    # 11-20
                                    'jumlah11': jumlah11,
                                    'sisa11': sisa11,
                                    'notes11': notes11,
                                    'jumlah12': jumlah12,
                                    'sisa12': sisa12,
                                    'notes12': notes12,
                                    'jumlah13': jumlah13,
                                    'sisa13': sisa13,
                                    'notes13': notes13,
                                    'jumlah14': jumlah14,
                                    'sisa14': sisa14,
                                    'notes14': notes14,
                                    'jumlah15': sisa15,
                                    'sisa15': sisa15,
                                    'notes15': notes15,
                                    'jumlah16': jumlah16,
                                    'sisa16': sisa16,
                                    'notes16': notes16,
                                    'jumlah17': jumlah17,
                                    'sisa17': sisa17,
                                    'notes17': notes17,
                                    'jumlah18': jumlah18,
                                    'sisa18': sisa18,
                                    'notes18': notes18,
                                    'jumlah19': jumlah19,
                                    'sisa19': sisa19,
                                    'notes19': notes19,
                                    'jumlah20': jumlah20,
                                    'sisa20': sisa20,
                                    'notes20': notes20,
                                    # 21-30
                                    'jumlah21': jumlah21,
                                    'sisa21': sisa21,
                                    'notes21': notes21,
                                    'jumlah22': jumlah22,
                                    'sisa22': sisa22,
                                    'notes22': notes22,
                                    'jumlah23': jumlah23,
                                    'sisa23': sisa23,
                                    'notes23': notes23,
                                    'jumlah24': jumlah24,
                                    'sisa24': sisa24,
                                    'notes24': notes24,
                                    'jumlah25': sisa25,
                                    'sisa25': sisa25,
                                    'notes25': notes25,
                                    'jumlah26': jumlah26,
                                    'sisa26': sisa26,
                                    'notes26': notes26,
                                    'jumlah27': jumlah27,
                                    'sisa27': sisa27,
                                    'notes27': notes27,
                                    'jumlah28': jumlah28,
                                    'sisa28': sisa28,
                                    'notes28': notes28,
                                    'jumlah29': jumlah29,
                                    'sisa29': sisa29,
                                    'notes29': notes29,
                                    'jumlah30': jumlah30,
                                    'sisa30': sisa30,
                                    'notes30': notes30,
                                    })

    def update_reports_summary_in(self):
        self.ensure_one()
        for sub in self :
            self._cr.execute("DELETE FROM mrp_subcontract_reports_in WHERE subcontract_id = %s" , ( [(sub.id)] ))
            # search bahan baku (wire)
            # search bahan baku (wire)
            picking = self.env['stock.picking']
            move = self.env['stock.move']
            rpt_details = self.env['mrp.subcontract.reports.in']
            materials = sub.product_ids.mapped('product_material_id')
            for wire in materials :
                # header (bahan bakunya)
                rpt_details.create({'subcontract_id':sub.id,
                                    'name' : wire.name,
                                    })
                datas = sub.product_ids.filtered(lambda i:i.product_material_id.id == wire.id)
                for data in datas :
                    # Finnish goodnya
                    jumlah1, jumlah2, jumlah3, jumlah4, jumlah5, jumlah6, jumlah7, jumlah8, jumlah9, jumlah10 = (0,)*10
                    jumlah11, jumlah12, jumlah13, jumlah14, jumlah15, jumlah16, jumlah17, jumlah18, jumlah19, jumlah20 = (0,)*10
                    jumlah21, jumlah22, jumlah23, jumlah24, jumlah25, jumlah26, jumlah27, jumlah28, jumlah29, jumlah30 = (0,)*10
                    sisa1, sisa2, sisa3, sisa4, sisa5, sisa6, sisa7, sisa8, sisa9, sisa10 = (0,)*10
                    sisa11, sisa12, sisa13, sisa14, sisa15, sisa16, sisa17, sisa18, sisa19, sisa20 = (0,)*10
                    sisa21, sisa22, sisa23, sisa24, sisa25, sisa26, sisa27, sisa28, sisa29, sisa30 = (0,)*10
                    notes1, notes2, notes3, notes4, notes5, notes6, notes7, notes8, notes9, notes10 = ('',)*10
                    notes11, notes12, notes13, notes14, notes15, notes16, notes17, notes18, notes19, notes20 = ('',)*10
                    notes21, notes22, notes23, notes24, notes25, notes26, notes27, notes28, notes29, notes30 = ('',)*10
                    jumlah = data.name
                    t_masuk = 0.0
                    count = 1
                    pickings = self.env['stock.picking'].search([('name','ilike','%IN%'),('subcontract_id', '=', sub.id),('state','=','done'),('company_id','=',sub.company_id.id)], order='date_done asc')
                    for pick in pickings :
                        # if pick.picking_type_id != sub.return_type_id.id:
                        #     continue
                        jumlah_basic = 0.0
                        for det in pick.move_ids_without_package.filtered(lambda mv:mv.product_id.id == data.product_id.id):
                            jumlah_basic += det.netto_wire if det.netto_wire > 0.0 else det.netto

                        if count == 1 :
                            jumlah1 = jumlah_basic
                            t_masuk += jumlah1
                            sisa1 = jumlah-t_masuk
                            notes1 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 2 :
                            jumlah2 = jumlah_basic
                            t_masuk += jumlah2
                            sisa2 = sisa1-jumlah2
                            notes2 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 3 :
                            jumlah3 = jumlah_basic
                            t_masuk += jumlah3
                            sisa3 = sisa2-jumlah3
                            notes3 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 4 :
                            jumlah4 = jumlah_basic
                            t_masuk += jumlah4
                            sisa4 = sisa3-jumlah4
                            notes4 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 5 :
                            jumlah5 = jumlah_basic
                            t_masuk += jumlah5
                            sisa5 = sisa4-jumlah5
                            notes5 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 6 :
                            jumlah6 = jumlah_basic
                            t_masuk += jumlah6
                            sisa6 = sisa5-jumlah6
                            notes6 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 7 :
                            jumlah7 = jumlah_basic
                            t_masuk += jumlah7
                            sisa7 = sisa6-jumlah7
                            notes7 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 8 :
                            jumlah8 = jumlah_basic
                            t_masuk += jumlah8
                            sisa8 = sisa7-jumlah8
                            notes8 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 9 :
                            jumlah9 = jumlah_basic
                            t_masuk += jumlah9
                            sisa9 = sisa8-jumlah9
                            notes9 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 10 :
                            jumlah10 = jumlah_basic
                            t_masuk += jumlah10
                            sisa10 = sisa9-jumlah10
                            notes10 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 11 :
                            jumlah11 = jumlah_basic
                            t_masuk += jumlah11
                            sisa11 = sisa10-jumlah11
                            notes11 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 12 :
                            jumlah12 = jumlah_basic
                            t_masuk += jumlah12
                            sisa12 = sisa11-jumlah12
                            notes12 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 13 :
                            jumlah13 = jumlah_basic
                            t_masuk += jumlah13
                            sisa13 = sisa12-jumlah13
                            notes13 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 14 :
                            jumlah14 = jumlah_basic
                            t_masuk += jumlah14
                            sisa14 = sisa13-jumlah14
                            notes14 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 15 :
                            jumlah15 = jumlah_basic
                            t_masuk += jumlah15
                            sisa15 = sisa14-jumlah15
                            notes15 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 16 :
                            jumlah16 = jumlah_basic
                            t_masuk += jumlah16
                            sisa16 = sisa15-jumlah16
                            notes16 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 17 :
                            jumlah17 = jumlah_basic
                            t_masuk += jumlah17
                            sisa17 = sisa16-jumlah17
                            notes17 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 18 :
                            jumlah18 = jumlah_basic
                            t_masuk += jumlah18
                            sisa18 = sisa17-jumlah18
                            notes18 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 19 :
                            jumlah19 = jumlah_basic
                            t_masuk += jumlah19
                            sisa19 = sisa18-jumlah19
                            notes19 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 20 :
                            jumlah20 = jumlah_basic
                            t_masuk += jumlah20
                            sisa20 = sisa19-jumlah20
                            notes20 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 21 :
                            jumlah21 = jumlah_basic
                            t_masuk += jumlah21
                            sisa21 = sisa20-jumlah21
                            notes21 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 22 :
                            jumlah22 = jumlah_basic
                            t_masuk += jumlah22
                            sisa22 = sisa21-jumlah22
                            notes22 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 23 :
                            jumlah23 = jumlah_basic
                            t_masuk += jumlah23
                            sisa23 = sisa22-jumlah23
                            notes23 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 24 :
                            jumlah24 = jumlah_basic
                            t_masuk += jumlah24
                            sisa24 = sisa23-jumlah24
                            notes24 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 25 :
                            jumlah25 = jumlah_basic
                            t_masuk += jumlah25
                            sisa25 = sisa24-jumlah25
                            notes25 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 26 :
                            jumlah26 = jumlah_basic
                            t_masuk += jumlah26
                            sisa26 = sisa25-jumlah26
                            notes26 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 27 :
                            jumlah27 = jumlah_basic
                            t_masuk += jumlah27
                            sisa27 = sisa26-jumlah27
                            notes27 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 28 :
                            jumlah28 = jumlah_basic
                            t_masuk += jumlah28
                            sisa28 = sisa27-jumlah28
                            notes28 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 29 :
                            jumlah29 = jumlah_basic
                            t_masuk += jumlah29
                            sisa29 = sisa28-jumlah29
                            notes29 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        elif count == 30 :
                            jumlah30 = jumlah_basic
                            t_masuk += jumlah30
                            sisa30 = sisa29-jumlah30
                            notes30 = pick.subcontract_id.name+';'+pick.name or '-'
                            count += 1
                        else :
                            break
                    
                    rpt_details.create({'subcontract_id':sub.id,
                                    'name' : ' - '+data.product_id.name,
                                    'jumlah' : data.name,
                                    'jenis' : data.uom_id.name,
                                    'jumlah1': jumlah1,
                                    'sisa1': sisa1,
                                    'notes1': notes1,
                                    'jumlah2': jumlah2,
                                    'sisa2': sisa2,
                                    'notes2': notes2,
                                    'jumlah3': jumlah3,
                                    'sisa3': sisa3,
                                    'notes3': notes3,
                                    'jumlah4': jumlah4,
                                    'sisa4': sisa4,
                                    'notes4': notes4,
                                    'jumlah5': sisa5,
                                    'sisa5': sisa5,
                                    'notes5': notes5,
                                    'jumlah6': jumlah6,
                                    'sisa6': sisa6,
                                    'notes6': notes6,
                                    'jumlah7': jumlah7,
                                    'sisa7': sisa7,
                                    'notes7': notes7,
                                    'jumlah8': jumlah8,
                                    'sisa8': sisa8,
                                    'notes8': notes8,
                                    'jumlah9': jumlah9,
                                    'sisa9': sisa9,
                                    'notes9': notes9,
                                    'jumlah10': jumlah10,
                                    'sisa10': sisa10,
                                    'notes10': notes10,
                                    # 11-20
                                    'jumlah11': jumlah11,
                                    'sisa11': sisa11,
                                    'notes11': notes11,
                                    'jumlah12': jumlah12,
                                    'sisa12': sisa12,
                                    'notes12': notes12,
                                    'jumlah13': jumlah13,
                                    'sisa13': sisa13,
                                    'notes13': notes13,
                                    'jumlah14': jumlah14,
                                    'sisa14': sisa14,
                                    'notes14': notes14,
                                    'jumlah15': sisa15,
                                    'sisa15': sisa15,
                                    'notes15': notes15,
                                    'jumlah16': jumlah16,
                                    'sisa16': sisa16,
                                    'notes16': notes16,
                                    'jumlah17': jumlah17,
                                    'sisa17': sisa17,
                                    'notes17': notes17,
                                    'jumlah18': jumlah18,
                                    'sisa18': sisa18,
                                    'notes18': notes18,
                                    'jumlah19': jumlah19,
                                    'sisa19': sisa19,
                                    'notes19': notes19,
                                    'jumlah20': jumlah20,
                                    'sisa20': sisa20,
                                    'notes20': notes20,
                                    # 21-30
                                    'jumlah21': jumlah21,
                                    'sisa21': sisa21,
                                    'notes21': notes21,
                                    'jumlah22': jumlah22,
                                    'sisa22': sisa22,
                                    'notes22': notes22,
                                    'jumlah23': jumlah23,
                                    'sisa23': sisa23,
                                    'notes23': notes23,
                                    'jumlah24': jumlah24,
                                    'sisa24': sisa24,
                                    'notes24': notes24,
                                    'jumlah25': sisa25,
                                    'sisa25': sisa25,
                                    'notes25': notes25,
                                    'jumlah26': jumlah26,
                                    'sisa26': sisa26,
                                    'notes26': notes26,
                                    'jumlah27': jumlah27,
                                    'sisa27': sisa27,
                                    'notes27': notes27,
                                    'jumlah28': jumlah28,
                                    'sisa28': sisa28,
                                    'notes28': notes28,
                                    'jumlah29': jumlah29,
                                    'sisa29': sisa29,
                                    'notes29': notes29,
                                    'jumlah30': jumlah30,
                                    'sisa30': sisa30,
                                    'notes30': notes30,
                                    })

    @api.multi
    def print_kontrak_kerja(self):
        for i in self :
            tgl_subkontrak = datetime.datetime.strptime(str(i.tgl_subkontrak), '%Y-%m-%d').strftime('%d %B %Y')
            tgl_jt = datetime.datetime.strptime(str(i.jt_tgl_subkontrak), '%Y-%m-%d').strftime('%d %B %Y')
            report_name = 'Kontrak Kerja Subcon %s'%(i.name)
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            wbf, workbook = i.add_workbook_format(workbook)
            worksheet = workbook.add_worksheet(i.partner_id.name)
            worksheet.set_column('A1:A1', 2)
            worksheet.set_column('B1:B1', 4)
            worksheet.set_column('C1:C1', 3)
            worksheet.set_column('D1:D1', 15)
            worksheet.set_column('E1:E1', 5)
            worksheet.set_column('F1:F1', 25)
            worksheet.set_column('G1:G1', 20)
            worksheet.set_column('H1:H1', 20)
            worksheet.set_column('I1:I1', 10)

            worksheet.merge_range('A1:K1', 'KONTRAK KERJA', wbf['header3'])
            worksheet.merge_range('A2:K2', i.nomor, wbf['header3'])
            
            worksheet.write('B4', 'Yang bertanda tangan dibawah ini :', wbf['title_doc2'])
            worksheet.write('C5', 'I', wbf['title_doc2'])

            worksheet.write('D5', 'Nama', wbf['title_doc2'])
            worksheet.write('E5', ':', wbf['title_doc2'])
            worksheet.write('F5', 'PRIYA SETIADI', wbf['title_doc2'])

            worksheet.write('D6', 'Jabatan', wbf['title_doc2'])
            worksheet.write('E6', ':', wbf['title_doc2'])
            worksheet.write('F6', 'Direktur', wbf['title_doc2'])

            worksheet.write('D7', 'Alamat', wbf['title_doc2'])
            worksheet.write('E7', ':', wbf['title_doc2'])
            worksheet.write('F7', 'PT. NITTO ALAM INDONESIA', wbf['title_doc2'])
            worksheet.write('F8', 'Jl. Manis II, Kawasan Industri Manis, Tangerang - Banten', wbf['title_doc2'])

            worksheet.write('B10', 'Selanjutnya disebut pihak pertama ( I ) :', wbf['title_doc2'])
            worksheet.write('C11', 'II', wbf['title_doc2'])
            
            worksheet.write('D11', 'Nama', wbf['title_doc2'])
            worksheet.write('E11', ':', wbf['title_doc2'])
            worksheet.write('F11', i.responsible or '', wbf['title_doc2'])

            worksheet.write('D12', 'Jabatan', wbf['title_doc2'])
            worksheet.write('E12', ':', wbf['title_doc2'])
            worksheet.write('F12', i.job or '', wbf['title_doc2'])

            worksheet.write('D13', 'Alamat', wbf['title_doc2'])
            worksheet.write('E13', ':', wbf['title_doc2'])
            worksheet.write('F13', i.partner_id.street or '', wbf['title_doc2'])
            worksheet.write('F14', i.partner_id.street2 or '', wbf['title_doc2'])

            worksheet.write('B15', 'Selanjutnya disebut pihak kedua ( II )', wbf['title_doc2'])
            worksheet.write('B16', 'Dengan ini pihak I dan pihak II mengadakan kerja sama dalam proses Pewarnaan ( Plating ) dan', wbf['title_doc2'])
            worksheet.write('B17', 'Striping dimana pihak I memberikan pekerjaan kepada pihak II dengan ketentuan sebagai berikut :', wbf['title_doc2'])

            worksheet.write('C18', '1', wbf['title_doc2'])
            worksheet.write('D18', 'Jenis barang', wbf['title_doc2'])
            worksheet.write('G18', ': '+ (i.jenis_barang or ''), wbf['title_doc2'])

            worksheet.write('C19', '2', wbf['title_doc2'])
            worksheet.write('D19', 'Jenis barang yang dikembalikan', wbf['title_doc2'])
            worksheet.write('G19', ': '+ (i.jenis_dikembalikan or ''), wbf['title_doc2'])

            worksheet.write('C20', '3', wbf['title_doc2'])
            worksheet.write('D20', 'Jumlah', wbf['title_doc2'])
            qty_wire = sum(i.product_ids.mapped('name'))
            worksheet.write('G20', ': '+'{:,}'.format(int(qty_wire))+' '+i.product_ids[0].uom_id.name+' / 10 kali', wbf['title_doc2'])

            worksheet.write('C21', '4', wbf['title_doc2'])
            worksheet.write('D21', 'Penyelesaian', wbf['title_doc2'])
            worksheet.write('G21', ': '+(i.penyelesaian or ''), wbf['title_doc2'])

            worksheet.write('C22', '5', wbf['title_doc2'])
            worksheet.write('D22', 'Scrap', wbf['title_doc2'])
            worksheet.write('G22', ': '+(i.scrap or ''), wbf['title_doc2'])

            worksheet.write('C23', '6', wbf['title_doc2'])
            worksheet.write('D23', 'Nilai jaminan'+ '  ('+i.partner_id.property_purchase_currency_id.name+' )', wbf['title_doc2'])
            # worksheet.write('G23', ': '+f"{int(i.nilai_jaminan):,}" or '', wbf['title_doc2'])
            worksheet.write('G23', ': '+'{:,}'.format(int(i.nilai_jaminan)) or '', wbf['title_doc2'])

            worksheet.write('C24', '7', wbf['title_doc2'])
            worksheet.write('D24', 'Pengeluaran dan pemasukan returnable packages dengan transaksi (Non Commercial Value)', wbf['title_doc2'])
            worksheet.write('G24', ': '+(i.non_commercial_value or ''), wbf['title_doc2'])

            worksheet.write('C25', '8', wbf['title_doc2'])
            worksheet.write('D25', 'Lain-lain', wbf['title_doc2'])
            worksheet.write('G25', ': '+(i.lain or ''), wbf['title_doc2'])

            ######## Tabel 1
            worksheet.write('D27', 'Data barang yang dikirim :', wbf['title_doc2'])
            worksheet.write('B28', 'No', wbf['header_table2'])
            worksheet.merge_range('C28:F28', 'Uraian Barang, Merk, Ukuran, Tipe, Spf Lain', wbf['header_table2'])
            worksheet.write('G28', 'Kode Barang', wbf['header_table2'])
            worksheet.write('H28', 'Jumlah', wbf['header_table2'])
            worksheet.write('I28', 'Satuan', wbf['header_table2'])
            worksheet.merge_range('J28:K28', 'Keterangan', wbf['header_table2'])
            no = 1
            row = 29
            for detail in i.product_ids:
               worksheet.merge_range('B%s:B%s'% (row,row+1), no, wbf['header_no2'])
               worksheet.merge_range('C%s:F%s'% (row,row), detail.product_id.name, wbf['title_doc_tbl_top'])
               worksheet.merge_range('C%s:F%s'% (row+1,row+1), i.jenis_dikirim or '', wbf['title_doc_tbl_bottom'])
               worksheet.merge_range('G%s:G%s'% (row,row+1), detail.product_id.default_code+' KZ', wbf['title_doc_tbl_center'])
               worksheet.merge_range('H%s:H%s'% (row,row+1), detail.name, wbf['title_doc_tbl_right'])
               worksheet.merge_range('I%s:I%s'% (row,row+1), detail.uom_id.name, wbf['title_doc_tbl_center'])
               worksheet.merge_range('J%s:K%s'% (row,row+1), detail.note or '', wbf['title_doc_tbl_left'])
               no+=1
               row+=2
            row+=1

            if i.product_add_ids :
                ######## Tabel 2
                worksheet.write('D%s'% row, 'Data barang yang ditambahkan oleh penerima pekerjaan :', wbf['title_doc2'])
                row+=1
                worksheet.write('B%s'% row, 'No', wbf['header_table2'])
                worksheet.merge_range('C%s:F%s'% (row,row), 'Uraian Barang, Merk, Ukuran, Tipe, Spf Lain', wbf['header_table2'])
                worksheet.write('G%s'% row, 'Kode Barang', wbf['header_table2'])
                worksheet.write('H%s'% row, 'Jumlah', wbf['header_table2'])
                worksheet.write('I%s'% row, 'Satuan', wbf['header_table2'])
                worksheet.merge_range('J%s:K%s'% (row,row), 'Keterangan', wbf['header_table2'])
                no = 1
                row+=1
                for detail2 in i.product_add_ids:
                   worksheet.write('B%s'% row, no, wbf['header_no2'])
                   worksheet.merge_range('C%s:F%s'% (row,row), detail2.name, wbf['title_doc_tbl_left'])
                   worksheet.write('G%s'% row, detail2.code, wbf['title_doc_tbl_center'])
                   worksheet.write('H%s'% row, detail2.qty, wbf['title_doc_tbl_right'])
                   worksheet.write('I%s'% row, detail2.uom_id.name, wbf['title_doc_tbl_center'])
                   worksheet.merge_range('J%s:K%s'% (row,row), detail2.note or '', wbf['title_doc_tbl_left'])
                   no+=1
                   row+=1
                row+=1

            ######## Tabel 3
            worksheet.write('D%s'% row, 'Data barang yang akan dihasilkan :', wbf['title_doc2'])
            row+=1
            worksheet.write('B%s'% row, 'No', wbf['header_table2'])
            worksheet.merge_range('C%s:F%s'% (row,row), 'Uraian Barang', wbf['header_table2'])
            worksheet.write('G%s'% row, 'Kode Barang', wbf['header_table2'])
            worksheet.write('H%s'% row, 'Jumlah', wbf['header_table2'])
            worksheet.write('I%s'% row, 'Satuan', wbf['header_table2'])
            worksheet.merge_range('J%s:K%s'% (row,row), 'Keterangan', wbf['header_table2'])
            no = 1
            row+=1
            for detail3 in i.product_ids:
               worksheet.merge_range('B%s:B%s'% (row,row+1), no, wbf['header_no2'])
               worksheet.merge_range('C%s:F%s'% (row,row), detail3.product_id.name, wbf['title_doc_tbl_top'])
               worksheet.merge_range('C%s:F%s'% (row+1,row+1), i.jenis_dikembalikan or '', wbf['title_doc_tbl_bottom'])
               worksheet.merge_range('G%s:G%s'% (row,row+1), detail3.product_id.default_code+' '+detail3.product_id.plt or '', wbf['title_doc_tbl_center'])
               worksheet.merge_range('H%s:H%s'% (row,row+1), detail3.name, wbf['title_doc_tbl_right'])
               worksheet.merge_range('I%s:I%s'% (row,row+1), detail3.uom_id.name, wbf['title_doc_tbl_center'])
               worksheet.merge_range('J%s:K%s'% (row,row+1), detail3.note2 or '', wbf['title_doc_tbl_left'])
               no+=1
               row+=2

            row+=1
            if i.product_scrap_ids :
                ######## Tabel 2
                worksheet.write('D%s'% row, 'Data Sisa Bahan  Baku / Scrap :', wbf['title_doc2'])
                row+=1
                worksheet.write('B%s'% row, 'No', wbf['header_table2'])
                worksheet.merge_range('C%s:F%s'% (row,row), 'Uraian Barang', wbf['header_table2'])
                worksheet.write('G%s'% row, 'Kode Barang', wbf['header_table2'])
                worksheet.write('H%s'% row, 'Jumlah', wbf['header_table2'])
                worksheet.write('I%s'% row, 'Satuan', wbf['header_table2'])
                worksheet.merge_range('J%s:K%s'% (row,row), 'Keterangan', wbf['header_table2'])
                no = 1
                row+=1
                for detail4 in i.product_scrap_ids:
                   worksheet.write('B%s'% row, no, wbf['header_no2'])
                   worksheet.merge_range('C%s:F%s'% (row,row), detail4.name, wbf['title_doc_tbl_left'])
                   worksheet.write('G%s'% row, detail4.code, wbf['title_doc_tbl_center'])
                   worksheet.write('H%s'% row, detail4.qty, wbf['title_doc_tbl_right'])
                   worksheet.write('I%s'% row, detail4.uom_id.name, wbf['title_doc_tbl_center'])
                   worksheet.merge_range('J%s:K%s'% (row,row), detail4.note or '', wbf['title_doc_tbl_left'])
                   no+=1
                   row+=1
                row+=1

            
            worksheet.merge_range('E%s:F%s'% (row,row), 'I. KETENTUAN UMUM', wbf['title_doc2'])

            worksheet.write('E%s'% str(row+1), 'a', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+1), 'Hasil produksi plating harus sesuai dengan ketentuan pihak I.', wbf['title_doc2'])

            worksheet.write('E%s'% str(row+2), 'b', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+2), 'Pihak II bersedia untuk melakukan pengontrolan produksi sesuai dengan ketentuan pihak I.', wbf['title_doc2'])            

            worksheet.write('E%s'% str(row+3), 'c', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+3), 'Pihak II bertanggung jawab atas keamanan dan keutuhan barang yang sedang diproses.', wbf['title_doc2'])            

            worksheet.write('E%s'% str(row+4), 'd', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+4), 'Pihak II bersedia mengembalikan barang / bahan sisa produksi kepada pihak I.', wbf['title_doc2'])

            worksheet.write('E%s'% str(row+5), 'e', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+5), 'Pihak I akan menyelesaikan pembayaran 60 hari setelah terima Faktur.', wbf['title_doc2'])
            
            worksheet.write('E%s'% str(row+6), 'f', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+6), 'Kontrak kerja sama ini berlaku mulai tgl '+ tgl_subkontrak +' sampai dengan tgl '+ tgl_jt+'.', wbf['title_doc2'])

            worksheet.write('E%s'% str(row+7), 'g', wbf['title_doc3'])
            worksheet.write('F%s'% str(row+7), 'Pihak II dapat menambahkan chemical sesuai dengan kesepakatan kedua belah pihak.', wbf['title_doc2'])            

            new_row = row+7
            # tambah II. Ketentuan kualitas
            if i.ketentuan_kualitas :
                worksheet.merge_range('E%s:F%s'% (row+9,row+9), 'II. KETENTUAN KUALITAS', wbf['title_doc2'])

                worksheet.write('E%s'% str(row+10), 'a', wbf['title_doc3'])
                worksheet.write('F%s'% str(row+10), 'SST sesuai standard, hasil test SST dikirim setiap 1 bulan minimal 1 kali', wbf['title_doc2'])

                worksheet.write('E%s'% str(row+11), 'b', wbf['title_doc3'])
                worksheet.write('F%s'% str(row+11), 'Thickness', wbf['title_doc2'])

                worksheet.write('E%s'% str(row+12), 'c', wbf['title_doc3'])
                worksheet.write('F%s'% str(row+12), 'Appearance sesuai limit sample', wbf['title_doc2'])

                worksheet.write('E%s'% str(row+13), 'd', wbf['title_doc3'])
                worksheet.write('F%s'% str(row+13), 'Mengikuti ketentuan chemical substances antara lain : RoHS, SoC, PFOS dll.', wbf['title_doc2'])

                # tambah 5 baris
                row+=7

            worksheet.write('B%s'% str(row+9), 'Keterangan-keterangan lain yang menyangkut kontrak ini akan merupakan lampiran tertulis yang saling mengikat antara kedua belah pihak.', wbf['title_doc4'])

            worksheet.write('B%s'% str(row+11), 'Tangerang, '+ tgl_subkontrak, wbf['title_doc4'])

            worksheet.merge_range('B%s:E%s'% (str(row+13),str(row+13)), 'Pihak I', wbf['title_doc3'])
            worksheet.merge_range('I%s:K%s'% (str(row+13),str(row+13)), 'Pihak II', wbf['title_doc3'])

            worksheet.merge_range('B%s:E%s'% (str(row+20),str(row+20)), 'PRIYA SETIADI', wbf['title_doc3'])
            worksheet.merge_range('I%s:K%s'% (str(row+20),str(row+20)), i.responsible or '', wbf['title_doc3'])

            worksheet.merge_range('B%s:E%s'% (str(row+21),str(row+21)), 'Direktur', wbf['title_doc3'])
            worksheet.merge_range('I%s:K%s'% (str(row+21),str(row+21)), i.job or '', wbf['title_doc3'])


            workbook.close()
            result = base64.encodestring(fp.getvalue())
            i.write({'file_data':result})
            filename = i.name+'-'+i.partner_id.name
            filename = filename + '%2Exlsx'
            url = "web/content/?model="+self._name+"&id="+str(i.id)+"&field=file_data&download=true&filename="+filename
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

MRPSubcontractor()


class MRPSubcontractorProduct(models.Model):
    _name = 'mrp.subcontract.product'
    _description = 'Subcontract Product Lines'

    def _get_default_uom(self):
        uom = self.env['uom.uom']
        uom_id = False
        uom_exist = uom.sudo().search(['|','|','|',('name','=','KG'),('name','=','Kg'),('name','=','kg'),('name','=','KGM')],limit=1)
        if uom_exist :
            uom_id = uom_exist.id
        return uom_id

    @api.multi
    def _compute_product_material_id(self):
        for line in self :
            if line.product_material_id :
                continue
            if line.product_id and line.subcontract_id and line.subcontract_id.stock_move_ids :
                # matching = line.subcontract_id.stock_move_ids.filtered(lambda x:x.product_id.id == line.product_id.id and x.picking_id.picking_type_id.id == line.subcontract_id.picking_type_id.id and x.state != 'cancel')
                matching = line.subcontract_id.stock_move_ids.filtered(lambda x:x.product_id.id == line.product_id.id and x.state != 'cancel')
                if matching :
                    # datas = self.env['mrp.workorder'].sudo().search([('stock_move_id','in',matching.ids)])
                    # if datas :
                    # productions = datas.mapped('production_id')
                    productions = matching.mapped('production_subcon_id')
                    wire = self.env['product.category'].sudo().search([('name','ilike','Wire')],limit=1)
                    if wire :
                        wire_id = False
                        for prod in productions :
                            wire_exist = prod.move_raw_ids.filtered(lambda rw:rw.product_id.product_tmpl_id.categ_id.id == wire.id)
                            if wire_exist :
                                wire_id = wire_exist[0].product_id
                                break
                        if wire_id :
                            line.product_material_id = wire_id.id

    name = fields.Float('Quantity',required=True)
    subcontract_id = fields.Many2one(comodel_name='mrp.subcontract',string='Subcontractor', ondelete='cascade')
    uom_id = fields.Many2one('uom.uom','UoM',default=_get_default_uom)
    workorder_id = fields.Many2one('mrp.workorder','Workorder')
    production_id = fields.Many2one('mrp.production','Manufacturing Order',related="workorder_id.production_id")
    # product_id = fields.Many2one('product.product','Finish Good',related="workorder_id.product_id")
    product_id = fields.Many2one('product.product','Product')
    description = fields.Char('Description 1')
    description2 = fields.Char('Descriptionotes 2')
    uom_product_id = fields.Many2one('uom.uom','Product UoM',related='product_id.uom_id')
    cost_per_qty = fields.Float('Cost per Qty')
    product_material_id = fields.Many2one('product.product','Raw Material',compute="_compute_product_material_id")
    note = fields.Char('Notes 1')
    note2 = fields.Char('Notes 2')


MRPSubcontractorProduct()


class MRPSubcontractorAddProduct(models.Model):
    _name = 'mrp.subcontract.add.product'
    _description = 'Subcontract Product Lines Additional Product'

    subcontract_id = fields.Many2one(comodel_name='mrp.subcontract',string='Subcontractor', ondelete='cascade')
    name = fields.Char('Name',required=True, size=80)
    code = fields.Char('Code', size=30)
    qty = fields.Float('Quantity')
    uom_id = fields.Many2one('uom.uom','UoM')
    note = fields.Char('Note', size=128)

MRPSubcontractorAddProduct()

class MRPSubcontractorScrap(models.Model):
    _name = 'mrp.subcontract.scrap'
    _description = 'Subcontract Product Lines Scrap Product'

    subcontract_id = fields.Many2one(comodel_name='mrp.subcontract',string='Subcontractor', ondelete='cascade')
    name = fields.Char('Name',required=True, size=80)
    code = fields.Char('Code', size=30)
    qty = fields.Float('Quantity')
    uom_id = fields.Many2one('uom.uom','UoM')
    note = fields.Char('Note', size=128)

MRPSubcontractorScrap()


class MRPSubcontractReports(models.Model):
    _name = 'mrp.subcontract.reports'
    _order = 'name desc'
    _description = 'Subcontractor Reports Out'

    subcontract_id = fields.Many2one('mrp.subcontract','Subcontract')
    name = fields.Char("Description")
    jumlah = fields.Float("Jumlah", digits=dp.get_precision('Product Unit(s)'))
    jenis = fields.Char("Jenis")
    jumlah1 = fields.Float("Jumlah1", digits=dp.get_precision('Product Unit(s)'))
    sisa1 = fields.Float("Sisa1", digits=dp.get_precision('Product Unit(s)'))
    notes1 = fields.Char("Notes1")
    jumlah2 = fields.Float("Jumlah2", digits=dp.get_precision('Product Unit(s)'))
    sisa2 = fields.Float("Sisa2", digits=dp.get_precision('Product Unit(s)'))
    notes2 = fields.Char("Notes2")
    jumlah3 = fields.Float("Jumlah3", digits=dp.get_precision('Product Unit(s)'))
    sisa3 = fields.Float("Sisa3", digits=dp.get_precision('Product Unit(s)'))
    notes3 = fields.Char("Notes3")
    jumlah4 = fields.Float("Jumlah4", digits=dp.get_precision('Product Unit(s)'))
    sisa4 = fields.Float("Sisa4", digits=dp.get_precision('Product Unit(s)'))
    notes4 = fields.Char("Notes4")
    jumlah5 = fields.Float("Jumlah5", digits=dp.get_precision('Product Unit(s)'))
    sisa5 = fields.Float("Sisa5", digits=dp.get_precision('Product Unit(s)'))
    notes5 = fields.Char("Notes5")
    jumlah6 = fields.Float("Jumlah6", digits=dp.get_precision('Product Unit(s)'))
    sisa6 = fields.Float("Sisa6", digits=dp.get_precision('Product Unit(s)'))
    notes6 = fields.Char("Notes6")
    jumlah7 = fields.Float("Jumlah7", digits=dp.get_precision('Product Unit(s)'))
    sisa7 = fields.Float("Sisa7", digits=dp.get_precision('Product Unit(s)'))
    notes7 = fields.Char("Notes7")
    jumlah8 = fields.Float("Jumlah8", digits=dp.get_precision('Product Unit(s)'))
    sisa8 = fields.Float("Sisa8", digits=dp.get_precision('Product Unit(s)'))
    notes8 = fields.Char("Notes8")
    jumlah9 = fields.Float("Jumlah9", digits=dp.get_precision('Product Unit(s)'))
    sisa9 = fields.Float("Sisa9", digits=dp.get_precision('Product Unit(s)'))
    notes9 = fields.Char("Notes9")
    jumlah10 = fields.Float("Jumlah10", digits=dp.get_precision('Product Unit(s)'))
    sisa10 = fields.Float("Sisa10", digits=dp.get_precision('Product Unit(s)'))
    notes10 = fields.Char("Notes10")

    #11-20
    jumlah11 = fields.Float("Jumlah11", digits=dp.get_precision('Product Unit(s)'))
    sisa11 = fields.Float("Sisa11", digits=dp.get_precision('Product Unit(s)'))
    notes11 = fields.Char("Notes11")
    jumlah12 = fields.Float("Jumlah12", digits=dp.get_precision('Product Unit(s)'))
    sisa12 = fields.Float("Sisa12", digits=dp.get_precision('Product Unit(s)'))
    notes12 = fields.Char("Notes12")
    jumlah13 = fields.Float("Jumlah13", digits=dp.get_precision('Product Unit(s)'))
    sisa13 = fields.Float("Sisa13", digits=dp.get_precision('Product Unit(s)'))
    notes13 = fields.Char("Notes13")
    jumlah14 = fields.Float("Jumlah14", digits=dp.get_precision('Product Unit(s)'))
    sisa14 = fields.Float("Sisa14", digits=dp.get_precision('Product Unit(s)'))
    notes14 = fields.Char("Notes14")
    jumlah15 = fields.Float("Jumlah15", digits=dp.get_precision('Product Unit(s)'))
    sisa15 = fields.Float("Sisa15", digits=dp.get_precision('Product Unit(s)'))
    notes15 = fields.Char("Notes15")
    jumlah16 = fields.Float("Jumlah16", digits=dp.get_precision('Product Unit(s)'))
    sisa16 = fields.Float("Sisa16", digits=dp.get_precision('Product Unit(s)'))
    notes16 = fields.Char("Notes16")
    jumlah17 = fields.Float("Jumlah17", digits=dp.get_precision('Product Unit(s)'))
    sisa17 = fields.Float("Sisa17", digits=dp.get_precision('Product Unit(s)'))
    notes17 = fields.Char("Notes17")
    jumlah18 = fields.Float("Jumlah18", digits=dp.get_precision('Product Unit(s)'))
    sisa18 = fields.Float("Sisa18", digits=dp.get_precision('Product Unit(s)'))
    notes18 = fields.Char("Notes18")
    jumlah19 = fields.Float("Jumlah19", digits=dp.get_precision('Product Unit(s)'))
    sisa19 = fields.Float("Sisa19", digits=dp.get_precision('Product Unit(s)'))
    notes19 = fields.Char("Notes19")
    jumlah20 = fields.Float("Jumlah20", digits=dp.get_precision('Product Unit(s)'))
    sisa20 =  fields.Float("Sisa20", digits=dp.get_precision('Product Unit(s)'))
    notes20 = fields.Char("Notes20")

    #21-30
    jumlah21 = fields.Float("Jumlah21", digits=dp.get_precision('Product Unit(s)'))
    sisa21 = fields.Float("Sisa21", digits=dp.get_precision('Product Unit(s)'))
    notes21 = fields.Char("Notes21")
    jumlah22 = fields.Float("Jumlah22", digits=dp.get_precision('Product Unit(s)'))
    sisa22 = fields.Float("Sisa22", digits=dp.get_precision('Product Unit(s)'))
    notes22 = fields.Char("Notes22")
    jumlah23 = fields.Float("Jumlah23", digits=dp.get_precision('Product Unit(s)'))
    sisa23 = fields.Float("Sisa23", digits=dp.get_precision('Product Unit(s)'))
    notes23 = fields.Char("Notes23")
    jumlah24 = fields.Float("Jumlah24", digits=dp.get_precision('Product Unit(s)'))
    sisa24 = fields.Float("Sisa24", digits=dp.get_precision('Product Unit(s)'))
    notes24 = fields.Char("Notes24")
    jumlah25 = fields.Float("Jumlah25", digits=dp.get_precision('Product Unit(s)'))
    sisa25 = fields.Float("Sisa25", digits=dp.get_precision('Product Unit(s)'))
    notes25 = fields.Char("Notes25")
    jumlah26 = fields.Float("Jumlah26", digits=dp.get_precision('Product Unit(s)'))
    sisa26 = fields.Float("Sisa26", digits=dp.get_precision('Product Unit(s)'))
    notes26 = fields.Char("Notes26")
    jumlah27 = fields.Float("Jumlah27", digits=dp.get_precision('Product Unit(s)'))
    sisa27 = fields.Float("Sisa27", digits=dp.get_precision('Product Unit(s)'))
    notes27 = fields.Char("Notes27")
    jumlah28 = fields.Float("Jumlah28", digits=dp.get_precision('Product Unit(s)'))
    sisa28 = fields.Float("Sisa28", digits=dp.get_precision('Product Unit(s)'))
    notes28 = fields.Char("Notes28")
    jumlah29 = fields.Float("Jumlah29", digits=dp.get_precision('Product Unit(s)'))
    sisa29 = fields.Float("Sisa29", digits=dp.get_precision('Product Unit(s)'))
    notes29 = fields.Char("Notes29")
    jumlah30 = fields.Float("Jumlah30", digits=dp.get_precision('Product Unit(s)'))
    sisa30 =  fields.Float("Sisa30", digits=dp.get_precision('Product Unit(s)'))
    notes30 = fields.Char("Notes30")

MRPSubcontractReports()


class MRPSubcontractReportsIn(models.Model):
    _name = 'mrp.subcontract.reports.in'
    _order = 'name desc'
    _description = 'Subcontractor Reports In'

    subcontract_id = fields.Many2one('mrp.subcontract','Subcontract')
    name = fields.Char("Description")
    jumlah = fields.Float("Jumlah", digits=dp.get_precision('Product Unit(s)'))
    jenis = fields.Char("Jenis")
    jumlah1 = fields.Float("Jumlah 1", digits=dp.get_precision('Product Unit(s)'))
    sisa1 = fields.Float("Sisa 1", digits=dp.get_precision('Product Unit(s)'))
    jumlah2 = fields.Float("Jumlah 2", digits=dp.get_precision('Product Unit(s)'))
    sisa2 = fields.Float("Sisa 2", digits=dp.get_precision('Product Unit(s)'))
    jumlah3 = fields.Float("Jumlah 3", digits=dp.get_precision('Product Unit(s)'))
    sisa3 = fields.Float("Sisa 3", digits=dp.get_precision('Product Unit(s)'))
    jumlah4 = fields.Float("Jumlah 4", digits=dp.get_precision('Product Unit(s)'))
    sisa4 = fields.Float("Sisa 4", digits=dp.get_precision('Product Unit(s)'))
    jumlah5 = fields.Float("Jumlah 5", digits=dp.get_precision('Product Unit(s)'))
    sisa5 = fields.Float("Sisa 5", digits=dp.get_precision('Product Unit(s)'))
    jumlah6 = fields.Float("Jumlah 6", digits=dp.get_precision('Product Unit(s)'))
    sisa6 = fields.Float("Sisa 6", digits=dp.get_precision('Product Unit(s)'))
    jumlah7 = fields.Float("Jumlah 7", digits=dp.get_precision('Product Unit(s)'))
    sisa7 = fields.Float("Sisa 7", digits=dp.get_precision('Product Unit(s)'))
    jumlah8 = fields.Float("Jumlah 8", digits=dp.get_precision('Product Unit(s)'))
    sisa8 = fields.Float("Sisa 8", digits=dp.get_precision('Product Unit(s)'))
    jumlah9 = fields.Float("Jumlah 9", digits=dp.get_precision('Product Unit(s)'))
    sisa9 = fields.Float("Sisa 9", digits=dp.get_precision('Product Unit(s)'))
    jumlah10 = fields.Float("Jumlah 10", digits=dp.get_precision('Product Unit(s)'))
    sisa10 = fields.Float("Sisa 10", digits=dp.get_precision('Product Unit(s)'))
    #11-20
    jumlah11 = fields.Float("Jumlah 11", digits=dp.get_precision('Product Unit(s)'))
    sisa11 = fields.Float("Sisa 11", digits=dp.get_precision('Product Unit(s)'))
    jumlah12 = fields.Float("Jumlah 12", digits=dp.get_precision('Product Unit(s)'))
    sisa12 = fields.Float("Sisa 12", digits=dp.get_precision('Product Unit(s)'))
    jumlah13 = fields.Float("Jumlah 13", digits=dp.get_precision('Product Unit(s)'))
    sisa13 = fields.Float("Sisa 13", digits=dp.get_precision('Product Unit(s)'))
    jumlah14 = fields.Float("Jumlah 14", digits=dp.get_precision('Product Unit(s)'))
    sisa14 = fields.Float("Sisa 14", digits=dp.get_precision('Product Unit(s)'))
    jumlah15 = fields.Float("Jumlah 15", digits=dp.get_precision('Product Unit(s)'))
    sisa15 = fields.Float("Sisa 15", digits=dp.get_precision('Product Unit(s)'))
    jumlah16 = fields.Float("Jumlah 16", digits=dp.get_precision('Product Unit(s)'))
    sisa16 = fields.Float("Sisa 16", digits=dp.get_precision('Product Unit(s)'))
    jumlah17 = fields.Float("Jumlah 17", digits=dp.get_precision('Product Unit(s)'))
    sisa17 = fields.Float("Sisa 17", digits=dp.get_precision('Product Unit(s)'))
    jumlah18 = fields.Float("Jumlah 18", digits=dp.get_precision('Product Unit(s)'))
    sisa18 = fields.Float("Sisa 18", digits=dp.get_precision('Product Unit(s)'))
    jumlah19 = fields.Float("Jumlah 19", digits=dp.get_precision('Product Unit(s)'))
    sisa19 = fields.Float("Sisa 19", digits=dp.get_precision('Product Unit(s)'))
    jumlah20 = fields.Float("Jumlah 20", digits=dp.get_precision('Product Unit(s)'))
    sisa20 = fields.Float("Sisa 20", digits=dp.get_precision('Product Unit(s)'))
    #11-30
    jumlah21 = fields.Float("Jumlah 21", digits=dp.get_precision('Product Unit(s)'))
    sisa21 = fields.Float("Sisa 21", digits=dp.get_precision('Product Unit(s)'))
    jumlah22 = fields.Float("Jumlah 22", digits=dp.get_precision('Product Unit(s)'))
    sisa22 = fields.Float("Sisa 22", digits=dp.get_precision('Product Unit(s)'))
    jumlah23 = fields.Float("Jumlah 23", digits=dp.get_precision('Product Unit(s)'))
    sisa23 = fields.Float("Sisa 23", digits=dp.get_precision('Product Unit(s)'))
    jumlah24 = fields.Float("Jumlah 24", digits=dp.get_precision('Product Unit(s)'))
    sisa24 = fields.Float("Sisa 24", digits=dp.get_precision('Product Unit(s)'))
    jumlah25 = fields.Float("Jumlah 25", digits=dp.get_precision('Product Unit(s)'))
    sisa25 = fields.Float("Sisa 25", digits=dp.get_precision('Product Unit(s)'))
    jumlah26 = fields.Float("Jumlah 26", digits=dp.get_precision('Product Unit(s)'))
    sisa26 = fields.Float("Sisa 26", digits=dp.get_precision('Product Unit(s)'))
    jumlah27 = fields.Float("Jumlah 27", digits=dp.get_precision('Product Unit(s)'))
    sisa27 = fields.Float("Sisa 27", digits=dp.get_precision('Product Unit(s)'))
    jumlah28 = fields.Float("Jumlah 28", digits=dp.get_precision('Product Unit(s)'))
    sisa28 = fields.Float("Sisa 28", digits=dp.get_precision('Product Unit(s)'))
    jumlah29 = fields.Float("Jumlah 29", digits=dp.get_precision('Product Unit(s)'))
    sisa29 = fields.Float("Sisa 29", digits=dp.get_precision('Product Unit(s)'))
    jumlah30 = fields.Float("Jumlah 30", digits=dp.get_precision('Product Unit(s)'))
    sisa30 = fields.Float("Sisa 30", digits=dp.get_precision('Product Unit(s)'))

MRPSubcontractReportsIn()