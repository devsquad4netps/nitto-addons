{
    "name"          : "Furnace",
    "version"       : "1.5",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com/",
    "category"      : "MRP",
    "summary"       : "Input data sebelum furnace",
    "description"   : """
        
    """,
    "depends"       : [
        "mrp","vit_nitto_mrp","sh_barcode_scanner"
    ],
    "data"          : [
        "security/groups.xml",
        "security/ir.model.access.csv",
        "views/furnace.xml",
        "views/product.xml",
        "data/data.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}