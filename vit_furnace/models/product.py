from odoo import models, fields, api, _


class ProductTemplate(models.Model):
    _inherit           = "product.template"

    std_furnace_id = fields.Many2one('vit.standar.furnace', 'Std Furnace')
    std_furnace_master_id = fields.Many2one('vit.standar.furnace.master', 'Kode Std Furnace')

ProductTemplate()