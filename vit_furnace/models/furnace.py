from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning,ValidationError
from odoo.tools import float_compare
from datetime import date, datetime, time
from itertools import groupby



class VitFurnace(models.Model):
    _name           = "vit.furnace"
    _inherit        = ["barcodes.barcode_events_mixin", "mail.thread"]
    _description    = 'Data Sebelum Furnace'

    def _scan_mo(self, barcode): 
        #import pdb;pdb.set_trace()
        if self and self.state in ["cancel","confirm"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state), self.state)
            raise UserError(_("You can not scan item in %s state.") %(value))
        elif self:
            machine = self.env['mrp.machine'].sudo().search([('code','=',barcode)],limit=1)
            if machine :
                self.machine_id = machine.id
            std_furnace = self.env['vit.standar.furnace'].sudo().search([('name','=',barcode)],limit=1)
            if std_furnace :
                self.std_furnace_id = std_furnace.id
                self.machine_id = std_furnace_id.machine_id.id
            user = self.env['res.users'].sudo().search([('login','=',barcode)],limit=1)
            if user :
                self.operator_id = user.id
            else :
                barcode_scan = barcode.split('#')
                barcode = barcode_scan[0].strip()
                poly = False
                if len(barcode_scan) > 1 :
                    poly = barcode_scan[1].strip()
                production = self.env['mrp.production'].sudo().search([('name','=',barcode)],limit=1)
                #data = []
                if production :
                    if not production.product_id.std_furnace_master_id :
                        raise Warning('Product %s (%s) belum di set standar furnace' % (production.product_id.name,barcode))
                    if production.product_id.std_furnace_master_id != self.std_furnace_id.furnace_master_id :
                        raise Warning('Standar Furnace dokumen ini %s berbeda dengan MO %s (%s) ' % (self.std_furnace_id.furnace_master_id.name,production.name,production.product_id.std_furnace_master_id.name))
                    # data.append((0,0, {'production_id':production.id,
                    #                     'no_polybox':production.no_poly_box,
                    #                     'product_id': production.product_id.id,
                    #                     'date':str(production.date_planned_start)[:10],
                    #                     'shift' : self.shift or False,
                    #                     }))
                    if not poly :
                        poly = production.no_poly_box.strip()
                    vals = {
                            'production_id':production.id,
                            'no_polybox': poly,
                            'product_id': production.product_id.id,
                            # 'date':str(production.date_planned_start)[:10],
                            'date':self.date,
                        }
                    if self.shift:
                        vals.update({
                            'shift' : self.shift,                            
                        })
                    if self.furnace_detail_ids.filtered(lambda i:i.production_id.id == production.id and i.no_polybox == poly) :
                        raise Warning('Barcode (%s) sudah diinsert/scan (%s)' % (production.name,barcode))                      
                    new_furnace_line = self.furnace_detail_ids.new(vals)
                    self.furnace_detail_ids |= new_furnace_line
                else :
                    self.barcode = barcode
                    self.onchange_barcode()
                    #raise Warning('Barcode %s tidak di temukan di data Manufacturing Order' % (barcode))

    def on_barcode_scanned(self, barcode): 
        self._scan_mo(barcode)


    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode :
            machine = self.env['mrp.machine'].sudo().search([('code','=',self.barcode)],limit=1)
            if machine :
                self.machine_id = machine.id
                self.barcode = False
            else :
                std_furnace = self.env['vit.standar.furnace'].sudo().search([('name','=',self.barcode)],limit=1)
                if std_furnace :
                    self.std_furnace_id = std_furnace.id
                    self.machine_id = std_furnace.machine_id.id
                    self.barcode = False
                else :
                    user = self.env['res.users'].sudo().search([('login','=',self.barcode)],limit=1)
                    if user :
                        self.operator_id = user.id
                        self.barcode = False
                    else :
                        barcode_scan = self.barcode.split('#')
                        barcode = barcode_scan[0].strip()
                        poly = False
                        if len(barcode_scan) > 1 :
                            poly = barcode_scan[1].strip()
                        production = self.env['mrp.production'].sudo().search([('name','=',barcode)],limit=1)
                        #data = []
                        if production :
                            if not production.product_id.std_furnace_master_id :
                                self.barcode = False
                                self.warning = "Product %s (%s) belum di set standar furnace !"%(production.product_id.name,barcode)
                                return
                                #raise Warning('Product %s (%s) belum di set standar furnace' % (production.product_id.name,barcode))
                            if production.product_id.std_furnace_master_id != self.std_furnace_id.furnace_master_id :
                                self.barcode = False
                                self.warning = "Standar Furnace dokumen ini %s berbeda dengan MO %s (%s) !"%(self.std_furnace_id.furnace_master_id.name,production.name,production.product_id.std_furnace_master_id.name)
                                return
                                #raise Warning('Standar Furnace dokumen ini %s berbeda dengan MO %s (%s) ' % (self.std_furnace_id.furnace_master_id.name,production.name,production.product_id.std_furnace_master_id.name))
                            # data.append((0,0, {'production_id':production.id,
                            #                     'no_polybox':production.no_poly_box,
                            #                     'product_id': production.product_id.id,
                            #                     'date':str(production.date_planned_start)[:10],
                            #                     'shift' : self.shift or False,
                            #                     }))
                            if not poly :
                                poly = production.no_poly_box.strip()
                            vals = {
                                    'production_id':production.id,
                                    'no_polybox': poly,
                                    'product_id': production.product_id.id,
                                    # 'date':str(production.date_planned_start)[:10],
                                    'date':self.date,
                                }
                            if self.shift:
                                vals.update({
                                    'shift' : self.shift,                            
                                })
                            if self.furnace_detail_ids.filtered(lambda i:i.production_id.id == production.id and i.no_polybox == poly) :
                                self.barcode = False
                                self.warning = "Barcode %s sudah diinsert/scan (%s) !"%(production.name,barcode)
                                return
                            #     raise Warning('Barcode (%s) sudah diinsert/scan (%s)' % (production.name,barcode))                      
                            new_furnace_line = self.furnace_detail_ids.new(vals)
                            self.furnace_detail_ids |= new_furnace_line
                            self.barcode = False
                            self.warning = False
                        else :
                            self.barcode = False
                            self.warning = "Barcode %s tidak ditemukan di data Manufacturing Order !"%barcode
                           #self.env.cr.commit()
                            #raise Warning('Barcode %s tidak di temukan di data Manufacturing Order' % (barcode))

    name = fields.Char('Number',required=True, default='/')
    date = fields.Date('Date', default=fields.Date.context_today, track_visibility='on_change',required=True,readonly=True,states={'draft': [('readonly', False)]})
    # date = fields.Date('Date', required=True, track_visibility='on_change',
    #     default=lambda self: fields.Date.to_string(date.today().replace(month=2)),readonly=True,states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,readonly=True)
    machine_id = fields.Many2one('mrp.machine', 'Mesin', track_visibility='on_change', required=True,readonly=True,states={'draft': [('readonly', False)]})
    std_furnace_id = fields.Many2one('vit.standar.furnace', 'Std Furnace', track_visibility='on_change', required=True,readonly=True,states={'draft': [('readonly', False)]})
    operator_id = fields.Many2one('res.users','Operator', track_visibility='on_change', required=True,readonly=True,states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company','Company', required=True,default=lambda self: self.env.user.company_id, track_visibility='onchange',readonly=True)
    shift = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift",readonly=True,states={'draft': [('readonly', False)]})
    state = fields.Selection([('draft','Draft'),('cancel','Cancel'),('confirm','Confirm')],default='draft', track_visibility='on_change', string="State")
    notes = fields.Text('Notes', track_visibility='on_change')
    furnace_detail_ids = fields.One2many('vit.furnace.line', 'furnace_id', 'Details',readonly=True,states={'draft': [('readonly', False)]})
    barcode = fields.Char('Barcode', help="Jika scan otomatis bermasalah, maka letakan kursor disini",readonly=True,states={'draft': [('readonly', False)]})
    warning = fields.Text('Warning', help="warning message")

    @api.model
    def create(self,vals):
        if 'name' in vals and vals['name'] == '/' :
            number = self.env['ir.sequence'].next_by_code('vit.furnace')
            if number :
                vals['name'] = number
        elif 'name' not in vals :
            number = self.env['ir.sequence'].next_by_code('vit.furnace')
            if number :
                vals['name'] = number
            else :
                vals['name'] = '#'
        return super(VitFurnace, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(VitFurnace, self).unlink()

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

VitFurnace()


class VitFurnaceLine(models.Model):
    _name           = "vit.furnace.line"
    _description    = 'Data Sebelum Furnace (Detail)'

    number = fields.Integer('Number')
    furnace_id = fields.Many2one('vit.furnace','Furnace',ondelete='cascade')
    production_id = fields.Many2one('mrp.production','Manufacturing Order')
    no_polybox = fields.Char('No Polybox', size=20)
    product_id = fields.Many2one('product.product', 'Product')
    date = fields.Date('Date')
    shift = fields.Selection([('1','1'),('2','2'),('3','3')], string="Shift")
    #std_furnace_id = fields.Many2one('vit.standar.furnace', 'Std Furnace')
    std_furnace_header_id = fields.Many2one('vit.standar.furnace', 'Std Furnace Header', related="furnace_id.std_furnace_id", store=True)
    std_furnace_header_master_id = fields.Many2one('vit.standar.furnace.master', 'Std Furnace Header (Master)', related="furnace_id.std_furnace_id.furnace_master_id", store=True)
    std_furnace_id = fields.Many2one('vit.standar.furnace', 'Std Furnace', related="product_id.std_furnace_id", store=True)
    std_furnace_master_id = fields.Many2one('vit.standar.furnace.master', 'Std Furnace', related="product_id.std_furnace_master_id", store=True)

    @api.onchange('production_id')
    def onchange_production_id(self):
        if self.production_id :
            self.no_polybox = self.production_id.no_poly_box
            self.product_id = self.production_id.product_id.id
            if self.furnace_id :
                self.shift = self.furnace_id.shift
                if self.furnace_id.date:
                    self.date  = self.furnace_id.date
            else :
                self.date = str(self.production_id.date_planned_start)[:10]
                #self.std_furnace_id =  self.production_id.product_id.std_furnace_id.id
            if self.production_id.product_id.std_furnace_master_id != self.furnace_id.std_furnace_id.furnace_master_id :
                raise Warning('Standar Furnace berbeda (%s)' % self.production_id.product_id.std_furnace_master_id.name)

VitFurnaceLine()


class VitStandarFurnace(models.Model):
    _name           = "vit.standar.furnace"
    _description    = 'Data Mapping Standar Furnace'

    @api.multi
    @api.depends('name', 'furnace_master_id')
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.furnace_master_id and res.furnace_master_id.description :
                name = '['+res.name+'] '+res.furnace_master_id.description
            result.append((res.id, name))
        return result


    machine_id = fields.Many2one('mrp.machine', 'Mesin', required=True)
    name = fields.Char('Reference', required=False)
    furnace_master_id = fields.Many2one('vit.standar.furnace.master','Kode Furnace')

    @api.onchange('machine_id','furnace_master_id')
    def onchange_furnace_master_id(self):
        if self.machine_id :
            self.name = self.machine_id.code
        if self.machine_id and self.furnace_master_id:
            self.name = str(self.machine_id.code)+str(self.furnace_master_id.name)

    _sql_constraints = [
        ('machine_furnace_master_uniq', 'unique (machine_id,furnace_master_id)', 'Mesin dan kode standar furnace harus unik'),
    ]

VitStandarFurnace()

class VitStandarFurnaceMaster(models.Model):
    _name           = "vit.standar.furnace.master"
    _description    = 'Data Master Standar Furnace'
    _order          = 'name asc'

    @api.multi
    @api.depends('name', 'description')
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.description :
                name = '['+res.name+'] '+res.description
            result.append((res.id, name))
        return result

    name = fields.Char('Name', required=True)
    description = fields.Char('Description', required=False)

    @api.onchange('name')
    def onchange_name(self):
        if self.name :
            self.name = self.name.upper()

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Nama standar furnace harus unik'),
    ]

VitStandarFurnaceMaster()