{
	"name": "MRP Nitto",
	"version": "1.9", 
	"depends": [
		"base",
		"mrp",
		"hr",
		"vit_nitto_partner"
		# "vit_subcontractor",
	],
	"author": "http://www.vitraining.com", 
	"category": "Tool", 
	'website': 'http://www.vitraining.com',
	"description": """\
Base MRP nitto
--------------------------------------------------


""",
	"data": [
		"security/group.xml",
		"security/ir.model.access.csv",
		"views/mrp_bom.xml",
		"views/mrp_workorder.xml",
		"views/mrp_production.xml",
		"views/hr_department.xml",
		"views/mrp_machine.xml",
		"report/qr_card.xml",
		"data/data.xml",
	],
	"installable": True,
	"auto_install": False,
	"application": True,
}