from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class StockMove(models.Model):
    _inherit = 'stock.move'

    def _action_cancel(self):
        res = False
        for move in self:
            prod_id = move.production_id
            if prod_id:
                move.production_id = False
                res = super(StockMove, self)._action_cancel()
                move.production_id = prod_id
            else :
                res = super(StockMove, self)._action_cancel()
        if not res :
            res = super(StockMove, self)._action_cancel()
        return res

StockMove()