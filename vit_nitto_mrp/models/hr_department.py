from odoo import models, fields, api

class HrDepartment(models.Model):
    _inherit = 'hr.department'


    code        = fields.Char("Code", size=3)
    sequence    = fields.Integer("Sequence")

HrDepartment()