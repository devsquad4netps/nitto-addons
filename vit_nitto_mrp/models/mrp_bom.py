from odoo import models, fields, api
from odoo.addons import decimal_precision as dp

class mrp_bom(models.Model):
    _inherit = 'mrp.bom'

    qty_pcs         = fields.Float(string='Qty/Kg',
        digits=dp.get_precision('Product Unit of Measure'))
    weight_perbox   = fields.Float(string='Berat Per Box',
        digits=dp.get_precision('Product Unit of Measure'))

mrp_bom()


class mrp_bom_line(models.Model):
    _inherit = 'mrp.bom.line'

    product_qty = fields.Float(
        'Product Quantity', default=1.0,
        digits=(2,6), required=True)

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id :
            if self.product_id.categ_id.name in ('WIRE','Wire','wire') :
                if self._context.get('params', False) :
                    bom = self._context.get('params')
                    if bom and 'id' in bom:
                        bom_id = bom['id']
                        bom = self.env['mrp.bom'].browse(bom_id)
                        pal = bom.product_tmpl_id.kg_pal
                        box = bom.product_tmpl_id.Std_PolyBox
                        if pal > 0 and box > 0.0 :
                            self.product_qty = (pal/box)*bom.product_qty
            self.product_uom_id = self.product_id.uom_id.id


    def button_compute_product_qty(self):
        for x in self :
            if x.product_id and x.bom_id:
                pal = x.bom_id.product_tmpl_id.kg_pal
                box = x.bom_id.product_tmpl_id.Std_PolyBox
                if pal > 0 and box > 0.0 :
                    x.product_qty = (pal/box)*x.bom_id.product_qty
                    x.product_uom_id = x.product_id.uom_id.id

mrp_bom_line()