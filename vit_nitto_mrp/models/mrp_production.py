from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)

class mrp_production(models.Model):
    _inherit = 'mrp.production'

    customer_id          = fields.Many2one("res.partner","Customer", domain="[('customer','=',True)]", track_visibility='on_change')
    department_id        = fields.Many2one("hr.department","Department", track_visibility='on_change')
    force_number         = fields.Char('Force Number', size=200, copy=False, track_visibility='on_change')
    auto_plan            = fields.Boolean('Auto Plan', copy=False, track_visibility='on_change', default=True)

    @api.onchange('product_id', 'picking_type_id', 'company_id')
    def onchange_product_id(self):
        res = super(mrp_production, self).onchange_product_id()
        if self.product_id:
            if self.product_id.categ_id.picking_type_production_id :
                self.picking_type_id = self.product_id.categ_id.picking_type_production_id.id
                if self.product_id.categ_id.picking_type_production_id.default_location_src_id :
                    self.location_src_id = self.product_id.categ_id.picking_type_production_id.default_location_src_id.id
                if self.product_id.categ_id.picking_type_production_id.default_location_dest_id :
                    self.location_dest_id = self.product_id.categ_id.picking_type_production_id.default_location_dest_id.id 
        return res

    @api.model
    def create(self, vals):
        if 'force_number' in vals :
            vals['name'] = vals['force_number']
        res = super(mrp_production, self).create(vals)
        dept = res.department_id
        if dept :
            dept_code = res.department_id.code
            end_code = res.name[3:]
            res.name = dept_code+end_code
            end_seq = res.name[:10]
            res.name = end_seq+str(dept.sequence).zfill(3)
            dept.sudo().write({'sequence':dept.sequence+1})
        if res.product_id.categ_id.picking_type_production_id :
            res.picking_type_id = res.product_id.categ_id.picking_type_production_id.id
            if res.product_id.categ_id.picking_type_production_id.default_location_src_id :
                res.location_src_id = res.product_id.categ_id.picking_type_production_id.default_location_src_id.id
            if res.product_id.categ_id.picking_type_production_id.default_location_dest_id :
                res.location_dest_id = res.product_id.categ_id.picking_type_production_id.default_location_dest_id.id
        # if res.force_number :
        #     res.name = res.force_number
        return res

    @api.multi
    def _get_produced_qty(self):
        res = super(mrp_production, self)._get_produced_qty()
        for production in self:
            if production.move_finished_ids :
                outstanding = production.move_finished_ids.filtered(lambda x: x.state not in ('cancel','done') and x.product_id.id == production.product_id.id) 
                if not outstanding and production.state == 'progress':
                    production.check_to_done = True
        return True

    @api.multi
    def button_plan(self):
        for mrp in self :
            res = super(mrp_production, mrp).button_plan()
            self.env.cr.commit()
        return res

    @api.multi
    def button_mark_done(self):
        res = super(mrp_production, self).button_mark_done()
        for mrp in self:
            if mrp.state == 'done':
                for wo in mrp.workorder_ids.filtered(lambda work:work.is_wip == True):
                    sql = "update mrp_workorder set is_wip=false where id = %s " % ( wo.id)
                    mrp.env.cr.execute(sql)
        return res

    @api.model
    def auto_button_plan(self):
        to_exec = self.search([('auto_plan','=',True),('state','=','confirmed')]) 
        for mrp in to_exec :
            try :
                mrp.button_plan()
                _logger.info("Run scheduler ========================> MO %s planned "%str(mrp.name))
            except :
                continue
        return True

    @api.model
    def auto_wip_mo(self):
        to_exec = self.env['mrp.workorder'].search([('state','in',('planned','progress'))]) 
        for mrp in to_exec :
            try :
                if mrp.workorder_ids :
                    wip_exist = mrp.workorder_ids.filtered(lambda mo:mo.is_wip)
                    wip_h_exist_wrong = mrp.workorder_ids.filtered(lambda wo:wo.is_wip and wo.next_work_order_id.state == 'done')
                    if not wip_exist or wip_h_exist_wrong:
                        for wo in mrp.workorder_ids :
                            if wo.state == 'done' :
                                if wo.next_work_order_id.state in ('pending','ready'):
                                    sql = "update mrp_workorder set is_wip=true where id=%s" % ( wo.id)
                                    wo.env.cr.execute(sql)
                                if wo.next_work_order_id.state == 'done':
                                    sql = "update mrp_workorder set is_wip=false where id=%s" % ( wo.id)
                                    wo.env.cr.execute(sql)
                                if wo.next_work_order_id.state == 'progress':
                                    sql = "update mrp_workorder set is_wip=true where id=%s" % ( wo.next_work_order_id.id)
                                    wo.env.cr.execute(sql)
                                self.env.cr.commit()
                                _logger.info("Run update is_wip success ========================> MO %s"%str(mrp.name))
            except :
                continue
        to_exec2 = self.env['mrp.workorder'].search([('state','=','done'),
                                                    ('next_work_order_id','=',False),
                                                    ('is_wip','=',True)]) 
        for wo in to_exec2 :
            try :
                sql = "update mrp_workorder set is_wip=false where id=%s" % ( wo.id)
                wo.env.cr.execute(sql)
                _logger.info("Run update is_wip = false success ========================> MO %s"%str(wo.name))
            except :
                continue
        return True

mrp_production()


class ProductCategory(models.Model):
    _inherit = 'product.category'

    picking_type_production_id = fields.Many2one('stock.picking.type', 'Picking Type Manufacture', company_dependent=True)

ProductCategory()