# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning
from odoo.addons import decimal_precision as dp


class MrpMachine(models.Model):
    _name = 'mrp.machine'
    _description = 'Master Mesin'

    @api.multi
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.code :
                name = '['+res.code+'] '+name
            if res.company_id and res.company_id.second_name:
                name = name+' - '+res.company_id.second_name
            result.append((res.id, name))
        return result

    code = fields.Char('Reference', size=12, required=True)
    name = fields.Char('Name', size=180, required=True)
    bagian_id = fields.Many2one('mrp.machine.bagian','Bagian')
    line_id = fields.Many2one('mrp.machine.line','Line')
    group_id = fields.Many2one('mrp.machine.group','Group')
    workcenter_id = fields.Many2one('mrp.workcenter','Workcenter')
    company_id = fields.Many2one('res.company','Company')
    description = fields.Text('Description')

    _sql_constraints = [
        ('code_comp_uniq', 'unique(code, company_id)',
         ("Reference must be uniq percompany!"))
    ]

MrpMachine()

class MrpMachineBagian(models.Model):
    _name = 'mrp.machine.bagian'
    _description = 'Master Mesin Bagian'

    name = fields.Char('Name', size=180, required=True)
    description = fields.Text('Description')
    company_id = fields.Many2one('res.company','Company')

    _sql_constraints = [
        ('name_comp_uniq', 'unique(name, company_id)',
         ("Reference must be uniq percompany!"))
    ]

MrpMachineBagian()

class MrpMachineLine(models.Model):
    _name = 'mrp.machine.line'
    _description = 'Master Mesin Line'

    name = fields.Char('Name', size=180, required=True)
    description = fields.Text('Description')
    company_id = fields.Many2one('res.company','Company')

    _sql_constraints = [
        ('name_comp_uniq', 'unique(name, company_id)',
         ("Reference must be uniq percompany!"))
    ]

MrpMachineLine()

class MrpMachineGroup(models.Model):
    _name = 'mrp.machine.group'
    _description = 'Master Mesin Group'

    name = fields.Char('Name', size=180, required=True)
    description = fields.Text('Description')
    company_id = fields.Many2one('res.company','Company')

    _sql_constraints = [
        ('name_comp_uniq', 'unique(name, company_id)',
         ("Reference must be uniq percompany!"))
    ]

MrpMachineGroup()