from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class mrp_workorder(models.Model):
    _inherit = 'mrp.workorder'

    @api.multi
    def button_start(self):
      if not self.user_has_groups('vit_nitto_mrp.group_bypass_scan_wo'):
        if not self.user_id or not self.machine_id :
            raise ValidationError("Operator dan id mesin harus diisi !")
      return super(mrp_workorder, self).button_start()

    @api.multi
    def record_production(self):
      if not self.user_has_groups('vit_nitto_mrp.group_bypass_scan_wo'):
        if not self.user_id or not self.machine_id :
            raise ValidationError("Operator dan id mesin harus diisi !")
      return super(mrp_workorder, self).record_production()

    iduser          = fields.Char(string='Operator', track_visibility='onchange')
    barcodewo       = fields.Char(string='ID Mesin', track_visibility='onchange')
    machine_id      = fields.Many2one(comodel_name="mrp.machine",string="Machine", track_visibility='onchange')
    user_id         = fields.Many2one(comodel_name="res.users", required=False, string="Operator", track_visibility='onchange')
    origin          = fields.Char(string='Source Document', related="production_id.origin",store=True)
    product_id      = fields.Many2one(
                        'product.product', 'Product',
                        related='production_id.product_id', readonly=True,
                        help='Technical: used in views only.', store=True)
    customer_id      = fields.Many2one("res.partner",string='Customer', related="production_id.customer_id", store=True)

    @api.onchange('iduser')
    def onchange_iduser(self):
        # pdb.set_trace()
        for x in self:
            if x.iduser:
                iduser = (x.iduser,)
                cr = self.env.cr
                cr.execute("SELECT id FROM res_users WHERE login=%s", (iduser))
                obj = cr.fetchall()
                if obj != []:
                    obj_id = obj[0][0]
                    if obj_id:
                        self.user_id = obj_id
                else:
                    raise ValidationError("Data user tidak di temukan")

    @api.onchange('barcodewo')
    def onchange_barcodewo(self):
        for x in self:
            if x.barcodewo:
                machine = self.env['mrp.machine'].search([('code','=',x.barcodewo),('company_id','=',x.production_id.company_id.id)],limit=1)
                if not machine :
                    raise ValidationError("Data mesin tidak di temukan")
                x.machine_id = machine[0].id

mrp_workorder()
